/*
 * @file         Lido.java
 * @creation     1999-01-17
 * @modification $Date: 2007-01-19 13:14:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * @version      $Revision: 1.11 $ $Date: 2007-01-19 13:14:29 $ by $Author: deniger $
 * @author       Axel von Arnim , Mickael Rubens
 */
public class Lido {
  /**
   * @param args les arguments
   */
  public static void main(final String args[]) {
    final Fudaa f=new Fudaa();
    f.launch(args,LidoImplementation.informationsSoftware(),false);
    f.startApp(new LidoImplementation());  }
}
