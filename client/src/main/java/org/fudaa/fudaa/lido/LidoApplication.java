/*
 * @file         LidoApplication.java
 * @creation     1999-01-15
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import com.memoire.bu.BuApplication;
/**
 * Application Lido
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim , Mickael Rubens
 */
public class LidoApplication extends BuApplication {
  public static java.awt.Frame FRAME= null;
  public LidoApplication() {
    super();
    FRAME= this;
    final LidoImplementation app= new LidoImplementation();
    setImplementation(app);
  }
}
