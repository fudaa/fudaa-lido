/*
 * @file         LidoAssistant.java
 * @creation     1999-10-12
 * @modification $Date: 2003-11-25 10:13:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import com.memoire.bu.BuAssistant;
/**
 * L'assistant du client Lido.
 *
 * @version      $Id: LidoAssistant.java,v 1.6 2003-11-25 10:13:48 deniger Exp $
 * @author       Axel von Arnim 
 */
public class LidoAssistant extends BuAssistant {}
