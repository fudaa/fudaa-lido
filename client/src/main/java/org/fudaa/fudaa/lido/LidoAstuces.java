/*
 * @file         LidoAstuces.java
 * @creation     2001-10-15
 * @modification $Date: 2007-01-19 13:14:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
/**
 * @version      $Id: LidoAstuces.java,v 1.11 2007-01-19 13:14:29 deniger Exp $
 * @author       Fred Denier
 */
public class LidoAstuces extends FudaaAstucesAbstract {
  public static LidoAstuces LIDO= new LidoAstuces();
  protected FudaaAstucesAbstract getParent() {
    return FudaaAstuces.FUDAA;
  }
  protected BuPreferences getPrefs() {
    return LidoPreferences.LIDO;
  }
}
