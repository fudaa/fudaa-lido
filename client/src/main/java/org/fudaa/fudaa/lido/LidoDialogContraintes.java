/*
 * @file         LidoDialogContraintes.java
 * @creation     2000-02-28
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JComponent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoDialogContraintes extends BuDialog {
  public final static int IGNORER= 165;
  public final static int RETOUR= 166;
  JButton btRetour_, btIgnorer_;
  public LidoDialogContraintes(
    final BuCommonInterface _parent,
    final BuInformationsSoftware _isoft,
    final Object _message,
    final boolean can_ignore) {
    super(_parent, _isoft, BuResource.BU.getString("Contraintes"), _message);
    final BuPanel pnb= new BuPanel();
    pnb.setLayout(new FlowLayout(FlowLayout.RIGHT));
    btRetour_= new BuButton(BuResource.BU.getString("Retour"));
    btRetour_.addActionListener(this);
    getRootPane().setDefaultButton(btRetour_);
    pnb.add(btRetour_);
    if (can_ignore) {
      btIgnorer_= new BuButton(BuResource.BU.getString("Ignorer"));
      btIgnorer_.addActionListener(this);
      pnb.add(btIgnorer_);
    }
    content_.add(BorderLayout.SOUTH, pnb);
  }
  public JComponent getComponent() {
    return null;
  }
  public void actionPerformed(final ActionEvent ev) {
    final JComponent source= (JComponent)ev.getSource();
    if (source == btIgnorer_) {
      reponse_= IGNORER;
    } else if (source == btRetour_) {
      reponse_= RETOUR;
    }
    dispose();
  }
}
