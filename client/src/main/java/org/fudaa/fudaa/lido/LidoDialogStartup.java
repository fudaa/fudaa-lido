/*
 * @file         LidoDialogStartup.java
 * @creation     1999-12-04
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoDialogStartup extends BuDialog {
  JButton btContinuer;
  JCheckBox cbShow;
  public LidoDialogStartup(
    final BuCommonInterface _parent,
    final BuInformationsSoftware _isoft,
    final Object _message) {
    super(_parent, _isoft, BuResource.BU.getString("Message"), _message);
    final BuPanel pnb= new BuPanel();
    pnb.setLayout(new BorderLayout());
    cbShow=
      new JCheckBox(
        BuResource.BU.getString("Afficher cet �cran la prochaine fois"));
    cbShow.setSelected(true);
    pnb.add(BorderLayout.WEST, cbShow);
    btContinuer=
      new BuButton(
        BuLib.loadCommandIcon("CONTINUER"),
        BuResource.BU.getString("Continuer"));
    btContinuer.addActionListener(this);
    getRootPane().setDefaultButton(btContinuer);
    pnb.add(BorderLayout.EAST, btContinuer);
    content_.add(BorderLayout.SOUTH, pnb);
  }
  public JComponent getComponent() {
    return null;
  }
  public void actionPerformed(final ActionEvent ev) {
    final JComponent source= (JComponent)ev.getSource();
    if (source == btContinuer) {
      reponse_= JOptionPane.OK_OPTION;
      setVisible(false);
    }
  }
  public boolean isShowNextTime() {
    return cbShow.isSelected();
  }
}
