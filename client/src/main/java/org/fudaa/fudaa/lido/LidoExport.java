/*
 * @file         LidoExport.java
 * @creation     1999-12-23
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuFileChooser;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFileChooser;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.dodico.lido.DParametresLido;

import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.14 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public final class LidoExport {
  private LidoExport() {}
  public static void exportResultatTXT(
    final SResultatsBiefRSN[] biefs,
    final String type,
    final File filename)
    throws IOException {
    System.err.println("export " + type + " dans " + filename.getPath());
    final PrintWriter fic= new PrintWriter(new FileOutputStream(filename));
    fic.println("R�sultats de '" + type + "' pour les biefs s�lectionn�s");
    for (int i= 0; i < biefs.length; i++) {
      fic.println();
      fic.println("Bief " + biefs[i].numero + " : ");
      fic.println(
        "num�ro abscisse "
          + (LidoResource.RESULTAT.LIGNEDEAU.equals(type)
            ? "ligne_eau"
            : LidoResource.RESULTAT.HAUTEUREAU.equals(type)
            ? "hauteur_eau"
            : LidoResource.RESULTAT.SECTIONMOUILLEE.equals(type)
            ? "lit_mineur lit_majeur"
            : LidoResource.RESULTAT.RAYON.equals(type)
            ? "lit_mineur lit_majeur"
            : LidoResource.RESULTAT.B1.equals(type)
            ? "largeur"
            : LidoResource.RESULTAT.VITESSE.equals(type)
            ? "lit_mineur lit_majeur"
            : LidoResource.RESULTAT.DEBITTOTAL.equals(type)
            ? "d�bit"
            : LidoResource.RESULTAT.FROUDE.equals(type)
            ? "Froude"
            : LidoResource.RESULTAT.DEBIT.equals(type)
            ? "lit_mineur lit_majeur"
            : LidoResource.RESULTAT.CHARGE.equals(type)
            ? "charge"
            : LidoResource.RESULTAT.REGIMECRIT.equals(type)
            ? "hauteur_critique"
            : LidoResource.RESULTAT.REGIMEUNIF.equals(type)
            ? "hauteur_normale"
            : LidoResource.RESULTAT.FORCETRAC.equals(type)
            ? "force_tractrice"
            : "variable_inconnue"));
      for (int j= 0; j < biefs[i].ligne.length; j++) {
        // calcul hauteur critique
        final double hc=
          biefs[i].ligne[j].y
            + (biefs[i].ligne[j].vmin
              * biefs[i].ligne[j].vmin
              / (2 * LidoResource.CONSTANTE.G));
        fic.println(
          biefs[i].ligne[j].i
            + " "
            + biefs[i].ligne[j].x
            + " "
            + (LidoResource.RESULTAT.LIGNEDEAU.equals(type)
              ? biefs[i].ligne[j].z + ""
              : LidoResource.RESULTAT.HAUTEUREAU.equals(type)
              ? biefs[i].ligne[j].y + ""
              : LidoResource.RESULTAT.SECTIONMOUILLEE.equals(type)
              ? biefs[i].ligne[j].s1 + " " + biefs[i].ligne[j].s2
              : LidoResource.RESULTAT.RAYON.equals(type)
              ? biefs[i].ligne[j].r1 + " " + biefs[i].ligne[j].r2
              : LidoResource.RESULTAT.B1.equals(type)
              ? biefs[i].ligne[j].b1 + ""
              : LidoResource.RESULTAT.VITESSE.equals(type)
              ? biefs[i].ligne[j].vmin + " " + biefs[i].ligne[j].vmaj
              : LidoResource.RESULTAT.DEBITTOTAL.equals(type)
              ? biefs[i].ligne[j].q + ""
              : LidoResource.RESULTAT.FROUDE.equals(type)
              ? biefs[i].ligne[j].froude + ""
              : LidoResource.RESULTAT.DEBIT.equals(type)
              ? biefs[i].ligne[j].qmin + " " + biefs[i].ligne[j].qmaj
              : LidoResource.RESULTAT.CHARGE.equals(type)
              ? biefs[i].ligne[j].charge + ""
              : LidoResource.RESULTAT.REGIMECRIT.equals(type)
              ? hc + ""
              : LidoResource.RESULTAT.REGIMEUNIF.equals(type)
              ? 0. + ""
              : LidoResource.RESULTAT.FORCETRAC.equals(type)
              ? 0. + ""
              : 0. + ""));
      }
    }
    fic.close();
  }
  public static void exportCLM(
    final SParametresCondLimBlocCLM[] lois,
    final File filename) {
    final SParametresCLM clmTmp= LidoParamsHelper.creeParametresCLM();
    clmTmp.condLimites= lois;
    clmTmp.nbCondLim= lois.length;
    DParametresLido.ecritParametresCLM(filename, clmTmp);
  }
  public static void exportEnveloppeLigneDeau(final SResultatsRSN rsn, final File filename)
    throws IOException {
    if (rsn.pasTemps.length < 2) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "Il faut au moins deux pas de temps\n"
          + "pour exporter l'enveloppe des lignes d'eau!")
        .activate();
    }
    final PrintWriter out= new PrintWriter(new FileWriter(filename));
    for (int bi= 0; bi < rsn.pasTemps[0].ligBief.length; bi++) {
      out.println("\nBief " + (bi + 1) + "\n");
      out.println("I X minH maxH");
      final SResultatsBiefRSN b= rsn.pasTemps[0].ligBief[bi];
      for (int i= 0; i < b.ligne.length; i++) {
        double minH= Double.POSITIVE_INFINITY;
        double maxH= Double.NEGATIVE_INFINITY;
        for (int t= 0; t < rsn.pasTemps.length; t++) {
          final SResultatsBiefRSN bp= rsn.pasTemps[t].ligBief[bi];
          if (bp.ligne[i].z < minH) {
            minH= bp.ligne[i].z;
          }
          if (bp.ligne[i].z > maxH) {
            maxH= bp.ligne[i].z;
          }
        }
        out.println(i + " " + b.ligne[i].x + " " + minH + " " + maxH);
      }
    }
    out.close();
  }
  public static void exportResultatsProfil(
    final SParametresBiefBlocPRO prof,
    final SResultatsRSN rsn,
    final File filename,
    final LidoParamsHelper ph)
    throws IOException {
    final int bief= ph.BIEF().getBiefContenantAbscisse(prof.abscisse);
    if (bief == -1) {
      return;
    }
    int section= -1;
    for (int i= 0; i < rsn.pasTemps[0].ligBief[bief].ligne.length; i++) {
      if (Math
        .round(
          LidoResource.PRECISION * rsn.pasTemps[0].ligBief[bief].ligne[i].x)
        >= Math.round(LidoResource.PRECISION * prof.abscisse)) {
        section= i;
        break;
      }
    }
    if (section == -1) {
      return;
    }
    final PrintWriter fic= new PrintWriter(new FileOutputStream(filename));
    fic.println(
      "R�sultats pour le profil "
        + (prof.indice + 1)
        + ": abscisse="
        + prof.abscisse);
    fic.println();
    fic.println(
      "temps x z y s1 s2 r1 r2 b1 vmin vmaj q froude qmin qmaj charge");
    for (int t= 0; t < rsn.pasTemps.length; t++) {
      fic.println(
        ""
          + rsn.pasTemps[t].t
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].x
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].z
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].y
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].s1
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].s2
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].r1
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].r2
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].b1
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].vmin
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].vmaj
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].q
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].froude
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].qmin
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].qmaj
          + " "
          + rsn.pasTemps[t].ligBief[bief].ligne[section].charge);
    }
    fic.close();
  }
  public static File chooseFile(final String _ext) {
    final String ext= _ext;
    File res= null;
    final BuFileChooser chooser= new CtuluFileChooser(false);
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
      public boolean accept(final java.io.File f) {
        return (ext == null)
          ? true
          : (f.getName().endsWith(CtuluLibString.DOT + ext) || f.isDirectory());
      }
      public String getDescription() {
        return (ext == null)
          ? "*.*"
          : "Fichiers " + ext.toUpperCase() + " (*." + ext + ")";
      }
    });
    chooser.setCurrentDirectory(LidoResource.lastExportDir);
    final int returnVal= chooser.showSaveDialog(LidoApplication.FRAME);
    String filename= null;
    if (returnVal != JFileChooser.APPROVE_OPTION) {
      return null;
    }
    filename= chooser.getSelectedFile().getPath();
    if (filename == null) {
      return null;
    }
    final int indexSlash= filename.lastIndexOf(java.io.File.separatorChar);
    if ((ext != null) && (!filename.endsWith(CtuluLibString.DOT + ext))) {
      final int index= filename.lastIndexOf('.');
      if (index > indexSlash) {
        filename= filename.substring(0, index);
      }
      filename += CtuluLibString.DOT + ext;
    }
    res= new File(filename);
    if (res.exists()) {
      if (new BuDialogConfirmation((BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME)
          .getImplementation()
          .getInformationsSoftware(),
        "Le fichier :\n"
          + filename
          + "\nexiste d�j�. Voulez-vous l'�craser?\n")
        .activate()
        == JOptionPane.NO_OPTION) {
        return null;
      }
    }
    if (indexSlash > 0) {
      LidoResource.lastExportDir=
        new java.io.File(filename.substring(0, indexSlash));
    } else {
      LidoResource.lastExportDir= chooser.getCurrentDirectory();
    }
    return res;
  }
}
