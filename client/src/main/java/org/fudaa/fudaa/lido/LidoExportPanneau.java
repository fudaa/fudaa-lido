/*
 * @file         LidoExportPanneau.java
 * @creation     1999-10-03
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * Une boite de selection d'exports pour Lido.
 *
 * @version      $Id: LidoExportPanneau.java,v 1.10 2006-09-19 15:05:01 deniger Exp $
 * @author       Axel von Arnim
 */
public class LidoExportPanneau
  extends JDialog
  implements ActionListener, WindowListener {
  // Donnees membres publiques
  public final static int CANCEL= 0;
  public final static int OK= 1;
  // Donnees membres privees
  private int status_;
  private File dir_;
  private BuPanel pnChoix_, pnOk_, pnGen_;
  private JCheckBox cbAll_;
  private BuTextField tfDir_;
  private JButton b_ok_, b_cancel_, btDir_;
  private Hashtable cbs_;
  private String[] choix_;
  // Constructeurs
  public LidoExportPanneau(final Frame _parent, final FudaaProjet p) {
    super(_parent, "Exportation", true);
    status_= CANCEL;
    dir_= new File(System.getProperty("user.dir"));
    int n= 0;
    pnChoix_= new BuPanel();
    pnChoix_.setLayout(new BuGridLayout(2, 5, 5, true, true));
    pnChoix_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    cbs_= new Hashtable();
    JCheckBox cb;
    cb= new JCheckBox(LidoResource.PRO);
    cb.setEnabled(p.containsParam(LidoResource.PRO));
    cbs_.put(LidoResource.PRO, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.RZO);
    cb.setEnabled(p.containsParam(LidoResource.RZO));
    cbs_.put(LidoResource.RZO, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.CAL);
    cb.setEnabled(p.containsParam(LidoResource.CAL));
    cbs_.put(LidoResource.CAL, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.CLM);
    cb.setEnabled(p.containsParam(LidoResource.CLM));
    cbs_.put(LidoResource.CLM, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.SNG);
    cb.setEnabled(p.containsParam(LidoResource.SNG));
    cbs_.put(LidoResource.SNG, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.LIG);
    cb.setEnabled(p.containsParam(LidoResource.LIG));
    cbs_.put(LidoResource.LIG, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.ERN);
    cb.setEnabled(p.containsResult(LidoResource.ERN));
    cbs_.put(LidoResource.ERN, cb);
    pnChoix_.add(cb, n++);
    cb= new JCheckBox(LidoResource.RSN);
    cb.setEnabled(p.containsResult(LidoResource.RSN));
    cbs_.put(LidoResource.RSN, cb);
    pnChoix_.add(cb, n++);
    choix_= new String[cbs_.size()];
    n= 0;
    pnGen_= new BuPanel();
    pnGen_.setLayout(new BuVerticalLayout());
    pnGen_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    pnGen_.add(pnChoix_, n++);
    cbAll_= new JCheckBox("Toute l'�tude");
    cbAll_.setSelected(false);
    cbAll_.addActionListener(this);
    pnGen_.add(cbAll_, n++);
    final BuPanel pnDir= new BuPanel();
    tfDir_= new BuTextField();
    tfDir_.setColumns(8);
    pnDir.add(tfDir_);
    btDir_= new BuButton("Parcourir");
    btDir_.setActionCommand("PARCOURIR");
    btDir_.addActionListener(this);
    pnDir.add(btDir_);
    pnGen_.add(new BuLabel("Exporter vers :"), n++);
    pnGen_.add(pnDir, n++);
    pnOk_= new BuPanel();
    b_ok_= new BuButton("Valider");
    b_ok_.setActionCommand("VALIDER");
    b_ok_.addActionListener(this);
    b_cancel_= new BuButton("Annuler");
    b_cancel_.setActionCommand("ANNULER");
    b_cancel_.addActionListener(this);
    pnOk_.add(b_ok_);
    pnOk_.add(b_cancel_);
    final Container content= getContentPane();
    content.setLayout(new BorderLayout());
    content.add(pnGen_, BorderLayout.NORTH);
    content.add(pnOk_, BorderLayout.SOUTH);
    pack();
    setResizable(false);
      setLocationRelativeTo(_parent);
  }
  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    final String action= _evt.getActionCommand();
    final Object src= _evt.getSource();
    if (src == cbAll_) {
      final Enumeration e= cbs_.elements();
      if (cbAll_.isSelected()) {
        while (e.hasMoreElements()) {
          final JCheckBox cb= (JCheckBox)e.nextElement();
          if (cb.isEnabled()) {
            cb.setSelected(true);
          }
        }
      } else {
        while (e.hasMoreElements()) {
          final JCheckBox cb= (JCheckBox)e.nextElement();
          cb.setSelected(false);
        }
      }
    } else if (action.equals("ANNULER")) {
      dir_= new File(System.getProperty("user.dir"));
      for (int i= 0; i < choix_.length; i++) {
        choix_[i]= null;
      }
      status_= CANCEL;
      dispose();
    } else if (action.equals("VALIDER")) {
      final Enumeration e= cbs_.keys();
      String key= null;
      int i= 0;
      while (e.hasMoreElements()) {
        key= (String)e.nextElement();
        if (((JCheckBox)cbs_.get(key)).isSelected()) {
          choix_[i++]= key;
        } else {
          choix_[i++]= null;
        }
      }
      String txt= tfDir_.getText();
      if ((txt == null) || ("".equals(txt))) {
        dir_= null;
        new BuDialogError(
          (BuCommonInterface)LidoApplication.FRAME,
          LidoImplementation.informationsSoftware(),
          "Vous devez choisir un nom de fichier!")
          .activate();
        return;
      }
      final int indSlash= txt.lastIndexOf(File.separator);
      final int ind= txt.lastIndexOf('.');
      if ((ind > 0) && (ind > indSlash)) {
        txt= txt.substring(0, ind);
      }
      dir_= new File(txt);
      final File parentf= dir_.getParentFile();
      if ((parentf != null) && (!parentf.canWrite())) {
        dir_= null;
        new BuDialogError(
          (BuCommonInterface)LidoApplication.FRAME,
          LidoImplementation.informationsSoftware(),
          "Le r�pertoire " + parentf + "\n" + "n'existe pas!")
          .activate();
        return;
      }
      status_= OK;
      dispose();
    } else if (action.equals("PARCOURIR")) {
      final File file= LidoExport.chooseFile("cal");
      tfDir_.setText(file == null ? "" : file.getPath());
    }
  }
  public String[] getChoix() {
    return choix_;
  }
  public File getFichier() {
    return dir_;
  }
  public int valeurRetour() {
    return status_;
  }
  // Window events
  public void windowActivated(final WindowEvent e) {}
  public void windowClosed(final WindowEvent e) {}
  public void windowClosing(final WindowEvent e) {
    dir_= new File(System.getProperty("user.dir"));
    for (int i= 0; i < choix_.length; i++) {
      choix_[i]= null;
    }
    dispose();
  }
  public void windowDeactivated(final WindowEvent e) {}
  public void windowDeiconified(final WindowEvent e) {}
  public void windowIconified(final WindowEvent e) {}
  public void windowOpened(final WindowEvent e) {}
}
