/*
 * @file         LidoFilleReseau.java
 * @creation     1999-04-08
 * @modification $Date: 2007-01-19 13:14:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Hashtable;

import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;

import com.memoire.bu.BuColorIcon;
import com.memoire.bu.BuCommonInterface;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresBiefNoeudLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.controle.BSelecteurAncre;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPositionRelativeSegment;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.geometrie.VecteurGrContour;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.geometrie.VecteurGrPositionRelativeSegment;
import org.fudaa.ebli.geometrie.VecteurGrSegment;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TracePoint;

import org.fudaa.fudaa.commun.impl.FudaaPanelNavigation;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.editor.LidoBiefEditor;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoLimiteEditor;
import org.fudaa.fudaa.lido.editor.LidoNoeudEditor;
import org.fudaa.fudaa.lido.tableau.LidoTableauProfils;
/**
 * @version      $Revision: 1.16 $ $Date: 2007-01-19 13:14:29 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoFilleReseau
  extends EbliFilleCalques
  implements PropertyChangeListener, ListSelectionListener {
  static boolean debug= true;
  static int TAILLE_BIEFS= LidoResource.VISU_REZO.TAILLE_BIEFS;
  static final int TOUT= 0;
  static final int SELECTION= 1;
  SParametresRZO rzo_;
  SParametresPRO pro_;
  LidoTableauProfils proTable_;
  LidoFilleReseauUtils scindeur_;
  BGroupeCalque gcGal_;
  BGroupeCalque gcObjets_;
  BGroupeCalque gcEtiquettes_;
  BCalqueSegment cqBiefs_;
  BCalqueSegment cqLiaisons_;
  BCalquePositionRelativeSegment cqProfils_;
  BCalquePositionRelativeSegment cqProfilsSelection_;
  BCalquePoint cqNoeuds_;
  BCalqueTexteRelatifSegment cqEtiquettesBiefs_;
  BCalqueTexteRelatifSegment cqEtiquettesExtremites_;
  BCalqueTexteRelatifSegment cqEtiquettesCondlim_;
  BCalqueTexteRelatifSegment cqEtiquettesProfils_;
  BCalqueLegende cqLegende_;
  BCalqueDeplacementInteraction cqDeplacementI_;
  //private boolean existeNoeudLido;
  private Noeud graphe_;
  // correspondances Calque <-> Objet "metier"
  private Hashtable GImap_;
  private Hashtable IGmap_;
  // correspondances Objet "metier" <-> structure parametre
  private Hashtable ISmap_;
  private Hashtable SImap_;
  // table de sauvegarde/restauration des donnees
  private Hashtable bck_;
  // selection
  VecteurGrContour selection_;
  FudaaProjet p_;
  public LidoFilleReseau(
    final BArbreCalque _arbre,
    final BuCommonInterface _app,
    final FudaaProjet _p) {
    super(null, _app);
    addInternalFrameListener(_arbre);
    p_= _p;
    setBackground(java.awt.Color.white);
//    existeNoeudLido= false;
    rzo_= null;
    pro_= null;
    proTable_= null;
    bck_= new Hashtable();
    selection_= new VecteurGrContour();
    scindeur_= LidoFilleReseauUtils.SCINDEUR;
    addPropertyChangeListener(scindeur_);
    gcGal_= new BGroupeCalque();
    gcGal_.setName("cqRESEAU");
    gcGal_.setTitle("R�seau");
    gcObjets_= new BGroupeCalque();
    gcObjets_.setName("cqOBJETS");
    gcObjets_.setTitle("Objets");
    gcEtiquettes_= new BGroupeCalque();
    gcEtiquettes_.setName("cqETIQUETTES");
    gcEtiquettes_.setTitle("Etiquettes");
    cqLegende_= new BCalqueLegende();
    cqLegende_.setName("cqLEGENDE");
    cqLegende_.setTitle("L�gende");
    cqLegende_.setBackground(new java.awt.Color(255, 255, 224));
    cqLegende_.setForeground(java.awt.Color.blue);
    cqLegende_.setAncre(BSelecteurAncre.UP_LEFT);
    cqLegende_.ajoute(null, new BuColorIcon(java.awt.Color.blue), "Extr�mit�s");
    cqLegende_.ajoute(null, new BuColorIcon(java.awt.Color.red), "Biefs");
    cqLegende_.ajoute(null, new BuColorIcon(java.awt.Color.magenta), "Noeuds");
    cqLegende_.ajoute(null, new BuColorIcon(java.awt.Color.green), "Profils");
    cqDeplacementI_= new BCalqueDeplacementInteraction();
    cqDeplacementI_.setTitle("Deplacement I");
    cqDeplacementI_.setName("cqDEPLACEMENT");
    // B.M. 18/09/2001 Le calque n'envoit plus d'evenement SelectionEvent.
    //    cqDeplacementI_.addSelectionListener(this);
    cqDeplacementI_.setGele(true);
    cqBiefs_= new BCalqueSegment();
    cqBiefs_.setName("cqBIEFS");
    cqBiefs_.setTitle("Biefs");
    cqLiaisons_= new BCalqueSegment();
    cqLiaisons_.setName("cqLIAISONS");
    cqLiaisons_.setTitle("Liaisons");
    cqLiaisons_.setTypeTrait(TraceLigne.TIRETE);
    cqProfils_= new BCalquePositionRelativeSegment();
    cqProfils_.setName("cqPROFILS");
    cqProfils_.setTitle("Profils");
    cqProfils_.setForeground(java.awt.Color.green);
    cqProfils_.setTypePoint(TracePoint.PLUS);
    cqProfils_.setContextuelDelegator(new DefaultContextuelDelegator() {
      public JPopupMenu getCmdsContextuelles() {
        if (selection_.nombre() == 0) {
          return construitMenu(
            new String[] { "Afficher tout", "AFFICHERTOUTPROFILS", });
        }
        return construitMenu(
          new String[] {
            "Afficher s�lection",
            "AFFICHERSELECTIONPROFILS",
            "_CB_" + (cqDeplacementI_.isGele() ? "OFF_" : "ON_") + "D�placer",
            "DEPLACER" });
      }
    });
    //cqProfils_.setCmdsContextuelles(new String[] {
    //    "Action1", "ACTION1",
    //    "Action2", "ACTION2",
    //    "Action3", "ACTION3"
    //});
    cqProfilsSelection_= new BCalquePositionRelativeSegment();
    cqProfilsSelection_.setName("cqPROFILSSELECTION");
    cqProfilsSelection_.setTitle("Profils selectionn�s");
    cqProfilsSelection_.setForeground(java.awt.Color.red);
    cqProfilsSelection_.setTypePoint(TracePoint.PLUS);
    cqNoeuds_= new BCalquePoint();
    cqNoeuds_.setName("cqNOEUDS");
    cqNoeuds_.setTitle("Noeuds");
    cqNoeuds_.setForeground(java.awt.Color.magenta);
    cqNoeuds_.setTypePoint(TracePoint.DISQUE);
    cqNoeuds_.setTaillePoint(3);
    gcObjets_.add(cqProfilsSelection_);
    gcObjets_.add(cqNoeuds_);
    gcObjets_.add(cqBiefs_);
    gcObjets_.add(cqLiaisons_);
    gcObjets_.add(cqProfils_);
    cqEtiquettesBiefs_= new BCalqueTexteRelatifSegment();
    cqEtiquettesBiefs_.setName("cqBIEFS");
    cqEtiquettesBiefs_.setTitle("Biefs");
    cqEtiquettesBiefs_.setForeground(java.awt.Color.red);
    cqEtiquettesBiefs_.setBackground(getBackground());
    cqEtiquettesBiefs_.setEtiquettePleine(true);
    cqEtiquettesBiefs_.setSurPosition(true);
    cqEtiquettesExtremites_= new BCalqueTexteRelatifSegment();
    cqEtiquettesExtremites_.setName("cqEXTREMITES");
    cqEtiquettesExtremites_.setTitle("Extremit�s");
    cqEtiquettesExtremites_.setForeground(java.awt.Color.blue);
    cqEtiquettesExtremites_.setSurPosition(true);
    cqEtiquettesCondlim_= new BCalqueTexteRelatifSegment();
    cqEtiquettesCondlim_.setName("cqCONDLIM");
    cqEtiquettesCondlim_.setTitle("Cond. lim.");
    cqEtiquettesCondlim_.setBackground(getBackground());
    cqEtiquettesCondlim_.setEtiquettePleine(true);
    cqEtiquettesCondlim_.setSurPosition(true);
    cqEtiquettesProfils_= new BCalqueTexteRelatifSegment();
    cqEtiquettesProfils_.setVisible(false);
    cqEtiquettesProfils_.setName("cqPROFILS");
    cqEtiquettesProfils_.setTitle("Profils");
    cqEtiquettesProfils_.setSurPosition(true);
    gcEtiquettes_.add(cqEtiquettesBiefs_);
    gcEtiquettes_.add(cqEtiquettesExtremites_);
    gcEtiquettes_.add(cqEtiquettesCondlim_);
    gcEtiquettes_.add(cqEtiquettesProfils_);
    gcGal_.add(cqLegende_);
    gcGal_.add(cqDeplacementI_);
    gcGal_.add(gcEtiquettes_);
    gcGal_.add(gcObjets_);
    setCalque(gcGal_);
    initialize();
  }
  public void reinitialise() {
    try {
      if (rzo_ != null) {
        construitReseau();
      }
      if (pro_ != null) {
        construitProfils();
      }
    } catch (final LidoReseauException e) {
      System.err.println(e);
    }
  }
  public void setParametresRZO(final SParametresRZO _rzo) {
    if (rzo_ == _rzo) {
      return;
    }
    final SParametresRZO vp= rzo_;
    rzo_= _rzo;
    if ((rzo_ != null)) {
      try {
        initialize();
        construitReseau();
        if (pro_ != null) {
          construitProfils();
        }
      } catch (final LidoReseauException e) {
        System.err.println(e);
      }
      repaint();
    }
    firePropertyChange("parametresRZO", vp, rzo_);
  }
  public void setParametresPRO(final SParametresPRO _pro) {
    if (pro_ == _pro) {
      return;
    }
    final SParametresPRO vp= pro_;
    pro_= _pro;
    if ((rzo_ != null) && (pro_ != null)) {
      try {
        construitProfils();
      } catch (final LidoReseauException e) {
        System.err.println(e);
      }
      repaint();
    }
    firePropertyChange("parametresPRO", vp, pro_);
  }
  public SParametresRZO getParametresRZO() {
    return rzo_;
  }
  public SParametresPRO getParametresPRO() {
    return pro_;
  }
  // arbre
  public void valueChanged(final TreeSelectionEvent _evt) {
    // vidage de la selection
    videSelection();
    super.valueChanged(_evt);
  }
  // menus contextuels
  public void actionPerformed(final ActionEvent e) {
    super.actionPerformed(e);
    final String cmd= e.getActionCommand();
    if ("AFFICHERTOUTPROFILS".equals(cmd)) {
      cmdAfficherProfils(TOUT);
    } else if ("AFFICHERSELECTIONPROFILS".equals(cmd)) {
      cmdAfficherProfils(SELECTION);
    } else if ("DEPLACER".equals(cmd)) {
      cmdDeplacer();
    } else if ("SUPPRIMER".equals(cmd)) {
      cmdSupprimerProfils();
    }
  }
  // actions
  private void cmdAfficherProfils(final int _mode) {
    proTable_= new LidoTableauProfils();
    if (_mode == TOUT) {
      proTable_.setObjects(pro_.profilsBief);
    } else if (_mode == SELECTION) {
      final Object[] sel= new Object[selection_.nombre()];
      for (int i= 0; i < sel.length; i++) {
        sel[i]= GImap_.get(selection_.renvoie(i));
      }
      final SParametresBiefBlocPRO[] pros= new SParametresBiefBlocPRO[sel.length];
      for (int i= 0; i < sel.length; i++) {
        pros[i]= (SParametresBiefBlocPRO)ISmap_.get(sel[i]);
      }
      proTable_.setObjects(pros);
    }
    proTable_.getSelectionModel().addListSelectionListener(this);
    final LidoDialogTableau dlProfils=
      new LidoDialogTableau(proTable_, "Profils", p_);
    dlProfils.setNavPanel(FudaaPanelNavigation.FERMER);
    dlProfils.setActionPanel(EbliPreferences.DIALOG.SUPPRIMER);
    dlProfils.addActionListener(this);
    dlProfils.activate();
    proTable_= null;
  }
  private void cmdSupprimerProfils() {
    final SParametresBiefBlocPRO[] profils=
      (SParametresBiefBlocPRO[])proTable_.getSelectedObjects();
    final SParametresBiefBlocPRO[] tableProfils=
      (SParametresBiefBlocPRO[])proTable_.getObjects();
    final SParametresBiefBlocPRO[] newTableProfils=
      new SParametresBiefBlocPRO[tableProfils.length - profils.length];
    //for(int i=0; i<profils.length; i++) {
    //  supprimeProfil(profils[i]);
    //  System.err.print(profils[i].indice+", ");
    //}
    final SParametresBiefBlocPRO[] newProfils=
      new SParametresBiefBlocPRO[pro_.profilsBief.length - profils.length];
    int n= 0;
    for (int p= 0; p < pro_.profilsBief.length; p++) {
      for (int i= 0; i < profils.length; i++) {
        if (pro_.profilsBief[p].indice != profils[i].indice) {
          newProfils[n++]= pro_.profilsBief[p];
        } else if (debug) {
          System.err.print(pro_.profilsBief[p].indice + CtuluLibString.VIR);
        }
      }
    }
    n= 0;
    for (int p= 0; p < tableProfils.length; p++) {
      for (int i= 0; i < profils.length; i++) {
        if (tableProfils[p].indice != profils[i].indice) {
          newTableProfils[n++]= tableProfils[p];
        }
      }
    }
    System.err.println();
    proTable_.setObjects(newTableProfils);
    proTable_.repaint();
    pro_.profilsBief= newProfils;
    backup();
    try {
      initialize();
      construitProfils();
      flush();
    } catch (final LidoReseauException e) {
      if (debug) {
        System.err.println(e);
      }
      restore();
    }
  }
  private void cmdDeplacer() {
    if (cqDeplacementI_.isGele()) {
      cqDeplacementI_.setGele(false);
    } else {
      cqDeplacementI_.setGele(true);
    }
  }
  // Selection
  public void selectedObjects(final SelectionEvent _evt) {
    final VecteurGrContour objets= _evt.getObjects();
    selection_= objets;
    //    for(int i=0; i<objets.nombre(); i++) {
    //      System.err.println("  "+objets.renvoie(i).toString());
    //    }
    // B.M. 18/09/2001 Le calque rafraichit directement la vueCalque en cas de
    // d�placement d'objet.
    //    cqDeplacementI_.addSelectionListener(this);
    //    if( _evt.getSource()==cqDeplacementI_ ) {
    //      getVueCalque().repaint(0);
    //      return;
    //    }
    cqDeplacementI_.setSelection(objets);
    /*
        if( objets.nombre()>0 ) {
          Object sel=GImap_.get(objets.renvoie(0));
          LidoCustomizer edit=null;
          if( sel instanceof Arete ) {
            edit=new LidoBiefEditor();
          } else
          if( sel instanceof Noeud ) {
            edit=new LidoNoeudEditor();
          } else
          if( sel instanceof LidoCondLim ) {
            edit=new LidoLimiteEditor();
          } else
          if( edit==null ) return;
          edit.addPropertyChangeListener(this);
          edit.setObject(ISmap_.get(sel));
          ((java.awt.Component)edit).show();
        }
    */
    if (objets.nombre() > 0) {
      final Object[] sel= new Object[objets.nombre()];
      for (int i= 0; i < sel.length; i++) {
        sel[i]= GImap_.get(objets.renvoie(i));
      }
      LidoCustomizer edit= null;
      if (sel[0] instanceof Arete) {
        edit= new LidoBiefEditor();
        edit.setObject(ISmap_.get(sel[0]));
      } else if (sel[0] instanceof Noeud) {
        edit= new LidoNoeudEditor();
        edit.setObject(ISmap_.get(sel[0]));
      } else if (sel[0] instanceof LidoCondLim) {
        edit= new LidoLimiteEditor();
        edit.setObject(ISmap_.get(sel[0]));
      } else        /*      if( sel[0] instanceof Profill ) {
                proTable_=new LidoTableauProfils();
                SParametresBiefBlocPRO[] pros=new SParametresBiefBlocPRO[sel.length];
                if( debug ) System.err.println("length="+sel.length);
                for(int i=0; i<sel.length; i++) {
                  if( debug ) System.err.println(i+", "+((Profill)sel[i]).id);
                  pros[i]=(SParametresBiefBlocPRO)ISmap_.get(sel[i]);
                  if( debug ) System.err.println((pros[i].indice+1));
                }
                proTable_.setProfils(pros);
                proTable_.getSelectionModel().addListSelectionListener(this);
                // Il n'y a pas de bief ici, mais uniquement des profils isoles
                // Il faut faire setProfils sur la table.
        //        SParametresBiefSituLigneRZO bief=(SParametresBiefSituLigneRZO)evt.getNewValue();
        //        int []intervalle=scindeur_.intervalleProfilsBief(bief);
        //        proTable_.restreintA(intervalle[0], intervalle[1]);
                LidoDialogTableau dlProfils=new LidoDialogTableau(proTable_);
                int reponse=dlProfils.activate();
                proTable_=null;
        //        int noProfScission;
        //        if( (reponse!=JOptionPane.OK_OPTION)||((noProfScission=proTable_.getSelectedRow())==-1) ) {
        //          restore();
        //          return;
        //        }
                //int noProfScission=Integer.parseInt(JOptionPane.showInputDialog("Profil scission"));
        //        noProfScission+=intervalle[0];
              } else*/
        if (edit == null) {
          return;
        }
      edit.addPropertyChangeListener(this);
      //((java.awt.Component)edit).show();
       ((java.awt.Component)edit).setVisible(true);
    }
  }
  public void valueChanged(final ListSelectionEvent evt) {
    //    ListSelectionModel model=((ListSelectionModel)evt.getSource());
    //    LidoTableauProfilsModel model2=(LidoTableauProfilsModel)proTable_.getModel();
    //    SParametresBiefBlocPRO profil=model2.getProfil(model.getLeadSelectionIndex());
    final SParametresBiefBlocPRO profil=
      (SParametresBiefBlocPRO)proTable_.getSelectedObject();
    if (debug) {
      System.err.println("CalqueProfilSelection: " + (profil.indice));
    }
    final Object tmp= SImap_.get(profil);
    final GrPositionRelativeSegment position=
      (GrPositionRelativeSegment)IGmap_.get(tmp);
    cqProfilsSelection_.ajoute(position);
    getVueCalque().repaint(0);
  }
  public void propertyChange(final PropertyChangeEvent evt) {
    final String prop= evt.getPropertyName();
    if (debug) {
      System.err.println("Property: " + prop);
    }
    if ("object".equals(prop)) {
      backup();
      final Object o= evt.getOldValue();
      final Object n= evt.getNewValue();
      if (o != n) { // une nouvelle structure parametre Lido a ete creee
        if (n instanceof SParametresBiefNoeudLigneRZO) { // un nouveau noeud
          scindeur_.insereNouveauNoeudLido((SParametresBiefNoeudLigneRZO)n);
        } else if (
          n instanceof SParametresBiefSituLigneRZO) { // un nouveau bief
          scindeur_.insereNouveauBiefLido((SParametresBiefSituLigneRZO)n);
        } else {
          System.err.println("LidoFilleReseau: nouvel objet inconnu : " + n);
        }
      }
    } else if ("scission".equals(prop)) {
      backup();
      final SParametresBiefSituLigneRZO bief=
        (SParametresBiefSituLigneRZO)evt.getNewValue();
      proTable_= new LidoTableauProfils();
      proTable_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      proTable_.setObjects(pro_.profilsBief);
      proTable_.getSelectionModel().addListSelectionListener(this);
      final int[] intervalle= scindeur_.intervalleProfilsBief(bief);
      proTable_.restreintA(intervalle[0], intervalle[1]);
      final LidoDialogTableau dlProfils=
        new LidoDialogTableau(proTable_, "Biefs", p_);
      final int reponse= dlProfils.activate();
      int noProfScission;
      if ((reponse != JOptionPane.OK_OPTION)
        || ((noProfScission= proTable_.getSelectedRow()) == -1)) {
        proTable_= null;
        restore();
        return;
      }
      proTable_= null;
      //int noProfScission=Integer.parseInt(JOptionPane.showInputDialog("Profil scission"));
      noProfScission += intervalle[0];
      if (debug) {
        System.err.println("Profil selectionne=" + (noProfScission + 1));
      }
      final String dbiefStr=
        (String)JOptionPane.showInputDialog(
          this,
          "DBIEF",
          "DBIEF",
          JOptionPane.QUESTION_MESSAGE,
          null,
          null,
          "10");
      double dbief;
      if (dbiefStr == null) {
        return;
      }
      dbief= new Double(dbiefStr).doubleValue();
      noProfScission--;
      // conversion en indice Java (Lido compte a partir de 1)
      try {
        scindeur_.scinde(bief, noProfScission, dbief);
      } catch (final LidoReseauException e) {
        if (debug) {
          System.err.println(e);
        }
        JOptionPane.showMessageDialog(this, e.getMessage());
        restore();
        return;
      }
    } else {
      return;
    }
    try {
      initialize();
      construitReseau();
      if (pro_ != null) {
        construitProfils();
      }
      flush();
    } catch (final LidoReseauException e) {
      if (debug) {
        System.err.println(e);
      }
      restore();
      if (!((LidoCustomizer)evt.getSource()).restore()) {
        System.err.println("LidoLidoCustomizer: could not restore data");
      }
    }
    getVueCalque().repaint(0);
  }
  private void construitReseau() throws LidoReseauException {
    // test de validite du reseau
    if ((rzo_.blocSitus == null)
      || (rzo_.blocSitus.ligneSitu == null)
      || (rzo_.blocSitus.ligneSitu.length == 0)) {
      throw new LidoReseauException("aucun reseau d�fini");
    }
    // test d'existence de noeuds lido
    if ((rzo_.blocNoeuds != null)
      && (rzo_.blocNoeuds.ligneNoeud != null)
      && (rzo_.blocNoeuds.ligneNoeud.length > 0)) {
      //existeNoeudLido= true;
      // Construction de la topologie du reseau
      if (debug) {
        System.err.println("\n*** Construction de la topologie du reseau");
      }
    }
    graphe_= construitTopologie();
    if (debug) {
      System.err.println("*** Fin de la construction");
    }
    // Placement du reseau et construction des calques
    // petite verif
    if (graphe_ == null) {
      throw new LidoReseauException("aucun noeud n'a pu etre construit");
    }
    // on parcours le reseau recursivement Noeud par Noeud
    if (debug) {
      System.err.println("\n*** Placement du reseau");
    }
    placeNoeud(graphe_, "");
    if (debug) {
      System.err.println("*** Fin du placement");
    // association aux calques de selection
    //cqSelEtiquettesBiefs_.add(cqEtiquettesBiefs_);
    }
  }
  private void construitProfils() throws LidoReseauException {
    if ((pro_.donneesBief == null) || (pro_.donneesBief.length == 0)) {
      throw new LidoReseauException("pas de donnees de profils pour les biefs");
    }
    if ((pro_.profilsBief == null) || (pro_.profilsBief.length == 0)) {
      throw new LidoReseauException("pas de profil defini");
    }
    // !!: je ne prends pas en compte les distances inter-biefs (DBIEF)
    //     ni ici, ni dans placeNoeud pour placer les extremites
    int premierProf= 0;
    double absPremierProfil= pro_.profilsBief[premierProf].abscisse;
    double absRelative= 0.;
    GrPositionRelativeSegment position= null;
    GrSegment origine= null;
    Arete arete= null;
    for (int b= 0; b < pro_.donneesBief.length; b++) {
      arete= (Arete)SImap_.get(rzo_.blocSitus.ligneSitu[b]);
      origine= new GrSegment(arete.e1.position, arete.e2.position);
      for (int p= premierProf;
        p < pro_.donneesBief[b].premierProfilAmont - 1;
        p++) {
        try {
          absRelative= pro_.profilsBief[p].abscisse - absPremierProfil;
        } catch (final ArrayIndexOutOfBoundsException ex) {
          throw new LidoReseauException(
            "le profil JDBIEF " + p + " n'existe pas dans les donnees PRO");
        }
        final double s= absRelative / arete.dist;
        //if( debug ) System.err.print("p="+p+",s="+s+" | ");
        position= new GrPositionRelativeSegment();
        position.origine_= origine;
        position.position_= s;
        //if( debug ) System.err.println(position);
        final Profill profil= new Profill(p + 1);
        profil.positionRel= position;
        profil.arete= arete;
        cqProfils_.ajoute(position);
        cqEtiquettesProfils_.ajoute(position, "" + profil.id);
        // correspondances Calque<->Profill<->Lido
        GImap_.put(position, profil);
        IGmap_.put(profil, position);
        ISmap_.put(profil, pro_.profilsBief[p]);
        SImap_.put(pro_.profilsBief[p], profil);
      }
      // reinitialisation pour le bief suivant
      if (b < pro_.donneesBief.length - 1) {
        premierProf= pro_.donneesBief[b].premierProfilAmont - 1;
        absPremierProfil= pro_.profilsBief[premierProf].abscisse;
      }
      //if( debug ) System.err.println("\n");
    }
  }
  // ALGO DE CONSTRUCTION: ITERATIF
  private Noeud construitTopologie() throws LidoReseauException {
    final SParametresBiefSituLigneRZO[] biefs= rzo_.blocSitus.ligneSitu;
    int idNoeud= 0;
    int idArete= 0;
    final Hashtable extrTraitees= new Hashtable();
    Noeud res= null;
    Noeud noeud= null;
    //Vector noeuds=new Vector();
    Arete arete= null;
    Extremite extremite= null;
    // s'il existe des noeuds Lido:
    if ((rzo_.blocNoeuds != null)
      && (rzo_.blocNoeuds.ligneNoeud != null)
      && (rzo_.blocNoeuds.ligneNoeud.length > 0)) {
      // on cree des Noeuds qui correspondent aux noeuds Lido
      if (debug) {
        System.err.println("- noeuds Lido");
      }
      for (int n= 0; n < rzo_.blocNoeuds.ligneNoeud.length; n++) {
        final int[] extr= rzo_.blocNoeuds.ligneNoeud[n].noeud;
        noeud= new Noeud(n + 1);
        idNoeud++;
        //noeud=new Noeud(++idNoeud);
        // correspondance Noeud <-> SParam
        ISmap_.put(noeud, rzo_.blocNoeuds.ligneNoeud[n]);
        SImap_.put(rzo_.blocNoeuds.ligneNoeud[n], noeud);
        if (debug) {
          System.err.println("Noeud " + noeud.id);
        }
        // conversion des extremites Lido en Extremites et ajout au Noeud
        for (int i= 0; i < extr.length; i++) {
          // le tableau des extr du noeud lido est termine (voir fichier .rzo)
          if (extr[i] == 0) {
            break;
          }
          extremite= new Extremite(extr[i]);
          // correspondance Extr <-> SParam
          ISmap_.put(extremite, new Integer(extr[i]));
          SImap_.put(new Integer(extr[i]), extremite);
          if (debug) {
            System.err.println("  Extremite " + extremite.id);
          }
          // - ajout de l'Extremite a la table des Extr traitees
          extrTraitees.put("" + extremite.id, extremite);
          // - initialisation de l'arete
          arete= null;
          // - recherche du bief auquel l'Extremite appartient
          for (int b= 0; b < biefs.length; b++) {
            Extremite extrOpp= null;
            // si elle est une branch1
            if (biefs[b].branch1 == extr[i]) {
              // on recherche l'Extr associee
              extrOpp= (Extremite)extrTraitees.get("" + biefs[b].branch2);
            } else              // si elle est une branch2
              if (biefs[b].branch2 == extr[i]) {
                // on recherche l'Extr associee
                extrOpp= (Extremite)extrTraitees.get("" + biefs[b].branch1);
              } else {
                // on passe au bief suivant
                continue;
              }
            // si l'Extremite opposee n'existe pas encore
            if (extrOpp == null) {
              // construction d'une arete avec extr
              arete= new Arete(b + 1);
              idArete++;
              //arete=new Arete(++idArete);
              // correspondance Arete <-> SParam
              ISmap_.put(arete, biefs[b]);
              SImap_.put(biefs[b], arete);
              // association arete/extremite
              arete.ajouteExtremite(extremite, 1);
              arete.dist= Math.abs(biefs[b].x2 - biefs[b].x1);
            } else {
              // sinon : on recupere son arete
              arete= extrOpp.arete;
              // et on la complete de cette 2eme Extremite
              // (en testant si c'est une branch1 ou 2 pour la position)
              arete.ajouteExtremite(
                extremite,
                (biefs[b].branch1 == extr[i]) ? 1 : 2);
            }
          }
          // si aucun bief n'a ete trouve -> erreur
          if (arete == null) {
            // erreur dans le fichier RZO: l'extremite ne correspond a aucun bief
            throw new LidoReseauException(
              "l'extremite "
                + extremite.id
                + " du noeud "
                + noeud.id
                + " ne correspond � aucun bief");
          }
          if (debug) {
            System.err.println("    Arete " + arete.id);
          }
          // - ajout au Noeud
          noeud.ajouteExtremite(extremite);
        }
        // ajout du Noeud a la liste
        //noeuds.addElement(noeud);
      }
    }
    // extremites libres (Noeuds "terminaux")
    if (debug) {
      System.err.println("- extremites libres");
    }
    arete= null;
    noeud= null;
    extremite= null;
    for (int b= 0; b < rzo_.blocSitus.ligneSitu.length; b++) {
      // on recupere le bief
      final SParametresBiefSituLigneRZO bief= biefs[b];
      // si l'extr 1 n'a pas deja ete traitee (dans les noeuds Lido):
      if (!extrTraitees.containsKey("" + bief.branch1)) {
        // on construit l'Extremite 1
        extremite= new Extremite(bief.branch1);
        // correspondance Extr <-> SParam
        ISmap_.put(extremite, new Integer(bief.branch1));
        SImap_.put(new Integer(bief.branch1), extremite);
        if (debug) {
          System.err.println("Extremite1 " + extremite.id);
        }
        // construction d'un Noeud avec extremite
        noeud= new Noeud(++idNoeud);
        // correspondance Noeud <-> SParam
        // noeud a un seul element => pas d'equivalent S
        // on sauve le Noeud1 du premier bief
        if (b == 0) {
          res= noeud;
        }
        noeud.ajouteExtremite(extremite);
        // on ajoute l'Extremite a la liste des Extr traitees
        extrTraitees.put("" + extremite.id, extremite);
        // on recherche l'Extr associee
        final Extremite extrOpp= (Extremite)extrTraitees.get("" + bief.branch2);
        // si elle n'existe pas
        if (extrOpp == null) {
          // construction d'une arete avec extremite
          arete= new Arete(b + 1);
          idArete++;
          // correspondance Arete <-> SParam
          ISmap_.put(arete, bief);
          SImap_.put(bief, arete);
          // association arete/extremite
          arete.ajouteExtremite(extremite, 1);
          arete.dist= Math.abs(bief.x2 - bief.x1);
        } else {
          // on recupere son arete
          arete= extrOpp.arete;
          // et on la complete de cette 2eme Extremite
          arete.ajouteExtremite(extremite, 1);
        }
        if (debug) {
          System.err.println("  Noeud " + noeud.id);
        }
      }
      // on fait tout pareil pour l'extr 2:
      // si l'extr 2 n'a pas deja ete traitee (dans les noeuds Lido):
      if (!extrTraitees.containsKey("" + bief.branch2)) {
        // on construit l'Extremite 2
        extremite= new Extremite(bief.branch2);
        // correspondance Extr <-> SParam
        ISmap_.put(extremite, new Integer(bief.branch2));
        SImap_.put(new Integer(bief.branch2), extremite);
        if (debug) {
          System.err.println("Extremite2 " + extremite.id);
        }
        // construction d'un Noeud avec extremite
        noeud= new Noeud(++idNoeud);
        // correspondance Noeud <-> SParam
        // noeud a une extr => pas d'equivalent S
        noeud.ajouteExtremite(extremite);
        // on recherche l'Extr associee
        final Extremite extrOpp= (Extremite)extrTraitees.get("" + bief.branch1);
        // si elle n'existe pas
        if (extrOpp == null) {
          // construction d'une arete avec extremite
          arete= new Arete(b + 1);
          idArete++;
          // correspondance Arete <-> SParam
          ISmap_.put(arete, bief);
          SImap_.put(bief, arete);
          // association arete/extremite
          arete.ajouteExtremite(extremite, 2);
          arete.dist= Math.abs(bief.x2 - bief.x1);
        } else {
          // sinon :
          // on recupere son arete
          arete= extrOpp.arete;
          // et on la complete de cette 2eme Extremite
          arete.ajouteExtremite(extremite, 2);
        }
        if (debug) {
          System.err.println("  Noeud " + noeud.id);
        }
      }
    }
    return res;
  }
  // ALGO DE PLACEMENT: RECURSIF EN PROFONDEUR D'ABORD
  private void placeNoeud(final Noeud noeud, final String indent)
    throws LidoReseauException {
    // on marque le Noeud comme traite (pour eviter de repasser dessus)
    noeud.traite= true;
    // on ajoute ce Noeud au calque des Noeuds
    cqNoeuds_.ajoute(noeud.position);
    // correspondance etiquetteExtr <-> Extr
    GImap_.put(noeud.position, noeud);
    IGmap_.put(noeud, noeud.position);
    if (debug) {
      System.err.println(indent + "|Noeud " + noeud.id);
    }
    if (debug) {
      System.err.println(indent + "|  position : " + noeud.position);
    }
    // calcul de la repartition d'angle
    double angle= 2 * Math.PI / noeud.extr.length;
    if (noeud.extr.length == 1) {
      angle= Math.PI;
    }
    if (debug) {
      System.err.println(indent + "|  angle : " + angle * 180 / Math.PI);
    }
    int nb= 0;
    // on parcourt les extremites de ce Noeud et on les place
    for (int i= 0; i < noeud.extr.length; i++) {
      // position de l'Extr i=position du noeud pour l'instant
      noeud.extr[i].position= noeud.position;
    }
    // on reparcourt les extremites de ce Noeud et on place recursivement les Noeuds
    for (int i= 0; i < noeud.extr.length; i++) {
      if (debug) {
        System.err.println(indent + "|  Extremite " + noeud.extr[i].id);
      }
      //if( debug ) System.err.println(indent+"|    Noeud : "+noeud.extr[i].noeud.id);
      //if( debug ) System.err.println(indent+"|    position : "+noeud.extr[i].position);
      // on va chercher le noeud auquel appartient l'extremite opposee a Extr i
      // sur son arete
      if (noeud.extr[i].arete == null) {
        throw new LidoReseauException(
          "l'extremite " + noeud.extr[i].id + " n'a pas d'arete");
      }
      if (noeud.extr[i].arete.autreExtremite(noeud.extr[i]) == null) {
        throw new LidoReseauException(
          "l'arete "
            + noeud.extr[i].arete.id
            + " n'a qu'une extremite: "
            + noeud.extr[i].id);
      }
      final Noeud noeudOppose=
        noeud.extr[i].arete.autreExtremite(noeud.extr[i]).noeud;
      // si le Noeud oppose n'a pas ete traite:
      if (!noeudOppose.traite) {
        // on calcule sa position (on ajoute l'arete et l'angle)
        ++nb;
        //        noeudOppose.position.x=noeud.position.x+
        //                               noeud.extr[i].arete.dist*Math.cos(Math.PI-(nb)*angle);
        //        noeudOppose.position.y=noeud.position.y+
        //                               noeud.extr[i].arete.dist*Math.sin(Math.PI-(nb)*angle);
        noeudOppose.position.x_=
          noeud.position.x_ + TAILLE_BIEFS * Math.cos(Math.PI - (nb) * angle);
        noeudOppose.position.y_=
          noeud.position.y_ + TAILLE_BIEFS * Math.sin(Math.PI - (nb) * angle);
        if (debug) {
          System.err.println(
            indent
              + "|    Noeud oppose : "
              + noeudOppose.id
              + " (nb="
              + nb
              + ", angle="
              + (180 - (nb) * angle * 180 / Math.PI)
              + ")");
        }
        //if( debug ) System.err.println(indent+"|    Arete : "+noeud.extr[i].arete.id+" (traitee="+noeud.extr[i].arete.traitee+")");
        //if( debug ) System.err.println(indent+"|      dist : "+noeud.extr[i].arete.dist);
        // on le place
        placeNoeud(noeudOppose, indent + "  ");
      }
      // si l'arete a deja ete traitee (dans le calque de biefs)
      if (noeud.extr[i].arete.traitee) {
        // on ajoute un vecteur de liaison entre le Noeud et l'Extr i
        //if( debug ) System.err.println(indent+"#    liaison Noeud : "+noeud.id+" -> Extremite : "+noeud.extr[i].arete.autreExtremite(noeud.extr[i]).id);
        //        GrSegment s=new GrSegment();
        //        s.o=noeud.position;
        //        s.e=noeud.extr[i].arete.autreExtremite(noeud.extr[i]).position;
        //        cqLiaisons_.ajoute(s);
      } else {
        // sinon: on introduit l'arete dans le calque des biefs
        noeud.extr[i].arete.traitee= true;
        if (debug) {
          System.err.println(
            indent
              + "#    dessin Arete : "
              + noeud.extr[i].arete.id
              + " (Extr "
              + noeud.extr[i].arete.e1.id
              + "->Extr "
              + noeud.extr[i].arete.e2.id
              + ")");
        }
        final GrSegment s= new GrSegment();
        s.o_= noeud.extr[i].arete.e1.position;
        s.e_= noeud.extr[i].arete.e2.position;
        cqBiefs_.ajoute(s);
        // correspondance positionBief <-> Arete
        GImap_.put(s, noeud.extr[i].arete);
        IGmap_.put(noeud.extr[i].arete, s);
        // etiquette de l'arete
        GrPositionRelativeSegment etiq= new GrPositionRelativeSegment();
        etiq.origine_= s;
        etiq.position_= 0.5;
        cqEtiquettesBiefs_.ajoute(etiq, "" + noeud.extr[i].arete.id);
        // correspondance etiquetteBief <-> Arete
        GImap_.put(etiq, noeud.extr[i].arete);
        IGmap_.put(noeud.extr[i].arete, etiq);
        // etiquettes extremites
        etiq= new GrPositionRelativeSegment();
        etiq.origine_= s;
        etiq.position_= 0.1;
        cqEtiquettesExtremites_.ajoute(etiq, "" + noeud.extr[i].arete.e1.id);
        etiq= new GrPositionRelativeSegment();
        etiq.origine_= s;
        etiq.position_= 0.9;
        cqEtiquettesExtremites_.ajoute(etiq, "" + noeud.extr[i].arete.e2.id);
      }
    }
    // Conditions Limites :
    // si c'est un noeud a extremite libre
    if (noeud.extr.length == 1) {
      // on cherche la CL associee au Noeud
      final Integer libre= ((Integer)ISmap_.get(noeud.extr[0]));
      if (libre == null) {
        throw new LidoReseauException(
          "l'Extremite "
            + noeud.extr[0].id
            + " ne correspond a aucune extremite libre Lido");
      }
      if ((rzo_.blocLims == null) || (rzo_.blocLims.ligneLim == null)) {
        throw new LidoReseauException("il n'y a pas de definition des CL des extremites libres");
      }
      int indice= -1;
      for (int j= 0; j < rzo_.blocLims.ligneLim.length; j++) {
        if (libre.intValue() == rzo_.blocLims.ligneLim[j].numExtBief) {
          indice= j;
          break;
        }
      }
      if (indice == -1) {
        throw new LidoReseauException(
          "l'extremite libre " + libre.intValue() + " n'a pas de CL");
      }
      // on ajoute la CL au calque des etiquettes de CL
      //variables inutiles
      //double dirX=(noeud.extr[0].arete.e1.position.x-noeud.extr[0].arete.e2.position.x)/TAILLE_BIEFS;
      //double dirY=(noeud.extr[0].arete.e1.position.y-noeud.extr[0].arete.e2.position.y)/TAILLE_BIEFS;
      final GrSegment s= new GrSegment();
      s.o_= noeud.extr[0].arete.e1.position;
      s.e_= noeud.extr[0].arete.e2.position;
      final GrPositionRelativeSegment etiq= new GrPositionRelativeSegment();
      etiq.origine_= s;
      if (noeud.extr[0].arete.e1.estLibre()) {
        etiq.position_= -0.2;
      } else {
        etiq.position_= 1.2;
      }
      cqEtiquettesCondlim_.ajoute(
        etiq,
        "CL "
          + rzo_.blocLims.ligneLim[indice].numLoi
          + "\ntyp "
          + rzo_.blocLims.ligneLim[indice].typLoi);
      // correspondance position etiquette <-> LidoCondLim
      final LidoCondLim cl= new LidoCondLim();
      cl.numExtBief= rzo_.blocLims.ligneLim[indice].numExtBief;
      cl.numLoi= rzo_.blocLims.ligneLim[indice].numLoi;
      cl.typLoi= rzo_.blocLims.ligneLim[indice].typLoi;
      GImap_.put(etiq, cl);
      IGmap_.put(cl, etiq);
      // correspondance LidoCondLim <-> lui-meme dans la ISmap_ car l'editeur
      //                                traite directement LidoCondLim
      //   rmq: ce n'est pas tres beau. A terme, les editeurs devront tous
      //        s'abstraire des S et travailler avec les I
      ISmap_.put(cl, cl);
      SImap_.put(cl, cl);
    }
  }
  private void backup() {
    bck_.put(cqBiefs_.getName(), cqBiefs_.getSegments());
    bck_.put(cqLiaisons_.getName(), cqLiaisons_.getSegments());
    bck_.put(cqProfils_.getName(), cqProfils_.getRelatifsSegment());
    bck_.put(cqNoeuds_.getName(), cqNoeuds_.getPoints());
    bck_.put(
      cqEtiquettesBiefs_.getName() + "_pts",
      cqEtiquettesBiefs_.getPositionsRelatives());
    bck_.put(
      cqEtiquettesExtremites_.getName() + "_pts",
      cqEtiquettesExtremites_.getPositionsRelatives());
    bck_.put(
      cqEtiquettesCondlim_.getName() + "_pts",
      cqEtiquettesCondlim_.getPositionsRelatives());
    bck_.put(
      cqEtiquettesProfils_.getName() + "_pts",
      cqEtiquettesProfils_.getPositionsRelatives());
    bck_.put(
      cqEtiquettesBiefs_.getName() + "_txt",
      cqEtiquettesBiefs_.getTextes());
    bck_.put(
      cqEtiquettesExtremites_.getName() + "_txt",
      cqEtiquettesExtremites_.getTextes());
    bck_.put(
      cqEtiquettesCondlim_.getName() + "_txt",
      cqEtiquettesCondlim_.getTextes());
    bck_.put(
      cqEtiquettesProfils_.getName() + "_txt",
      cqEtiquettesProfils_.getTextes());
    bck_.put("graphe", graphe_);
    bck_.put("GImap", GImap_);
    bck_.put("IGmap", IGmap_);
    bck_.put("ISmap", ISmap_);
    bck_.put("SImap", SImap_);
    System.err.println("LidoFilleReseau: data backuped");
  }
  private void initialize() {
    cqBiefs_.reinitialise();
    cqLiaisons_.reinitialise();
    cqProfils_.reinitialise();
    cqProfilsSelection_.reinitialise();
    cqNoeuds_.reinitialise();
    cqEtiquettesBiefs_.reinitialise();
    cqEtiquettesExtremites_.reinitialise();
    cqEtiquettesCondlim_.reinitialise();
    cqEtiquettesProfils_.reinitialise();
    graphe_= null;
    GImap_= new Hashtable();
    IGmap_= new Hashtable();
    ISmap_= new Hashtable();
    SImap_= new Hashtable();
  }
  private void restore() {
    cqBiefs_.setSegments((VecteurGrSegment)bck_.get(cqBiefs_.getName()));
    cqLiaisons_.setSegments((VecteurGrSegment)bck_.get(cqLiaisons_.getName()));
    cqProfils_.setRelatifsSegment(
      (VecteurGrPositionRelativeSegment)bck_.get(cqProfils_.getName()));
    cqNoeuds_.setPoints((VecteurGrPoint)bck_.get(cqNoeuds_.getName()));
    cqEtiquettesBiefs_.setPositionsRelatives(
      (VecteurGrPositionRelativeSegment)bck_.get(
        cqEtiquettesBiefs_.getName() + "_pts"));
    cqEtiquettesBiefs_.setTextes(
      (String[])bck_.get(cqEtiquettesBiefs_.getName() + "_txt"));
    cqEtiquettesExtremites_.setPositionsRelatives(
      (VecteurGrPositionRelativeSegment)bck_.get(
        cqEtiquettesExtremites_.getName() + "_pts"));
    cqEtiquettesExtremites_.setTextes(
      (String[])bck_.get(cqEtiquettesExtremites_.getName() + "_txt"));
    cqEtiquettesCondlim_.setPositionsRelatives(
      (VecteurGrPositionRelativeSegment)bck_.get(
        cqEtiquettesCondlim_.getName() + "_pts"));
    cqEtiquettesCondlim_.setTextes(
      (String[])bck_.get(cqEtiquettesCondlim_.getName() + "_txt"));
    cqEtiquettesProfils_.setPositionsRelatives(
      (VecteurGrPositionRelativeSegment)bck_.get(
        cqEtiquettesProfils_.getName() + "_pts"));
    cqEtiquettesProfils_.setTextes(
      (String[])bck_.get(cqEtiquettesProfils_.getName() + "_txt"));
    graphe_= (Noeud)bck_.get("graphe");
    GImap_= (Hashtable)bck_.get("GImap");
    IGmap_= (Hashtable)bck_.get("IGmap");
    ISmap_= (Hashtable)bck_.get("ISmap");
    SImap_= (Hashtable)bck_.get("SImap");
    System.err.println("LidoFilleReseau: data restored");
    flush();
  }
  private void flush() {
    bck_= new Hashtable();
    System.gc();
    System.err.println("LidoFilleReseau: backup flushed");
  }
}
class Extremite {
  public GrPoint position;
  public int id;
  public Noeud noeud;
  public Arete arete;
  public Extremite(final int _id) {
    id= _id;
    noeud= null;
    arete= null;
    position= new GrPoint(0., 0., 0.);
  }
  public boolean estLibre() {
    if (noeud != null) {
      return (noeud.extr.length == 1);
    }
    return false;
  }
}
class Arete {
  public int id;
  public boolean traitee;
  public Extremite e1;
  public Extremite e2;
  public double dist;
  public Arete(final int _id) {
    id= _id;
    traitee= false;
    e1= e2= null;
    dist= 0.;
  }
  public void ajouteExtremite(final Extremite _e, final int pos)
    throws LidoReseauException {
    if ((e1 != null) && (e2 != null)) {
      throw new LidoReseauException(
        "l'arete " + id + " a deja deux extremites : " + e1.id + ", " + e2.id);
    }
    if (pos == 1) {
      if (e1 == null) {
        e1= _e;
      } else {
        e2= e1;
        e1= _e;
      }
    } else if (pos == 2) {
      if (e2 == null) {
        e2= _e;
      } else {
        e1= e2;
        e2= _e;
      }
    } else {
      throw new LidoReseauException("position " + pos + " impossible");
    }
    _e.arete= this;
  }
  public Extremite autreExtremite(final Extremite e) {
    return (((e1 != null) && (e.id == e1.id)) ? e2 : e1);
  }
}
class Noeud {
  public GrPoint position;
  public int id;
  public boolean traite;
  public Extremite[] extr;
  public Noeud(final int _id) {
    id= _id;
    traite= false;
    extr= new Extremite[0];
    position= new GrPoint(0., 0., 0.);
  }
  public void ajouteExtremite(final Extremite e) {
    final Extremite[] newextr= new Extremite[extr.length + 1];
    for (int i= 0; i < extr.length; i++) {
      newextr[i]= extr[i];
    }
    e.noeud= this;
    newextr[extr.length]= e;
    extr= newextr;
  }
}
class Profill {
  public GrPositionRelativeSegment positionRel;
  public int id;
  public Arete arete;
  public Profill(final int _id) {
    id= _id;
    positionRel= new GrPositionRelativeSegment();
    arete= null;
  }
}
// Classe "rustine" pour encapsuler les CL dans un objet. TEMPORAIRE
// NOUVEAU: Utiliser SParametresBiefLimBlocRZO dans LidoTableauLimites.java?
class LidoCondLim {
  public int numExtBief;
  public int numLoi;
  public int typLoi;
  public LidoCondLim() {
    numExtBief= 0;
    numLoi= 0;
    typLoi= 0;
  }
}
/* OLD: l'algo a un peu change
  - on construit des Noeuds "composes" d'extremites de biefs [ei]
    qui contiennent les distances de biefs associes [bi]
    - bief 1: on cree un Noeud pour e1 et ses associees eventuelles (impossible)
    - pour chaque bief > 1
      - si e2 a deja ete traitee on passe
      - on cree un nouveau Noeud ou on met e2 et toutes les
        extr associees a e2 par un noeud Lido. On met aussi les distances des
        biefs associes.
  - on cherche le plus "gros" Noeud N
  SUB:
  - on calcule l'angle de repartition
    des biefs (360/[ei].length)
  - pour chaque extremite ei
    - on calcule sa position (angle,dist (fixee pour l'instant))
    - on ajoute la distance du bief associe bi
    - on cherche le Noeud auquel appartient l'autre extr ei' du bief
    - on marque l'extremite ei' comme traitee
    - on procede de meme sur le Noeud N' auquel appartient ei'
      -> goto SUB
*/
/* ANCIEN ALGO DE CONSTRUCTION DE LA TOPOLOGIE (RECURSIF)
  private Noeud construitTopologie() throws LidoReseauException {
    SParametresBiefSituLigneRZO[] biefs=rzo_.blocSitus.ligneSitu;
    int idNoeud=0;
    int idArete=0;
    // on parcourt les biefs ( "de gauche a droite, sens de la riviere" )
    return construitNoeud(0, null, idNoeud, idArete);
  }
*/
/*
  private Noeud construitNoeud(int b, Noeud n1, int idNoeud, int idArete)
    throws LidoReseauException {
    SParametresBiefSituLigneRZO bief=rzo_.blocSitus.ligneSitu[b];

    // nouvelle Extremite (branch1) et nouvelle Arete
    Extremite e1=new Extremite(bief.branch1);
System.err.println("Extremite1 "+e1.id);
    // - premier Noeud
    if( n1==null)
      n1=new Noeud(++idNoeud);
System.err.println("  Noeud "+n1.id);
    // - l'Extremite s'ajoute au Noeud precedent
    n1.ajouteExtremite(e1);
    Arete a=new Arete(++idArete);
System.err.println("    Arete "+a.id);
    a.ajouteExtremite(e1);
    a.dist=Math.abs(bief.x2-bief.x1);

    // nouvelle Extremite (branch2), nouveau Noeud, meme Arete
    Extremite e2=new Extremite(bief.branch2);
System.err.println("Extremite2 "+e2.id);
    Noeud n2=new Noeud(++idNoeud);
System.err.println("  Noeud "+n2.id);
    n2.ajouteExtremite(e2);
System.err.println("    Arete "+a.id);
    a.ajouteExtremite(e2);
    // si e2 appartient a un noeud lido
    int[] noeudLido=null;
    if( existeNoeudLido ) noeudLido=noeudLido(e2.id);
    if( noeudLido!=null ) {
      // on parcourt les extr (sauf e2 bien sur)
      for(int i=0; i<noeudLido.length; i++) {
        if( noeudLido[i]!=e2.id ) {
          // on continue sur les biefs correspondants
          int indiceBief=indiceBief(noeudLido[i]);
          if( indiceBief==-1 )
            throw new LidoReseauException("l'extremite "+noeudLido[i]+" du noeud "+n2.id+" n'appartient � aucun bief");
          construitNoeud(indiceBief, n2, idNoeud, idArete);
        }
      }
    } else {
      // e2 n'appartient pas a un noeud lido:
      // on continue sur le bief suivant s'il y en a un
      if( (b+1)<rzo_.blocSitus.ligneSitu.length )
        construitNoeud(b+1, n2, idNoeud, idArete);
    }
    return n2;
  }

  private int[] noeudLido(int e) {
    for( int n=0; n<rzo_.blocNoeuds.ligneNoeud.length; n++ ) {
      for( int i=0; i<rzo_.blocNoeuds.ligneNoeud[n].noeud.length; i++ ) {
        if( rzo_.blocNoeuds.ligneNoeud[n].noeud[i]==e )
          return rzo_.blocNoeuds.ligneNoeud[n].noeud;
      }
    }
    return null;
  }

  private int indiceBief(int e) {
    for( int b=0; b<rzo_.blocSitus.ligneSitu.length; b++ ) {
      if( rzo_.blocSitus.ligneSitu[b].branch1==e )
        return b;
    }
    return -1;
  }
*/
/* ANCIEN ALGO DE CONSTRUCTION POUR LES NOEUDS SIMPLES
        // si c'est le premier bief, on cree un nouveau Noeud avec
        if( noeudPrec==null ) {
          Noeud n1=new Noeud(++idNoeud);
System.err.println("  Noeud "+n1.id);
          n1.ajouteExtremite(e1);
          // - ajout du Noeud a la liste
          poignee=n1;
          //noeuds.addElement(n1);
        } else {
        // sinon on l'ajoute au Noeud precedent
          noeudPrec.ajouteExtremite(e1);
System.err.println("  Noeud "+noeudPrec.id);
        }
        // nouvelle arete
        arete=new Arete(++idArete);
System.err.println("    Arete "+arete.id);
        arete.ajouteExtremite(e1);
        arete.dist=Math.abs(biefs[b].x2-biefs[b].x1);
      } else {
      // sinon: on recupere son arete pour mettre e2 dedans
        arete=((Extremite)extrTraitees.get(""+bief.branch1)).arete;
      }
      // si l'extr 2 n'a pas deja ete traitee (dans les noeuds Lido):
      if( !extrTraitees.containsKey(""+bief.branch2) ) {
        // on construit l'Extremite 2
        e2=new Extremite(bief.branch2);
System.err.println("Extremite2 "+e2.id);
        // on en fait un nouveau Noeud
        Noeud n2=new Noeud(++idNoeud);
System.err.println("  Noeud "+n2.id);
        n2.ajouteExtremite(e2);
        // - ajout du Noeud a la liste
        poignee=n2;
        //noeuds.addElement(n2);
        // ce Noeud est le noeudPrec pour le bief suivant
        noeudPrec=n2;
        // on ajoute cette Extr a l'arete et inversement
System.err.println("    Arete "+arete.id);
        arete.ajouteExtremite(e2);
      } else {
        // securite (mais il n'arrive jamais qu'une extr 1 non traitee s'"accroche"
        // a une extr 2 non consecutive, car si une extr 2 appartient a un noeud
        // Lido, alors, toutes les extr 1 des biefs suivants aussi).
        noeudPrec=null;
        // rien pour l'arete
      }
    }
*/
