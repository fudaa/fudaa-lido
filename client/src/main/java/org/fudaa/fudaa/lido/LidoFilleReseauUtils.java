/*
 * @file         LidoFilleReseauUtils.java
 * @creation     1999-04-08
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.fudaa.dodico.corba.lido.SParametresBiefNoeudLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSingLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresDonBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SParametresRZO;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
final class LidoFilleReseauUtils implements PropertyChangeListener {
  static boolean debug= true;
  public final static LidoFilleReseauUtils SCINDEUR= new LidoFilleReseauUtils();
  SParametresRZO rzo_;
  SParametresPRO pro_;
  private LidoFilleReseauUtils() {
    rzo_= null;
    pro_= null;
  }
  public void setParametresRZO(final SParametresRZO _rzo) {
    if (rzo_ == _rzo) {
      return;
    }
    rzo_= _rzo;
  }
  public void setParametresPRO(final SParametresPRO _pro) {
    if (pro_ == _pro) {
      return;
    }
    pro_= _pro;
  }
  public SParametresRZO getParametresRZO() {
    return rzo_;
  }
  public SParametresPRO getParametresPRO() {
    return pro_;
  }
  public void propertyChange(final PropertyChangeEvent evt) {
    final String prop= evt.getPropertyName();
    final Object n= evt.getNewValue();
    if ("parametresRZO".equals(prop)) {
      setParametresRZO((SParametresRZO)n);
    } else if ("parametresPRO".equals(prop)) {
      setParametresPRO((SParametresPRO)n);
    }
  }
  public void scinde(
    final SParametresBiefSituLigneRZO _bief,
    final int noProfilScission,
    final double _dbief)
    throws LidoReseauException {
    // remarque: nouveauBief est le demibief droit!
    // verification que le profil appartient bien au bief
    final int indiceDonBief= chercheDonBief(_bief);
    if (debug) {
      System.err.println("indiceDonBief=" + indiceDonBief);
    }
    if (((pro_.donneesBief[indiceDonBief].premierProfilAmont - 1)
      < noProfilScission)
      || ((indiceDonBief > 0)
        && ((pro_.donneesBief[indiceDonBief - 1].premierProfilAmont - 1)
          > noProfilScission))) {
      throw new LidoReseauException(
        "le profil "
          + (noProfilScission + 1)
          + " n'appartient pas au bief "
          + _bief.numBief);
    }
    int idExtrMax= rzo_.nbBief * 2; // (il y a deux extremites par bief!)
    // 1) Modification de RZO:
    // 1.A) nouveau Bief
    final SParametresBiefSituLigneRZO nouveauBief= new SParametresBiefSituLigneRZO();
    // nouveauBief.x2 est _bief.x2
    nouveauBief.x2= _bief.x2;
    // nouveauBief.x1 est l'ascisse de scission + dbief
    nouveauBief.x1= pro_.profilsBief[noProfilScission].abscisse + _dbief;
    // le numero de nouveauBief est nbBief+1 (on incremente en meme temps nbBief)
    nouveauBief.numBief= (++rzo_.nbBief);
    // nouveauBief.branch1 est une nouvelle extremite
    nouveauBief.branch1= ++idExtrMax;
    // nouveauBief.branch2 est l'extremite 2 de _bief
    nouveauBief.branch2= _bief.branch2;
    // ajout du nouveau bief (on recupere la position d'insertion)
    final int position= insereNouveauBiefLido(nouveauBief);
    // _bief.x1 reste inchange
    // _bief.x2 devient l'abscisse du profil de scission
    _bief.x2= pro_.profilsBief[noProfilScission].abscisse;
    // _bief.numBief inchange
    // _bief.branch1 inchange
    // _bief.branch2 est une nouvelle extremite
    _bief.branch2= ++idExtrMax;
    // 1.B) nouveau noeud
    final SParametresBiefNoeudLigneRZO nouveauNoeud=
      new SParametresBiefNoeudLigneRZO();
    // incrementation du nombre de noeuds
    rzo_.nbNoeud++;
    // ce noeud est compose des deux nouvelles extremites
    nouveauNoeud.noeud= new int[5];
    // attention: placer les extr de gauche a droite (sens des abscisses)
    nouveauNoeud.noeud[0]= _bief.branch2;
    nouveauNoeud.noeud[1]= nouveauBief.branch1;
    // ajout du nouveau noeud
    insereNouveauNoeudLido(nouveauNoeud);
    // 1.C) nouvelle singularite
    final SParametresBiefSingLigneRZO nouvelleSing= new SParametresBiefSingLigneRZO();
    // on met 0 et 0 (pas de singularite)
    nouvelleSing.xSing= 0.;
    nouvelleSing.nSing= 0;
    // ajout de la nouvelle singularite a la position correspondant au bief
    insereNouvelleSingLido(nouvelleSing, position);
    // 2) Modification de PRO:
    // nouveau bief PRO
    final SParametresDonBiefBlocPRO nouveauDonBief= new SParametresDonBiefBlocPRO();
    // premier profil=scission+1 (+1 encore car Lido PRO compte a partir de 1)
    nouveauDonBief.premierProfilAmont= noProfilScission + 2;
    // dbief
    nouveauDonBief.distancePremierDernierProfil= _dbief;
    // ajout du nouveau bief
    insereNouveauBiefLido(nouveauDonBief);
  }
  public int insereNouveauNoeudLido(final SParametresBiefNoeudLigneRZO n) {
    final SParametresBiefNoeudLigneRZO[] oldN= rzo_.blocNoeuds.ligneNoeud;
    final SParametresBiefNoeudLigneRZO[] newN=
      new SParametresBiefNoeudLigneRZO[oldN.length + 1];
    for (int i= 0; i < oldN.length; i++) {
      newN[i]= oldN[i];
    }
    newN[oldN.length]= n;
    rzo_.blocNoeuds.ligneNoeud= newN;
    return oldN.length;
  }
  public int insereNouveauBiefLido(final SParametresBiefSituLigneRZO n) {
    final SParametresBiefSituLigneRZO[] oldN= rzo_.blocSitus.ligneSitu;
    final SParametresBiefSituLigneRZO[] newN=
      new SParametresBiefSituLigneRZO[oldN.length + 1];
    // il faut placer le nouveau donBief correctement (par ordre croissant des abscisses)
    int res= -1;
    int i= 0;
    while ((i < oldN.length) && (n.x1 > oldN[i].x1)) {
      newN[i]= oldN[i++];
    }
    newN[i]= n;
    res= i;
    while (i < oldN.length) {
      newN[i + 1]= oldN[i++];
    }
    rzo_.blocSitus.ligneSitu= newN;
    return res;
  }
  public int insereNouveauBiefLido(final SParametresDonBiefBlocPRO n) {
    final SParametresDonBiefBlocPRO[] oldN= pro_.donneesBief;
    final SParametresDonBiefBlocPRO[] newN=
      new SParametresDonBiefBlocPRO[oldN.length + 1];
    // il faut placer le nouveau donBief correctement (par ordre croissant des profils)
    int res= -1;
    int i= 0;
    while ((i < oldN.length)
      && (n.premierProfilAmont > oldN[i].premierProfilAmont)) {
      newN[i]= oldN[i++];
    }
    newN[i]= n;
    res= i;
    while (i < oldN.length) {
      newN[i + 1]= oldN[i++];
    }
    pro_.donneesBief= newN;
    return res;
  }
  public int insereNouvelleSingLido(
    final SParametresBiefSingLigneRZO n,
    final int position) {
    final SParametresBiefSingLigneRZO[] oldN= rzo_.blocSings.ligneSing;
    final SParametresBiefSingLigneRZO[] newN=
      new SParametresBiefSingLigneRZO[oldN.length + 1];
    int i= 0;
    while ((i < oldN.length) && (position > i)) {
      newN[i]= oldN[i++];
    }
    newN[i]= n;
    while (i < oldN.length) {
      newN[i + 1]= oldN[i++];
    }
    rzo_.blocSings.ligneSing= newN;
    return position;
  }
  public int[] intervalleProfilsBief(final SParametresBiefSituLigneRZO _bief) {
    final int[] res= new int[2];
    final int indiceDonBief= chercheDonBief(_bief);
    res[1]= pro_.donneesBief[indiceDonBief].premierProfilAmont - 1;
    if (indiceDonBief == 0) {
      res[0]= 1;
    } else {
      res[0]= pro_.donneesBief[indiceDonBief - 1].premierProfilAmont - 1;
    }
    return res;
  }
  private int chercheDonBief(final SParametresBiefSituLigneRZO _bief) {
    int i= 0;
    while ((i < rzo_.nbBief)
      && (_bief.x1
        > pro_.profilsBief[pro_.donneesBief[i].premierProfilAmont].abscisse)) {
      i++;
    }
    return i;
  }
}
