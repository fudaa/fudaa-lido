/**
 * @file         LidoImplementation.java
 * @creation     1999-01-17
 * @modification $Date: 2007-05-04 13:59:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;

import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.lido.DCalculLido;

import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.ressource.EbliResource;

import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.aide.FudaaAidePreferencesPanel;
import org.fudaa.fudaa.commun.aide.FudaaAstucesDialog;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.*;
import org.fudaa.fudaa.lido.ihmhelper.LidoIHMParams;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @version $Revision: 1.23 $ $Date: 2007-05-04 13:59:31 $ by $Author: deniger $
 * @author Axel von Arnim , Mickael Rubens
 */
public class LidoImplementation extends FudaaImplementation implements FudaaParamListener,
    FudaaProjetListener {

  public static ICalculLido                  SERVEUR_LIDO   ;
  public static IConnexion                   CONNEXION_LIDO ;
  //  public static IPersonne PERSONNE =null;
  private static  boolean                     DEMO_VERSION   ;
  // donnees membres privees
  IParametresLido                            iparams_;
  IResultatsLido                             iresults_;
  private LidoAssistant                      assistant_;
  //  private String args [] = new String [2];
  private FudaaProjet                        projet_;
  private FudaaProjetInformationsFrame       fProprietes_;
  //ajout de l'impression
  protected EbliFillePrevisualisation        previsuFille_;
  LidoFilleReseau                            fReseau_;
  //BArbreCalque arbre_;
  LidoIHMParams                              ihmP_;
  FudaaParamEventView                        msgView_;
  JFrame                                     ts_;
  protected BuHelpFrame                      aide_;
  // HACK IMMONDE POUR PASSER UN MAILLE PERMANENT
  private boolean                            isMaillePerm_;
  private static final boolean               enrResults_    = ((System
                                                                .getProperty("project.loadresults") == null) || (System
                                                                .getProperty("project.loadresults")
                                                                .equals("yes")));
  // InformationsSoftware
  public final static BuInformationsSoftware isApp_         = new BuInformationsSoftware();
  public final static BuInformationsDocument idApp_         = new BuInformationsDocument();
  private final static int                   RECENT_COUNT   = 10;
  static {
    isApp_.name = "Lido";
    // IMPORTANT: DECOMMENTER LE NETCHECK DANS LIDO.JAVA
    isApp_.version = "1.40";
    isApp_.date = "13-novembre-2003";
    isApp_.rights = "Tous droits r�serv�s. CETMEF (c)1999-2003";
    isApp_.contact = "benedicte.weil@equipement.gouv.fr";
    isApp_.license = "GPL";
    isApp_.languages = "fr,en";
    isApp_.http = "http://www.cetmef.equipement.gouv.fr/projets/hydraulique/clubcourseau/";
    isApp_.update = "http://www.utc.fr/fudaa/distrib/deltas/lido/";
    //isApp_.man = REMOTE_MAN;
    isApp_.man =FudaaLib.LOCAL_MAN + "lido/";
    isApp_.logo = LidoResource.LIDO.getIcon("lidologo");
    isApp_.banner = LidoResource.LIDO.getIcon("lidobanner");
    isApp_.authors = new String[] { "Axel von Arnim", "Mickael Rubens"};
    isApp_.contributors = new String[] { "Fr�d�ric Deniger"};
    isApp_.documentors = new String[] { "St�phane Ladreyt"};
    isApp_.testers = new String[] { "St�phane Ladreyt"};
    isApp_.specifiers = new String[] { "David Goutx, St�phane Ladreyt, Axel von Arnim"};
    isApp_.libraries = null;
    isApp_.thanks = new String[] { "Dodico and Ebli team"};
    idApp_.name = "Etude";
    idApp_.version = "0.01";
    idApp_.organization = "CETMEF";
    idApp_.author = System.getProperty("user.name");
    idApp_.contact = idApp_.author + "@equipement.gouv.fr";
    idApp_.date = FuLib.date();
    idApp_.logo = EbliResource.EBLI.getIcon("minlogo.gif");
    //    BuPrinter.INFO_LOG=isApp_;
    //    BuPrinter.INFO_DOC=idApp_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isApp_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isApp_;
  }

  // Constructeur
  public LidoImplementation() {
    super();
  }

  public void init() {
    super.init();
    try {
      if ((BuPreferences.BU.getStringProperty("button.icon") == null)
          || (BuPreferences.BU.getStringProperty("button.icon").equals(""))) {
        BuPreferences.BU.putStringProperty("button.icon", "true");
      }
      if ((BuPreferences.BU.getStringProperty("button.text") == null)
          || (BuPreferences.BU.getStringProperty("button.text").equals(""))) {
        BuPreferences.BU.putStringProperty("button.text", "false");
      }
      if (BuPreferences.BU.getIntegerProperty("browser.type", -1) == -1) {
        BuPreferences.BU.putIntegerProperty("browser.type", 2);
      }
      projet_ = null;
      ts_ = null;
      final BuInformationsSoftware infSoft = getInformationsSoftware();
      getApp().setTitle("Lido");
      final BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
      getApp().setMainMenuBar(mb);
      mb.addActionListener(this);
      mb.addMenu(buildLidoMenu());
      BuMenu menu = (BuMenu) mb.getMenu("IMPORTER");
      menu.addMenuItem("Lido 2.0", "IMPORTLIDO", null, false);
      menu = (BuMenu) mb.getMenu("EXPORTER");
      menu.addMenuItem("Lido 2.0", "EXPORTLIDO", null, false);
      menu.addMenuItem("R�sultats", "EXPORTRES", null, false);
      menu.addMenuItem("Ligne d'eau", "EXPORTLIG", null, false);
      menu.addMenuItem("Enveloppe des lignes d'eau", "EXPORTENV", null, false);
      final JMenuItem code1dItem = new JMenuItem("Noyau de calcul...");
      code1dItem.setActionCommand("ABOUTCODE1D");
      code1dItem.addActionListener(this);
      ((BuMenu) mb.getMenu("MENU_AIDE")).insert(code1dItem, 8);
      //      ((BuMenu)mb.getMenu("MENU_AIDE")).add(
      //        ((BuMenu)mb.getMenu("MENU_AIDE")).addMenuItem(
      //        "Astuces", "ASTUCES",null, false), 1);
      final BuToolBar tb = BuToolBar.buildBasicToolBar();
      getApp().setMainToolBar(tb);
      removeAction("DEFAIRE");
      removeAction("REFAIRE");
      removeAction("COPIER");
      removeAction("COUPER");
      removeAction("COLLER");
      removeAction("DUPLIQUER");
      removeAction("TOUTSELECTIONNER");
      removeAction("RECHERCHER");
      removeAction("REMPLACER");
      removeAction("RANGERICONES");
      removeAction("RANGERPALETTES");
      removeAction("ASTUCE");
      removeAction("INDEX_ALPHA");
      removeAction("INDEX_THEMA");
      removeAction("POINTEURAIDE");
      menu = (BuMenu) mb.getMenu("MENU_EDITION");
      mb.remove(menu);
      menu = (BuMenu) mb.getMenu("MENU_FENETRES");
      menu.addSeparator();
      menu.addMenuItem("Console", "CONSOLE", null, false);
      menu.addMenuItem(BuResource.BU.getString("Pr�f�rences"), "PREFERENCE", false, KeyEvent.VK_F2);
      tb.addSeparator();
      tb.addToolButton("Regime", "REGIME", LidoResource.LIDO.getIcon("regime"), false);
      tb.addSeparator();
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      tb.addToolButton("Calculer", "CALCULER", false);
      tb.addActionListener(this);
      getApp().setEnabledForAction("PREFERENCE", true);
      getApp().setEnabledForAction("CREER", true);
      getApp().setEnabledForAction("OUVRIR", true);
      getApp().setEnabledForAction("PROPRIETE", false);
      getApp().setEnabledForAction("LIDO", false);
      getApp().setEnabledForAction("CALCULER", false);
      getApp().setEnabledForAction("QUITTER", true);
      getApp().setEnabledForAction("ENREGISTRER", false);
      getApp().setEnabledForAction("ENREGISTRERSOUS", false);
      getApp().setEnabledForAction("FERMER", false);
      getApp().setEnabledForAction("IMPORTER", true);
      getApp().setEnabledForAction("IMPPROJET", true);
      getApp().setEnabledForAction("IMPORTLIDO", false);
      getApp().setEnabledForAction("EXPORTER", false);
      getApp().removeAction("VISIBLE_LEFTCOLUMN");
      setEnabledForAction("ASTUCES", true);
      assistant_ = new LidoAssistant();
      LidoResource.ASSISTANT = assistant_;
      //assistant_.setBorder(null);
      final BuMainPanel mp = getApp().getMainPanel();
      final BuColumn rc = mp.getRightColumn();
      rc.addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      final BuTaskView taches_ = new BuTaskView();
      final BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      //new BuTaskOperation(this, "SpyTask", "oprServeurCopie" ).run();
      rc.addToggledComponent("T�ches", "TACHE", sp, this);
      getMainPanel().setTaskView(taches_);
      msgView_ = new FudaaParamEventView();
      final BuScrollPane sp2 = new BuScrollPane(msgView_);
      sp2.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent("Messages", "MESSAGE", sp2, this);
      setMessageView(msgView_);
      FudaaParamChangeLog.CHANGE_LOG
          .setApplication((BuApplication) LidoApplication.FRAME, msgView_);
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(LidoPreferences.LIDO);
        mr.setResource(LidoResource.LIDO);
      }
      mp.setLogo(infSoft.logo);
    }
    catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  class My2000Panel extends BuPanel {

    public My2000Panel() {
      super();
      setLayout(null);
      final BuPicture pict = new BuPicture(isApp_.banner);
      pict.setOpaque(false);
      setPreferredSize(pict.getPreferredSize());
      final Component gp = new BuLabelMultiLine("Bienvenue dans Lido !");
      gp.setLocation(new Point((getPreferredSize().width - gp.getPreferredSize().width) / 2,
          getPreferredSize().height - gp.getPreferredSize().height));
      gp.setSize(gp.getPreferredSize());
      add(gp);
      add(pict);
    }
  }

  public void start() {
    final BuGlassPaneStop gps = new BuGlassPaneStop();
    gps.setLayout(new BuBorderLayout());
    gps.setVisible(true);
    final BuPanel pn = new BuPanel();
    final BuGridLayout lyt = new BuGridLayout(1, 10, 20, true, true, true, true);
    pn.setLayout(lyt);
    pn.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    final BuPanel pnContent = new BuPanel();
    pnContent.setLayout(new BuBorderLayout());
    pnContent.add(CtuluDialogPanel.getInfosPanel(getInformationsSoftware()),
        BuBorderLayout.WEST);
    pnContent.add(pn);
    final BuProgressBar progress = new BuProgressBar();
    progress.setMinimum(0);
    progress.setMaximum(100);
    final BuLabel lb = new BuLabel();
    pn.add(progress);
    pn.add(lb);
    lb.setText("Mise en place des composants");
    final JDialog msg = new JDialog(getFrame());
    msg.setModal(false);
    msg.setTitle("Initialisation ...");
    msg.setContentPane(pnContent);
    msg.setSize(400, 150);
    msg.doLayout();
    //    msg.pack();
    final Dimension dParent = Toolkit.getDefaultToolkit().getScreenSize();
    final Point pParent = new Point(0, 0);
    final Point pThis = new Point();
    pThis.x = pParent.x + (dParent.width - msg.getWidth()) / 2;
    pThis.y = pParent.y + (dParent.height - msg.getHeight()) / 2;
    msg.setLocation(pThis);
    msg.show();
    ((JFrame) getFrame()).setGlassPane(gps);
    final BuMainPanel mp = getMainPanel();
    super.start();
    BuPreferences.BU.applyOn(getApp());
    progress.setValue(10);
    mp.setProgression(10);
    //    projet_=new FudaaProjet(getApp(), new FudaaFiltreFichier("fld"), FuLib.getUserHome());
    //A la demande de St�phane Ladreyt: le dossier par defaut est modifie.
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("fld"));
    projet_.addFudaaProjetListener(this);
    final BuInformationsSoftware infSoft = getInformationsSoftware();
    mp.setLogo(infSoft.logo);
    progress.setValue(30);
    mp.setProgression(30);
    //    arbre_ = new BArbreCalque();
    //JScrollPane sp=new JScrollPane(arbre_);
    //sp.setSize(150,150);
    //mp.getRightColumn().addToggledComponent("Calques","CALQUE",sp,this);
    //mp.doLayout();
    //mp.validate();
    final BuMenuBar mb = getMainMenuBar();
    /*
     * for(int i=1; i <=4; i++) { String
     * fp=LidoPreferences.LIDO.getStringProperty("file.recent."+i+".path"); String
     * fi=LidoPreferences.LIDO.getStringProperty("file.recent."+i+".icon"); if(
     * (fp!=null)&&(!"".equals(fp))&&(new File(BuLib.expandedPath(fp)).canRead()) ) { BuIcon
     * icon=BuResource.BU.getIcon("texte"); if( !"".equals(icon) ) icon=BuResource.BU.getIcon(fi);
     * mb.addReopenItem(fp,icon); recent_count=(recent_count%4)+1; } }
     * getApp().setEnabledForAction("REOUVRIR",true);
     */
    mb.removeActionListener(this);
    assistant_.addEmitters((Container) getApp());
    mb.addActionListener(this);
    lb.setText("initialisation Lido");
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    fProprietes_.setProjet(projet_);
    progress.setValue(50);
    mp.setProgression(50);
    ihmP_ = new LidoIHMParams();
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    ihmP_.setProjet(projet_);
    //    if (!"no".equals(LidoPreferences.LIDO.getStringProperty("startupmessage")))
    //    {
    //      LidoDialogStartup startup =
    //        new LidoDialogStartup(getApp(), isApp_, new My2000Panel());
    //      startup.activate();
    //      LidoPreferences.LIDO.putStringProperty(
    //        "startupmessage",
    //        startup.isShowNextTime() ? "yes" : "no");
    //      LidoPreferences.LIDO.writeIniFile();
    //    }
    progress.setValue(70);
    msg.dispose();
    if (FudaaAidePreferencesPanel.isAstucesVisibles(LidoPreferences.LIDO)) {
      FudaaAstucesDialog.showDialog(LidoPreferences.LIDO, this, LidoAstuces.LIDO);
    }
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue dans " + isApp_.name + " !\n");
    mp.setProgression(0);
  }


  public void setMessageView(final FudaaParamEventView v) {
    msgView_ = v;
  }

  public FudaaParamEventView getMessageView() {
    return msgView_;
  }

  // Evenements
  public void actionPerformed(final ActionEvent _evt) {
    try {
      String action = _evt.getActionCommand();
      String arg = "";
      final int i = action.indexOf('(');
      if (i >= 0) {
        arg = action.substring(i + 1, action.length() - 1);
        action = action.substring(0, i);
      }
      if (action.equals("ABOUTCODE1D")) {
        new BuDialogMessage(getApp(), getInformationsSoftware(),
            "Noyau de calcul:\n  Lido 2.0\nAuteur:\n  EDF/LNH\nContact:\n  St�phane Ladreyt (CETMEF)")
            .activate();
      }
      else if (action.equals("CREER")) {
        creer();
      } else if (action.equals("OUVRIR")) {
        ouvrir(null);
      } else if (action.equals("REOUVRIR")) {
        ouvrir(arg);
      } else if (action.equals("ENREGISTRER")) {
        enregistrer();
      } else if (action.equals("ENREGISTRERSOUS")) {
        enregistrerSous();
      } else if (action.equals("FERMER")) {
        fermer();
      } else if (action.equals("PREFERENCE")) {
        preferences();
      } else if (action.equals("PROPRIETE")) {
        if (fProprietes_.getDesktopPane() != getMainPanel().getDesktop()) {
          addInternalFrame(fProprietes_);
        } else {
          activateInternalFrame(fProprietes_);
          //fProprietes_.setVisible(true);
          //if( fProprietes_.isClosed() ) fProprietes_.setClosed(false);
        }
      }
      else if (action.equals("CONSOLE")) {
        ts_.show();
      } else //impression
      if (("PREVISUALISER".equals(action)) || ("MISEENPAGE".equals(action))
          || ("IMPRIMER".equals(action))) {
        gestionnaireImpression(action);
      }
      else if (action.startsWith("IMPPROJET")) {
        importerProjet();
      } else if (action.equals("IMPORTLIDO")) {
        importerLido();
      } else if (action.equals("EXPORTLIDO")) {
        exporterLido();
      } else if (action.equals("EXPORTRES")) {
        exporterResultats();
      } else if (action.equals("EXPORTLIG")) {
        exporterLignedeau();
      } else if (action.equals("EXPORTENV")) {
        exporterEnveloppeLignedeau();
      } else if (action.equals("REGIME")) {
        changerRegime();
      } else if (action.equals("CALCULER")) {
        if (DEMO_VERSION) {
          new BuDialogError(getApp(), getInformationsSoftware(),
              "Ceci est une version de d�monstration,\n" + "vous ne pouvez pas lancer de calcul")
              .activate();
        } else {
          calculer();
        }
      }
      else if (action.equals("PROFIL")) {
        ihmP_.PROFIL().editer();
      } else if (action.equals("CALA:STRSTO")) {
        ihmP_.CALAGE().editer();
      } else if (action.equals("CALA:LAISSE")) {
        ihmP_.LAISSE().editer();
      } else if (action.equals("REZO:RESEAU")) {
        ihmP_.RESEAU().editer();
      } else if (action.equals("REZO:BIEF")) {
        ihmP_.BIEF().editer();
      } else if (action.equals("REZO:NOEUD")) {
        ihmP_.NOEUD().editer();
      } else if (action.equals("HYDRO:LIMITE")) {
        final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
        if ((!projet_.containsParam(LidoResource.RZO)) || (rzo.nbBief <= 0)) {
          new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: aucun bief d�fini (RZO)")
              .activate();
          return;
        }
        ihmP_.LIMITE().editer();
      }
      else if (action.equals("HYDRO:APPORT")) {
        final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
        if ((!projet_.containsParam(LidoResource.RZO)) || (rzo.nbBief <= 0)) {
          new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: aucun bief d�fini (RZO)")
              .activate();
          return;
        }
        ihmP_.APPORT().editer();
      }
      else if (action.equals("HYDRO:PERTE")) {
        final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
        if ((!projet_.containsParam(LidoResource.RZO)) || (rzo.nbBief <= 0)) {
          new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: aucun bief d�fini (RZO)")
              .activate();
          return;
        }
        ihmP_.PERTE().editer();
      }
      else if (action.equals("HYDRO:SINGULARITE")) {
        final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
        if ((!projet_.containsParam(LidoResource.RZO)) || (rzo.nbBief <= 0)) {
          new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: aucun bief d�fini (RZO)")
              .activate();
          return;
        }
        ihmP_.SINGULARITE().editer();
      }
      //        else if (action.equals("REZO:VISUALISER"))
      //        {
      //          if (fReseau_ == null)
      //          {
      //            fReseau_ = new LidoFilleReseau(arbre_, getApp(), projet_);
      //            addInternalFrame(fReseau_);
      //            fReseau_.restaurer();
      //            try
      //            {
      //              fReseau_.setMaximum(true);
      //            }
      //            catch (java.beans.PropertyVetoException e)
      //            {
      //            }
      //          }
      //          fReseau_.setParametresRZO(
      //            (SParametresRZO) projet_.getParam(LidoResource.RZO));
      //          fReseau_.setParametresPRO(
      //            (SParametresPRO) projet_.getParam(LidoResource.PRO));
      //          fReseau_.reinitialise();
      //          fReseau_.getVueCalque().changeRepere(
      //            this,
      //            fReseau_.getVueCalque().getCalque().getDomaine());
      //          activateInternalFrame(fReseau_);
      //        }
      else if (action.equals("REGI:PARAMCALCUL")) {
        ihmP_.PARAMCALCUL().editer();
      } else if (action.equals("REGI:SECTIONCALCUL")) {
        ihmP_.SECTIONCALCUL().editer();
      } else if (action.equals("REGI:PLANIMETRAGE")) {
        ihmP_.PLANIMETRAGE().editer();
      } else if (action.equals("REGI:VARTEMP")) {
        ihmP_.VARTEMP().editer();
      } else if (action.equals("LIGNINIT")) {
        final File file = LidoImport.chooseFile("lig");
        if (file == null) {
          return;
        }
        projet_.addImport(LidoResource.LIG, file.getPath());
      }
      else if (action.equals("RESU:ENLONG")) {
        ihmP_.RESULTATENLONG().editer();
      } else if (action.equals("RESU:ENTRAVERS")) {
        ihmP_.RESULTATPROFIL().editer();
      } else if (action.equals("RESU:LAISSE")) {
        ihmP_.RESULTATLAISSE().editer();
      } else if (action.equals("RESU:INFOCALCUL")) {
        ihmP_.RESULTATERREURS().editer();
      }
      else if (action.equals("CALLGC")) {
        final Runtime run = Runtime.getRuntime();
        System.out.println("Total mem: " + run.totalMemory() / 1024 + " Ko");
        System.out.println("Free mem: " + run.freeMemory() / 1024 + " Ko");
        System.out.println("Calling GC...");
        System.out.println("Total mem: " + run.totalMemory() / 1024 + " Ko");
        System.out.println("Free mem: " + run.freeMemory() / 1024 + " Ko");
      }
      else if (action.equals("ASSISTANT") || action.equals("CALQUE") || action.equals("TACHE")
          || action.equals("MESSAGE")) {
        final BuColumn rc = getMainPanel().getRightColumn();
        rc.toggleComponent(action);
        setCheckedForAction(action, rc.isToggleComponentVisible(action));
      }
      else //ajout fred deniger
      if (action.equals("ASTUCES")) {
        FudaaAstucesDialog.showDialog(LidoPreferences.LIDO, this, LidoAstuces.LIDO);
      } else {
        super.actionPerformed(_evt);
      }
    }
    catch (final Throwable t) {
      new BuDialogError(getApp(), getInformationsSoftware(), t.getMessage()).activate();
      t.printStackTrace();
    }
  }

  public void exit() {
    fermer();
    closeConnexions();
    CtuluFavoriteFiles.INSTANCE.saveFavorites();
    FudaaPreferences.FUDAA.writeIniFile();
    super.exit();
  }

  public void finalize() {
    closeConnexions();
  }

  public void displayURL(String _url) {
    if ((_url == null) || (_url.length() == 0)) {
      _url = FudaaLib.LOCAL_MAN;
    }
    if ((_url.startsWith("file")) && (_url.endsWith("/"))) {
      _url = _url + "index.html";
    }
    if (BuPreferences.BU.getIntegerProperty("browser.type") == 1) {
      if (aide_ == null) {
        aide_ = new BuHelpFrame();
      }
      addInternalFrame(aide_);
      aide_.setDocumentUrl(_url);
    }
    else {
      FudaaBrowserControl.displayURL(_url);
    }
  }

  public void contextHelp(final String _url) {
    String url = new String(_url);
    if ((url == null) || (url.length() == 0) || (url.startsWith("#"))) {
      url = "index.html";
    }
    super.contextHelp(url);
  }

  public boolean confirmExit() {
    return true;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  // FudaaParamListener
  public void paramStructCreated(final FudaaParamEvent e) {
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("EXPORTLIG", false);
      setEnabledForAction("EXPORTENV", false);
      //      ihmP_.fermerResultats();
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  public void paramStructDeleted(final FudaaParamEvent e) {
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("EXPORTLIG", false);
      setEnabledForAction("EXPORTENV", false);
      //      ihmP_.fermerResultats();
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  public void paramStructModified(final FudaaParamEvent e) {
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("EXPORTLIG", false);
      setEnabledForAction("EXPORTENV", false);
      //      ihmP_.fermerResultats();
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  // fudaaprojet listener
  public void dataChanged(final FudaaProjetEvent e) {
    switch (e.getID()) {
    case FudaaProjetEvent.RESULT_ADDED:
    case FudaaProjetEvent.RESULTS_CLEARED:
      {
        FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
        setEnabledForAction("ENREGISTRER", true);
        break;
      }
    }
  }

  /*
   * public void contextHelp(String _url) { String netHelp=System.getProperty("net.access.man"); if(
   * !"remote".equals(netHelp) ) isApp_.man=LOCAL_MAN; else isApp_.man=REMOTE_MAN;
   * super.contextHelp(_url); }
   */
  public void statusChanged(final FudaaProjetEvent e) {
    if (e.getID() == FudaaProjetEvent.HEADER_CHANGED) {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
    }
  }

  // Menu Lido
  private static BuMenu buildLidoMenu() {
    final BuMenu r = new BuMenu("Lido", "LIDO");
    r.addMenuItem("Profils en travers", "PROFIL", null, true);
    final BuMenu mnCala = new BuMenu("Calage", "CALAGE");
    mnCala.addMenuItem("Strickler/Zones de stockage", "CALA:STRSTO", null, true);
    mnCala.addMenuItem("Laisses de crue", "CALA:LAISSE", null, true);
    r.addSubMenu(mnCala, true);
    final BuMenu mnRezo = new BuMenu("Param�tres du r�seau", "RESEAU");
    mnRezo.addMenuItem("Biefs", "REZO:BIEF", null, true);
    mnRezo.addMenuItem("Noeuds", "REZO:NOEUD", null, true);
    mnRezo.addMenuItem("Visualiser", "REZO:VISUALISER", null, false);
    r.addSubMenu(mnRezo, true);
    final BuMenu mnHydr = new BuMenu("Param�tres hydrauliques", "HYDRO");
    mnHydr.addMenuItem("Conditions limites", "HYDRO:LIMITE", null, true);
    mnHydr.addMenuItem("Apports/Soutirages", "HYDRO:APPORT", null, true);
    mnHydr.addMenuItem("Pertes de charge", "HYDRO:PERTE", null, true);
    mnHydr.addMenuItem("Lois de singularit�s", "HYDRO:SINGULARITE", null, true);
    r.addSubMenu(mnHydr, true);
    final BuMenu mnPerm = new BuMenu("R�gime permanent", "PERMANENT");
    mnPerm.addMenuItem("Param�tres de calcul", "REGI:PARAMCALCUL", null, true);
    mnPerm.addMenuItem("Sections de calcul", "REGI:SECTIONCALCUL", null, true);
    mnPerm.addMenuItem("Planim�trage", "REGI:PLANIMETRAGE", null, true);
    r.addSubMenu(mnPerm, true);
    final BuMenu mnNPerm = new BuMenu("R�gime non permanent", "NONPERMANENT");
    mnNPerm.addMenuItem("Variables temporelles", "REGI:VARTEMP", null, true);
    mnNPerm.addMenuItem("Sections de calcul", "REGI:SECTIONCALCUL", null, true);
    mnNPerm.addMenuItem("Planim�trage", "REGI:PLANIMETRAGE", null, true);
    r.addSubMenu(mnNPerm, false);
    final BuMenu mnResu = new BuMenu("R�sultats", "RESULTAT");
    mnResu.addMenuItem("Profils en long", "RESU:ENLONG", null, true);
    mnResu.addMenuItem("Profils en travers", "RESU:ENTRAVERS", null, true);
    mnResu.addMenuItem("Laisses de crues", "RESU:LAISSE", null, true);
    mnResu.addMenuItem("Informations calcul", "RESU:INFOCALCUL", null, true);
    r.addSubMenu(mnResu, false);
    //    r.addMenuItem("Lib�rer m�moire", "CALLGC", true);
    return r;
  }

  //  private void aideIndex()
  //  {
  //    BuInformationsSoftware infSoft = getInformationsSoftware();
  //    String tm = "Allez donc faire un tour ici:\n" + infSoft.http;
  //    BuDialogMessage dialogMesg = new BuDialogMessage(this, infSoft, tm);
  //    dialogMesg.activate();
  //  }
  private void calculer() {
    if (!isConnected()) {
      new BuDialogError(getApp(), isApp_, "vous n'etes pas connect� � un serveur!").activate();
      return;
    }
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    final String proAbsMsg = LidoModifStructRecord.STRUCT_MODIF_RECORD.getProfilAbscissesMsg();
    final String apportsMsg = LidoModifStructRecord.STRUCT_MODIF_RECORD.getApportsMsg();
    final String pertesMsg = LidoModifStructRecord.STRUCT_MODIF_RECORD.getPertesMsg();
    final String loissngMsg = LidoModifStructRecord.STRUCT_MODIF_RECORD.getLoissngMsg();
    final String loisclmMsg = LidoModifStructRecord.STRUCT_MODIF_RECORD.getLoisclmMsg();
    String msgRecord = "";
    if (proAbsMsg != null) {
      msgRecord += "Vous avez modifi� les abscisses des profils:\n" + proAbsMsg +  CtuluLibString.LINE_SEP_SIMPLE
          + "Avez-vous v�rifi�:\n" + "- le planim�trage\n" + "- les sections de calcul\n"
          + "- les param�tres de calcul\n" + "- les param�tres hydrauliques?\n\n";
    }
    if (apportsMsg != null) {
      msgRecord += "Vous avez modifi� les apports d'abscisse:\n" + apportsMsg + "\n\n";
    }
    if (pertesMsg != null) {
      msgRecord += "Vous avez modifi� les pertes d'abscisse:\n" + pertesMsg + "\n\n";
    }
    if (loissngMsg != null) {
      msgRecord += "Vous avez modifi� les lois de singularite:\n" + loissngMsg + "\n\n";
    }
    if (loisclmMsg != null) {
      msgRecord += "Vous avez modifi� les lois hydrauliques:\n" + loisclmMsg + "\n\n";
    }
    if (!msgRecord.equals("")) {
      msgRecord += "Voulez-vous vraiment lancer le calcul?\n";
      if (new BuDialogConfirmation(getApp(), isApp_, msgRecord).activate() == JOptionPane.NO_OPTION) {
        LidoModifStructRecord.STRUCT_MODIF_RECORD.reinit();
        return;
      }
      LidoModifStructRecord.STRUCT_MODIF_RECORD.reinit();
    }
    if (!verifieContraintesCalcul()) {
      return;
    }
    ihmP_.fermer();
    if (iparams_ == null) {
      new BuDialogMessage(getApp(), isApp_, "Impossible de transmettre les param�tres au serveur!")
          .activate();
      return;
    }
    // HACK IMMONDE POUR PASSER LE CALCUL EN MAILLE PERMANENT
    if ("P".equals(cal.genCal.regime.toUpperCase().trim())) {
      final int resp = new BuDialogConfirmation(getApp(), isApp_, "Etes-vous en r�seau maill�?")
          .activate();
      isMaillePerm_ = (resp == JOptionPane.YES_OPTION);
      if (isMaillePerm_) {
        ihmP_.getPH().hackPermanentMaille("PRE-TRAITEMENT");
      }
    }
    else {
      isMaillePerm_ = false;
    }
    setEnabledForAction("LIDO", false);
    setEnabledForAction("IMPORTER", false);
    final BuTaskOperation op = new BuTaskOperation(this, "Calcul", "oprCalculer");
    op.start();
    //FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    final BuMainPanel mp = getMainPanel();
    new Thread() {

      public void run() {
        SProgression msg;
        if (op.isAlive()) {
          msg = SERVEUR_LIDO.progression();
          while (msg == null) {
            msg = SERVEUR_LIDO.progression();
            try {
              Thread.sleep(1000);
            }
            catch (final InterruptedException e) {}
          }
        }
        while (op.isAlive()) {
          msg = SERVEUR_LIDO.progression();
          if (msg == null) {
            mp.setProgression(100);
            mp.setMessage("Op�ration temin�e");
            try {
              Thread.sleep(1000);
            }
            catch (final InterruptedException e) {}
            break;
          }
          mp.setMessage(msg.operation);
          mp.setProgression(msg.pourcentage);
          try {
            Thread.sleep(1000);
          }
          catch (final InterruptedException e) {}
        }
        mp.setProgression(0);
        mp.setMessage("");
      }
    }.start();
  }

  private void importerProjet() {
    setEnabledForAction("IMPORTLIDO", true);
    setEnabledForAction("EXPORTLIDO", true);
    setEnabledForAction("IMPPROJET", false);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("LIDO", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("REGIME", true);
    setEnabledForAction("CALCULER", true);
    LidoModifStructRecord.STRUCT_MODIF_RECORD.reinit();
    updateMenus();
    updateTitle();
  }

  private void importerLido() {
    final File file = LidoImport.chooseFile("cal");
    if (file == null) {
      return;
    }
    ihmP_.fermer();
    try {
      String name = file.getPath();
      name = name.substring(0, name.lastIndexOf('.'));
      final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
      final SParametresPRO pro = (SParametresPRO) projet_.getParam(LidoResource.PRO);
      final SParametresSNG sng = (SParametresSNG) projet_.getParam(LidoResource.SNG);
      final SParametresCLM clm = (SParametresCLM) projet_.getParam(LidoResource.CLM);
      final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
      final SParametresLIG lig = (SParametresLIG) projet_.getParam(LidoResource.LIG);
      projet_.addImport(LidoResource.RZO, name + ".rzo");
      if ((rzo != null) && (!((SParametresRZO) projet_.getParam(LidoResource.RZO)).status)) {
        System.err.println("parametre incorrect -> reinitialisation");
        projet_.addParam(LidoResource.RZO, rzo);
      }
      final int nbBief = ((SParametresRZO) projet_.getParam(LidoResource.RZO)).nbBief;
      projet_.addImport(LidoResource.PRO, name + ".pro", new Object[] { new Integer(nbBief)});
      if ((pro != null) && (!((SParametresPRO) projet_.getParam(LidoResource.PRO)).status )) {
        System.err.println("parametre incorrect -> reinitialisation");
        projet_.addParam(LidoResource.PRO, pro);
      }
      projet_.addImport(LidoResource.SNG, name + ".sng");
      if ((sng != null) && (!((SParametresSNG) projet_.getParam(LidoResource.SNG)).status)) {
        System.err.println("parametre incorrect -> reinitialisation");
        projet_.addParam(LidoResource.SNG, sng);
      }
      projet_.addImport(LidoResource.CLM, name + ".clm");
      if ((clm != null) && (!((SParametresCLM) projet_.getParam(LidoResource.CLM)).status )) {
        System.err.println("parametre incorrect -> reinitialisation");
        projet_.addParam(LidoResource.CLM, clm);
      }
      projet_.addImport(LidoResource.CAL, name + ".cal");
      if ((cal != null) && (!((SParametresCAL) projet_.getParam(LidoResource.CAL)).status )) {
        System.err.println("parametre incorrect -> reinitialisation");
        projet_.addParam(LidoResource.CAL, cal);
      }
      projet_.addImport(LidoResource.LIG, name + ".lig");
      if ((lig != null) && (!((SParametresLIG) projet_.getParam(LidoResource.LIG)).status )) {
        System.err.println("parametre incorrect -> reinitialisation");
        projet_.addParam(LidoResource.LIG, lig);
      }
    }
    catch (final Throwable t) {
      if (previsuFille_.isVisible()) {
        previsuFille_.setVisible(false);
      }
      projet_.fermer();
      System.err.println(t);
      new BuDialogError(getApp(), getInformationsSoftware(), t.getMessage()).activate();
      return;
    }
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructCreated(new FudaaParamEvent(this, 0, "", null,
        "import Lido 2.0"));
    if (!verifieContraintes()) {
      fermer();
      return;
    }
    new BuDialogMessage(getApp(), isApp_, "Param�tres charg�s").activate();
    setEnabledForAction("IMPORTLIDO", true);
    setEnabledForAction("IMPPROJET", false);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("LIDO", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    LidoModifStructRecord.STRUCT_MODIF_RECORD.reinit();
    updateMenus();
    updateTitle();
  }

  private void exporterResultats() {
    final File file = LidoExport.chooseFile(null);
    if (file == null) {
      return;
    }
    if (projet_.containsResult(LidoResource.RSN)) {
      projet_.export(LidoResource.RSN, file.getPath(),
          new Object[] { projet_.getParam(LidoResource.CAL), new Boolean(false), new SProgression()});
    } else {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il n'y a pas de r�sultats RSN :\nvous n'avez pas lance de calcul").activate();
    }
  }

  private void exporterLignedeau() {
    final File file = LidoExport.chooseFile("lig");
    if (file == null) {
      return;
    }
    if (projet_.containsResult(LidoResource.LIG)) {
      projet_.export(LidoResource.LIG, file.getPath());
    } else {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il n'y a pas de r�sultats LIG :\nvous n'avez pas lance de calcul").activate();
    }
  }

  private void exporterEnveloppeLignedeau() {
    final File file = LidoExport.chooseFile(null);
    if (file == null) {
      return;
    }
    if (projet_.containsResult(LidoResource.RSN)) {
      try {
        LidoExport.exportEnveloppeLigneDeau((SResultatsRSN) projet_.getResult(LidoResource.RSN),
            file);
      }
      catch (final IOException e) {
        new BuDialogError(getApp(), getInformationsSoftware(), e.getMessage()).activate();
      }
    } else {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il n'y a pas de r�sultats RSN :\nvous n'avez pas lance de calcul").activate();
    }
  }

  private void exporterLido() {
    final LidoExportPanneau p = new LidoExportPanneau(LidoApplication.FRAME, projet_);
    p.show();
    if (p.valeurRetour() != LidoExportPanneau.OK) {
      return;
    }
    final String[] choix = p.getChoix();
    if (choix == null) {
      return;
    }
    final File dir = p.getFichier();
    if (dir == null) {
      return;
    }
    final String nom = dir.getAbsolutePath();
    // application des auto-contraintes
    appliqueAutoContraintes();
    for (int i = 0; i < choix.length; i++) {
      if (LidoResource.CAL.equals(choix[i])) {
        projet_.export(LidoResource.CAL, nom + ".cal");
      } else if (LidoResource.CLM.equals(choix[i])) {
        projet_.export(LidoResource.CLM, nom + ".clm");
      } else if (LidoResource.LIG.equals(choix[i])) {
        projet_.export(LidoResource.LIG, nom + ".lig");
      } else if (LidoResource.PRO.equals(choix[i])) {
        projet_.export(LidoResource.PRO, nom + ".pro",
            new Object[] { projet_.getParam(LidoResource.RZO)});
      } else if (LidoResource.RZO.equals(choix[i])) {
        projet_.export(LidoResource.RZO, nom + ".rzo");
      } else if (LidoResource.SNG.equals(choix[i])) {
        projet_.export(LidoResource.SNG, nom + ".sng");
      } else if (LidoResource.ERN.equals(choix[i])) {
        if (projet_.containsResult(LidoResource.ERN)) {
          projet_.export(LidoResource.ERN, nom
              + ".ern");
        } else {
          new BuDialogError(getApp(), getInformationsSoftware(),
              "Il n'y a pas de r�sultats ERN :\nvous n'avez pas lance de calcul").activate();
        }
      }
      if (LidoResource.RSN.equals(choix[i])) {
        if (projet_.containsResult(LidoResource.RSN)) {
          projet_.export(LidoResource.RSN, nom
              + ".rsn", new Object[] { projet_.getParam(LidoResource.CAL), new SProgression()});
        } else {
          new BuDialogError(getApp(), getInformationsSoftware(),
              "Il n'y a pas de r�sultats RSN :\nvous n'avez pas lance de calcul").activate();
        }
      }
    }
  }

  private void creer() {
    fermer();
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si creation de projet annulee
    } else { // nouveau projet cree
      ihmP_.initialiseParametres();
      enregistrer();
      updateMenus();
      updateTitle();
      setEnabledForAction("IMPORTLIDO", true);
      setEnabledForAction("EXPORTLIDO", true);
      setEnabledForAction("IMPPROJET", false);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("LIDO", true);
      setEnabledForAction("REGIME", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("FERMER", true);
      LidoModifStructRecord.STRUCT_MODIF_RECORD.reinit();
    }
  }

  private void ouvrir(final String arg) {
    fermer();
    projet_.setEnrResultats(enrResults_);
    projet_.ouvrir(arg);
    if (!projet_.estConfigure()) {
      fermer(); // si ouverture de projet echouee
      return;
    }
    if (arg == null) {
      final String r = projet_.getFichier();
      getMainMenuBar().addRecentFile(r, "lido");
      /*
       * getMainMenuBar().addReopenItem(r, LidoResource.LIDO.getIcon("lido"));
       * recent_count=(recent_count%4)+1; LidoPreferences.LIDO.putStringProperty
       * ("file.recent."+recent_count+".path",r); LidoPreferences.LIDO.putStringProperty
       * ("file.recent."+recent_count+".icon","lido");
       */
      LidoPreferences.LIDO.writeIniFile();
    }
    if (!verifieContraintes()) {
      fermer();
      return;
    }
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), isApp_, "Param�tres charg�s.\n"
          + "Ce projet contient des r�sultats.").activate();
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("RESULTAT", true);
      setEnabledForAction("EXPORTRES", true);
      final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
      if (cal == null) {
        return;
      }
      if ("P".equals(cal.genCal.regime.trim().toUpperCase())) {
        setEnabledForAction("EXPORTLIG", true);
        setEnabledForAction("EXPORTENV", false);
      }
      else {
        setEnabledForAction("EXPORTLIG", false);
        setEnabledForAction("EXPORTENV", true);
      }
    } else {
      new BuDialogMessage(getApp(), isApp_, "Param�tres charg�s").activate();
    }
    setEnabledForAction("IMPORTLIDO", true);
    setEnabledForAction("EXPORTLIDO", true);
    setEnabledForAction("IMPPROJET", false);
    setEnabledForAction("EXPORTER", true);
    setEnabledForAction("PROPRIETE", true);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("LIDO", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("REGIME", true);
    setEnabledForAction("CALCULER", true);
    LidoModifStructRecord.STRUCT_MODIF_RECORD.reinit();
    updateMenus();
    updateTitle();
  }

  private void fermer() {
    if (projet_.estConfigure() && FudaaParamChangeLog.CHANGE_LOG.isDirty()) {
      final int res = new BuDialogConfirmation(getApp(), isApp_, "Votre projet va etre ferm�.\n"
          + "Voulez-vous l'enregistrer avant?\n").activate();
      if (res == JOptionPane.YES_OPTION) {
        enregistrer();
      }
    }
    if (fProprietes_.isVisible()) {
      fProprietes_.setVisible(false);
    }
    if ((previsuFille_ != null) && (previsuFille_.isVisible())) {
      previsuFille_.setVisible(false);
    }
    projet_.fermer();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("IMPORTLIDO", false);
    setEnabledForAction("EXPORTLIDO", false);
    setEnabledForAction("IMPPROJET", true);
    setEnabledForAction("EXPORTER", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("LIDO", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("REGIME", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("RESULTAT", false);
    setEnabledForAction("EXPORTRES", false);
    setEnabledForAction("EXPORTLIG", false);
    setEnabledForAction("EXPORTENV", false);
    setTitle("Lido");
  }

  private void enregistrer() {
    if (enrResults_) {
      projet_.setEnrResultats(false);
      if (projet_.containsResults()) {
        final int res = new BuDialogConfirmation(getApp(), isApp_, "Ce projet contient des r�sultats:\n"
            + "voulez-vous les sauvegarder?").activate();
        if (res == JOptionPane.YES_OPTION) {
          projet_.setEnrResultats(true);
        }
      }
    }
    projet_.enregistre();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(LidoPreferences.LIDO.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "lido");
      LidoPreferences.LIDO.writeIniFile();
    }
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
  }

  private void enregistrerSous() {
    if (enrResults_) {
      projet_.setEnrResultats(false);
      if (projet_.containsResults()) {
        final int res = new BuDialogConfirmation(getApp(), isApp_, "Ce projet contient des r�sultats:\n"
            + "voulez-vous les sauvegarder?").activate();
        if (res == JOptionPane.YES_OPTION) {
          projet_.setEnrResultats(true);
        }
      }
    }
    projet_.enregistreSous();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(LidoPreferences.LIDO.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "lido");
      LidoPreferences.LIDO.writeIniFile();
    }
    updateTitle();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
  }

  public void oprCalculer() {
    setEnabledForAction("CONNECTER", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("RESULTAT", false);
    setEnabledForAction("EXPORTRES", false);
    setEnabledForAction("EXPORTLIG", false);
    setEnabledForAction("EXPORTENV", false);
    System.err.println("Transmission des parametres...");
    iparams_.parametresRZO((SParametresRZO) projet_.getParam(LidoResource.RZO));
    iparams_.parametresPRO((SParametresPRO) projet_.getParam(LidoResource.PRO));
    iparams_.parametresSNG((SParametresSNG) projet_.getParam(LidoResource.SNG));
    iparams_.parametresCLM((SParametresCLM) projet_.getParam(LidoResource.CLM));
    iparams_.parametresCAL((SParametresCAL) projet_.getParam(LidoResource.CAL));
    iparams_.parametresLIG((SParametresLIG) projet_.getParam(LidoResource.LIG));
    System.err.println("Execution du calcul...");
    String msg = "";
    try {
      SERVEUR_LIDO.calcul(CONNEXION_LIDO);
    }
    catch (final Throwable u) {
      msg += "Une erreur s'est produite. Cela peut-etre du \n";
      msg += "� une erreur du calcul. Cliquez sur Continuer\n";
      msg += "pour voir les informations de calcul.\n";
      new BuDialogError(getApp(), isApp_, msg).activate();
      receptionResultats();
      // HACK IMMONDE POUR PASSER LE CALCUL EN MAILLE PERMANENT
      if (isMaillePerm_) {
        ihmP_.getPH().hackPermanentMaille("POST-TRAITEMENT");
      }
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("LIDO", true);
      setEnabledForAction("IMPORTER", true);
      return;
    }
    receptionResultats();
    // HACK IMMONDE POUR PASSER LE CALCUL EN MAILLE PERMANENT
    if (isMaillePerm_) {
      ihmP_.getPH().hackPermanentMaille("POST-TRAITEMENT");
    }
    System.err.println("Operation terminee.");
    setEnabledForAction("LIDO", true);
    setEnabledForAction("IMPORTER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("RESULTAT", true);
    setEnabledForAction("EXPORTRES", true);
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    if (cal == null) {
      return;
    }
    if ("P".equals(cal.genCal.regime.trim().toUpperCase())) {
      setEnabledForAction("EXPORTLIG", true);
      setEnabledForAction("EXPORTENV", false);
    }
    else {
      setEnabledForAction("EXPORTLIG", false);
      setEnabledForAction("EXPORTENV", true);
    }
    setEnabledForAction("CONNECTER", true);
  }

  protected void receptionResultats() {
    System.err.println("R�ception des resultats...");
    if (iresults_ == null) {
      new BuDialogError(getApp(), isApp_, "Impossible de r�cup�rer les r�sultats du serveur!")
          .activate();
      return;
    }
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    SResultatsRSN rsn = null;
    SResultatsERN ern = null;
    SParametresLIG lig = null;
    try {
      rsn = iresults_.resultatsRSN();
    }
    catch (final org.omg.CORBA.SystemException ce) {
      new BuDialogError(getApp(), isApp_, "Impossible de r�cup�rer les r�sultats RSN!").activate();
      ce.printStackTrace();
    }
    try {
      ern = iresults_.resultatsERN();
    }
    catch (final org.omg.CORBA.SystemException ce) {
      new BuDialogError(getApp(), isApp_, "Impossible de r�cup�rer les r�sultats ERN!").activate();
      ce.printStackTrace();
    }
    if ((cal != null) && ("P".equals(cal.genCal.regime.trim().toUpperCase()))) {
      try {
        lig = iresults_.resultatsLIG();
      }
      catch (final org.omg.CORBA.SystemException ce) {
        new BuDialogError(getApp(), isApp_, "Impossible de r�cup�rer les r�sultats LIG!")
            .activate();
        ce.printStackTrace();
      }
    }
    if (rsn != null) {
      projet_.addResult(LidoResource.RSN, rsn);
    }
    if (ern != null) {
      projet_.addResult(LidoResource.ERN, ern);
      final int rep = new BuDialogConfirmation(getApp(), isApp_, "Calcul termin�\n"
          + "Voulez-vous voir les informations de calcul ?\n").activate();
      if (rep == JOptionPane.YES_OPTION) {
        ihmP_.RESULTATERREURS().editer();
      }
    }
    if (lig != null) {
      projet_.addResult(LidoResource.LIG, lig);
      projet_.addParam(LidoResource.LIG, lig);
    }
  }

  private void updateTitle() {
    String name = projet_.getFichier();
    final int pos = name.lastIndexOf(System.getProperty("file.separator"));
    if (pos != -1) {
      name = name.substring(pos + 1);
    }
    setTitle("Lido - "
        + name
        + " - "
        + (projet_.containsParam(LidoResource.CAL) ? ((SParametresCAL) projet_
            .getParam(LidoResource.CAL)).genCal.regime : ""));
  }

  private void updateMenus() {
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    if (cal == null) {
      return;
    }
    if ("P".equals(cal.genCal.regime.trim().toUpperCase())) {
      setEnabledForAction("PROFIL", true);
      setEnabledForAction("RESEAU", true);
      setEnabledForAction("PARAMCALCUL", true);
      setEnabledForAction("PERMANENT", true);
      setEnabledForAction("NONPERMANENT", false);
    }
    else {
      setEnabledForAction("PROFIL", false);
      setEnabledForAction("RESEAU", false);
      setEnabledForAction("PARAMCALCUL", false);
      setEnabledForAction("PERMANENT", false);
      setEnabledForAction("NONPERMANENT", true);
    }
  }

  private void changerRegime() {
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    final SParametresCLM clm = (SParametresCLM) projet_.getParam(LidoResource.CLM);
    final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
    final SParametresLIG lig = (SParametresLIG) projet_.getResult(LidoResource.LIG);
    if (lig == null) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Erreur: vous devez lancer un calcul\n" + "en permanent pour pouvoir passer en\n"
              + "non permanent.\n").activate();
      return;
    }
    if ("P".equals(cal.genCal.regime.toUpperCase().trim())) {
      String fichier = projet_.getFichier();
      fichier = fichier.substring(0, fichier.lastIndexOf('.'));
      if (fichier.endsWith("P")) {
        fichier = fichier.substring(0, fichier.lastIndexOf('P'));
      }
      if (fichier.endsWith("-")) {
        fichier = fichier.substring(0, fichier.length() - 1);
      }
      fichier += "-NP";
      final BuPanel pn = new BuPanel();
      pn.setLayout(new BuVerticalLayout(5, true, false));
      pn.add(new BuLabelMultiLine("Vous allez passer en r�gime non permanent.\n"
          + "Votre projet va etre sauvegard� dans le fichier:\n"), 0);
      final BuTextField tf = new BuTextField();
      tf.setText(fichier);
      pn.add(tf, 1);
      pn.add(new BuLabel("Voulez-vous continuer?"), 2);
      final BuDialogConfirmation dm = new BuDialogConfirmation(getApp(), getInformationsSoftware(), pn);
      dm.setSize(pn.getPreferredSize());
      if (dm.activate() != JOptionPane.YES_OPTION) {
        return;
      }
      if (projet_.estConfigure() && FudaaParamChangeLog.CHANGE_LOG.isDirty()) {
        final int res = new BuDialogConfirmation(getApp(), isApp_, "Votre projet en mode permanent\n"
            + "va etre ferm�.\n" + "Voulez-vous l'enregistrer avant?\n").activate();
        if (res == JOptionPane.YES_OPTION) {
          enregistrer();
        }
      }
      // vidage des resultats
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTRES", false);
      setEnabledForAction("EXPORTLIG", false);
      setEnabledForAction("EXPORTENV", false);
      ihmP_.fermer();
      projet_.clearResults();
      // vidage des CL et des apports
      clm.condLimites = new SParametresCondLimBlocCLM[0];
      clm.nbCondLim = 0;
      clm.apport = new SParametresApportLigneCLM[0];
      clm.nbApport = 0;
      rzo.blocLims.ligneLim = new SParametresBiefLimLigneRZO[0];
      rzo.nbLimi = 0;
      // RAZ des variables temporelles
      cal.temporel.tInit = 0.;
      cal.temporel.tMax = 0.;
      cal.temporel.pas2T = 0.;
      cal.temporel.pas2TImp = 0.;
      cal.temporel.pas2TStoc = 0.;
      // nettoyage du LIG
      lig.zref = new double[0];
      lig.vmin = new double[0];
      lig.vmaj = new double[0];
      lig.rgc = new double[0];
      lig.rdc = new double[0];
      lig.st1 = new double[0];
      lig.st2 = new double[0];
      lig.rmin = new double[0];
      lig.rmaj = new double[0];
      lig.vol = new double[0];
      lig.vols = new double[0];
      lig.frou = new double[0];
      final String tfText = tf.getText();
      if ((tfText == null) || (tfText.equals(""))) {
        return;
      }
      final File npFile = new File(tfText);
      if ((npFile.getParentFile() != null) && (!npFile.getParentFile().canWrite())) {
        new BuDialogError(getApp(), getInformationsSoftware(), "Erreur: le fichier\n"
            + npFile.getAbsolutePath() +  CtuluLibString.LINE_SEP_SIMPLE + "n'est pas accessible en �criture!\n").activate();
        return;
      }
      projet_.setFichier(npFile.getAbsolutePath());
      new BuDialogMessage(getApp(), getInformationsSoftware(),
          "Avant de lancer le calcul, vous devez modifier:\n" + "  - les conditions aux limites\n"
              + "  - les apports/soutirages\n" + "  - le planim�trage\n"
              + "  - les sections de calcul\n" + "renseigner\n" + "  - les zones de stockage\n"
              + "  - les variables temporelles\n" + "et completer\n" + "  - les singularit�s\n")
          .activate();
      cal.genCal.regime = "NP";
      updateMenus();
      updateTitle();
      if (new File(projet_.getFichier()).exists()) {
        if (new BuDialogConfirmation(getApp(), isApp_, "Le fichier :\n" + projet_.getFichier()
            + "\nexiste d�j�. Voulez-vous l'�craser?\n").activate() == JOptionPane.NO_OPTION) {
          fermer();
        } else {
          enregistrer();
        }
      }
    }
    else if ("NP".equals(cal.genCal.regime.toUpperCase().trim())) {
      new BuDialogMessage(getApp(), getInformationsSoftware(),
          "Vous ne pouvez pas repasser en r�gime permanent.\n"
              + "Vous devez rouvrir votre projet P.\n").activate();
      return;
    }
  }

  protected void buildPreferences(final List _prefs){
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new LidoPreferencesPanel(this));
    _prefs.add(new FudaaAidePreferencesPanel(this, LidoPreferences.LIDO));
    _prefs.add(new EbliMiseEnPagePreferencesPanel());
  }

  private boolean verifieContraintes() {
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    final SParametresSNG sng = (SParametresSNG) projet_.getParam(LidoResource.SNG);
    if (cal == null) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "ERREUR: pas de donn�es g�r�rales (CAL)\n" + "Cette �tude ne peut etre ouverte, d�sol�!")
          .activate();
      return false;
    }
    cal.temporel.numDerPaStoc = 0;
    cal.planimetrage.varPlanNbVal = LidoResource.PLANIMETRAGE.NBVALEURS_PLANI;
    cal.genCal.impGeo = "NON";
    cal.genCal.impPlan = "NON";
    cal.genCal.impRez = "NON";
    cal.genCal.impHyd = "NON";
    cal.genCal.code = "REZO";
    cal.fic.nFGeo = 20;
    cal.fic.nFSing = 21;
    cal.fic.nFRez = 26;
    cal.fic.nFLign = 22;
    cal.fic.nFCLim = 23;
    cal.fic.nFSLec = 24;
    cal.fic.nFSEcr = 25;
    if ("P".equalsIgnoreCase(cal.genCal.regime.trim())) {
      setEnabledForAction("NONPERMANENT", false);
      setEnabledForAction("PERMANENT", true);
      cal.temporel.tInit = 1.;
      cal.temporel.tMax = 1.;
      cal.temporel.pas2T = 1.;
      cal.temporel.pas2TImp = 1.;
      cal.temporel.pas2TStoc = 1.;
    }
    else {
      setEnabledForAction("NONPERMANENT", true);
      setEnabledForAction("PERMANENT", false);
    }
    // Lois SNG, si pas de fichier (pour les anciens projets qui n'initialisaient
    // pas correctement)
    if (!sng.status ) {
      projet_.addParam(LidoResource.SNG, LidoParamsHelper.creeParametresSNG());
    }
    // Pour les projets ne contenant pas de donnees EXT (anterieurs au 24/12/99)
    if (!projet_.containsParam(LidoResource.EXT)) {
      projet_.addParam(LidoResource.EXT, LidoParamsHelper.creeParametresEXT());
    }
    return true;
  }

  private void appliqueAutoContraintes() {
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    final SParametresPRO pro = (SParametresPRO) projet_.getParam(LidoResource.PRO);
    final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
    // zones de stockage oui/non
    boolean found = false;
    for (int i = 0; i < pro.nbProfils; i++) {
      if ((pro.profilsBief[i].nbPoints > 0)
          && ((Math.round(LidoResource.PRECISION * pro.profilsBief[i].absMajSto[0]) > Math
              .round(LidoResource.PRECISION * pro.profilsBief[i].abs[0])) || (Math
              .round(LidoResource.PRECISION * pro.profilsBief[i].absMajSto[1]) < Math
              .round(LidoResource.PRECISION
                  * pro.profilsBief[i].abs[pro.profilsBief[i].abs.length - 1])))) {
        found = true;
        break;
      }
    }
    if (found) {
      pro.zoneStock = 1;
    } else {
      pro.zoneStock = 0;
    }
    // seuil oui/non
    found = false;
    for (int i = 0; i < rzo.blocSings.ligneSing.length; i++) {
      if (rzo.blocSings.ligneSing[i].nSing > 0) {
        found = true;
        break;
      }
    }
    if (found) {
      cal.genCal.seuil = "OUI";
    } else {
      cal.genCal.seuil = "NON";
    }
    // xOrigi/xFin
    final double xori = ihmP_.getPH().CALCUL().getXOrigine();
    final double xfin = ihmP_.getPH().CALCUL().getXFin();
    if (((cal.genCal.biefXOrigi == 0.) && (cal.genCal.biefXFin == 0.))
        || ((cal.genCal.biefXOrigi < xori) || (cal.genCal.biefXFin > xfin))) {
      ihmP_.getPH().CALCUL().recalculeXOrigiXFin();
    }
    // variables temporelles NP
    if ("NP".equalsIgnoreCase(cal.genCal.regime.trim())) {
      if ((cal.temporel.tInit == 0.) && (cal.temporel.tMax == 0.)) {
        cal.temporel.tInit = ihmP_.getPH().LOICLM().getTempsInitial();
        cal.temporel.tMax = ihmP_.getPH().LOICLM().getTempsFinal();
      }
    }
  }

  private boolean verifieContraintesCalcul() {
    // serveur LIDO
    if (!isConnected()) {
      new BuDialogError(getApp(), isApp_, "vous n'etes pas connect� � un serveur LIDO!").activate();
      return false;
    }
    final SParametresCAL cal = (SParametresCAL) projet_.getParam(LidoResource.CAL);
    final SParametresPRO pro = (SParametresPRO) projet_.getParam(LidoResource.PRO);
    final SParametresLIG lig = (SParametresLIG) projet_.getParam(LidoResource.LIG);
    final SParametresRZO rzo = (SParametresRZO) projet_.getParam(LidoResource.RZO);
    // struct CAL
    // donnees CAL presentes
    if (!projet_.containsParam(LidoResource.CAL)) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "ERREUR: pas de donn�es g�n�rales (CAL)").activate();
      return false;
    }
    // struct RZO
    if (!projet_.containsParam(LidoResource.RZO)) {
      new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: pas de donn�es r�seau (RZO)")
          .activate();
      return false;
    }
    // struct PRO
    if (!projet_.containsParam(LidoResource.PRO)) {
      new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: pas de profils (PRO)")
          .activate();
      return false;
    }
    boolean permanent = "P".equalsIgnoreCase(cal.genCal.regime.trim());
    // application des auto-contraintes
    appliqueAutoContraintes();
    // struct SNG (si cal.seuil=OUI)
    if (("OUI".equalsIgnoreCase(cal.genCal.seuil.trim()))
        && (!projet_.containsParam(LidoResource.SNG))) {
      new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: pas de singularit�s (SNG)")
          .activate();
      return false;
    }
    // CL
    if (rzo.blocSitus.ligneSitu.length == 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "ERREUR: aucun bief n'a �t� d�fini (RZO)").activate();
      return false;
    }
    if (rzo.blocLims.ligneLim.length == 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "ERREUR: les conditions limites sont\n" + "ind�finies (RZO)").activate();
      return false;
    }
    for (int i = 0; i < rzo.blocLims.ligneLim.length; i++) {
      final SParametresCondLimBlocCLM loiclm = ihmP_.getPH().LIMITE().getLimite(rzo.blocLims.ligneLim[i]);
      if (loiclm == null) {
        new BuDialogError((BuCommonInterface) LidoApplication.FRAME,
            ((BuCommonInterface) LidoApplication.FRAME).getImplementation()
                .getInformationsSoftware(), "Aucune loi ne correspond � la limite d'extr�mit�\n"
                + rzo.blocLims.ligneLim[i].numExtBief + ".\n").activate();
        return false;
      }
      final int extremite = rzo.blocLims.ligneLim[i].numExtBief;
      final double abs = ihmP_.getPH().BIEF().getAbscisseExtremite(extremite);
      final SParametresBiefBlocPRO profil = ihmP_.getPH().PROFIL().getProfilByAbscisse(abs);
      if (profil == null) {
        new BuDialogError((BuCommonInterface) LidoApplication.FRAME,
            ((BuCommonInterface) LidoApplication.FRAME).getImplementation()
                .getInformationsSoftware(), "Aucun profil ne correspond � l'abscisse\n" + abs
                + ".\n" + "V�rifiez vos abscisses de biefs et\n" + "param�tres de calcul")
            .activate();
        return false;
      }
      final double cote = ihmP_.getPH().PROFIL().getFond(profil);
      boolean ok = true;
      switch (loiclm.typLoi) {
      case LidoResource.LIMITE.MAREE:
      case LidoResource.LIMITE.LIMNI:
        {
          ok = ihmP_.getPH().LOICLM().verifieCote(loiclm, cote);
          break;
        }
      case LidoResource.LIMITE.TARAGE:
        {
          if (!permanent) {
            ok = ihmP_.getPH().LOICLM().verifieCote(loiclm, cote);
          }
          break;
        }
      }
      if (!ok) {
        new BuDialogError((BuCommonInterface) LidoApplication.FRAME,
            ((BuCommonInterface) LidoApplication.FRAME).getImplementation()
                .getInformationsSoftware(), "La cote de la condition limite " + (i + 1) + " ("
                + loiclm.description + ")\n" + "n'est pas sup�rieure � la cote du profil\n"
                + "o� elle s'applique ! Vous devez corriger.").activate();
        return false;
      }
      if (!permanent) {
        ok = ihmP_.getPH().LOICLM().verifieTempsNP(loiclm);
        if (!ok) {
          new BuDialogError((BuCommonInterface) LidoApplication.FRAME,
              ((BuCommonInterface) LidoApplication.FRAME).getImplementation()
                  .getInformationsSoftware(), "Condition limite " + (i + 1) + " ("
                  + loiclm.description + "):\n" + "Les temps sont incorrects\n"
                  + "Ils doivent etre distincts\n" + "et croissants.").activate();
          return false;
        }
      }
    }
    // coherence du reseau
    // 1) recouvrement de biefs
    // 2) extremites libres non CLis�es
    // 3) biefs non reli�s au r�seau (aucune branch associ�e dans un noeud)
    // 4) tous les profils sont contenus dans 1 bief
    // 5) biefs "vides" (sans profils dedans)
    for (int i = 0; i < rzo.blocSitus.ligneSitu.length; i++) {
      if (ihmP_.getPH().BIEF().compteProfilsDansBief(rzo.blocSitus.ligneSitu[i]) == 0) {
        new BuDialogError(getApp(), getInformationsSoftware(), "ERREUR: le bief "
            + (rzo.blocSitus.ligneSitu[i].numBief + 1) +  CtuluLibString.LINE_SEP_SIMPLE + "ne contient aucun profil.")
            .activate();
        return false;
      }
    }
    // elements hors-bief
    final SParametresPerteLigneCLM[] pertesHorsBief = ihmP_.getPH().PERTE().getPertesHorsBief();
    if ((pertesHorsBief != null) && (pertesHorsBief.length > 0)) {
      String pertesStr = "";
      for (int i = 0; i < pertesHorsBief.length; i++) {
        pertesStr += pertesHorsBief[i].xPerte +  CtuluLibString.LINE_SEP_SIMPLE;
      }
      new BuDialogMessage((BuCommonInterface) LidoApplication.FRAME,
          ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
          "Les pertes aux abscisses:\n" + pertesStr + "sont hors-bief.").activate();
      return false;
    }
    final SParametresApportLigneCLM[] apportsHorsBief = ihmP_.getPH().APPORT().getApportsHorsBief();
    if ((apportsHorsBief != null) && (apportsHorsBief.length > 0)) {
      String apportsStr = "";
      for (int i = 0; i < apportsHorsBief.length; i++) {
        apportsStr += apportsHorsBief[i].xApport +  CtuluLibString.LINE_SEP_SIMPLE;
      }
      new BuDialogMessage((BuCommonInterface) LidoApplication.FRAME,
          ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
          "Les apports aux abscisses:\n" + apportsStr + "sont hors-bief.").activate();
      return false;
    }
    final SParametresBiefSingLigneRZO[] singsHorsBief = ihmP_.getPH().SINGULARITE()
        .getSingularitesHorsBief();
    if ((singsHorsBief != null) && (singsHorsBief.length > 0)) {
      String singsStr = "";
      for (int i = 0; i < singsHorsBief.length; i++) {
        singsStr += singsHorsBief[i].xSing +  CtuluLibString.LINE_SEP_SIMPLE;
      }
      new BuDialogMessage((BuCommonInterface) LidoApplication.FRAME,
          ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
          "Les singularit�s aux abscisses:\n" + singsStr + "sont hors-bief.").activate();
      return false;
    }
    final SParametresBiefBlocPRO[] profsHorsBief = ihmP_.getPH().PROFIL().getProfilsHorsBief();
    if ((profsHorsBief != null) && (profsHorsBief.length > 0)) {
      String profsStr = "";
      for (int i = 0; i < profsHorsBief.length; i++) {
        profsStr += profsHorsBief[i].abscisse +  CtuluLibString.LINE_SEP_SIMPLE;
      }
      new BuDialogMessage((BuCommonInterface) LidoApplication.FRAME,
          ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
          "Les profils aux abscisses:\n" + profsStr + "sont hors-bief.").activate();
      return false;
    }
    final SParametresSerieLigneCAL[] sects2HorsBief = ihmP_.getPH().PERMSECTION2().getSectionsHorsBief();
    if ((sects2HorsBief != null) && (sects2HorsBief.length > 0)) {
      String sects2Str = "";
      for (int i = 0; i < sects2HorsBief.length; i++) {
        sects2Str += "[" + sects2HorsBief[i].absDebBief + CtuluLibString.VIR + sects2HorsBief[i].absFinBief + "]"
            +  CtuluLibString.LINE_SEP_SIMPLE;
      }
      new BuDialogMessage((BuCommonInterface) LidoApplication.FRAME,
          ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
          "Les sections aux abscisses:\n" + sects2Str + "sont hors-bief.").activate();
      return false;
    }
    final SParametresSectionLigneCAL[] sects3HorsBief = ihmP_.getPH().PERMSECTION3()
        .getSectionsHorsBief();
    if ((sects3HorsBief != null) && (sects3HorsBief.length > 0)) {
      String sects3Str = "";
      for (int i = 0; i < sects3HorsBief.length; i++) {
        sects3Str += sects3HorsBief[i].absSect +  CtuluLibString.LINE_SEP_SIMPLE;
      }
      new BuDialogMessage((BuCommonInterface) LidoApplication.FRAME,
          ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
          "Les sections aux abscisses:\n" + sects3Str + "sont hors-bief.").activate();
      return false;
    }
    // verif des Stricklers
    String msg = "";
    for (int i = 0; i < pro.nbProfils; i++) {
      if (pro.profilsBief[i].coefStrickMajMin <= 0) {
        msg += (i + 1) + CtuluLibString.VIR;
      }
    }
    if (!"".equals(msg)) {
      msg = msg.substring(0, msg.length() - 1);
      new BuDialogError(getApp(), getInformationsSoftware(),
          "ERREUR: les profils suivants ont un Strickler\n" + "sur lit mineur n�gatif ou nul:\n"
              + msg).activate();
      return false;
    }
    if (!permanent) {
      if ((cal.temporel.tMax - cal.temporel.tInit) <= 0) {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "ERREUR: les param�tres temporels sont\n" + "incorrects. Tf<=Ti -> 0 pas de temps.")
            .activate();
        return false;
      }
      boolean ok = true;
      final double tMinClm = ihmP_.getPH().LOICLM().getTempsInitial();
      final double tMaxClm = ihmP_.getPH().LOICLM().getTempsFinal();
      if (tMinClm < tMaxClm) {
        ok = ok && (cal.temporel.tInit >= tMinClm) && (cal.temporel.tMax <= tMaxClm);
      }
      final double tMinSng = ihmP_.getPH().LOISNG().getTempsInitial();
      final double tMaxSng = ihmP_.getPH().LOISNG().getTempsFinal();
      if (tMinSng < tMaxSng) {
        ok = ok && (cal.temporel.tInit >= tMinSng) && (cal.temporel.tMax <= tMaxSng);
      }
      if (!ok) {
        new BuDialogError((BuCommonInterface) LidoApplication.FRAME,
            ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
            "L'intervalle de temps doit etre\n" + "inclus dans les temps des conditions\n"
                + "limites et des singularites!").activate();
        return false;
      }
      ok = ((cal.temporel.pas2TImp % cal.temporel.pas2T) == 0.);
      if (!ok) {
        new BuDialogError((BuCommonInterface) LidoApplication.FRAME,
            ((BuCommonInterface) LidoApplication.FRAME).getInformationsSoftware(),
            "Le pas de temps d'impression" + "doit etre un multiple du pas" + "de temps de calcul.")
            .activate();
        return false;
      }
      if ((lig == null) || (!lig.status )) {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "ERREUR: il n'y a pas de ligne d'eau\n" + "initiale. Vous devez en cr�er une en\n"
                + "lan�ant le calcul en Permanent.").activate();
        return false;
      }
      // r�cup�ration des �tudes d'avant v0.31
      // (des tableaux nulls font planter le transfert du lig sur le serveur)
      if (lig.zref == null) {
        lig.zref = new double[0];
      }
      if (lig.vmin == null) {
        lig.vmin = new double[0];
      }
      if (lig.vmaj == null) {
        lig.vmaj = new double[0];
      }
      if (lig.rgc == null) {
        lig.rgc = new double[0];
      }
      if (lig.rdc == null) {
        lig.rdc = new double[0];
      }
      if (lig.st1 == null) {
        lig.st1 = new double[0];
      }
      if (lig.st2 == null) {
        lig.st2 = new double[0];
      }
      if (lig.rmin == null) {
        lig.rmin = new double[0];
      }
      if (lig.rmaj == null) {
        lig.rmaj = new double[0];
      }
      if (lig.vol == null) {
        lig.vol = new double[0];
      }
      if (lig.vols == null) {
        lig.vols = new double[0];
      }
      if (lig.frou == null) {
        lig.frou = new double[0];
      }
    }
    return true;
  }

  private void gestionnaireImpression(final String _commande) {
    final JInternalFrame frame = getCurrentInternalFrame();
    EbliPageable target = null;
    if (frame instanceof EbliPageable) {
      target = (EbliPageable) frame;
    } else if (frame instanceof EbliFillePrevisualisation) {
      target = ((EbliFillePrevisualisation) frame)
          .getEbliPageable();
    } else {
      new BuDialogWarning(this, getInformationsSoftware(), FudaaLib
          .getS("Cette fen�tre n'est pas imprimable")).activate();
      return;
    }
    if ("IMPRIMER".equals(_commande)) {
      cmdImprimer(target);
    } else if ("MISEENPAGE".equals(_commande)) {
      cmdMiseEnPage(target);
    } else if ("PREVISUALISER".equals(_commande)) {
      cmdPrevisualisation(target);
    }
  }

  /**
   * Impression de la page <code>_target</code> dans un nouveau thread.
   */
  public void cmdImprimer(final EbliPageable _target) {
    final PrinterJob printJob = PrinterJob.getPrinterJob();
    final BuMainPanel mp = getMainPanel();
    printJob.setPageable(_target);
    if (printJob.printDialog()) {
      mp.setProgression(5);
      new BuTaskOperation(this, BuResource.BU.getString("Impression")) {

        public void act() {
          try {
            mp.setProgression(10);
            printJob.print();
            mp.setProgression(100);
          }
          catch (final Exception PrintException) {
            mp.setProgression(0);
            PrintException.printStackTrace();
          }
        }
      }.start();
    }
  }

  public void cmdMiseEnPage(final EbliPageable _target) {
    new EbliMiseEnPageDialog(_target, getApp(), getInformationsSoftware()).activate();
  }

  public void cmdPrevisualisation(final EbliPageable _target) {
    if (previsuFille_ == null) {
      previsuFille_ = new EbliFillePrevisualisation(getApp(), _target);
      addInternalFrame(previsuFille_);
    }
    else {
      previsuFille_.setEbliPageable(_target);
      if (previsuFille_.isClosed()) {
        addInternalFrame(previsuFille_);
      }
      else {
        activateInternalFrame(previsuFille_);
      }
    }
    try {
      previsuFille_.setMaximum(true);
    }
    catch (final java.beans.PropertyVetoException _e) {
      previsuFille_.setSize(100, 100);
    }
  }
  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return LidoPreferences.LIDO;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
  CONNEXION_LIDO=null;
  SERVEUR_LIDO=null;
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c=new FudaaDodicoTacheConnexion(SERVEUR_LIDO,CONNEXION_LIDO);
    return new FudaaDodicoTacheConnexion[]{c};
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[]{DCalculLido.class};
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
  final FudaaDodicoTacheConnexion c=(FudaaDodicoTacheConnexion)_r.get(DCalculLido.class);
  CONNEXION_LIDO=c.getConnexion();
  SERVEUR_LIDO=ICalculLidoHelper.narrow(c.getTache());
  }
}