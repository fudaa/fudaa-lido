/*
 * @file         LidoImport.java
 * @creation     1999-08-03
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JFileChooser;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuFileChooser;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluFileChooser;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresPRO;

import org.fudaa.dodico.lido.DParametresLido;
// ATTENTION: On ne gere ici que le cas de profils entres par points!!
// prevoir la gestion par largeurs ou transformer automatiquement en
// points au niveau de Dodico.
/**
 * @version      $Revision: 1.14 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public final class LidoImport {
  
  private LidoImport() {}
  public static SParametresBiefBlocPRO[] importProfilsTXT(final File filename) {
    final Vector profils= new Vector();
    BufferedReader fic= null;
    String line= null;
    String logMsg=
      "R�sultat de l'importation de " + filename.getPath() + ":\n\n";
    try {
      fic= new BufferedReader(new FileReader(filename));
      line= fic.readLine();
    } catch (final IOException e) {
      my_perror(e);
    }
    int lineNb= 0;
    String state= "debut";
    SParametresBiefBlocPRO nouveauProfil= null;
    //debut lecture
    while (line != null) {
      line= line.trim();
      lineNb++;
      //System.err.println("line "+lineNb+", state="+state); 
      if ((!line.equals("")) && (!line.startsWith("#"))) {
        if (state.equals("debut")) {
          System.err.println("Importation de " + line);
          logMsg += "Importation de " + line + "\n";
          nouveauProfil= new SParametresBiefBlocPRO();
          // nom du profil
          nouveauProfil.numProfil= line;
          state= "abscisse";
        } else {
          final String debLine = "' (ligne ";
          if (state.equals("abscisse")) {
            try {
              nouveauProfil.abscisse= Double.valueOf(line).doubleValue();
            } catch (final NumberFormatException e) {
              my_perror(
                "abscisse incorrecte: '" + line + debLine + lineNb + ")");
              logMsg += "abscisse incorrecte: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            }
            state= "nbPoints";
          } else if (state.equals("nbPoints")) {
            nouveauProfil.nbPoints= 0;
            // on va incrementer cette valeur jusqu'a abs.length
            int size= 0;
            try {
              size= Integer.valueOf(line).intValue();
            } catch (final NumberFormatException e) {
              my_perror(
                "nombre de points incorrect: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "nombre de points incorrect: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            }
            nouveauProfil.abs= new double[size];
            nouveauProfil.cotes= new double[size];
            if (size > 0) {
              state= "points";
            } else {
              state= "absMajMin"; // on saute les points (y'en a pas)
            }
          } else if (state.equals("points")) {
            final StringTokenizer tk= new StringTokenizer(line);
            try {
              nouveauProfil.abs[nouveauProfil.nbPoints]=
                Double.valueOf(tk.nextToken()).doubleValue();
              nouveauProfil.cotes[nouveauProfil.nbPoints]=
                Double.valueOf(tk.nextToken()).doubleValue();
            } catch (final NoSuchElementException e) {
              my_perror(
                "point mal d�fini: '"
                  + line
                  + debLine
                  + lineNb
                  + ")\n  (ou bien le nombre de points est inexact)");
              logMsg += "point mal d�fini: '"
                + line
                + debLine
                + lineNb
                + ")\n  (ou bien le nombre de points est inexact)\n";
              break;
            } catch (final NumberFormatException e) {
              my_perror(
                "point incorrect: '"
                  + line
                  + debLine
                  + lineNb
                  + ")\n  (ou bien le nombre de points est inexact)");
              logMsg += "point incorrect: '"
                + line
                + debLine
                + lineNb
                + ")\n  (ou bien le nombre de points est inexact)\n";
              break;
            }
            nouveauProfil.nbPoints++;
            if (nouveauProfil.nbPoints < nouveauProfil.abs.length) {
              state= "points";
            } else {
              // rajouter un point extremite si necessaire pour
              // que les deux bords du profil soient a la meme cote
              if (nouveauProfil.cotes.length > 0) {
                if (nouveauProfil.cotes[0]
                  < nouveauProfil.cotes[nouveauProfil.cotes.length - 1]) {
                  final double[] tmpCotes= new double[nouveauProfil.cotes.length + 1];
                  final double[] tmpAbs= new double[nouveauProfil.cotes.length + 1];
                  tmpCotes[0]=
                    nouveauProfil.cotes[nouveauProfil.cotes.length - 1];
                  tmpAbs[0]= nouveauProfil.abs[0];
                  for (int i= 0; i < nouveauProfil.cotes.length; i++) {
                    tmpCotes[i + 1]= nouveauProfil.cotes[i];
                    tmpAbs[i + 1]= nouveauProfil.abs[i];
                  }
                  nouveauProfil.cotes= tmpCotes;
                  nouveauProfil.abs= tmpAbs;
                  nouveauProfil.nbPoints++;
                } else if (
                  nouveauProfil.cotes[0]
                    > nouveauProfil.cotes[nouveauProfil.cotes.length - 1]) {
                  final double[] tmpCotes= new double[nouveauProfil.cotes.length + 1];
                  final double[] tmpAbs= new double[nouveauProfil.cotes.length + 1];
                  for (int i= 0; i < nouveauProfil.cotes.length; i++) {
                    tmpCotes[i]= nouveauProfil.cotes[i];
                    tmpAbs[i]= nouveauProfil.abs[i];
                  }
                  tmpCotes[nouveauProfil.cotes.length]= nouveauProfil.cotes[0];
                  tmpAbs[nouveauProfil.cotes.length]=
                    nouveauProfil.abs[nouveauProfil.cotes.length - 1];
                  nouveauProfil.cotes= tmpCotes;
                  nouveauProfil.abs= tmpAbs;
                  nouveauProfil.nbPoints++;
                }
              }
              state= "absMajMin";
            }
          } else if (state.equals("absMajMin")) {
            nouveauProfil.absMajMin= new double[2];
            final StringTokenizer tk= new StringTokenizer(line);
            try {
              nouveauProfil.absMajMin[0]=
                Double.valueOf(tk.nextToken()).doubleValue();
              nouveauProfil.absMajMin[1]=
                Double.valueOf(tk.nextToken()).doubleValue();
            } catch (final NoSuchElementException e) {
              my_perror(
                "abscisse de limite de lit mineur mal d�finie: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "abscisse de limite de lit mineur mal d�finie: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            } catch (final NumberFormatException e) {
              my_perror(
                "abscisse de limite de lit mineur incorrecte: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "abscisse de limite de lit mineur incorrecte: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            }
            state= "coteRiv";
          } else if (state.equals("coteRiv")) {
            final StringTokenizer tk= new StringTokenizer(line);
            try {
              nouveauProfil.coteRivGa=
                Double.valueOf(tk.nextToken()).doubleValue();
              nouveauProfil.coteRivDr=
                Double.valueOf(tk.nextToken()).doubleValue();
            } catch (final NoSuchElementException e) {
              my_perror(
                "cote de limite de lit mineur mal d�finie: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "cote de limite de lit mineur mal d�finie: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            } catch (final NumberFormatException e) {
              my_perror(
                "cote de limite de lit mineur incorrecte: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "cote de limite de lit mineur incorrecte: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            }
            state= "coefStrick";
          } else if (state.equals("coefStrick")) {
            final StringTokenizer tk= new StringTokenizer(line);
            try {
              nouveauProfil.coefStrickMajMin=
                Double.valueOf(tk.nextToken()).doubleValue();
              nouveauProfil.coefStrickMajSto=
                Double.valueOf(tk.nextToken()).doubleValue();
            } catch (final NoSuchElementException e) {
              my_perror(
                "coefficient de Strickler mal d�fini: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "coefficient de Strickler mal d�fini: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            } catch (final NumberFormatException e) {
              my_perror(
                "coefficient de Strickler incorrect: '"
                  + line
                  + debLine
                  + lineNb
                  + ")");
              logMsg += "coefficient de Strickler incorrect: '"
                + line
                + debLine
                + lineNb
                + ")\n";
              break;
            }
            state= "fin";
            continue; // ne pas lire la ligne suivante, sinon on la saute!...
          } else if (state.equals("fin")) {
            // on initialise les variables non d�finies dans le fichier
            nouveauProfil.indice= 0;
            nouveauProfil.absMajSto= new double[2];
            if (nouveauProfil.nbPoints > 0) {
              nouveauProfil.absMajSto[0]= nouveauProfil.abs[0];
              nouveauProfil.absMajSto[1]=
                nouveauProfil.abs[nouveauProfil.nbPoints - 1];
            }
            // A FAIRE: entree des points par largeur
            nouveauProfil.altMinMaj= new double[2];
            nouveauProfil.altMajSto= new double[2];
            profils.add(nouveauProfil);
            System.err.println("OK");
            logMsg += "OK\n";
            state= "debut";
          }
        }
      } // fin if(!line.equals(""))    
      try {
        line= fic.readLine();
      } catch (final IOException e) {
        my_perror(e);
      }
    }
    try {
      fic.close();
    } catch (final IOException e) {
      my_perror(e);
    }
    new BuDialogMessage(
      (BuCommonInterface)LidoApplication.FRAME,
      LidoImplementation.informationsSoftware(),
      logMsg)
      .activate();
    final SParametresBiefBlocPRO[] res= new SParametresBiefBlocPRO[profils.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresBiefBlocPRO)profils.get(i);
    }
    DParametresLido.normaliseProfils(res);
    return res;
  }
  public static SParametresBiefBlocPRO[] importProfilsPRO_LIDO(final File filename) {
    SParametresPRO pro= null;
    try {
      pro= DParametresLido.litParametresPRO(filename, 0);
    } catch (final Exception e) {
      my_perror(e);
      return null;
    }
    return pro.profilsBief;
  }
  public static SParametresBiefBlocPRO[] importProfilsPRO_RIVICAD(final File filename) {
    SParametresPRO pro= null;
    try {
      pro= DParametresLido.importationLIDO1_1(filename);
    } catch (final Exception e) {
      my_perror(e);
      return null;
    }
    if (pro == null) {
      return null;
    }
    return pro.profilsBief;
  }
  public static SParametresCondLimBlocCLM[] importCLM(final File filename) {
    SParametresCLM clm= null;
    try {
      clm= DParametresLido.litParametresCLM(filename);
    } catch (final Exception e) {
      my_perror(e);
      return null;
    }
    if (clm == null) {
      return new SParametresCondLimBlocCLM[0];
    }
    return clm.condLimites;
  }
  public static File chooseFile(final String _ext) {
    final String ext= _ext;
    File res= null;
    final BuFileChooser chooser= new CtuluFileChooser(false);
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
      public boolean accept(final java.io.File f) {
        return (ext == null)
          ? true
          : (f.getName().endsWith(CtuluLibString.DOT + ext) || f.isDirectory());
      }
      public String getDescription() {
        return (ext == null)
          ? "*.*"
          : ("Fichiers " + ext.toUpperCase() + " (*." + ext + ")");
      }
    });
    chooser.setCurrentDirectory(LidoResource.lastImportDir);
    final int returnVal= chooser.showOpenDialog(LidoApplication.FRAME);
    String filename= null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename= chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    final int indexSlash= filename.lastIndexOf(java.io.File.separatorChar);
    res= new File(filename);
    if (indexSlash > 0) {
      LidoResource.lastImportDir=
        new java.io.File(filename.substring(0, indexSlash));
    } else {
      LidoResource.lastImportDir= chooser.getCurrentDirectory();
    }
    return res;
  }
  private static void my_perror(final String msg) {
    System.err.println(msg);
  }
  private static void my_perror(final Exception e) {
    e.printStackTrace();
  }
}
