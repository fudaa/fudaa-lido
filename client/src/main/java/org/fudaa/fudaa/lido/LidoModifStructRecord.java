/*
 * @file         LidoModifStructRecord.java
 * @creation     2000-05-06
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresApportLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresPerteLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author        
 */
class LidoModifStructRecord implements FudaaParamListener {
  public static LidoModifStructRecord STRUCT_MODIF_RECORD=
    new LidoModifStructRecord();
  private Vector profilsAbscisses_;
  private Vector apports_;
  private Vector pertes_;
  private Vector loissng_;
  private Vector loisclm_;
  protected LidoModifStructRecord() {
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
  }
  public void reinit() {
    profilsAbscisses_= new Vector();
    apports_= new Vector();
    pertes_= new Vector();
    loissng_= new Vector();
    loisclm_= new Vector();
  }
  public String getProfilAbscissesMsg() {
    if ((profilsAbscisses_ == null) || (profilsAbscisses_.size() == 0)) {
      return null;
    }
    String msg= "";
    for (int i= 0; i < profilsAbscisses_.size() - 1; i++) {
      msg += (((Integer)profilsAbscisses_.get(i)).intValue() + 1) + ", ";
    }
    msg
      += (((Integer)profilsAbscisses_.get(profilsAbscisses_.size() - 1))
        .intValue()
        + 1)
      + CtuluLibString.DOT;
    return msg;
  }
  public String getApportsMsg() {
    if ((apports_ == null) || (apports_.size() == 0)) {
      return null;
    }
    String msg= "";
    for (int i= 0; i < apports_.size() - 1; i++) {
      msg += apports_.get(i) + ", ";
    }
    msg += apports_.get(apports_.size() - 1) + CtuluLibString.DOT;
    return msg;
  }
  public String getPertesMsg() {
    if ((pertes_ == null) || (pertes_.size() == 0)) {
      return null;
    }
    String msg= "";
    for (int i= 0; i < pertes_.size() - 1; i++) {
      msg += pertes_.get(i) + ", ";
    }
    msg += pertes_.get(pertes_.size() - 1) + CtuluLibString.DOT;
    return msg;
  }
  public String getLoissngMsg() {
    if ((loissng_ == null) || (loissng_.size() == 0)) {
      return null;
    }
    String msg= "";
    for (int i= 0; i < loissng_.size() - 1; i++) {
      msg += loissng_.get(i) + ", ";
    }
    msg += loissng_.get(loissng_.size() - 1) + CtuluLibString.DOT;
    return msg;
  }
  public String getLoisclmMsg() {
    if ((loisclm_ == null) || (loisclm_.size() == 0)) {
      return null;
    }
    String msg= "";
    for (int i= 0; i < loisclm_.size() - 1; i++) {
      msg += loisclm_.get(i) + ", ";
    }
    msg += loisclm_.get(loisclm_.size() - 1) + CtuluLibString.DOT;
    return msg;
  }
  // FudaaParamListener
  public void paramStructCreated(final FudaaParamEvent e) {
    paramStructModified(e);
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    paramStructModified(e);
  }
  public void paramStructModified(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    final String field= e.getField();
    if ((struct instanceof SParametresBiefBlocPRO)
      && ("abscisse".equals(field))) {
      final Integer newAbsMod= new Integer(((SParametresBiefBlocPRO)struct).indice);
      if (!profilsAbscisses_.contains(newAbsMod)) {
        profilsAbscisses_.add(newAbsMod);
      }
    } else if ((struct instanceof SParametresApportLigneCLM)) {
      final Double newApp= new Double(((SParametresApportLigneCLM)struct).xApport);
      if (!apports_.contains(newApp)) {
        apports_.add(newApp);
      }
    } else if ((struct instanceof SParametresPerteLigneCLM)) {
      final Double newPer= new Double(((SParametresPerteLigneCLM)struct).xPerte);
      if (!pertes_.contains(newPer)) {
        pertes_.add(newPer);
      }
    } else if ((struct instanceof SParametresSingBlocSNG)) {
      final String newSing= new String(((SParametresSingBlocSNG)struct).titre);
      if (!loissng_.contains(newSing)) {
        loissng_.add(newSing);
      }
    } else if ((struct instanceof SParametresCondLimBlocCLM)) {
      final String newLoi=
        new String(((SParametresCondLimBlocCLM)struct).description);
      if (!loisclm_.contains(newLoi)) {
        loisclm_.add(newLoi);
      }
    }
  }
}
