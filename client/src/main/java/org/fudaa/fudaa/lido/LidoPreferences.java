/*
 * @file         LidoPreferences.java
 * @creation     1999-12-03
 * @modification $Date: 2004-04-30 07:33:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import com.memoire.bu.BuPreferences;
/**
 * @version      $Revision: 1.7 $ $Date: 2004-04-30 07:33:18 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoPreferences extends BuPreferences {
  public final static LidoPreferences LIDO= new LidoPreferences();
}
