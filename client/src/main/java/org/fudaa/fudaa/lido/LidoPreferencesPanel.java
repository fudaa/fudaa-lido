/*
 * @file         LidoPreferencesPanel.java
 * @creation     2000-01-13
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import javax.swing.JCheckBox;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.ebli.commun.EbliPreferences;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPreferencesPanel extends BuAbstractPreferencesPanel {
  JCheckBox cbDialog_, cbStartup_, cbChargeRes_;
  BuTextField tfProfilPoint_, tfProfil_, tfTemps_;
  LidoImplementation appli_;
  public LidoPreferencesPanel(final LidoImplementation appli) {
    appli_= appli;
    final BuVerticalLayout lo= new BuVerticalLayout(5, true, false);
    setLayout(lo);
    int n= 0;
    cbDialog_= new JCheckBox("Fenetres int�rieures par d�faut");
    cbDialog_.setSelected(
      !"yes".equals(EbliPreferences.EBLI.getStringProperty("dialog.external")));
    add(cbDialog_, n++);
    cbStartup_= new JCheckBox("Ecran d'accueil au d�marrage");
    cbStartup_.setSelected(
      !"no".equals(LidoPreferences.LIDO.getStringProperty("startupmessage")));
    add(cbStartup_, n++);
    cbChargeRes_= new JCheckBox("Chargement des r�sultats dans les projets");
    cbChargeRes_.setSelected(
      !"no".equals(
        LidoPreferences.LIDO.getStringProperty("project.loadresults")));
    add(cbChargeRes_, n++);
    tfProfilPoint_= BuTextField.createIntegerField();
    tfProfilPoint_.setValue(new Integer(5));
    add(new BuLabel("Vitesse d�filement rapide des points de profils:"), n++);
    add(tfProfilPoint_, n++);
    tfProfil_= BuTextField.createIntegerField();
    tfProfil_.setValue(new Integer(5));
    add(new BuLabel("Vitesse d�filement rapide des profils:"), n++);
    add(tfProfil_, n++);
    tfTemps_= BuTextField.createIntegerField();
    tfTemps_.setValue(new Integer(5));
    add(new BuLabel("Vitesse d�filement rapide des pas de temps:"), n++);
    add(tfTemps_, n++);
  }
  public String getToolTipText() {
    return "Lido";
  }
  public String getTitle() {
    return "Lido";
  }
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    boolean nextBoot= false;
    String oldval= EbliPreferences.EBLI.getStringProperty("dialog.external");
    EbliPreferences.EBLI.putStringProperty(
      "dialog.external",
      (cbDialog_.isSelected() ? "no" : "yes"));
    nextBoot=
      nextBoot
        && (EbliPreferences
          .EBLI
          .getStringProperty("dialog.external")
          .equals(oldval));
    oldval= LidoPreferences.LIDO.getStringProperty("startupmessage");
    LidoPreferences.LIDO.putStringProperty(
      "startupmessage",
      (cbStartup_.isSelected() ? "yes" : "no"));
    nextBoot=
      nextBoot
        && (LidoPreferences
          .LIDO
          .getStringProperty("startupmessage")
          .equals(oldval));
    oldval= LidoPreferences.LIDO.getStringProperty("project.loadresults");
    LidoPreferences.LIDO.putStringProperty(
      "project.loadresults",
      (cbChargeRes_.isSelected() ? "yes" : "no"));
    nextBoot=
      nextBoot
        && (LidoPreferences
          .LIDO
          .getStringProperty("project.loadresults")
          .equals(oldval));
    Object val= tfProfilPoint_.getValue();
    LidoPreferences.LIDO.putStringProperty(
      "profil.point.rapide",
      (val == null ? "5" : ((Integer)val).toString()));
    val= tfProfil_.getValue();
    LidoPreferences.LIDO.putStringProperty(
      "profil.rapide",
      (val == null ? "5" : ((Integer)val).toString()));
    val= tfTemps_.getValue();
    LidoPreferences.LIDO.putStringProperty(
      "temps.rapide",
      (val == null ? "5" : ((Integer)val).toString()));
    LidoPreferences.LIDO.writeIniFile();
    EbliPreferences.EBLI.writeIniFile();
    if (nextBoot) {
      new BuDialogMessage(
        appli_.getApp(),
        appli_.getInformationsSoftware(),
        "Les changements seront pris en compte\n"
          + "au prochaine red�marrage de Lido")
        .activate();
    }
  }
  public boolean isPreferencesApplyable() {
    return false;
  }
  public void applyPreferences() {}
  public boolean isPreferencesCancelable() {
    return false;
  }
  public void cancelPreferences() {}
}
