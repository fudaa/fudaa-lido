/*
 * @file         LidoReseauException.java
 * @creation     1999-04-08
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author        
 */
class LidoReseauException extends Exception {
  LidoReseauException(final String msg) {
    super(msg);
  }
}
