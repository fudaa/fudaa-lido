/*
 * @file         LidoResource.java
 * @creation     1999-01-17
 * @modification $Date: 2004-04-30 07:33:18 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import com.memoire.bu.BuResource;
/**
 * @version      $Revision: 1.7 $ $Date: 2004-04-30 07:33:18 $ by $Author: deniger $
 * @author       Axel von Arnim , Mickael Rubens
 */
public class LidoResource extends BuResource {
  public final static LidoResource LIDO= new LidoResource();
  public final static String PRO= "PRO";
  public final static String RZO= "RZO";
  public final static String CAL= "CAL";
  public final static String CLM= "CLM";
  public final static String SNG= "SNG";
  public final static String EXT= "EXT";
  public final static String LIG= "LIG";
  public final static String ERN= "ERN";
  public final static String RSN= "RSN";
  public static LidoAssistant ASSISTANT= null;
  public final static int COULEURS[]=
    new int[] {
      0xCC0000,
      0x00CC00,
      0x0000CC,
      0xCCCC00,
      0x00CCCC,
      0xCC00CC,
      0xCC8800,
      0x88CC00,
      0x8800CC,
      0x88CC00,
      0xCC8800,
      0xCC0088,
      0xCC8888,
      0x88CC88,
      0x8888CC };
  public static java.io.File lastExportDir=
    new java.io.File(System.getProperty("user.dir"));
  public static java.io.File lastImportDir=
    new java.io.File(System.getProperty("user.dir"));
  public static final int PRECISION= 10000;
  public static final int FRACTIONDIGITS= 3;
  public static final class CONSTANTE {
    public final static double G= 9.81;
  }
  public static final class LIMITE {
    public final static int MAREE= 4;
    public final static int TARAGE= 3;
    public final static int LIMNI= 2;
    public final static int HYDRO= 1;
  }
  public static final class PROFIL {
    public final static int PROFILS= 1;
    public final static int CALAGE= 2;
  }
  public static final class RESULTAT {
    public final static String HAUTEUREAU= "HAUTEUREAU";
    public final static String LIGNEDEAU= "LIGNEDEAU";
    public final static String SECTIONMOUILLEE= "SECTIONMOUILLEE";
    public final static String RAYON= "RAYON";
    public final static String B1= "LARGEURMIROIR";
    public final static String VITESSE= "VITESSE";
    public final static String DEBITTOTAL= "DEBITTOTAL";
    public final static String FROUDE= "FROUDE";
    public final static String DEBIT= "DEBIT";
    public final static String CHARGE= "CHARGE";
    public final static String REGIMECRIT= "REGIMECRIT";
    public final static String REGIMEUNIF= "REGIMEUNIF";
    public final static String FORCETRAC= "FORCETRAC";
  }
  public static final class CALCUL {
    public final static String REGIME_PERM= "permanent";
    public final static String REGIME_NONPERM= "non permanent";
    public final static String CODE_STR_LIDO= "LIDO";
    public final static String CODE_STR_SARA= "SARA";
    public final static String CODE_STR_REZO= "REZO";
    public final static String LIT_STR_MINMAJ= "Mineur/Majeur";
    public final static String LIT_STR_FONBER= "Fond/Berge";
  }
  public static final class PLANIMETRAGE {
    public final static int NBVALEURS_PLANI= 50;
  }
  public static final class BIEF {
    public final static double DBIEF= 1.;
  }
  public static final class SINGULARITE {
    public final static int ITYPE= 0;
    //indice du type de sing dans le tabParamEntier
    public final static int SEUIL_NOYE= 1;
    public final static int SEUIL_DENOYE= 2;
    public final static int SEUIL_GEOM= 3;
    public final static int LIMNI_AMONT= 4;
    public final static int TARAGE_AMONT= 5;
    public final static int TARAGE_AVAL= 7;
    public final static int DEBIT= 0x01; // identifie la variable "debit"
    public final static int COTEAVAL= 0x02; // identifie "code aval"
    public static final class ICOURBE {
      public final static int INBPOINT= 1;
      public final static int IHT= 0; // TARAGE: cotes
      public final static int IHL= 1; // LIMNI: cotes
      public final static int IQT= 1; // TARAGE: debits
      public final static int ITL= 0; // LIMNI: temps
      public final static int IYS= 0; // SEUIL: abs crete
      public final static int IZS= 1; // SEUIL: cotes crete
      public final static int IVARMAXCOT= 0; // LIMNI: variation max cote
      public final static int ICOTMOY= 0;
      public final static int ICOEFDEBIT= 1; // SEUIL: coeff debit
    }
    public static final class ISEUIL_DENOYE {
      public final static int ICOTAMONT= 1;
      public final static int IDEBIT= 0;
      public final static int INBDEBIT= 1;
      public final static int ICOTMOY= 0;
    }
    public static final class ISEUIL_NOYE {
      public final static int ICOTAVAL= 0;
      public final static int IDEBIT= 1;
      public final static int INBCOTAVAL= 1;
      public final static int INBDEBIT= 2;
      public final static int ICOTMOY= 0;
      public final static int ICOTAMONTOFFSET= 2;
    }
  }
  public static final class VISU_REZO {
    public final static int TAILLE_BIEFS= 100;
  }
  public static final class VISU_BIEF {
    public final static int DROITE= 0x01;
    public final static int GAUCHE= 0x02;
  }
}
