/*
 * @file         LidoDnDTransfer.java
 * @creation     1999-12-17
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.dnd;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoDnDTransfer implements Transferable {
  private LidoDnDTransferParam param_;
  public LidoDnDTransfer(final Object o, final String f) {
    param_= new LidoDnDTransferParam(o, f);
  }
  public Object getTransferData(final DataFlavor flavor) {
    if (isDataFlavorSupported(flavor)) {
      return param_;
    }
    return null;
  }
  public DataFlavor[] getTransferDataFlavors() {
    return new DataFlavor[] {
       new DataFlavor(
        param_.getParam().getClass(),
        DataFlavor.javaJVMLocalObjectMimeType)};
  }
  public boolean isDataFlavorSupported(final DataFlavor flavor) {
    return param_.getParam().getClass().equals(flavor.getRepresentationClass());
  }
}
