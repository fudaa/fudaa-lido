/*
 * @file         LidoDnDTransferParam.java
 * @creation     1999-12-17
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.dnd;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoDnDTransferParam implements java.io.Serializable {
  private Object object_;
  private String field_;
  public LidoDnDTransferParam(final Object o, final String f) {
    object_= o;
    field_= f;
  }
  public Object getParam() {
    return object_;
  }
  public String getField() {
    return field_;
  }
}
