/*
 * @file         LidoBiefEditor.java
 * @creation     1999-04-27
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoBiefEditor extends LidoCustomizer implements ActionListener {
  BuTextField tfNumero_, tfPProfil_, tfDProfil_, tfExtrAmont_, tfExtrAval_;
  BuButton btScinder_;
  BuPanel pnEdition_, pnBief_, pnScinder_;
  BorderLayout loBief_;
  BuGridLayout loEdition_;
  private SParametresBiefSituLigneRZO bief_, biefBck_;
  public LidoBiefEditor() {
    this(null);
  }
  public LidoBiefEditor(final BDialogContent parent) {
    super(parent, "Edition de bief");
    bief_= null;
    biefBck_= null;
    loEdition_= new BuGridLayout(2, 5, 5, false, false);
    loBief_= new BorderLayout();
    final Container pnMain_= getContentPane();
    pnEdition_= new BuPanel();
    pnEdition_.setLayout(loEdition_);
    pnEdition_.setBorder(new EmptyBorder(new Insets(0, 5, 0, 0)));
    pnBief_= new BuPanel();
    pnBief_.setLayout(loBief_);
    pnBief_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    pnScinder_= new BuPanel();
    final int textSize= 10;
    int n= 0;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(false);
    pnEdition_.add(new BuLabel("Num�ro du Bief"), n++);
    pnEdition_.add(tfNumero_, n++);
    tfPProfil_= BuTextField.createDoubleField();
    tfPProfil_.setColumns(textSize);
    pnEdition_.add(new BuLabel("Abscisse premier profil"), n++);
    pnEdition_.add(tfPProfil_, n++);
    tfDProfil_= BuTextField.createDoubleField();
    tfDProfil_.setColumns(textSize);
    pnEdition_.add(new BuLabel("Abscisse dernier profil"), n++);
    pnEdition_.add(tfDProfil_, n++);
    tfExtrAmont_= BuTextField.createIntegerField();
    tfExtrAmont_.setColumns(textSize);
    pnEdition_.add(new BuLabel("Extr�mit� amont"), n++);
    pnEdition_.add(tfExtrAmont_, n++);
    tfExtrAval_= BuTextField.createIntegerField();
    tfExtrAval_.setColumns(textSize);
    pnEdition_.add(new BuLabel("Extr�mit� aval"), n++);
    pnEdition_.add(tfExtrAval_, n++);
    pnBief_.add(pnEdition_, BorderLayout.CENTER);
    btScinder_= new BuButton("Scinder");
    btScinder_.setName("btSCINDER");
    btScinder_.setActionCommand("SCINDER");
    btScinder_.addActionListener(this);
    pnScinder_.add(btScinder_);
    pnBief_.add(pnScinder_, BorderLayout.SOUTH);
    pnMain_.add(pnBief_, BorderLayout.CENTER);
    setNavPanel(EbliPreferences.DIALOG.VALIDER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      final SParametresBiefSituLigneRZO vp= bief_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, bief_);
      }
      fermer();
    } else if ("SCINDER".equals(cmd)) {
      fermer();
      firePropertyChange("scission", null, bief_);
    }
  }
  // LidoCustomizer
  public void setObject(final Object _b) {
    if (!(_b instanceof SParametresBiefSituLigneRZO)) {
      return;
    }
    if (_b == bief_) {
      return;
    }
    bief_= (SParametresBiefSituLigneRZO)_b;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        bief_,
        "bief " + (bief_.numBief));
  }
  public boolean restore() {
    if (biefBck_ == null) {
      return false;
    }
    bief_.numBief= biefBck_.numBief;
    bief_.x1= biefBck_.x1;
    bief_.x2= biefBck_.x2;
    bief_.branch1= biefBck_.branch1;
    bief_.branch2= biefBck_.branch2;
    biefBck_= null;
    return true;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (bief_ == null) {
      bief_= new SParametresBiefSituLigneRZO();
      biefBck_= null;
    } else {
      biefBck_=
        new SParametresBiefSituLigneRZO(
          bief_.x1,
          bief_.x2,
          bief_.numBief,
          bief_.branch1,
          bief_.branch2);
    }
    final int nnum= ((Integer)tfNumero_.getValue()).intValue();
    if (nnum != bief_.numBief) {
      bief_.numBief= nnum;
      changed= true;
    }
    final double nx1= ((Double)tfPProfil_.getValue()).doubleValue();
    if (nx1 != bief_.x1) {
      bief_.x1= nx1;
      changed= true;
    }
    final double nx2= ((Double)tfDProfil_.getValue()).doubleValue();
    if (nx2 != bief_.x2) {
      bief_.x2= nx2;
      changed= true;
    }
    final int nb1= ((Integer)tfExtrAmont_.getValue()).intValue();
    if (nb1 != bief_.branch1) {
      bief_.branch1= nb1;
      changed= true;
    }
    final int nb2= ((Integer)tfExtrAval_.getValue()).intValue();
    if (nb2 != bief_.branch2) {
      bief_.branch2= nb2;
      changed= true;
    }
    return changed;
  }
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(bief_.numBief));
    tfPProfil_.setValue(new Double(bief_.x1));
    tfDProfil_.setValue(new Double(bief_.x2));
    tfExtrAmont_.setValue(new Integer(bief_.branch1));
    tfExtrAval_.setValue(new Integer(bief_.branch2));
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == bief_);
  }
}
