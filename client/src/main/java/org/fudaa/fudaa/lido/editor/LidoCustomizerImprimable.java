/*
 * @file         LidoCustomizerImprimable.java
 * @creation     2001-12-05
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JComponent;

import com.memoire.bu.BuCommonInterface;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.impression.EbliPageableDelegate;
/**
 * @version      $Id: LidoCustomizerImprimable.java,v 1.9 2006-09-19 15:05:00 deniger Exp $
 * @author       Fred Deniger
 */
public abstract class LidoCustomizerImprimable
  extends LidoCustomizer
  implements EbliPageable {
  private EbliPageableDelegate delegueImpression_;
  protected LidoCustomizerImprimable(final String _title) {
    super(_title);
    initImpression();
  }
  protected LidoCustomizerImprimable(final String _title, final JComponent _message) {
    super(_title, _message);
    initImpression();
  }
  protected LidoCustomizerImprimable(final BDialogContent _parent, final String _title) {
    super(_parent, _title);
    initImpression();
  }
  protected LidoCustomizerImprimable(
    final BuCommonInterface app,
    final BDialogContent _parent,
    final String _title) {
    super(app, _parent, _title);
    initImpression();
  }
  protected LidoCustomizerImprimable(
    final BDialogContent _parent,
    final String _title,
    final JComponent _message) {
    super(_parent, _title, _message);
    initImpression();
  }
  private  void initImpression() {
    delegueImpression_= new EbliPageableDelegate(this);
  }
  public Printable getPrintable(final int _i) {
    return delegueImpression_.getPrintable(_i);
  }
  public PageFormat getPageFormat(final int _i) {
    return delegueImpression_.getPageFormat(_i);
  }
  public EbliPageFormat getDefaultEbliPageFormat() {
    return delegueImpression_.getDefaultEbliPageFormat();
  }
}
