/*
 * @file         LidoDialogTableau.java
 * @creation     1999-08-12
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.print.PageFormat;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuResource;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BDialogContentImprimable;
import org.fudaa.ebli.impression.EbliPrinter;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.tableau.LidoTableauBase;
/**
 * @version      $Id: LidoDialogTableau.java,v 1.9 2006-09-19 15:05:00 deniger Exp $
 * @author       Axel von Arnim 
 */
public class LidoDialogTableau extends BDialogContentImprimable {
  JTable table_;
  FudaaProjet p_;
  public LidoDialogTableau(final JTable table, final String _title, final FudaaProjet _p) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      BuResource.BU.getString(_title),
      null);
    p_= _p;
    table_= table;
    init();
  }
  public LidoDialogTableau(
    final BDialogContent parent,
    final JTable table,
    final String _title,
    final FudaaProjet _p) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      parent,
      BuResource.BU.getString(_title),
      null);
    p_= _p;
    table_= table;
    init();
  }
  private void init() {
    setClosable(false);
    if (table_ == null) {
      System.err.println(
        "LidoDialogTableau: Warning: passing null table to constructor");
    }
    table_.setColumnSelectionAllowed(false);
    if (table_ instanceof WindowListener) {
      ((BDialog)getDialog()).addWindowListener((WindowListener)table_);
    }
    final JComponent mct= (JComponent)getContentPane();
    final JScrollPane spm= new JScrollPane(table_);
    spm.setBorder(
      new CompoundBorder(
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)),
        spm.getBorder()));
    spm.setPreferredSize(
      new Dimension(table_.getPreferredSize().width + 30, 400));
    mct.add(BorderLayout.CENTER, spm);
  }
  public JTable getTable() {
    return table_;
  }
  public void fermer() {
    if (!isShowing()) {
      return;
    }
    if ((table_ != null) && (table_ instanceof LidoTableauBase)) {
      ((LidoTableauBase)table_).trie();
      if (!((LidoTableauBase)table_).verifieContraintes()) {
        return;
      }
    }
    super.fermer();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    if (("FERMER".equals(cmd)) || ("EDITER".equals(cmd))) {
      if ((table_ != null) && table_.isEditing()) {
        System.err.println("Table revalidate");
        table_.editingStopped(new ChangeEvent(this));
      }
    } else if ("SUPPRIMER".equals(cmd)) {
      if ((table_ != null) && table_.isEditing()) {
        System.err.println("Table editing canceled");
        table_.editingCanceled(new ChangeEvent(this));
      }
    }
    super.actionPerformed(_evt);
  }
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    return EbliPrinter.printTable(_g, _format, table_, _numPage);
  }
  public int getNumberOfPages() {
    return 1;
  }
  public BuInformationsDocument getInformationsDocument() {
    return p_.getInformationsDocument();
  }
}
