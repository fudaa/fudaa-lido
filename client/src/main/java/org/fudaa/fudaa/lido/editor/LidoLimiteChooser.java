/*
 * @file         LidoLimiteChooser.java
 * @creation     1999-10-09
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;

import org.fudaa.fudaa.lido.LidoApplication;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoLimiteChooser extends BDialog {
  public static final int PERMANENT_LIM= 1;
  public static final int PERMANENT_APP= 2;
  public static final int NON_PERMANENT= 3;
  JRadioButton btTarage_, btMaree_, btLimni_, btHydro_;
  BuGridLayout loBoutons_;
  int permanent_;
  public LidoLimiteChooser(final int permanent) {
    super((BuCommonInterface)LidoApplication.FRAME);
    setTitle("Loi hydraulique");
    setLocationRelativeTo(LidoApplication.FRAME);
    permanent_= permanent;
    init();
  }
  public LidoLimiteChooser(final IDialogInterface parent, final int permanent) {
    super((BuCommonInterface)LidoApplication.FRAME, parent);
    setTitle("Loi hydraulique");
    setLocationRelativeTo(parent.getComponent());
    permanent_= permanent;
    init();
  }
  private void init() {
    loBoutons_= new BuGridLayout(2, 5, 5, true, true);
    loBoutons_.setCfilled(false);
    final Container content= getContentPane();
    content.setLayout(loBoutons_);
    int n= 0;
    if (permanent_ == NON_PERMANENT) {
      btTarage_= new JRadioButton("Tarage");
      btTarage_.setActionCommand("LIMI:TARAGE");
      content.add(btTarage_, n++);
      btMaree_= new JRadioButton("Mar�e");
      btMaree_.setActionCommand("LIMI:MAREE");
      content.add(btMaree_, n++);
    }
    if (permanent_ != PERMANENT_APP) {
      btLimni_= new JRadioButton("Limnigramme");
      btLimni_.setActionCommand("LIMI:LIMNI");
      content.add(btLimni_, n++);
    }
    btHydro_= new JRadioButton("Hydrogramme");
    btHydro_.setActionCommand("LIMI:HYDRO");
    content.add(btHydro_, n++);
    pack();
  }
  public void addActionListener(final ActionListener l) {
    if (permanent_ == NON_PERMANENT) {
      btTarage_.addActionListener(l);
      btMaree_.addActionListener(l);
    }
    btHydro_.addActionListener(l);
    if (permanent_ != PERMANENT_APP) {
      btLimni_.addActionListener(l);
    }
  }
  public void removeActionListener(final ActionListener l) {
    if (permanent_ == NON_PERMANENT) {
      btTarage_.removeActionListener(l);
    }
    btHydro_.removeActionListener(l);
    if (permanent_ != PERMANENT_APP) {
      btLimni_.removeActionListener(l);
    }
  }
}
