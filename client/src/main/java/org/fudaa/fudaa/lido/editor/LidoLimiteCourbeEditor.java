/*
 * @file         LidoLimiteCourbeEditor.java
 * @creation     1999-08-26
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheLimiteCourbe;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauAbstract;
/**
 * @version      $Id: LidoLimiteCourbeEditor.java,v 1.11 2006-09-19 15:05:00 deniger Exp $
 * @author       Axel von Arnim
 */
public class LidoLimiteCourbeEditor extends LidoCustomizerImprimable {
  public final static int TARAGE= LidoResource.LIMITE.TARAGE;
  public final static int LIMNI= LidoResource.LIMITE.LIMNI;
  public final static int HYDRO= LidoResource.LIMITE.HYDRO;
  SParametresCondLimBlocCLM cl_;
  // commun
  private BuButton btCreer_, btSupprimer_, btSupprimerLoi_, btDeselectionner_;
  private BuTextField tfNomLoi_;
  // specifique
  //private BuTextField tfSpecific_;
  private DebitTable tbDebit_;
  private LidoParamsHelper ph_;
  private LidoGrapheLimiteCourbe graphe_;
  int type_;
  private String TAB_LABEL;
  String TAB_HEADER_COL0;
  String TAB_HEADER_COL1;
  private String GRA_TITLE;
  private String GRA_ABS_LABEL;
  private String GRA_ORD_LABEL;
  private String GRA_ABS_UNITE;
  private String GRA_ORD_UNITE;
  public LidoLimiteCourbeEditor(final int type, final LidoParamsHelper ph) {
    super(
      "Edition de Limite : "
        + (type == TARAGE
          ? "tarage"
          : type == LIMNI
          ? "limnigramme"
          : type == HYDRO
          ? "hydrogramme"
          : null));
    type_= type;
    ph_= ph;
    init();
  }
  public LidoLimiteCourbeEditor(
    final BDialogContent parent,
    final int type,
    final LidoParamsHelper ph) {
    super(
      parent,
      "Edition de Limite : "
        + (type == TARAGE
          ? "tarage"
          : type == LIMNI
          ? "limnigramme"
          : type == HYDRO
          ? "hydrogramme"
          : null));
    type_= type;
    ph_= ph;
    init();
  }
  private void init() {
    if (type_ == TARAGE) {
      TAB_LABEL= "Courbe de tarage";
      TAB_HEADER_COL0= "Z eau (m)";
      TAB_HEADER_COL1= "D�bit Q (m3/s)";
      GRA_TITLE= "tarage";
      GRA_ABS_LABEL= "Z";
      GRA_ORD_LABEL= "Q";
      GRA_ABS_UNITE= "m";
      GRA_ORD_UNITE= "m3/s";
    } else if (type_ == LIMNI) {
      TAB_LABEL= "Limnigramme";
      TAB_HEADER_COL0= "Temps (s)";
      TAB_HEADER_COL1= "Z eau (m)";
      GRA_TITLE= "limni";
      GRA_ABS_LABEL= "T";
      GRA_ORD_LABEL= "Z";
      GRA_ABS_UNITE= "s";
      GRA_ORD_UNITE= "m";
    } else if (type_ == HYDRO) {
      TAB_LABEL= "Hydrogramme";
      TAB_HEADER_COL0= "Temps (s)";
      TAB_HEADER_COL1= "D�bit Q (m3/s)";
      GRA_TITLE= "hydro";
      GRA_ABS_LABEL= "T";
      GRA_ORD_LABEL= "Q";
      GRA_ABS_UNITE= "s";
      GRA_ORD_UNITE= "m3/s";
    }
    final Container content= getContentPane();
    //-------- Edition ----------//
    int n= 0;
    //-------------------------
    //BuPanel pn1=new BuPanel();
    //pn1.setLayout(new BuGridLayout(2, INSETS_SIZE, INSETS_SIZE, false, false));
    //n=0;
    //tfAbs_=BuTextField.createDoubleField();
    //tfAbs_.setEditable(false);
    //tfAbs_.setColumns(8);
    //pn1.add(new BuLabel("Abscisse de la singularit�"), n++);
    //pn1.add(tfAbs_, n++);
    //-------------------------
    //-------------------------
    final BuPanel pn2= new BuPanel();
    pn2.setLayout(new BuGridLayout(1, INSETS_SIZE, INSETS_SIZE, false, false));
    n= 0;
    // case 0:0
    final BuPanel pnDebitTable= new BuPanel();
    pnDebitTable.setBorder(new LineBorder(Color.black));
    pnDebitTable.setLayout(new BuVerticalLayout(0, false, false));
    pnDebitTable.add(new BuLabelMultiLine(TAB_LABEL), 0);
    tbDebit_= new DebitTable();
    addPropertyChangeListener(tbDebit_);
    pnDebitTable.add(tbDebit_.getTableHeader(), 1);
    pnDebitTable.add(tbDebit_, 2);
    pn2.add(pnDebitTable, n++);
    final JScrollPane sp2= new JScrollPane(pn2);
    sp2.setBorder(
      new CompoundBorder(
        new BevelBorder(BevelBorder.LOWERED),
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE))));
    sp2.setPreferredSize(new Dimension(200, 150));
    //-------------------------
    //-------------------------
    final BuPanel pn3= new BuPanel();
    btCreer_= new BuButton();
    btCreer_.setToolTipText("Ins�rer");
    btCreer_.setIcon(BuResource.BU.getIcon("creer"));
    btCreer_.setActionCommand("CREER");
    btCreer_.addActionListener(this);
    pn3.add(btCreer_);
    btSupprimer_= new BuButton();
    btSupprimer_.setToolTipText("supprimer");
    btSupprimer_.setIcon(BuResource.BU.getIcon("detruire"));
    btSupprimer_.setActionCommand("SUPPRIMER");
    btSupprimer_.addActionListener(this);
    pn3.add(btSupprimer_);
    btDeselectionner_= new BuButton();
    btDeselectionner_.setToolTipText("d�s�lectionner tout");
    btDeselectionner_.setIcon(BuResource.BU.getIcon("texte"));
    btDeselectionner_.setActionCommand("TOUTDESELECTIONNER");
    btDeselectionner_.addActionListener(this);
    pn3.add(btDeselectionner_);
    //-------------------------
    //-------------------------
    final BuPanel pn4= new BuPanel();
    btSupprimerLoi_= new BuButton("Supprimer cette loi");
    btSupprimerLoi_.setActionCommand("SUPPRIMERLOI");
    btSupprimerLoi_.addActionListener(this);
    pn4.add(btSupprimerLoi_);
    //-------------------------
    final BuPanel pnEdition= new BuPanel();
    pnEdition.setLayout(new BuVerticalLayout(INSETS_SIZE, false, false));
    tfNomLoi_= new BuTextField();
    tfNomLoi_.setPreferredSize(
      new Dimension(
        sp2.getPreferredSize().width,
        tfNomLoi_.getPreferredSize().height));
    pnEdition.add(new BuLabel("Nom de la loi : "));
    pnEdition.add(tfNomLoi_);
    //pnEdition.add(pn1);
    pnEdition.add(sp2);
    pnEdition.add(pn3);
    pnEdition.add(pn4);
    pnEdition.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    //-------- Graphe ----------//
    graphe_=
      new LidoGrapheLimiteCourbe(
        type_,
        GRA_TITLE,
        GRA_ABS_LABEL,
        GRA_ORD_LABEL,
        GRA_ABS_UNITE,
        GRA_ORD_UNITE);
    graphe_.setInteractif(true);
    tbDebit_.addPropertyChangeListener(graphe_);
    addPropertyChangeListener(graphe_);
    graphe_.setPreferredSize(new Dimension(320, 240));
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ALIGN_RIGHT);
    content.add(BorderLayout.WEST, pnEdition);
    content.add(BorderLayout.CENTER, graphe_);
    pack();
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (tbDebit_.isEditing()) {
        System.err.println("Table validate");
        tbDebit_.editingStopped(new ChangeEvent(this));
      }
      if (getValeurs()) {
        objectModified();
      }
      firePropertyChange("limite", null, cl_);
      firePropertyChange("valider", null, cl_);
      if (verifieContraintes()) {
        if (ph_.LOICLMPOINT_COMPARATOR(cl_.typLoi) == null) {
          System.out.println("Probleme");
        }
        java.util.Arrays.sort(
          cl_.point,
          ph_.LOICLMPOINT_COMPARATOR(cl_.typLoi));
        fermer();
      }
    } else      /*if( "NBPOINT".equals(cmd) ) {
        if( getValeurs() ) firePropertyChange("limite", null, cl_);
      } else*/
      if ("CREER".equals(cmd)) {
        if (tbDebit_ == null) {
          return;
        }
        getValeurs();
        int pos[]= tbDebit_.getSelectedRows();
        if (pos.length == 0) {
          pos= new int[] { -1 };
        }
        ph_.LOICLM().nouveauPoint(cl_, pos[0]);
        setObjectModified(true);
        firePropertyChange("limite", null, cl_);
      } else if ("SUPPRIMER".equals(cmd)) {
        if (tbDebit_ == null) {
          return;
        }
        ph_.LOICLM().supprimePoints(cl_, tbDebit_.getSelectedRows());
        setObjectModified(true);
        firePropertyChange("limite", null, cl_);
      } else if ("TOUTDESELECTIONNER".equals(cmd)) {
        tbDebit_.clearSelection();
        tbDebit_.editingStopped(new ChangeEvent(this));
      } else if ("SUPPRIMERLOI".equals(cmd)) {
        ph_.LOICLM().supprimeLoiclm(cl_);
        cl_= null;
        objectDeleted();
        //firePropertyChange("loisupprimee", old, null);
        fermer();
      }
  }
  private boolean verifieContraintes() {
    return true;
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresCondLimBlocCLM)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    final SParametresCondLimBlocCLM vp= cl_;
    cl_= (SParametresCondLimBlocCLM)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        cl_,
        "loiclm " + (cl_.numCondLim));
    firePropertyChange("limite", vp, cl_);
  }
  public boolean restore() {
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    /*    int newNbDebit=((Integer)tfNbPoint_.getValue()).intValue();
        int newNbCotAval=((Integer)tfNbCotAval_.getValue()).intValue();
    
        if( cl_.tabParamEntier[INBPOINT]!=newNbDebit ) {
          cl_.tabParamEntier[INBPOINT]=newNbDebit;
          changed=true;
        }
        if( cl_.tabParamEntier[INBCOTAVAL]!=newNbCotAval ) {
          cl_.tabParamEntier[INBCOTAVAL]=newNbCotAval;
          changed=true;
        }*/
    /*if( type_==SEUIL_GEOM ) {
      double newCoefDebit=((Double)tfSpecific_.getValue()).doubleValue();
      if( cl_.tabParamReel[ICOEFDEBIT]!=newCoefDebit ) {
        cl_.tabParamReel[ICOEFDEBIT]=newCoefDebit;
        changed=true;
      }
    }
    if( type_==LIMNI_AMONT ) {
      double newVarMaxCot=((Double)tfSpecific_.getValue()).doubleValue();
      if( cl_.tabParamReel[IVARMAXCOT]!=newVarMaxCot ) {
        cl_.tabParamReel[IVARMAXCOT]=newVarMaxCot;
        changed=true;
      }
    }*/
    String nom= tfNomLoi_.getText();
    if ((nom == null) || (nom.trim().equals(""))) {
      nom= "LOI LIMITE";
    }
    if (!nom.equals(cl_.description)) {
      changed= true;
      cl_.description= nom;
    }
    return changed;
    /*    if( cl_==null ) {
          clBck_=null;
        } else {
          clBck_=new SParametresBiefSingLigneRZO();
          clBck_.numBief=cl_.numBief;
          clBck_.xSing=cl_.xSing;
          clBck_.nSing=cl_.nSing;
        }*/
  }
  protected void setValeurs() {
    //tfAbs_.setValue(new Double(0.));  // ?? A VOIR: aller la chercher dans RZO?
    /*if( cl_.tabParamReel.length<=0 ) {
      System.err.println("LidoLimiteCourbeEditor : tabParamReel vide");
      return;
    }
    if( type_==SEUIL_GEOM ) {
      tfSpecific_.setValue(new Double(cl_.tabParamReel[ICOEFDEBIT]));
    }
    if( type_==LIMNI_AMONT ) {
      tfSpecific_.setValue(new Double(cl_.tabParamReel[IVARMAXCOT]));
    }*/
    tfNomLoi_.setText(cl_.description);
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    return graphe_.print(_g, _format, _page);
  }
  public int getNumberOfPages() {
    return 1;
  }
  public BuInformationsDocument getInformationsDocument() {
    return ph_.getInformationsDocument();
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
  class DebitTable
    extends LidoTableauAbstract
    implements PropertyChangeListener {
    public DebitTable() {
      super();
      setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
      reinitialise();
    }
    public void propertyChange(final PropertyChangeEvent e) {
      if ("limite".equals(e.getPropertyName())) {
        reinitialise();
      }
    }
    public void limiteChanged() {
      this.firePropertyChange("limite", null, cl_);
    }
    private void reinitialise() {
      if ((cl_ == null) || (cl_.point == null) || (cl_.point.length == 0)) {
        return;
      }
      if (isEditing()) {
        System.err.println("canceling edition");
        editingCanceled(new ChangeEvent(this));
      }
      clearSelection();
      setModel(new DebitTableModel(this));
      //      TableCellRenderer tcr = LidoParamsHelper.createDoubleCellRenderer();
      //      TableCellEditor tceDouble =LidoParamsHelper.createDoubleCellEditor();
      //      TableColumnModel colModel = getColumnModel();
      //      int n = colModel.getColumnCount();
      //      for (int i = 0; i < n; i++)
      //      {
      //        colModel.getColumn(i).setCellRenderer(tcr);
      //        colModel.getColumn(i).setCellEditor(tceDouble);
      //      }
    }
  }
  class DebitTableModel implements TableModel {
    DebitTable table_;
    public DebitTableModel(final DebitTable table) {
      table_= table;
    }
    public Class getColumnClass(final int column) {
      return Double.class;
    }
    public int getColumnCount() {
      return 2;
    }
    public String getColumnName(final int column) {
      String res= null;
      switch (column) {
        case 0 :
          res= TAB_HEADER_COL0;
          break;
        case 1 :
          res= TAB_HEADER_COL1;
          break;
      }
      return res;
    }
    public int getRowCount() {
      return cl_.nbPoints;
    }
    public Object getValueAt(final int row, final int column) {
      Object res= null;
      switch (column) {
        case 0 :
          {
            if (type_ == TARAGE) {
              res= new Double(cl_.point[row].zLim);
            }
            if (type_ == LIMNI) {
              res= new Double(cl_.point[row].tLim);
            }
            if (type_ == HYDRO) {
              res= new Double(cl_.point[row].tLim);
            }
            break;
          }
        case 1 :
          {
            if (type_ == TARAGE) {
              res= new Double(cl_.point[row].qLim);
            }
            if (type_ == LIMNI) {
              res= new Double(cl_.point[row].zLim);
            }
            if (type_ == HYDRO) {
              res= new Double(cl_.point[row].qLim);
            }
            break;
          }
      }
      return res;
    }
    public boolean isCellEditable(final int row, final int column) {
      return true;
    }
    public void setValueAt(final Object value, final int row, final int column) {
      switch (column) {
        case 0 :
          {
            if (type_ == TARAGE) {
              cl_.point[row].zLim= ((Double)value).doubleValue();
            }
            if (type_ == LIMNI) {
              cl_.point[row].tLim= ((Double)value).doubleValue();
            }
            if (type_ == HYDRO) {
              cl_.point[row].tLim= ((Double)value).doubleValue();
            }
            break;
          }
        case 1 :
          {
            if (type_ == TARAGE) {
              cl_.point[row].qLim= ((Double)value).doubleValue();
            }
            if (type_ == LIMNI) {
              cl_.point[row].zLim= ((Double)value).doubleValue();
            }
            if (type_ == HYDRO) {
              cl_.point[row].qLim= ((Double)value).doubleValue();
            }
          }
      }
      objectModified();
      table_.limiteChanged();
    }
    public void addTableModelListener(final TableModelListener _l) {}
    public void removeTableModelListener(final TableModelListener _l) {}
  }
}
