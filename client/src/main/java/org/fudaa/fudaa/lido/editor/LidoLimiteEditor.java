/*
 * @file         LidoLimiteEditor.java
 * @creation     1999-05-03
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuNumericValueValidator;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.lido.SParametresBiefLimLigneRZO;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.dialog.BPanneauNavigation;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoLimiteEditor
  extends LidoCustomizer
  implements ActionListener {
  public final static int TARAGE= LidoResource.LIMITE.TARAGE;
  public final static int LIMNI= LidoResource.LIMITE.LIMNI;
  public final static int HYDRO= LidoResource.LIMITE.HYDRO;
  BuTextField tfNumero_, tfNumLoi_, tfTypLoi_;
  BuPanel pnEdition_;
  BuGridLayout loEdition_;
  private SParametresBiefLimLigneRZO cl_, clBck_;
  public LidoLimiteEditor() {
    this(null);
  }
  public LidoLimiteEditor(final BDialogContent parent) {
    super("Edition de CL");
    cl_= clBck_= null;
    loEdition_= new BuGridLayout(2, 5, 5, false, false);
    final Container pnMain_= getContentPane();
    pnEdition_= new BuPanel();
    pnEdition_.setLayout(loEdition_);
    pnEdition_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    final int textSize= 10;
    int n= 0;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(false);
    pnEdition_.add(new BuLabel("Num�ro de l'extr�mit� libre"), n++);
    pnEdition_.add(tfNumero_, n++);
    tfNumLoi_= BuTextField.createIntegerField();
    tfNumLoi_.setColumns(textSize);
    tfNumLoi_.setValueValidator(BuNumericValueValidator.POSITIVE_INTEGER);
    pnEdition_.add(new BuLabel("Num�ro de la loi CLM"), n++);
    pnEdition_.add(tfNumLoi_, n++);
    tfTypLoi_= BuTextField.createIntegerField();
    tfTypLoi_.setColumns(textSize);
    tfTypLoi_.setValueValidator(new BuNumericValueValidator(true, 1, 3));
    pnEdition_.add(new BuLabel("Type de la loi CLM"), n++);
    pnEdition_.add(tfTypLoi_, n++);
    setActionPanel(BPanneauEditorAction.EDITER);
    setNavPanel(BPanneauNavigation.VALIDER);
    pnMain_.add(pnEdition_, BorderLayout.CENTER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      final SParametresBiefLimLigneRZO vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    }
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresBiefLimLigneRZO)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    cl_= (SParametresBiefLimLigneRZO)_n;
    //if( cl_.numLoi!=null )
    //  tfNumLoi_.setValueValidator(new BuNumericValueValidator(true, 1, cl_.nbLimi));
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        cl_,
        "limite " + (cl_.numLoi));
  }
  public boolean restore() {
    if (clBck_ == null) {
      return false;
    }
    cl_.numExtBief= clBck_.numExtBief;
    cl_.numLoi= clBck_.numLoi;
    cl_.typLoi= clBck_.typLoi;
    clBck_= null;
    return true;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      clBck_= new SParametresBiefLimLigneRZO();
      clBck_.numExtBief= cl_.numExtBief;
      clBck_.numLoi= cl_.numLoi;
      clBck_.typLoi= cl_.typLoi;
    }
    Integer val= null;
    int ov= cl_.numExtBief;
    val= (Integer)tfNumero_.getValue();
    if (val == null) {
      cl_.numExtBief= 0;
    } else {
      cl_.numExtBief= val.intValue();
    }
    if (ov != cl_.numExtBief) {
      changed= true;
    }
    ov= cl_.numLoi;
    val= (Integer)tfNumLoi_.getValue();
    if (val == null) {
      cl_.numLoi= 0;
    } else {
      cl_.numLoi= val.intValue();
    }
    if (ov != cl_.numLoi) {
      changed= true;
    }
    ov= cl_.typLoi;
    val= (Integer)tfTypLoi_.getValue();
    if (val == null) {
      cl_.typLoi= 0;
    } else {
      cl_.typLoi= val.intValue();
    }
    if (ov != cl_.typLoi) {
      changed= true;
    }
    return changed;
  }
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(cl_.numExtBief));
    tfNumLoi_.setValue(new Integer(cl_.numLoi));
    tfTypLoi_.setValue(new Integer(cl_.typLoi));
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
}
