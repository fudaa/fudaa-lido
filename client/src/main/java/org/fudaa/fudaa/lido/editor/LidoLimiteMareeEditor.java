/*
 * @file         LidoLimiteMareeEditor.java
 * @creation     2001-05-10
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;

import javax.swing.BorderFactory;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimPointLigneCLM;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheLimiteCourbe;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoLimiteMareeEditor
  extends LidoCustomizerImprimable
  implements ChangeListener {
  SParametresCondLimBlocCLM cl_;
  // commun
  private BuButton btSupprimerLoi_;
  private BuTextField tfNomLoi_, tfHaute_, tfBasse_, tfDuree_;
  private JSlider slDecalage_;
  private LidoParamsHelper ph_;
  private LidoGrapheLimiteCourbe graphe_;
  private LoiMaree loiInterne_;
  public LidoLimiteMareeEditor(final LidoParamsHelper ph) {
    super("Edition de Limite : mar�e", null);
    ph_= ph;
    init();
  }
  public LidoLimiteMareeEditor(final BDialogContent parent, final LidoParamsHelper ph) {
    super(parent, "Edition de Limite : mar�e", null);
    ph_= ph;
    init();
  }
  private void init() {
    final Container content= getContentPane();
    int n= 0;
    final BuPanel pn= new BuPanel();
    pn.setLayout(new BuVerticalLayout(5, true, true));
    pn.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    final BuPanel pn4= new BuPanel();
    btSupprimerLoi_= new BuButton("Supprimer cette loi");
    btSupprimerLoi_.setActionCommand("SUPPRIMERLOI");
    btSupprimerLoi_.addActionListener(this);
    pn4.add(btSupprimerLoi_);
    tfNomLoi_= new BuTextField();
    tfHaute_= BuTextField.createDoubleField();
    tfHaute_.addActionListener(this);
    tfBasse_= BuTextField.createDoubleField();
    tfBasse_.addActionListener(this);
    tfDuree_= BuTextField.createIntegerField();
    tfDuree_.addActionListener(this);
    slDecalage_= new JSlider(SwingConstants.HORIZONTAL, 0, 12, 0);
    slDecalage_.setMinorTickSpacing(1);
    slDecalage_.setMajorTickSpacing(3);
    slDecalage_.setSnapToTicks(true);
    slDecalage_.addChangeListener(this);
    slDecalage_.setPaintTicks(true);
    slDecalage_.setPaintLabels(true);
    slDecalage_.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    pn.add(new BuLabel("Nom de la loi : "), n++);
    pn.add(tfNomLoi_, n++);
    pn.add(new BuLabel("Cote pleine mer : "), n++);
    pn.add(tfHaute_, n++);
    pn.add(new BuLabel("Cote basse mer : "), n++);
    pn.add(tfBasse_, n++);
    pn.add(new BuLabel("Dur�e simulation (heures) : "), n++);
    pn.add(tfDuree_, n++);
    pn.add(new BuLabel("D�calage (heures) : "), n++);
    pn.add(slDecalage_, n++);
    pn.add(pn4);
    graphe_=
      new LidoGrapheLimiteCourbe(
        LidoResource.LIMITE.LIMNI,
        "Mar�e",
        "T",
        "Z",
        "s",
        "m");
    graphe_.setInteractif(true);
    addPropertyChangeListener(graphe_);
    graphe_.setPreferredSize(new Dimension(320, 240));
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ALIGN_RIGHT);
    content.add(BorderLayout.WEST, pn);
    content.add(BorderLayout.CENTER, graphe_);
    pack();
  }
  public void stateChanged(final ChangeEvent e) {
    final Object src= e.getSource();
    if ((src == slDecalage_) && (!slDecalage_.getValueIsAdjusting())) {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("limite", null, cl_);
      }
    }
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("limite", null, cl_);
      }
      if (verifieContraintes()) {
        java.util.Arrays.sort(
          cl_.point,
          ph_.LOICLMPOINT_COMPARATOR(cl_.typLoi));
        fermer();
      }
    } else if ("SUPPRIMERLOI".equals(cmd)) {
      ph_.LOICLM().supprimeLoiclm(cl_);
      //SParametresCondLimBlocCLM old=cl_;
      cl_= null;
      objectDeleted();
      //firePropertyChange("loisupprimee", old, null);
      fermer();
    } else {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("limite", null, cl_);
      }
    }
  }
  private boolean verifieContraintes() {
    return true;
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresCondLimBlocCLM)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    final SParametresCondLimBlocCLM vp= cl_;
    cl_= (SParametresCondLimBlocCLM)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        cl_,
        "loiclm " + (cl_.numCondLim));
    firePropertyChange("limite", vp, cl_);
  }
  public boolean restore() {
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if ((cl_ == null)) {
      return changed;
    }
    String nom= tfNomLoi_.getText();
    if ((nom == null) || (nom.trim().equals(""))) {
      nom= "LOI LIMITE";
    }
    if (!nom.equals(cl_.description)) {
      changed= true;
      cl_.description= nom;
    }
    Object val= null;
    double dval= 0.;
    int ival= 0;
    val= tfHaute_.getValue();
    dval= ((Double)val).doubleValue();
    if (dval != loiInterne_.coteHaute) {
      loiInterne_.coteHaute= dval;
      changed= true;
    }
    val= tfBasse_.getValue();
    dval= ((Double)val).doubleValue();
    if (dval != loiInterne_.coteBasse) {
      loiInterne_.coteBasse= dval;
      changed= true;
    }
    val= tfDuree_.getValue();
    ival= ((Integer)val).intValue();
    if (ival != loiInterne_.duree) {
      loiInterne_.duree= ival;
      changed= true;
    }
    ival= slDecalage_.getValue();
    if (ival != loiInterne_.decalage) {
      loiInterne_.decalage= ival;
      changed= true;
    }
    if (changed) {
      loiInterne_.synchroniseLoiclm();
    }
    return changed;
  }
  protected void setValeurs() {
    if ((cl_ == null)) {
      return;
    }
    loiInterne_= calculeLoiMaree();
    tfNomLoi_.setText(cl_.description);
    tfHaute_.setValue(new Double(loiInterne_.coteHaute));
    tfBasse_.setValue(new Double(loiInterne_.coteBasse));
    tfDuree_.setValue(new Integer(loiInterne_.duree));
  }
  public int getNumberOfPages() {
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    return ph_.getInformationsDocument();
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
  private LoiMaree calculeLoiMaree() {
    if (cl_ == null) {
      return null;
    }
    final LoiMaree loi= new LoiMaree();
    return loi;
  }
  class LoiMaree {
    public double coteHaute;
    public double coteBasse;
    public int duree;
    public int decalage;
    public void synchroniseLoiclm() {
      final int nbPoints= duree + 1;
      cl_.point= new SParametresCondLimPointLigneCLM[nbPoints];
      for (int i= 0; i < nbPoints; i++) {
        cl_.point[i]= new SParametresCondLimPointLigneCLM();
        cl_.point[i].tLim= i * 3600.;
        cl_.point[i].qLim= 0.;
        cl_.point[i].zLim=
          ((coteBasse - coteHaute) * Math.cos(Math.PI * (i + decalage) / 6)
            + (coteHaute + coteBasse))
            / 2;
      }
      cl_.nbPoints= nbPoints;
    }
  }
  public int activate() {
    getDialog().setSize(10, 10);
    revalidate();
    getDialog().show();
    return super.activate();
  }
}
