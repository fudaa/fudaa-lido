/*
 * @file         LidoLoiclmLibraryEditor.java
 * @creation     1999-12-27
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.dialog.BPanneauNavigation;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoExport;
import org.fudaa.fudaa.lido.LidoImport;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheLimiteCourbe;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoLoiclmLibraryEditor
  extends LidoCustomizer
  implements PropertyChangeListener {
  public final static int VALIDATION= 112334;
  private SParametresCLM clm_;
  MyThumbnailPanel[] pnCL_;
  JScrollPane spCL_;
  BuPanel pnCLGen_;
  MyStatusPanel pnStatus_;
  Vector selectedLois_;
  LidoParamsHelper ph_;
  public LidoLoiclmLibraryEditor(final SParametresCLM clm, final LidoParamsHelper ph) {
    super("Gestionnaire de lois hydrauliques");
    clm_= clm;
    ph_= ph;
    init();
  }
  public LidoLoiclmLibraryEditor(
    final BDialogContent _parent,
    final SParametresCLM clm,
    final LidoParamsHelper ph) {
    super(_parent, "Gestionnaire de lois hydrauliques");
    clm_= clm;
    ph_= ph;
    init();
  }
  private void init() {
    if ((clm_ == null) || (clm_.condLimites == null)) {
      setSize(new Dimension(300, 200));
      return;
    }
    selectedLois_= new Vector();
    ph_.LOICLM().addPropertyChangeListener(this);
    final Container content= getContentPane();
    pnStatus_= new MyStatusPanel();
    pnCLGen_= new BuPanel();
    final BuGridLayout lo= new BuGridLayout(3, 0, 0, false, false);
    lo.setCfilled(false);
    pnCLGen_.setLayout(lo);
    pnCL_= new MyThumbnailPanel[clm_.nbCondLim];
    System.err.println("nbCondLim=" + clm_.nbCondLim);
    System.err.println("condLimites.length=" + clm_.condLimites.length);
    for (int i= 0; i < clm_.nbCondLim; i++) {
      System.err.println(
        "clm_.condLimites[i]=" + clm_.condLimites[i].description);
      pnCL_[i]= new MyThumbnailPanel(clm_.condLimites[i], pnStatus_);
      pnCL_[i].setActionCommand("THUMB" + i);
      pnCL_[i].addActionListener(this);
      pnCLGen_.add(pnCL_[i], i);
    }
    spCL_= new JScrollPane(pnCLGen_);
    spCL_.setBorder(
      new CompoundBorder(
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)),
        spCL_.getBorder()));
    spCL_.setPreferredSize(
      new Dimension(
        pnCLGen_.getPreferredSize().width + 30,
        pnCLGen_.getPreferredSize().width + 30));
    final BuPanel pnGen= new BuPanel();
    pnGen.setLayout(new BuVerticalLayout(5, true, false));
    pnGen.add(spCL_, 0);
    pnGen.add(pnStatus_, 1);
    content.add(BorderLayout.CENTER, pnGen);
    setNavPanel(BPanneauNavigation.FERMER);
    setActionPanel(
      BPanneauEditorAction.SUPPRIMER
        | BPanneauEditorAction.IMPORTER
        | BPanneauEditorAction.EXPORTER);
    addAction(
      "Sélectionner tout",
      BuResource.BU.getIcon("toutselectionner"),
      "SELECTIONNERTOUT");
    addAction(
      "Désélectionner tout",
      BuResource.BU.getIcon("texte"),
      "DESELECTIONNERTOUT");
    pack();
  }
  private void reinit() {
    pnCLGen_.removeAll();
    pnCL_= new MyThumbnailPanel[clm_.nbCondLim];
    for (int i= 0; i < clm_.nbCondLim; i++) {
      pnCL_[i]= new MyThumbnailPanel(clm_.condLimites[i], pnStatus_);
      pnCL_[i].setActionCommand("THUMB" + i);
      pnCL_[i].addActionListener(this);
      pnCLGen_.add(pnCL_[i], i);
    }
    spCL_.setPreferredSize(
      new Dimension(
        pnCLGen_.getPreferredSize().width + 30,
        Math.max(200, pnCLGen_.getPreferredSize().width + 30)));
    pack();
  }
  public boolean restore() {
    return false;
  }
  public void setObject(final Object clm) {
    if (!(clm instanceof SParametresCLM)) {
      return;
    }
    if (clm == clm_) {
      return;
    }
    final SParametresCLM vp= clm_;
    clm_= (SParametresCLM)clm;
    init();
    setObjectModified(false);
    firePropertyChange("clm", vp, clm_);
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    final int id= _evt.getID();
    if (_evt.getSource() instanceof MyThumbnailPanel) {
      final MyThumbnailPanel src= (MyThumbnailPanel)_evt.getSource();
      if (id == MyThumbnailPanel.SELECTION) {
        if (src.isSelected()) {
          selectedLois_.add(src.getLoi());
        } else {
          selectedLois_.remove(src.getLoi());
        }
      } else if (id == MyThumbnailPanel.VALIDATION) {
        for (int i= 0; i < pnCL_.length; i++) {
          pnCL_[i].setSelected(false);
        }
        selectedLois_.clear();
        src.setSelected(true);
        selectedLois_.add(src.getLoi());
        reponse_= VALIDATION;
        fermer();
      }
    } else if ("EXPORTER".equals(cmd)) {
      if (selectedLois_.size() > 0) {
        final File file= LidoExport.chooseFile("clm");
        if (file == null) {
          return;
        }
        try {
          LidoExport.exportCLM(getSelectedLois(), file);
        } catch (final Throwable t) {
          new BuDialogError(
            (BuCommonInterface)LidoApplication.FRAME,
            ((BuCommonInterface)LidoApplication.FRAME)
              .getImplementation()
              .getInformationsSoftware(),
            t.getMessage())
            .activate();
        }
      }
    } else if ("IMPORTER".equals(cmd)) {
      final File file= LidoImport.chooseFile("clm");
      if (file == null) {
        return;
      }
      try {
        final SParametresCondLimBlocCLM[] lois= LidoImport.importCLM(file);
        ph_.LOICLM().importeLois(lois);
        for (int i= 0; i < lois.length; i++) {
          LIDO_MODIFY_EVENT=
            new FudaaParamEvent(
              this,
              0,
              LidoResource.CLM,
              lois[i],
              "loiclm " + (lois[i].numCondLim));
          objectCreated();
        }
      } catch (final Throwable t) {
        new BuDialogError(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME)
            .getImplementation()
            .getInformationsSoftware(),
          t.getMessage())
          .activate();
      }
    } else if ("SUPPRIMER".equals(cmd)) {
      final SParametresCondLimBlocCLM[] lois= getSelectedLois();
      ph_.LOICLM().supprimeSelection(lois);
      for (int i= 0; i < lois.length; i++) {
        LIDO_MODIFY_EVENT=
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            lois[i],
            "loiclm " + (lois[i].numCondLim));
        objectDeleted();
      }
      for (int i= 0; i < pnCL_.length; i++) {
        pnCL_[i].setSelected(false);
      }
      selectedLois_.clear();
    } else if ("SELECTIONNERTOUT".equals(cmd)) {
      for (int i= 0; i < pnCL_.length; i++) {
        pnCL_[i].setSelected(true);
        selectedLois_.add(pnCL_[i].getLoi());
      }
    } else if ("DESELECTIONNERTOUT".equals(cmd)) {
      for (int i= 0; i < pnCL_.length; i++) {
        pnCL_[i].setSelected(false);
      }
    }
    super.actionPerformed(_evt);
  }
  public void propertyChange(final PropertyChangeEvent e) {
    if (e.getPropertyName().equals("loiclm")) {
      if (pnCLGen_ == null) {
        return;
      }
      reinit();
    }
  }
  public SParametresCondLimBlocCLM[] getSelectedLois() {
    final SParametresCondLimBlocCLM[] res=
      new SParametresCondLimBlocCLM[selectedLois_.size()];
    for (int i= 0; i < selectedLois_.size(); i++) {
      res[i]= (SParametresCondLimBlocCLM)selectedLois_.get(i);
    }
    return res;
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o instanceof SParametresCondLimBlocCLM[]);
  }
  protected void setValeurs() {
    reinit();
  }
  protected boolean getValeurs() {
    return false;
  }
}
class MyStatusPanel extends BuPanel {
  BuLabel lbTypeCL_, lbNomCL_, lbNumeroCL_, lbNbPointsCL_;
  public MyStatusPanel() {
    setLayout(new BuGridLayout(2, 1, 1, true, false));
    setBorder(new TitledBorder("Description"));
    int m= 0;
    add(new BuLabel("Loi: "), m++);
    lbNumeroCL_= new BuLabel();
    add(lbNumeroCL_, m++);
    add(new BuLabel("Nom: "), m++);
    lbNomCL_= new BuLabel();
    add(lbNomCL_, m++);
    add(new BuLabel("Type: "), m++);
    lbTypeCL_= new BuLabel();
    add(lbTypeCL_, m++);
    add(new BuLabel("Points: "), m++);
    lbNbPointsCL_= new BuLabel();
    add(lbNbPointsCL_, m++);
    doLayout();
  }
  public void setLoi(final SParametresCondLimBlocCLM loi) {
    if (loi == null) {
      lbNumeroCL_.setText("");
      lbNomCL_.setText("");
      lbTypeCL_.setText("");
      lbNbPointsCL_.setText("");
    } else {
      lbNumeroCL_.setText("" + loi.numCondLim);
      lbNomCL_.setText("" + loi.description);
      lbTypeCL_.setText(
        ""
          + (loi.typLoi == LidoResource.LIMITE.HYDRO
            ? "hydrogramme"
            : loi.typLoi == LidoResource.LIMITE.LIMNI
            ? "limnigramme"
            : loi.typLoi == LidoResource.LIMITE.MAREE
            ? "marée"
            : loi.typLoi == LidoResource.LIMITE.TARAGE
            ? "tarage"
            : "inconnu"));
      lbNbPointsCL_.setText("" + loi.nbPoints);
    }
  }
}
class MyThumbnailPanel extends BuPanel implements MouseListener {
  public final static int SELECTION= 1;
  public final static int VALIDATION= 2;
  MyStatusPanel status_;
  SParametresCondLimBlocCLM loi_;
  LidoGrapheLimiteCourbe graphe_;
  boolean selected_;
  Vector listeners;
  String actionCmd_;
  public MyThumbnailPanel(final SParametresCondLimBlocCLM loi, final MyStatusPanel s) {
    loi_= loi;
    graphe_= new LidoGrapheLimiteCourbe(loi_.typLoi, "", "", "", "", "");
    graphe_.setThumbnail(true, 64, 64);
    graphe_.setLimite(loi_);
    add(graphe_);
    status_= s;
    addMouseListener(this);
    listeners= new Vector();
    actionCmd_= "THUMB" + loi_.numCondLim;
  }
  public SParametresCondLimBlocCLM getLoi() {
    return loi_;
  }
  public void setSelected(final boolean s) {
    if (selected_ == s) {
      return;
    }
    selected_= s;
    setBackground(
      selected_
        ? UIManager.getColor("MenuItem.selectionBackground")
        : UIManager.getColor("*.background"));
  }
  public boolean isSelected() {
    return selected_;
  }
  public void mouseClicked(final MouseEvent e) {
    switch (e.getClickCount()) {
      case 1 :
        {
          setSelected(!selected_);
          fireActionEvent(new ActionEvent(this, SELECTION, getActionCommand()));
          break;
        }
      case 2 :
        {
          fireActionEvent(
            new ActionEvent(this, VALIDATION, getActionCommand()));
          break;
        }
    }
  }
  public void mouseEntered(final MouseEvent e) {
    status_.setLoi(loi_);
  }
  public void mouseExited(final MouseEvent e) {
    status_.setLoi(null);
  }
  public void mousePressed(final MouseEvent e) {}
  public void mouseReleased(final MouseEvent e) {}
  public void addActionListener(final ActionListener l) {
    if (!listeners.contains(l)) {
      listeners.add(l);
    }
  }
  public void removeActionListener(final ActionListener l) {
    if (listeners.contains(l)) {
      listeners.remove(l);
    }
  }
  private void fireActionEvent(final ActionEvent e) {
    for (int i= 0; i < listeners.size(); i++) {
      ((ActionListener)listeners.get(i)).actionPerformed(e);
    }
  }
  public void setActionCommand(final String cmd) {
    actionCmd_= cmd;
  }
  public String getActionCommand() {
    return actionCmd_;
  }
}
