/*
 * @file         LidoNoeudEditor.java
 * @creation     1999-04-29
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresBiefNoeudLigneRZO;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoNoeudEditor extends LidoCustomizer implements ActionListener {
  BuTextField tfNumero_;
  BuTextField[] tfExtr_;
  BuPanel pnEdition_;
  BorderLayout loMain_;
  BuGridLayout loEdition_;
  private SParametresBiefNoeudLigneRZO noeud_, noeudBck_;
  public LidoNoeudEditor() {
    this(null);
  }
  public LidoNoeudEditor(final BDialogContent parent) {
    super(parent, "Edition de noeud");
    noeud_= null;
    noeudBck_= null;
    loEdition_= new BuGridLayout(2, 5, 5, false, false);
    final Container pnMain_= getContentPane();
    pnEdition_= new BuPanel();
    pnEdition_.setLayout(loEdition_);
    pnEdition_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    final int textSize= 10;
    int n= 0;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(false);
    pnEdition_.add(new BuLabel("Num�ro du Noeud"), n++);
    pnEdition_.add(tfNumero_, n++);
    tfExtr_= new BuTextField[5];
    for (int i= 0; i < tfExtr_.length; i++) {
      tfExtr_[i]= BuTextField.createIntegerField();
      tfExtr_[i].setColumns(textSize);
      pnEdition_.add(new BuLabel("Extremit� " + (i + 1)), n++);
      pnEdition_.add(tfExtr_[i], n++);
    }
    setNavPanel(EbliPreferences.DIALOG.VALIDER);
    pnMain_.add(pnEdition_, BorderLayout.CENTER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      final SParametresBiefNoeudLigneRZO vp= noeud_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, noeud_);
      }
      fermer();
    }
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresBiefNoeudLigneRZO)) {
      return;
    }
    if (_n == noeud_) {
      return;
    }
    noeud_= (SParametresBiefNoeudLigneRZO)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        noeud_,
        "noeud ["
          + noeud_.noeud[0]
          + CtuluLibString.VIR
          + noeud_.noeud[1]
          + CtuluLibString.VIR
          + noeud_.noeud[2]
          + CtuluLibString.VIR
          + noeud_.noeud[3]
          + CtuluLibString.VIR
          + noeud_.noeud[4]
          + "]");
  }
  public boolean restore() {
    if (noeudBck_ == null) {
      return false;
    }
    noeud_.noeud= noeudBck_.noeud;
    noeudBck_= null;
    return true;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (noeud_ == null) {
      noeud_= new SParametresBiefNoeudLigneRZO();
      noeud_.noeud= new int[tfExtr_.length];
      noeudBck_= null;
    } else {
      final int[] tmp= new int[noeud_.noeud.length];
      for (int i= 0; i < noeud_.noeud.length; i++) {
        tmp[i]= noeud_.noeud[i];
      }
      noeudBck_= new SParametresBiefNoeudLigneRZO(tmp);
    }
    //noeud_.numNoeud=((Integer)tfNumero_.getValue()).intValue();
    final int[] extrs= new int[tfExtr_.length];
    for (int i= 0; i < tfExtr_.length; i++) {
      final int ov= extrs[i];
      extrs[i]= ((Integer)tfExtr_[i].getValue()).intValue();
      if (ov != extrs[i]) {
        changed= true;
      }
    }
    noeud_.noeud= extrs;
    return changed;
  }
  protected void setValeurs() {
    //tfNumero_.setValue(new Integer(noeud_.numNoeud));
    for (int i= 0; i < tfExtr_.length; i++) {
      tfExtr_[i].setValue(new Integer(noeud_.noeud[i]));
    }
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == noeud_);
  }
}
