/*
 * @file         LidoNombreSectionsEditor.java
 * @creation     2000-01-07
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresSerieLigneCAL;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoNombreSectionsEditor extends LidoCustomizer {
  SParametresSerieLigneCAL[] sections_;
  BuPanel pnGen_;
  BuTextField tfNombre_;
  BuLabel lbNombre_;
  public LidoNombreSectionsEditor(final BDialogContent parent) {
    super(parent, "Nombre de sections");
    init();
  }
  private void init() {
    sections_= null;
    tfNombre_= BuTextField.createIntegerField();
    lbNombre_= new BuLabel();
    pnGen_= new BuPanel();
    pnGen_.setBorder(
      new EmptyBorder(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE));
    pnGen_.setLayout(new BuVerticalLayout(5, true, false));
    pnGen_.add(lbNombre_, 0);
    pnGen_.add(tfNombre_, 1);
    getContentPane().add(BorderLayout.CENTER, pnGen_);
    setNavPanel(EbliPreferences.DIALOG.FERMER);
    pack();
  }
  public boolean restore() {
    return false;
  }
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresSerieLigneCAL[])) {
      return;
    }
    if (_n == sections_) {
      return;
    }
    sections_= (SParametresSerieLigneCAL[])_n;
    if ((sections_ != null)) {
      lbNombre_.setText(sections_.length + " sections selectionnées");
    }
    pack();
    setObjectModified(false);
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    if ("FERMER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("object", null, sections_);
      }
    }
    super.actionPerformed(e);
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (sections_ == null) {
      return changed;
    }
    final Integer val= (Integer)tfNombre_.getValue();
    if (val == null) {
      return changed;
    }
    int ov= 0;
    for (int i= 0; i < sections_.length; i++) {
      ov= sections_[i].nbSectCalc;
      sections_[i].nbSectCalc= val.intValue();
      if (ov != sections_[i].nbSectCalc) {
        LIDO_MODIFY_EVENT=
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CAL,
            sections_[i],
            "section "
              + sections_[i].absDebBief
              + CtuluLibString.VIR
              + sections_[i].absFinBief
              + "]");
        setObjectModified(false);
        objectModified();
        changed= true;
      }
    }
    return changed;
  }
  protected void setValeurs() {}
  protected boolean isObjectModificationImportant(final Object o) {
    return (o instanceof SParametresSerieLigneCAL);
  }
  public void paramStructCreated(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    if (struct == null) {
      return;
    }
    if (isObjectModificationImportant(struct)) {
      fermer();
    }
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    if (struct == null) {
      return;
    }
    if (isObjectModificationImportant(struct)) {
      fermer();
    }
  }
  public void paramStructModified(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    if (struct == null) {
      return;
    }
    if (isObjectModificationImportant(struct)) {
      fermer();
    }
  }
}
