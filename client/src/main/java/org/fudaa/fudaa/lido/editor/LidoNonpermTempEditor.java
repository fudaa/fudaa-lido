/*
 * @file         LidoNonpermTempEditor.java
 * @creation     1999-09-20
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.util.StringTokenizer;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresTempCAL;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Id: LidoNonpermTempEditor.java,v 1.11 2006-09-19 15:05:00 deniger Exp $
 * @author       Axel von Arnim
 */
public class LidoNonpermTempEditor
  extends LidoCustomizer
  implements ActionListener {
  final static String SEC= "s";
  final static String MN_SEC= "mn:s";
  final static String H_MN_SEC= "h:mn:s";
  final static String J_H_MN_SEC= "jrs:h:mn:s";
  JComboBox cmbUniteTi_, cmbUniteTf_, cmbUniteDt_, cmbUniteDtImp_;
  MyTimeField tfTi_, tfTf_, tfDt_, tfDtImp_;
  BuPanel pnHaut_, pnTout_;
  SParametresTempCAL cl_, clBck_;
  LidoParamsHelper ph_;
  public LidoNonpermTempEditor(final LidoParamsHelper ph) {
    this(null, ph);
  }
  public LidoNonpermTempEditor(final BDialogContent parent, final LidoParamsHelper ph) {
    super("Variables temporelles");
    ph_= ph;
    cl_= clBck_= null;
    int n= 0;
    final BuLabelMultiLine lbMilieu=
      new BuLabelMultiLine("D�termination des variables temporelles\npour un r�gime non permanent");
    lbMilieu.setFont(
      lbMilieu.getFont().deriveFont(lbMilieu.getFont().getSize() + 2));
    lbMilieu.setForeground(Color.black);
    lbMilieu.setHorizontalAlignment(SwingConstants.CENTER);
    n= 0;
    pnHaut_= new BuPanel();
    pnHaut_.setLayout(new BuGridLayout(3, 0, 5, true, false));
    tfTi_= new MyTimeField();
    tfTi_.setColumns(8);
    tfTi_.setEditable(true);
    cmbUniteTi_=
      new JComboBox(new String[] { SEC, MN_SEC, H_MN_SEC, J_H_MN_SEC });
    cmbUniteTi_.addActionListener(this);
    pnHaut_.add(new BuLabel("Temps initial (Ti)"), n++);
    pnHaut_.add(tfTi_, n++);
    pnHaut_.add(cmbUniteTi_, n++);
    tfTf_= new MyTimeField();
    tfTf_.setColumns(8);
    cmbUniteTf_=
      new JComboBox(new String[] { SEC, MN_SEC, H_MN_SEC, J_H_MN_SEC });
    cmbUniteTf_.addActionListener(this);
    pnHaut_.add(new BuLabel("Temps final (Tf)"), n++);
    pnHaut_.add(tfTf_, n++);
    pnHaut_.add(cmbUniteTf_, n++);
    tfDt_= new MyTimeField();
    tfDt_.setColumns(8);
    cmbUniteDt_=
      new JComboBox(new String[] { SEC, MN_SEC, H_MN_SEC, J_H_MN_SEC });
    cmbUniteDt_.addActionListener(this);
    pnHaut_.add(new BuLabel("Pas de temps (Dt)"), n++);
    pnHaut_.add(tfDt_, n++);
    pnHaut_.add(cmbUniteDt_, n++);
    tfDtImp_= new MyTimeField();
    tfDtImp_.setColumns(8);
    cmbUniteDtImp_=
      new JComboBox(new String[] { SEC, MN_SEC, H_MN_SEC, J_H_MN_SEC });
    cmbUniteDtImp_.addActionListener(this);
    pnHaut_.add(new BuLabel("Pas de temps d'impression (DtImp)"), n++);
    pnHaut_.add(tfDtImp_, n++);
    pnHaut_.add(cmbUniteDtImp_, n++);
    n= 0;
    pnTout_= new BuPanel();
    pnTout_.setLayout(new BuVerticalLayout(5, true, true));
    pnTout_.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    pnTout_.add(pnHaut_, n++);
    getContentPane().add(BorderLayout.CENTER, pnTout_);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    final Object src= _evt.getSource();
    if ("VALIDER".equals(cmd)) {
      final SParametresTempCAL vp= cl_;
      final boolean changed= getValeurs();
      if (!verifieContraintes()) {
        return;
      }
      if (changed) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    } else if (src == cmbUniteTi_) {
      tfTi_.setMode((String)cmbUniteTi_.getSelectedItem());
    } else if (src == cmbUniteTf_) {
      tfTf_.setMode((String)cmbUniteTf_.getSelectedItem());
    } else if (src == cmbUniteDt_) {
      tfDt_.setMode((String)cmbUniteDt_.getSelectedItem());
    }
    if (src == cmbUniteDtImp_) {
      tfDtImp_.setMode((String)cmbUniteDtImp_.getSelectedItem());
    }
  }
  public void setTemps(
    final double tInit,
    final double tFin,
    final double pas2T,
    final double pas2TImp) {
    tfTi_.setValue(new Double(tInit));
    tfTf_.setValue(new Double(tFin));
    tfDt_.setValue(new Double(pas2T));
    tfDtImp_.setValue(new Double(pas2TImp));
    getValeurs();
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresTempCAL)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    cl_= (SParametresTempCAL)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(this, 0, LidoResource.CAL, cl_, "temps");
  }
  public boolean restore() {
    if (clBck_ == null) {
      return false;
    }
    cl_.tInit= clBck_.tInit;
    cl_.tMax= clBck_.tMax;
    cl_.pas2T= clBck_.pas2T;
    cl_.numDerPaStoc= clBck_.numDerPaStoc;
    cl_.pas2TImp= clBck_.pas2TImp;
    cl_.pas2TStoc= clBck_.pas2TStoc;
    clBck_= null;
    return true;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      clBck_= new SParametresTempCAL();
      clBck_.tInit= cl_.tInit;
      clBck_.tMax= cl_.tMax;
      clBck_.pas2T= cl_.pas2T;
      clBck_.numDerPaStoc= cl_.numDerPaStoc;
      clBck_.pas2TImp= cl_.pas2TImp;
      clBck_.pas2TStoc= cl_.pas2TStoc;
    }
    Double val= null;
    double ov= 0.;
    ov= cl_.tInit;
    val= (Double)tfTi_.getValue();
    // A FAIRE: g�rer la valeur par d�faut (mettre la min des CL)
    if (val == null) {
      cl_.tInit= 0.;
    } else {
      cl_.tInit= val.doubleValue();
    }
    if (ov != cl_.tInit) {
      changed= true;
    }
    ov= cl_.tMax;
    val= (Double)tfTf_.getValue();
    // A FAIRE: g�rer la valeur par d�faut (mettre la max des CL)
    if (val == null) {
      cl_.tMax= 0.;
    } else {
      cl_.tMax= val.doubleValue();
    }
    if (ov != cl_.tMax) {
      changed= true;
    }
    ov= cl_.pas2T;
    val= (Double)tfDt_.getValue();
    // A FAIRE: g�rer la valeur par d�faut (mettre max-min des CL)
    if (val == null) {
      cl_.pas2T= 0.;
    } else {
      cl_.pas2T= val.doubleValue();
    }
    if (ov != cl_.pas2T) {
      changed= true;
    }
    ov= cl_.pas2TImp;
    val= (Double)tfDtImp_.getValue();
    // A FAIRE: g�rer la valeur par d�faut (mettre max-min des CL)
    if (val == null) {
      cl_.pas2TImp= 0.;
    } else {
      cl_.pas2TImp= val.doubleValue();
    }
    if (ov != cl_.pas2TImp) {
      changed= true;
    }
    cl_.pas2TStoc= cl_.pas2TImp;
    return changed;
  }
  protected void setValeurs() {
    tfTi_.setValue(new Double(cl_.tInit));
    tfTf_.setValue(new Double(cl_.tMax));
    tfDt_.setValue(new Double(cl_.pas2T));
    tfDtImp_.setValue(new Double(cl_.pas2TImp));
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
  private boolean verifieContraintes() {
    boolean ok= true;
    final double tMinClm= ph_.LOICLM().getTempsInitial();
    final double tMaxClm= ph_.LOICLM().getTempsFinal();
    if (tMinClm < tMaxClm) {
      ok= ok && (cl_.tInit >= tMinClm) && (cl_.tMax <= tMaxClm);
    }
    final double tMinSng= ph_.LOISNG().getTempsInitial();
    final double tMaxSng= ph_.LOISNG().getTempsFinal();
    if (tMinSng < tMaxSng) {
      ok= ok && (cl_.tInit >= tMinSng) && (cl_.tMax <= tMaxSng);
    }
    if (!ok) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "L'intervalle de temps doit etre\n"
          + "inclus dans les temps des conditions\n"
          + "limites et des singularites!")
        .activate();
    }
    ok= ((cl_.pas2TImp % cl_.pas2T) == 0.);
    if (!ok) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "Le pas de temps d'impression\n"
          + "doit etre un multiple du pas\n"
          + "de temps de calcul.")
        .activate();
    }
    return ok;
  }
}
// HACK immonde car le TIME_FIELD de BuTextField ne marche pas bien
class MyTimeField extends BuTextField {
  final static String SEC= "s";
  final static String MN_SEC= "mn:s";
  final static String H_MN_SEC= "h:mn:s";
  final static String J_H_MN_SEC= "jrs:h:mn:s";
  String mode_= SEC;
  int modeMaxTokens_;
  public void setMode(final String mode) {
    if ((mode == null) || mode_.equals(mode)) {
      return;
    }
    final Double val= (Double)getValue();
    if (mode.equals(SEC)) {
      mode_= SEC;
      modeMaxTokens_= 1;
    } else if (mode.equals(MN_SEC)) {
      mode_= MN_SEC;
      modeMaxTokens_= 2;
    } else if (mode.equals(H_MN_SEC)) {
      mode_= H_MN_SEC;
      modeMaxTokens_= 3;
    } else if (mode.equals(J_H_MN_SEC)) {
      mode_= J_H_MN_SEC;
      modeMaxTokens_= 4;
    }
    if (val != null) {
      setValue(val);
    } else {
      setText("");
    }
  }
  protected void processFocusEvent(final FocusEvent _evt) {
    if (_evt.getID() == FocusEvent.FOCUS_LOST) {
      final Double val= (Double)getValue();
      if (val != null) {
        setValue(val);
      } else {
        setText("");
      }
    }
    super.processFocusEvent(_evt);
  }
  protected void processKeyEvent(final KeyEvent _evt) {
    System.out.println("autre touche");
    if (_evt.getID() == KeyEvent.KEY_TYPED) {
      final char key= _evt.getKeyChar();
      if (Character.isISOControl(key)) {
        super.processKeyEvent(_evt);
        return;
      }
      if (!Character.isDigit(key) && (key != ':')) {
        return;
      }
      if (key == ':') {
        if (getText().endsWith(":")) {
          return;
        }
        final int countTokens= new StringTokenizer(getText(), ":").countTokens();
        if (countTokens >= modeMaxTokens_) {
          return;
        }
      }
    }
    super.processKeyEvent(_evt);
  }
  public void setValue(final Double t) {
    setText(timeToString(t.doubleValue()));
  }
  public Object getValue() {
    final String t= getText();
    if ((t == null) || "".equals(t)) {
      return null;
    }
    try {
      return new Double(stringToTime(t));
    } catch (final Exception e) {
      System.err.println("incorrect time!");
      return null;
    }
  }
  private double stringToTime(final String t) {
    if ("".equals(t)) {
      return 0.;
    }
    double time= 0.;
    final String mode= mode_;
    String eaten= t;
    String spit= "";
    int ind= 0;
    // nettoyage de la fin de la chaine
    while (eaten.endsWith(":")) {
      eaten= eaten.substring(0, eaten.length() - 1);
    }
    // secondes
    if (mode.endsWith("s")) {
      ind= eaten.lastIndexOf(':');
      if (ind > 0) {
        spit= eaten.substring(ind + 1);
        eaten= eaten.substring(0, ind);
        time += Integer.parseInt(spit);
      } else {
        return time + Integer.parseInt(eaten);
      }
    }
    // minutes
    if (mode.endsWith("mn:s")) {
      ind= eaten.lastIndexOf(':');
      if (ind > 0) {
        spit= eaten.substring(ind + 1);
        eaten= eaten.substring(0, ind);
        time += 60. * Integer.parseInt(spit);
      } else {
        return time + 60. * Integer.parseInt(eaten);
      }
    }
    // heures
    if (mode.endsWith("h:mn:s")) {
      ind= eaten.lastIndexOf(':');
      if (ind > 0) {
        spit= eaten.substring(ind + 1);
        eaten= eaten.substring(0, ind);
        time += 3600. * Integer.parseInt(spit);
      } else {
        return time + 3600. * Integer.parseInt(eaten);
      }
    }
    // jours
    if (mode.endsWith("jrs:h:mn:s")) {
      ind= eaten.lastIndexOf(':');
      if (ind > 0) {
        spit= eaten.substring(ind + 1);
        eaten= eaten.substring(0, ind);
        time += 86400. * Integer.parseInt(spit);
      } else {
        time += 86400. * Integer.parseInt(eaten);
      }
    }
    return time;
  }
  private String timeToString(final double _time) {
    long time= (long)_time;
    String text= "";
    String mode= String.valueOf(mode_);
    long val;
    // jours
    if (mode.startsWith("jrs")) {
      val= time / 86400;
      time %= 86400;
      text += val + ":";
      mode= mode.substring(4);
    }
    // heures
    if (mode.startsWith("h")) {
      val= time / 3600;
      time %= 3600;
      text += val + ":";
      mode= mode.substring(2);
    }
    // minutes
    if (mode.startsWith("mn")) {
      val= time / 60;
      time %= 60;
      text += val + ":";
      mode= mode.substring(3);
    }
    // secondes
    text += time;
    return text;
  }
}
