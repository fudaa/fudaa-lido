/*
 * @file         LidoPermCalEditor.java
 * @creation     1999-09-10
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresGenCAL;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPermCalEditor
  extends LidoCustomizer
  implements ActionListener {
  final static String REGIME_PERM= LidoResource.CALCUL.REGIME_PERM;
  final static String REGIME_NONPERM= LidoResource.CALCUL.REGIME_NONPERM;
  final static String CODE_STR_LIDO= LidoResource.CALCUL.CODE_STR_LIDO;
  final static String CODE_STR_SARA= LidoResource.CALCUL.CODE_STR_SARA;
  final static String CODE_STR_REZO= LidoResource.CALCUL.CODE_STR_REZO;
  final static String LIT_STR_MINMAJ= LidoResource.CALCUL.LIT_STR_MINMAJ;
  final static String LIT_STR_FONBER= LidoResource.CALCUL.LIT_STR_FONBER;
  //  JComboBox cmbReg_, cmbCode_;
  JComboBox cmbLit_;
  //JCheckBox cbSeuil_;
  BuTextField tfOrigine_, tfFin_;
  BuPanel pnHaut_, pnBas_, pnTout_;
  SParametresGenCAL cl_, clBck_;
  public LidoPermCalEditor() {
    this(null);
  }
  public LidoPermCalEditor(final BDialogContent parent) {
    super("Param�tres de calcul");
    cl_= clBck_= null;
    int n= 0;
    n= 0;
    pnHaut_= new BuPanel();
    pnHaut_.setLayout(new BuGridLayout(2, 5, 5, true, false));
    // ON VIRE LE REGIME QUI EST CHOISI AILLEURS: PAR UN BOUTON DE LA TOOLBAR
    //    cmbReg_=new JComboBox(new String[]
    //      { REGIME_PERM, REGIME_NONPERM });
    // ON VIRE LE CODE: REZO DOIT ETRE TJRS UTILISE
    //    cmbCode_=new JComboBox(new String[]
    //      { CODE_STR_REZO, CODE_STR_SARA });
    cmbLit_= new JComboBox(new String[] { LIT_STR_MINMAJ, LIT_STR_FONBER });
    //    cbSeuil_=new JCheckBox();
    //    BuLabel lbCode=new BuLabel("Nature du code utilis�");
    //    pnHaut_.add(lbCode, n++);
    //    pnHaut_.add(cmbCode_, n++);
    //    pnHaut_.add(new BuLabel("R�gime"), n++);
    //    pnHaut_.add(cmbReg_, n++);
    pnHaut_.add(new BuLabel("Composition du lit"), n++);
    pnHaut_.add(cmbLit_, n++);
    //    pnHaut_.add(new BuLabel("Seuils"), n++);
    //    pnHaut_.add(cbSeuil_, n++);
    n= 0;
    pnBas_= new BuPanel();
    pnBas_.setLayout(new BuGridLayout(2, 5, 5, true, false));
    tfOrigine_= BuTextField.createDoubleField();
    tfOrigine_.setColumns(8);
    dndInitDropTarget(tfOrigine_);
    tfFin_= BuTextField.createDoubleField();
    tfFin_.setColumns(8);
    dndInitDropTarget(tfFin_);
    final BuLabel lbOrigine= new BuLabel("Origine (amont)");
    //    lbOrigine.setPreferredSize(lbCode.getPreferredSize());
    pnBas_.add(lbOrigine, n++);
    pnBas_.add(tfOrigine_, n++);
    pnBas_.add(new BuLabel("Fin (aval)"), n++);
    pnBas_.add(tfFin_, n++);
    n= 0;
    pnTout_= new BuPanel();
    pnTout_.setLayout(new BuVerticalLayout(5, true, true));
    pnTout_.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    pnTout_.add(pnHaut_, n++);
    final BuLabel lbMilieu= new BuLabel("Partie de la rivi�re � �tudier");
    lbMilieu.setFont(
      lbMilieu.getFont().deriveFont(lbMilieu.getFont().getSize() + 2));
    lbMilieu.setForeground(Color.black);
    lbMilieu.setHorizontalAlignment(SwingConstants.CENTER);
    pnTout_.add(lbMilieu, n++);
    pnTout_.add(pnBas_, n++);
    getContentPane().add(BorderLayout.CENTER, pnTout_);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      final SParametresGenCAL vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    }
  }
  /*public void setXIntervalle(double xOrigine, double xFin)
  {
    tfOrigine_.setValue(new Double(xOrigine));
    tfFin_.setValue(new Double(xFin));
  }*/
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresGenCAL)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    cl_= (SParametresGenCAL)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(this, 0, LidoResource.CAL, cl_, "calcul");
  }
  public boolean restore() {
    if (clBck_ == null) {
      return false;
    }
    cl_.code= clBck_.code;
    cl_.regime= clBck_.regime;
    cl_.seuil= clBck_.seuil;
    cl_.compLits= clBck_.compLits;
    cl_.frotPV= clBck_.frotPV;
    cl_.impGeo= clBck_.impGeo;
    cl_.impPlan= clBck_.impPlan;
    cl_.impRez= clBck_.impRez;
    cl_.impHyd= clBck_.impHyd;
    cl_.biefXOrigi= clBck_.biefXOrigi;
    cl_.biefXFin= clBck_.biefXFin;
    clBck_= null;
    return true;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      clBck_= new SParametresGenCAL();
      clBck_.code= cl_.code;
      clBck_.regime= cl_.regime;
      clBck_.seuil= cl_.seuil;
      clBck_.compLits= cl_.compLits;
      clBck_.frotPV= cl_.frotPV;
      clBck_.impGeo= cl_.impGeo;
      clBck_.impPlan= cl_.impPlan;
      clBck_.impRez= cl_.impRez;
      clBck_.impHyd= cl_.impHyd;
      clBck_.biefXOrigi= cl_.biefXOrigi;
      clBck_.biefXFin= cl_.biefXFin;
    }
    Object val= null;
    // ON DEGAGE LE REGIME D'ICI
    /*    val=cmbReg_.getSelectedItem();
        if( val==null ) cl_.regime=((String)cmbReg_.getItemAt(0)).toUpperCase();
        else
          if( ((String)val).equals(REGIME_PERM) )
            cl_.regime="P";
          else
            cl_.regime="NP";*/
    // ON CABLE LE CODE EN DUR: REZO
    /*    val=cmbCode_.getSelectedItem();
        if( val==null ) cl_.code=((String)cmbCode_.getItemAt(0)).toUpperCase();
        else cl_.code=((String)val).toUpperCase();*/
    cl_.code= "REZO";
    final String ol= cl_.compLits;
    val= cmbLit_.getSelectedItem();
    if (val == null) {
      cl_.compLits= ((String)cmbLit_.getItemAt(0)).toUpperCase();
    } else {
      cl_.compLits= ((String)val).toUpperCase();
    }
    if (!cl_.compLits.equals(ol)) {
      changed= true;
    }
    //    if( cbSeuil_.isSelected() )
    //      cl_.seuil="OUI";
    //    else
    //      cl_.seuil="NON";
    final double oo= cl_.biefXOrigi;
    val= tfOrigine_.getValue();
    // r�fl�chir a la valeur par d�faut (x premier profil?)
    if (val == null) {
      cl_.biefXOrigi= 0.;
    // A FAIRE : verifier contrainte : doit correspondre a une abscisse de profil
    } else {
      cl_.biefXOrigi= ((Double)val).doubleValue();
    }
    if (cl_.biefXOrigi != oo) {
      changed= true;
    }
    final double of= cl_.biefXFin;
    val= tfFin_.getValue();
    if (val == null) {
      cl_.biefXFin= 0.;
    // A FAIRE : verifier contrainte : doit correspondre a une abscisse de profil
    } else {
      cl_.biefXFin= ((Double)val).doubleValue();
    }
    if (cl_.biefXFin != of) {
      changed= true;
    }
    return changed;
  }
  protected void setValeurs() {
    /*    if( cl_.regime.trim().equalsIgnoreCase("P") )
          cmbReg_.setSelectedItem(REGIME_PERM);
        else
          cmbReg_.setSelectedItem(REGIME_NONPERM);

        if( cl_.code.trim().equalsIgnoreCase(CODE_STR_LIDO) )
          cmbCode_.setSelectedItem(CODE_STR_LIDO);
        else
        if( cl_.code.trim().equalsIgnoreCase(CODE_STR_SARA) )
          cmbCode_.setSelectedItem(CODE_STR_SARA);
        else
        if( cl_.code.trim().equalsIgnoreCase(CODE_STR_REZO) )
          cmbCode_.setSelectedItem(CODE_STR_REZO);
        else {
          System.err.println("LidoPermCalEditor : Warning : unrecognized CODE ("+cl_.code.trim()+")");
        }*/
    if (cl_.compLits.trim().equalsIgnoreCase(LIT_STR_MINMAJ)) {
      cmbLit_.setSelectedItem(LIT_STR_MINMAJ);
    } else if (cl_.compLits.trim().equalsIgnoreCase(LIT_STR_FONBER)) {
      cmbLit_.setSelectedItem(LIT_STR_FONBER);
    } else {
      System.err.println(
        "LidoPermCalEditor : Warning : unrecognized COMP_LITS ("
          + cl_.compLits.trim()
          + ")");
    }
    //    cbSeuil_.setSelected("OUI".equalsIgnoreCase(cl_.seuil.trim()));
    tfOrigine_.setValue(new Double(cl_.biefXOrigi));
    tfFin_.setValue(new Double(cl_.biefXFin));
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
  // DND Target
  protected Class dndGetSourceParamClass() {
    return SParametresBiefBlocPRO.class;
  }
  protected boolean dndIsDropAccepted(final Point p) {
    return true;
  }
  protected void dndDropSucceeded() {
    final SParametresBiefBlocPRO profil=
      (SParametresBiefBlocPRO)dndGetDropObject().getParam();
    if (profil != null) {
      final Component c= dndGetDropComponent();
      if (c == tfOrigine_) {
        tfOrigine_.setValue(new Double(profil.abscisse));
      } else if (c == tfFin_) {
        tfFin_.setValue(new Double(profil.abscisse));
      }
      objectModified();
      firePropertyChange("object", cl_, cl_);
    }
  }
}
