/*
 * @file         LidoPermPlanimetrageEditor.java
 * @creation     1999-09-15
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresPasLigneCAL;
import org.fudaa.dodico.corba.lido.SParametresPlaniCAL;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPermPlanimetrageEditor
  extends LidoCustomizer
  implements ActionListener, PropertyChangeListener {
  // cl_.varPlanNbVal est toujours = 40 (d'apres Stephane Ladreyt)
  public final static int NBVALEURS_PLANI=
    LidoResource.PLANIMETRAGE.NBVALEURS_PLANI;
  private NumberFormat nf2_;
  LidoParamsHelper ph_;
  SParametresPlaniCAL cl_, clBck_;
  BuPanel pnBas_, pnTout_;
  BuTextField[] tfDu_, tfAu_, tfPas_;
  BuButton[] btVisu_;
  public LidoPermPlanimetrageEditor() {
    this(null);
  }
  public LidoPermPlanimetrageEditor(final BDialogContent parent) {
    super("Planim�trage");
    int n= 0;
    nf2_= NumberFormat.getInstance(Locale.US);
    nf2_.setMaximumFractionDigits(4);
    nf2_.setGroupingUsed(false);
    /*    BuLabel lbMilieu=new BuLabel("Hauteur d'eau maximale entre les profils � d�terminer ci-dessous");
        lbMilieu.setFont(lbMilieu.getFont().deriveFont(lbMilieu.getFont().getSize()+2));
        lbMilieu.setForeground(Color.black);
        lbMilieu.setHorizontalAlignment(JLabel.CENTER);
    */
    n= 0;
    pnBas_= new BuPanel();
    pnBas_.setLayout(new BuGridLayout(7, 5, 5, true, true));
    tfDu_= new BuTextField[5];
    tfAu_= new BuTextField[5];
    tfPas_= new BuTextField[5];
    btVisu_= new BuButton[5];
    for (int i= 0; i < 5; i++) {
      tfDu_[i]= BuTextField.createIntegerField();
      tfDu_[i].setColumns(8);
      tfAu_[i]= BuTextField.createIntegerField();
      tfAu_[i].setColumns(8);
      tfPas_[i]= BuTextField.createDoubleField();
      tfPas_[i].setColumns(8);
      btVisu_[i]= new BuButton("Voir");
      btVisu_[i].addActionListener(this);
      pnBas_.add(new BuLabel("Du profil"), n++);
      pnBas_.add(tfDu_[i], n++);
      pnBas_.add(new BuLabel("au profil"), n++);
      pnBas_.add(tfAu_[i], n++);
      pnBas_.add(new BuLabel("hauteur du pas"), n++);
      pnBas_.add(tfPas_[i], n++);
      pnBas_.add(btVisu_[i], n++);
    }
    pnTout_= new BuPanel();
    pnTout_.setLayout(new BuVerticalLayout(5, false, true));
    pnTout_.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    //    pnTout_.add(lbMilieu);
    pnTout_.add(pnBas_);
    getContentPane().add(BorderLayout.CENTER, pnTout_);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ANNULER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    final Object src= _evt.getSource();
    if ("VALIDER".equals(cmd)) {
      final SParametresPlaniCAL vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    } else {
      for (int i= 0; i < 5; i++) {
        if (src == btVisu_[i]) {
          getValeurs();
          if (i >= cl_.varPlanNbPas) {
            return;
          }
          final LidoCustomizer edit= new LidoPermPlanimetrageMinmaxEditor(this);
          ((LidoPermPlanimetrageMinmaxEditor)edit).setParamsHelper(ph_);
          edit.addPropertyChangeListener(this);
          getValeurs();
          edit.setObject(cl_.varsPlanimetrage[i]);
          edit.show();
          break;
        }
      }
    }
  }
  public void propertyChange(final PropertyChangeEvent e) {
    if ("taillePas".equals(e.getPropertyName())) {
      setValeurs();
    }
  }
  public void setParamsHelper(final LidoParamsHelper _ph) {
    if ((ph_ == _ph) || (_ph == null)) {
      return;
    }
    ph_= _ph;
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresPlaniCAL)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    System.err.println("resetting plani");
    cl_= (SParametresPlaniCAL)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(this, 0, LidoResource.CAL, cl_, "planimetrage");
  }
  public boolean restore() {
    if (clBck_ == null) {
      return false;
    }
    // A FAIRE : recopier clBck_ dans cl_
    clBck_= null;
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      // A FAIRE : creer une copie de cl_
    }
    cl_.varPlanNbVal= NBVALEURS_PLANI;
    final int oldNb= cl_.varPlanNbPas;
    cl_.varPlanNbPas= 0;
    for (int i= 0; i < 5; i++) {
      if ((tfDu_[i].getValue() != null) && (tfAu_[i].getValue() != null)) {
        cl_.varPlanNbPas++;
      } else {
        break;
      }
    }
    if (oldNb != cl_.varPlanNbPas) {
      changed= true;
    }
    final SParametresPasLigneCAL[] varsOld= cl_.varsPlanimetrage;
    cl_.varsPlanimetrage= new SParametresPasLigneCAL[cl_.varPlanNbPas];
    Object val= null;
    for (int i= 0; i < cl_.varPlanNbPas; i++) {
      cl_.varsPlanimetrage[i]= new SParametresPasLigneCAL();
      //      if( i<varsOld.length )
      //        cl_.varsPlanimetrage[i].taillePas=varsOld[i].taillePas;
      val= tfPas_[i].getValue();
      if (val == null) {
        val= new Double(0.);
      }
      cl_.varsPlanimetrage[i].taillePas= ((Double)val).doubleValue();
      cl_.varsPlanimetrage[i].profilDebut=
        ((Integer)tfDu_[i].getValue()).intValue();
      cl_.varsPlanimetrage[i].profilFin=
        ((Integer)tfAu_[i].getValue()).intValue();
      if ((i < varsOld.length)
        && (varsOld[i] != null)
        && ((varsOld[i].taillePas != cl_.varsPlanimetrage[i].taillePas)
          || (varsOld[i].profilDebut != cl_.varsPlanimetrage[i].profilDebut)
          || (varsOld[i].profilFin != cl_.varsPlanimetrage[i].profilFin))) {
        changed= true;
      }
    }
    return changed;
  }
  private void initValeurs() {
    for (int i= 0; i < 5; i++) {
      tfDu_[i].setText("");
      tfAu_[i].setText("");
      tfPas_[i].setText("");
    }
  }
  protected void setValeurs() {
    initValeurs();
    for (int i= 0; i < cl_.varPlanNbPas; i++) {
      tfDu_[i].setValue(new Integer(cl_.varsPlanimetrage[i].profilDebut));
      tfAu_[i].setValue(new Integer(cl_.varsPlanimetrage[i].profilFin));
      tfPas_[i].setValue(
        new Double(nf2_.format(cl_.varsPlanimetrage[i].taillePas)));
    }
  }
  protected boolean isObjectModificationImportant(final Object o) {
    boolean res= (o == cl_);
    for (int i= 0; i < cl_.varPlanNbPas; i++) {
      res= res || (cl_.varsPlanimetrage[i] == o);
    }
    return res;
  }
}
