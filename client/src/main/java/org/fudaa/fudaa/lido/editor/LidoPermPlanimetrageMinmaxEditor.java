/*
 * @file         LidoPermPlanimetrageMinmaxEditor.java
 * @creation     1999-09-16
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPasLigneCAL;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheProfil;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
// ATTENTION: On ne gere ici que le cas de profils entres par points!!
// prevoir la gestion par largeurs ou transformer automatiquement en
// points au niveau de Dodico.
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPermPlanimetrageMinmaxEditor
  extends LidoCustomizerImprimable {
  LidoGrapheProfil graphe_;
  LidoParamsHelper ph_;
  SParametresPasLigneCAL cl_, clBck_;
  BuPanel pn1_, pn2_, pnEdition_, pnTout_;
  AbstractButton btReculer_, btAvancer_, btReculerVite_, btAvancerVite_;
  JCheckBox cbZoom_, cbRives_, cbAdjustZ_, cbFixAxes_, cbPasPlani_;
  BGrapheEditeurAxes edAxes_;
  BuTextField tfHmax_;
  int indProfil_;
  public LidoPermPlanimetrageMinmaxEditor() {
    super("Pas de planimétrage");
    init();
  }
  public LidoPermPlanimetrageMinmaxEditor(final BDialogContent _parent) {
    super(_parent, "Pas de planimétrage");
    init();
  }
  private void init() {
    cl_= clBck_= null;
    ph_= null;
    indProfil_= 0;
    int n;
    pn1_= new BuPanel();
    tfHmax_= BuTextField.createDoubleField();
    tfHmax_.addActionListener(this);
    tfHmax_.setColumns(8);
    pn1_.add(new BuLabel("Hauteur maximale supposée (m)"));
    pn1_.add(tfHmax_);
    n= 0;
    pn2_= new BuPanel();
    pn2_.setLayout(new BuHorizontalLayout(5, false, false));
    btReculerVite_= new BuButton(BuResource.BU.getIcon("reculervite"), "<<");
    btReculerVite_.setActionCommand("RECULERVITE");
    btReculerVite_.addActionListener(this);
    //    btReculer_=new BuToggleButton("Profondeur min");
    btReculer_= new BuButton(BuResource.BU.getIcon("reculer"), "<");
    btReculer_.setActionCommand("RECULER");
    btReculer_.addActionListener(this);
    //    btReculer_.setSelected(true);
    //    btAvancer_=new BuToggleButton("Profondeur max");
    btAvancer_= new BuButton(BuResource.BU.getIcon("avancer"), ">");
    btAvancer_.setActionCommand("AVANCER");
    btAvancer_.addActionListener(this);
    //    btAvancer_.setSelected(false);
    //    ButtonGroup bg=new ButtonGroup();
    //    bg.add(btReculer_);
    //    bg.add(btAvancer_);
    btAvancerVite_= new BuButton(BuResource.BU.getIcon("avancervite"), ">>");
    btAvancerVite_.setActionCommand("AVANCERVITE");
    btAvancerVite_.addActionListener(this);
    pn2_.add(btReculerVite_, n++);
    pn2_.add(btReculer_, n++);
    pn2_.add(btAvancer_, n++);
    pn2_.add(btAvancerVite_, n++);
    graphe_= new LidoGrapheProfil();
    graphe_.setInteractif(true);
    graphe_.setEauVisible(true);
    graphe_.setPasPlanimetrageVisible(false);
    graphe_.setPreferredSize(new Dimension(320, 240));
    pnEdition_= new BuPanel();
    pnEdition_.setLayout(new BuVerticalLayout(5, false, false));
    pnEdition_.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    pnEdition_.add(pn1_);
    pnEdition_.add(pn2_);
    pnEdition_.add(new BuLabel("Bornes des axes"));
    edAxes_= new BGrapheEditeurAxes(graphe_);
    edAxes_.addActionListener(this);
    pnEdition_.add(edAxes_);
    cbZoom_= new JCheckBox("Zoom sur lit mineur");
    cbZoom_.addActionListener(this);
    cbZoom_.setSelected(graphe_.isZoomLitMineur());
    pnEdition_.add(cbZoom_);
    cbRives_= new JCheckBox("Affichage des rives");
    cbRives_.addActionListener(this);
    cbRives_.setSelected(graphe_.isRivesVisible());
    pnEdition_.add(cbRives_);
    cbAdjustZ_= new JCheckBox("Ajustement de la cote");
    cbAdjustZ_.addActionListener(this);
    cbAdjustZ_.setSelected(graphe_.isYAjuste());
    pnEdition_.add(cbAdjustZ_);
    cbFixAxes_= new JCheckBox("Fixer l'échelle");
    cbFixAxes_.addActionListener(this);
    cbFixAxes_.setSelected(graphe_.isFixAxes());
    pnEdition_.add(cbFixAxes_);
    cbPasPlani_= new JCheckBox("Affichage des pas");
    cbPasPlani_.addActionListener(this);
    cbPasPlani_.setSelected(graphe_.isPasPlanimetrageVisible());
    pnEdition_.add(cbPasPlani_);
    pnTout_= new BuPanel();
    pnTout_.setLayout(new BorderLayout());
    pnTout_.add(BorderLayout.WEST, pnEdition_);
    pnTout_.add(BorderLayout.CENTER, graphe_);
    getContentPane().add(BorderLayout.CENTER, pnTout_);
    setNavPanel(EbliPreferences.DIALOG.VALIDER);
    pack();
  }
  public void setParamsHelper(final LidoParamsHelper _ph) {
    if ((ph_ == _ph) || (_ph == null)) {
      return;
    }
    ph_= _ph;
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresPasLigneCAL)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    cl_= (SParametresPasLigneCAL)_n;
    indProfil_= cl_.profilDebut - 1;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(this, 0, LidoResource.CAL, cl_, "planimetrage");
  }
  public boolean restore() {
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      // A FAIRE : creer une copie de cl_
    }
    final double vp= cl_.taillePas;
    final Object val= tfHmax_.getValue();
    // valeur par defaut = 1m??
    if (val == null) {
      cl_.taillePas= 1. / LidoResource.PLANIMETRAGE.NBVALEURS_PLANI;
    } else {
      cl_.taillePas=
        ((Double)val).doubleValue() / LidoResource.PLANIMETRAGE.NBVALEURS_PLANI;
    }
    if (vp != cl_.taillePas) {
      changed= true;
    }
    if (changed) {
      firePropertyChange(
        "taillePas",
        new Double(vp),
        new Double(cl_.taillePas));
    }
    return changed;
  }
  protected void setValeurs() {
    if (cl_ == null) {
      return;
    }
    tfHmax_.setValue(
      new Double(cl_.taillePas * LidoResource.PLANIMETRAGE.NBVALEURS_PLANI));
    if (ph_ == null) {
      return;
    }
    //    SParametresBiefBlocPRO prof=ph_.PROFIL().profondeurMin(cl_.profilDebut, cl_.profilFin);
    final SParametresBiefBlocPRO prof= ph_.PROFIL().getProfil(cl_.profilDebut - 1);
    if (prof == null) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "ERREUR: numeros de profils\n" + "hors limites !")
        .activate();
    }
    if (indProfil_ == (cl_.profilDebut - 1)) {
      btReculer_.setEnabled(false);
      btReculerVite_.setEnabled(false);
    }
    if (indProfil_ == (cl_.profilFin - 1)) {
      btAvancer_.setEnabled(false);
      btAvancerVite_.setEnabled(false);
    }
    if ((cl_.profilFin - cl_.profilDebut) < 1) {
      btReculer_.setEnabled(false);
      btReculerVite_.setEnabled(false);
      btAvancer_.setEnabled(false);
      btAvancerVite_.setEnabled(false);
    }
    graphe_.setProfil(prof);
    graphe_.setNiveauEau(
      ph_.PROFIL().getFond(prof)
        + cl_.taillePas * LidoResource.PLANIMETRAGE.NBVALEURS_PLANI);
    //    btReculer_.setSelected(true);
    //    btAvancer_.setSelected(false);
    edAxes_.setAxes(graphe_.getAxes());
  }
  public void actionPerformed(final ActionEvent e) {
    String cmd= e.getActionCommand();
    if (cmd == null) {
      cmd= "";
    }
    final Object src= e.getSource();
    if ("VALIDER".equals(cmd)) {
      final SParametresPasLigneCAL vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    } else if (ph_ == null) {
      return;
    } else if (cmd.startsWith("RECULER")) {
      // ATTENTION: les indices de profil dans SParametrePRO sont indiceReel-1 !!
      //      graphe_.setProfil(ph_.PROFIL().profondeurMin(cl_.profilDebut, cl_.profilFin));
      if (!btAvancer_.isEnabled()) {
        btAvancer_.setEnabled(true);
      }
      if (!btAvancerVite_.isEnabled()) {
        btAvancerVite_.setEnabled(true);
      }
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
        indProfil_= Math.max(cl_.profilDebut - 1, indProfil_ - avt);
      } else {
        indProfil_--;
      }
      if (indProfil_ <= (cl_.profilDebut - 1)) {
        btReculer_.setEnabled(false);
        btReculerVite_.setEnabled(false);
      }
      graphe_.setProfil(ph_.PROFIL().getProfil(indProfil_));
      graphe_.setNiveauEau(
        ph_.PROFIL().getFond(ph_.PROFIL().getProfil(indProfil_))
          + cl_.taillePas * LidoResource.PLANIMETRAGE.NBVALEURS_PLANI);
    } else if (cmd.startsWith("AVANCER")) {
      //      graphe_.setProfil(ph_.PROFIL().profondeurMax(cl_.profilDebut, cl_.profilFin));
      if (!btReculer_.isEnabled()) {
        btReculer_.setEnabled(true);
      }
      if (!btReculerVite_.isEnabled()) {
        btReculerVite_.setEnabled(true);
      }
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
        indProfil_= Math.min(cl_.profilFin - 1, indProfil_ + avt);
      } else {
        indProfil_++;
      }
      if (indProfil_ >= (cl_.profilFin - 1)) {
        btAvancer_.setEnabled(false);
        btAvancerVite_.setEnabled(false);
      }
      graphe_.setProfil(ph_.PROFIL().getProfil(indProfil_));
      graphe_.setNiveauEau(
        ph_.PROFIL().getFond(ph_.PROFIL().getProfil(indProfil_))
          + cl_.taillePas * LidoResource.PLANIMETRAGE.NBVALEURS_PLANI);
    } else if (src == tfHmax_) {
      final SParametresPasLigneCAL vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      graphe_.setNiveauEau(
        ph_.PROFIL().getFond(ph_.PROFIL().getProfil(indProfil_))
          + cl_.taillePas * LidoResource.PLANIMETRAGE.NBVALEURS_PLANI);
    } else if (src == cbZoom_) {
      graphe_.setZoomLitMineur(cbZoom_.isSelected());
    } else if (src == cbRives_) {
      graphe_.setRivesVisible(cbRives_.isSelected());
    } else if (src == cbAdjustZ_) {
      graphe_.setYAjuste(cbAdjustZ_.isSelected());
    } else if (src == cbFixAxes_) {
      final boolean selected= cbFixAxes_.isSelected();
      if (selected) {
        cbZoom_.setSelected(false);
        graphe_.setZoomLitMineur(false);
        cbAdjustZ_.setSelected(false);
        graphe_.setYAjuste(false);
      }
      graphe_.setFixAxes(selected);
    } else if (src == cbPasPlani_) {
      graphe_.setPasPlanimetrageVisible(cbPasPlani_.isSelected());
    } else if (src == edAxes_) {
      final Double[][] axes= edAxes_.getAxes();
      graphe_.setAxes(axes[0], axes[1]);
    }
    edAxes_.setAxes(graphe_.getAxes());
  }
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    if (ph_ != null) {
      return ph_.getInformationsDocument();
    }
      return new BuInformationsDocument();
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
}
