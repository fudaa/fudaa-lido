/*
 * @file         LidoPermSectionsEditor.java
 * @creation     1999-09-14
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresSectionsCAL;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.dialog.BPanneauNavigation;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPermSectionsEditor extends LidoCustomizer {
  SParametresSectionsCAL cl_, clBck_;
  BuPanel pnTout_, pnBas_;
  JRadioButton[] rbs_;
  public LidoPermSectionsEditor() {
    this(null);
  }
  public LidoPermSectionsEditor(final BDialogContent parent) {
    super("Sections de calcul");
    cl_= clBck_= null;
    int n= 0;
    final BuLabel lbMilieu= new BuLabel("Mode de d�finition des sections de calcul");
    lbMilieu.setFont(
      lbMilieu.getFont().deriveFont(lbMilieu.getFont().getSize() + 2));
    lbMilieu.setForeground(Color.black);
    lbMilieu.setHorizontalAlignment(SwingConstants.CENTER);
    n= 0;
    pnBas_= new BuPanel();
    pnBas_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE))));
    pnBas_.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bg= new ButtonGroup();
    rbs_= new JRadioButton[4];
    for (int i= 0; i < 4; i++) {
      rbs_[i]= new JRadioButton();
      rbs_[i].addActionListener(this);
      bg.add(rbs_[i]);
    }
    pnBas_.add(rbs_[0], n++);
    pnBas_.add(
      new BuLabelMultiLine("les abscisses des profils sont prises\ncomme abscisses de sections de calcul"),
      n++);
    pnBas_.add(rbs_[1], n++);
    pnBas_.add(
      new BuLabelMultiLine("les abscisses des sections de calcul\nsont d�finies en s�ries"),
      n++);
    // ON VIRE LES CAS 3 ET 4 CAR ILS NE SONT JAMAIS UTILISES DANS LIDO ET EN PLUS ILS DECONNENT
    //    pnBas_.add(rbs_[2], n++);
    //    pnBas_.add(new BuMultiLabel("les sections de calcul sont\nd�finies une � une"), n++);
    //    pnBas_.add(rbs_[3], n++);
    //    pnBas_.add(new BuMultiLabel("les sections de calcul sont\nlues dans un fichier de ligne d'eau initiale"), n++);
    pnTout_= new BuPanel();
    pnTout_.setLayout(new BuVerticalLayout(5, false, true));
    pnTout_.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    pnTout_.add(lbMilieu);
    pnTout_.add(pnBas_);
    getContentPane().add(BorderLayout.CENTER, pnTout_);
    setActionPanel(BPanneauEditorAction.EDITER);
    setNavPanel(BPanneauNavigation.VALIDER | BPanneauNavigation.ANNULER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    final Object src= _evt.getSource();
    if ("VALIDER".equals(cmd)) {
      final SParametresSectionsCAL vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    } else if (src == rbs_[0]) {
      setEnabledAction(BPanneauEditorAction.EDITER, false);
    } else if (src == rbs_[1]) {
      setEnabledAction(BPanneauEditorAction.EDITER, true);
    }
  }
  public int getSelectedChoix() {
    for (int i= 0; i < 4; i++) {
      if (rbs_[i].isSelected()) {
        return i;
      }
    }
    return 0;
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresSectionsCAL)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    cl_= (SParametresSectionsCAL)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(this, 0, LidoResource.CAL, cl_, "type sections");
  }
  public boolean restore() {
    if (clBck_ == null) {
      return false;
    }
    // A FAIRE : recopier clBck_ dans cl_
    clBck_= null;
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      clBck_= new SParametresSectionsCAL();
      // A FAIRE : creer une copie de cl_
    }
    final int ov= cl_.nChoix;
    for (int i= 0; i < 4; i++) {
      if (rbs_[i].isSelected()) {
        cl_.nChoix= i + 1;
        break;
      }
    }
    if (ov != cl_.nChoix) {
      changed= true;
    }
    return changed;
  }
  protected void setValeurs() {
    rbs_[cl_.nChoix - 1].setSelected(true);
    if ((cl_.nChoix - 1) == 0) {
      setEnabledAction(BPanneauEditorAction.EDITER, false);
    } else {
      setEnabledAction(BPanneauEditorAction.EDITER, true);
    }
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
}
