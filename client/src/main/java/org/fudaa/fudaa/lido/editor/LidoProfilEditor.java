/*
 * @file         LidoProfilEditor.java
 * @creation     1999-05-27
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JCheckBox;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.dialog.BPanneauNavigation;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoDialogContraintes;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheProfil;
// ATTENTION: On ne gere ici que le cas de profils entres par points!!
// prevoir la gestion par largeurs ou transformer automatiquement en
// points au niveau de Dodico.
/**
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoProfilEditor extends LidoCustomizerImprimable//implements BuPrintable
{
  public static final int EDITER= 0x01;
  public static final int VOIR= 0x02;
  public static final int PROFILS= LidoResource.PROFIL.PROFILS;
  public static final int CALAGE= LidoResource.PROFIL.CALAGE;
  public static final int LEGER= 0x01;
  public static final int COMPLET= 0x02;
  private final static int PRECISION= LidoResource.PRECISION;
  SParametresBiefBlocPRO profil_;
  LidoGrapheProfil graphe_;
  int mode_;
  int mode2_;
  int mode3_;
  int indPoint_;
  BuPanel pnEditCoord_, pnEditLimLit_, pnEdit_, pnGraph_;
  BuLabel lbEditTitre_;
  BuTextField tfAbs_, tfCote_, tfAller_;
  BGrapheEditeurAxes edAxes_;
  JCheckBox cbZoom_, cbRives_, cbAdjustZ_, cbFixAxes_;
  JCheckBox cbLitMinGa_, cbLitMinDr_, cbLitMajGa_, cbLitMajDr_;
  BuCommonInterface app_;
  FudaaProjet p_;
  public LidoProfilEditor(final int mode, final int mode2, final FudaaProjet _p) {
    this(mode, mode2, COMPLET, _p);
  }
  public LidoProfilEditor(final int mode, final int mode2, final int mode3, final FudaaProjet _p) {
    super("Edition de profil");
    if (mode == VOIR) {
      mode_= VOIR;
    } else {
      mode_= EDITER;
    }
    if (mode2 == CALAGE) {
      mode2_= CALAGE;
    } else {
      mode2_= PROFILS;
    }
    if (mode3 == LEGER) {
      mode3_= LEGER;
    } else {
      mode3_= COMPLET;
    }
    app_= (BuCommonInterface)LidoApplication.FRAME;
    p_= _p;
    init();
  }
  public LidoProfilEditor(
    final BDialogContent _parent,
    final int mode,
    final int mode2,
    final FudaaProjet _p) {
    this(_parent, mode, mode2, COMPLET, _p);
  }
  public LidoProfilEditor(
    final BDialogContent _parent,
    final int mode,
    final int mode2,
    final int mode3,
    final FudaaProjet _p) {
    super(_parent, "Edition de profil");
    p_= _p;
    if (mode == VOIR) {
      mode_= VOIR;
    } else {
      mode_= EDITER;
    }
    if (mode2 == CALAGE) {
      mode2_= CALAGE;
    } else {
      mode2_= PROFILS;
    }
    if (mode3 == LEGER) {
      mode3_= LEGER;
    } else {
      mode3_= COMPLET;
    }
    app_= (BuCommonInterface)LidoApplication.FRAME;
    init();
  }
  public LidoProfilEditor(
    final BuCommonInterface app,
    final BDialogContent _parent,
    final int mode,
    final int mode2,
    final FudaaProjet _p) {
    this(app, _parent, mode, mode2, COMPLET, _p);
  }
  public LidoProfilEditor(
    final BuCommonInterface app,
    final BDialogContent _parent,
    final int mode,
    final int mode2,
    final int mode3,
    final FudaaProjet _p) {
    super(app, _parent, "Edition de profil");
    if (mode == VOIR) {
      mode_= VOIR;
    } else {
      mode_= EDITER;
    }
    if (mode2 == CALAGE) {
      mode2_= CALAGE;
    } else {
      mode2_= PROFILS;
    }
    if (mode3 == LEGER) {
      mode3_= LEGER;
    } else {
      mode3_= COMPLET;
    }
    app_= app;
    p_= _p;
    init();
  }
  private void init() {
    profil_= null;
    indPoint_= -1;
    graphe_= new LidoGrapheProfil();
    graphe_.setInteractif(true);
    addPropertyChangeListener(graphe_);
    if (mode_ == VOIR) {
      getContentPane().add(graphe_);
    } else {
      graphe_.setPreferredSize(new Dimension(320, 240));
      if (mode3_ == LEGER) {
        graphe_.setRivesVisible(false);
      }
      final Insets is= new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE);
      pnEditCoord_= new BuPanel();
      final BuGridLayout loEditCoord= new BuGridLayout();
      loEditCoord.setColumns(2);
      loEditCoord.setHgap(5);
      loEditCoord.setVgap(5);
      pnEditCoord_.setLayout(loEditCoord);
      tfAbs_= BuTextField.createDoubleField();
      tfAbs_.setColumns(5);
      final MyPointChangeAdapter absListener= new MyPointChangeAdapter();
      tfAbs_.addFocusListener(absListener);
      tfAbs_.addActionListener(absListener);
      int n= 0;
      pnEditCoord_.add(new BuLabel("Abscisse (t) "), n++);
      pnEditCoord_.add(tfAbs_, n++);
      tfCote_= BuTextField.createDoubleField();
      final MyPointChangeAdapter cotListener= new MyPointChangeAdapter();
      tfCote_.addFocusListener(cotListener);
      tfCote_.addActionListener(cotListener);
      pnEditCoord_.add(new BuLabel("Cote (Z) "), n++);
      pnEditCoord_.add(tfCote_, n++);
      tfAller_= BuTextField.createIntegerField();
      tfAller_.addActionListener(this);
      pnEditCoord_.add(new BuLabel("Aller � "), n++);
      pnEditCoord_.add(tfAller_, n++);
      pnEditLimLit_= new BuPanel();
      final BuGridLayout loEditLimLit= new BuGridLayout();
      loEditLimLit.setColumns(2);
      loEditLimLit.setHgap(5);
      loEditLimLit.setVgap(5);
      pnEditLimLit_.setLayout(loEditLimLit);
      n= 0;
      if (mode2_ == PROFILS && mode3_ == COMPLET) {
        pnEditLimLit_.add(new BuLabel("Berges "), n++);
        cbLitMinGa_= new JCheckBox("g");
        cbLitMinGa_.addActionListener(this);
        cbLitMinDr_= new JCheckBox("d");
        cbLitMinDr_.addActionListener(this);
        final BuPanel pnLitMin= new BuPanel();
        pnLitMin.add(cbLitMinGa_);
        pnLitMin.add(cbLitMinDr_);
        pnEditLimLit_.add(pnLitMin, n++);
      }
      if (mode2_ == CALAGE) {
        pnEditLimLit_.add(new BuLabel("Zones de stockage "), n++);
        cbLitMajGa_= new JCheckBox("g");
        cbLitMajGa_.addActionListener(this);
        cbLitMajDr_= new JCheckBox("d");
        cbLitMajDr_.addActionListener(this);
        final BuPanel pnLitMaj= new BuPanel();
        pnLitMaj.add(cbLitMajGa_);
        pnLitMaj.add(cbLitMajDr_);
        pnEditLimLit_.add(pnLitMaj, n++);
      }
      pnEdit_= new BuPanel();
      pnEdit_.setLayout(new BuVerticalLayout(5, true, false));
      pnEdit_.setBorder(
        new CompoundBorder(new EmptyBorder(is), new EtchedBorder()));
      n= 0;
      lbEditTitre_= new BuLabel("Pas de point");
      lbEditTitre_.setForeground(Color.black);
      pnEdit_.add(lbEditTitre_, n++);
      pnEdit_.add(pnEditCoord_, n++);
      pnEdit_.add(pnEditLimLit_, n++);
      pnEdit_.add(new BuLabel("Bornes des axes"), n++);
      edAxes_= new BGrapheEditeurAxes(graphe_);
      edAxes_.addActionListener(this);
      pnEdit_.add(edAxes_, n++);
      if (mode3_ == COMPLET) {
        cbZoom_= new JCheckBox("Zoom sur lit mineur");
        cbZoom_.addActionListener(this);
        cbZoom_.setSelected(graphe_.isZoomLitMineur());
        pnEdit_.add(cbZoom_, n++);
        cbRives_= new JCheckBox("Affichage des rives");
        cbRives_.addActionListener(this);
        cbRives_.setSelected(graphe_.isRivesVisible());
        pnEdit_.add(cbRives_, n++);
        cbAdjustZ_= new JCheckBox("Ajustement de la cote");
        cbAdjustZ_.addActionListener(this);
        cbAdjustZ_.setSelected(graphe_.isYAjuste());
        pnEdit_.add(cbAdjustZ_, n++);
        cbFixAxes_= new JCheckBox("Fixer l'�chelle");
        cbFixAxes_.addActionListener(this);
        cbFixAxes_.setSelected(graphe_.isFixAxes());
        pnEdit_.add(cbFixAxes_, n++);
      }
      if (mode2_ == CALAGE) {
        tfAbs_.setEnabled(false);
        tfCote_.setEnabled(false);
      }
      getContentPane().add(BorderLayout.WEST, pnEdit_);
      //pnGraph_=new BuPanel();
      //pnGraph_.setLayout(new BorderLayout());
      //pnGraph_.add(BorderLayout.CENTER, graphe_);
      //pnGraph_.setBorder(new EmptyBorder(is));
      //getContentPane().add(BorderLayout.CENTER, pnGraph_);
      getContentPane().add(BorderLayout.CENTER, graphe_);
      if (mode3_ == COMPLET) {
        setNavPanel(
          BPanneauNavigation.RECULER_VITE
            | BPanneauNavigation.RECULER
            | BPanneauNavigation.AVANCER
            | BPanneauNavigation.AVANCER_VITE
            | BPanneauNavigation.VALIDER
            | BPanneauNavigation.ALIGN_RIGHT);
      } else if (mode3_ == LEGER) {
        setNavPanel(
          BPanneauNavigation.VALIDER | BPanneauNavigation.ALIGN_RIGHT);
      }
      if (mode2_ == PROFILS) {
        setActionPanel(
          BPanneauEditorAction.CREER | BPanneauEditorAction.SUPPRIMER);
      }
      addAction("<<", BuResource.BU.getIcon("reculervite"), "POINTRECULERVITE");
      addAction("<", BuResource.BU.getIcon("reculer"), "POINTRECULER");
      addAction(">", BuResource.BU.getIcon("avancer"), "POINTAVANCER");
      addAction(">>", BuResource.BU.getIcon("avancervite"), "POINTAVANCERVITE");
    }
    pack();
  }
  class MyPointChangeAdapter extends FocusAdapter implements ActionListener {
    public void focusLost(final FocusEvent e) {
      eventOccurred();
    }
    public void actionPerformed(final ActionEvent e) {
      eventOccurred();
    }
    private void eventOccurred() {
      if (validePoint()) {
        objectModified();
        setValeurs();
        fireObjectChanged();
      }
    }
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresBiefBlocPRO)) {
      return;
    }
    if (_n == profil_) {
      return;
    }
    profil_= (SParametresBiefBlocPRO)_n;
    graphe_.setProfil(profil_);
    if (mode_ == VOIR) {
      return;
    }
    if (profil_.nbPoints > 0) {
      graphe_.setPointEdited(0);
      indPoint_= 0;
    } else {
      indPoint_= -1;
    }
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.PRO,
        profil_,
        "profil " + (profil_.numProfil));
  }
  public boolean restore() {
    return false;
  }
  protected void setValeurs() {
    if ((indPoint_ >= profil_.nbPoints) || (indPoint_ < 0)) {
      return;
    }
    if (profil_.nbPoints == 0) {
      tfAbs_.setText("");
      tfCote_.setText("");
      lbEditTitre_.setText("Pas de point");
      if (mode3_ == COMPLET) {
        if (mode2_ == PROFILS) {
          cbLitMinGa_.setSelected(false);
          cbLitMinDr_.setSelected(false);
        } else if (mode2_ == CALAGE) {
          cbLitMajGa_.setSelected(false);
          cbLitMajDr_.setSelected(false);
        }
      }
      repaint();
      return;
    }
    setTitle("Edition du profil " + profil_.numProfil);
    tfAbs_.setValue(new Double(profil_.abs[indPoint_]));
    tfCote_.setValue(new Double(profil_.cotes[indPoint_]));
    lbEditTitre_.setText("Point " + (indPoint_ + 1));
    if (mode3_ == COMPLET) {
      if (mode2_ == PROFILS) {
        // detection de limite lit mineur/lit majeur
        if ((Math.round(PRECISION * profil_.absMajMin[0])
          == Math.round(PRECISION * profil_.abs[indPoint_]))
          && (Math.round(PRECISION * profil_.coteRivGa)
            == Math.round(PRECISION * profil_.cotes[indPoint_]))) {
          cbLitMinGa_.setSelected(true);
        } else {
          cbLitMinGa_.setSelected(false);
        }
        if ((Math.round(PRECISION * profil_.absMajMin[1])
          == Math.round(PRECISION * profil_.abs[indPoint_]))
          && (Math.round(PRECISION * profil_.coteRivDr)
            == Math.round(PRECISION * profil_.cotes[indPoint_]))) {
          cbLitMinDr_.setSelected(true);
        } else {
          cbLitMinDr_.setSelected(false);
        }
      }
      if (mode2_ == CALAGE) {
        // detection de limite lit majeur/zone stockage
        if (Math.round(PRECISION * profil_.absMajSto[0])
          == Math.round(PRECISION * profil_.abs[indPoint_])) {
          cbLitMajGa_.setSelected(true);
        } else {
          cbLitMajGa_.setSelected(false);
        }
        if (Math.round(PRECISION * profil_.absMajSto[1])
          == Math.round(PRECISION * profil_.abs[indPoint_])) {
          cbLitMajDr_.setSelected(true);
        } else {
          cbLitMajDr_.setSelected(false);
        }
      }
    }
    edAxes_.setAxes(graphe_.getAxes());
    repaint();
  }
  protected boolean getValeurs() {
    return validePoint();
  }
  protected boolean isParoiVerticaleBergeGauche() {
    final long absBergeG= Math.round(PRECISION * profil_.absMajMin[0]);
    boolean firstTrouve= false;
    final int l= profil_.abs.length - 1;
    for (int i= l; i >= 0; i--) {
      if (absBergeG == Math.round(PRECISION * profil_.abs[i])) {
        if (firstTrouve) {
          return true;
        }
        firstTrouve= true;
      }
    }
    return false;
  }
  protected boolean isParoiVerticaleBergeDroite() {
    final long absBergeG= Math.round(PRECISION * profil_.absMajMin[1]);
    boolean firstTrouve= false;
    final int l= profil_.abs.length - 1;
    for (int i= l; i >= 0; i--) {
      if (absBergeG == Math.round(PRECISION * profil_.abs[i])) {
        if (firstTrouve) {
          return true;
        }
        firstTrouve= true;
      }
    }
    return false;
  }
  boolean validePoint() {
    if ((indPoint_ >= profil_.nbPoints) || (profil_.nbPoints <= 0)) {
      return false;
    }
    if (indPoint_ < 0) {
      return false;
    }
    // maj stockage et rive
    boolean stocG= false, stocD= false;
    boolean riveG= false, riveD= false;
    if (Math.round(PRECISION * profil_.absMajSto[0])
      == Math.round(PRECISION * profil_.abs[indPoint_])) {
      stocG= true;
    }
    if (Math.round(PRECISION * profil_.absMajSto[1])
      == Math.round(PRECISION * profil_.abs[indPoint_])) {
      stocD= true;
    }
    if ((Math.round(PRECISION * profil_.absMajMin[0])
      == Math.round(PRECISION * profil_.abs[indPoint_]))
      && (Math.round(PRECISION * profil_.coteRivGa)
        == Math.round(PRECISION * profil_.cotes[indPoint_]))) {
      riveG= true;
    }
    if ((Math.round(PRECISION * profil_.absMajMin[1])
      == Math.round(PRECISION * profil_.abs[indPoint_]))
      && (Math.round(PRECISION * profil_.coteRivDr)
        == Math.round(PRECISION * profil_.cotes[indPoint_]))) {
      riveD= true;
    }
    boolean changed= false;
    final double newAbs= ((Double)tfAbs_.getValue()).doubleValue();
    final double newCote= ((Double)tfCote_.getValue()).doubleValue();
    if (profil_.abs[indPoint_] != newAbs) {
      //dans le cas de paroi verticale sur la berge de gauche
      //et on deplace le point vers l'interieur
      //il ne faut pas toucher la zone de stockage.
      if (stocG
        && (Math.round(PRECISION * profil_.absMajMin[0])
          == Math.round(PRECISION * profil_.abs[indPoint_]))
        && (newAbs > profil_.abs[indPoint_])
        && isParoiVerticaleBergeGauche()) {
        stocG= false;
      }
      //dans le cas de paroi verticale sur la berge de droite (idem)
      if (stocD
        && (Math.round(PRECISION * profil_.absMajMin[1])
          == Math.round(PRECISION * profil_.abs[indPoint_]))
        && (newAbs < profil_.abs[indPoint_])
        && isParoiVerticaleBergeDroite()) {
        stocD= false;
      }
      if (stocG) {
        profil_.absMajSto[0]= newAbs;
      } else if (stocD) {
        profil_.absMajSto[1]= newAbs;
      }
      if (riveG) {
        profil_.absMajMin[0]= newAbs;
      } else if (riveD) {
        profil_.absMajMin[1]= newAbs;
      }
      profil_.abs[indPoint_]= newAbs;
      changed= true;
    }
    if (profil_.cotes[indPoint_] != newCote) {
      if (riveD) {
        profil_.coteRivDr= newCote;
      }
      if (riveG) {
        profil_.coteRivGa= newCote;
      }
      profil_.cotes[indPoint_]= newCote;
      changed= true;
    }
    if (profil_.abs.length > 0) {
      if (stocG && stocD) {
        profil_.absMajSto[0]= profil_.abs[0];
        profil_.absMajSto[1]= profil_.abs[profil_.abs.length - 1];
      }
      if (riveG && riveD) {
        profil_.absMajMin[0]= profil_.abs[0];
        profil_.coteRivGa= profil_.cotes[0];
        profil_.absMajMin[1]= profil_.abs[profil_.abs.length - 1];
        profil_.coteRivDr= profil_.cotes[profil_.abs.length - 1];
      }
    }
    //pour etre sur
    if (profil_.absMajSto[0] > profil_.absMajMin[0]) {
      profil_.absMajSto[0]= profil_.absMajMin[0];
    }
    if (profil_.absMajSto[1] < profil_.absMajMin[1]) {
      profil_.absMajSto[1]= profil_.absMajMin[1];
    }
    return changed;
  }
  public void actionPerformed(final ActionEvent e) {
    String cmd= e.getActionCommand();
    if (cmd == null) {
      cmd= "";
    }
    final Object src= e.getSource();
    if ("VALIDER".equals(cmd)) {
      fermer();
    } else if (src == tfAller_) {
      final Integer a= (Integer)tfAller_.getValue();
      if (a == null) {
        return;
      }
      int ai= a.intValue() - 1;
      if (ai < 0) {
        ai= 0;
      } else if (ai >= profil_.nbPoints) {
        ai= profil_.nbPoints - 1;
      }
      indPoint_= ai;
      graphe_.setPointEdited(indPoint_);
    } else if (cmd.startsWith("POINTAVANCER")) {
      if (validePoint()) {
        objectModified();
      }
      if (cmd.endsWith("VITE")) {
        final int avt=
          LidoPreferences.LIDO.getIntegerProperty("profil.point.rapide", 5);
        if (indPoint_ >= (profil_.nbPoints - avt)) {
          indPoint_= indPoint_ + avt - profil_.nbPoints;
        } else {
          indPoint_ += avt;
        }
      } else {
        if (indPoint_ >= (profil_.nbPoints - 1)) {
          indPoint_= 0;
        } else {
          indPoint_++;
        }
      }
      // A FAIRE: reclasser les points
      if (lbEditTitre_.getForeground() == Color.red) {
        lbEditTitre_.setForeground(Color.black);
      }
      graphe_.setPointEdited(indPoint_);
    } else if (cmd.startsWith("POINTRECULER")) {
      if (validePoint()) {
        objectModified();
      }
      if (cmd.endsWith("VITE")) {
        final int avt=
          LidoPreferences.LIDO.getIntegerProperty("profil.point.rapide", 5);
        if (indPoint_ <= avt) {
          indPoint_= profil_.nbPoints + (indPoint_ - avt);
        } else {
          indPoint_ -= avt;
        }
      } else {
        if (indPoint_ <= 0) {
          indPoint_= profil_.nbPoints - 1;
        } else {
          indPoint_--;
        }
      }
      if (lbEditTitre_.getForeground() == Color.red) {
        lbEditTitre_.setForeground(Color.black);
      }
      graphe_.setPointEdited(indPoint_);
    } else if ("SUPPRIMER".equals(cmd)) {
      if (profil_.nbPoints < 1) {
        return;
      }
      final double[] tmpAbs= profil_.abs;
      final double[] tmpCotes= profil_.cotes;
      profil_.nbPoints--;
      profil_.abs= new double[profil_.nbPoints];
      profil_.cotes= new double[profil_.nbPoints];
      int n= 0;
      for (int i= 0; i < profil_.nbPoints + 1; i++) {
        if (i != indPoint_) {
          profil_.abs[n]= tmpAbs[i];
          profil_.cotes[n]= tmpCotes[i];
          n++;
        } else {
          System.err.println("Point " + (indPoint_ + 1) + " supprim�");
        }
      }
      final int suppr= indPoint_;
      if (indPoint_ >= profil_.nbPoints) {
        indPoint_= profil_.nbPoints - 1;
      }
      if (profil_.nbPoints == 0) { // dernier point enleve
        profil_.absMajMin[0]= 0.;
        profil_.coteRivGa= 0.;
        profil_.absMajMin[1]= 0.;
        profil_.coteRivDr= 0.;
        profil_.absMajSto[0]= 0.;
        profil_.absMajSto[1]= 0.;
        // ne pas oublier de gerer altMinMaj, etc...
        System.err.println("Dernier point supprim�");
      } else {
        if ((Math.round(PRECISION * profil_.absMajMin[0])
          == Math.round(PRECISION * tmpAbs[suppr]))
          && (Math.round(PRECISION * profil_.coteRivGa)
            == Math.round(PRECISION * tmpCotes[suppr]))) {
          profil_.absMajMin[0]= profil_.abs[0];
          profil_.coteRivGa= profil_.cotes[0];
          System.err.println("Rive gauche supprim�e");
        }
        if ((Math.round(PRECISION * profil_.absMajMin[1])
          == Math.round(PRECISION * tmpAbs[suppr]))
          && (Math.round(PRECISION * profil_.coteRivDr)
            == Math.round(PRECISION * tmpCotes[suppr]))) {
          profil_.absMajMin[1]= profil_.abs[indPoint_];
          profil_.coteRivDr= profil_.cotes[indPoint_];
          System.err.println("Rive droite supprim�e");
        }
        if (Math.round(PRECISION * profil_.absMajSto[0])
          == Math.round(PRECISION * tmpAbs[suppr])) {
          profil_.absMajSto[0]= profil_.abs[0];
          System.err.println("Zone stockage gauche supprim�e");
        }
        if (Math.round(PRECISION * profil_.absMajSto[1])
          == Math.round(PRECISION * tmpAbs[suppr])) {
          profil_.absMajSto[1]= profil_.abs[indPoint_];
          System.err.println("Zone stockage droite supprim�e");
        }
      }
      graphe_.setPointEdited(indPoint_);
      objectModified();
    } else if ("CREER".equals(cmd)) {
      final double[] tmpAbs= profil_.abs;
      final double[] tmpCotes= profil_.cotes;
      profil_.nbPoints++;
      profil_.abs= new double[profil_.nbPoints];
      profil_.cotes= new double[profil_.nbPoints];
      indPoint_++; // on insere APRES indPoint_
      if (profil_.nbPoints > 1) {
        for (int i= 0; i < indPoint_; i++) {
          profil_.abs[i]= tmpAbs[i];
          profil_.cotes[i]= tmpCotes[i];
        }
        profil_.abs[indPoint_]= profil_.abs[indPoint_ - 1];
        profil_.cotes[indPoint_]= profil_.cotes[indPoint_ - 1];
        System.err.println("Point " + (indPoint_ + 1) + " ins�r�");
        for (int i= indPoint_ + 1; i < profil_.nbPoints; i++) {
          profil_.abs[i]= tmpAbs[i - 1];
          profil_.cotes[i]= tmpCotes[i - 1];
        }
      }
      graphe_.setPointEdited(indPoint_);
      lbEditTitre_.setForeground(Color.red);
      objectModified();
    }
    if (src == cbLitMinGa_) {
      if (cbLitMinGa_.isSelected()) {
        if (cbLitMinDr_.isSelected()) {
          cbLitMinGa_.setSelected(false);
          return;
        }
        profil_.absMajMin[0]= profil_.abs[indPoint_];
        profil_.coteRivGa= profil_.cotes[indPoint_];
        // GERER le cas d'entree de profils par largeurs:
        //profil_.altMinMaj[0]=profil_.cotes[indPoint_];
      } else {
        profil_.absMajMin[0]= profil_.abs[0];
        profil_.coteRivGa= profil_.cotes[0];
      }
      fireObjectChanged();
      objectModified();
    } else if (src == cbLitMinDr_) {
      if (cbLitMinDr_.isSelected()) {
        if (cbLitMinGa_.isSelected()) {
          cbLitMinDr_.setSelected(false);
          return;
        }
        profil_.absMajMin[1]= profil_.abs[indPoint_];
        profil_.coteRivDr= profil_.cotes[indPoint_];
        // GERER le cas d'entree de profils par largeurs:
        //profil_.altMinMaj[0]=profil_.cotes[indPoint_];
      } else {
        profil_.absMajMin[1]= profil_.abs[profil_.nbPoints - 1];
        profil_.coteRivDr= profil_.cotes[profil_.nbPoints - 1];
      }
      fireObjectChanged();
      objectModified();
    } else if (src == cbLitMajGa_) {
      if (cbLitMajGa_.isSelected()) {
        if (cbLitMajDr_.isSelected()) {
          cbLitMajGa_.setSelected(false);
          return;
        }
        profil_.absMajSto[0]= profil_.abs[indPoint_];
        // GERER le cas d'entree de profils par largeurs:
        //profil_.altMajSto[0]=profil_.cotes[indPoint_];
      } else {
        profil_.absMajSto[0]= profil_.abs[0];
      }
      fireObjectChanged();
      objectModified();
    } else if (src == cbLitMajDr_) {
      if (cbLitMajDr_.isSelected()) {
        if (cbLitMajGa_.isSelected()) {
          cbLitMajDr_.setSelected(false);
          return;
        }
        profil_.absMajSto[1]= profil_.abs[indPoint_];
        // GERER le cas d'entree de profils par largeurs:
        //profil_.altMajSto[0]=profil_.cotes[indPoint_];
      } else {
        profil_.absMajSto[1]= profil_.abs[profil_.nbPoints - 1];
      }
      fireObjectChanged();
      objectModified();
    } else if (src == cbZoom_) {
      graphe_.setZoomLitMineur(cbZoom_.isSelected());
    } else if (src == cbRives_) {
      graphe_.setRivesVisible(cbRives_.isSelected());
    } else if (src == cbAdjustZ_) {
      graphe_.setYAjuste(cbAdjustZ_.isSelected());
    } else if (src == cbFixAxes_) {
      final boolean selected= cbFixAxes_.isSelected();
      if (selected) {
        cbZoom_.setSelected(false);
        graphe_.setZoomLitMineur(false);
        cbAdjustZ_.setSelected(false);
        graphe_.setYAjuste(false);
      }
      graphe_.setFixAxes(selected);
    } else if (src == edAxes_) {
      final Double[][] axes= edAxes_.getAxes();
      graphe_.setAxes(axes[0], axes[1]);
    }
    setValeurs();
  }
  private boolean verifieContraintes() {
    if (mode3_ == LEGER) {
      return true;
    }
    String alert= "";
    boolean can_ignore= true;
    // verif du nombre de points
    if (profil_.nbPoints <= 0) {
      alert += "Le profil ne contient aucun point\n\n";
    } else {
      if (mode2_ == PROFILS) {
        // verification des limites du lit mineur
        if (Math.round(PRECISION * profil_.absMajMin[0])
          == Math.round(PRECISION * profil_.absMajMin[1])) {
          alert += "Les rives sont confondues\n\n";
        } else if ((profil_.absMajMin[0] > profil_.absMajMin[1])) {
          alert += "Les rives sont invers�es\n\n";
          can_ignore= false;
        }
        if ((Math.round(PRECISION * profil_.absMajMin[0])
          == Math.round(PRECISION * profil_.abs[0]))
          && (Math.round(PRECISION * profil_.coteRivGa)
            == Math.round(PRECISION * profil_.cotes[0]))) {
          alert += "La rive gauche est fix�e au premier point du profil\n\n";
        }
        if ((Math.round(PRECISION * profil_.absMajMin[1])
          == Math.round(PRECISION * profil_.abs[profil_.nbPoints - 1]))
          && (Math.round(PRECISION * profil_.coteRivDr)
            == Math.round(PRECISION * profil_.cotes[profil_.nbPoints - 1]))) {
          alert += "La rive droite est fix�e au dernier point du profil\n\n";
        }
      }
      if (mode2_ == CALAGE) {
        // verification des limites du lit majeur
        if (Math.round(PRECISION * profil_.absMajSto[0])
          == Math.round(PRECISION * profil_.absMajSto[1])) {
          alert += "Les zones de stockage sont confondues\n\n";
        } else if ((profil_.absMajSto[0] > profil_.absMajSto[1])) {
          alert += "Les zones de stockage sont invers�es\n\n";
          can_ignore= false;
        }
        if (Math.round(PRECISION * profil_.absMajSto[0])
          == Math.round(PRECISION * profil_.abs[0])) {
          alert += "La zone stockage gauche n'existe pas\n\n";
        }
        if (Math.round(PRECISION * profil_.absMajSto[1])
          == Math.round(PRECISION * profil_.abs[profil_.nbPoints - 1])) {
          alert += "La zone stockage droite n'existe pas\n\n";
        }
      }
      // verification des rives/zones stockage
      if ((profil_.absMajSto[0] > profil_.absMajMin[0])) {
        alert += "Les zones de stockage et rive gauches sont invers�es\n\n";
        can_ignore= false;
      }
      if ((profil_.absMajSto[1] < profil_.absMajMin[1])) {
        alert += "Les zones de stockage et rive droites sont invers�es\n\n";
        can_ignore= false;
      }
      if (mode2_ == PROFILS) {
        // verification des parois verticales
        final int[] vert= new int[profil_.nbPoints];
        int n= 0;
        vert[0]= -1;
        for (int i= 1; i < profil_.nbPoints; i++) {
          vert[i]= -1; // affectation par defaut
          if (profil_.abs[i - 1] == profil_.abs[i]) {
            vert[n++]= i;
          }
        }
        if (vert[0] != -1) {
          alert += "D�tection de parois verticales sur les points:\n";
        }
        for (int i= 0; i < profil_.nbPoints; i++) {
          if (vert[i] != -1) {
            alert += "  " + vert[i] + "-" + (vert[i] + 1) + "\n";
          }
        }
      }
    }
    if (alert.equals("")) {
      return true;
    }
    final int resp=
      new LidoDialogContraintes(
        app_,
        app_.getImplementation().getInformationsSoftware(),
        new String("Attention:\n\n" + alert),
        can_ignore)
        .activate();
    if (resp == LidoDialogContraintes.IGNORER) {
      return true;
    }
    return false;
  }
  protected void fireObjectChanged() {
    firePropertyChange("profil", null, profil_);
  }
  public void fermer() {
    boolean ok= true;
    // verifier les contraintes: -les deux rives ont ete selectionnees
    //                           -pas de pente verticale
    //                           -...
    ok= verifieContraintes();
    if (ok) {
      super.fermer();
    }
  }
  /*public void print(PrintJob _j, Graphics _g)
  {
    if( graphe_==null ) return;
    graphe_.setName(getTitle());
    graphe_.print(_j, _g);
  }*/
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    return p_.getInformationsDocument();
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == profil_);
  }
}
