/*
 * @file         LidoSingulariteChooser.java
 * @creation     1999-08-18
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.Container;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;

import org.fudaa.fudaa.lido.LidoApplication;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoSingulariteChooser extends BDialog {
  JRadioButton btSeuilNoye_,
    btSeuilDenoye_,
    btSeuilGeom_,
    btLimni_,
    btTarageAmont_,
    btTarageAval_;
  BuGridLayout loBoutons_;
  boolean permanent_;
  public LidoSingulariteChooser(final boolean permanent) {
    super((BuCommonInterface)LidoApplication.FRAME);
    setTitle("Choix de Singularit�");
    setLocationRelativeTo(LidoApplication.FRAME);
    permanent_= permanent;
    init();
  }
  public LidoSingulariteChooser(final IDialogInterface parent, final boolean permanent) {
    super((BuCommonInterface)LidoApplication.FRAME, parent);
    setTitle("Choix de Singularit�");
    setLocationRelativeTo(parent.getComponent());
    permanent_= permanent;
    init();
  }
  private void init() {
    loBoutons_= new BuGridLayout(2, 5, 5, true, true);
    final Container content= getContentPane();
    content.setLayout(loBoutons_);
    int n= 0;
    btSeuilNoye_= new JRadioButton("Seuil noy�");
    btSeuilNoye_.setActionCommand("SING:SEUILNOY");
    content.add(btSeuilNoye_, n++);
    if (!permanent_) {
      btLimni_= new JRadioButton("Limnigramme amont");
      btLimni_.setActionCommand("SING:LIMNIAMONT");
      content.add(btLimni_, n++);
    }
    btSeuilDenoye_= new JRadioButton("Seuil d�noy�");
    btSeuilDenoye_.setActionCommand("SING:SEUILDENOY");
    content.add(btSeuilDenoye_, n++);
    if (!permanent_) {
      btTarageAmont_= new JRadioButton("Tarage amont");
      btTarageAmont_.setActionCommand("SING:TARAGEAMONT");
      content.add(btTarageAmont_, n++);
    }
    btSeuilGeom_= new JRadioButton("Seuil g�om�trique");
    btSeuilGeom_.setActionCommand("SING:SEUILGEOM");
    content.add(btSeuilGeom_, n++);
    if (!permanent_) {
      btTarageAval_= new JRadioButton("Tarage aval");
      btTarageAval_.setActionCommand("SING:TARAGEAVAL");
      content.add(btTarageAval_, n++);
    }
    pack();
  }
  public void addActionListener(final ActionListener l) {
    btSeuilNoye_.addActionListener(l);
    btSeuilDenoye_.addActionListener(l);
    btSeuilGeom_.addActionListener(l);
    if (!permanent_) {
      btLimni_.addActionListener(l);
      btTarageAmont_.addActionListener(l);
      btTarageAval_.addActionListener(l);
    }
  }
  public void removeActionListener(final ActionListener l) {
    btSeuilNoye_.removeActionListener(l);
    btSeuilDenoye_.removeActionListener(l);
    btSeuilGeom_.removeActionListener(l);
    if (!permanent_) {
      btLimni_.removeActionListener(l);
      btTarageAmont_.removeActionListener(l);
      btTarageAval_.removeActionListener(l);
    }
  }
}
