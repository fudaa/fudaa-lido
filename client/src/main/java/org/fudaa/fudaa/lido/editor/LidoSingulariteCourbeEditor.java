/**
 * @creation     1999-08-24
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.memoire.bu.*;
import com.memoire.fu.FuVectorint;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.tableau.EbliGrapheValeur;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheSingulariteCourbe;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauAbstract;
import org.fudaa.fudaa.seuil.SeuilApplication;
import org.fudaa.fudaa.seuil.SeuilImplementation;
import org.fudaa.fudaa.seuil.SeuilLido;
/**
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoSingulariteCourbeEditor
  extends LidoCustomizerImprimable
  implements ChangeListener {
  public final static int TARAGE_AMONT= LidoResource.SINGULARITE.TARAGE_AMONT;
  public final static int TARAGE_AVAL= LidoResource.SINGULARITE.TARAGE_AVAL;
  public final static int LIMNI_AMONT= LidoResource.SINGULARITE.LIMNI_AMONT;
  public final static int SEUIL_GEOM= LidoResource.SINGULARITE.SEUIL_GEOM;
  protected static int INBPOINT= LidoResource.SINGULARITE.ICOURBE.INBPOINT;
  private static final int IHT= LidoResource.SINGULARITE.ICOURBE.IHT;
  // TARAGE: cotes
  private static final int IHL= LidoResource.SINGULARITE.ICOURBE.IHL; // LIMNI: cotes
  private static final int IQT= LidoResource.SINGULARITE.ICOURBE.IQT;
  // TARAGE: debits
  private static final int ITL= LidoResource.SINGULARITE.ICOURBE.ITL; // LIMNI: temps
  private static final int IYS= LidoResource.SINGULARITE.ICOURBE.IYS;
  // SEUIL: abs crete
  private static final int IZS= LidoResource.SINGULARITE.ICOURBE.IZS;
  // SEUIL: cotes crete
  private static final int IVARMAXCOT= LidoResource.SINGULARITE.ICOURBE.IVARMAXCOT;
  // LIMNI: variation max cote
  private static final int ICOTMOY= LidoResource.SINGULARITE.ICOURBE.ICOTMOY;
  private static final int ICOEFDEBIT= LidoResource.SINGULARITE.ICOURBE.ICOEFDEBIT;
  // SEUIL: coeff debit
  protected SParametresSingBlocSNG cl_;
  // specifique
  protected BuTextField tfSpecific_, tfCotMoy_;
  protected BuButton btCreer_,
    btSupprimer_,
    btSupprimerLoi_,
    btDeselectionner_,
    btCotMoy_;
  protected BuButton btUtiliseSeuil_;
  protected DebitTable tbDebit_;
  protected LidoParamsHelper ph_;
  protected LidoGrapheSingulariteCourbe graphe_;
  protected int type_;
  protected int IABS;
  protected int IORD;
  protected double abs_;
  protected String TAB_LABEL;
  protected String TAB_HEADER_COL0;
  protected String TAB_HEADER_COL1;
  protected String GRA_TITLE;
  protected String GRA_ABS_LABEL;
  protected String GRA_ORD_LABEL;
  protected String GRA_ABS_UNITE;
  protected String GRA_ORD_UNITE;
  protected SeuilImplementation imp_;
  public LidoSingulariteCourbeEditor(final int type, final LidoParamsHelper ph) {
    super(
      "Edition de Singularit� : "
        + (type == TARAGE_AMONT
          ? "tarage amont"
          : type == TARAGE_AVAL
          ? "tarage aval"
          : type == LIMNI_AMONT
          ? "limnigramme amont"
          : type == SEUIL_GEOM
          ? "seuil g�om�trique"
          : null));
    type_= type;
    ph_= ph;
    init();
  }
  public LidoSingulariteCourbeEditor(
    final BDialogContent parent,
    final int type,
    final LidoParamsHelper ph) {
    super(
      parent,
      "Edition de Singularit� : "
        + (type == TARAGE_AMONT
          ? "tarage amont"
          : type == TARAGE_AVAL
          ? "tarage aval"
          : type == LIMNI_AMONT
          ? "limnigramme amont"
          : type == SEUIL_GEOM
          ? "seuil g�om�trique"
          : null));
    type_= type;
    ph_= ph;
    init();
  }
  public LidoSingulariteCourbeEditor(
    final BDialogContent parent,
    final int type,
    final LidoParamsHelper ph,
    final double abs) {
    super(
      parent,
      "Edition de Singularit� : "
        + (type == TARAGE_AMONT
          ? "tarage amont"
          : type == TARAGE_AVAL
          ? "tarage aval"
          : type == LIMNI_AMONT
          ? "limnigramme amont"
          : type == SEUIL_GEOM
          ? "seuil g�om�trique"
          : null));
    abs_= abs;
    type_= type;
    ph_= ph;
    init();
  }
  private void init() {
    if (type_ == TARAGE_AMONT) {
      IABS= IHT;
      IORD= IQT;
      TAB_LABEL= "Courbe de tarage\n� l'amont du seuil";
      TAB_HEADER_COL0= "Z amont (m)";
      TAB_HEADER_COL1= "D�bit Q (m3/s)";
      GRA_TITLE= "tarage amont";
      GRA_ABS_LABEL= "Z";
      GRA_ORD_LABEL= "Q";
      GRA_ABS_UNITE= "m";
      GRA_ORD_UNITE= "m3/s";
    } else if (type_ == TARAGE_AVAL) {
      IABS= IHT;
      IORD= IQT;
      TAB_LABEL= "Courbe de tarage\n� l'aval du seuil";
      TAB_HEADER_COL0= "Z aval (m)";
      TAB_HEADER_COL1= "D�bit Q (m3/s)";
      GRA_TITLE= "tarage aval";
      GRA_ABS_LABEL= "Z";
      GRA_ORD_LABEL= "Q";
      GRA_ABS_UNITE= "m";
      GRA_ORD_UNITE= "m3/s";
    } else if (type_ == LIMNI_AMONT) {
      IABS= ITL;
      IORD= IHL;
      TAB_LABEL= "Limnigramme\n� l'amont du seuil";
      TAB_HEADER_COL0= "Temps (s)";
      TAB_HEADER_COL1= "Cote amont (m)";
      GRA_TITLE= "limni amont";
      GRA_ABS_LABEL= "T";
      GRA_ORD_LABEL= "Z";
      GRA_ABS_UNITE= "s";
      GRA_ORD_UNITE= "m";
    } else if (type_ == SEUIL_GEOM) {
      IABS= IYS;
      IORD= IZS;
      TAB_LABEL= "D�finition g�om�trique\ndu seuil";
      TAB_HEADER_COL0= "Abscisse t";
      TAB_HEADER_COL1= "Cote Z";
      GRA_TITLE= "seuil";
      GRA_ABS_LABEL= "t";
      GRA_ORD_LABEL= "Z";
      GRA_ABS_UNITE= "m";
      GRA_ORD_UNITE= "m";
    }
    final Container content= getContentPane();
    //-------- Edition ----------//
    //-------------------------
    int n= 0;
    //-------------------------
    //-------------------------
    final BuPanel pn3= new BuPanel();
    final BuGridLayout lo3=
      new BuGridLayout(2, INSETS_SIZE, INSETS_SIZE, true, false);
    pn3.setLayout(lo3);
    n= 0;
    if (type_ == SEUIL_GEOM) {
      tfSpecific_= BuTextField.createDoubleField();
      tfSpecific_.setDisplayFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
      tfSpecific_.addActionListener(this);
      tfSpecific_.setActionCommand("COEFDEBIT");
      tfSpecific_.setColumns(8);
      pn3.add(new BuLabel("Coefficient de d�bit du seuil"), n++);
      pn3.add(tfSpecific_, n++);
      tfCotMoy_= BuTextField.createDoubleField();
      //      tfCotMoy_=new BuTextField();
      tfCotMoy_.setDisplayFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
      tfCotMoy_.addActionListener(this);
      tfCotMoy_.setActionCommand("COTMOY");
      tfCotMoy_.setColumns(8);
      tfCotMoy_.setEditable(true);
      pn3.add(new BuLabel("Cote moyenne du seuil"), n++);
      pn3.add(tfCotMoy_, n++);
      btCotMoy_= new BuButton("Recalculer");
      btCotMoy_.setActionCommand("RECALCULER");
      btCotMoy_.addActionListener(this);
      pn3.add(new BuLabel(""), n++);
      pn3.add(btCotMoy_, n++);
    }
    if (type_ == LIMNI_AMONT) {
      tfSpecific_= BuTextField.createDoubleField();
      tfSpecific_.setDisplayFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
      tfSpecific_.addActionListener(this);
      tfSpecific_.setActionCommand("VARMAXCOT");
      tfSpecific_.setColumns(8);
      pn3.add(
        new BuLabelMultiLine("Variation max de la cote\npendant le pas de temps"),
        n++);
      pn3.add(tfSpecific_, n++);
    }
    //-------------------------
    //-------------------------
    final BuPanel pn2= new BuPanel();
    pn2.setLayout(new BuGridLayout(1, INSETS_SIZE, INSETS_SIZE, false, false));
    n= 0;
    // case 0:0
    final BuPanel pnDebitTable= new BuPanel();
    pnDebitTable.setBorder(new LineBorder(Color.black));
    pnDebitTable.setLayout(new BuVerticalLayout(0, false, false));
    pnDebitTable.add(new BuLabelMultiLine(TAB_LABEL), 0);
    tbDebit_= new DebitTable();
    addPropertyChangeListener(tbDebit_);
    final JTableHeader header= tbDebit_.getTableHeader();
    pnDebitTable.add(header, 1);
    pnDebitTable.add(tbDebit_, 2);
    header.setReorderingAllowed(false);
    header.addMouseListener(new MouseAdapter() {
      public void mouseReleased(final MouseEvent _evt) {
        if (header.getResizingColumn() != null) {
          return;
        }
        final TableColumnModel model= header.getColumnModel();
        final int colSelectedIndex= model.getColumnIndexAtX(_evt.getX());
        if (colSelectedIndex == 0) {
          tbDebit_.sortTableDonneesLois();
        }
      }
    });
    pn2.add(pnDebitTable, n++);
    final JScrollPane sp2= new JScrollPane(pn2);
    sp2.setBorder(
      new CompoundBorder(
        new BevelBorder(BevelBorder.LOWERED),
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE))));
    sp2.setPreferredSize(
      new Dimension(Math.max(250, pn3.getPreferredSize().width), 150));
    //-------------------------
    //-------------------------
    final BuPanel pn4= new BuPanel();
    btCreer_= new BuButton();
    btCreer_.setToolTipText("Ins�rer");
    btCreer_.setIcon(BuResource.BU.getIcon("creer"));
    btCreer_.setActionCommand("CREER");
    btCreer_.addActionListener(this);
    pn4.add(btCreer_);
    btSupprimer_= new BuButton();
    btSupprimer_.setToolTipText("supprimer");
    btSupprimer_.setIcon(BuResource.BU.getIcon("detruire"));
    btSupprimer_.setActionCommand("SUPPRIMER");
    btSupprimer_.addActionListener(this);
    pn4.add(btSupprimer_);
    btDeselectionner_= new BuButton();
    btDeselectionner_.setToolTipText("d�s�lectionner tout");
    btDeselectionner_.setIcon(BuResource.BU.getIcon("texte"));
    btDeselectionner_.setActionCommand("TOUTDESELECTIONNER");
    btDeselectionner_.addActionListener(this);
    pn4.add(btDeselectionner_);
    //-------------------------
    //-------------------------
    final BuPanel pn5= new BuPanel();
    btSupprimerLoi_= new BuButton("Supprimer cette loi");
    btSupprimerLoi_.setActionCommand("SUPPRIMERLOI");
    btSupprimerLoi_.addActionListener(this);
    pn5.add(btSupprimerLoi_);
    //-------------------------
    final BuPanel pn6= new BuPanel();
    btUtiliseSeuil_= new BuButton("Utiliser le Logiciel Seuil");
    //temporaire en attendant seuil
    if (!new File(System.getProperty("user.dir")
      + File.separator
      + "serveurs"
      + File.separator
      + "seuil")
      .exists()) {
      btUtiliseSeuil_.setEnabled(false);
    }
    btUtiliseSeuil_.setActionCommand("UTILISEUIL");
    btUtiliseSeuil_.addActionListener(this);
    pn6.add(btUtiliseSeuil_);
    //-------------------------
    final BuPanel pnEdition= new BuPanel();
    pnEdition.setLayout(new BuVerticalLayout(INSETS_SIZE, false, false));
    pnEdition.add(sp2);
    pnEdition.add(pn3);
    pnEdition.add(pn4);
    pnEdition.add(pn5);
    if (type_ == SEUIL_GEOM) {
      pnEdition.add(pn6);
    }
    pnEdition.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    //-------- Graphe ----------//
    graphe_=
      new LidoGrapheSingulariteCourbe(
        IABS,
        IORD,
        GRA_TITLE,
        GRA_ABS_LABEL,
        GRA_ORD_LABEL,
        GRA_ABS_UNITE,
        GRA_ORD_UNITE);
    graphe_.setInteractif(true);
    addPropertyChangeListener(graphe_);
    tbDebit_.addPropertyChangeListener(graphe_);
    graphe_.setPreferredSize(new Dimension(320, 240));
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ALIGN_RIGHT);
    content.add(BorderLayout.WEST, pnEdition);
    content.add(BorderLayout.CENTER, graphe_);
    pack();
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      if (tbDebit_.isEditing()) {
        System.err.println("Table validate");
        tbDebit_.editingStopped(new ChangeEvent(this));
      }
      if (getValeurs()) {
        objectModified();
        firePropertyChange("singularite", null, cl_);
      }
      if (verifieContraintes()) {
        tbDebit_.sortTableDonneesLois();
        fermer();
      }
    } else if ("RECALCULER".equals(cmd)) {
      if (type_ == SEUIL_GEOM) {
        calculeCoteMoyenne();
      }
      setValeurs();
    } else if ("CREER".equals(cmd)) {
      if (tbDebit_ == null) {
        return;
      }
      int pos[]= tbDebit_.getSelectedRows();
      if (pos.length == 0) {
        pos= new int[] { -1 };
      }
      ph_.LOISNG().nouveauPoint(cl_, 0, pos[0]);
      firePropertyChange("singularite", null, cl_);
      setValeurs();
    } else if ("SUPPRIMER".equals(cmd)) {
      if (tbDebit_ == null) {
        return;
      }
      ph_.LOISNG().supprimePoints(cl_, tbDebit_.getSelectedRows(), 0);
      //      tbDebit_.tableChanged(new TableModelEvent(tbDebit_.getModel()));
      firePropertyChange("singularite", null, cl_);
      setValeurs();
    } else if ("TOUTDESELECTIONNER".equals(cmd)) {
      tbDebit_.clearSelection();
      tbDebit_.editingStopped(new ChangeEvent(this));
    } else if ("SUPPRIMERLOI".equals(cmd)) {
      ph_.LOISNG().supprimeLoisng(cl_);
      final SParametresSingBlocSNG old= cl_;
      cl_= null;
      firePropertyChange("loisupprimee", old, null);
      fermer();
    } else if ("COTMOY".equals(cmd)) {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("singularite", null, cl_);
      }
    } else if ("UTILISEUIL".equals(cmd)) {
      lanceSeuil();
    }
  }
  private boolean verifieContraintes() {
    return true;
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresSingBlocSNG)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    final SParametresSingBlocSNG vp= cl_;
    cl_= (SParametresSingBlocSNG)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.SNG,
        cl_,
        "singularite " + cl_.numSing);
    firePropertyChange("singularite", vp, cl_);
  }
  public boolean restore() {
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    /*    int newNbDebit=((Integer)tfNbPoint_.getValue()).intValue();
        int newNbCotAval=((Integer)tfNbCotAval_.getValue()).intValue();
    
        if( cl_.tabParamEntier[INBPOINT]!=newNbDebit ) {
          cl_.tabParamEntier[INBPOINT]=newNbDebit;
          changed=true;
        }
        if( cl_.tabParamEntier[INBCOTAVAL]!=newNbCotAval ) {
          cl_.tabParamEntier[INBCOTAVAL]=newNbCotAval;
          changed=true;
        }*/
    if (type_ == SEUIL_GEOM) {
      final double newCoefDebit= ((Double)tfSpecific_.getValue()).doubleValue();
      if (cl_.tabParamReel[ICOEFDEBIT] != newCoefDebit) {
        cl_.tabParamReel[ICOEFDEBIT]= newCoefDebit;
        changed= true;
      }
      final double cotMoy= ((Double)tfCotMoy_.getValue()).doubleValue();
      if (cl_.tabParamReel[ICOTMOY] != cotMoy) {
        cl_.tabParamReel[ICOTMOY]= cotMoy;
        changed= true;
      }
    }
    if (type_ == LIMNI_AMONT) {
      final double newVarMaxCot= ((Double)tfSpecific_.getValue()).doubleValue();
      if (cl_.tabParamReel[IVARMAXCOT] != newVarMaxCot) {
        cl_.tabParamReel[IVARMAXCOT]= newVarMaxCot;
        changed= true;
      }
    }
    return changed;
    /*    if( cl_==null ) {
          clBck_=null;
        } else {
          clBck_=new SParametresBiefSingLigneRZO();
          clBck_.numBief=cl_.numBief;
          clBck_.xSing=cl_.xSing;
          clBck_.nSing=cl_.nSing;
        }*/
  }
  protected void setValeurs() {
    if (cl_.tabParamReel.length <= 0) {
      System.err.println("LidoSingulariteCourbeEditor : tabParamReel vide");
      return;
    }
    if (type_ == SEUIL_GEOM) {
      tfCotMoy_.setValue(new Double(cl_.tabParamReel[ICOTMOY]));
      tfSpecific_.setValue(new Double(cl_.tabParamReel[ICOEFDEBIT]));
    }
    if (type_ == LIMNI_AMONT) {
      tfSpecific_.setValue(new Double(cl_.tabParamReel[IVARMAXCOT]));
    }
  }
  private double calculeCoteMoyenne() {
    if (cl_ == null
      || cl_.tabParamReel.length <= 0
      || cl_.tabParamEntier.length <= 0
      || cl_.tabParamEntier[INBPOINT] == 0) {
      return 0.;
    }
    double cotMoy= 0.;
    for (int i= 1; i < cl_.tabParamEntier[INBPOINT]; i++) {
      final double t1= cl_.tabDonLois[IABS][i - 1];
      final double z1= cl_.tabDonLois[IORD][i - 1];
      final double t2= cl_.tabDonLois[IABS][i];
      final double z2= cl_.tabDonLois[IORD][i];
      cotMoy += (z1 + z2) * (t2 - t1) / 2.;
    }
    final double t1= cl_.tabDonLois[IABS][0];
    double tn= t1;
    if (cl_.tabParamEntier[INBPOINT] > 0) {
      tn= cl_.tabDonLois[IABS][cl_.tabParamEntier[INBPOINT] - 1];
    }
    if ((tn - t1 == 0.)) {
      cotMoy= 0.;
    } else {
      cotMoy /= (tn - t1);
    }
    cl_.tabParamReel[ICOTMOY]= cotMoy;
    return cotMoy;
  }
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    return ph_.getInformationsDocument();
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
  class DebitTable
    extends LidoTableauAbstract
    implements PropertyChangeListener {
    boolean isInit_= false;
    private TableModelEvent evt_;
    public DebitTable() {
      super();
      setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
      //  DebitTable.this.init();
    }
    public void propertyChange(final PropertyChangeEvent e) {
      if ("singularite".equals(e.getPropertyName())) {
        reinitialise();
      }
    }
    void feuPropertyChange(final String p, final Object ov, final Object nv) {
      super.firePropertyChange(p, ov, nv);
    }
    protected void sortTableDonneesLois() {
      final EbliGrapheValeur grapheValeur= new EbliGrapheValeur(true);
      int nb= cl_.tabParamEntier[INBPOINT];
      boolean b;
      double[][] tableau= cl_.tabDonLois;
      final FuVectorint d= new FuVectorint(nb / 2);
      for (int i= 0; i < nb; i++) {
        b= grapheValeur.addXY(tableau[IABS][i], tableau[IORD][i]);
        if (!b) {
          d.addElement(i);
        }
      }
      if (d.size() > 0) {
        ph_.LOISNG().supprimePoints(cl_, d.toArray(), 0);
        final StringBuffer s=
          new StringBuffer("Les abscisses suivantes sont supprim�es (en double):\n");
        s.append(d.elementAt(0));
        final int n= d.size();
        for (int i= 1; i < n; i++) {
          s.append(", ").append(tableau[IABS][d.elementAt(i)]);
        }
        new BuDialogMessage(null, null, s.toString()).activate();
      }
      nb= cl_.tabParamEntier[INBPOINT];
      final EbliGrapheValeur.ValeurField[] newVal= grapheValeur.getValeurField();
      if (newVal.length != nb) {
        System.err.println("le tri a echou�");
        new BuDialogMessage(null, null, "le tri a echou�").activate();
      } else {
        tableau= cl_.tabDonLois;
        for (int i= 0; i < nb; i++) {
          tableau[IABS][i]= newVal[i].getX();
          tableau[IORD][i]= newVal[i].getY();
        }
        objectModified();
        setValeurs();
        if (evt_ == null) {
          evt_= new TableModelEvent(DebitTable.this.getModel());
        }
        DebitTable.this.tableChanged(evt_);
        DebitTable.this.feuPropertyChange("singularite", null, cl_);
      }
    }
    private void reinitialise() {
      if (!isInit_) {
        init();
      }
      if (evt_ == null) {
        evt_= new TableModelEvent(DebitTable.this.getModel());
      }
      DebitTable.this.tableChanged(evt_);
    }
    private void init() {
      if ((cl_ == null)
        || (cl_.tabDonLois == null)
        || (cl_.tabDonLois.length < (IORD + 1))
        || (cl_.tabDonLois.length < (IABS + 1))) {
        return;
      }
      if (isEditing()) {
        System.err.println("canceling edition");
        editingCanceled(new ChangeEvent(this));
      }
      setModel(new TableModel() {
        public Class getColumnClass(final int column) {
          return Double.class;
        }
        public int getColumnCount() {
          return 2;
        }
        public String getColumnName(final int column) {
          //          String res=null;
          //          switch(column) {
          //          case 0: res=TAB_HEADER_COL0; break;
          //          case 1: res=TAB_HEADER_COL1; break;
          //          }
          //          return res;
          return column == 0 ? TAB_HEADER_COL0 : TAB_HEADER_COL1;
        }
        public int getRowCount() {
          return cl_.tabParamEntier[INBPOINT];
        }
        public Object getValueAt(final int row, final int column) {
          //          Object res=null;
          //          switch(column) {
          //          case 0: res=new Double(cl_.tabDonLois[IABS][row]); break;
          //          case 1: res=new Double(cl_.tabDonLois[IORD][row]); break;
          //          }
          //          return res;
          return new Double(cl_.tabDonLois[column == 0 ? IABS : IORD][row]);
        }
        public boolean isCellEditable(final int row, final int column) {
          return true;
        }
        public void setValueAt(final Object value, final int row, final int column) {
          switch (column) {
            case 0 :
              cl_.tabDonLois[IABS][row]= ((Double)value).doubleValue();
              break;
            case 1 :
              cl_.tabDonLois[IORD][row]= ((Double)value).doubleValue();
              break;
          }
          objectModified();
          setValeurs();
          DebitTable.this.feuPropertyChange("singularite", null, cl_);
        }
        public void addTableModelListener(final TableModelListener _l) {}
        public void removeTableModelListener(final TableModelListener _l) {}
      });
      //      TableCellRenderer tcr=LidoParamsHelper.createDoubleCellRenderer();
      //      TableCellEditor tceDouble=LidoParamsHelper.createDoubleCellEditor();
      //      int n=getModel().getColumnCount();
      //      TableColumnModel model=getColumnModel();
      //      for(int i=0;i<n;i++) {
      //        model.getColumn(i).setCellRenderer(tcr);
      //        model.getColumn(i).setCellEditor(tceDouble);
      //      }
    }
  }
  private void lanceSeuil() {
    SeuilLido.t.addChangeListener(this);
    //BuPreferences.BU.applyLookAndFeel();
    // Affichage de la fenetre stdout/stderr
    final String ls= System.getProperty("line.separator");
    final String wlcmsg=
      "******************************************************************************"
        + ls
        + "*                             Bienvenue dans Seuil                           *"
        + ls
        + "*                             ------------------                             *"
        + ls
        + "* Ceci est la console texte. Elle affiche tous les messages systeme:         *"
        + ls
        + "* erreurs, taches en cours. Consultez-la regulierement pour savoir           *"
        + ls
        + "* si le programme est actif, si une erreur s'est poduite, ...                *"
        + ls
        + "* En cas d'erreur, joignez son contenu (enregistre dans le fichier ts.log)   *"
        + ls
        + "* au mail de notification de bogue, ceci nous aidera a comprendre.           *"
        + ls
        + "******************************************************************************"
        + ls
        + ls;
    System.out.println(wlcmsg);
    System.out.println("Client Seuil");
    //BuPreferences.BU.applyLanguage(SeuilImplementation.informationsSoftware().languages);
    // Splash
    final BuSplashScreen ss=
      new BuSplashScreen(
        SeuilImplementation.informationsSoftware(),
        3000,
        new String[0][0]    /*,
                 SeuilImplementation.class.getName()*/
    );
    ss.start();
    // NetCheck
    ss.setProgression(60);
    SeuilApplication apps;
    apps= new SeuilApplication();
    System.out.println("Initialisation...");
    ss.setText("Initialisation...");
    ss.setProgression(80);
    apps.init();
    final JFrame fts= new JFrame("Console");
    fts.pack();
    ((SeuilImplementation)apps.getImplementation()).setConsole(fts);
    ss.setProgression(100);
    ss.setVisible(false);
    ss.dispose();
    try {
      apps.start();
    } catch (final Throwable e) {
      final ByteArrayOutputStream out= new ByteArrayOutputStream();
      e.printStackTrace(new PrintStream(out));
      new BuDialogError(
        apps,
        SeuilImplementation.informationsSoftware(),
        out.toString())
        .activate();
      try {
        out.close();
      } catch (final IOException ioe) {};
    }
    int kam, kav;
    kav= ph_.PROFIL().getIndiceProfilAval(abs_);
    kam= ph_.PROFIL().getIndiceProfilAmont(abs_);
    final SParametresBiefBlocPRO proAm= ph_.PROFIL().getProfil(kam);
    final SParametresBiefBlocPRO proAv= ph_.PROFIL().getProfil(kav);
    imp_= (SeuilImplementation)apps.getImplementation();
    imp_.lidocreer(proAm, proAv);
  }
  /*
    public void getValeursSeuil()
    {
      double refam;
      double largw;
      double tm;
      double x1,y1,x2,y2,x3,y3,x4,y4;
  
      System.out.println("seuilResults.coefSeuil = "+imp_.seuilResults.resultatsSEU().coefSeuil );
      SParametresSEU parm_=(SParametresSEU)imp_.projet_.getParam(SeuilResource.SEUIL01);
      System.out.println("pelle    = "+parm_.pelle );
      System.out.println("largeur  = "+parm_.largEcoul);
      System.out.println("oblicite = "+parm_.oblicite);
      refam =  parm_.profAmont.xy[0].y ;
      for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
      {
        if(  parm_.profAmont.xy[i].y < refam)
        {
        refam = parm_.profAmont.xy[i].y ;
        }
      }
  
      largw =parm_.largEcoul * Math.cos(parm_.oblicite);
      tm =  (parm_.profAmont.xy[0].x + parm_.profAmont.xy[parm_.profAmont.nbPoints-1].x)/2.;
      x1 = tm-largw/2.;
      x4=  tm+largw/2.;
      x2 = x1;
      x3 = x4;
      y1 = refam;
      y4 = refam;
      y2 = refam+ parm_.pelle;
      y3 = y2;
  
      cl_.tabParamReel[ICOEFDEBIT]  =  imp_.seuilResults.resultatsSEU().coefSeuil;
      cl_.tabParamEntier[INBPOINT]  =  4 ;
      cl_.tabDonLois[IABS][0]  =   x1 ;
      cl_.tabDonLois[IORD][0]   =  y1 ;
      cl_.tabDonLois[IABS][1]  =   x2 ;
      cl_.tabDonLois[IORD][1]   =  y2 ;
      cl_.tabDonLois[IABS][2]  =   x3 ;
      cl_.tabDonLois[IORD][2]   =  y3 ;
      cl_.tabDonLois[IABS][3]  =   x4 ;
      cl_.tabDonLois[IORD][3]   =  y4 ;
  
      //calculeCoteMoyenne();
  
  
      firePropertyChange("singularite", null, cl_);    //pour dessin et table
      calculeCoteMoyenne();
      setValeurs();     // pour coef debit
    }
  */
  public void stateChanged(final ChangeEvent evt) {
    final Object source= evt.getSource();
    System.out.println("Lido ChangEvent source = " + source);
    if (source instanceof SeuilLido) {
      cl_.tabParamReel[ICOEFDEBIT]= SeuilLido.t.coefdebit;
      System.out.println("Lido coefSeuil = " + cl_.tabParamReel[ICOEFDEBIT]);
      cl_.tabParamEntier[INBPOINT]= 4;
      cl_.tabDonLois[IABS][0]= SeuilLido.t.x1;
      cl_.tabDonLois[IORD][0]= SeuilLido.t.y1;
      cl_.tabDonLois[IABS][1]= SeuilLido.t.x2;
      cl_.tabDonLois[IORD][1]= SeuilLido.t.y2;
      cl_.tabDonLois[IABS][2]= SeuilLido.t.x3;
      cl_.tabDonLois[IORD][2]= SeuilLido.t.y3;
      cl_.tabDonLois[IABS][3]= SeuilLido.t.x4;
      cl_.tabDonLois[IORD][3]= SeuilLido.t.y4;
      firePropertyChange("singularite", null, cl_); // pour dessin et table
      calculeCoteMoyenne();
      setValeurs(); // pour coef debit
    }
  }
}
