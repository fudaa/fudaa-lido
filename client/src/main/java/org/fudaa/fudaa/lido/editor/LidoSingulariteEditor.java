/*
 * @file         LidoSingulariteEditor.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.lido.SParametresBiefSingLigneRZO;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BPanneauEditorAction;
import org.fudaa.ebli.dialog.BPanneauNavigation;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoSingulariteEditor
  extends LidoCustomizer
  implements ActionListener {
  public static final int ITYPE= LidoResource.SINGULARITE.ITYPE;
  public final static int SEUIL_NOYE= LidoResource.SINGULARITE.SEUIL_NOYE;
  public final static int SEUIL_DENOYE= LidoResource.SINGULARITE.SEUIL_DENOYE;
  public final static int SEUIL_GEOM= LidoResource.SINGULARITE.SEUIL_GEOM;
  public final static int LIMNI_AMONT= LidoResource.SINGULARITE.LIMNI_AMONT;
  public final static int TARAGE_AMONT= LidoResource.SINGULARITE.TARAGE_AMONT;
  public final static int TARAGE_AVAL= LidoResource.SINGULARITE.TARAGE_AVAL;
  BuTextField tfNumero_, tfAbsSeuil_, tfNumLoiSeuil_;
  BuPanel pnEdition_;
  BuGridLayout loEdition_;
  private SParametresBiefSingLigneRZO cl_, clBck_;
  public LidoSingulariteEditor() {
    super("Edition de Singularité");
    init();
  }
  public LidoSingulariteEditor(final BDialogContent parent) {
    super(parent, "Edition de Singularité");
    init();
  }
  private void init() {
    cl_= clBck_= null;
    loEdition_= new BuGridLayout(2, 5, 5, false, false);
    final Container pnMain_= getContentPane();
    pnEdition_= new BuPanel();
    pnEdition_.setLayout(loEdition_);
    pnEdition_.setBorder(
      new CompoundBorder(
        new EtchedBorder(),
        new EmptyBorder(new Insets(5, 5, 5, 5))));
    final int textSize= 10;
    int n= 0;
    tfNumero_= BuTextField.createIntegerField();
    tfNumero_.setColumns(textSize);
    tfNumero_.setEditable(false);
    pnEdition_.add(new BuLabel("Numéro de bief"), n++);
    pnEdition_.add(tfNumero_, n++);
    tfAbsSeuil_= BuTextField.createDoubleField();
    tfAbsSeuil_.setColumns(textSize);
    pnEdition_.add(new BuLabel("Abscisse du seuil"), n++);
    pnEdition_.add(tfAbsSeuil_, n++);
    tfNumLoiSeuil_= BuTextField.createIntegerField();
    tfNumLoiSeuil_.setColumns(textSize);
    //tfNumLoiSeuil_.setValueValidator(new BuNumericValueValidator(true, 1, 3));
    pnEdition_.add(new BuLabel("Numéro loi de seuil"), n++);
    pnEdition_.add(tfNumLoiSeuil_, n++);
    setActionPanel(BPanneauEditorAction.EDITER);
    setNavPanel(BPanneauNavigation.VALIDER);
    pnMain_.add(pnEdition_, BorderLayout.CENTER);
    pack();
  }
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd= _evt.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      final SParametresBiefSingLigneRZO vp= cl_;
      if (getValeurs()) {
        objectModified();
        firePropertyChange("object", vp, cl_);
      }
      fermer();
    }
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresBiefSingLigneRZO)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    cl_= (SParametresBiefSingLigneRZO)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        cl_,
        "singularite " + (cl_.numBief));
  }
  public boolean restore() {
    if (clBck_ == null) {
      return false;
    }
    cl_.numBief= clBck_.numBief;
    cl_.xSing= clBck_.xSing;
    cl_.nSing= clBck_.nSing;
    clBck_= null;
    return true;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (cl_ == null) {
      clBck_= null;
    } else {
      clBck_= new SParametresBiefSingLigneRZO();
      clBck_.numBief= cl_.numBief;
      clBck_.xSing= cl_.xSing;
      clBck_.nSing= cl_.nSing;
    }
    Number val= null;
    // numBief n'est pas editable
    //val=(Integer)tfNumero_.getValue();
    //if( val==null ) cl_.numBief=0;
    //else cl_.numBief=val.intValue();
    final double ox= cl_.xSing;
    val= (Double)tfAbsSeuil_.getValue();
    if (val == null) {
      cl_.xSing= 0.;
    } else {
      cl_.xSing= val.doubleValue();
    }
    if (ox != cl_.xSing) {
      changed= true;
    }
    final int on= cl_.nSing;
    val= (Integer)tfNumLoiSeuil_.getValue();
    if (val == null) {
      cl_.nSing= 0;
    } else {
      cl_.nSing= val.intValue();
    }
    if (on != cl_.nSing) {
      changed= true;
    }
    return changed;
  }
  protected void setValeurs() {
    tfNumero_.setValue(new Integer(cl_.numBief + 1));
    tfAbsSeuil_.setValue(new Double(cl_.xSing));
    tfNumLoiSeuil_.setValue(new Integer(cl_.nSing));
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
}
