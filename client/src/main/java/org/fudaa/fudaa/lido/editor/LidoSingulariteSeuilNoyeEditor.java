/*
 * @file         LidoSingulariteSeuilNoyeEditor.java
 * @creation     1999-08-18
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheSeuilNoye;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauAbstract;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoSingulariteSeuilNoyeEditor
  extends LidoCustomizerImprimable
  implements ListSelectionListener {
  static final int ICOTAVAL= LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAVAL;
  static final int IDEBIT= LidoResource.SINGULARITE.ISEUIL_NOYE.IDEBIT;
  static final int INBCOTAVAL= LidoResource.SINGULARITE.ISEUIL_NOYE.INBCOTAVAL;
  static final int INBDEBIT= LidoResource.SINGULARITE.ISEUIL_NOYE.INBDEBIT;
  static final int ICOTMOY= LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTMOY;
  static final int ICOTAMONTOFFSET=
    LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET;
  SParametresSingBlocSNG cl_;
  private LidoParamsHelper ph_;
  private BuTextField tfCoteMoy_, tfNbDebit_, tfNbCotAval_;
  private BuButton btCreer_, btSupprimer_, btSupprimerLoi_, btDeselectionner_;
  private DebitTable tbDebit_;
  private CotesAvalTable tbCotesAval_;
  private CotesAmontTable tbCotesAmont_;
  private LidoGrapheSeuilNoye graphe_;
  int varEdition_;
  public LidoSingulariteSeuilNoyeEditor(final LidoParamsHelper ph) {
    super("Edition de Singularit� : seuil noy�");
    ph_= ph;
    init();
  }
  public LidoSingulariteSeuilNoyeEditor(
    final BDialogContent parent,
    final LidoParamsHelper ph) {
    super(parent, "Edition de Singularit� : seuil noy�");
    ph_= ph;
    init();
  }
  private void init() {
    varEdition_=
      LidoResource.SINGULARITE.DEBIT | LidoResource.SINGULARITE.COTEAVAL;
    final Container content= getContentPane();
    //-------- Edition ----------//
    //-------------------------
    final BuPanel pn1= new BuPanel();
    pn1.setLayout(new BuGridLayout(2, INSETS_SIZE, INSETS_SIZE, false, false));
    int n= 0;
    tfCoteMoy_= BuTextField.createDoubleField();
    tfCoteMoy_.addActionListener(this);
    tfCoteMoy_.setColumns(8);
    pn1.add(new BuLabel("Cote moyenne de la crete du seuil"), n++);
    pn1.add(tfCoteMoy_, n++);
    //-------------------------
    //-------------------------
    final BuPanel pn2= new BuPanel();
    pn2.setLayout(new BuGridLayout(2, INSETS_SIZE, INSETS_SIZE, false, false));
    n= 0;
    // case 0:0 vide
    pn2.add(new BuLabel(""), n++);
    // case 0:1
    final BuPanel pnDebitTable= new BuPanel();
    pnDebitTable.setBorder(new LineBorder(Color.black));
    pnDebitTable.setLayout(new BuVerticalLayout(0, false, false));
    pnDebitTable.add(new BuLabel("D�bit"));
    tbDebit_= new DebitTable();
    tbDebit_.addMouseListener(new MouseAdapter() {
      public void mousePressed(final MouseEvent e) {
        varEdition_= LidoResource.SINGULARITE.DEBIT;
      }
    });
    addPropertyChangeListener(tbDebit_);
    tbDebit_.getColumnModel().getSelectionModel().addListSelectionListener(
      this);
    pnDebitTable.add(tbDebit_);
    pnDebitTable.addMouseListener(new MouseAdapter() {
      public void mousePressed(final MouseEvent e) {
        varEdition_= LidoResource.SINGULARITE.DEBIT;
      }
    });
    pn2.add(pnDebitTable, n++);
    // case 1:0
    final BuPanel pnCotesAvalTable= new BuPanel();
    pnCotesAvalTable.setBorder(new LineBorder(Color.black));
    pnCotesAvalTable.setLayout(new BuVerticalLayout(0, false, false));
    pnCotesAvalTable.add(new BuLabel("Cote aval"));
    tbCotesAval_= new CotesAvalTable();
    tbCotesAval_.addMouseListener(new MouseAdapter() {
      public void mousePressed(final MouseEvent e) {
        varEdition_= LidoResource.SINGULARITE.COTEAVAL;
      }
    });
    addPropertyChangeListener(tbCotesAval_);
    tbCotesAval_.getSelectionModel().addListSelectionListener(this);
    pnCotesAvalTable.add(tbCotesAval_);
    pnCotesAvalTable.addMouseListener(new MouseAdapter() {
      public void mousePressed(final MouseEvent e) {
        varEdition_= LidoResource.SINGULARITE.COTEAVAL;
      }
    });
    pn2.add(pnCotesAvalTable, n++);
    // case 1:1
    final BuPanel pnCotesAmontTable= new BuPanel();
    pnCotesAmontTable.setBorder(new LineBorder(Color.black));
    pnCotesAmontTable.setLayout(new BuVerticalLayout(0, false, false));
    pnCotesAmontTable.add(new BuLabel("Cote amont"));
    tbCotesAmont_= new CotesAmontTable();
    addPropertyChangeListener(tbCotesAmont_);
    pnCotesAmontTable.add(tbCotesAmont_);
    pn2.add(pnCotesAmontTable, n++);
    final JScrollPane sp2= new JScrollPane(pn2);
    sp2.setBorder(
      new CompoundBorder(
        new BevelBorder(BevelBorder.LOWERED),
        new EmptyBorder(
          new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE))));
    sp2.setPreferredSize(new Dimension(pn1.getPreferredSize().width, 150));
    //-------------------------
    //-------------------------
    final BuPanel pn3= new BuPanel();
    pn3.setLayout(new BuGridLayout(2, INSETS_SIZE, INSETS_SIZE, true, true));
    n= 0;
    tfNbDebit_= BuTextField.createIntegerField();
    tfNbDebit_.setEditable(false);
    tfNbDebit_.addActionListener(this);
    tfNbDebit_.setActionCommand("NBDEBIT");
    tfNbDebit_.setColumns(8);
    pn3.add(new BuLabel("Nb de d�bits de r�f�rence"), n++);
    pn3.add(tfNbDebit_, n++);
    tfNbCotAval_= BuTextField.createIntegerField();
    tfNbCotAval_.setEditable(false);
    tfNbCotAval_.addActionListener(this);
    tfNbCotAval_.setActionCommand("NBCOTAVAL");
    tfNbCotAval_.setColumns(8);
    pn3.add(new BuLabel("Nb de cotes aval de r�f�rence"), n++);
    pn3.add(tfNbCotAval_, n++);
    //-------------------------
    //-------------------------
    final BuPanel pn4= new BuPanel();
    btCreer_= new BuButton();
    btCreer_.setToolTipText("Ins�rer");
    btCreer_.setIcon(BuResource.BU.getIcon("creer"));
    btCreer_.setActionCommand("CREER");
    btCreer_.addActionListener(this);
    pn4.add(btCreer_);
    btSupprimer_= new BuButton();
    btSupprimer_.setToolTipText("supprimer");
    btSupprimer_.setIcon(BuResource.BU.getIcon("detruire"));
    btSupprimer_.setActionCommand("SUPPRIMER");
    btSupprimer_.addActionListener(this);
    pn4.add(btSupprimer_);
    btDeselectionner_= new BuButton();
    btDeselectionner_.setToolTipText("d�s�lectionner tout");
    btDeselectionner_.setIcon(BuResource.BU.getIcon("texte"));
    btDeselectionner_.setActionCommand("TOUTDESELECTIONNER");
    btDeselectionner_.addActionListener(this);
    pn4.add(btDeselectionner_);
    //-------------------------
    //-------------------------
    final BuPanel pn5= new BuPanel();
    btSupprimerLoi_= new BuButton("Supprimer cette loi");
    btSupprimerLoi_.setActionCommand("SUPPRIMERLOI");
    btSupprimerLoi_.addActionListener(this);
    pn5.add(btSupprimerLoi_);
    //-------------------------
    final BuPanel pnEdition= new BuPanel();
    pnEdition.setLayout(new BuVerticalLayout(INSETS_SIZE, false, false));
    pnEdition.add(pn1);
    pnEdition.add(sp2);
    pnEdition.add(pn3);
    pnEdition.add(pn4);
    pnEdition.add(pn5);
    pnEdition.setBorder(
      new EmptyBorder(
        new Insets(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE)));
    //-------- Graphe ----------//
    graphe_= new LidoGrapheSeuilNoye();
    addPropertyChangeListener(graphe_);
    tbDebit_.addPropertyChangeListener(graphe_);
    tbCotesAval_.addPropertyChangeListener(graphe_);
    tbCotesAmont_.addPropertyChangeListener(graphe_);
    graphe_.setPreferredSize(new Dimension(320, 240));
    content.add(BorderLayout.WEST, pnEdition);
    content.add(BorderLayout.CENTER, graphe_);
    setNavPanel(
      EbliPreferences.DIALOG.VALIDER | EbliPreferences.DIALOG.ALIGN_RIGHT);
    pack();
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    final Object src= e.getSource();
    if ("VALIDER".equals(cmd)) {
      if (tbDebit_.isEditing()) {
        System.err.println("Table validate");
        tbDebit_.editingStopped(new ChangeEvent(this));
      }
      if (tbCotesAval_.isEditing()) {
        System.err.println("Table validate");
        tbCotesAval_.editingStopped(new ChangeEvent(this));
      }
      if (tbCotesAmont_.isEditing()) {
        System.err.println("Table validate");
        tbCotesAmont_.editingStopped(new ChangeEvent(this));
      }
      if (getValeurs()) {
        objectModified();
        firePropertyChange("singularite", null, cl_);
      }
      if (verifieContraintes()) {
        fermer();
      }
    } else if ("NBDEBIT".equals(cmd)) {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("singularite", null, cl_);
      }
    }
    if ("NBCOTAVAL".equals(cmd)) {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("singularite", null, cl_);
      }
    } else if (src == tfCoteMoy_) {
      if (getValeurs()) {
        objectModified();
        firePropertyChange("singularite", null, cl_);
      }
    } else if ("CREER".equals(cmd)) {
      if (tbDebit_ == null) {
        return;
      }
      if (tbCotesAval_ == null) {
        return;
      }
      int[] pos= null;
      if ((varEdition_ & LidoResource.SINGULARITE.DEBIT) != 0) {
        pos= tbDebit_.getSelectedColumns();
      }
      if ((varEdition_ & LidoResource.SINGULARITE.COTEAVAL) != 0) {
        pos= tbCotesAval_.getSelectedRows();
      }
      if ((pos == null) || (pos.length == 0)) {
        pos= new int[] { -1 };
      }
      ph_.LOISNG().nouveauPoint(cl_, varEdition_, pos[0]);
      if (cl_.tabParamEntier[LidoResource.SINGULARITE.ISEUIL_NOYE.INBDEBIT]
        < 2) {
        ph_.LOISNG().nouveauPoint(cl_, LidoResource.SINGULARITE.DEBIT, pos[0]);
      }
      if (cl_.tabParamEntier[LidoResource.SINGULARITE.ISEUIL_NOYE.INBCOTAVAL]
        < 2) {
        ph_.LOISNG().nouveauPoint(
          cl_,
          LidoResource.SINGULARITE.COTEAVAL,
          pos[0]);
      }
      setObjectModified(true);
      firePropertyChange("singularite", null, cl_);
      setValeurs();
    } else if ("SUPPRIMER".equals(cmd)) {
      if (tbDebit_ == null) {
        return;
      }
      if (tbCotesAval_ == null) {
        return;
      }
      if ((varEdition_ & LidoResource.SINGULARITE.DEBIT) != 0) {
        ph_.LOISNG().supprimePoints(
          cl_,
          tbDebit_.getSelectedColumns(),
          LidoResource.SINGULARITE.DEBIT);
        if (cl_.tabParamEntier[LidoResource.SINGULARITE.ISEUIL_NOYE.INBDEBIT]
          < 2) {
          ph_.LOISNG().supprimePoints(
            cl_,
            new int[] { 0 },
            LidoResource.SINGULARITE.DEBIT);
        }
      } else if ((varEdition_ & LidoResource.SINGULARITE.COTEAVAL) != 0) {
        ph_.LOISNG().supprimePoints(
          cl_,
          tbCotesAval_.getSelectedRows(),
          LidoResource.SINGULARITE.COTEAVAL);
        if (cl_.tabParamEntier[LidoResource.SINGULARITE.ISEUIL_NOYE.INBCOTAVAL]
          < 2) {
          ph_.LOISNG().supprimePoints(
            cl_,
            new int[] { 0 },
            LidoResource.SINGULARITE.COTEAVAL);
        }
      }
      setObjectModified(true);
      firePropertyChange("singularite", null, cl_);
      setValeurs();
    } else if ("TOUTDESELECTIONNER".equals(cmd)) {
      varEdition_=
        LidoResource.SINGULARITE.DEBIT | LidoResource.SINGULARITE.COTEAVAL;
      final ChangeEvent c= new ChangeEvent(this);
      tbDebit_.clearSelection();
      tbDebit_.editingStopped(c);
      tbCotesAmont_.clearSelection();
      tbCotesAmont_.editingStopped(c);
      tbCotesAval_.clearSelection();
      tbCotesAval_.editingStopped(c);
    } else if ("SUPPRIMERLOI".equals(cmd)) {
      ph_.LOISNG().supprimeLoisng(cl_);
      final SParametresSingBlocSNG old= cl_;
      cl_= null;
      objectDeleted();
      firePropertyChange("loisupprimee", old, null);
      fermer();
    }
  }
  private boolean verifieContraintes() {
    return true;
  }
  public void setFond(final double f) {
    graphe_.setFond(f);
  }
  // ListSelectionListener
  public void valueChanged(final ListSelectionEvent e) {
    if (cl_ == null) {
      return;
    }
    final Object src= e.getSource();
    if (src == tbCotesAval_.getSelectionModel()) {
      graphe_.setCoteAval(tbCotesAval_.getSelectedRow());
    }
    if (src == tbDebit_.getColumnModel().getSelectionModel()) {
      graphe_.setDebit(tbDebit_.getSelectedColumn());
    }
  }
  // LidoCustomizer
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresSingBlocSNG)) {
      return;
    }
    if (_n == cl_) {
      return;
    }
    final SParametresSingBlocSNG vp= cl_;
    cl_= (SParametresSingBlocSNG)_n;
    setValeurs();
    setObjectModified(false);
    LIDO_MODIFY_EVENT=
      new FudaaParamEvent(
        this,
        0,
        LidoResource.SNG,
        cl_,
        "singularite " + cl_.numSing);
    firePropertyChange("singularite", vp, cl_);
  }
  public boolean restore() {
    return false;
  }
  protected boolean getValeurs() {
    boolean changed= false;
    /*    int newNbDebit=((Integer)tfNbDebit_.getValue()).intValue();
        int newNbCotAval=((Integer)tfNbCotAval_.getValue()).intValue();

        if( cl_.tabParamEntier[INBDEBIT]!=newNbDebit ) {
          cl_.tabParamEntier[INBDEBIT]=newNbDebit;
          changed=true;
        }
        if( cl_.tabParamEntier[INBCOTAVAL]!=newNbCotAval ) {
          cl_.tabParamEntier[INBCOTAVAL]=newNbCotAval;
          changed=true;
        }*/
    final double newCoteMoy= ((Double)tfCoteMoy_.getValue()).doubleValue();
    if (cl_.tabParamReel[ICOTMOY] != newCoteMoy) {
      cl_.tabParamReel[ICOTMOY]= newCoteMoy;
      changed= true;
    }
    return changed;
    /*    if( cl_==null ) {
          clBck_=null;
        } else {
          clBck_=new SParametresBiefSingLigneRZO();
          clBck_.numBief=cl_.numBief;
          clBck_.xSing=cl_.xSing;
          clBck_.nSing=cl_.nSing;
        }*/
  }
  protected void setValeurs() {
    if (cl_.tabParamReel.length <= 0) {
      System.err.println("LidoSingulariteSeuilNoyeEditor : tabParamReel vide");
      return;
    }
    tfCoteMoy_.setValue(new Double(cl_.tabParamReel[ICOTMOY]));
    tfNbDebit_.setValue(new Integer(cl_.tabParamEntier[INBDEBIT]));
    tfNbCotAval_.setValue(new Integer(cl_.tabParamEntier[INBCOTAVAL]));
  }
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    return ph_.getInformationsDocument();
  }
  protected boolean isObjectModificationImportant(final Object o) {
    return (o == cl_);
  }
  class DebitTable
    extends LidoTableauAbstract
    implements PropertyChangeListener {
    public DebitTable() {
      super();
      setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
      setColumnSelectionAllowed(true);
      setRowSelectionAllowed(false);
      //setTableHeader(null);
      reinitialise();
    }
    public void propertyChange(final PropertyChangeEvent e) {
      if ("singularite".equals(e.getPropertyName())) {
        reinitialise();
      }
    }
    void feuPropertyChange(final String p, final Object ov, final Object nv) {
      super.firePropertyChange(p, ov, nv);
    }
    private void reinitialise() {
      if ((cl_ == null)
        || (cl_.tabDonLois == null)
        || (cl_.tabDonLois.length < (IDEBIT + 1))) {
        return;
      }
      if (isEditing()) {
        System.err.println("canceling edition");
        editingCanceled(new ChangeEvent(this));
      }
      setModel(new TableModel() {
        public Class getColumnClass(final int column) {
          return Double.class;
        }
        public int getColumnCount() {
          return cl_.tabParamEntier[INBDEBIT];
        }
        public String getColumnName(final int column) {
          return "" + column;
        }
        public int getRowCount() {
          return 1;
        }
        public Object getValueAt(final int row, final int column) {
          return new Double(cl_.tabDonLois[IDEBIT][column]);
        }
        public boolean isCellEditable(final int row, final int column) {
          return true;
        }
        public void setValueAt(final Object value, final int row, final int column) {
          cl_.tabDonLois[IDEBIT][column]= ((Double)value).doubleValue();
          DebitTable.this.feuPropertyChange("singularite", null, cl_);
          objectModified();
        }
        public void addTableModelListener(final TableModelListener _l) {}
        public void removeTableModelListener(final TableModelListener _l) {}
      });
      //      TableCellRenderer tcr = LidoParamsHelper.createDoubleCellRenderer();
      //      TableCellEditor tceDouble =LidoParamsHelper.createDoubleCellEditor();
      //      int n = getModel().getColumnCount();
      //      TableColumnModel model = getColumnModel();
      //      for (int i = 0; i < n; i++)
      //      {
      //        model.getColumn(i).setCellRenderer(tcr);
      //        model.getColumn(i).setCellEditor(tceDouble);
      //      }
    }
  }
  class CotesAvalTable extends JTable implements PropertyChangeListener {
    public CotesAvalTable() {
      super();
      setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
      setColumnSelectionAllowed(false);
      setRowSelectionAllowed(true);
      setTableHeader(null);
      reinitialise();
    }
    public void propertyChange(final PropertyChangeEvent e) {
      if ("singularite".equals(e.getPropertyName())) {
        reinitialise();
      }
    }
    void feuPropertyChange(final String p, final Object ov, final Object nv) {
      super.firePropertyChange(p, ov, nv);
    }
    private void reinitialise() {
      if ((cl_ == null)
        || (cl_.tabDonLois == null)
        || (cl_.tabDonLois.length < (ICOTAVAL + 1))) {
        return;
      }
      if (isEditing()) {
        System.err.println("canceling edition");
        editingCanceled(new ChangeEvent(this));
      }
      setModel(new TableModel() {
        public Class getColumnClass(final int column) {
          return Double.class;
        }
        public int getColumnCount() {
          return 1;
        }
        public String getColumnName(final int column) {
          return "" + column;
        }
        public int getRowCount() {
          return cl_.tabParamEntier[INBCOTAVAL];
        }
        public Object getValueAt(final int row, final int column) {
          return new Double(cl_.tabDonLois[ICOTAVAL][row]);
        }
        public boolean isCellEditable(final int row, final int column) {
          return true;
        }
        public void setValueAt(final Object value, final int row, final int column) {
          cl_.tabDonLois[ICOTAVAL][row]= ((Double)value).doubleValue();
          CotesAvalTable.this.feuPropertyChange("singularite", null, cl_);
          objectModified();
        }
        public void addTableModelListener(final TableModelListener _l) {}
        public void removeTableModelListener(final TableModelListener _l) {}
      });
      final TableColumnModel model= getColumnModel();
      model.getColumn(0).setCellRenderer(
        LidoParamsHelper.createDoubleCellRenderer());
      model.getColumn(0).setCellEditor(
        LidoParamsHelper.createDoubleCellEditor());
    }
  }
  class CotesAmontTable
    extends LidoTableauAbstract
    implements PropertyChangeListener {
    public CotesAmontTable() {
      super();
      setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
      setRowSelectionAllowed(false);
      setColumnSelectionAllowed(false);
      setTableHeader(null);
      reinitialise();
    }
    public void propertyChange(final PropertyChangeEvent e) {
      if ("singularite".equals(e.getPropertyName())) {
        reinitialise();
      }
    }
    void feuPropertyChange(final String p, final Object ov, final Object nv) {
      super.firePropertyChange(p, ov, nv);
    }
    private void reinitialise() {
      if ((cl_ == null) || (cl_.tabDonLois == null)) {
        return; //||(cl_.tabDonLois.length<ICOTAMONTOFFSET+1) ) return;
      }
      if (isEditing()) {
        editingCanceled(new ChangeEvent(this));
      }
      setModel(new TableModel() {
        public Class getColumnClass(final int column) {
          return Double.class;
        }
        public int getColumnCount() {
          return cl_.tabParamEntier[INBDEBIT];
        }
        public String getColumnName(final int column) {
          return "" + column;
        }
        // !!: les cotes amonts sont stockees dans tabDonLois, a partir de la 3eme colonne
        public int getRowCount() {
          return cl_.tabParamEntier[INBCOTAVAL];
        }
        public Object getValueAt(final int row, final int column) {
          return new Double(cl_.tabDonLois[column + ICOTAMONTOFFSET][row]);
        }
        public boolean isCellEditable(final int row, final int column) {
          return true;
        }
        public void setValueAt(final Object value, final int row, final int column) {
          cl_.tabDonLois[column + ICOTAMONTOFFSET][row]=
            ((Double)value).doubleValue();
          CotesAmontTable.this.feuPropertyChange("singularite", null, cl_);
          objectModified();
        }
        public void addTableModelListener(final TableModelListener _l) {}
        public void removeTableModelListener(final TableModelListener _l) {}
      });
      //      TableCellRenderer tcr = LidoParamsHelper.createDoubleCellRenderer();
      //      TableCellEditor tceDouble =LidoParamsHelper.createDoubleCellEditor();
      //      TableColumnModel colModel = getColumnModel();
      //      int n = colModel.getColumnCount();
      //      for (int i = 0; i < n; i++)
      //      {
      //        colModel.getColumn(i).setCellRenderer(tcr);
      //        colModel.getColumn(i).setCellEditor(tceDouble);
      //      }
    }
  }
}
