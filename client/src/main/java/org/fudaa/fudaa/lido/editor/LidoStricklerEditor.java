/*
 * @file         LidoStricklerEditor.java
 * @creation     1999-11-17
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoStricklerEditor extends LidoCustomizer {
  SParametresBiefBlocPRO[] profils_;
  BuPanel pnCbs_, pnGen_;
  BuTextField tfStrick_;
  JRadioButton btMin_, btMaj_;
  public LidoStricklerEditor(final BDialogContent parent) {
    super(parent, "Choix du Strickler");
    init();
  }
  private void init() {
    profils_= null;
    tfStrick_= BuTextField.createIntegerField();
    pnCbs_= new BuPanel();
    btMin_= new JRadioButton("Mineur");
    btMin_.setSelected(true);
    btMaj_= new JRadioButton("Majeur");
    final ButtonGroup gp= new ButtonGroup();
    gp.add(btMin_);
    gp.add(btMaj_);
    pnCbs_.add(btMin_);
    pnCbs_.add(btMaj_);
    pnGen_= new BuPanel();
    pnGen_.setBorder(
      new EmptyBorder(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE));
    pnGen_.setLayout(new BuVerticalLayout(5, true, false));
    pnGen_.add(tfStrick_, 0);
    pnGen_.add(pnCbs_, 1);
    getContentPane().add(BorderLayout.CENTER, pnGen_);
    setNavPanel(EbliPreferences.DIALOG.FERMER);
    pack();
  }
  public boolean restore() {
    return false;
  }
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresBiefBlocPRO[])) {
      return;
    }
    if (_n == profils_) {
      return;
    }
    profils_= (SParametresBiefBlocPRO[])_n;
    setObjectModified(false);
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    if ("FERMER".equals(cmd)) {
      if (getValeurs()) {
        firePropertyChange("object", null, profils_);
      }
    }
    super.actionPerformed(e);
  }
  protected boolean getValeurs() {
    boolean changed= false;
    if (profils_ == null) {
      return changed;
    }
    final Integer val= (Integer)tfStrick_.getValue();
    double ov= 0.;
    if (val == null) {
      return changed;
    }
    if (btMin_.isSelected()) {
      for (int i= 0; i < profils_.length; i++) {
        ov= profils_[i].coefStrickMajMin;
        profils_[i].coefStrickMajMin= val.doubleValue();
        if (ov != profils_[i].coefStrickMajMin) {
          LIDO_MODIFY_EVENT=
            new FudaaParamEvent(
              this,
              0,
              LidoResource.PRO,
              profils_[i],
              "profil " + (profils_[i].numProfil));
          setObjectModified(false);
          objectModified();
          changed= true;
        }
      }
    } else if (btMaj_.isSelected()) {
      for (int i= 0; i < profils_.length; i++) {
        ov= profils_[i].coefStrickMajSto;
        profils_[i].coefStrickMajSto= val.doubleValue();
        if (ov != profils_[i].coefStrickMajSto) {
          LIDO_MODIFY_EVENT=
            new FudaaParamEvent(
              this,
              0,
              LidoResource.PRO,
              "profil " + (profils_[i].numProfil));
          setObjectModified(false);
          objectModified();
          changed= true;
        }
      }
    }
    return changed;
  }
  protected void setValeurs() {}
  protected boolean isObjectModificationImportant(final Object o) {
    return (o instanceof SParametresBiefBlocPRO);
  }
  public void paramStructCreated(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    if (struct == null) {
      return;
    }
    if (isObjectModificationImportant(struct)) {
      fermer();
    }
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    if (struct == null) {
      return;
    }
    if (isObjectModificationImportant(struct)) {
      fermer();
    }
  }
  public void paramStructModified(final FudaaParamEvent e) {
    final Object struct= e.getStruct();
    if (struct == null) {
      return;
    }
    if (isObjectModificationImportant(struct)) {
      fermer();
    }
  }
}
