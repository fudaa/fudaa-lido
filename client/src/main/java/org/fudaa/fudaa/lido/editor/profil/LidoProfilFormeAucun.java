/*
 * @file         LidoProfilFormeAucun.java
 * @creation     1999-12-30
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor.profil;
import java.beans.PropertyChangeListener;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoProfilFormeAucun implements LidoProfilFormeSimple {
  GrPoint startPoint_;
  public LidoProfilFormeAucun() {
    startPoint_= new GrPoint(0, 0, 0);
  }
  public void setConnectMode(final int mode) {}
  public BDialog getEditor() {
    return null;
  }
  public GrPoint[] getPoints() {
    return new GrPoint[0];
  }
  public void setStartPoint(final GrPoint p) {
    startPoint_= p;
  }
  public GrPoint getEndPoint() {
    return startPoint_;
  }
  public String getName() {
    return "Aucun";
  }
  public boolean isEditable() {
    return false;
  }
  public void addPropertyChangeListener(final PropertyChangeListener l) {}
  public void removePropertyChangeListener(final PropertyChangeListener l) {}
  public void setParent(final IDialogInterface d) {}
}
