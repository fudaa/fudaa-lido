/*
 * @file         LidoProfilFormeSimple.java
 * @creation     1999-12-28
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor.profil;
import java.beans.PropertyChangeListener;

import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;
import org.fudaa.ebli.geometrie.GrPoint;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public interface LidoProfilFormeSimple {
  int NO_CONNECT= 0;
  int CONNECT_RIGHT= 1;
  int CONNECT_LEFT= 2;
  void setConnectMode(int mode);
  BDialog getEditor();
  GrPoint[] getPoints();
  void setStartPoint(GrPoint p);
  GrPoint getEndPoint();
  String getName();
  boolean isEditable();
  void addPropertyChangeListener(PropertyChangeListener l);
  void removePropertyChangeListener(PropertyChangeListener l);
  void setParent(IDialogInterface d);
}
