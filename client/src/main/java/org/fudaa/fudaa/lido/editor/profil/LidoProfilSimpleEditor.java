/*
 * @file         LidoProfilSimpleEditor.java
 * @creation     1999-12-28
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.editor.profil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.graphe.LidoGrapheProfil;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoProfilSimpleEditor extends LidoCustomizer {
  private final static int LITMAJG= 0;
  private final static int LITMIN= 1;
  private final static int LITMAJD= 2;
  private final static int NBSECTIONS= 3;
  SParametresBiefBlocPRO profil_;
  LidoGrapheProfil graphe_;
  FormeComboBox cm_[];
  BuButton bt_[];
  BuTextField tfFond_;
  JCheckBox cbRives_;
  static String tit_[]=
    { "Lit majeur rive gauche", "Lit mineur", "Lit majeur rive droite" };
  public static LidoProfilFormeSimple[] createForms() {
    return new LidoProfilFormeSimple[] {
      new LidoProfilFormeAucun(),
      new LidoProfilFormeRectangle(),
      new LidoProfilFormeTrapeze()};
  }
  public LidoProfilSimpleEditor() {
    super("Creation de profil simple");
    init();
  }
  public LidoProfilSimpleEditor(final BDialogContent _parent) {
    super(_parent, "Creation de profil simple");
    init();
  }
  private void init() {
    final Container cp= getContentPane();
    LidoProfilFormeSimple[] formes_= null;
    graphe_= new LidoGrapheProfil();
    graphe_.setPreferredSize(new Dimension(320, 240));
    graphe_.setRivesVisible(true);
    addPropertyChangeListener(graphe_);
    int n= 0;
    final BuPanel pnCombos= new BuPanel();
    pnCombos.setLayout(new BuVerticalLayout(5, true, false));
    pnCombos.setBorder(
      new CompoundBorder(
        new EmptyBorder(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE),
        new EtchedBorder()));
    BuPanel pnLit;
    cm_= new FormeComboBox[NBSECTIONS];
    bt_= new BuButton[NBSECTIONS];
    for (int l= 0; l < NBSECTIONS; l++) {
      pnCombos.add(new BuLabel(tit_[l]), n++);
      pnLit= new BuPanel();
      pnLit.setLayout(new BuHorizontalLayout(5, false, false));
      cm_[l]= new FormeComboBox();
      formes_= createForms();
      for (int i= 0; i < formes_.length; i++) {
        formes_[i].setParent(getDialog());
        cm_[l].addItem(formes_[i]);
      }
      cm_[l].addActionListener(this);
      pnLit.add(cm_[l], 0);
      bt_[l]= new BuButton("E");
      bt_[l].setEnabled(
        ((LidoProfilFormeSimple)cm_[l].getSelectedItem()).isEditable());
      bt_[l].addActionListener(this);
      pnLit.add(bt_[l], 1);
      pnCombos.add(pnLit, n++);
    }
    pnCombos.add(new BuLabel("Cote du fond"), n++);
    tfFond_= BuTextField.createDoubleField();
    tfFond_.setValue(new Double(0.));
    tfFond_.addActionListener(this);
    pnCombos.add(tfFond_, n++);
    cbRives_= new JCheckBox("Affichage des rives");
    cbRives_.addActionListener(this);
    cbRives_.setSelected(graphe_.isRivesVisible());
    pnCombos.add(cbRives_, n++);
    cp.add(BorderLayout.WEST, pnCombos);
    cp.add(BorderLayout.CENTER, graphe_);
    setNavPanel(EbliPreferences.DIALOG.VALIDER);
    pack();
  }
  public boolean restore() {
    return false;
  }
  public void setObject(final Object _n) {
    if (!(_n instanceof SParametresBiefBlocPRO)) {
      return;
    }
    if (_n == profil_) {
      return;
    }
    profil_= (SParametresBiefBlocPRO)_n;
    graphe_.setProfil(profil_);
  }
  protected boolean getValeurs() {
    redessine();
    return true;
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    final Object src= e.getSource();
    if ("VALIDER".equals(cmd)) {
      getValeurs();
      fermer();
    } else {
      for (int l= 0; l < NBSECTIONS; l++) {
        final LidoProfilFormeSimple choix=
          (LidoProfilFormeSimple)cm_[l].getSelectedItem();
        if (src == bt_[l]) {
          if (choix.isEditable()) {
            choix.getEditor().show();
          }
        }
        if (src == cm_[l]) {
          bt_[l].setEnabled(choix.isEditable());
        }
      }
      if (src == cbRives_) {
        graphe_.setRivesVisible(cbRives_.isSelected());
      }
      redessine();
    }
  }
  private void redessine() {
    // reconstruction du profil
    double coteFond= 0., minCote= Double.POSITIVE_INFINITY;
    final Double fond= (Double)tfFond_.getValue();
    if (fond != null) {
      coteFond= fond.doubleValue();
    }
    final Vector points= new Vector();
    GrPoint sp= new GrPoint();
    profil_.absMajMin[0]= profil_.absMajMin[1]= 0.;
    profil_.absMajSto[0]= profil_.absMajSto[1]= 0.;
    profil_.coteRivGa= profil_.coteRivGa= 0.;
    LidoProfilFormeSimple forme=
      (LidoProfilFormeSimple)cm_[LITMAJG].getSelectedItem();
    forme.setConnectMode(LidoProfilFormeSimple.CONNECT_RIGHT);
    GrPoint[] pts= forme.getPoints();
    if (pts != null) {
      for (int i= 0; i < pts.length; i++) {
        points.add(pts[i]);
        if (pts[i].y_ < minCote) {
          minCote= pts[i].y_;
        }
      }
      if (points.size() > 0) {
        sp= (GrPoint)points.lastElement();
      }
    }
    forme= (LidoProfilFormeSimple)cm_[LITMIN].getSelectedItem();
    forme.setConnectMode(LidoProfilFormeSimple.NO_CONNECT);
    forme.setStartPoint(sp);
    pts= forme.getPoints();
    if (pts != null) {
      int s= 0;
      if (points.size() > 0) {
        s= 1;
      }
      if (pts.length == 0) {
        profil_.absMajMin[0]= profil_.absMajMin[1]= forme.getEndPoint().x_;
        profil_.coteRivGa= profil_.coteRivDr= forme.getEndPoint().y_;
      } else {
        profil_.absMajMin[0]= sp.x_;
        profil_.coteRivGa= sp.y_;
        profil_.absMajMin[1]= forme.getEndPoint().x_;
        profil_.coteRivDr= forme.getEndPoint().y_;
      }
      for (int i= s; i < pts.length; i++) {
        points.add(pts[i]);
        if (pts[i].y_ < minCote) {
          minCote= pts[i].y_;
        }
      }
      if (points.size() > 0) {
        sp= (GrPoint)points.lastElement();
      }
    }
    forme= (LidoProfilFormeSimple)cm_[LITMAJD].getSelectedItem();
    forme.setConnectMode(LidoProfilFormeSimple.CONNECT_LEFT);
    forme.setStartPoint(sp);
    pts= forme.getPoints();
    if (pts != null) {
      for (int i= 1; i < pts.length; i++) {
        points.add(pts[i]);
        if (pts[i].y_ < minCote) {
          minCote= pts[i].y_;
        }
      }
    }
    final double[] abs= new double[points.size()];
    final double[] cotes= new double[points.size()];
    for (int i= 0; i < points.size(); i++) {
      abs[i]= ((GrPoint)points.get(i)).x_;
      cotes[i]= ((GrPoint)points.get(i)).y_ - minCote + coteFond;
    }
    if (abs.length > 0) {
      profil_.absMajSto[0]= abs[0];
      profil_.absMajSto[1]= abs[abs.length - 1];
    }
    profil_.coteRivGa += coteFond - minCote;
    profil_.coteRivDr += coteFond - minCote;
    profil_.nbPoints= abs.length;
    profil_.abs= abs;
    profil_.cotes= cotes;
    firePropertyChange("profil", null, profil_);
  }
  protected void setValeurs() {}
  protected boolean isObjectModificationImportant(final Object o) {
    return false;
  }
}
class FormeComboBox extends JComboBox {
  public FormeComboBox() {
    super();
    setRenderer(new FormeComboBoxRenderer());
  }
}
class FormeComboBoxRenderer extends DefaultListCellRenderer {
  public Component getListCellRendererComponent(
    final javax.swing.JList list,
    final java.lang.Object value,
    final int index,
    final boolean isSelected,
    final boolean cellHasFocus) {
    final Component comp=
      super.getListCellRendererComponent(
        list,
        value,
        index,
        isSelected,
        cellHasFocus);
    if ((comp instanceof JLabel)) {
      ((JLabel)comp).setText(((LidoProfilFormeSimple)value).getName());
    }
    return comp;
  }
}
