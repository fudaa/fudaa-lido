/*
 * @file         LidoGrapheCourbe.java
 * @creation     2000-07-17
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Dimension;
import java.util.Vector;

import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;
/**
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoGrapheCourbe extends BGraphe {
  private double[][] v_;
  private String GRA_TITLE;
  private String GRA_ABS_LABEL;
  private String GRA_ORD_LABEL;
  private String GRA_ABS_UNITE;
  private String GRA_ORD_UNITE;
  private boolean thumbnail_;
  private Graphe graphe_;
  private Marges marges_;
  private Axe axeX, axeY;
  private CourbeDefault crb_;
  public LidoGrapheCourbe(
    final String gra_title,
    final String gra_abs_label,
    final String gra_ord_label,
    final String gra_abs_unite,
    final String gra_ord_unite) {
    super();
    GRA_TITLE= gra_title;
    GRA_ABS_LABEL= gra_abs_label;
    GRA_ORD_LABEL= gra_ord_label;
    GRA_ABS_UNITE= gra_abs_unite;
    GRA_ORD_UNITE= gra_ord_unite;
    v_= new double[0][2];
    thumbnail_= false;
    setPreferredSize(new Dimension(640, 480));
    init();
  }
  protected void init() {
    updateGraphe();
  }
  public void setValeurs(final double[][] v) {
    if ((v == v_) || (v == null) || (v.length == 0) || (v[0].length < 2)) {
      return;
    }
    final double[][] vp= v_;
    v_= v;
    if (getGraphe() == null) {
      initGraphe();
    }
    updateGraphe();
    firePropertyChange("valeurs", vp, v_);
  }
  public void setThumbnail(final boolean t, final int width, final int height) {
    thumbnail_= t;
    setPreferredSize(new Dimension(width, height));
    updateGraphe();
  }
  private void initGraphe() {
    graphe_= new Graphe();
    marges_= new Marges();
    if (thumbnail_) {
      marges_.gauche_= 5;
      marges_.droite_= 5;
      marges_.haut_= 5;
      marges_.bas_= 5;
    } else {
      marges_.gauche_= 60;
      marges_.droite_= 80;
      marges_.haut_= 45;
      marges_.bas_= 30;
    }
    graphe_.animation_= false;
    if (!thumbnail_) {
      graphe_.titre_= GRA_TITLE;
      graphe_.legende_= true;
    } else {
      graphe_.legende_= false;
      graphe_.copyright_= false;
    }
    graphe_.marges_= marges_;
    axeX= new Axe();
    if (!thumbnail_) {
      axeX.titre_= GRA_ABS_LABEL;
      axeX.unite_= GRA_ABS_UNITE;
      axeX.graduations_= true;
      axeX.pas_= 5;
    } else {
      axeX.graduations_= false;
    }
    axeX.vertical_= false;
    graphe_.ajoute(axeX);
    axeY= new Axe();
    if (!thumbnail_) {
      axeY.titre_= GRA_ORD_LABEL;
      axeY.unite_= GRA_ORD_UNITE;
      axeY.graduations_= true;
      axeY.pas_= 5;
    } else {
      axeY.graduations_= false;
    }
    axeY.vertical_= true;
    graphe_.ajoute(axeY);
    crb_= new CourbeDefault();
    if (!thumbnail_) {
      crb_.titre_= GRA_TITLE;
      crb_.marqueurs_= true;
    } else {
      crb_.marqueurs_= false;
    }
    graphe_.ajoute(crb_);
    setGraphe(graphe_);
  }
  private void updateGraphe() {
    if (getGraphe() == null) {
      return;
    }
    final int nbPoint= v_.length;
    final Vector crbVals= new Vector();
    Valeur v;
    int pasX= 5, pasZ= 5;
    double maxAbsVal= 0.;
    double minAbsVal= 0.;
    double maxCotVal= 0.;
    double minCotVal= 0.;
    if (nbPoint > 0) {
      minAbsVal= maxAbsVal= v_[0][0];
      minCotVal= maxCotVal= v_[0][1];
    }
    for (int i= 0; i < nbPoint; i++) {
      if (minAbsVal > v_[i][0]) {
        minAbsVal= v_[i][0];
      }
      if (maxAbsVal < v_[i][0]) {
        maxAbsVal= v_[i][0];
      }
      if (minCotVal > v_[i][1]) {
        minCotVal= v_[i][1];
      }
      if (maxCotVal < v_[i][1]) {
        maxCotVal= v_[i][1];
      }
      v= new Valeur();
      v.s_= v_[i][0];
      v.v_= v_[i][1];
      crbVals.add(v);
    }
    final double minZ= Math.floor(minCotVal);
    final double maxZ= Math.ceil(maxCotVal);
    final double minX= Math.floor(minAbsVal);
    final double maxX= Math.ceil(maxAbsVal);
    pasX= (int) ((maxX - minX) / 10);
    pasZ= (int) ((maxZ - minZ) / 10);
    axeX.minimum_= minX;
    axeX.maximum_= maxX;
    axeX.pas_= pasX;
    axeY.minimum_= minZ;
    axeY.maximum_= maxZ;
    axeY.pas_= pasZ;
    crb_.valeurs_= crbVals;
    fullRepaint();
  }
}
