/*
 * @file         LidoGrapheLignedeau.java
 * @creation     1999-10-05
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Contrainte;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;

import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.15 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoGrapheLignedeau extends BGraphe {
  private static final int GAUCHE= LidoResource.VISU_BIEF.GAUCHE;
  private static final int DROITE= LidoResource.VISU_BIEF.DROITE;
  private Hashtable biefs_;
  private boolean yAjuste_;
  private double[] lidoIX_, lidoIZ_;
  private SParametresEXT ext_;
  private SResultatsRSN rsn_;
  private int temps_;
  String crbTitr[];
  Graphe graphe_;
  CourbeDefault crbLidoI;
  Vector crbLidoImport;
  Contrainte[] ctrLaisses;
  Marges marges;
  Axe axeX, axeY;
  CourbeDefault crbFond[],
    crbRiveDr[],
    crbRiveGa[],
    crbLidoR[],
    crbLidoEnvMin[],
    crbLidoEnvMax[];
  double maxAbsVal, minAbsVal, maxCotVal, minCotVal, maxZ_;
  private Double[] axeX_;
  private Double[] axeZ_;
  boolean isModeResultat_;
  boolean isRapide_;
  public LidoGrapheLignedeau(final boolean res) {
    isModeResultat_= res;
    biefs_= new Hashtable();
    yAjuste_= true;
    lidoIX_= null;
    lidoIZ_= null;
    rsn_= null;
    temps_= 0;
    maxZ_= Double.NEGATIVE_INFINITY;
    axeX_= new Double[2];
    axeZ_= new Double[2];
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    crbLidoImport= new Vector();
    isRapide_= false;
    setPreferredSize(new Dimension(640, 480));
  }
  // en mode bief
  public void addBief(
    final SParametresBiefSituLigneRZO b,
    final SParametresBiefBlocPRO[] p) {
    if (isModeResultat_) {
      throw new IllegalArgumentException("en mode resultat, il faut appeler: addBief(int, SParametresBiefBlocPRO[])");
    }
    if ((b == null) || (p == null) || (p.length == 0) || biefs_.containsKey(b)) {
      return;
    }
    biefs_.put(b, p);
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
  }
  // en mode resultat
  public void addBief(final int b, final SParametresBiefBlocPRO[] p) {
    if (!isModeResultat_) {
      throw new IllegalArgumentException("en mode bief, il faut appeler: addBief(SParametresBiefSituLigneRZO, SParametresBiefBlocPRO[])");
    }
    if ((p == null) || (p.length == 0) || biefs_.containsKey("" + b)) {
      return;
    }
    biefs_.put("" + b, p);
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    if (rsn_ != null) {
      for (int t= 0; t < rsn_.pasTemps.length; t++) {
        final SResultatsBiefRSN bie= rsn_.pasTemps[t].ligBief[b];
        for (int i= 0; i < bie.ligne.length; i++) {
          if (bie.ligne[i].z > maxZ_) {
            maxZ_= bie.ligne[i].z;
          }
        }
      }
    }
  }
  public void setYAjuste(final boolean v) {
    if (yAjuste_ == v) {
      return;
    }
    yAjuste_= v;
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    updateGraphe();
  }
  public boolean isYAjuste() {
    return yAjuste_;
  }
  public void setRapide(final boolean v) {
    if (isRapide_ == v) {
      return;
    }
    isRapide_= v;
    calculeCourbes(false);
    updateGraphe();
  }
  public boolean isRapide() {
    return isRapide_;
  }
  public void setRive(final int r) {
    for (int i= 0; i < crbRiveDr.length; i++) {
      crbRiveDr[i].visible_= ((r & DROITE) == DROITE);
    }
    for (int i= 0; i < crbRiveGa.length; i++) {
      crbRiveGa[i].visible_= ((r & GAUCHE) == GAUCHE);
    }
    fullRepaint();
  }
  public int getRive() {
    int r= 0;
    if ((crbRiveDr.length > 0) && crbRiveDr[0].visible_) {
      r= r | DROITE;
    }
    if (CtuluLibMessage.DEBUG) {
      if ((crbRiveGa.length > 0) && crbRiveGa[0].visible_) {
        r= r | GAUCHE;
      }
    }
    return r;
  }
  public void setLigneDeauInitiale(final double[] x, final double[] z) {
    if ((x == lidoIX_) && (z == lidoIZ_)) {
      return;
    }
    lidoIX_= x;
    lidoIZ_= z;
    updateGraphe();
  }
  public void addLigneDeauImport(final double[] x, final double[] z, final String tit) {
    final CourbeDefault c= new CourbeDefault();
    c.titre_= tit;
    c.trace_= "lineaire";
    c.marqueurs_= false;
    c.visible_= false;
    final Aspect aspect= new Aspect();
    aspect.contour_= new Color(0xAA, 0x00, 0xFF);
    c.aspect_= aspect;
    graphe_.ajoute(c);
    crbLidoImport.add(c);
    if ((x != null) && (z != null)) {
      for (int j= 0; j < x.length; j++) {
        final Valeur v= new Valeur();
        v.s_= x[j];
        v.v_= z[j];
        c.valeurs_.add(v);
      }
    }
    updateGraphe();
  }
  public void setLigneDeauImportVisible(final boolean v) {
    try {
      if (isModeResultat_) {
        for (int i= 0; i < crbLidoImport.size(); i++) {
          ((CourbeDefault)crbLidoImport.get(i)).visible_= v;
        }
      }
      fullRepaint();
    } catch (final NullPointerException e) {
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
      System.err.println("data not committed?");
    }
  }
  public boolean isLigneDeauImportVisible() {
    boolean res= false;
    try {
      if (isModeResultat_) {
        res=
          (crbLidoImport.size() == 0)
            ? false
            : ((CourbeDefault)crbLidoImport.get(0)).visible_;
      } else {
        res= false;
      }
    } catch (final NullPointerException e) {
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
      System.err.println("data not committed?");
    }
    return res;
  }
  public void setResultats(final SResultatsRSN rsn) {
    if (rsn_ == rsn) {
      return;
    }
    rsn_= rsn;
  }
  public void setLaisses(final SParametresEXT ext) {
    if (ext_ == ext) {
      return;
    }
    ext_= ext;
    updateGraphe();
  }
  public void setFondVisible(final boolean v) {
    try {
      for (int i= 0; i < crbFond.length; i++) {
        crbFond[i].visible_= v;
      }
      fullRepaint();
    } catch (final NullPointerException e) {
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
      System.err.println("data not committed?");
    }
  }
  public boolean isFondVisible() {
    boolean res= false;
    try {
      res= (crbFond.length == 0) ? false : crbFond[0].visible_;
    } catch (final NullPointerException e) {
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
      System.err.println("data not committed?");
    }
    return res;
  }
  public void setLigneDeauIVisible(final boolean v) {
    try {
      crbLidoI.visible_= v;
      fullRepaint();
    } catch (final NullPointerException e) {
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
      System.err.println("data not committed?");
    }
  }
  public boolean isLigneDeauIVisible() {
    boolean res= false;
    try {
      res= crbLidoI.visible_;
    } catch (final NullPointerException e) {
      System.err.println("data not committed?");
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
    }
    return res;
  }
  public void setLigneDeauRVisible(final boolean v) {
    try {
      if (isModeResultat_) {
        for (int i= 0; i < crbLidoR.length; i++) {
          crbLidoR[i].visible_= v;
        }
      }
      fullRepaint();
    } catch (final NullPointerException e) {
      System.err.println("data not committed?");
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
    }
  }
  public boolean isLigneDeauRVisible() {
    boolean res= false;
    try {
      if (isModeResultat_) {
        res= (crbLidoR.length == 0) ? false : crbLidoR[0].visible_;
      } else {
        res= false;
      }
    } catch (final NullPointerException e) {
      System.err.println("data not committed?");
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
    }
    return res;
  }
  public void setLigneDeauEnvVisible(final boolean v) {
    try {
      if (isModeResultat_) {
        for (int i= 0; i < crbLidoEnvMin.length; i++) {
          crbLidoEnvMin[i].visible_= crbLidoEnvMax[i].visible_= v;
        }
      }
      if (v) {
        calculeEnveloppeLigneDeau();
      }
      fullRepaint();
    } catch (final NullPointerException e) {
      System.err.println("data not committed?");
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
    }
  }
  public boolean isLigneDeauEnvVisible() {
    boolean res= false;
    try {
      if (isModeResultat_) {
        res= (crbLidoEnvMin.length == 0) ? false : crbLidoEnvMin[0].visible_;
      } else {
        res= false;
      }
    } catch (final NullPointerException e) {
      System.err.println("data not committed?");
      if (CtuluLibMessage.DEBUG) {
        e.printStackTrace();
      }
    }
    return res;
  }
  public void setAxes(final Double[] x, final Double[] z) {
    if ((x == null) || (x.length < 2) || (z == null) || (z.length < 2)) {
      System.err.println("LidoGrapheLignedeau: axe invalide");
      return;
    }
    axeX_= x;
    axeZ_= z;
    commitData();
  }
  public Double[][] getAxes() {
    return new Double[][] { axeX_, axeZ_ };
  }
  public void setTemps(final int t) {
    if (t == temps_) {
      return;
    }
    if (rsn_ != null) {
      if ((t < 0) || (t >= rsn_.pasTemps.length)) {
        return;
      }
    }
    temps_= t;
    calculeCourbes(true);
    updateGraphe();
  }
  public void commitData() {
    if ((axeX_[0] == null)
      || (axeX_[1] == null)
      || (axeZ_[0] == null)
      || (axeZ_[1] == null)) {
      calculeBornes();
    }
    if (getGraphe() == null) {
      initGraphe();
    }
    calculeCourbes(false);
    updateGraphe();
  }
  private void calculeEnveloppeLigneDeau() {
    if ((biefs_.size() == 0) || (rsn_ == null) || (rsn_.pasTemps.length < 2)) {
      return;
    }
    final Enumeration k= biefs_.keys();
    int c= 0;
    while (k.hasMoreElements()) {
      crbLidoEnvMin[c].valeurs_= new Vector();
      crbLidoEnvMax[c].valeurs_= new Vector();
      final int key= Integer.parseInt((String)k.nextElement());
      final SResultatsBiefRSN b= rsn_.pasTemps[0].ligBief[key];
      for (int i= 0; i < b.ligne.length; i++) {
        double minH= Double.POSITIVE_INFINITY;
        double maxH= Double.NEGATIVE_INFINITY;
        for (int t= 0; t < rsn_.pasTemps.length; t++) {
          final SResultatsBiefRSN bp= rsn_.pasTemps[t].ligBief[key];
          if (bp.ligne[i].z < minH) {
            minH= bp.ligne[i].z;
          }
          if (bp.ligne[i].z > maxH) {
            maxH= bp.ligne[i].z;
          }
        }
        Valeur v= new Valeur();
        v.s_= b.ligne[i].x;
        v.v_= minH;
        crbLidoEnvMin[c].valeurs_.add(v);
        v= new Valeur();
        v.s_= b.ligne[i].x;
        v.v_= maxH;
        crbLidoEnvMax[c].valeurs_.add(v);
      }
      c++;
    }
  }
  private void calculeBornes() {
    if ((biefs_.size() == 0)) {
      return;
    }
    if (isModeResultat_
      && ((rsn_ == null) || (temps_ < 0) || (temps_ >= rsn_.pasTemps.length))) {
      return;
    }
    final Enumeration k= biefs_.keys();
    maxAbsVal= Double.NEGATIVE_INFINITY;
    minAbsVal= Double.POSITIVE_INFINITY;
    maxCotVal= maxZ_;
    minCotVal= Double.POSITIVE_INFINITY;
    int c= 0;
    while (k.hasMoreElements()) {
      final Object key= k.nextElement();
      SResultatsBiefRSN b= null;
      if (isModeResultat_) {
        b= rsn_.pasTemps[temps_].ligBief[Integer.parseInt((String)key)];
      }
      // -- RIVES -- //
      final SParametresBiefBlocPRO[] p= (SParametresBiefBlocPRO[])biefs_.get(key);
      for (int i= 0; i < p.length; i++) {
        if (!isModeResultat_) {
          // on cherche le fond du lit
          double profLitI= 0.;
          if (p[i].nbPoints > 0) {
            profLitI= p[i].cotes[0];
          }
          for (int z= 0; z < p[i].nbPoints; z++) {
            if (p[i].cotes[z] < profLitI) {
              profLitI= p[i].cotes[z];
            }
          }
          if (profLitI > maxCotVal) {
            maxCotVal= profLitI;
          }
          if (profLitI < minCotVal) {
            minCotVal= profLitI;
          }
        }
        if (p[i].abscisse > maxAbsVal) {
          maxAbsVal= p[i].abscisse;
        }
        if (p[i].abscisse < minAbsVal) {
          minAbsVal= p[i].abscisse;
        }
        if (p[i].coteRivDr > maxCotVal) {
          maxCotVal= p[i].coteRivDr;
        }
        if (p[i].coteRivGa > maxCotVal) {
          maxCotVal= p[i].coteRivGa;
        }
      }
      if (isModeResultat_) {
        // -- FOND et LIDOR -- // (temps=0)
        for (int i= 0; i < b.ligne.length; i++) {
          final double fondZ= b.ligne[i].z - b.ligne[i].y;
          if (fondZ < minCotVal) {
            minCotVal= fondZ;
          }
          if (b.ligne[i].z < minCotVal) {
            minCotVal= b.ligne[i].z;
          }
          if (b.ligne[i].z > maxCotVal) {
            maxCotVal= b.ligne[i].z;
          }
        }
      }
      c++;
    }
    axeX_[0]= new Double(minAbsVal);
    axeX_[1]= new Double(maxAbsVal);
    axeZ_[0]= new Double(minCotVal);
    axeZ_[1]= new Double(maxCotVal);
  }
  private void calculeCourbes(boolean resOnly) {
    if ((biefs_.size() == 0)) {
      return;
    }
    if (isModeResultat_
      && ((rsn_ == null) || (temps_ < 0) || (temps_ >= rsn_.pasTemps.length))) {
      return;
    }
    final Enumeration k= biefs_.keys();
    if (!resOnly) {
      maxAbsVal= axeX_[1].doubleValue();
      minAbsVal= axeX_[0].doubleValue();
      maxCotVal= axeZ_[1].doubleValue();
      minCotVal= axeZ_[0].doubleValue();
    }
    int c= 0;
    while (k.hasMoreElements()) {
      final Object key= k.nextElement();
      SResultatsBiefRSN b= null;
      if (isModeResultat_) {
        b= rsn_.pasTemps[temps_].ligBief[Integer.parseInt((String)key)];
      }
      // -- RIVES -- //
      if (!resOnly) {
        final SParametresBiefBlocPRO[] p= (SParametresBiefBlocPRO[])biefs_.get(key);
        final double[] xData= new double[p.length];
        final double[] gData= new double[p.length];
        final double[] dData= new double[p.length];
        double[] fData= null;
        if (!isModeResultat_) {
          fData= new double[p.length];
        }
        for (int i= 0; i < p.length; i++) {
          xData[i]= p[i].abscisse;
          gData[i]= p[i].coteRivGa;
          dData[i]= p[i].coteRivDr;
          if (!isModeResultat_) {
            // on cherche le fond du lit
            double profLitI= 0.;
            if (p[i].nbPoints > 0) {
              profLitI= p[i].cotes[0];
            }
            for (int z= 0; z < p[i].nbPoints; z++) {
              if (p[i].cotes[z] < profLitI) {
                profLitI= p[i].cotes[z];
              }
            }
            fData[i]= profLitI;
          }
        }
        List[] crbs= null;
        if (isModeResultat_) {
          crbs= formatBinData(new double[][] { { minAbsVal, maxAbsVal }, {
              minCotVal, maxCotVal }
          }, xData, new double[][] { gData, dData }, false);
        } else {
          crbs= formatBinData(new double[][] { { minAbsVal, maxAbsVal }, {
              minCotVal, maxCotVal }
          }, xData, new double[][] { gData, dData, fData }, false);
        }
        if (crbs == null) {
          break;
        }
        crbRiveGa[c].valeurs_= crbs[0];
        crbRiveDr[c].valeurs_= crbs[1];
        if (!isModeResultat_) {
          crbFond[c].valeurs_= crbs[2];
        }
      }
      if (isModeResultat_) {
        // -- FOND et LIDOR --
        final double[] xData= new double[b.ligne.length];
        double[] fData= new double[b.ligne.length];
        if (!resOnly) {
          fData= new double[b.ligne.length];
        }
        final double[] zData= new double[b.ligne.length];
        for (int i= 0; i < b.ligne.length; i++) {
          xData[i]= b.ligne[i].x;
          if (!resOnly) {
            fData[i]= b.ligne[i].z - b.ligne[i].y;
          }
          zData[i]= b.ligne[i].z;
        }
        double[][] yData= null;
        if (!resOnly) {
          yData= new double[][] { zData, fData };
        } else {
          yData= new double[][] { zData };
        }
        final List[] crbs=
          formatBinData(new double[][] { { minAbsVal, maxAbsVal }, {
            minCotVal, maxCotVal }
        }, xData, yData, isRapide_);
        if (crbs == null) {
          continue;
        }
        crbLidoR[c].valeurs_= crbs[0];
        if (!resOnly) {
          crbFond[c].valeurs_= crbs[1];
        }
      }
      c++;
    }
  }
  private void initGraphe() {
    if ((biefs_.size() == 0)) {
      return;
    }
    if (isModeResultat_
      && ((rsn_ == null) || (temps_ < 0) || (temps_ >= rsn_.pasTemps.length))) {
      return;
    }
    if ((axeX_[0] == null)
      || (axeX_[1] == null)
      || (axeZ_[0] == null)
      || (axeZ_[1] == null)) {
      System.err.println("LidoGrapheLignedeau: data not commited");
      return;
    }
    graphe_= new Graphe();
    marges= new Marges();
    marges.gauche_= 60;
    marges.droite_= 80;
    marges.haut_= 45;
    marges.bas_= 30;
    graphe_.titre_= "Ligne d'eau";
    graphe_.animation_= false;
    graphe_.legende_= true;
    graphe_.marges_= marges;
    axeX= new Axe();
    axeX.titre_= "abscisse";
    axeX.unite_= "m";
    axeX.vertical_= false;
    axeX.graduations_= true;
    axeX.grille_= true;
    graphe_.ajoute(axeX);
    axeY= new Axe();
    axeY.titre_= "cote";
    axeY.unite_= "m";
    axeY.vertical_= true;
    axeY.graduations_= true;
    axeY.grille_= true;
    graphe_.ajoute(axeY);
    Aspect aspect= null;
    crbFond= new CourbeDefault[biefs_.size()];
    crbRiveGa= new CourbeDefault[biefs_.size()];
    crbRiveDr= new CourbeDefault[biefs_.size()];
    crbTitr= new String[biefs_.size()];
    crbLidoR= new CourbeDefault[biefs_.size()];
    if (isModeResultat_) {
      crbLidoEnvMin= new CourbeDefault[biefs_.size()];
      crbLidoEnvMax= new CourbeDefault[biefs_.size()];
    }
    for (int c= 0; c < crbFond.length; c++) {
      crbTitr[c]= "" + (c + 1);
      crbFond[c]= new CourbeDefault();
      crbFond[c].titre_= "fond " + crbTitr[c];
      crbFond[c].trace_= "lineaire";
      crbFond[c].marqueurs_= false;
      crbFond[c].visible_= true;
      aspect= new Aspect();
      aspect.contour_=
        new Color(
          LidoResource.COULEURS[(3 * c) % LidoResource.COULEURS.length]);
      crbFond[c].aspect_= aspect;
      graphe_.ajoute(crbFond[c]);
      crbRiveDr[c]= new CourbeDefault();
      crbRiveDr[c].titre_= "rive D " + crbTitr[c];
      crbRiveDr[c].trace_= "lineaire";
      crbRiveDr[c].marqueurs_= false;
      crbRiveDr[c].visible_= true;
      aspect= new Aspect();
      aspect.contour_=
        new Color(
          LidoResource.COULEURS[(3 * c + 1) % LidoResource.COULEURS.length]);
      crbRiveDr[c].aspect_= aspect;
      graphe_.ajoute(crbRiveDr[c]);
      crbRiveGa[c]= new CourbeDefault();
      crbRiveGa[c].titre_= "rive G " + crbTitr[c];
      crbRiveGa[c].trace_= "lineaire";
      crbRiveGa[c].marqueurs_= false;
      crbRiveGa[c].visible_= true;
      aspect= new Aspect();
      aspect.contour_=
        new Color(
          LidoResource.COULEURS[(3 * c + 2) % LidoResource.COULEURS.length]);
      crbRiveGa[c].aspect_= aspect;
      graphe_.ajoute(crbRiveGa[c]);
      if (isModeResultat_) {
        crbLidoR[c]= new CourbeDefault();
        crbLidoR[c].trace_= "lineaire";
        crbLidoR[c].marqueurs_= false;
        crbLidoR[c].visible_= true;
        aspect= new Aspect();
        aspect.contour_= new Color(0x00, 0x00, 0xFF);
        crbLidoR[c].aspect_= aspect;
        graphe_.ajoute(crbLidoR[c]);
        crbLidoEnvMin[c]= new CourbeDefault();
        crbLidoEnvMin[c].trace_= "lineaire";
        crbLidoEnvMin[c].marqueurs_= false;
        crbLidoEnvMin[c].visible_= false;
        aspect= new Aspect();
        aspect.contour_= new Color(0xAA, 0x00, 0xFF);
        crbLidoEnvMin[c].aspect_= aspect;
        graphe_.ajoute(crbLidoEnvMin[c]);
        crbLidoEnvMax[c]= new CourbeDefault();
        crbLidoEnvMax[c].trace_= "lineaire";
        crbLidoEnvMax[c].marqueurs_= false;
        crbLidoEnvMax[c].visible_= false;
        aspect= new Aspect();
        aspect.contour_= new Color(0xAA, 0x00, 0xFF);
        crbLidoEnvMax[c].aspect_= aspect;
        graphe_.ajoute(crbLidoEnvMax[c]);
      }
    }
    crbLidoI= new CourbeDefault();
    crbLidoI.titre_= "ligne initiale";
    crbLidoI.trace_= "lineaire";
    crbLidoI.marqueurs_= false;
    crbLidoI.visible_= false;
    aspect= new Aspect();
    aspect.contour_= new Color(0x00, 0x00, 0xFF);
    crbLidoI.aspect_= aspect;
    graphe_.ajoute(crbLidoI);
    if (isModeResultat_) {
      if ((ext_ != null) && (ext_.laisses != null)) {
        ctrLaisses= new Contrainte[ext_.laisses.length];
        for (int i= 0; i < ext_.laisses.length; i++) {
          ctrLaisses[i]= new Contrainte();
          ctrLaisses[i].titre_= ext_.laisses[i].titre;
          ctrLaisses[i].vertical_= true;
          ctrLaisses[i].type_= "max";
          ctrLaisses[i].v_= ext_.laisses[i].cote;
          ctrLaisses[i].s_= ext_.laisses[i].abscisse;
          graphe_.ajoute(ctrLaisses[i]);
        }
      }
    }
    setGraphe(graphe_);
  }
  private void updateGraphe() {
    if ((biefs_.size() == 0)) {
      return;
    }
    if (isModeResultat_
      && ((rsn_ == null) || (temps_ < 0) || (temps_ >= rsn_.pasTemps.length))) {
      return;
    }
    if ((axeX_[0] == null)
      || (axeX_[1] == null)
      || (axeZ_[0] == null)
      || (axeZ_[1] == null)) {
      System.err.println("LidoGrapheLignedeau: data not commited");
      return;
    }
    Valeur v= null;
    if (crbLidoI.visible_ && (lidoIX_ != null) && (lidoIZ_ != null)) {
      for (int i= 0; i < lidoIX_.length; i++) {
        v= new Valeur();
        v.s_= lidoIX_[i];
        v.v_= lidoIZ_[i];
        crbLidoI.valeurs_.add(v);
      }
    }
    if (!yAjuste_) {
      final double tailleAxeX= getWidth() - marges.gauche_ - marges.droite_;
      final double tailleAxeY= getHeight() - marges.haut_ - marges.bas_;
      maxCotVal= minCotVal + tailleAxeY * (maxAbsVal - minAbsVal) / tailleAxeX;
    }
    axeX.minimum_= axeX_[0] != null ? axeX_[0].doubleValue() : minAbsVal;
    axeX.maximum_= axeX_[1] != null ? axeX_[1].doubleValue() : maxAbsVal;
    axeY.minimum_= axeZ_[0] != null ? axeZ_[0].doubleValue() : minCotVal;
    axeY.maximum_= axeZ_[1] != null ? axeZ_[1].doubleValue() : maxCotVal;
    if (ctrLaisses != null) {
      for (int i= 0; i < ctrLaisses.length; i++) {
        graphe_.enleve(ctrLaisses[i]);
      }
    }
    if ((ext_ != null) && (ext_.laisses != null)) {
      ctrLaisses= new Contrainte[ext_.laisses.length];
      for (int i= 0; i < ext_.laisses.length; i++) {
        ctrLaisses[i]= new Contrainte();
        ctrLaisses[i].titre_= ext_.laisses[i].titre;
        ctrLaisses[i].vertical_= true;
        ctrLaisses[i].type_= "max";
        ctrLaisses[i].v_= ext_.laisses[i].cote;
        ctrLaisses[i].s_= ext_.laisses[i].abscisse;
        graphe_.ajoute(ctrLaisses[i]);
      }
    }
    fullRepaint();
  }
}
