/*
 * @file         LidoGrapheLimiteCourbe.java
 * @creation     1999-08-24
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoGrapheLimiteCourbe
  extends LidoGrapheCourbe
  implements PropertyChangeListener {
  public final static int TARAGE= LidoResource.LIMITE.TARAGE;
  public final static int LIMNI= LidoResource.LIMITE.LIMNI;
  public final static int HYDRO= LidoResource.LIMITE.HYDRO;
  private SParametresCondLimBlocCLM cl_;
  private int type_;
  public LidoGrapheLimiteCourbe(
    final int type,
    final String gra_title,
    final String gra_abs_label,
    final String gra_ord_label,
    final String gra_abs_unite,
    final String gra_ord_unite) {
    super(
      gra_title,
      gra_abs_label,
      gra_ord_label,
      gra_abs_unite,
      gra_ord_unite);
    type_= type;
    cl_= null;
  }
  public void setLimite(final SParametresCondLimBlocCLM s) {
    if (s == cl_) {
      return;
    }
    final SParametresCondLimBlocCLM vp= cl_;
    cl_= s;
    final double[][] v= new double[cl_.nbPoints][2];
    for (int i= 0; i < cl_.nbPoints; i++) {
      if (type_ == LidoResource.LIMITE.TARAGE) {
        v[i][0]= cl_.point[i].zLim;
        v[i][1]= cl_.point[i].qLim;
      } else if (type_ == LidoResource.LIMITE.LIMNI) {
        v[i][0]= cl_.point[i].tLim;
        v[i][1]= cl_.point[i].zLim;
      } else if (type_ == LidoResource.LIMITE.HYDRO) {
        v[i][0]= cl_.point[i].tLim;
        v[i][1]= cl_.point[i].qLim;
      }
    }
    setValeurs(v);
    firePropertyChange("limite", vp, cl_);
  }
  // PropertyChangeListener
  public void propertyChange(final PropertyChangeEvent e) {
    if ("limite".equals(e.getPropertyName())) {
      final SParametresCondLimBlocCLM nv= (SParametresCondLimBlocCLM)e.getNewValue();
      cl_= null;
      setLimite(nv);
    }
  }
}
