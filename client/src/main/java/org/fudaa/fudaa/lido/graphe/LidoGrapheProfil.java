/*
 * @file         LidoGrapheProfil.java
 * @creation     1999-12-10
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Contrainte;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;

import org.fudaa.fudaa.lido.LidoResource;
/**
 * Un composant pour afficher les profils.
 *
 * @version      $Id: LidoGrapheProfil.java,v 1.17 2006-09-19 15:05:06 deniger Exp $
 * @author       Axel von Arnim
 */
public class LidoGrapheProfil
  extends BGraphe
  implements PropertyChangeListener {
  private SParametresBiefBlocPRO profil_;
  private int indEdited_;
  private boolean zoomLitMineur_;
  //  private boolean rivesVisibles_;
  private boolean yAjuste_;
  private boolean fixAxes_;
  //  private double niveauEau_, niveauEauEnvMin_, niveauEauEnvMax_;
  //  private boolean eauVisible_, eauEnvVisible_;
  private Double[] axeX_;
  private Double[] axeZ_;
  private boolean pasPlani_;
  ////  private String crbTitre_;
  private Graphe graphe_;
  private Marges marges_;
  private Axe axeX, axeY;
  private CourbeDefault crbProfil_, crbPtEdited_;
  private Contrainte ctrStoG_,
    ctrStoD_,
    ctrRivG_,
    ctrRivD_,
    ctrEau_,
    ctrEauEnvMin_,
    ctrEauEnvMax_,
    ctrPasPlani_[];
  private NumberFormat nf2_;
  private double profilMinOrdonnee_,
    profilMaxOrdonnee_,
    profilMinAbscisse_,
    profilMaxAbscisse_,
    profilMaxOrdonneeLitMineur_,
    profilMinOrdonneeLitMineur_;
  public LidoGrapheProfil() {
    nf2_= NumberFormat.getInstance(Locale.US);
    nf2_.setMaximumFractionDigits(LidoResource.FRACTIONDIGITS);
    nf2_.setGroupingUsed(false);
    axeX_= new Double[2];
    axeZ_= new Double[2];
    initGraphe();
    profil_= null;
    indEdited_= -1;
    zoomLitMineur_= false;
    //    rivesVisibles_ = true;
    //    eauVisible_ = false;
    //    eauEnvVisible_ = false;
    yAjuste_= true;
    fixAxes_= false;
    //    niveauEau_ = 0.;
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    pasPlani_= false;
    setPreferredSize(new Dimension(640, 480));
  }
  public void setProfil(final SParametresBiefBlocPRO p) {
    if (p == profil_) {
      return;
    }
    final SParametresBiefBlocPRO vp= profil_;
    profil_= p;
    if (!fixAxes_) {
      axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    }
    //    if (getGraphe() == null)
    //      initGraphe();
    updateParametres();
    updateGraphe();
    firePropertyChange("profil", vp, profil_);
  }
  public SParametresBiefBlocPRO getProfil() {
    return profil_;
  }
  public void setZoomLitMineur(final boolean z) {
    if (zoomLitMineur_ == z) {
      return;
    }
    zoomLitMineur_= z;
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    updateGraphe();
  }
  public boolean isZoomLitMineur() {
    return zoomLitMineur_;
  }
  public void setYAjuste(final boolean v) {
    if (CtuluLibMessage.DEBUG) {
      System.out.println("setYajust");
    }
    if (yAjuste_ == v) {
      return;
    }
    yAjuste_= v;
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    updateGraphe();
  }
  public boolean isYAjuste() {
    return yAjuste_;
  }
  public void setFixAxes(final boolean v) {
    if (fixAxes_ == v) {
      return;
    }
    fixAxes_= v;
  }
  public boolean isFixAxes() {
    return fixAxes_;
  }
  public void setRivesVisible(final boolean v) {
    if (ctrStoG_.visible_ == v) {
      return;
    }
    ctrStoG_.visible_= v;
    ctrStoD_.visible_= v;
    ctrRivG_.visible_= v;
    ctrRivD_.visible_= v;
    fullRepaint();
  }
  public boolean isRivesVisible() {
    return ctrStoG_.visible_;
  }
  public void setEauVisible(final boolean v) {
    if (CtuluLibMessage.DEBUG) {
      System.out.println("setVisible " + v);
    }
    if (ctrEau_.visible_ == v) {
      return;
    }
    ctrEau_.visible_= v;
    fullRepaint();
  }
  public boolean isEauVisible() {
    return ctrEau_.visible_;
  }
  public void setEauEnvVisible(final boolean v) {
    if (ctrEauEnvMin_.visible_ == v) {
      return;
    }
    ctrEauEnvMin_.visible_= v;
    ctrEauEnvMax_.visible_= v;
    fullRepaint();
  }
  public boolean isEauEnvVisible() {
    return ctrEauEnvMin_.visible_;
  }
  public void setNiveauEauEnv(final double min, final double max) {
    if ((ctrEauEnvMin_.v_ == min) && (ctrEauEnvMax_.v_ == max)) {
      return;
    }
    ctrEauEnvMin_.v_= min;
    ctrEauEnvMax_.v_= max;
    ctrEauEnvMax_.titre_= "" + nf2_.format(max);
    fullRepaint();
  }
  public void setNiveauEau(final double n) {
    if (ctrEau_.v_ == n) {
      return;
    }
    if (CtuluLibMessage.DEBUG) {
      System.out.println("niveau eau " + n);
    }
    ctrEau_.v_= n;
    updatePasPlanimetrage();
  }
  public void setPasPlanimetrageVisible(final boolean v) {
    if (pasPlani_ == v) {
      return;
    }
    pasPlani_= v;
    for (int i= 0; i < ctrPasPlani_.length; i++) {
      ctrPasPlani_[i].visible_= pasPlani_;
    }
    fullRepaint();
  }
  public boolean isPasPlanimetrageVisible() {
    return pasPlani_;
  }
  public void setAxes(final Double[] x, final Double[] z) {
    if (CtuluLibMessage.DEBUG) {
      System.out.println("update graphe set Axes");
    }
    if ((x == null) || (x.length < 2) || (z == null) || (z.length < 2)) {
      System.err.println("LidoGrapheProfil: axe invalide");
      return;
    }
    axeX_= x;
    axeZ_= z;
    updateGraphe();
  }
  public Double[][] getAxes() {
    return new Double[][] { axeX_, axeZ_ };
  }
  public void setPointEdited(final int ind) {
    if (ind == indEdited_) {
      return;
    }
    indEdited_= ind;
    if (((indEdited_ >= 0) && (indEdited_ < profil_.nbPoints))) {
      final double rivGaAbs= profil_.absMajMin[0];
      final double rivDrAbs= profil_.absMajMin[1];
      if ((!zoomLitMineur_)
        || ((profil_.abs[indEdited_] >= rivGaAbs)
          && (profil_.abs[indEdited_] <= rivDrAbs))) {
        crbPtEdited_.valeurs_= new Vector(1);
        final Valeur v= new Valeur();
        v.s_= profil_.abs[indEdited_];
        v.v_= profil_.cotes[indEdited_];
        crbPtEdited_.valeurs_.add(v);
        crbPtEdited_.visible_= true;
        fullRepaint();
        return;
      }
      crbPtEdited_.valeurs_= null;
      crbPtEdited_.visible_= false;
      fullRepaint();
    }
  }
  public int getPointEdited() {
    return indEdited_;
  }
  // PropertyChangeListener
  // gere les changements de contenu de profil_
  // (contrairement a setProfil qui gere les changements de pointeur)
  public void propertyChange(final PropertyChangeEvent e) {
    if ("profil".equals(e.getPropertyName())) {
      if (e.getOldValue() != null) {
        return;
      }
      axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
      updateParametres();
      updateGraphe();
    }
  }
  private void initGraphe() {
    //    if (profil_ == null)
    //      return;
    graphe_= new Graphe();
    marges_= new Marges();
    marges_.gauche_= 50;
    marges_.droite_= 60;
    marges_.haut_= 45;
    marges_.bas_= 30;
    graphe_.animation_= false;
    graphe_.legende_= false;
    graphe_.marges_= marges_;
    axeX= new Axe();
    axeX.titre_= "abscisse";
    axeX.unite_= "m";
    axeX.vertical_= false;
    axeX.graduations_= true;
    axeX.grille_= false;
    axeX.pas_= 5;
    graphe_.ajoute(axeX);
    axeY= new Axe();
    axeY.titre_= "cote";
    axeY.unite_= "m";
    axeY.vertical_= true;
    axeY.graduations_= true;
    axeY.grille_= false;
    axeY.pas_= 5;
    graphe_.ajoute(axeY);
    crbProfil_= new CourbeDefault();
    //    crbProfil_.titre = crbTitre_;
    crbProfil_.trace_= "lineaire";
    crbProfil_.marqueurs_= true;
    graphe_.ajoute(crbProfil_);
    ctrStoG_= new Contrainte();
    ctrStoG_.titre_= "SG";
    ctrStoG_.vertical_= false;
    ctrStoG_.type_= "max";
    ctrStoG_.couleur_= new Color(0x00, 0xDD, 0x00);
    graphe_.ajoute(ctrStoG_);
    ctrStoD_= new Contrainte();
    ctrStoD_.titre_= "SD";
    ctrStoD_.vertical_= false;
    ctrStoD_.type_= "max";
    ctrStoD_.couleur_= new Color(0x00, 0xDD, 0x00);
    graphe_.ajoute(ctrStoD_);
    ctrRivG_= new Contrainte();
    ctrRivG_.titre_= "RG";
    ctrRivG_.vertical_= false;
    ctrRivG_.type_= "max";
    graphe_.ajoute(ctrRivG_);
    ctrRivD_= new Contrainte();
    ctrRivD_.titre_= "RD";
    ctrRivD_.vertical_= false;
    ctrRivD_.type_= "max";
    graphe_.ajoute(ctrRivD_);
    ctrEau_= new Contrainte();
    ctrEau_.titre_= "Eau";
    ctrEau_.vertical_= true;
    ctrEau_.type_= "max";
    graphe_.ajoute(ctrEau_);
    ctrEauEnvMin_= new Contrainte();
    ctrEauEnvMin_.titre_= "";
    ctrEauEnvMin_.vertical_= true;
    ctrEauEnvMin_.type_= "max";
    ctrEauEnvMin_.couleur_= new Color(0xAA, 0x00, 0xFF);
    graphe_.ajoute(ctrEauEnvMin_);
    ctrEauEnvMax_= new Contrainte();
    ctrEauEnvMax_.titre_= "";
    ctrEauEnvMax_.vertical_= true;
    ctrEauEnvMax_.type_= "max";
    ctrEauEnvMax_.couleur_= new Color(0xAA, 0x00, 0xFF);
    graphe_.ajoute(ctrEauEnvMax_);
    ctrPasPlani_= new Contrainte[LidoResource.PLANIMETRAGE.NBVALEURS_PLANI];
    for (int i= 0; i < ctrPasPlani_.length; i++) {
      ctrPasPlani_[i]= new Contrainte();
      ctrPasPlani_[i].titre_= "pas " + i;
      ctrPasPlani_[i].vertical_= true;
      ctrPasPlani_[i].type_= "max";
      graphe_.ajoute(ctrPasPlani_[i]);
    }
    crbPtEdited_= new CourbeDefault();
    crbPtEdited_.type_= "nuage";
    crbPtEdited_.marqueurs_= true;
    final Aspect aspect= new Aspect();
    aspect.contour_= new Color(0x00, 0xFF, 0xFF);
    aspect.texte_= new Color(0x00, 0xFF, 0xFF);
    crbPtEdited_.aspect_= aspect;
    graphe_.ajoute(crbPtEdited_);
    ctrStoG_.visible_= true;
    ctrStoD_.visible_= true;
    ctrRivG_.visible_= true;
    ctrRivD_.visible_= true;
    ctrEau_.visible_= false;
    ctrEauEnvMin_.visible_= false;
    ctrEauEnvMax_.visible_= false;
    setGraphe(graphe_);
  }
  private void updateGraphe() {
    if ((profil_ == null)) {
      return;
    }
    int pasX= 5, pasZ= 5;
    double maxAbsVal= Double.NEGATIVE_INFINITY;
    double minAbsVal= Double.POSITIVE_INFINITY;
    double maxCotVal= Double.NEGATIVE_INFINITY;
    double minCotVal= Double.POSITIVE_INFINITY;
    final double rivGaAbs= profil_.absMajMin[0];
    final double rivDrAbs= profil_.absMajMin[1];
    //    double stoGaAbs = profil_.absMajSto[0];
    //    double stoDrAbs = profil_.absMajSto[1];
    //    if (profil_.nbPoints == 0)
    //    {
    //      minAbsVal = 0.;
    //      minCotVal = 0.;
    //      maxAbsVal = 0.;
    //      maxCotVal = 0.;
    //    }
    //    else
    //    {
    //      if (zoomLitMineur_)
    //      {
    //        minAbsVal = rivGaAbs;
    //        maxAbsVal = rivDrAbs;
    //      }
    //      else
    //        minAbsVal = maxAbsVal = profil_.abs[0];
    //    }
    boolean axesContraints= false;
    if (axeX_[0] != null) {
      minAbsVal= axeX_[0].doubleValue();
      axesContraints= true;
    }
    if (axeX_[1] != null) {
      maxAbsVal= axeX_[1].doubleValue();
      axesContraints= true;
    }
    if (axeZ_[0] != null) {
      minCotVal= axeZ_[0].doubleValue();
      axesContraints= true;
    }
    if (axeZ_[1] != null) {
      maxCotVal= axeZ_[1].doubleValue();
      axesContraints= true;
    }
    //    //    Valeur v = new Valeur();
    //    //    ArrayList vals = new ArrayList(profil_.nbPoints);
    //    for (int i = 0; i < profil_.nbPoints; i++)
    //    {
    //      //      v = new Valeur();
    //      //      v.s = profil_.abs[i];
    //      //      v.v = profil_.cotes[i];
    //      //      vals.add(v);
    //      if (axesContraints)
    //        continue;
    //      if (!zoomLitMineur_)
    //      {
    //        if (profil_.abs[i] < minAbsVal)
    //          minAbsVal = profil_.abs[i];
    //        if (profil_.abs[i] > maxAbsVal)
    //          maxAbsVal = profil_.abs[i];
    //      }
    //      // zoomLitMineur : on elimine les points hors-lit
    //      if ((!zoomLitMineur_)
    //        || ((profil_.abs[i] >= rivGaAbs) && (profil_.abs[i] <= rivDrAbs)))
    //      {
    //        if (profil_.cotes[i] < minCotVal)
    //          minCotVal = profil_.cotes[i];
    //        if (profil_.cotes[i] > maxCotVal)
    //          maxCotVal = profil_.cotes[i];
    //      }
    //    }
    if (!axesContraints) {
      if (profil_.nbPoints == 0) {
        minAbsVal= 0.;
        minCotVal= 0.;
        maxAbsVal= 0.;
        maxCotVal= 0.;
      } else if (zoomLitMineur_) {
        minAbsVal= rivGaAbs;
        maxAbsVal= rivDrAbs;
        minCotVal= profilMinOrdonneeLitMineur_;
        maxCotVal= profilMaxOrdonneeLitMineur_;
      } else {
        minAbsVal= profilMinAbscisse_;
        maxAbsVal= profilMaxAbscisse_;
        minCotVal= profilMinOrdonnee_;
        maxCotVal= profilMaxOrdonnee_;
      }
      if (!yAjuste_) {
        final double tailleAxeX= getWidth() - marges_.gauche_ - marges_.droite_;
        final double tailleAxeY= getHeight() - marges_.haut_ - marges_.bas_;
        maxCotVal=
          minCotVal + tailleAxeY * (maxAbsVal - minAbsVal) / tailleAxeX;
      }
      if ((ctrEau_.v_) > maxCotVal) {
        maxCotVal= ctrEau_.v_;
      }
      axeX_[0]= new Double(minAbsVal);
      axeX_[1]= new Double(maxAbsVal);
      axeZ_[0]= new Double(minCotVal);
      axeZ_[1]= new Double(maxCotVal);
    }
    double minZ= Math.floor(minCotVal);
    double maxZ= Math.ceil(maxCotVal);
    double minX= Math.floor(minAbsVal);
    double maxX= Math.ceil(maxAbsVal);
    pasX= (int) ((maxX - minX) / 10);
    pasZ= (int) ((maxZ - minZ) / 10);
    minZ -= pasZ;
    maxZ += pasZ;
    minX -= pasX;
    maxX += pasX;
    //    crbPtEdited_.valeurs = new Vector(2);
    //    graphe_.titre = "Profil " + (profil_.indice + 1) + " en travers";
    //    graphe_.soustitre = profil_.numProfil;
    axeX.minimum_= minX;
    axeX.maximum_= maxX;
    axeX.pas_= pasX;
    axeY.minimum_= minZ;
    axeY.maximum_= maxZ;
    axeY.pas_= pasZ;
    //    crbProfil_.valeurs = new Vector(vals);
    //    ctrStoG_.v = stoGaAbs;
    //    ctrStoG_.visible = rivesVisibles_;
    //    //    ctrStoD_.v = stoDrAbs;
    //    ctrStoD_.visible = rivesVisibles_;
    //    //   ctrRivG_.v = rivGaAbs;
    //    ctrRivG_.visible = rivesVisibles_;
    //    //   ctrRivD_.v = rivDrAbs;
    //    ctrRivD_.visible = rivesVisibles_;
    //    for (int i = 0; i < ctrPasPlani_.length; i++)
    //    {
    //      //      ctrPasPlani_[i].v =
    //      //        (niveauEau_ - minCotVal)
    //      //          * i
    //      //          / LidoResource.PLANIMETRAGE.NBVALEURS_PLANI
    //      //          + minCotVal;
    //      ctrPasPlani_[i].visible = pasPlani_;
    //    }
    //    //    ctrEau_.v = niveauEau_;
    //    ctrEau_.visible = eauVisible_;
    //    ctrEauEnvMin_.v = niveauEauEnvMin_;
    //    //ctrEauEnvMin_.titre=""+niveauEauEnvMin_;
    //    ctrEauEnvMin_.visible = eauEnvVisible_;
    //    ctrEauEnvMax_.v = niveauEauEnvMax_;
    //    ctrEauEnvMax_.titre = "" + nf2_.format(niveauEauEnvMax_);
    //    ctrEauEnvMax_.visible = eauEnvVisible_;
    fullRepaint();
  }
  private void updateParametres() {
    if ((profil_ == null)) {
      return;
    }
    profilMinOrdonnee_= Double.POSITIVE_INFINITY;
    profilMinAbscisse_= Double.POSITIVE_INFINITY;
    profilMinOrdonneeLitMineur_= Double.POSITIVE_INFINITY;
    profilMaxOrdonnee_= Double.NEGATIVE_INFINITY;
    profilMaxAbscisse_= Double.NEGATIVE_INFINITY;
    profilMaxOrdonneeLitMineur_= Double.NEGATIVE_INFINITY;
    final double rivGaAbs= profil_.absMajMin[0];
    final double rivDrAbs= profil_.absMajMin[1];
    Valeur v= new Valeur();
    final ArrayList vals= new ArrayList(profil_.nbPoints);
    for (int i= 0; i < profil_.nbPoints; i++) {
      v= new Valeur();
      v.s_= profil_.abs[i];
      v.v_= profil_.cotes[i];
      vals.add(v);
      //on recherche les max,min des profils
      if (v.s_ < profilMinAbscisse_) {
        profilMinAbscisse_= v.s_;
      }
      if (v.s_ > profilMaxAbscisse_) {
        profilMaxAbscisse_= v.s_;
      }
      if (v.v_ < profilMinOrdonnee_) {
        profilMinOrdonnee_= v.v_;
      }
      if (v.v_ > profilMaxOrdonnee_) {
        profilMaxOrdonnee_= v.v_;
      }
      //recherche du max,min en cote dans le lit mineure
      if ((v.s_ >= rivGaAbs) && (v.s_ <= rivDrAbs)) {
        if (v.v_ < profilMinOrdonneeLitMineur_) {
          profilMinOrdonneeLitMineur_= v.v_;
        }
        if (v.v_ > profilMaxOrdonneeLitMineur_) {
          profilMaxOrdonneeLitMineur_= v.v_;
        }
      }
    }
    crbPtEdited_.valeurs_= new Vector(1);
    if (((indEdited_ >= 0) && (indEdited_ < profil_.nbPoints))) {
      v= new Valeur();
      v.s_= profil_.abs[indEdited_];
      v.v_= profil_.cotes[indEdited_];
      crbPtEdited_.valeurs_.add(v);
    }
    graphe_.titre_= "Profil " + (profil_.indice + 1) + " en travers";
    graphe_.soustitre_= profil_.numProfil;
    crbProfil_.valeurs_= new Vector(vals);
    ctrStoG_.v_= profil_.absMajSto[0];
    ctrStoD_.v_= profil_.absMajSto[1];
    ctrRivG_.v_= rivGaAbs;
    ctrRivD_.v_= rivDrAbs;
    updatePasPlanimetrage();
  }
  private void updatePasPlanimetrage() {
    final double d=
      (ctrEau_.v_ - profilMinOrdonnee_)
        / LidoResource.PLANIMETRAGE.NBVALEURS_PLANI;
    for (int i= 0; i < ctrPasPlani_.length; i++) {
      ctrPasPlani_[i].v_= (d * i) + profilMinOrdonnee_;
      ctrPasPlani_[i].visible_= pasPlani_;
    }
    fullRepaint();
  }
}
