/*
 * @file         LidoGrapheResultatEnLong.java
 * @creation     1999-10-06
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Contrainte;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;

import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoGrapheResultatEnLong extends BGraphe {
  public final static String HAUTEUREAU= LidoResource.RESULTAT.HAUTEUREAU;
  public final static String SECTIONMOUILLEE=
    LidoResource.RESULTAT.SECTIONMOUILLEE;
  public final static String RAYON= LidoResource.RESULTAT.RAYON;
  public final static String B1= LidoResource.RESULTAT.B1;
  public final static String VITESSE= LidoResource.RESULTAT.VITESSE;
  public final static String DEBITTOTAL= LidoResource.RESULTAT.DEBITTOTAL;
  public final static String FROUDE= LidoResource.RESULTAT.FROUDE;
  public final static String DEBIT= LidoResource.RESULTAT.DEBIT;
  public final static String CHARGE= LidoResource.RESULTAT.CHARGE;
  public final static String REGIMECRIT= LidoResource.RESULTAT.REGIMECRIT;
  public final static String REGIMEUNIF= LidoResource.RESULTAT.REGIMEUNIF;
  public final static String FORCETRAC= LidoResource.RESULTAT.FORCETRAC;
  //public static int MIN=0;
  //public static int MAX=1;
  private String type_;
  private Vector biefs_;
  //private int minMax_;
  boolean minOn_, maxOn_;
  private SResultatsRSN rsn_;
  private int temps_;
  private String axeYLabel_, axeYUnite_, crbTitre_;
  private double maxAbsVal, minAbsVal, maxCotVal, minCotVal, minZ_, maxZ_;
  private Double[] axeX_;
  private Double[] axeZ_;
  private double[] contraintes_;
  private Graphe graphe_;
  private Marges marges_;
  private Axe axeX, axeY;
  private CourbeDefault crbDataMin_[], crbDataMax_[];
  private Contrainte ctr_[];
  private boolean isRapide_;
  public LidoGrapheResultatEnLong(final String type) {
    type_= type;
    biefs_= new Vector();
    //minMax_=MIN;
    minOn_= true;
    maxOn_= false;
    rsn_= null;
    temps_= 0;
    isRapide_= false;
    maxZ_= Double.NEGATIVE_INFINITY;
    minZ_= Double.POSITIVE_INFINITY;
    axeX_= new Double[2];
    axeZ_= new Double[2];
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    contraintes_= new double[0];
    if (HAUTEUREAU.equals(type_)) {
      axeYLabel_= "hauteur";
      axeYUnite_= "m";
      crbTitre_= "Hauteur d'eau";
    } else if (SECTIONMOUILLEE.equals(type_)) {
      axeYLabel_= "sections";
      axeYUnite_= "";
      crbTitre_= "Section mouill�e par lit";
    } else if (RAYON.equals(type_)) {
      axeYLabel_= "rayon";
      axeYUnite_= "m";
      crbTitre_= "Rayon hydraulique par lit";
    } else if (B1.equals(type_)) {
      axeYLabel_= "largeur";
      axeYUnite_= "m";
      crbTitre_= "Largeur au miroir";
    } else if (VITESSE.equals(type_)) {
      axeYLabel_= "vitesse";
      axeYUnite_= "m/s";
      crbTitre_= "Vitesse par lit";
      contraintes_= new double[] { 0.3, 0.6, 1, 1.5, 4 };
    } else if (DEBITTOTAL.equals(type_)) {
      axeYLabel_= "d�bit";
      axeYUnite_= "m3/s";
      crbTitre_= "D�bit total";
    } else if (FROUDE.equals(type_)) {
      axeYLabel_= "froude";
      axeYUnite_= "";
      crbTitre_= "Nombre de Froude";
    } else if (DEBIT.equals(type_)) {
      axeYLabel_= "d�bit";
      axeYUnite_= "m3/s";
      crbTitre_= "D�bit par lit";
    } else if (CHARGE.equals(type_)) {
      axeYLabel_= "charge";
      axeYUnite_= "";
      crbTitre_= "Charge";
    } else if (REGIMECRIT.equals(type_)) {
      axeYLabel_= "hc";
      axeYUnite_= "m";
      crbTitre_= "Hauteur critique";
    } else if (REGIMEUNIF.equals(type_)) {
      axeYLabel_= "hn";
      axeYUnite_= "m";
      crbTitre_= "Hauteur normale";
    } else if (FORCETRAC.equals(type_)) {
      axeYLabel_= "ft";
      axeYUnite_= "";
      crbTitre_= "Force tractrice";
    } else {
      axeYLabel_= "";
      axeYUnite_= "";
      crbTitre_= "Variable inconnue";
    }
    setPreferredSize(new Dimension(640, 480));
  }
  public void addBief(final int _b) {
    final Integer bI= new Integer(_b);
    if (biefs_.contains(bI)) {
      return;
    }
    biefs_.add(bI);
    axeX_[0]= axeX_[1]= axeZ_[0]= axeZ_[1]= null;
    if (rsn_ != null) {
      for (int t= 0; t < rsn_.pasTemps.length; t++) {
        final SResultatsBiefRSN b= rsn_.pasTemps[t].ligBief[_b];
        for (int i= 0; i < b.ligne.length; i++) {
          double varMin= Double.POSITIVE_INFINITY;
          double varMax= Double.NEGATIVE_INFINITY;
          if (HAUTEUREAU.equals(type_)) {
            varMin= varMax= b.ligne[i].y;
          } else if (SECTIONMOUILLEE.equals(type_)) {
            varMin= Math.min(b.ligne[i].s1, b.ligne[i].s2);
            varMax= Math.max(b.ligne[i].s1, b.ligne[i].s2);
          } else if (RAYON.equals(type_)) {
            varMin= Math.min(b.ligne[i].r1, b.ligne[i].r2);
            varMax= Math.max(b.ligne[i].r1, b.ligne[i].r2);
          } else if (B1.equals(type_)) {
            varMin= varMax= b.ligne[i].b1;
          } else if (VITESSE.equals(type_)) {
            varMin= Math.min(b.ligne[i].vmin, b.ligne[i].vmaj);
            varMax= Math.max(b.ligne[i].vmin, b.ligne[i].vmaj);
          } else if (DEBITTOTAL.equals(type_)) {
            varMin= varMax= b.ligne[i].q;
          } else if (FROUDE.equals(type_)) {
            varMin= varMax= b.ligne[i].froude;
          } else if (DEBIT.equals(type_)) {
            varMin= Math.min(b.ligne[i].qmin, b.ligne[i].qmaj);
            varMax= Math.max(b.ligne[i].qmin, b.ligne[i].qmaj);
          } else if (CHARGE.equals(type_)) {
            varMin= varMax= b.ligne[i].charge;
          } else if (REGIMECRIT.equals(type_)) {
            varMin=
              varMax=
                b.ligne[i].y
                  + (b.ligne[i].vmin
                    * b.ligne[i].vmin
                    / (2 * LidoResource.CONSTANTE.G));
          } else if (REGIMEUNIF.equals(type_)) {
            varMin= varMax= 0.;
          } else if (FORCETRAC.equals(type_)) {
            varMin= varMax= 0.;
          }
          if (varMax > maxZ_) {
            maxZ_= varMax;
          }
          if (varMin < minZ_) {
            minZ_= varMin;
          }
        }
      }
    }
  }
  public void setRapide(final boolean v) {
    if (isRapide_ == v) {
      return;
    }
    isRapide_= v;
    calculeCourbes();
    updateGraphe();
  }
  public boolean isRapide() {
    return isRapide_;
  }
  public void setMinEnabled(final boolean m) {
    if (minOn_ == m) {
      return;
    }
    minOn_= m;
    updateGraphe();
  }
  public boolean isMinEnabled() {
    return minOn_;
  }
  public void setMaxEnabled(final boolean m) {
    if (maxOn_ == m) {
      return;
    }
    maxOn_= m;
    updateGraphe();
  }
  public boolean isMaxEnabled() {
    return maxOn_;
  }
  public void setResultats(final SResultatsRSN rsn) {
    if (rsn_ == rsn) {
      return;
    }
    rsn_= rsn;
  }
  public void setAxes(final Double[] x, final Double[] z) {
    if ((x == null) || (x.length < 2) || (z == null) || (z.length < 2)) {
      System.err.println("LidoGrapheResultatEnLong: axe invalide");
      return;
    }
    axeX_= x;
    axeZ_= z;
    commitData();
  }
  public Double[][] getAxes() {
    return new Double[][] { axeX_, axeZ_ };
  }
  public void setContraintes(final double[] c) {
    if (contraintes_ == c) {
      return;
    }
    contraintes_= c;
    updateGraphe();
  }
  public double[] getContraintes() {
    return contraintes_;
  }
  public void setTemps(final int t) {
    if (t == temps_) {
      return;
    }
    if (rsn_ != null) {
      if ((t < 0) || (t >= rsn_.pasTemps.length)) {
        return;
      }
    }
    temps_= t;
    calculeCourbes();
    updateGraphe();
  }
  public void commitData() {
    if ((axeX_[0] == null)
      || (axeX_[1] == null)
      || (axeZ_[0] == null)
      || (axeZ_[1] == null)) {
      calculeBornes();
    }
    if (getGraphe() == null) {
      initGraphe();
    }
    calculeCourbes();
    updateGraphe();
  }
  private void calculeBornes() {
    if ((biefs_.size() == 0)
      || (rsn_ == null)
      || (temps_ < 0)
      || (temps_ >= rsn_.pasTemps.length)
      || ((!minOn_) && (!maxOn_))) {
      return;
    }
    final Enumeration k= biefs_.elements();
    maxAbsVal= Double.NEGATIVE_INFINITY;
    minAbsVal= Double.POSITIVE_INFINITY;
    maxCotVal= maxZ_;
    minCotVal= minZ_;
    int c= 0;
    while (k.hasMoreElements()) {
      final Integer key= (Integer)k.nextElement();
      final SResultatsBiefRSN b= rsn_.pasTemps[temps_].ligBief[key.intValue()];
      if (HAUTEUREAU.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].y < minCotVal) {
            minCotVal= b.ligne[i].y;
          }
          if (b.ligne[i].y > maxCotVal) {
            maxCotVal= b.ligne[i].y;
          }
        }
      } else if (SECTIONMOUILLEE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].s1 < minCotVal) {
            minCotVal= b.ligne[i].s1;
          }
          if (b.ligne[i].s1 > maxCotVal) {
            maxCotVal= b.ligne[i].s1;
          }
          if (b.ligne[i].s2 < minCotVal) {
            minCotVal= b.ligne[i].s2;
          }
          if (b.ligne[i].s2 > maxCotVal) {
            maxCotVal= b.ligne[i].s2;
          }
        }
      } else if (RAYON.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].r1 < minCotVal) {
            minCotVal= b.ligne[i].r1;
          }
          if (b.ligne[i].r1 > maxCotVal) {
            maxCotVal= b.ligne[i].r1;
          }
          if (b.ligne[i].r2 < minCotVal) {
            minCotVal= b.ligne[i].r2;
          }
          if (b.ligne[i].r2 > maxCotVal) {
            maxCotVal= b.ligne[i].r2;
          }
        }
      } else if (B1.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].b1 < minCotVal) {
            minCotVal= b.ligne[i].b1;
          }
          if (b.ligne[i].b1 > maxCotVal) {
            maxCotVal= b.ligne[i].b1;
          }
        }
      } else if (VITESSE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].vmin < minCotVal) {
            minCotVal= b.ligne[i].vmin;
          }
          if (b.ligne[i].vmin > maxCotVal) {
            maxCotVal= b.ligne[i].vmin;
          }
          if (b.ligne[i].vmaj < minCotVal) {
            minCotVal= b.ligne[i].vmaj;
          }
          if (b.ligne[i].vmaj > maxCotVal) {
            maxCotVal= b.ligne[i].vmaj;
          }
        }
      } else if (DEBITTOTAL.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].q < minCotVal) {
            minCotVal= b.ligne[i].q;
          }
          if (b.ligne[i].q > maxCotVal) {
            maxCotVal= b.ligne[i].q;
          }
        }
      } else if (FROUDE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].froude < minCotVal) {
            minCotVal= b.ligne[i].froude;
          }
          if (b.ligne[i].froude > maxCotVal) {
            maxCotVal= b.ligne[i].froude;
          }
        }
      } else if (DEBIT.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].qmin < minCotVal) {
            minCotVal= b.ligne[i].qmin;
          }
          if (b.ligne[i].qmin > maxCotVal) {
            maxCotVal= b.ligne[i].qmin;
          }
          if (b.ligne[i].qmaj < minCotVal) {
            minCotVal= b.ligne[i].qmaj;
          }
          if (b.ligne[i].qmaj > maxCotVal) {
            maxCotVal= b.ligne[i].qmaj;
          }
        }
      } else if (CHARGE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          if (b.ligne[i].charge < minCotVal) {
            minCotVal= b.ligne[i].charge;
          }
          if (b.ligne[i].charge > maxCotVal) {
            maxCotVal= b.ligne[i].charge;
          }
        }
      } else if (REGIMECRIT.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          final double hc=
            b.ligne[i].y
              + (b.ligne[i].vmin
                * b.ligne[i].vmin
                / (2 * LidoResource.CONSTANTE.G));
          if (hc < minCotVal) {
            minCotVal= hc;
          }
          if (hc > maxCotVal) {
            maxCotVal= hc;
          }
        }
      } else if (REGIMEUNIF.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          minCotVal= 0.;
          maxCotVal= 0.;
        }
      } else if (FORCETRAC.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          minCotVal= 0.;
          maxCotVal= 0.;
        }
      }
      for (int i= 0; i < b.ligne.length; i++) {
        if (b.ligne[i].x < minAbsVal) {
          minAbsVal= b.ligne[i].x;
        }
        if (b.ligne[i].x > maxAbsVal) {
          maxAbsVal= b.ligne[i].x;
        }
      }
      c++;
    }
    axeX_[0]= new Double(minAbsVal);
    axeX_[1]= new Double(maxAbsVal);
    axeZ_[0]= new Double(minCotVal);
    axeZ_[1]= new Double(maxCotVal);
  }
  private void calculeCourbes() {
    if ((biefs_.size() == 0)
      || (rsn_ == null)
      || (temps_ < 0)
      || (temps_ >= rsn_.pasTemps.length)
      || ((!minOn_) && (!maxOn_))) {
      return;
    }
    final Enumeration k= biefs_.elements();
    maxAbsVal= axeX_[1].doubleValue();
    minAbsVal= axeX_[0].doubleValue();
    maxCotVal= axeZ_[1].doubleValue();
    minCotVal= axeZ_[0].doubleValue();
    int c= 0;
    while (k.hasMoreElements()) {
      final Integer key= (Integer)k.nextElement();
      final SResultatsBiefRSN b= rsn_.pasTemps[temps_].ligBief[key.intValue()];
      crbDataMax_[c].titre_= "bief " + (b.numero + 1);
      crbDataMin_[c].titre_= "bief " + (b.numero + 1);
      final double[] xData= new double[b.ligne.length];
      double[] z1Data= new double[b.ligne.length];
      double[] z2Data= new double[b.ligne.length];
      for (int i= 0; i < b.ligne.length; i++) {
        xData[i]= b.ligne[i].x;
      }
      if (HAUTEUREAU.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].y;
        }
        z2Data= null;
      } else if (SECTIONMOUILLEE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].s1;
        }
        for (int i= 0; i < b.ligne.length; i++) {
          z2Data[i]= b.ligne[i].s2;
        }
      } else if (RAYON.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].r1;
        }
        for (int i= 0; i < b.ligne.length; i++) {
          z2Data[i]= b.ligne[i].r2;
        }
      } else if (B1.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].b1;
        }
        z2Data= null;
      } else if (VITESSE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].vmin;
        }
        for (int i= 0; i < b.ligne.length; i++) {
          z2Data[i]= b.ligne[i].vmaj;
        }
      } else if (DEBITTOTAL.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].q;
        }
        z2Data= null;
      } else if (FROUDE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].froude;
        }
        z2Data= null;
      } else if (DEBIT.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].qmin;
        }
        for (int i= 0; i < b.ligne.length; i++) {
          z2Data[i]= b.ligne[i].qmaj;
        }
      } else if (CHARGE.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= b.ligne[i].charge;
        }
        z2Data= null;
      } else if (REGIMECRIT.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          final double hc=
            b.ligne[i].y
              + (b.ligne[i].vmin
                * b.ligne[i].vmin
                / (2 * LidoResource.CONSTANTE.G));
          z1Data[i]= hc;
        }
        z2Data= null;
      } else if (REGIMEUNIF.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= 0.;
        }
        z2Data= null;
      } else if (FORCETRAC.equals(type_)) {
        for (int i= 0; i < b.ligne.length; i++) {
          z1Data[i]= 0.;
        }
        z2Data= null;
      } else {
        z1Data= null;
        z2Data= null;
      }
      double[][] zData= null;
      if (z1Data == null) {
        continue;
      }
      if (z2Data == null) {
        zData= new double[][] { z1Data };
      } else {
        zData= new double[][] { z1Data, z2Data };
      }
      final List[] crbs= formatBinData(new double[][] { { minAbsVal, maxAbsVal }, {
          minCotVal, maxCotVal }
      }, xData, zData, isRapide_);
      if (crbs == null) {
        continue;
      }
      crbDataMin_[c].valeurs_= crbs[0];
      if (z2Data != null) {
        crbDataMax_[c].valeurs_= crbs[1];
      }
      c++;
    }
  }
  private void initGraphe() {
    if ((biefs_.size() == 0)
      || (rsn_ == null)
      || (temps_ < 0)
      || (temps_ >= rsn_.pasTemps.length)) {
      return;
    }
    if ((axeX_[0] == null)
      || (axeX_[1] == null)
      || (axeZ_[0] == null)
      || (axeZ_[1] == null)) {
      System.err.println("LidoGrapheLignedeau: data not commited");
      return;
    }
    Aspect aspect;
    graphe_= new Graphe();
    marges_= new Marges();
    marges_.gauche_= 60;
    marges_.droite_= 80;
    marges_.haut_= 45;
    marges_.bas_= 30;
    graphe_.titre_= crbTitre_;
    graphe_.animation_= false;
    graphe_.legende_= true;
    graphe_.marges_= marges_;
    axeX= new Axe();
    axeX.titre_= "abscisse";
    axeX.unite_= "m";
    axeX.vertical_= false;
    axeX.graduations_= true;
    axeX.grille_= false;
    graphe_.ajoute(axeX);
    axeY= new Axe();
    axeY.titre_= axeYLabel_;
    axeY.unite_= axeYUnite_;
    axeY.vertical_= true;
    axeY.graduations_= true;
    axeY.grille_= false;
    graphe_.ajoute(axeY);
    crbDataMin_= new CourbeDefault[biefs_.size()];
    crbDataMax_= new CourbeDefault[biefs_.size()];
    final int coul= 0xFF0000;
    for (int c= 0; c < crbDataMin_.length; c++) {
      crbDataMin_[c]= new CourbeDefault();
      crbDataMin_[c].trace_= "lineaire";
      crbDataMin_[c].marqueurs_= false;
      aspect= new Aspect();
      aspect.contour_= new Color(coul);
      crbDataMin_[c].aspect_= aspect;
      graphe_.ajoute(crbDataMin_[c]);
      crbDataMax_[c]= new CourbeDefault();
      crbDataMax_[c].trace_= "lineaire";
      crbDataMax_[c].marqueurs_= false;
      aspect= new Aspect();
      aspect.contour_= new Color(coul);
      crbDataMax_[c].aspect_= aspect;
      graphe_.ajoute(crbDataMax_[c]);
    }
    ctr_= new Contrainte[contraintes_.length];
    for (int i= 0; i < contraintes_.length; i++) {
      ctr_[i]= new Contrainte();
      ctr_[i].titre_= "" + contraintes_[i];
      ctr_[i].vertical_= true;
      ctr_[i].type_= "max";
      ctr_[i].couleur_= new Color(0xFF, 0xAA, 0xAA);
      graphe_.ajoute(ctr_[i]);
    }
    setGraphe(graphe_);
  }
  private void updateGraphe() {
    if ((biefs_.size() == 0)
      || (rsn_ == null)
      || (temps_ < 0)
      || (temps_ >= rsn_.pasTemps.length)) {
      return;
    }
    if ((axeX_[0] == null)
      || (axeX_[1] == null)
      || (axeZ_[0] == null)
      || (axeZ_[1] == null)) {
      System.err.println("LidoGrapheLignedeau: data not commited");
      return;
    }
    axeX.minimum_= axeX_[0] != null ? axeX_[0].doubleValue() : minAbsVal;
    axeX.maximum_= axeX_[1] != null ? axeX_[1].doubleValue() : maxAbsVal;
    axeY.minimum_= axeZ_[0] != null ? axeZ_[0].doubleValue() : minCotVal;
    axeY.maximum_= axeZ_[1] != null ? axeZ_[1].doubleValue() : maxCotVal;
    for (int c= 0; c < crbDataMin_.length; c++) {
      crbDataMin_[c].visible_= minOn_;
      crbDataMax_[c].visible_= maxOn_;
    }
    for (int i= 0; i < ctr_.length; i++) {
      graphe_.enleve(ctr_[i]);
    }
    final Contrainte[] oldCtr= ctr_;
    ctr_= new Contrainte[contraintes_.length];
    for (int i= 0; i < Math.min(ctr_.length, oldCtr.length); i++) {
      ctr_[i]= oldCtr[i];
    }
    for (int i= Math.min(ctr_.length, oldCtr.length); i < ctr_.length; i++) {
      ctr_[i]= new Contrainte();
      ctr_[i].titre_= "" + contraintes_[i];
      ctr_[i].vertical_= true;
      ctr_[i].type_= "max";
      ctr_[i].couleur_= new Color(0xFF, 0xAA, 0xAA);
      graphe_.ajoute(ctr_[i]);
    }
    for (int i= 0; i < contraintes_.length; i++) {
      ctr_[i].v_= contraintes_[i];
      ctr_[i].titre_= "" + contraintes_[i];
    }
    for (int i= 0; i < ctr_.length; i++) {
      graphe_.ajoute(ctr_[i]);
    }
    fullRepaint();
  }
}
