/*
 * @file         LidoGrapheSeuilDenoye.java
 * @creation     1999-08-24
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;

import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.ebli.graphe.BGraphe;

import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoGrapheSeuilDenoye
  extends BGraphe
  implements PropertyChangeListener {
  public static int ICOTAMONT= 1;
  public static int IDEBIT= 0;
  public static int INBDEBIT= 1;
  public static int ICOTMOY= 0;
  private NumberFormat nf2_;
  private SParametresSingBlocSNG cl_;
  private double cotFond_;
  public LidoGrapheSeuilDenoye() {
    super();
    setName("" + Math.random());
    //    nf2_=NumberFormat.getInstance(Locale.US);
    //    nf2_.setMaximumFractionDigits(3);
    //    nf2_.setGroupingUsed(false);
    cl_= null;
    nf2_= LidoParamsHelper.NUMBER_FORMAT;
    cotFond_= Double.POSITIVE_INFINITY;
    setPreferredSize(new Dimension(640, 480));
    init();
  }
  private void init() {
    if (cl_ == null) {
      return;
    }
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
  }
  public void setFond(final double fond) {
    final double vp= cotFond_;
    cotFond_= fond;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("fond", vp, cotFond_);
  }
  public void setSingularite(final SParametresSingBlocSNG s) {
    if (s == cl_) {
      return;
    }
    final SParametresSingBlocSNG vp= cl_;
    cl_= s;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("singularite", vp, cl_);
  }
  // PropertyChangeListener
  public void propertyChange(final PropertyChangeEvent e) {
    if ("singularite".equals(e.getPropertyName())) {
      final SParametresSingBlocSNG nv= (SParametresSingBlocSNG)e.getNewValue();
      setSingularite(nv);
      init();
    }
  }
  private InputStream generateGDF() throws IOException {
    if ((cl_ == null)
      || (cl_.tabDonLois == null)
      || (cl_.tabDonLois.length < (IDEBIT + 1))) {
      return null;
    }
    final int nbDebit= cl_.tabParamEntier[INBDEBIT];
    String gdfStr= "";
    int pasX= 5, pasZ= 5;
    final String coteAmontCrb[]= new String[cl_.tabParamEntier[INBDEBIT]];
    final String coteAmontTit[]= new String[cl_.tabParamEntier[INBDEBIT]];
    final String crbTitre= "Singularité " + cl_.numSing;
    final double maxAbsVal= 40.;
    final double minAbsVal= 0.;
    double maxCotVal= 0.;
    double minCotVal= 0.;
    // le min, a priori, en seuil noye, seuil le seuil...
    minCotVal= maxCotVal= Math.min(cl_.tabParamReel[ICOTMOY], cotFond_);
    final String seuilCrb=
      "      "
        + nf2_.format((minAbsVal + maxAbsVal) / 2)
        + " "
        + nf2_.format(cl_.tabParamReel[ICOTMOY])
        + "\n";
    for (int debit= 0; debit < nbDebit; debit++) {
      if (cl_.tabDonLois[ICOTAMONT][debit] < minCotVal) {
        minCotVal= cl_.tabDonLois[ICOTAMONT][debit];
      }
      if (cl_.tabDonLois[ICOTAMONT][debit] > maxCotVal) {
        maxCotVal= cl_.tabDonLois[ICOTAMONT][debit];
      }
      coteAmontCrb[debit]=
        "      "
          + nf2_.format(minAbsVal)
          + " "
          + nf2_.format(cl_.tabDonLois[ICOTAMONT][debit])
          + "\n";
      coteAmontCrb[debit] += "      "
        + nf2_.format(minAbsVal + 10.)
        + " "
        + nf2_.format(cl_.tabDonLois[ICOTAMONT][debit])
        + "\n";
      coteAmontTit[debit]= "Z aval " + cl_.tabDonLois[IDEBIT][debit];
    }
    double minZ= Math.floor(minCotVal);
    double maxZ= Math.ceil(maxCotVal);
    final double minX= Math.floor(minAbsVal);
    final double maxX= Math.ceil(maxAbsVal);
    pasX= (int) ((maxX - minX) / 10);
    pasZ= (int) ((maxZ - minZ) / 10);
    minZ -= pasZ;
    maxZ += pasZ;
    gdfStr += "graphe\n{\n";
    gdfStr += "  titre \"" + crbTitre + "\n";
    gdfStr += "  animation non\n";
    gdfStr += "  legende oui\n";
    gdfStr += "  marges\n  {\n";
    gdfStr += "    gauche 60\n";
    gdfStr += "    droite 80\n";
    gdfStr += "    haut   45\n";
    gdfStr += "    bas    30\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    orientation horizontal\n";
    gdfStr += "    minimum " + nf2_.format(minX) + "\n";
    gdfStr += "    maximum " + nf2_.format(maxX) + "\n";
    gdfStr += "    pas " + pasX + "\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"cote\"\n";
    gdfStr += "    unite \"m\"\n";
    gdfStr += "    orientation vertical\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf2_.format(minZ) + "\n";
    gdfStr += "    maximum " + nf2_.format(maxZ) + "\n";
    gdfStr += "    pas " + pasZ + "\n  }\n";
    int coulAm= 0x5555FF;
    for (int i= 0; i < nbDebit; i++) {
      gdfStr += "  courbe\n  {\n";
      gdfStr += "    titre \"" + coteAmontTit[i] + "\"\n";
      gdfStr += "    marqueurs non\n";
      gdfStr += "    aspect\n    {\n";
      gdfStr += "      contour.couleur "
        + Integer.toHexString(coulAm)
        + "\n    }\n";
      gdfStr += "    valeurs\n    {\n";
      gdfStr += coteAmontCrb[i];
      gdfStr += "    }\n  }\n";
      coulAm= couleur(coulAm);
    }
    gdfStr += "  courbe\n  {\n";
    gdfStr += "    titre \"seuil\"\n";
    gdfStr += "    type histogramme\n";
    gdfStr += "    marqueurs non\n";
    gdfStr += "    aspect\n    {\n";
    gdfStr += "      contour.couleur 00FF00\n    }\n";
    gdfStr += "    valeurs\n    {\n";
    gdfStr += seuilCrb;
    gdfStr += "    }\n  }\n";
    if (cotFond_ < Double.POSITIVE_INFINITY) {
      gdfStr += "  contrainte\n  {\n";
      gdfStr += "    orientation vertical\n";
      gdfStr += "    couleur FFAAAA\n";
      gdfStr += "    type max\n";
      gdfStr += "    valeur " + nf2_.format(cotFond_) + "\n  }\n";
    }
    gdfStr += "}\n";
    //FileWriter GDFout=new FileWriter("lido.gdf");
    //GDFout.write(gdfStr, 0, gdfStr.length());
    //GDFout.close();
    return new ByteArrayInputStream(gdfStr.getBytes());
  }
  private int couleur(final int base) {
    int r= base >> 16 & 0xFF;
    int g= base >> 8 & 0xFF;
    int b= base & 0xFF;
    if ((r <= g) && (r <= b)) {
      r += 50;
      g -= 25;
      b -= 25;
    } else if ((g <= r) && (g <= b)) {
      r -= 25;
      g += 50;
      b -= 25;
    } else if ((b <= g) && (b <= r)) {
      r -= 25;
      g -= 25;
      b += 50;
    }
    r= Math.abs(r % 0x100);
    g= Math.abs(g % 0x100);
    b= Math.abs(b % 0x100);
    return (r << 16 | g << 8 | b);
  }
}
