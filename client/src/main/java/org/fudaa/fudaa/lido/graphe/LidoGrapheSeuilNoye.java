/*
 * @file         LidoGrapheSeuilNoye.java
 * @creation     1999-08-24
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;

import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.ebli.graphe.BGraphe;

import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoGrapheSeuilNoye
  extends BGraphe
  implements PropertyChangeListener {
  public static int ICOTAVAL= 0;
  public static int IDEBIT= 1;
  public static int INBCOTAVAL= 1;
  public static int INBDEBIT= 2;
  public static int ICOTMOY= 0;
  public static int ICOTAMONTOFFSET= 2;
  private NumberFormat nf2_;
  private int indDebit_, indCoteAval_;
  private SParametresSingBlocSNG cl_;
  private double cotFond_;
  public LidoGrapheSeuilNoye() {
    super();
    nf2_= LidoParamsHelper.NUMBER_FORMAT;
    //    nf2_=NumberFormat.getInstance(Locale.US);
    //    nf2_.setMaximumFractionDigits(3);
    //    nf2_.setGroupingUsed(false);
    cl_= null;
    setPreferredSize(new Dimension(640, 480));
    cotFond_= Double.POSITIVE_INFINITY;
    init();
  }
  private void init() {
    if (cl_ == null) {
      return;
    }
    indDebit_= -1;
    if (cl_.tabParamEntier[INBCOTAVAL] > 0) {
      indCoteAval_= 0;
    } else {
      indCoteAval_= -1;
    }
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
  }
  public void setSingularite(final SParametresSingBlocSNG s) {
    if (s == cl_) {
      return;
    }
    final SParametresSingBlocSNG vp= cl_;
    cl_= s;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("singularite", vp, cl_);
  }
  public void setDebit(final int deb) {
    if ((deb == indDebit_)
      || (deb < 0)
      || (deb >= cl_.tabParamEntier[INBDEBIT])) {
      return;
    }
    final int vp= indDebit_;
    indCoteAval_= -1;
    indDebit_= deb;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("debit", vp, indDebit_);
  }
  public void setCoteAval(final int cot) {
    if ((cot == indCoteAval_)
      || (cot < 0)
      || (cot >= cl_.tabParamEntier[INBCOTAVAL])) {
      return;
    }
    final int vp= indCoteAval_;
    indDebit_= -1;
    indCoteAval_= cot;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("coteAval", vp, indCoteAval_);
  }
  public void setFond(final double fond) {
    final double vp= cotFond_;
    cotFond_= fond;
    try {
      final InputStream is= generateGDF();
      if (is != null) {
        setFluxDonnees(is);
        is.close();
      }
    } catch (final IOException ex) {
      System.err.println(ex);
    }
    firePropertyChange("fond", vp, cotFond_);
  }
  // PropertyChangeListener
  public void propertyChange(final PropertyChangeEvent e) {
    if ("singularite".equals(e.getPropertyName())) {
      final SParametresSingBlocSNG nv= (SParametresSingBlocSNG)e.getNewValue();
      setSingularite(nv);
      try {
        final InputStream is= generateGDF();
        if (is != null) {
          setFluxDonnees(is);
          is.close();
        }
      } catch (final IOException ex) {
        System.err.println(ex);
      }
    }
  }
  private InputStream generateGDF() throws IOException {
    if ((cl_ == null)
      || (cl_.tabDonLois == null)
      || (cl_.tabDonLois.length < ICOTAMONTOFFSET + 1)) {
      return null;
    }
    final int nbDebit= cl_.tabParamEntier[INBDEBIT];
    final int nbCoteAval= cl_.tabParamEntier[INBCOTAVAL];
    String gdfStr= "";
    String coteAvalCrb[]= null;
    String coteAvalTit[]= null;
    String coteAmontCrb[]= null;
    String coteAmontTit[]= null;
    final String crbTitre= "Singularité " + cl_.numSing;
    int pasX= 5, pasZ= 5;
    final double maxAbsVal= 40.;
    final double minAbsVal= 0.;
    double maxCotVal= 0.;
    double minCotVal= 0.;
    // le min, a priori, en seuil noye, seuil le seuil...
    minCotVal= maxCotVal= Math.min(cl_.tabParamReel[ICOTMOY], cotFond_);
    final String seuilCrb=
      "      "
        + nf2_.format((minAbsVal + maxAbsVal) / 2)
        + " "
        + nf2_.format(cl_.tabParamReel[ICOTMOY])
        + "\n";
    // on a selectionne un debit (une "colonne")
    if (indCoteAval_ == -1) {
      coteAvalCrb= new String[nbCoteAval];
      coteAvalTit= new String[nbCoteAval];
      coteAmontCrb= new String[nbCoteAval];
      coteAmontTit= new String[nbCoteAval];
      for (int i= 0; i < nbCoteAval; i++) {
        coteAvalCrb[i]= "";
        coteAvalTit[i]= "";
        coteAmontCrb[i]= "";
        coteAmontTit[i]= "";
      }
    } else      // on a selectionne une cote aval (une "ligne")
      if (indDebit_ == -1) {
        coteAvalCrb= new String[] { "" };
        coteAvalTit= new String[] { "" };
        coteAmontCrb= new String[nbDebit];
        coteAmontTit= new String[nbDebit];
        for (int i= 0; i < nbDebit; i++) {
          coteAmontCrb[i]= "";
          coteAmontTit[i]= "";
        }
      } else {
        coteAvalCrb=
          new String[] {
            "      "
              + nf2_.format(maxAbsVal - 10.)
              + " "
              + nf2_.format(0.)
              + "\n",
            "      " + nf2_.format(maxAbsVal) + " " + nf2_.format(0.) + "\n" };
        coteAvalTit= new String[] { "" };
        coteAmontCrb=
          new String[] {
            "      " + nf2_.format(minAbsVal) + " " + nf2_.format(0.) + "\n",
            "      "
              + nf2_.format(minAbsVal + 10.)
              + " "
              + nf2_.format(0.)
              + "\n" };
        coteAmontTit= new String[] { "" };
      }
    // cotes aval
    for (int i= 0; i < nbCoteAval; i++) {
      if (cl_.tabDonLois[ICOTAVAL][i] < minCotVal) {
        minCotVal= cl_.tabDonLois[ICOTAVAL][i];
      }
      if (cl_.tabDonLois[ICOTAVAL][i] > maxCotVal) {
        maxCotVal= cl_.tabDonLois[ICOTAVAL][i];
      }
      // on a selectionne un debit
      if (indCoteAval_ == -1) {
        coteAvalCrb[i] += "      "
          + nf2_.format(maxAbsVal - 10.)
          + " "
          + nf2_.format(cl_.tabDonLois[ICOTAVAL][i])
          + "\n";
        coteAvalCrb[i] += "      "
          + nf2_.format(maxAbsVal)
          + " "
          + nf2_.format(cl_.tabDonLois[ICOTAVAL][i])
          + "\n";
        coteAvalTit[i] += "Zav. " + cl_.tabDonLois[ICOTAVAL][i];
        // on decale de 2 car les cotes amont commences a la ligne 3 du tabDonLois (!...)
        coteAmontCrb[i] += "      "
          + nf2_.format(minAbsVal)
          + " "
          + nf2_.format(cl_.tabDonLois[indDebit_ + ICOTAMONTOFFSET][i])
          + "\n";
        coteAmontCrb[i] += "      "
          + nf2_.format(minAbsVal + 10.)
          + " "
          + nf2_.format(cl_.tabDonLois[indDebit_ + ICOTAMONTOFFSET][i])
          + "\n";
        coteAmontTit[i] += "Zam.(av. " + cl_.tabDonLois[ICOTAVAL][i] + ")";
      }
    }
    // on a selectionne une cote aval
    if (indDebit_ == -1) {
      coteAvalCrb[0] += "      "
        + nf2_.format(maxAbsVal - 10.)
        + " "
        + nf2_.format(cl_.tabDonLois[ICOTAVAL][indCoteAval_])
        + "\n";
      coteAvalCrb[0] += "      "
        + nf2_.format(maxAbsVal)
        + " "
        + nf2_.format(cl_.tabDonLois[ICOTAVAL][indCoteAval_])
        + "\n";
      coteAvalTit[0] += "Zav. " + cl_.tabDonLois[ICOTAVAL][indCoteAval_];
    }
    // cotes amont
    for (int debit= 0; debit < nbDebit; debit++) {
      // on decale de 2 car les cotes amont commences a la ligne 3 du tabDonLois (!...)
      for (int i= 0; i < nbCoteAval; i++) {
        if (cl_.tabDonLois[debit + ICOTAMONTOFFSET][i] < minCotVal) {
          minCotVal= cl_.tabDonLois[debit + ICOTAMONTOFFSET][i];
        }
        if (cl_.tabDonLois[debit + ICOTAMONTOFFSET][i] > maxCotVal) {
          maxCotVal= cl_.tabDonLois[debit + ICOTAMONTOFFSET][i];
        }
      }
      // on a selectionne une cote aval
      if (indDebit_ == -1) {
        coteAmontCrb[debit] += "      "
          + nf2_.format(minAbsVal)
          + " "
          + nf2_.format(cl_.tabDonLois[debit + ICOTAMONTOFFSET][indCoteAval_])
          + "\n";
        coteAmontCrb[debit] += "      "
          + nf2_.format(minAbsVal + 10.)
          + " "
          + nf2_.format(cl_.tabDonLois[debit + ICOTAMONTOFFSET][indCoteAval_])
          + "\n";
        coteAmontTit[debit] += "Zam.(Q " + cl_.tabDonLois[IDEBIT][debit] + ")";
      }
    }
    double minZ= Math.floor(minCotVal);
    double maxZ= Math.ceil(maxCotVal);
    final double minX= Math.floor(minAbsVal);
    final double maxX= Math.ceil(maxAbsVal);
    pasX= (int) ((maxX - minX) / 10);
    pasZ= (int) ((maxZ - minZ) / 10);
    minZ -= pasZ;
    maxZ += pasZ;
    gdfStr += "graphe\n{\n";
    gdfStr += "  titre \"" + crbTitre + "\n";
    gdfStr += "  animation non\n";
    gdfStr += "  legende oui\n";
    gdfStr += "  marges\n  {\n";
    gdfStr += "    gauche 60\n";
    gdfStr += "    droite 80\n";
    gdfStr += "    haut   45\n";
    gdfStr += "    bas    30\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"abscisse\"\n";
    gdfStr += "    unite \"m\"\n";
    gdfStr += "    orientation horizontal\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf2_.format(minX) + "\n";
    gdfStr += "    maximum " + nf2_.format(maxX) + "\n";
    gdfStr += "    pas " + pasX + "\n  }\n";
    gdfStr += "  axe\n  {\n";
    gdfStr += "    titre \"cote\"\n";
    gdfStr += "    unite \"m\"\n";
    gdfStr += "    orientation vertical\n";
    gdfStr += "    graduations oui\n";
    gdfStr += "    minimum " + nf2_.format(minZ) + "\n";
    gdfStr += "    maximum " + nf2_.format(maxZ) + "\n";
    gdfStr += "    pas " + pasZ + "\n  }\n";
    int coulAv= 0xFF5555;
    int coulAm= 0x5555FF;
    // on a selectionne un debit (une "colonne")
    if (indCoteAval_ == -1) {
      for (int i= 0; i < nbCoteAval; i++) {
        // aval
        gdfStr += "  courbe\n  {\n";
        gdfStr += "    titre \"" + coteAvalTit[i] + "\"\n";
        gdfStr += "    marqueurs non\n";
        gdfStr += "    aspect\n    {\n";
        gdfStr += "      contour.couleur "
          + Integer.toHexString(coulAv)
          + "\n    }\n";
        gdfStr += "    valeurs\n    {\n";
        gdfStr += coteAvalCrb[i];
        gdfStr += "    }\n  }\n";
        // amont
        gdfStr += "  courbe\n  {\n";
        gdfStr += "    titre \"" + coteAmontTit[i] + "\"\n";
        gdfStr += "    marqueurs non\n";
        gdfStr += "    aspect\n    {\n";
        gdfStr += "      contour.couleur "
          + Integer.toHexString(coulAm)
          + "\n    }\n";
        gdfStr += "    valeurs\n    {\n";
        gdfStr += coteAmontCrb[i];
        gdfStr += "    }\n  }\n";
        coulAv= couleur(coulAv);
        coulAm= couleur(coulAm);
      }
    } else      // on a selectionne une cote aval (une "ligne")
      if (indDebit_ == -1) {
        // aval
        gdfStr += "  courbe\n  {\n";
        gdfStr += "    titre \"" + coteAvalTit[0] + "\"\n";
        gdfStr += "    marqueurs non\n";
        gdfStr += "    aspect\n    {\n";
        gdfStr += "      contour.couleur "
          + Integer.toHexString(coulAv)
          + "\n    }\n";
        gdfStr += "    valeurs\n    {\n";
        gdfStr += coteAvalCrb[0];
        gdfStr += "    }\n  }\n";
        // amont
        for (int i= 0; i < nbDebit; i++) {
          gdfStr += "  courbe\n  {\n";
          gdfStr += "    titre \"" + coteAmontTit[i] + "\"\n";
          gdfStr += "    marqueurs non\n";
          gdfStr += "    aspect\n    {\n";
          gdfStr += "      contour.couleur "
            + Integer.toHexString(coulAm)
            + "\n    }\n";
          gdfStr += "    valeurs\n    {\n";
          gdfStr += coteAmontCrb[i];
          gdfStr += "    }\n  }\n";
          coulAm= couleur(coulAm);
        }
      }
    gdfStr += "  courbe\n  {\n";
    gdfStr += "    titre \"seuil\"\n";
    gdfStr += "    type histogramme\n";
    gdfStr += "    marqueurs non\n";
    gdfStr += "    aspect\n    {\n";
    gdfStr += "      contour.couleur 00FF00\n    }\n";
    gdfStr += "    valeurs\n    {\n";
    gdfStr += seuilCrb;
    gdfStr += "    }\n  }\n";
    if (cotFond_ < Double.POSITIVE_INFINITY) {
      gdfStr += "  contrainte\n  {\n";
      gdfStr += "    orientation vertical\n";
      gdfStr += "    couleur FFAAAA\n";
      gdfStr += "    type max\n";
      gdfStr += "    valeur " + nf2_.format(cotFond_) + "\n  }\n";
    }
    gdfStr += "}\n";
    //FileWriter GDFout=new FileWriter("lido.gdf");
    //GDFout.write(gdfStr, 0, gdfStr.length());
    //GDFout.close();
    return new ByteArrayInputStream(gdfStr.getBytes());
  }
  private int couleur(final int base) {
    int r= base >> 16 & 0xFF;
    int g= base >> 8 & 0xFF;
    int b= base & 0xFF;
    if ((r <= g) && (r <= b)) {
      r += 50;
      g -= 25;
      b -= 25;
    } else if ((g <= r) && (g <= b)) {
      r -= 25;
      g += 50;
      b -= 25;
    } else if ((b <= g) && (b <= r)) {
      r -= 25;
      g -= 25;
      b += 50;
    }
    r= Math.abs(r % 0x100);
    g= Math.abs(g % 0x100);
    b= Math.abs(b % 0x100);
    return (r << 16 | g << 8 | b);
  }
}
