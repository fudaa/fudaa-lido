/*
 * @file         LidoGrapheSingulariteCourbe.java
 * @creation     1999-08-24
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.graphe;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;

import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoGrapheSingulariteCourbe
  extends BGraphe
  implements PropertyChangeListener {
  public static int INBPOINT= LidoResource.SINGULARITE.ICOURBE.INBPOINT;
  public static int IHT= LidoResource.SINGULARITE.ICOURBE.IHT;
  public static int IHL= LidoResource.SINGULARITE.ICOURBE.IHL;
  public static int IQT= LidoResource.SINGULARITE.ICOURBE.IQT;
  public static int ITL= LidoResource.SINGULARITE.ICOURBE.ITL;
  public static int IYS= LidoResource.SINGULARITE.ICOURBE.IYS;
  public static int IZS= LidoResource.SINGULARITE.ICOURBE.IZS;
  public static int IVARMAXCOT= LidoResource.SINGULARITE.ICOURBE.IVARMAXCOT;
  public static int ICOTMOY= LidoResource.SINGULARITE.ICOURBE.ICOTMOY;
  public static int ICOEFDEBIT= LidoResource.SINGULARITE.ICOURBE.ICOEFDEBIT;
  private SParametresSingBlocSNG cl_;
  private int IABS;
  private int IORD;
  private String GRA_TITLE;
  private String GRA_ABS_LABEL;
  private String GRA_ORD_LABEL;
  private String GRA_ABS_UNITE;
  private String GRA_ORD_UNITE;
  private Graphe graphe_;
  private Marges marges_;
  private Axe axeX, axeY;
  private CourbeDefault crb_;
  public LidoGrapheSingulariteCourbe(
    final int iabs,
    final int iord,
    final String gra_title,
    final String gra_abs_label,
    final String gra_ord_label,
    final String gra_abs_unite,
    final String gra_ord_unite) {
    super();
    IABS= iabs;
    IORD= iord;
    GRA_TITLE= gra_title;
    GRA_ABS_LABEL= gra_abs_label;
    GRA_ORD_LABEL= gra_ord_label;
    GRA_ABS_UNITE= gra_abs_unite;
    GRA_ORD_UNITE= gra_ord_unite;
    cl_= null;
    setPreferredSize(new Dimension(640, 480));
  }
  public void setSingularite(final SParametresSingBlocSNG s) {
    if (s == cl_) {
      return;
    }
    final SParametresSingBlocSNG vp= cl_;
    cl_= s;
    if (getGraphe() == null) {
      initGraphe();
    }
    updateGraphe();
    firePropertyChange("singularite", vp, cl_);
  }
  // PropertyChangeListener
  public void propertyChange(final PropertyChangeEvent e) {
    if ("singularite".equals(e.getPropertyName())) {
      final SParametresSingBlocSNG nv= (SParametresSingBlocSNG)e.getNewValue();
      setSingularite(nv);
    }
  }
  private void initGraphe() {
    if ((cl_ == null)
      || (cl_.tabDonLois == null)
      || (cl_.tabDonLois.length < (IORD + 1))
      || (cl_.tabDonLois.length < (IABS + 1))) {
      return;
    }
    graphe_= new Graphe();
    marges_= new Marges();
    marges_.gauche_= 60;
    marges_.droite_= 80;
    marges_.haut_= 45;
    marges_.bas_= 30;
    graphe_.titre_= "Singularité " + cl_.numSing;
    graphe_.animation_= false;
    graphe_.legende_= true;
    graphe_.marges_= marges_;
    axeX= new Axe();
    axeX.titre_= GRA_ABS_LABEL;
    axeX.unite_= GRA_ABS_UNITE;
    axeX.vertical_= false;
    axeX.graduations_= true;
    graphe_.ajoute(axeX);
    axeY= new Axe();
    axeY.titre_= GRA_ORD_LABEL;
    axeY.unite_= GRA_ORD_UNITE;
    axeY.vertical_= true;
    axeY.graduations_= true;
    graphe_.ajoute(axeY);
    crb_= new CourbeDefault();
    crb_.titre_= GRA_TITLE;
    crb_.marqueurs_= true;
    graphe_.ajoute(crb_);
    setGraphe(graphe_);
  }
  private void updateGraphe() {
    if ((cl_ == null)
      || (cl_.tabDonLois == null)
      || (cl_.tabDonLois.length < (IORD + 1))
      || (cl_.tabDonLois.length < (IABS + 1))) {
      return;
    }
    final int nbPoint= cl_.tabParamEntier[INBPOINT];
    if (nbPoint <= 0) {
      return;
    }
    Valeur v;
    final Vector crbVals= new Vector();
    double maxAbsVal= 0.;
    double minAbsVal= 0.;
    double maxCotVal= 0.;
    double minCotVal= 0.;
    if (nbPoint > 0) {
      minAbsVal= maxAbsVal= cl_.tabDonLois[IABS][0];
      minCotVal= maxCotVal= cl_.tabDonLois[IORD][0];
    }
    for (int i= 0; i < nbPoint; i++) {
      if (minAbsVal > cl_.tabDonLois[IABS][i]) {
        minAbsVal= cl_.tabDonLois[IABS][i];
      }
      if (maxAbsVal < cl_.tabDonLois[IABS][i]) {
        maxAbsVal= cl_.tabDonLois[IABS][i];
      }
      if (minCotVal > cl_.tabDonLois[IORD][i]) {
        minCotVal= cl_.tabDonLois[IORD][i];
      }
      if (maxCotVal < cl_.tabDonLois[IORD][i]) {
        maxCotVal= cl_.tabDonLois[IORD][i];
      }
      v= new Valeur();
      v.s_= cl_.tabDonLois[IABS][i];
      v.v_= cl_.tabDonLois[IORD][i];
      crbVals.add(v);
    }
    axeX.minimum_= minAbsVal;
    axeX.maximum_= maxAbsVal;
    axeY.minimum_= minCotVal;
    axeY.maximum_= maxCotVal;
    crb_.valeurs_= crbVals;
    fullRepaint();
  }
}
