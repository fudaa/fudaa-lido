/*
 * @file         LidoIHMParams.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Id: LidoIHMParams.java,v 1.10 2006-09-19 15:04:59 deniger Exp $
 * @author       Axel von Arnim 
 */
public class LidoIHMParams implements FudaaProjetListener {
  FudaaProjet projet_;
  LidoIHM_Profil ihmProfil_;
  LidoIHM_Calage ihmCalage_;
  LidoIHM_Laisse ihmLaisse_;
  LidoIHM_Reseau ihmReseau_;
  LidoIHM_Bief ihmBief_;
  LidoIHM_Noeud ihmNoeud_;
  LidoIHM_Limite ihmLimite_;
  LidoIHM_Apport ihmApport_;
  LidoIHM_Perte ihmPerte_;
  LidoIHM_Singularite ihmSingularite_;
  LidoIHM_Paramcalcul ihmParamcalcul_;
  LidoIHM_Sectioncalcul ihmSectioncalcul_;
  LidoIHM_Planimetrage ihmPlanimetrage_;
  LidoIHM_Vartemp ihmVartemp_;
  LidoIHM_Resultatenlong ihmResultatenlong_;
  LidoIHM_Resultatprofil ihmResultatprofil_;
  LidoIHM_Resultatlaisse ihmResultatlaisse_;
  LidoIHM_Resultaterreurs ihmResultaterreurs_;
  LidoParamsHelper ph_;
  public LidoIHMParams() {
    ihmProfil_= null;
    ihmCalage_= null;
    ihmLaisse_= null;
    ihmReseau_= null;
    ihmBief_= null;
    ihmNoeud_= null;
    ihmLimite_= null;
    ihmApport_= null;
    ihmPerte_= null;
    ihmSingularite_= null;
    ihmParamcalcul_= null;
    ihmSectioncalcul_= null;
    ihmPlanimetrage_= null;
    ihmVartemp_= null;
    ihmResultatenlong_= null;
    ihmResultatprofil_= null;
    ihmResultatlaisse_= null;
    ihmResultaterreurs_= null;
    ph_= new LidoParamsHelper();
  }
  public void setProjet(final FudaaProjet p) {
    if (p == projet_) {
      return;
    }
    if (projet_ != null) {
      projet_.removeFudaaProjetListener(this);
    }
    projet_= p;
    projet_.addFudaaProjetListener(this);
    ph_.setProjet(p);
  }
  // ferme toutes les fenetres ihm
  public void fermer() {
    if (ihmProfil_ != null) {
      ihmProfil_.fermer();
    }
    if (ihmCalage_ != null) {
      ihmCalage_.fermer();
    }
    if (ihmLaisse_ != null) {
      ihmLaisse_.fermer();
    }
    if (ihmReseau_ != null) {
      ihmReseau_.fermer();
    }
    if (ihmBief_ != null) {
      ihmBief_.fermer();
    }
    if (ihmNoeud_ != null) {
      ihmNoeud_.fermer();
    }
    if (ihmLimite_ != null) {
      ihmLimite_.fermer();
    }
    if (ihmApport_ != null) {
      ihmApport_.fermer();
    }
    if (ihmPerte_ != null) {
      ihmPerte_.fermer();
    }
    if (ihmSingularite_ != null) {
      ihmSingularite_.fermer();
    }
    if (ihmParamcalcul_ != null) {
      ihmParamcalcul_.fermer();
    }
    if (ihmSectioncalcul_ != null) {
      ihmSectioncalcul_.fermer();
    }
    if (ihmPlanimetrage_ != null) {
      ihmPlanimetrage_.fermer();
    }
    if (ihmVartemp_ != null) {
      ihmVartemp_.fermer();
    }
    fermerResultats();
  }
  // ferme toutes les fenetres resultat
  public void fermerResultats() {
    if (ihmResultatenlong_ != null) {
      ihmResultatenlong_.fermer();
    }
    if (ihmResultatprofil_ != null) {
      ihmResultatprofil_.fermer();
    }
    //    if( ihmResultatlaisse_!=null )       ihmResultatlaisse_.fermer();
    if (ihmResultatlaisse_ != null) {
      ihmResultatlaisse_.fermer();
    }
    if (ihmResultaterreurs_ != null) {
      ihmResultaterreurs_.fermer();
    }
  }
  // FudaaProjetListener
  public void dataChanged(final FudaaProjetEvent e) {
    final FudaaProjet p= (FudaaProjet)e.getSource();
    switch (e.getID()) {
      case FudaaProjetEvent.PARAM_ADDED :
      case FudaaProjetEvent.PARAM_IMPORTED :
        {
          setProjetParametres(p);
        }
      case FudaaProjetEvent.RESULT_ADDED :
      case FudaaProjetEvent.RESULTS_CLEARED :
        {
          fermerResultats();
          setProjetResultats(p);
          break;
        }
    }
  }
  public void statusChanged(final FudaaProjetEvent e) {
    final FudaaProjet p= (FudaaProjet)e.getSource();
    if ((e.getID() == FudaaProjetEvent.PROJECT_OPENED)
      || (e.getID() == FudaaProjetEvent.PROJECT_CLOSED)) {
      fermer();
    }
    setProjetParametres(p);
    setProjetResultats(p);
  }
  public void initialiseParametres() {
    ph_.initialiseParametres();
  }
  public LidoParamsHelper getPH() {
    return ph_;
  }
  public LidoIHM_Profil PROFIL() {
    if (ihmProfil_ == null) {
      ihmProfil_= new LidoIHM_Profil(projet_, ph_);
    }
    return ihmProfil_;
  }
  public LidoIHM_Calage CALAGE() {
    if (ihmCalage_ == null) {
      ihmCalage_= new LidoIHM_Calage(projet_, ph_);
    }
    return ihmCalage_;
  }
  public LidoIHM_Laisse LAISSE() {
    if (ihmLaisse_ == null) {
      ihmLaisse_= new LidoIHM_Laisse(projet_, ph_);
    }
    return ihmLaisse_;
  }
  public LidoIHM_Reseau RESEAU() {
    if (ihmReseau_ == null) {
      ihmReseau_= new LidoIHM_Reseau(projet_, ph_);
    }
    return ihmReseau_;
  }
  public LidoIHM_Bief BIEF() {
    if (ihmBief_ == null) {
      ihmBief_= new LidoIHM_Bief(projet_, ph_);
    }
    return ihmBief_;
  }
  public LidoIHM_Noeud NOEUD() {
    if (ihmNoeud_ == null) {
      ihmNoeud_= new LidoIHM_Noeud(projet_, ph_);
    }
    return ihmNoeud_;
  }
  public LidoIHM_Limite LIMITE() {
    if (ihmLimite_ == null) {
      ihmLimite_= new LidoIHM_Limite(projet_, ph_);
    }
    return ihmLimite_;
  }
  public LidoIHM_Apport APPORT() {
    if (ihmApport_ == null) {
      ihmApport_= new LidoIHM_Apport(projet_, ph_);
    }
    return ihmApport_;
  }
  public LidoIHM_Perte PERTE() {
    if (ihmPerte_ == null) {
      ihmPerte_= new LidoIHM_Perte(projet_, ph_);
    }
    return ihmPerte_;
  }
  public LidoIHM_Singularite SINGULARITE() {
    if (ihmSingularite_ == null) {
      ihmSingularite_= new LidoIHM_Singularite(projet_, ph_);
    }
    return ihmSingularite_;
  }
  public LidoIHM_Paramcalcul PARAMCALCUL() {
    if (ihmParamcalcul_ == null) {
      ihmParamcalcul_= new LidoIHM_Paramcalcul(projet_, ph_);
    }
    return ihmParamcalcul_;
  }
  public LidoIHM_Sectioncalcul SECTIONCALCUL() {
    if (ihmSectioncalcul_ == null) {
      ihmSectioncalcul_= new LidoIHM_Sectioncalcul(projet_, ph_);
    }
    return ihmSectioncalcul_;
  }
  public LidoIHM_Planimetrage PLANIMETRAGE() {
    if (ihmPlanimetrage_ == null) {
      ihmPlanimetrage_= new LidoIHM_Planimetrage(projet_, ph_);
    }
    return ihmPlanimetrage_;
  }
  public LidoIHM_Vartemp VARTEMP() {
    if (ihmVartemp_ == null) {
      ihmVartemp_= new LidoIHM_Vartemp(projet_, ph_);
    }
    return ihmVartemp_;
  }
  public LidoIHM_Resultatenlong RESULTATENLONG() {
    if (ihmResultatenlong_ == null) {
      ihmResultatenlong_= new LidoIHM_Resultatenlong(projet_, ph_);
    }
    return ihmResultatenlong_;
  }
  public LidoIHM_Resultatprofil RESULTATPROFIL() {
    if (ihmResultatprofil_ == null) {
      ihmResultatprofil_= new LidoIHM_Resultatprofil(projet_, ph_);
    }
    return ihmResultatprofil_;
  }
  public LidoIHM_Resultatlaisse RESULTATLAISSE() {
    if (ihmResultatlaisse_ == null) {
      ihmResultatlaisse_= new LidoIHM_Resultatlaisse(projet_, ph_);
    }
    return ihmResultatlaisse_;
  }
  public LidoIHM_Resultaterreurs RESULTATERREURS() {
    if (ihmResultaterreurs_ == null) {
      ihmResultaterreurs_= new LidoIHM_Resultaterreurs(projet_, ph_);
    }
    return ihmResultaterreurs_;
  }
  private void setProjetParametres(final FudaaProjet p) {
    if (ihmProfil_ != null) {
      ihmProfil_.setProjet(p);
    }
    if (ihmCalage_ != null) {
      ihmCalage_.setProjet(p);
    }
    if (ihmLaisse_ != null) {
      ihmLaisse_.setProjet(p);
    }
    if (ihmReseau_ != null) {
      ihmReseau_.setProjet(p);
    }
    if (ihmBief_ != null) {
      ihmBief_.setProjet(p);
    }
    if (ihmNoeud_ != null) {
      ihmNoeud_.setProjet(p);
    }
    if (ihmLimite_ != null) {
      ihmLimite_.setProjet(p);
    }
    if (ihmApport_ != null) {
      ihmApport_.setProjet(p);
    }
    if (ihmPerte_ != null) {
      ihmPerte_.setProjet(p);
    }
    if (ihmSingularite_ != null) {
      ihmSingularite_.setProjet(p);
    }
    if (ihmParamcalcul_ != null) {
      ihmParamcalcul_.setProjet(p);
    }
    if (ihmSectioncalcul_ != null) {
      ihmSectioncalcul_.setProjet(p);
    }
    if (ihmPlanimetrage_ != null) {
      ihmPlanimetrage_.setProjet(p);
    }
    if (ihmVartemp_ != null) {
      ihmVartemp_.setProjet(p);
    }
  }
  private void setProjetResultats(final FudaaProjet p) {
    if (ihmResultatenlong_ != null) {
      ihmResultatenlong_.setProjet(p);
    }
    if (ihmResultatprofil_ != null) {
      ihmResultatprofil_.setProjet(p);
    }
    if (ihmResultaterreurs_ != null) {
      ihmResultaterreurs_.setProjet(p);
    }
    if (ihmResultatlaisse_ != null) {
      ihmResultatlaisse_.setProjet(p);
    }
  }
}
