/*
 * @file         LidoIHM_Apport.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;

import org.fudaa.dodico.corba.lido.SParametresApportLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoLimiteChooser;
import org.fudaa.fudaa.lido.editor.LidoLimiteCourbeEditor;
import org.fudaa.fudaa.lido.editor.LidoLimiteMareeEditor;
import org.fudaa.fudaa.lido.editor.LidoLimitePermEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoLoiclmCellEditor;
import org.fudaa.fudaa.lido.tableau.LidoTableauApports;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Apport                                */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Apport extends LidoIHM_Base {
  SParametresCLM clm_;
  SParametresCAL cal_;
  LidoTableauApports appTable_;
  LidoIHM_Apport(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoIHM_Apport: Warning: passing null CLM to constructor");
    } else {
      if (appTable_ != null) {
        appTable_.setObjects(clm_.apport);
      }
    }
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Apport: Warning: passing null CAL to constructor");
    }
    reinit();
  }
  public void editer() {
    if (clm_ == null) {
      return;
    }
    if (cal_ == null) {
      return;
    }
    if (dl != null) {
      appTable_.setObjects(clm_.apport);
      dl.activate();
      return;
    }
    // loiclmEditor:
    final LidoLoiclmCellEditor loiclmEditor= new LidoLoiclmCellEditor();
    final PropertyChangeListener LOISUPPR_LISTENER=
      new ApportLoiSupprimeeListener(ph_, loiclmEditor, clm_);
    final PropertyChangeListener NUMLOI_LISTENER= new ApportNumLoiListener(ph_);
    //loiclmEditor.setLoisclm(ph_.APPORT().getLoisApports());
    loiclmEditor.setLoisclm(clm_.condLimites);
    appTable_= new LidoTableauApports(loiclmEditor);
    appTable_.addPropertyChangeListener(NUMLOI_LISTENER);
    ph_.APPORT().addPropertyChangeListener(appTable_);
    appTable_.setAutosort(false);
    appTable_.setObjects(clm_.apport);
    dl= new LidoDialogTableau(appTable_, "Apports/soutirages", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER
        | EbliPreferences.DIALOG.SUPPRIMER
        | EbliPreferences.DIALOG.EDITER);
    dl.setName("TABLEAUAPPORTS");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauApports table= (LidoTableauApports)dl.getTable();
        if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.APPORT().nouveauApport(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.APPORT().supprimeSelection(
            (SParametresApportLigneCLM[])table.getSelectedObjects());
        } else if ("EDITER".equals(e.getActionCommand())) {
          final SParametresApportLigneCLM select=
            (SParametresApportLigneCLM)table.getSelectedObject();
          if (select == null) {
            return;
          }
          final SParametresCondLimBlocCLM o= ph_.APPORT().getLimite(select);
          if (o == null) {
            // nouvelle loi limite
            final LidoLimiteChooser ch=
              new LidoLimiteChooser(
                dl.getDialog(),
                "P".equals(cal_.genCal.regime.trim().toUpperCase())
                  ? LidoLimiteChooser.PERMANENT_APP
                  : LidoLimiteChooser.NON_PERMANENT);
            ch.addActionListener(new ActionListener() {
              public void actionPerformed(final ActionEvent che) {
                final String cmd= che.getActionCommand();
                ((JDialog) ((JComponent)che.getSource()).getTopLevelAncestor())
                  .dispose();
                if ((cmd != null) && (cmd.startsWith("LIMI:"))) {
                  final SParametresCondLimBlocCLM o2=
                    ph_.LOICLM().nouveauLoiclm(clm_.nbCondLim);
                  select.numLoi= o2.numCondLim;
                  //loiclmEditor.setLoisclm(ph_.APPORT().getLoisApports());
                  loiclmEditor.setLoisclm(clm_.condLimites);
                  LidoCustomizer edit_= null;
                  if ("LIMI:TARAGE".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.TARAGE;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.TARAGE);
                    edit_=
                      new LidoLimiteCourbeEditor(
                        dl,
                        LidoResource.LIMITE.TARAGE,
                        ph_);
                  } else if ("LIMI:MAREE".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.MAREE;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.MAREE);
                    edit_= new LidoLimiteMareeEditor(dl, ph_);
                  } else if ("LIMI:LIMNI".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.LIMNI;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.LIMNI);
                    if ("P".equals(cal_.genCal.regime.trim().toUpperCase())) {
                      edit_=
                        new LidoLimitePermEditor(
                          dl,
                          LidoLimitePermEditor.LIMNI,
                          ph_);
                    } else {
                      edit_=
                        new LidoLimiteCourbeEditor(
                          dl,
                          LidoResource.LIMITE.LIMNI,
                          ph_);
                    }
                  } else if ("LIMI:HYDRO".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.HYDRO;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.HYDRO);
                    if ("P".equals(cal_.genCal.regime.trim().toUpperCase())) {
                      edit_=
                        new LidoLimitePermEditor(
                          dl,
                          LidoLimitePermEditor.HYDRO,
                          ph_);
                    } else {
                      edit_=
                        new LidoLimiteCourbeEditor(
                          dl,
                          LidoResource.LIMITE.HYDRO,
                          ph_);
                    }
                  }
                  edit_.addPropertyChangeListener(LOISUPPR_LISTENER);
                  edit_.setObject(o2);
                  listenToEditor(edit_);
                  edit_.show();
                  dl.getTable().repaint();
                }
              }
            });
            ch.show();
          } else {
            LidoCustomizer edit2= null;
            if ("P".equals(cal_.genCal.regime.trim().toUpperCase())) {
              if (!ph_.LOICLM().verifiePermanent(o)) {
                final int rep=
                  new BuDialogConfirmation(
                    (BuCommonInterface)LidoApplication.FRAME,
                    ((BuCommonInterface)LidoApplication.FRAME)
                      .getImplementation()
                      .getInformationsSoftware(),
                    "Cette loi n'est pas une loi de\n"
                      + "r�gime permanent!\n"
                      + "Voulez-vous la d�truire?\n")
                    .activate();
                if (rep == JOptionPane.YES_OPTION) {
                  ph_.LOICLM().supprimeLoiclm(o);
                  loiclmEditor.setLoisclm(clm_.condLimites);
                  edit2= null;
                } else {
                  edit2= new LidoLimiteCourbeEditor(dl, select.typLoi, ph_);
                }
              } else if (select.typLoi == LidoResource.LIMITE.MAREE) {
                edit2= new LidoLimiteMareeEditor(dl, ph_);
              } else {
                edit2= new LidoLimitePermEditor(dl, select.typLoi, ph_);
              }
            } else {
              edit2= new LidoLimiteCourbeEditor(dl, select.typLoi, ph_);
            }
            if (edit2 == null) {
              return;
            }
            edit2.addPropertyChangeListener(LOISUPPR_LISTENER);
            edit2.setObject(o);
            listenToEditor(edit2);
            edit2.show();
          }
        }
      }
    });
    dl.activate();
  }
}
class ApportLoiSupprimeeListener implements PropertyChangeListener {
  //private LidoParamsHelper ph_;
  private LidoLoiclmCellEditor ce_;
  private SParametresCLM clm_;
  public ApportLoiSupprimeeListener(
    final LidoParamsHelper ph,
    final LidoLoiclmCellEditor ce,
    final SParametresCLM clm) {
    /*ph_= ph;*/
    ce_= ce;
    clm_= clm;
  }
  public void propertyChange(final PropertyChangeEvent e) {
    if ("loisupprimee".equals(e.getPropertyName())) {
      ce_.setLoisclm(clm_.condLimites);
    }
  }
}
class ApportNumLoiListener implements PropertyChangeListener {
  private LidoParamsHelper ph_;
  public ApportNumLoiListener(final LidoParamsHelper ph) {
    ph_= ph;
  }
  public void propertyChange(final PropertyChangeEvent e) {
    if ("numloichanged".equals(e.getPropertyName())) {
      final SParametresApportLigneCLM apport=
        (SParametresApportLigneCLM)e.getNewValue();
      final SParametresCondLimBlocCLM loi= ph_.APPORT().getLimite(apport);
      if (loi == null) {
        apport.typLoi= 0;
      } else {
        apport.typLoi= loi.typLoi;
      }
    }
  }
}
