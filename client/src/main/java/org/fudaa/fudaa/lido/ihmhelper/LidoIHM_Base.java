/*
 * @file         LidoIHM_Base.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import com.memoire.bu.BuInformationsDocument;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Base                                  */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
abstract public class LidoIHM_Base implements FudaaParamListener {
  protected LidoDialogTableau dl;
  protected LidoParamsHelper ph_;
  protected Vector editors_;
  protected FudaaProjet p_;
  LidoIHM_Base(final FudaaProjet p, final LidoParamsHelper _ph) {
    p_= p;
    dl= null;
    ph_= _ph;
    editors_= new Vector();
    setProjet(p);
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
  }
  protected void installContextHelp(final LidoDialogTableau e) {
    if (e == null) {
      return;
    }
    String className= getClass().getName();
    final int index= className.lastIndexOf('.');
    if (index > 0) {
      className= className.substring(index + 1);
    }
    ((LidoApplication)LidoApplication.FRAME)
      .getImplementation()
      .installContextHelp(
      e,
      className + ".html");
  }
  public BuInformationsDocument getInformationsDocument() {
    return ph_.getInformationsDocument();
  }
  public void fermer() {
    if (dl != null) {
      dl.fermer();
      dl= null;
    }
    for (int i= 0; i < editors_.size(); i++) {
      final LidoCustomizer edit= (LidoCustomizer)editors_.get(i);
      if (edit.isShowing()) {
        edit.fermer();
      }
    }
    editors_.clear();
  }
  protected void addEditor(final LidoCustomizer e) {
    if (!editors_.contains(e)) {
      editors_.add(e);
    }
  }
  protected void closeEditors() {
    for (int i= 0; i < editors_.size(); i++) {
      ((LidoCustomizer)editors_.get(i)).fermer();
    }
  }
  abstract void setProjet(FudaaProjet p);
  // methode a appeler ABSOLUMENT dans les setProjet()
  protected void reinit() {
    boolean showing= false;
    closeEditors();
    if (dl != null) {
      showing= dl.isShowing();
    }
    if (showing) {
      editer();
    //fermer();
    }
  }
  abstract public void editer();
  protected void listenToEditor(final LidoCustomizer e) {
    e.addPropertyChangeListener(new EditorPropertyChangeListener());
    String className= e.getClass().getName();
    final int index= className.lastIndexOf('.');
    if (index > 0) {
      className= className.substring(index + 1);
    }
    ((LidoApplication)LidoApplication.FRAME)
      .getImplementation()
      .installContextHelp(
      e,
      className + ".html");
    addEditor(e);
  }
  public void paramStructCreated(final FudaaParamEvent e) {}
  public void paramStructDeleted(final FudaaParamEvent e) {}
  public void paramStructModified(final FudaaParamEvent e) {}
  class EditorPropertyChangeListener implements PropertyChangeListener {
    public void propertyChange(final PropertyChangeEvent p) {
      if ("object".equals(p.getPropertyName())) {
        System.err.println("object changed");
        if ((dl != null) && (dl.getTable() != null)) {
          dl.getTable().repaint();
        }
      }
    }
  }
}
