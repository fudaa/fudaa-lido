/*
 * @file         LidoIHM_Bief.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresLIG;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.dialog.BDialog;
import org.fudaa.ebli.dialog.IDialogInterface;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoBiefEditor;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauBiefs;
import org.fudaa.fudaa.lido.visu.LidoVisuLignedeau;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Bief                                  */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Bief extends LidoIHM_Base {
  SParametresRZO rzo_;
  SParametresPRO pro_;
  SParametresCAL cal_;
  SParametresLIG lig_;
  // HACK: temporaire (pour appeler le dialog de noeuds)
  //       en attendant les dialogs non modaux
  LidoTableauBiefs bieTable_;
  LidoIHM_Bief(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoIHM_Bief: Warning: passing null PRO to constructor");
    }
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoIHM_Bief: Warning: passing null RZO to constructor");
    } else {
      if (bieTable_ != null) {
        bieTable_.setObjects(rzo_.blocSitus.ligneSitu);
      }
    }
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Bief: Warning: passing null CAL to constructor");
    }
    lig_= (SParametresLIG)p.getParam(LidoResource.LIG);
    if (lig_ == null) {
      System.err.println(
        "LidoIHM_Bief: Warning: passing null LIG to constructor");
    }
    reinit();
  }
  public void editer() {
    if (rzo_ == null) {
      return;
    }
    if (dl != null) {
      bieTable_.setObjects(rzo_.blocSitus.ligneSitu);
      dl.activate();
      return;
    }
    bieTable_= new LidoTableauBiefs(ph_);
    bieTable_.setAutosort(true);
    bieTable_.setObjects(rzo_.blocSitus.ligneSitu);
    ph_.BIEF().addPropertyChangeListener(bieTable_);
    dl= new LidoDialogTableau(bieTable_, "Biefs", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER
        | EbliPreferences.DIALOG.SUPPRIMER
        | EbliPreferences.DIALOG.VOIR);
    dl.addAction("Scinder", "SCINDER");
    dl.setName("TABLEAUBIEFS");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauBiefs table= (LidoTableauBiefs)dl.getTable();
        if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.BIEF().nouveauBief(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.BIEF().supprimeSelection(
            (SParametresBiefSituLigneRZO[])table.getSelectedObjects());
        } else if ("EDITER".equals(e.getActionCommand())) {
          final Object[] select= table.getSelectedObjects();
          if ((select == null) || (select.length == 0)) {
            return;
          }
          for (int i= 0; i < select.length; i++) {
            final LidoBiefEditor edit_= new LidoBiefEditor(dl);
            edit_.setObject(select[i]);
            listenToEditor(edit_);
            edit_.show();
          }
        }
        if ("SCINDER".equals(e.getActionCommand())) {
          final Object[] select= table.getSelectedObjects();
          if ((select == null) || (select.length != 1)) {
            return;
          }
          final int profilchoisi= new ProfilChooser().activate() - 1;
          final SParametresBiefBlocPRO profil= ph_.PROFIL().getProfil(profilchoisi);
          if (profil == null) {
            return;
          }
          ph_.BIEF().scindeBief((SParametresBiefSituLigneRZO)select[0], profil);
        } else if ("VOIR".equals(e.getActionCommand())) {
          if (pro_ == null) {
            return;
          }
          if (cal_ == null) {
            return;
          }
          final SParametresBiefSituLigneRZO[] select=
            (SParametresBiefSituLigneRZO[])table.getSelectedObjects();
          if ((select == null) || (select.length == 0)) {
            return;
          }
          final LidoVisuLignedeau g= new LidoVisuLignedeau(dl, false, p_);
          final Vector pv[]= new Vector[select.length];
          for (int i= 0; i < select.length; i++) {
            //              int proDepart=select[i].numBief==0?0:(pro_.donneesBief[select[i].numBief-1].premierProfilAmont+1);
            //              int proFin=pro_.donneesBief[select[i].numBief].premierProfilAmont;
            //              SParametresBiefBlocPRO[] pros=new SParametresBiefBlocPRO[proFin-proDepart+1];
            //              for(int p=proDepart; p<=proFin; p++) {
            //                pros[p-proDepart]=pro_.profilsBief[p];
            //              }
            pv[i]= new Vector();
            for (int p= 0; p < pro_.nbProfils; p++) {
              if ((pro_.profilsBief[p].abscisse >= select[i].x1)
                && (pro_.profilsBief[p].abscisse <= select[i].x2)) {
                pv[i].add(pro_.profilsBief[p]);
              }
            }
          }
          for (int i= 0; i < select.length; i++) {
            final Object[] prosObj= pv[i].toArray();
            final SParametresBiefBlocPRO[] pros=
              new SParametresBiefBlocPRO[prosObj.length];
            for (int j= 0; j < prosObj.length; j++) {
              pros[j]= (SParametresBiefBlocPRO)prosObj[j];
            }
            g.addBief(select[i], pros);
          }
          g.commitData();
          if ((lig_ != null)
            && ("NP".equalsIgnoreCase(cal_.genCal.regime.trim()))) {
            g.setLigneDeauInitialeEnabled(true);
            g.setLigneDeau(lig_, null, true);
          }
          g.setTitle("Visualisation de bief");
          g.show();
        }
      }
    });
    dl.activate();
  }
}
class ProfilChooser extends BDialog implements ActionListener {
  BuGridLayout loBoutons_;
  int profilchoisi_;
  BuTextField tf;
  public ProfilChooser() {
    super((BuCommonInterface)LidoApplication.FRAME);
    setTitle("Choix du profil");
    setLocationRelativeTo(LidoApplication.FRAME);
    init();
  }
  public ProfilChooser(final IDialogInterface parent) {
    super((BuCommonInterface)LidoApplication.FRAME, parent);
    setTitle("Choix du profil");
    setLocationRelativeTo(parent.getComponent());
    init();
  }
  private void init() {
    setModal(true);
    profilchoisi_= -1;
    loBoutons_= new BuGridLayout(1, 5, 5, true, true);
    loBoutons_.setCfilled(false);
    final Container content= getContentPane();
    content.setLayout(loBoutons_);
    int n= 0;
    final BuLabel lb= new BuLabel("Choisissez un profil:");
    tf= BuTextField.createIntegerField();
    tf.setColumns(8);
    final BuPanel pn= new BuPanel();
    final FlowLayout lo= new FlowLayout(FlowLayout.CENTER);
    pn.setLayout(lo);
    final BuButton bt1= new BuButton();
    bt1.setText(BuResource.BU.getString("Valider"));
    bt1.setIcon(BuResource.BU.getIcon("valider"));
    bt1.setName("btBIEFVALIDER");
    bt1.setActionCommand("VALIDER");
    bt1.addActionListener(this);
    final BuButton bt2= new BuButton();
    bt2.setText(BuResource.BU.getString("Annuler"));
    bt2.setIcon(BuResource.BU.getIcon("annuler"));
    bt2.setName("btBIEFANNULER");
    bt2.setActionCommand("ANNULER");
    bt2.addActionListener(this);
    pn.add(bt2);
    pn.add(bt1);
    content.add(lb, n++);
    content.add(tf, n++);
    content.add(pn, n++);
    pack();
  }
  public int activate() {
    show();
    return profilchoisi_;
  }
  public void actionPerformed(final ActionEvent e) {
    final String cmd= e.getActionCommand();
    if ("VALIDER".equals(cmd)) {
      final Object n= tf.getValue();
      if (n == null) {
        profilchoisi_= -1;
      } else {
        profilchoisi_= ((Integer)n).intValue();
      }
    } else if ("ANNULER".equals(cmd)) {
      profilchoisi_= -1;
    }
    dispose();
  }
}
