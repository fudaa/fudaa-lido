/*
 * @file         LidoIHM_Calage.java
 * @creation     1999-11-16
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPRO;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoProfilEditor;
import org.fudaa.fudaa.lido.editor.LidoStricklerEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauProfils;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Calage                                */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Calage extends LidoIHM_Base {
  SParametresPRO pro_;
  private LidoTableauProfils proTable_;
  LidoIHM_Calage(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoIHM_Calage: Warning: passing null PRO to constructor");
    } else {
      if (proTable_ != null) {
        proTable_.setObjects(pro_.profilsBief);
      }
    }
    reinit();
  }
  public void editer() {
    if (pro_ == null) {
      return;
    }
    if (dl != null) {
      proTable_.setObjects(pro_.profilsBief);
      dl.activate();
      return;
    }
    proTable_= new LidoTableauProfils(ph_);
    ph_.PROFIL().addPropertyChangeListener(proTable_);
    proTable_.setAutosort(true);
    proTable_.setObjects(pro_.profilsBief);
    proTable_.setMode(LidoTableauProfils.CALAGE);
    dl= new LidoDialogTableau(proTable_, "Calage", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.addAction("Zones de stockage", "EDITER");
    dl.addAction("Strickler", "STRICKLER");
    dl.setName("TABLEAUCALAGE");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      protected void finalize() {
        System.err.println("dlActionListener GCed !");
      }
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauProfils table= (LidoTableauProfils)dl.getTable();
        if ("EDITER".equals(e.getActionCommand())) {
          final SParametresBiefBlocPRO select=
            (SParametresBiefBlocPRO)table.getSelectedObject();
          if (select == null) {
            return;
          }
          final LidoProfilEditor edit_=
            new LidoProfilEditor(
              dl,
              LidoProfilEditor.EDITER,
              LidoProfilEditor.CALAGE,
              p_);
          edit_.setObject(select);
          listenToEditor(edit_);
          edit_.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(final PropertyChangeEvent p) {
              if ("object".equals(p.getPropertyName())) {
                ((LidoTableauProfils)dl.getTable()).repaint();
              }
            }
          });
          edit_.addActionListener(new ActionListener() {
            int count= 0;
            public void actionPerformed(final ActionEvent e2) {
              final String cmd= e2.getActionCommand();
              if (cmd.startsWith("RECULER")) {
                if ((select.indice + count) == 0) {
                  return;
                }
                if (cmd.endsWith("VITE")) {
                  final int avt=
                    LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
                  count= Math.max(count - avt, -select.indice);
                } else {
                  count--;
                }
                edit_.setObject(pro_.profilsBief[select.indice + count]);
              } else if (cmd.startsWith("AVANCER")) {
                if ((select.indice + count) == (pro_.nbProfils - 1)) {
                  return;
                }
                if (cmd.endsWith("VITE")) {
                  final int avt=
                    LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
                  count=
                    Math.min(count + avt, -select.indice + pro_.nbProfils - 1);
                } else {
                  count++;
                }
                edit_.setObject(pro_.profilsBief[select.indice + count]);
              }
            }
          });
          edit_.show();
        } else if ("STRICKLER".equals(e.getActionCommand())) {
          final SParametresBiefBlocPRO[] select=
            (SParametresBiefBlocPRO[])table.getSelectedObjects();
          if (select == null) {
            return;
          }
          final LidoStricklerEditor edit= new LidoStricklerEditor(dl);
          edit.setObject(select);
          listenToEditor(edit);
          edit.show();
        }
      }
    });
    dl.activate();
  }
}
