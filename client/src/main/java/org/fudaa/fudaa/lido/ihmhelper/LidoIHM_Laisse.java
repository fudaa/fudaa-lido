/*
 * @file         LidoIHM_Laisse.java
 * @creation     1999-12-24
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SParametresLaisseLigneEXT;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauLaisses;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Laisse                                */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Laisse extends LidoIHM_Base {
  private SParametresEXT ext_;
  private LidoTableauLaisses laiTable_;
  LidoIHM_Laisse(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    ext_= (SParametresEXT)p.getParam(LidoResource.EXT);
    if (ext_ == null) {
      System.err.println(
        "LidoIHM_Laisse: Warning: passing null EXT to constructor");
    } else {
      if (laiTable_ != null) {
        laiTable_.setObjects(ext_.laisses);
      }
    }
    reinit();
  }
  public void editer() {
    if (ext_ == null) {
      return;
    }
    if (dl != null) {
      laiTable_.setObjects(ext_.laisses);
      dl.activate();
      return;
    }
    laiTable_= new LidoTableauLaisses();
    laiTable_.setAutosort(true);
    laiTable_.setObjects(ext_.laisses);
    ph_.LAISSE().addPropertyChangeListener(laiTable_);
    dl= new LidoDialogTableau(laiTable_, "Laisses de crues", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER | EbliPreferences.DIALOG.SUPPRIMER);
    dl.setName("TABLEAULAISSES");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauLaisses table= (LidoTableauLaisses)dl.getTable();
        if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.LAISSE().nouveauLaisse(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.LAISSE().supprimeSelection(
            (SParametresLaisseLigneEXT[])table.getSelectedObjects());
        }
      }
    });
    dl.activate();
  }
}
