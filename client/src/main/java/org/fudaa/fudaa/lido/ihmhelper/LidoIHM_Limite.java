/*
 * @file         LidoIHM_Limite.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresBiefLimLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoLimiteChooser;
import org.fudaa.fudaa.lido.editor.LidoLimiteCourbeEditor;
import org.fudaa.fudaa.lido.editor.LidoLimiteMareeEditor;
import org.fudaa.fudaa.lido.editor.LidoLimitePermEditor;
import org.fudaa.fudaa.lido.editor.LidoLoiclmLibraryEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoLoiclmCellEditor;
import org.fudaa.fudaa.lido.tableau.LidoTableauLimites;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Limite                                */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Limite extends LidoIHM_Base {
  SParametresRZO rzo_;
  SParametresCLM clm_;
  SParametresCAL cal_;
  LidoTableauLimites limTable_;
  JComboBox extrLibreEditor;
  LidoLoiclmCellEditor loiclmEditor;
  LidoIHM_Limite(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoIHM_Limite: Warning: passing null RZO to constructor");
    } else {
      if (limTable_ != null) {
        limTable_.setObjects(rzo_.blocLims.ligneLim);
      }
    }
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoIHM_Limite: Warning: passing null CLM to constructor");
    }
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Limite: Warning: passing null CAL to constructor");
    }
    reinit();
  }
  public void editer() {
    if (rzo_ == null) {
      return;
    }
    if (clm_ == null) {
      return;
    }
    if (cal_ == null) {
      return;
    }
    final boolean permanent=
      "P".equals(cal_.genCal.regime.trim().toUpperCase());
    if (dl != null) {
      if (!permanent) {
        if (!dl.containsAction("GEST")) {
          dl.addAction("Lois", "GEST");
        }
      }
      limTable_.setObjects(rzo_.blocLims.ligneLim);
      extrLibreEditor.removeAllItems();
      final Integer[] extrLibres= ph_.BIEF().extremitesLibres();
      for (int i= 0; i < extrLibres.length; i++) {
        extrLibreEditor.addItem(extrLibres[i]);
      }
      dl.activate();
      return;
    }
    // loiclmEditor:
    loiclmEditor= new LidoLoiclmCellEditor();
    final PropertyChangeListener NUMLOI_LISTENER= new LimiteNumLoiListener(ph_);
    loiclmEditor.setLoisclm(clm_.condLimites);
    // extrlibreEditor
    extrLibreEditor= new JComboBox();
    final Integer[] extrLibres= ph_.BIEF().extremitesLibres();
    for (int i= 0; i < extrLibres.length; i++) {
      extrLibreEditor.addItem(extrLibres[i]);
    }
    limTable_=
      new LidoTableauLimites(
        loiclmEditor,
        new DefaultCellEditor(extrLibreEditor));
    limTable_.addPropertyChangeListener(NUMLOI_LISTENER);
    limTable_.setAutosort(false);
    limTable_.setObjects(rzo_.blocLims.ligneLim);
    ph_.LIMITE().addPropertyChangeListener(limTable_);
    ph_.BIEF().addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(final PropertyChangeEvent e) {
        if ("biefs".equals(e.getPropertyName())) {
          extrLibreEditor.removeAllItems();
          final Integer[] lib= ph_.BIEF().extremitesLibres();
          for (int i= 0; i < lib.length; i++) {
            extrLibreEditor.addItem(lib[i]);
          }
        }
      }
    });
    dl= new LidoDialogTableau(limTable_, "Conditions limites", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER
        | EbliPreferences.DIALOG.SUPPRIMER
        | EbliPreferences.DIALOG.EDITER);
    if (!permanent) {
      dl.addAction("Lois", "GEST");
    }
    dl.setName("TABLEAULIMITES");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauLimites table= (LidoTableauLimites)dl.getTable();
        if ("GEST".equals(e.getActionCommand())) {
          final SParametresBiefLimLigneRZO[] select=
            (SParametresBiefLimLigneRZO[])table.getSelectedObjects();
          final LidoLoiclmLibraryEditor clmlib=
            new LidoLoiclmLibraryEditor(dl, clm_, ph_);
          listenToEditor(clmlib);
          final int res= clmlib.activate();
          if (res == LidoLoiclmLibraryEditor.VALIDATION) {
            final SParametresCondLimBlocCLM[] lois= clmlib.getSelectedLois();
            if (lois.length == 1) {
              for (int i= 0; i < select.length; i++) {
                select[i].numLoi= lois[0].numCondLim;
                select[i].typLoi= lois[0].typLoi;
                FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
                  new FudaaParamEvent(
                    this,
                    0,
                    LidoResource.RZO,
                    select[i],
                    "limite " + (select[i].numLoi)));
              }
              table.repaint();
            } else if (lois.length > 1) {
              new BuDialogError(
                (BuCommonInterface)LidoApplication.FRAME,
                ((BuCommonInterface)LidoApplication.FRAME)
                  .getImplementation()
                  .getInformationsSoftware(),
                "Vous devez choisir une seule loi !")
                .activate();
            }
          }
        } else if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.LIMITE().nouveauLimite(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.LIMITE().supprimeSelection(
            (SParametresBiefLimLigneRZO[])table.getSelectedObjects());
        } else if ("EDITER".equals(e.getActionCommand())) {
          final SParametresBiefLimLigneRZO select=
            (SParametresBiefLimLigneRZO)table.getSelectedObject();
          if (select == null) {
            return;
          }
          final SParametresCondLimBlocCLM o= ph_.LIMITE().getLimite(select);
          if (o == null) {
            // nouvelle loi limite
            final LidoLimiteChooser ch=
              new LidoLimiteChooser(
                dl.getDialog(),
                permanent
                  ? LidoLimiteChooser.PERMANENT_LIM
                  : LidoLimiteChooser.NON_PERMANENT);
            ch.addActionListener(new ActionListener() {
              public void actionPerformed(final ActionEvent che) {
                final String cmd= che.getActionCommand();
                ((JDialog) ((JComponent)che.getSource()).getTopLevelAncestor())
                  .dispose();
                if ((cmd != null) && (cmd.startsWith("LIMI:"))) {
                  final SParametresCondLimBlocCLM o2=
                    ph_.LOICLM().nouveauLoiclm(clm_.nbCondLim);
                  select.numLoi= o2.numCondLim;
                  loiclmEditor.setLoisclm(clm_.condLimites);
                  LidoCustomizer edit_= null;
                  if ("LIMI:TARAGE".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.TARAGE;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.TARAGE);
                    edit_=
                      new LidoLimiteCourbeEditor(
                        dl,
                        LidoResource.LIMITE.TARAGE,
                        ph_);
                  } else if ("LIMI:MAREE".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.MAREE;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.MAREE);
                    edit_= new LidoLimiteMareeEditor(dl, ph_);
                  } else if ("LIMI:LIMNI".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.LIMNI;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.LIMNI);
                    if (permanent) {
                      edit_=
                        new LidoLimitePermEditor(
                          dl,
                          LidoResource.LIMITE.LIMNI,
                          ph_);
                    } else {
                      edit_=
                        new LidoLimiteCourbeEditor(
                          dl,
                          LidoResource.LIMITE.LIMNI,
                          ph_);
                    }
                  } else if ("LIMI:HYDRO".equals(cmd)) {
                    select.typLoi= LidoResource.LIMITE.HYDRO;
                    ph_.LOICLM().changeTypeLoi(o2, LidoResource.LIMITE.HYDRO);
                    if (permanent) {
                      edit_=
                        new LidoLimitePermEditor(
                          dl,
                          LidoResource.LIMITE.HYDRO,
                          ph_);
                    } else {
                      edit_=
                        new LidoLimiteCourbeEditor(
                          dl,
                          LidoResource.LIMITE.HYDRO,
                          ph_);
                    }
                  }
                  edit_.setObject(o2);
                  listenToEditor(edit_);
                  edit_.addPropertyChangeListener(
                    new VerifLimiteListener(ph_, select, permanent));
                  edit_.show();
                  dl.getTable().repaint();
                }
              }
            });
            ch.show();
          } else {
            LidoCustomizer edit2= null;
            if (permanent) {
              if (!ph_.LOICLM().verifiePermanent(o)) {
                final int rep=
                  new BuDialogConfirmation(
                    (BuCommonInterface)LidoApplication.FRAME,
                    ((BuCommonInterface)LidoApplication.FRAME)
                      .getImplementation()
                      .getInformationsSoftware(),
                    "Cette loi n'est pas une loi de\n"
                      + "r�gime permanent!\n"
                      + "Voulez-vous la d�truire?\n")
                    .activate();
                if (rep == JOptionPane.YES_OPTION) {
                  ph_.LOICLM().supprimeLoiclm(o);
                  loiclmEditor.setLoisclm(clm_.condLimites);
                  edit2= null;
                } else {
                  edit2= new LidoLimiteCourbeEditor(dl, select.typLoi, ph_);
                }
              } else {
                edit2= new LidoLimitePermEditor(dl, select.typLoi, ph_);
              }
            } else if (select.typLoi == LidoResource.LIMITE.MAREE) {
              edit2= new LidoLimiteMareeEditor(dl, ph_);
            } else {
              edit2= new LidoLimiteCourbeEditor(dl, select.typLoi, ph_);
            }
            if (edit2 == null) {
              return;
            }
            edit2.setObject(o);
            listenToEditor(edit2);
            edit2.addPropertyChangeListener(
              new VerifLimiteListener(ph_, select, permanent));
            edit2.show();
          }
        }
      }
    });
    dl.activate();
  }
  public void paramStructCreated(final FudaaParamEvent e) {
    if (e.getStruct() instanceof SParametresCondLimBlocCLM) {
      System.err.println("maj celleditor");
      loiclmEditor.setLoisclm(clm_.condLimites);
    }
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    if (e.getStruct() instanceof SParametresCondLimBlocCLM) {
      System.err.println("maj celleditor");
      loiclmEditor.setLoisclm(clm_.condLimites);
    }
  }
}
class VerifLimiteListener implements PropertyChangeListener {
  private LidoParamsHelper ph_;
  private SParametresBiefLimLigneRZO limrzo_;
  private boolean permanent_;
  public VerifLimiteListener(
    final LidoParamsHelper ph,
    final SParametresBiefLimLigneRZO rzo,
    final boolean perm) {
    ph_= ph;
    limrzo_= rzo;
    permanent_= perm;
  }
  public void propertyChange(final PropertyChangeEvent e) {
    if (!"valider".equals(e.getPropertyName())) {
      return;
    }
    final SParametresCondLimBlocCLM loiclm=
      (SParametresCondLimBlocCLM)e.getNewValue();
    final int extremite= limrzo_.numExtBief;
    final double abs= ph_.BIEF().getAbscisseExtremite(extremite);
    final SParametresBiefBlocPRO profil= ph_.PROFIL().getProfilByAbscisse(abs);
    final double cote= ph_.PROFIL().getFond(profil);
    boolean ok= true;
    switch (loiclm.typLoi) {
      case LidoResource.LIMITE.MAREE :
      case LidoResource.LIMITE.LIMNI :
        {
          ok= ph_.LOICLM().verifieCote(loiclm, cote);
          break;
        }
      case LidoResource.LIMITE.TARAGE :
        {
          if (!permanent_) {
            ok= ph_.LOICLM().verifieCote(loiclm, cote);
          }
          break;
        }
    }
    if (!ok) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME)
          .getImplementation()
          .getInformationsSoftware(),
        "La cote de la condition limite n'est pas\n"
          + "sup�rieure � la cote du profil o� elle\n"
          + "s'applique ! Vous devez corriger.")
        .activate();
    }
    if (!permanent_) {
      ok= ph_.LOICLM().verifieTempsNP(loiclm);
      if (!ok) {
        new BuDialogError(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME)
            .getImplementation()
            .getInformationsSoftware(),
          "Les temps sont incorrects\n"
            + "Ils doivent etre distincts\n"
            + "et croissants.")
          .activate();
      }
    }
    ((LidoCustomizer)e.getSource()).removePropertyChangeListener(this);
  }
}
class LimiteNumLoiListener implements PropertyChangeListener {
  private LidoParamsHelper ph_;
  public LimiteNumLoiListener(final LidoParamsHelper ph) {
    ph_= ph;
  }
  public void propertyChange(final PropertyChangeEvent e) {
    if ("numloichanged".equals(e.getPropertyName())) {
      final SParametresBiefLimLigneRZO limite=
        (SParametresBiefLimLigneRZO)e.getNewValue();
      final SParametresCondLimBlocCLM loi= ph_.LIMITE().getLimite(limite);
      if (loi == null) {
        limite.typLoi= 0;
      } else {
        limite.typLoi= loi.typLoi;
      }
    }
  }
}
