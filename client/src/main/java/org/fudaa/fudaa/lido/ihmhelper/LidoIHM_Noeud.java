/*
 * @file         LidoIHM_Noeud.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fudaa.dodico.corba.lido.SParametresBiefNoeudLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoNoeudEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauNoeuds;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Noeud                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Noeud extends LidoIHM_Base {
  SParametresRZO rzo_;
  LidoTableauNoeuds noeTable_;
  LidoIHM_Noeud(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoIHM_Noeud: Warning: passing null RZO to constructor");
    } else {
      if (noeTable_ != null) {
        noeTable_.setObjects(rzo_.blocNoeuds.ligneNoeud);
      }
    }
    reinit();
  }
  public void editer() {
    if (rzo_ == null) {
      return;
    }
    if (dl != null) {
      noeTable_.setObjects(rzo_.blocNoeuds.ligneNoeud);
      dl.activate();
      return;
    }
    noeTable_= new LidoTableauNoeuds();
    noeTable_.setAutosort(false);
    noeTable_.setObjects(rzo_.blocNoeuds.ligneNoeud);
    ph_.NOEUD().addPropertyChangeListener(noeTable_);
    dl= new LidoDialogTableau(noeTable_, "Noeuds", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER | EbliPreferences.DIALOG.SUPPRIMER);
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauNoeuds table= (LidoTableauNoeuds)dl.getTable();
        if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.NOEUD().nouveauNoeud(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.NOEUD().supprimeSelection(
            (SParametresBiefNoeudLigneRZO[])table.getSelectedObjects());
        } else if ("EDITER".equals(e.getActionCommand())) {
          final Object[] select= table.getSelectedObjects();
          if (select == null) {
            return;
          }
          for (int i= 0; i < select.length; i++) {
            final LidoNoeudEditor edit_= new LidoNoeudEditor(dl);
            edit_.setObject(select[i]);
            listenToEditor(edit_);
            edit_.show();
          }
        }
      }
    });
    dl.activate();
  }
}
