/*
 * @file         LidoIHM_Paramcalcul.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;

import org.fudaa.dodico.corba.lido.SParametresCAL;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoPermCalEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Paramcalcul                           */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Paramcalcul extends LidoIHM_Base {
  SParametresCAL cal_;
  LidoPermCalEditor edit;
  LidoIHM_Paramcalcul(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Paramcalcul: Warning: passing null CAL to constructor");
    } else {
      if (edit != null) {
        edit.setObject(cal_.genCal);
      }
    }
    reinit();
  }
  public void editer() {
    if (cal_ == null) {
      return;
    }
    if (edit == null) {
      edit= new LidoPermCalEditor();
      edit.setActionPanel(EbliPreferences.DIALOG.RESET);
      edit.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent e) {
          final String cmd= e.getActionCommand();
          if ("RESET".equals(cmd)) {
            final int res=
              new BuDialogConfirmation(
                (BuCommonInterface)LidoApplication.FRAME,
                ((BuCommonInterface)LidoApplication.FRAME)
                  .getInformationsSoftware(),
                "Voulez-vous recalculer automatiquement\n"
                  + "les abscisses de calcul?")
                .activate();
            if (res == JOptionPane.YES_OPTION) {
              ph_.CALCUL().recalculeXOrigiXFin();
            }
          }
        }
      });
      edit.setObject(cal_.genCal);
      listenToEditor(edit);
      final LidoAssistant ass= LidoResource.ASSISTANT;
      if (ass != null) {
        ass.addEmitters(edit);
      }
    }
    final double xori= ph_.CALCUL().getXOrigine();
    final double xfin= ph_.CALCUL().getXFin();
    if (((cal_.genCal.biefXOrigi == 0.) && (cal_.genCal.biefXFin == 0.))
      || ((cal_.genCal.biefXOrigi < xori) || (cal_.genCal.biefXFin > xfin))) {
      ph_.CALCUL().recalculeXOrigiXFin();
    }
    edit.show();
    //    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(this, FudaaParamEvent.CAL_GENERAL, LidoResource.CAL));
  }
}
