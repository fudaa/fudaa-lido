/*
 * @file         LidoIHM_Perte.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresPerteLigneCLM;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauPertes;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Perte                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Perte extends LidoIHM_Base {
  SParametresCLM clm_;
  LidoTableauPertes perTable_;
  LidoIHM_Perte(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoIHM_Perte: Warning: passing null CLM to constructor");
    } else {
      if (perTable_ != null) {
        perTable_.setObjects(clm_.perte);
      }
    }
    reinit();
  }
  public void editer() {
    if (clm_ == null) {
      return;
    }
    if (dl != null) {
      perTable_.setObjects(clm_.perte);
      dl.activate();
      return;
    }
    perTable_= new LidoTableauPertes();
    perTable_.setAutosort(false);
    perTable_.setObjects(clm_.perte);
    ph_.PERTE().addPropertyChangeListener(perTable_);
    dl= new LidoDialogTableau(perTable_, "Pertes", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER | EbliPreferences.DIALOG.SUPPRIMER);
    dl.setName("TABLEAUPERTES");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauPertes table= (LidoTableauPertes)dl.getTable();
        if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.PERTE().nouveauPerte(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.PERTE().supprimeSelection(
            (SParametresPerteLigneCLM[])table.getSelectedObjects());
        }
      }
    });
    dl.activate();
  }
}
