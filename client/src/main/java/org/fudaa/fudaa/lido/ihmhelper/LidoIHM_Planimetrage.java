/*
 * @file         LidoIHM_Planimetrage.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import org.fudaa.dodico.corba.lido.SParametresCAL;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoPermPlanimetrageEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Planimetrage                          */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoIHM_Planimetrage extends LidoIHM_Base {
  SParametresCAL cal_;
  LidoCustomizer edit;
  LidoIHM_Planimetrage(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Planimetrage: Warning: passing null CAL to constructor");
    } else {
      if (edit != null) {
        edit.setObject(cal_.planimetrage);
      }
    }
    reinit();
  }
  public void editer() {
    if (cal_ == null) {
      return;
    }
    if (edit == null) {
      edit= new LidoPermPlanimetrageEditor();
      ((LidoPermPlanimetrageEditor)edit).setParamsHelper(ph_);
      edit.setObject(cal_.planimetrage);
      listenToEditor(edit);
      final LidoAssistant ass= LidoResource.ASSISTANT;
      if (ass != null) {
        ass.addEmitters(edit);
      }
    }
    edit.show();
  }
}
