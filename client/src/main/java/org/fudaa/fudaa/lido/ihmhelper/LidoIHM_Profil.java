/*
 * @file         LidoIHM_Profil.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;

import com.memoire.bu.*;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.lido.DParametresLido;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.*;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoProfilEditor;
import org.fudaa.fudaa.lido.editor.profil.LidoProfilSimpleEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauProfils;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Profil                                */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Profil extends LidoIHM_Base {
  SParametresPRO pro_;
  LidoTableauProfils proTable_;
  LidoIHM_Profil(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoIHM_Profil: Warning: passing null PRO to constructor");
    } else {
      if (proTable_ != null) {
        proTable_.setObjects(pro_.profilsBief);
      }
    }
    reinit();
  }
  public void importer() {
    if (pro_ == null) {
      return;
    }
    final MyImportChooser c= new MyImportChooser(LidoApplication.FRAME);
    c.show();
    final int choice= c.getChoice();
    if (choice == 0) {
      return;
    }
    /*if( choice==MyImportChooser.PRO_RIVICAD ) {
      new BuDialogError((BuCommonInterface)LidoApplication.FRAME,
         ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "ERREUR: l'importation de profils\n"+
        "'Rivicad' n'est pas disponible\n"+
        "pour l'instant. D�sol�..."
        ).activate();
      return;
    }*/
    final File file=
      LidoImport.chooseFile(
        choice == MyImportChooser.TXT
          ? "txt"
          : choice == MyImportChooser.PRO_LIDO
          ? "pro"
          : choice == MyImportChooser.PRO_RIVICAD
          ? "lid"
          : null);
    if (file == null) {
      return;
    }
    SParametresBiefBlocPRO[] imports= null;
    if (choice == MyImportChooser.TXT) {
      imports= LidoImport.importProfilsTXT(file);
    } else if (choice == MyImportChooser.PRO_LIDO) {
      imports= LidoImport.importProfilsPRO_LIDO(file);
    } else if (choice == MyImportChooser.PRO_RIVICAD) {
      imports= LidoImport.importProfilsPRO_RIVICAD(file);
    }
    if ((imports == null)) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "L'importation des profils a �chou�.\n")
        .activate();
      return;
    }
    if ((imports.length == 0)) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "Aucun profil n'est disponible dans cette import!\n")
        .activate();
      return;
    }
    if (c.isMiseAZero()) {
      ph_.PROFIL().miseAZero(imports);
    }
    if (c.isNouveauBief()) {
      final double offset= c.getOffset();
      final SParametresBiefSituLigneRZO bief= ph_.BIEF().nouveauBief(imports, offset);
      if (bief == null) {
        new BuDialogMessage(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
          "La cr�ation du bief a �chou�.\n"
            + "Les profils n'ont pas �t� import�s.\n")
          .activate();
        return;
      }
      new BuDialogMessage(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "Le bief "
          + (bief.numBief + 1)
          + " a �t� cr�e avec\n"
          + "les profils import�s.\n")
        .activate();
    }
    ph_.PROFIL().importProfils(imports);
  }
  public void editer() {
    if (pro_ == null) {
      System.err.println("abort");
      return;
    }
    if (dl != null) {
      proTable_.setObjects(pro_.profilsBief);
      dl.activate();
      return;
    }
    proTable_= new LidoTableauProfils(ph_);
    ph_.PROFIL().addPropertyChangeListener(proTable_);
    proTable_.setAutosort(true);
    proTable_.setObjects(pro_.profilsBief);
    proTable_.setMode(LidoTableauProfils.PROFILS);
    dl= new LidoDialogTableau(proTable_, "Profils", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.CREER
        | EbliPreferences.DIALOG.SUPPRIMER
        | EbliPreferences.DIALOG.EDITER
        | EbliPreferences.DIALOG.IMPORTER);
    dl.addAction("Dupliquer", "DUPLIQUER");
    dl.setName("TABLEAUPROFILS");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      protected void finalize() {
        System.err.println("dlActionListener GCed !");
      }
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauProfils table= (LidoTableauProfils)dl.getTable();
        if ("IMPORTER".equals(e.getActionCommand())) {
          importer();
          proTable_.setObjects(pro_.profilsBief);
        } else if ("CREER".equals(e.getActionCommand())) {
          final int nouvPos= table.getPositionNouveau();
          ph_.PROFIL().nouveauProfil(nouvPos);
          table.editeCellule(nouvPos, 0);
        } else if ("DUPLIQUER".equals(e.getActionCommand())) {
          final SParametresBiefBlocPRO[] select=
            (SParametresBiefBlocPRO[])table.getSelectedObjects();
          if (select.length != 1) {
            return;
          }
          final int nouvPos= table.getSelectedIndex() + 1;
          ph_.PROFIL().dupliqueProfil(select[0], nouvPos);
          table.editeCellule(nouvPos, 0);
          new BuDialogMessage(
            (BuCommonInterface)LidoApplication.FRAME,
            ((BuCommonInterface)LidoApplication.FRAME)
              .getInformationsSoftware(),
            "Vous avez dupliqu� un profil:\n"
              + "n'oubliez pas de v�rifier les sections\n"
              + "de calcul.\n")
            .activate();
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.PROFIL().supprimeSelection(
            (SParametresBiefBlocPRO[])table.getSelectedObjects());
          final SParametresPerteLigneCLM[] pertesHorsBief=
            ph_.PERTE().getPertesHorsBief();
          if ((pertesHorsBief != null) && (pertesHorsBief.length > 0)) {
            String pertesStr= "";
            for (int i= 0; i < pertesHorsBief.length; i++) {
              pertesStr += pertesHorsBief[i].xPerte + "\n";
            }
            new BuDialogMessage(
              (BuCommonInterface)LidoApplication.FRAME,
              ((BuCommonInterface)LidoApplication.FRAME)
                .getInformationsSoftware(),
              "Les pertes aux abscisses:\n" + pertesStr + "sont hors-bief.")
              .activate();
          }
          final SParametresApportLigneCLM[] apportsHorsBief=
            ph_.APPORT().getApportsHorsBief();
          if ((apportsHorsBief != null) && (apportsHorsBief.length > 0)) {
            String apportsStr= "";
            for (int i= 0; i < apportsHorsBief.length; i++) {
              apportsStr += apportsHorsBief[i].xApport + "\n";
            }
            new BuDialogMessage(
              (BuCommonInterface)LidoApplication.FRAME,
              ((BuCommonInterface)LidoApplication.FRAME)
                .getInformationsSoftware(),
              "Les apports aux abscisses:\n" + apportsStr + "sont hors-bief.")
              .activate();
          }
          final SParametresBiefSingLigneRZO[] singsHorsBief=
            ph_.SINGULARITE().getSingularitesHorsBief();
          if ((singsHorsBief != null) && (singsHorsBief.length > 0)) {
            String singsStr= "";
            for (int i= 0; i < singsHorsBief.length; i++) {
              singsStr += singsHorsBief[i].xSing + "\n";
            }
            new BuDialogMessage(
              (BuCommonInterface)LidoApplication.FRAME,
              ((BuCommonInterface)LidoApplication.FRAME)
                .getInformationsSoftware(),
              "Les singularit�s aux abscisses:\n"
                + singsStr
                + "sont hors-bief.")
              .activate();
          }
        } else if ("EDITER".equals(e.getActionCommand())) {
          final SParametresBiefBlocPRO select=
            (SParametresBiefBlocPRO)table.getSelectedObject();
          if (select == null) {
            return;
          }
          if (select.nbPoints == 0) {
            final int res=
              new BuDialogConfirmation(
                (BuCommonInterface)LidoApplication.FRAME,
                ((BuCommonInterface)LidoApplication.FRAME)
                  .getInformationsSoftware(),
                "Ce profil ne contient aucun point.\n"
                  + "Cr�ation en mode simple ?\n")
                .activate();
            if (res == JOptionPane.OK_OPTION) {
              final LidoProfilSimpleEditor edit_=
                new LidoProfilSimpleEditor(dl);
              edit_.setObject(select);
              edit_.addPropertyChangeListener(new PropertyChangeListener() {
                public void propertyChange(final PropertyChangeEvent pce) {
                  if ("fermer".equals(pce.getPropertyName())) {
                    normaliseProfil(select);
                  }
                }
              });
              listenToEditor(edit_);
              edit_.show();
              return;
            }
          }
          final LidoProfilEditor edit_=
            new LidoProfilEditor(
              dl,
              LidoProfilEditor.EDITER,
              LidoProfilEditor.PROFILS,
              p_);
          edit_.setObject(select);
          edit_.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(final PropertyChangeEvent pce) {
              if ("fermer".equals(pce.getPropertyName())) {
                normaliseProfil(select);
              }
            }
          });
          listenToEditor(edit_);
          edit_.addActionListener(new ActionListener() {
            int count= 0;
            public void actionPerformed(final ActionEvent e2) {
              final String cmd= e2.getActionCommand();
              if (cmd.startsWith("RECULER")) {
                if ((select.indice + count) == 0) {
                  return;
                }
                if (cmd.endsWith("VITE")) {
                  final int avt=
                    LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
                  count= Math.max(count - avt, -select.indice);
                } else {
                  count--;
                }
                edit_.setObject(pro_.profilsBief[select.indice + count]);
              } else if (cmd.startsWith("AVANCER")) {
                if ((select.indice + count) == (pro_.nbProfils - 1)) {
                  return;
                }
                if (cmd.endsWith("VITE")) {
                  final int avt=
                    LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
                  count=
                    Math.min(count + avt, -select.indice + pro_.nbProfils - 1);
                } else {
                  count++;
                }
                edit_.setObject(pro_.profilsBief[select.indice + count]);
              }
            }
          });
          edit_.show();
        }
      }
    });
    dl.activate();
  }
  void normaliseProfil(final SParametresBiefBlocPRO profilBief) {
    if (DParametresLido
      .normaliseProfils(new SParametresBiefBlocPRO[] { profilBief })) {
      new BuDialogMessage(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
        "Avertissement: le profil "
          + profilBief.indice
          + " a �t� modifi�:\n"
          + "un point y a �t� automatiquement\n"
          + "ajout� pour �galiser les hauteurs\n"
          + "des limites du lit majeur.")
        .activate();
    }
  }
}
class MyImportChooser extends JDialog implements ActionListener {
  public final static int TXT= 0x01;
  public final static int PRO_LIDO= 0x02;
  public final static int PRO_RIVICAD= 0x04;
  private int choice_;
  private JRadioButton cbTxt_, cbProLido_, cbProRivicad_;
  private JCheckBox cbBief_, cbMiseAZero_, cbOffset_;
  private BuTextField tfOffset_;
  public MyImportChooser(final Frame parent) {
    super(parent, true);
    choice_= 0;
    getContentPane().setLayout(new BuVerticalLayout());
    final BuPanel pn= new BuPanel();
    pn.setBorder(
      new CompoundBorder(
        new EmptyBorder(new Insets(5, 5, 5, 5)),
        new EtchedBorder()));
    pn.setLayout(new BuVerticalLayout());
    cbTxt_= new JRadioButton("Format texte (.TXT)");
    cbTxt_.addActionListener(this);
    pn.add(cbTxt_, 0);
    cbProLido_= new JRadioButton("Format Lido 2.0 (.PRO)");
    cbProLido_.addActionListener(this);
    pn.add(cbProLido_, 1);
    cbProRivicad_= new JRadioButton("Format Rivicad (.LID)");
    cbProRivicad_.addActionListener(this);
    pn.add(cbProRivicad_, 2);
    getContentPane().add(pn, 0);
    cbBief_= new JCheckBox("Cr�er un nouveau bief");
    cbBief_.addActionListener(this);
    cbBief_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    cbBief_.setSelected(true);
    getContentPane().add(cbBief_, 1);
    cbMiseAZero_= new JCheckBox("Mise � 0 des abscisses");
    cbMiseAZero_.addActionListener(this);
    cbMiseAZero_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    cbMiseAZero_.setSelected(false);
    getContentPane().add(cbMiseAZero_, 2);
    cbOffset_= new JCheckBox("D�caler les abscisses");
    cbOffset_.addActionListener(this);
    cbOffset_.setBorder(new EmptyBorder(new Insets(5, 5, 5, 5)));
    cbOffset_.setSelected(false);
    getContentPane().add(cbOffset_, 3);
    tfOffset_= BuTextField.createDoubleField();
    tfOffset_.setValue(new Double(0.));
    tfOffset_.setEnabled(false);
    getContentPane().add(tfOffset_, 4);
    pack();
    setLocationRelativeTo(parent);
    //pack();
  }
  public void actionPerformed(final ActionEvent e) {
    final Object src= e.getSource();
    if (src == cbTxt_) {
      choice_= TXT;
      dispose();
    } else if (src == cbProLido_) {
      choice_= PRO_LIDO;
      dispose();
    } else if (src == cbProRivicad_) {
      choice_= PRO_RIVICAD;
      dispose();
    } else if (src == cbBief_) {
      if (cbBief_.isSelected()) {
        cbMiseAZero_.setSelected(false);
      }
    } else if (src == cbMiseAZero_) {
      if (cbMiseAZero_.isSelected()) {
        cbBief_.setSelected(false);
        cbOffset_.setSelected(false);
        tfOffset_.setEnabled(false);
      }
    } else if (src == cbOffset_) {
      if (cbOffset_.isSelected()) {
        cbMiseAZero_.setSelected(false);
        tfOffset_.setEnabled(true);
      } else {
        tfOffset_.setEnabled(false);
      }
    }
  }
  public int getChoice() {
    return choice_;
  }
  public boolean isNouveauBief() {
    return cbBief_.isSelected();
  }
  public boolean isMiseAZero() {
    return cbMiseAZero_.isSelected();
  }
  public boolean isOffset() {
    return cbOffset_.isSelected();
  }
  public double getOffset() {
    final Double val= (Double)tfOffset_.getValue();
    if (val == null) {
      return 0.;
    }
    return val.doubleValue();
  }
}
