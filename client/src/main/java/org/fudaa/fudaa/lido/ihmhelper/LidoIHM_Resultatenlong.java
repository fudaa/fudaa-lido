/*
 * @file         LidoIHM_Resultatenlong.java
 * @creation     1999-10-05
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SParametresLIG;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoExport;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauResultats;
import org.fudaa.fudaa.lido.visu.LidoVisuLignedeau;
import org.fudaa.fudaa.lido.visu.LidoVisuResultatEnLong;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Resultatenlong                        */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Resultatenlong extends LidoIHM_Base {
  SResultatsRSN rsn_;
  SParametresPRO pro_;
  SParametresLIG lig_;
  // ATTENTION: ce sont les LIG calcules (et pas les params)
  SParametresEXT ext_;
  SParametresCAL cal_;
  LidoTableauResultats resTable_;
  LidoIHM_Resultatenlong(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    rsn_= (SResultatsRSN)p.getResult(LidoResource.RSN);
    if (rsn_ == null) {
      System.err.println(
        "LidoIHM_ResultatEnLong: Warning: passing null RSN to constructor");
    } else {
      if (resTable_ != null) {
        resTable_.setObjects(rsn_.pasTemps[0].ligBief);
      }
    }
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoIHM_ResultatEnLong: Warning: passing null PRO to constructor");
    }
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_ResultatEnLong: Warning: passing null CAL to constructor");
    }
    // ATTENTION : Oui! c'est getResult et non getParam, car on veut les LIG
    //             calcules, et non ceux des params NP
    lig_= (SParametresLIG)p.getParam(LidoResource.LIG);
    if (lig_ == null) {
      System.err.println(
        "LidoIHM_ResultatEnLong: Warning: passing null LIG to constructor");
    }
    ext_= (SParametresEXT)p.getParam(LidoResource.EXT);
    if (ext_ == null) {
      System.err.println(
        "LidoIHM_ResultatEnLong: Warning: passing null EXT to constructor");
    }
    reinit();
  }
  public void editer() {
    if (rsn_ == null) {
      return;
    }
    if (dl != null) {
      resTable_.setObjects(rsn_.pasTemps[0].ligBief);
      dl.activate();
      return;
    }
    resTable_= new LidoTableauResultats();
    resTable_.setAutosort(false);
    if (rsn_.pasTemps.length == 0) {
      return;
    }
    resTable_.setObjects(rsn_.pasTemps[0].ligBief);
    dl= new LidoDialogTableau(resTable_, "R�sultats en long", p_);
    installContextHelp(dl);
    if ((rsn_.pasTemps != null) && (rsn_.pasTemps.length > 0)) {
      final Container cp= dl.getContentPane();
      final JList lsTemps= new JList();
      class ListeTempsListener implements ListSelectionListener {
        JList list_;
        public ListeTempsListener(final JList l) {
          list_= l;
        }
        public void valueChanged(final ListSelectionEvent e) {
          final int index= list_.getSelectedIndex();
          if (index < 0) {
            return;
          }
          resTable_.setObjects(rsn_.pasTemps[index].ligBief);
        }
      }
      lsTemps.addListSelectionListener(new ListeTempsListener(lsTemps));
      final JScrollPane sp= new JScrollPane(lsTemps);
      sp.setBorder(
        new CompoundBorder(
          new EmptyBorder(new Insets(10, 0, 10, 10)),
          new BevelBorder(BevelBorder.LOWERED)));
      cp.add(BorderLayout.EAST, sp);
      final String[] pas= new String[rsn_.pasTemps.length];
      for (int i= 0; i < rsn_.pasTemps.length; i++) {
        pas[i]=
          "Pas " + rsn_.pasTemps[i].np + " (t=" + rsn_.pasTemps[i].t + ")  ";
      }
      lsTemps.setListData(pas);
      lsTemps.setSelectedIndex(0);
    }
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.VOIR | EbliPreferences.DIALOG.EXPORTER);
    dl.addActionList(new String[][] { { "Ligne d'eau", "LIGNEDEAU" }, {
        "Hauteur d'eau", LidoResource.RESULTAT.HAUTEUREAU }, {
        "Section mouill�e", LidoResource.RESULTAT.SECTIONMOUILLEE }, {
        "Rayon hydraulique", LidoResource.RESULTAT.RAYON }, {
        "Largeur au miroir", LidoResource.RESULTAT.B1 }, {
        "Vitesse", LidoResource.RESULTAT.VITESSE }, {
        "D�bit total", LidoResource.RESULTAT.DEBITTOTAL }, {
        "Froude", LidoResource.RESULTAT.FROUDE }, {
        "D�bit", LidoResource.RESULTAT.DEBIT }, {
        "Charge", LidoResource.RESULTAT.CHARGE }, {
        "R�gime critique",
          LidoResource
            .RESULTAT
            .REGIMECRIT } /* ,
              { "R�gime uniforme", LidoResource.RESULTAT.REGIMEUNIF },
              { "Force tractrice", LidoResource.RESULTAT.FORCETRAC } */ //S Ladreyt
    });
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauResultats table= (LidoTableauResultats)dl.getTable();
        if ("VOIR".equals(e.getActionCommand())) {
          if ("LIGNEDEAU".equals(dl.getSelectedActionList())) {
            if (pro_ == null) {
              return;
            }
            final SResultatsBiefRSN[] select=
              (SResultatsBiefRSN[])table.getSelectedObjects();
            if ((select == null) || (select.length == 0)) {
              return;
            }
            final LidoVisuLignedeau g= new LidoVisuLignedeau(dl, true, p_);
            g.setBuildMode(true);
            g.setResultats(rsn_);
            final Vector pv[]= new Vector[select.length];
            for (int i= 0; i < select.length; i++) {
              //              int proDepart=select[i].numBief==0?0:(pro_.donneesBief[select[i].numBief-1].premierProfilAmont+1);
              //              int proFin=pro_.donneesBief[select[i].numBief].premierProfilAmont;
              //              SParametresBiefBlocPRO[] pros=new SParametresBiefBlocPRO[proFin-proDepart+1];
              //              for(int p=proDepart; p<=proFin; p++) {
              //                pros[p-proDepart]=pro_.profilsBief[p];
              //              }
              pv[i]= new Vector();
              for (int p= 0; p < pro_.nbProfils; p++) {
                if ((pro_.profilsBief[p].abscisse >= select[i].ligne[0].x)
                  && (pro_.profilsBief[p].abscisse
                    <= select[i].ligne[select[i].ligne.length - 1].x)) {
                  pv[i].add(pro_.profilsBief[p]);
                }
              }
            }
            for (int i= 0; i < select.length; i++) {
              final Object[] prosObj= pv[i].toArray();
              final SParametresBiefBlocPRO[] pros=
                new SParametresBiefBlocPRO[prosObj.length];
              for (int j= 0; j < prosObj.length; j++) {
                pros[j]= (SParametresBiefBlocPRO)prosObj[j];
              }
              g.addBief(select[i].numero, pros);
            }
            g.commitData();
            if (cal_ != null) {
              if ("NP".equalsIgnoreCase(cal_.genCal.regime.trim())) {
                g.setLigneDeau(lig_, null, true);
              } else {
                g.setPermanent(true);
              }
            }
            g.setLaisses(ext_);
            g.setBuildMode(false);
            g.pack();
            g.show();
          } else {
            final SResultatsBiefRSN[] select=
              (SResultatsBiefRSN[])table.getSelectedObjects();
            if ((select == null) || (select.length == 0)) {
              return;
            }
            final LidoVisuResultatEnLong g=
              new LidoVisuResultatEnLong(
                dl,
                dl.getSelectedActionList(),
                getInformationsDocument());
            g.setResultats(rsn_);
            for (int i= 0; i < select.length; i++) {
              g.addBief(select[i].numero);
            }
            g.pack();
            g.show();
          }
        } else if ("EXPORTER".equals(e.getActionCommand())) {
          final File file= LidoExport.chooseFile(null);
          if (file == null) {
            return;
          }
          System.out.println("fichier non null");
          final SResultatsBiefRSN[] select=
            (SResultatsBiefRSN[])table.getSelectedObjects();
          System.out.println("test de select");
          if ((select == null) || (select.length == 0)) {
            return;
          }
          System.out.println("select nombre:" + select.length);
          try {
            System.out.println(
              "LidoExport.exportResultatTXT(select, dl.getSelectedActionList(), file);");
            LidoExport.exportResultatTXT(
              select,
              dl.getSelectedActionList(),
              file);
          } catch (final java.io.IOException ioe) {
            new BuDialogError(
              (BuCommonInterface)LidoApplication.FRAME,
              ((BuCommonInterface)LidoApplication.FRAME)
                .getInformationsSoftware(),
              ioe.getMessage())
              .activate();
            return;
          }
        }
      }
    });
    /*int reponse= */dl.activate();
    //System.out.println("reponse "+reponse);
  }
}
