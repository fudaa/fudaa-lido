/*
 * @file         LidoIHM_Resultaterreurs.java
 * @creation     2000-04-05
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.dodico.corba.lido.SResultatsERN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Resultaterreurs                       */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Resultaterreurs extends LidoIHM_Base {
  SResultatsRSN rsn_;
  private SResultatsERN ern_;
  JDialog edit;
  JTextArea taERN_;
  JTextArea taRSN_;
  JList lsTemps_;
  LidoIHM_Resultaterreurs(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    rsn_= (SResultatsRSN)p.getResult(LidoResource.RSN);
    if (rsn_ == null) {
      System.err.println(
        "LidoIHM_Resultaterreurs: Warning: passing null RSN to constructor");
    }
    ern_= (SResultatsERN)p.getResult(LidoResource.ERN);
    if (ern_ == null) {
      System.err.println(
        "LidoIHM_Resultaterreurs: Warning: passing null ERN to constructor");
    }
    reinit();
  }
  public void editer() {
    if (edit == null) {
      edit= new JDialog(LidoApplication.FRAME, "Rapport de calcul");
      edit.setModal(true);
      //edit.setLocationRelativeTo(LidoApplication.FRAME);
      final Container cp= edit.getContentPane();
      final JTabbedPane tb= new JTabbedPane();
      taERN_= new JTextArea();
      taERN_.setEditable(false);
      JScrollPane sp= new JScrollPane(taERN_);
      sp.setBorder(
        new CompoundBorder(
          new EmptyBorder(new Insets(10, 10, 10, 10)),
          new BevelBorder(BevelBorder.LOWERED)));
      tb.addTab("ERN", sp);
      final JPanel pnRSN= new JPanel();
      pnRSN.setLayout(new BorderLayout());
      taRSN_= new JTextArea();
      taRSN_.setEditable(false);
      sp= new JScrollPane(taRSN_);
      sp.setBorder(
        new CompoundBorder(
          new EmptyBorder(new Insets(10, 10, 10, 10)),
          new BevelBorder(BevelBorder.LOWERED)));
      pnRSN.add(BorderLayout.CENTER, sp);
      lsTemps_= new JList();
      lsTemps_.addListSelectionListener(
        new ListeTempsListener(lsTemps_, taRSN_));
      sp= new JScrollPane(lsTemps_);
      sp.setBorder(
        new CompoundBorder(
          new EmptyBorder(new Insets(10, 0, 10, 10)),
          new BevelBorder(BevelBorder.LOWERED)));
      pnRSN.add(BorderLayout.EAST, sp);
      tb.addTab("RSN", pnRSN);
      cp.add(tb);
      edit.pack();
    }
    if ((ern_ == null) || (ern_.contenu == null) || (ern_.contenu.equals(""))) {
      taERN_.setText("Pas d'information ERN!");
    } else {
      taERN_.setText(ern_.contenu);
    }
    if ((rsn_ == null) || (rsn_.pasTemps == null)) {
      taRSN_.setText("Pas d'information RSN!");
    } else {
      final Vector pasv= new Vector();
      for (int i= 0; i < rsn_.pasTemps.length; i++) {
        try {
          pasv.add(
            "Pas " + rsn_.pasTemps[i].np + " (t=" + rsn_.pasTemps[i].t + ")  ");
        } catch (final NullPointerException e) {
          break;
        }
      }
      final String[] pas= new String[pasv.size()];
      for (int i= 0; i < pas.length; i++) {
        pas[i]= (String)pasv.get(i);
      }
      lsTemps_.setListData(pas);
      lsTemps_.setSelectedIndex(0);
    }
    edit.setSize(new Dimension(640, 480));
    edit.show();
  }
  class ListeTempsListener implements ListSelectionListener {
    JList list_;
    JTextArea tx_;
    public ListeTempsListener(final JList l, final JTextArea tx) {
      list_= l;
      tx_= tx;
    }
    public void valueChanged(final ListSelectionEvent e) {
      int index= -1;
      try {
        index= list_.getSelectedIndex();
        if (index < 0) {
          return;
        }
        taRSN_.setText(rsn_.pasTemps[index].info);
      } catch (final NullPointerException ne) {
        System.err.println("JList: null at index " + index);
      }
    }
  }
}
