/*
 * @file         LidoIHM_Resultatlaisse.java
 * @creation     2000-05-03
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauLaisses;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Resultatlaisse                        */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Resultatlaisse extends LidoIHM_Base {
  private SParametresEXT ext_;
  private SResultatsRSN rsn_;
  private LidoTableauLaisses laiTable_;
  LidoIHM_Resultatlaisse(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    //    //System.out.println("set Projet pour LidoIHM_Resultatlaisse");
    p_= p;
    rsn_= (SResultatsRSN)p.getResult(LidoResource.RSN);
    if (rsn_ == null) {
      System.err.println(
        "LidoIHM_Resultatlaisse: Warning: passing null RSN to constructor");
    }
    ext_= (SParametresEXT)p.getParam(LidoResource.EXT);
    if (ext_ == null) {
      System.err.println(
        "LidoIHM_Resultatlaisse: Warning: passing null EXT to constructor");
    } else {
      if (laiTable_ != null) {
        laiTable_.setObjects(ext_.laisses);
      }
    }
    reinit();
  }
  public void editer() {
    if (ext_ == null) {
      return;
    }
    if (dl != null) {
      laiTable_.setObjects(ext_.laisses);
      dl.activate();
      //System.out.println("dl non null");
      return;
    }
    //System.out.println("construction table");
    laiTable_= new LidoTableauLaisses();
    laiTable_.setModeResultat(true, ph_, rsn_);
    laiTable_.setAutosort(true);
    laiTable_.setObjects(ext_.laisses);
    dl= new LidoDialogTableau(laiTable_, "Laisses de crues", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setName("TABLEAURESULTATSLAISSES");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.activate();
  }
}
