/*
 * @file         LidoIHM_Resultatprofil.java
 * @creation     1999-10-19
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauProfils;
import org.fudaa.fudaa.lido.visu.LidoVisuProfilTravers;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Resultatprofil                        */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Resultatprofil extends LidoIHM_Base {
  SParametresPRO pro_;
  SResultatsRSN rsn_;
  SParametresCAL cal_;
  private LidoTableauProfils proTable_;
  LidoIHM_Resultatprofil(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_ResultatEnLong: Warning: passing null CAL to constructor");
    }
    rsn_= (SResultatsRSN)p.getResult(LidoResource.RSN);
    if (rsn_ == null) {
      System.err.println(
        "LidoIHM_ResultatsProfils: Warning: passing null RSN to constructor");
    }
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoIHM_ResultatsProfils: Warning: passing null PRO to constructor");
    } else {
      if (proTable_ != null) {
        proTable_.setObjects(pro_.profilsBief);
      }
    }
    reinit();
  }
  public void editer() {
    if (rsn_ == null) {
      return;
    }
    if (pro_ == null) {
      return;
    }
    if (dl != null) {
      proTable_.setObjects(pro_.profilsBief);
      dl.activate();
      return;
    }
    proTable_= new LidoTableauProfils(ph_);
    proTable_.setAutosort(true);
    proTable_.setObjects(pro_.profilsBief);
    proTable_.setModeResultat(true);
    dl= new LidoDialogTableau(proTable_, "Résultats en travers", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(EbliPreferences.DIALOG.VOIR);
    dl.setName("TABLEAURESULTATSPROFILS");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauProfils table= (LidoTableauProfils)dl.getTable();
        if ("VOIR".equals(e.getActionCommand())) {
          final SParametresBiefBlocPRO select=
            (SParametresBiefBlocPRO)table.getSelectedObject();
          if (select == null) {
            return;
          }
          final LidoVisuProfilTravers g= new LidoVisuProfilTravers(dl, ph_);
          g.setBuildMode(true);
          g.setName("VISUPROFILDIALOG");
          if (cal_ != null) {
            if ("NP".equalsIgnoreCase(cal_.genCal.regime.trim())) {
              g.setPermanent(false);
            } else {
              g.setPermanent(true);
            }
          }
          g.setProfil(select);
          g.setResultats(rsn_);
          g.addActionListener(new ActionListener() {
            int count= 0;
            public void actionPerformed(final ActionEvent e2) {
              final String cmd= e2.getActionCommand();
              if (cmd.startsWith("PROFRECULER")) {
                if ((select.indice + count) == 0) {
                  return;
                }
                if (cmd.endsWith("VITE")) {
                  final int avt=
                    LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
                  count= Math.max(count - avt, -select.indice);
                } else {
                  count--;
                }
                g.setProfil(pro_.profilsBief[select.indice + count]);
              } else if (cmd.startsWith("PROFAVANCER")) {
                if ((select.indice + count) == (pro_.nbProfils - 1)) {
                  return;
                }
                if (cmd.endsWith("VITE")) {
                  final int avt=
                    LidoPreferences.LIDO.getIntegerProperty("profil.rapide", 5);
                  count=
                    Math.min(count + avt, -select.indice + pro_.nbProfils - 1);
                } else {
                  count++;
                }
                g.setProfil(pro_.profilsBief[select.indice + count]);
              }
            }
          });
          g.setBuildMode(false);
          g.pack();
          g.show();
        }
      }
    });
    dl.activate();
  }
}
