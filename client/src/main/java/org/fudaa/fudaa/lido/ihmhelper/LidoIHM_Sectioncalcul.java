/*
 * @file         LidoIHM_Sectioncalcul.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JOptionPane;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;

import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresSectionLigneCAL;
import org.fudaa.dodico.corba.lido.SParametresSerieLigneCAL;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoNombreSectionsEditor;
import org.fudaa.fudaa.lido.editor.LidoPermSectionsEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauPermSection2s;
import org.fudaa.fudaa.lido.tableau.LidoTableauPermSection3s;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Sectioncalcul                         */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Sectioncalcul extends LidoIHM_Base {
  SParametresCAL cal_;
  LidoCustomizer edit;
  LidoTableauPermSection2s table2s;
  LidoTableauPermSection3s table3s;
  LidoDialogTableau dl2;
  LidoDialogTableau dl3;
  LidoIHM_Sectioncalcul(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Sectioncalcul: Warning: passing null CAL to constructor");
    } else {
      if (table2s != null) {
        table2s.setObjects(cal_.sections.series.ligne);
      }
      if (table3s != null) {
        table3s.setObjects(cal_.sections.sections.ligne);
      }
      if (edit != null) {
        edit.setObject(cal_.sections);
      }
    }
    reinit();
  }
  public void editer() {
    if (cal_ == null) {
      return;
    }
    if (edit != null) {
      edit.show();
      return;
    }
    edit= new LidoPermSectionsEditor();
    edit.setObject(cal_.sections);
    edit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if ("EDITER".equals(e.getActionCommand())) {
          switch (((LidoPermSectionsEditor)edit).getSelectedChoix()) {
            case 1 :
              {
                if (cal_.sections.series.nbSeries == 0) {
                  ph_.PERMSECTION2().remplitAvecProfils(cal_.sections.series);
                  if (table2s != null) {
                    table2s.setObjects(cal_.sections.series.ligne);
                  }
                }
                if (table2s == null) {
                  table2s= new LidoTableauPermSection2s(ph_);
                  table2s.setAutosort(false);
                  table2s.setObjects(cal_.sections.series.ligne);
                  ph_.PERMSECTION2().addPropertyChangeListener(table2s);
                }
                if (dl2 != null) {
                  table2s.trie();
                  dl2.activate();
                  return;
                }
                dl2=
                  new LidoDialogTableau(
                    edit,
                    table2s,
                    "Sections de calcul",
                    p_);
                installContextHelp(dl2);
                dl2.setNavPanel(EbliPreferences.DIALOG.FERMER);
                dl2.setActionPanel(
                  EbliPreferences.DIALOG.CREER
                    | EbliPreferences.DIALOG.SUPPRIMER
                    | EbliPreferences.DIALOG.RESET);
                dl2.addAction("Nbr sections", "NOMBRE");
                final LidoAssistant ass= LidoResource.ASSISTANT;
                if (ass != null) {
                  ass.addEmitters(dl2);
                }
                dl2.addActionListener(new ActionListener() {
                  public void actionPerformed(final ActionEvent evt) {
                    if ("CREER".equals(evt.getActionCommand())) {
                      final int nouvPos= table2s.getPositionNouveau();
                      ph_.PERMSECTION2().nouveauPermSection2(nouvPos);
                      table2s.editeCellule(nouvPos, 0);
                    } else if ("SUPPRIMER".equals(evt.getActionCommand())) {
                      ph_.PERMSECTION2().supprimeSelection(
                        (SParametresSerieLigneCAL[])table2s
                          .getSelectedObjects());
                    } else if ("RESET".equals(evt.getActionCommand())) {
                      final int res=
                        new BuDialogConfirmation(
                          (BuCommonInterface)LidoApplication.FRAME,
                          ((BuCommonInterface)LidoApplication.FRAME)
                            .getInformationsSoftware(),
                          "Voulez-vous recalculer automatiquement\n"
                            + "les sections de calcul?")
                          .activate();
                      if (res == JOptionPane.YES_OPTION) {
                        ph_.PERMSECTION2().remplitAvecProfils(
                          cal_.sections.series);
                      }
                    } else if ("NOMBRE".equals(evt.getActionCommand())) {
                      final SParametresSerieLigneCAL[] select=
                        (SParametresSerieLigneCAL[])table2s
                          .getSelectedObjects();
                      if (select == null) {
                        return;
                      }
                      final LidoNombreSectionsEditor localEdit=
                        new LidoNombreSectionsEditor(dl2);
                      localEdit.setObject(select);
                      localEdit
                        .addPropertyChangeListener(
                          new PropertyChangeListener() {
                        public void propertyChange(final PropertyChangeEvent p) {
                          if ("object".equals(p.getPropertyName())) {
                            table2s.repaint();
                          }
                        }
                      });
                      listenToEditor(localEdit);
                      localEdit.show();
                    }
                  }
                });
                dl2.activate();
                break;
              }
            case 2 :
              {
                if (table3s == null) {
                  table3s= new LidoTableauPermSection3s();
                  table3s.setAutosort(false);
                  table3s.setObjects(cal_.sections.sections.ligne);
                  ph_.PERMSECTION3().addPropertyChangeListener(table3s);
                }
                if (dl3 != null) {
                  table3s.trie();
                  dl3.activate();
                  return;
                }
                dl3=
                  new LidoDialogTableau(
                    edit,
                    table3s,
                    "Sections de calcul",
                    p_);
                installContextHelp(dl3);
                dl3.setNavPanel(EbliPreferences.DIALOG.FERMER);
                dl3.setActionPanel(
                  EbliPreferences.DIALOG.CREER
                    | EbliPreferences.DIALOG.SUPPRIMER);
                final LidoAssistant ass= LidoResource.ASSISTANT;
                if (ass != null) {
                  ass.addEmitters(dl3);
                }
                dl3.addActionListener(new ActionListener() {
                  public void actionPerformed(final ActionEvent evt) {
                    if ("CREER".equals(evt.getActionCommand())) {
                      final int nouvPos= table3s.getPositionNouveau();
                      ph_.PERMSECTION3().nouveauPermSection3(nouvPos);
                      table3s.editeCellule(nouvPos, 0);
                    } else if ("SUPPRIMER".equals(evt.getActionCommand())) {
                      ph_.PERMSECTION3().supprimeSelection(
                        (SParametresSectionLigneCAL[])table3s
                          .getSelectedObjects());
                    }
                  }
                });
                dl3.activate();
                break;
              }
          }
        }
      }
    });
    listenToEditor(edit);
    edit.show();
  }
}
