/*
 * @file         LidoIHM_Singularite.java
 * @creation     1999-07-26
 * @modification $Date: 2006-09-19 15:04:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JDialog;

import org.fudaa.dodico.corba.lido.SParametresBiefSingLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresRZO;
import org.fudaa.dodico.corba.lido.SParametresSNG;
import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.ebli.commun.EbliPreferences;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoAssistant;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.editor.LidoCustomizer;
import org.fudaa.fudaa.lido.editor.LidoDialogTableau;
import org.fudaa.fudaa.lido.editor.LidoSingulariteChooser;
import org.fudaa.fudaa.lido.editor.LidoSingulariteCourbeEditor;
import org.fudaa.fudaa.lido.editor.LidoSingulariteSeuilDenoyeEditor;
import org.fudaa.fudaa.lido.editor.LidoSingulariteSeuilNoyeEditor;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
import org.fudaa.fudaa.lido.tableau.LidoTableauSingularites;
/******************************************************************************/
/*                                                                            */
/*                              LidoIHM_Singularite                           */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:04:59 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoIHM_Singularite extends LidoIHM_Base {
  SParametresRZO rzo_;
  SParametresCAL cal_;
  SParametresSNG sng_;
  LidoTableauSingularites sinTable_;
  LidoIHM_Singularite(final FudaaProjet p, final LidoParamsHelper _ph) {
    super(p, _ph);
  }
  void setProjet(final FudaaProjet p) {
    p_= p;
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoIHM_Singularite: Warning: passing null RZO to constructor");
    } else {
      if (sinTable_ != null) {
        sinTable_.setObjects(rzo_.blocSings.ligneSing);
      }
    }
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoIHM_Singularite: Warning: passing null CAL to constructor");
    }
    sng_= (SParametresSNG)p.getParam(LidoResource.SNG);
    if (sng_ == null) {
      System.err.println(
        "LidoIHM_Singularite: Warning: passing null SNG to constructor");
    }
    reinit();
  }
  public void editer() {
    if (rzo_ == null) {
      return;
    }
    if (cal_ == null) {
      return;
    }
    if (sng_ == null) {
      return;
    }
    if (dl != null) {
      sinTable_.setObjects(rzo_.blocSings.ligneSing);
      dl.activate();
      return;
    }
    sinTable_= new LidoTableauSingularites();
    sinTable_.setAutosort(false);
    sinTable_.setObjects(rzo_.blocSings.ligneSing);
    ph_.SINGULARITE().addPropertyChangeListener(sinTable_);
    dl= new LidoDialogTableau(sinTable_, "SingularitÚs", p_);
    installContextHelp(dl);
    dl.setNavPanel(EbliPreferences.DIALOG.FERMER);
    dl.setActionPanel(
      EbliPreferences.DIALOG.SUPPRIMER | EbliPreferences.DIALOG.EDITER);
    dl.setName("TABLEAUSINGULARITES");
    final LidoAssistant ass= LidoResource.ASSISTANT;
    if (ass != null) {
      ass.addEmitters(dl);
    }
    dl.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final LidoTableauSingularites table= (LidoTableauSingularites)dl.getTable();
        if ("CREER".equals(e.getActionCommand())) {
          final SParametresBiefSingLigneRZO select=
            (SParametresBiefSingLigneRZO)table.getSelectedObject();
          if (select == null) {
            return;
          //int nouvPos=table.getPositionNouveau();
          //Object nouv=ph_.SINGULARITE().nouveauSingularite(nouvPos);
          //table.editeCellule(nouvPos, 0);
          }
        } else if ("SUPPRIMER".equals(e.getActionCommand())) {
          ph_.SINGULARITE().supprimeSelection(
            (SParametresBiefSingLigneRZO[])table.getSelectedObjects());
          table.repaint();
        } else if ("EDITER".equals(e.getActionCommand())) {
          final SParametresBiefSingLigneRZO select=
            (SParametresBiefSingLigneRZO)table.getSelectedObject();
          if (select == null) {
            return;
          }
          /*LidoSingulariteEditor edit_=new LidoSingulariteEditor(dl);
          edit_.setObject(select);
          edit_.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent p)
            {
              if( "object".equals(p.getPropertyName()) )
                ((LidoTableauSingularites)dl.getTable()).repaint();
            }
          });
          edit_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent sae) {
              if( "EDITER".equals(e.getActionCommand()) ) {*/
          final SParametresSingBlocSNG o= ph_.SINGULARITE().getSingularite(select);
          if (o == null) {
            final LidoSingulariteChooser ch=
              new LidoSingulariteChooser(
                dl.getDialog(),
                "P".equals(cal_.genCal.regime.trim().toUpperCase()));
            ch.addActionListener(new ActionListener() {
              public void actionPerformed(final ActionEvent che) {
                final String cmd= che.getActionCommand();
                ((JDialog) ((JComponent)che.getSource()).getTopLevelAncestor())
                  .dispose();
                if ((cmd != null) && (cmd.startsWith("SING:"))) {
                  final SParametresSingBlocSNG o2=
                    ph_.LOISNG().nouveauLoisng(sng_.nbSing);
                  select.nSing= o2.numSing;
                  //loiclmEditor.setLoisclm(ph_.LIMITE().getLoisLimites());
                  //                        loiclmEditor.setLoisclm(clm_.condLimites);
                  LidoCustomizer edit_= null;
                  if ("SING:SEUILNOY".equals(cmd)) {
                    o2.tabParamEntier[LidoResource.SINGULARITE.ITYPE]=
                      LidoResource.SINGULARITE.SEUIL_NOYE;
                    edit_= new LidoSingulariteSeuilNoyeEditor(dl, ph_);
                    ((LidoSingulariteSeuilNoyeEditor)edit_).setFond(
                      ph_.PROFIL().interpoleFond(select.xSing));
                  } else if ("SING:SEUILDENOY".equals(cmd)) {
                    o2.tabParamEntier[LidoResource.SINGULARITE.ITYPE]=
                      LidoResource.SINGULARITE.SEUIL_DENOYE;
                    edit_= new LidoSingulariteSeuilDenoyeEditor(dl, ph_);
                    ((LidoSingulariteSeuilDenoyeEditor)edit_).setFond(
                      ph_.PROFIL().interpoleFond(select.xSing));
                  } else if ("SING:SEUILGEOM".equals(cmd)) {
                    o2.tabParamEntier[LidoResource.SINGULARITE.ITYPE]=
                      LidoResource.SINGULARITE.SEUIL_GEOM;
                    edit_=
                      new LidoSingulariteCourbeEditor(
                        dl,
                        LidoResource.SINGULARITE.SEUIL_GEOM,
                        ph_,
                        select.xSing);
                    //edit_=new LidoSingulariteCourbeEditor(dl, LidoResource.SINGULARITE.SEUIL_GEOM, ph_);
                  } else if ("SING:TARAGEAMONT".equals(cmd)) {
                    o2.tabParamEntier[LidoResource.SINGULARITE.ITYPE]=
                      LidoResource.SINGULARITE.TARAGE_AMONT;
                    edit_=
                      new LidoSingulariteCourbeEditor(
                        dl,
                        LidoResource.SINGULARITE.TARAGE_AMONT,
                        ph_);
                  } else if ("SING:TARAGEAVAL".equals(cmd)) {
                    o2.tabParamEntier[LidoResource.SINGULARITE.ITYPE]=
                      LidoResource.SINGULARITE.TARAGE_AVAL;
                    edit_=
                      new LidoSingulariteCourbeEditor(
                        dl,
                        LidoResource.SINGULARITE.TARAGE_AVAL,
                        ph_);
                  } else if ("SING:LIMNIAMONT".equals(cmd)) {
                    o2
                      .tabParamReel[LidoResource
                      .SINGULARITE
                      .ICOURBE
                      .IVARMAXCOT]=
                      0.01;
                    o2.tabParamEntier[LidoResource.SINGULARITE.ITYPE]=
                      LidoResource.SINGULARITE.LIMNI_AMONT;
                    edit_=
                      new LidoSingulariteCourbeEditor(
                        dl,
                        LidoResource.SINGULARITE.LIMNI_AMONT,
                        ph_);
                  }
                  edit_.setObject(o2);
                  listenToEditor(edit_);
                  edit_.show();
                  dl.getTable().repaint();
                }
              }
            });
            ch.show();
          } else {
            LidoCustomizer edit2= null;
            switch (o.tabParamEntier[LidoResource.SINGULARITE.ITYPE]) {
              case LidoResource.SINGULARITE.SEUIL_NOYE :
                {
                  edit2= new LidoSingulariteSeuilNoyeEditor(dl, ph_);
                  ((LidoSingulariteSeuilNoyeEditor)edit2).setFond(
                    ph_.PROFIL().interpoleFond(select.xSing));
                  break;
                }
              case LidoResource.SINGULARITE.SEUIL_DENOYE :
                {
                  edit2= new LidoSingulariteSeuilDenoyeEditor(dl, ph_);
                  ((LidoSingulariteSeuilDenoyeEditor)edit2).setFond(
                    ph_.PROFIL().interpoleFond(select.xSing));
                  break;
                }
              case LidoResource.SINGULARITE.SEUIL_GEOM :
              case LidoResource.SINGULARITE.LIMNI_AMONT :
              case LidoResource.SINGULARITE.TARAGE_AMONT :
              case LidoResource.SINGULARITE.TARAGE_AVAL :
                {
                  edit2=
                    new LidoSingulariteCourbeEditor(
                      dl,
                      o.tabParamEntier[LidoResource.SINGULARITE.ITYPE],
                      ph_);
                  break;
                }
              default :
                System.err.println("singularite de type inconnu");
                return;
            }
            edit2.setObject(o);
            listenToEditor(edit2);
            edit2.show();
            //                  edit2.removePropertyChangeListener(LOISUPPR_LISTENER);
          }
          /*}
          });
          edit_.show();*/
        }
      }
    });
    dl.activate();
  }
}
