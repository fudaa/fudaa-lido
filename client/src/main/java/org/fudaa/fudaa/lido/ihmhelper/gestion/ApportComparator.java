/*
 * @file         ApportComparator.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Comparator;

import org.fudaa.dodico.corba.lido.SParametresApportLigneCLM;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class ApportComparator implements Comparator {
  ApportComparator() {}
  public int compare(final Object o1, final Object o2) {
    return ((SParametresApportLigneCLM)o1).xApport
      < ((SParametresApportLigneCLM)o2).xApport
      ? -1
      : ((SParametresApportLigneCLM)o1).xApport
        == ((SParametresApportLigneCLM)o2).xApport
      ? 0
      : 1;
  }
  public boolean equals(final Object obj) {
    return false;
  }
}
