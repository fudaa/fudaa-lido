/*
 * @file         LidoPH_Apport.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresApportLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Apport                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPH_Apport extends LidoPH_Base {
  private SParametresCLM clm_;
  LidoPH_Apport(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoPH_Apport: Warning: passing null CLM to constructor");
    }
  }
  public SParametresApportLigneCLM nouveauApport(final int pos) {
    if ((clm_ == null) || (clm_.apport == null)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    final SParametresApportLigneCLM[] apps= clm_.apport;
    final SParametresApportLigneCLM nouv= new SParametresApportLigneCLM();
    // A FAIRE: gerer intelligemment numExtBief
    nouv.xApport= 0.;
    nouv.numLoi= 0; // numLoi doit en fait etre automatiquement:
    // nb lois + 1
    nouv.typLoi= 0;
    nouv.coefApport= 1.;
    final SParametresApportLigneCLM[] nouvApp=
      new SParametresApportLigneCLM[apps.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvApp[i]= apps[i];
    }
    nouvApp[pos]= nouv;
    for (int i= pos; i < apps.length; i++) {
      nouvApp[i + 1]= apps[i];
    }
    clm_.apport= nouvApp;
    clm_.nbApport++;
    prop_.firePropertyChange("apports", apps, nouvApp);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        nouv,
        "apport " + nouv.xApport));
    return nouv;
  }
  /*private SParametresApportLigneCLM supprimeApport(SParametresApportLigneCLM p) {
    if ((clm_ == null) || (clm_.apport == null) || (clm_.apport.length == 0)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    if (p == null)
      return null;
    SParametresApportLigneCLM[] apps= clm_.apport;
    SParametresApportLigneCLM[] nouvApps=
      new SParametresApportLigneCLM[apps.length - 1];
    int i= 0;
    for (i= 0; i < apps.length; i++) {
      if (p == apps[i])
        break;
    }
    // pas trouve
    if (i >= apps.length)
      return null;
    int n= 0;
    for (i= 0; i < apps.length; i++) {
      if (p != apps[i])
        nouvApps[n++]= apps[i];
    }
    clm_.apport= nouvApps;
    clm_.nbApport= nouvApps.length;
    // reclassement des indices ?
    prop_.firePropertyChange("apports", apps, nouvApps);
    fireParamStructDeleted(
      new FudaaParamEvent(this, 0, LidoResource.CLM, p, "apport " + p.xApport));
    return p;
  }*/
  /*private SParametresApportLigneCLM[] new_supprimeSelection(SParametresApportLigneCLM[] sel) {
    if ((clm_ == null) || (clm_.apport == null) || (clm_.apport.length == 0)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    if (sel == null)
      return null;
    Vector v= new Vector();
    for (int i= 0; i < sel.length; i++)
      v.add(supprimeApport(sel[i]));
    SParametresApportLigneCLM[] res= new SParametresApportLigneCLM[v.size()];
    for (int i= 0; i < v.size(); i++)
      res[i]= (SParametresApportLigneCLM)v.get(i);
    return res;
  }*/
  public SParametresApportLigneCLM[] supprimeSelection(final SParametresApportLigneCLM[] sel) {
    if ((clm_ == null) || (clm_.apport == null) || (clm_.apport.length == 0)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresApportLigneCLM[] apps= clm_.apport;
    final SParametresApportLigneCLM[] nouvApps=
      new SParametresApportLigneCLM[apps.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < apps.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (apps[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvApps[n++]= apps[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            apps[i],
            "apport " + apps[i].xApport));
      }
    }
    clm_.apport= nouvApps;
    clm_.nbApport= nouvApps.length;
    // reclassement des indices ?
    prop_.firePropertyChange("apports", apps, nouvApps);
    return sel;
  }
  public SParametresCondLimBlocCLM getLimite(final SParametresApportLigneCLM sel) {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.condLimites.length == 0)) {
      System.err.println("LidoPH_Limite: Warning: limites null");
      return null;
    }
    if ((clm_ == null) || (clm_.apport == null) || (clm_.apport.length == 0)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    for (int i= 0; i < clm_.condLimites.length; i++) {
      if (clm_.condLimites[i].numCondLim == sel.numLoi) {
        return clm_.condLimites[i];
      }
    }
    return null;
  }
  public SParametresApportLigneCLM[] getApportsByLoi(final SParametresCondLimBlocCLM loi) {
    if ((clm_ == null) || (clm_.condLimites == null)) {
      System.err.println("LidoPH_Limite: Warning: limites null");
      return null;
    }
    if ((clm_ == null) || (clm_.apport == null)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    final Vector trouves= new Vector();
    for (int i= 0; i < clm_.apport.length; i++) {
      if (clm_.apport[i].numLoi == loi.numCondLim) {
        trouves.add(clm_.apport[i]);
      }
    }
    final SParametresApportLigneCLM[] res=
      new SParametresApportLigneCLM[trouves.size()];
    for (int i= 0; i < trouves.size(); i++) {
      res[i]= (SParametresApportLigneCLM)trouves.get(i);
    }
    return res;
  }
  public SParametresCondLimBlocCLM[] getLoisApports() {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.apport == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    final Vector lois= new Vector();
    for (int i= 0; i < clm_.nbApport; i++) {
      final SParametresCondLimBlocCLM loi= getLimite(clm_.apport[i]);
      if ((loi != null) && !lois.contains(loi)) {
        lois.add(loi);
      }
    }
    final SParametresCondLimBlocCLM[] res= new SParametresCondLimBlocCLM[lois.size()];
    for (int i= 0; i < lois.size(); i++) {
      res[i]= (SParametresCondLimBlocCLM)lois.get(i);
    }
    return res;
  }
  void changeTypeLoi(final SParametresCondLimBlocCLM loi) {
    final SParametresApportLigneCLM[] apps= getApportsByLoi(loi);
    for (int i= 0; i < apps.length; i++) {
      apps[i].typLoi= loi.typLoi;
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.CLM,
          apps[i],
          "apport " + apps[i].xApport));
    }
    prop_.firePropertyChange("apports", null, clm_.apport);
  }
  void loiclmSupprimee(final SParametresCondLimBlocCLM loi) {
    if ((clm_ == null) || (clm_.apport == null) || (clm_.apport.length == 0)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return;
    }
    for (int i= 0; i < clm_.apport.length; i++) {
      if (clm_.apport[i].numLoi == loi.numCondLim) {
        clm_.apport[i].typLoi= 0;
        clm_.apport[i].numLoi= 0;
        fireParamStructCreated(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            clm_.apport[i],
            "apport " + clm_.apport[i].xApport));
      }
    }
    prop_.firePropertyChange("apports", null, clm_.apport);
  }
  public SParametresApportLigneCLM[] getApportsHorsBief() {
    if ((clm_ == null) || (clm_.apport == null) || (clm_.apport.length == 0)) {
      System.err.println("LidoPH_Apport: Warning: apports null");
      return null;
    }
    final SParametresApportLigneCLM[] apps= clm_.apport;
    final Vector resV= new Vector();
    for (int i= 0; i < apps.length; i++) {
      final int b= ph_.BIEF().getBiefContenantAbscisse(apps[i].xApport);
      if (b == -1) {
        System.err.println("Apport " + apps[i].xApport + " hors-bief");
        resV.add(apps[i]);
      }
    }
    final SParametresApportLigneCLM[] res= new SParametresApportLigneCLM[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresApportLigneCLM)resV.get(i);
    }
    return res;
  }
}
