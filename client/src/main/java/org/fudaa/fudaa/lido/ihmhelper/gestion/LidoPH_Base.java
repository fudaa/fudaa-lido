/*
 * @file         LidoPH_Base.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Base                                   */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
abstract public class LidoPH_Base {
  protected PropertyChangeSupport prop_;
  protected LidoParamsHelper ph_;
  LidoPH_Base(final FudaaProjet p, final LidoParamsHelper ph) {
    ph_= ph;
    prop_= new PropertyChangeSupport(this);
    setProjet(p);
  }
  abstract void setProjet(FudaaProjet p);
  protected LidoParamsHelper getPH() {
    return ph_;
  }
  public void addPropertyChangeListener(final PropertyChangeListener l) {
    prop_.addPropertyChangeListener(l);
  }
  public void removePropertyChangeListener(final PropertyChangeListener l) {
    prop_.removePropertyChangeListener(l);
  }
  public synchronized void fireParamStructCreated(final FudaaParamEvent e) {
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructCreated(e);
  }
  public synchronized void fireParamStructDeleted(final FudaaParamEvent e) {
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructDeleted(e);
  }
  public synchronized void fireParamStructModified(final FudaaParamEvent e) {
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(e);
  }
}
