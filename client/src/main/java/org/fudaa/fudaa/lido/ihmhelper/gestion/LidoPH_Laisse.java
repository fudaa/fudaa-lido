/*
 * @file         LidoPH_Laisse.java
 * @creation     1999-12-24
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SParametresLaisseLigneEXT;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Laisse                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoPH_Laisse extends LidoPH_Base {
  private SParametresEXT ext_;
  LidoPH_Laisse(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    ext_= (SParametresEXT)p.getParam(LidoResource.EXT);
    if (ext_ == null) {
      System.err.println(
        "LidoPH_Laisse: Warning: passing null EXT to constructor");
    }
  }
  public SParametresLaisseLigneEXT nouveauLaisse(final int pos) {
    if ((ext_ == null) || (ext_.laisses == null)) {
      System.err.println("LidoPH_Laisse: Warning: laisses null");
      return null;
    }
    final SParametresLaisseLigneEXT[] lais= ext_.laisses;
    final SParametresLaisseLigneEXT nouv= new SParametresLaisseLigneEXT();
    nouv.titre= "Laisse " + (lais.length + 1);
    nouv.abscisse= 0.;
    nouv.cote= 0.;
    final SParametresLaisseLigneEXT[] nouvLai=
      new SParametresLaisseLigneEXT[lais.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvLai[i]= lais[i];
    }
    nouvLai[pos]= nouv;
    for (int i= pos; i < lais.length; i++) {
      nouvLai[i + 1]= lais[i];
    }
    ext_.laisses= nouvLai;
    prop_.firePropertyChange("laisses", lais, nouvLai);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.EXT,
        nouv,
        "laisse " + nouv.titre));
    return nouv;
  }
  public SParametresLaisseLigneEXT[] supprimeSelection(final SParametresLaisseLigneEXT[] sel) {
    if ((ext_ == null)
      || (ext_.laisses == null)
      || (ext_.laisses.length == 0)) {
      System.err.println("LidoPH_Laisses: Warning: laisses null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresLaisseLigneEXT[] lais= ext_.laisses;
    final SParametresLaisseLigneEXT[] nouvLais=
      new SParametresLaisseLigneEXT[lais.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < lais.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (lais[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvLais[n++]= lais[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.EXT,
            lais[i],
            "laisse " + lais[i].titre));
      }
    }
    ext_.laisses= nouvLais;
    prop_.firePropertyChange("laisses", lais, nouvLais);
    return sel;
  }
}
