/*
 * @file         LidoPH_Limite.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresBiefLimLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Limite                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPH_Limite extends LidoPH_Base {
  private SParametresRZO rzo_;
  private SParametresCLM clm_;
  LidoPH_Limite(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoPH_Limite: Warning: passing null RZO to constructor");
    }
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoPH_Limite: Warning: passing null CLM to constructor");
    }
  }
  public SParametresBiefLimLigneRZO nouveauLimite(final int pos) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    // A FAIRE: gerer les incoherences!!
    final SParametresBiefLimLigneRZO[] lims= rzo_.blocLims.ligneLim;
    final SParametresBiefLimLigneRZO nouv= new SParametresBiefLimLigneRZO();
    // A FAIRE: gerer intelligemment numExtBief
    nouv.numExtBief= 0;
    nouv.numLoi= 0;
    nouv.typLoi= 0;
    final SParametresBiefLimLigneRZO[] nouvLim=
      new SParametresBiefLimLigneRZO[lims.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvLim[i]= lims[i];
    }
    nouvLim[pos]= nouv;
    for (int i= pos; i < lims.length; i++) {
      nouvLim[i + 1]= lims[i];
    }
    rzo_.blocLims.ligneLim= nouvLim;
    rzo_.nbLimi++;
    prop_.firePropertyChange("limites", lims, nouvLim);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        nouv,
        "limite " + nouv.numLoi));
    return nouv;
  }
  /*private SParametresBiefLimLigneRZO supprimeLimite(SParametresBiefLimLigneRZO p) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    if (p == null)
      return null;
    SParametresBiefLimLigneRZO[] lims= rzo_.blocLims.ligneLim;
    SParametresBiefLimLigneRZO[] nouvLims=
      new SParametresBiefLimLigneRZO[lims.length - 1];
    int i= 0;
    for (i= 0; i < lims.length; i++) {
      if (p == lims[i])
        break;
    }
    // pas trouve
    if (i >= lims.length)
      return null;
    int n= 0;
    for (i= 0; i < lims.length; i++) {
      if (p != lims[i])
        nouvLims[n++]= lims[i];
    }
    rzo_.blocLims.ligneLim= nouvLims;
    rzo_.nbLimi= nouvLims.length;
    // reclassement des indices ?
    // A FAIRE: gerer les CLM
    prop_.firePropertyChange("limites", lims, nouvLims);
    fireParamStructDeleted(
      new FudaaParamEvent(this, 0, LidoResource.RZO, p, "limite " + p.numLoi));
    return p;
  }*/
  /*private SParametresBiefLimLigneRZO[] new_supprimeSelection(SParametresBiefLimLigneRZO[] sel) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    if (sel == null)
      return null;
    Vector v= new Vector();
    for (int i= 0; i < sel.length; i++)
      v.add(supprimeLimite(sel[i]));
    SParametresBiefLimLigneRZO[] res= new SParametresBiefLimLigneRZO[v.size()];
    for (int i= 0; i < v.size(); i++)
      res[i]= (SParametresBiefLimLigneRZO)v.get(i);
    return res;
  }*/
  public SParametresBiefLimLigneRZO[] supprimeSelection(final SParametresBiefLimLigneRZO[] sel) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresBiefLimLigneRZO[] lims= rzo_.blocLims.ligneLim;
    final SParametresBiefLimLigneRZO[] nouvLims=
      new SParametresBiefLimLigneRZO[lims.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < lims.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (lims[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvLims[n++]= lims[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            lims[i],
            "limite " + lims[i].numLoi));
      }
    }
    rzo_.blocLims.ligneLim= nouvLims;
    rzo_.nbLimi= nouvLims.length;
    // reclassement des indices ?
    // A FAIRE: gerer les CLM
    prop_.firePropertyChange("limites", lims, nouvLims);
    return sel;
  }
  public SParametresCondLimBlocCLM getLimite(final SParametresBiefLimLigneRZO sel) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.condLimites.length == 0)) {
      System.err.println("LidoPH_Limite: Warning: limites null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    for (int i= 0; i < clm_.condLimites.length; i++) {
      if (clm_.condLimites[i].numCondLim == sel.numLoi) {
        return clm_.condLimites[i];
      }
    }
    return null;
  }
  public SParametresBiefLimLigneRZO[] getLimitesByLoi(final SParametresCondLimBlocCLM loi) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.condLimites.length == 0)) {
      System.err.println("LidoPH_Limite: Warning: limites null");
      return null;
    }
    final Vector trouves= new Vector();
    for (int i= 0; i < rzo_.blocLims.ligneLim.length; i++) {
      if (rzo_.blocLims.ligneLim[i].numLoi == loi.numCondLim) {
        trouves.add(rzo_.blocLims.ligneLim[i]);
      }
    }
    final SParametresBiefLimLigneRZO[] res=
      new SParametresBiefLimLigneRZO[trouves.size()];
    for (int i= 0; i < trouves.size(); i++) {
      res[i]= (SParametresBiefLimLigneRZO)trouves.get(i);
    }
    return res;
  }
  public SParametresCondLimBlocCLM[] getLoisLimites() {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return null;
    }
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.condLimites.length == 0)) {
      System.err.println("LidoPH_Limite: Warning: limites null");
      return null;
    }
    final Vector lois= new Vector();
    for (int i= 0; i < rzo_.nbLimi; i++) {
      final SParametresCondLimBlocCLM loi= getLimite(rzo_.blocLims.ligneLim[i]);
      if ((loi != null) && !lois.contains(loi)) {
        lois.add(loi);
      }
    }
    final SParametresCondLimBlocCLM[] res= new SParametresCondLimBlocCLM[lois.size()];
    for (int i= 0; i < lois.size(); i++) {
      res[i]= (SParametresCondLimBlocCLM)lois.get(i);
    }
    return res;
  }
  void changeTypeLoi(final SParametresCondLimBlocCLM loi) {
    final SParametresBiefLimLigneRZO[] apps= getLimitesByLoi(loi);
    for (int i= 0; i < apps.length; i++) {
      apps[i].typLoi= loi.typLoi;
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.RZO,
          apps[i],
          "limite " + apps[i].numLoi));
    }
    prop_.firePropertyChange("limites", null, rzo_.blocLims.ligneLim);
  }
  void loiclmSupprimee(final SParametresCondLimBlocCLM loi) {
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return;
    }
    for (int i= 0; i < rzo_.blocLims.ligneLim.length; i++) {
      if (rzo_.blocLims.ligneLim[i].numLoi == loi.numCondLim) {
        rzo_.blocLims.ligneLim[i].numLoi= 0;
        rzo_.blocLims.ligneLim[i].typLoi= 0;
        fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            rzo_.blocLims.ligneLim[i],
            "limite " + rzo_.blocLims.ligneLim[i].numLoi));
      }
    }
    prop_.firePropertyChange("limites", null, rzo_.blocLims.ligneLim);
  }
  boolean majBiefSupprime(final SParametresBiefSituLigneRZO b) {
    boolean maj= false;
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return maj;
    }
    if (b == null) {
      return maj;
    }
    for (int n= 0; n < rzo_.nbLimi; n++) {
      if ((rzo_.blocLims.ligneLim[n].numExtBief == b.branch1)
        || (rzo_.blocLims.ligneLim[n].numExtBief == b.branch2)) {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            rzo_.blocLims.ligneLim[n],
            "limite " + rzo_.blocLims.ligneLim[n].numLoi));
        rzo_.blocLims.ligneLim[n].numExtBief= 0;
        rzo_.blocLims.ligneLim[n].numLoi= 0;
        rzo_.blocLims.ligneLim[n].typLoi= 0;
        System.err.println("Suppression limite " + n);
        maj= true;
      }
    }
    if (maj) {
      prop_.firePropertyChange("limites", null, rzo_.blocLims.ligneLim);
    }
    return maj;
  }
  boolean majExtrRenommee(final int oldExtr, final int newExtr) {
    boolean maj= false;
    if ((rzo_ == null)
      || (rzo_.blocLims == null)
      || (rzo_.blocLims.ligneLim == null)) {
      System.err.println("LidoPH_Limite: Warning: lims null");
      return maj;
    }
    for (int n= 0; n < rzo_.nbLimi; n++) {
      if (rzo_.blocLims.ligneLim[n].numExtBief == oldExtr) {
        rzo_.blocLims.ligneLim[n].numExtBief= newExtr;
        fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            rzo_.blocLims.ligneLim[n],
            "limite " + rzo_.blocLims.ligneLim[n].numLoi));
        maj= true;
      }
    }
    return maj;
  }
}
