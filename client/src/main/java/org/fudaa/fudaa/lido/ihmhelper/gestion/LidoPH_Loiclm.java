/*
 * @file         LidoPH_Loiclm.java
 * @creation     1999-10-10
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimPointLigneCLM;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Loiclm                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoPH_Loiclm extends LidoPH_Base {
  public final static int TARAGE= LidoResource.LIMITE.TARAGE;
  public final static int LIMNI= LidoResource.LIMITE.LIMNI;
  public final static int MAREE= LidoResource.LIMITE.MAREE;
  public final static int HYDRO= LidoResource.LIMITE.HYDRO;
  private SParametresCLM clm_;
  private SParametresCondLimBlocCLM loiPrec;
  LidoPH_Loiclm(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    loiPrec= null;
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoPH_Loiclm: Warning: passing null CLM to constructor");
    }
  }
  public SParametresCondLimBlocCLM nouveauLoiclm(final int pos) {
    if ((clm_ == null) || (clm_.condLimites == null)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return null;
    }
    final SParametresCondLimBlocCLM[] lims= clm_.condLimites;
    final SParametresCondLimBlocCLM nouv= new SParametresCondLimBlocCLM();
    // A FAIRE: gerer intelligemment numExtBief
    nouv.numCondLim= getNumeroLoiclmLibre();
    nouv.description= "LOI LIMITE";
    nouv.nbPoints= 0;
    nouv.point= new SParametresCondLimPointLigneCLM[0];
    final SParametresCondLimBlocCLM[] nouvLim=
      new SParametresCondLimBlocCLM[lims.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvLim[i]= lims[i];
    }
    nouvLim[pos]= nouv;
    for (int i= pos; i < lims.length; i++) {
      nouvLim[i + 1]= lims[i];
    }
    clm_.condLimites= nouvLim;
    clm_.nbCondLim++;
    prop_.firePropertyChange("loiclm", lims, nouvLim);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        nouv,
        "loiclm " + nouv.numCondLim));
    return nouv;
  }
  public void supprimeLoiclm(final SParametresCondLimBlocCLM loi) {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.nbCondLim == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return;
    }
    final SParametresCondLimBlocCLM[] lois= clm_.condLimites;
    clm_.condLimites= new SParametresCondLimBlocCLM[lois.length - 1];
    int n= 0;
    for (int i= 0; i < lois.length; i++) {
      if (lois[i] != loi) {
        clm_.condLimites[n++]= lois[i];
      }
    }
    clm_.nbCondLim= clm_.condLimites.length;
    // remise a 0 des numLoi des Apports qui avaient celle-la...
    ph_.APPORT().loiclmSupprimee(loi);
    // remise a 0 des numLoi des CL qui avaient celle-la...
    ph_.LIMITE().loiclmSupprimee(loi);
    prop_.firePropertyChange("loiclm", lois, clm_.condLimites);
    fireParamStructDeleted(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        loi,
        "loiclm " + loi.numCondLim));
  }
  public SParametresCondLimBlocCLM[] supprimeSelection(final SParametresCondLimBlocCLM[] sel) {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.nbCondLim == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresCondLimBlocCLM[] lims= clm_.condLimites;
    final SParametresCondLimBlocCLM[] nouvLims=
      new SParametresCondLimBlocCLM[lims.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < lims.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (lims[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvLims[n++]= lims[i]; // pas trouve
      } else {
        // remise a 0 des numLoi des Apports qui avaient celle-la...
        ph_.APPORT().loiclmSupprimee(lims[i]);
        // remise a 0 des numLoi des CL qui avaient celle-la...
        ph_.LIMITE().loiclmSupprimee(lims[i]);
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            lims[i],
            "loiclm " + lims[i].numCondLim));
      }
    }
    clm_.condLimites= nouvLims;
    clm_.nbCondLim= nouvLims.length;
    // reclassement des indices ?
    prop_.firePropertyChange("loiclm", lims, nouvLims);
    return sel;
  }
  public synchronized void importeLois(final SParametresCondLimBlocCLM[] lois) {
    if ((clm_ == null)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return;
    }
    if (clm_.condLimites == null) {
      clm_.condLimites= new SParametresCondLimBlocCLM[0];
      clm_.nbCondLim= 0;
    }
    final SParametresCondLimBlocCLM[] old= clm_.condLimites;
    final SParametresCondLimBlocCLM[] nouv=
      new SParametresCondLimBlocCLM[clm_.nbCondLim + lois.length];
    for (int i= 0; i < clm_.nbCondLim; i++) {
      nouv[i]= old[i];
    }
    int num= getNumeroLoiclmLibre();
    for (int i= 0; i < lois.length; i++) {
      nouv[i + clm_.nbCondLim]= lois[i];
      nouv[i + clm_.nbCondLim].numCondLim= num++;
      fireParamStructCreated(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.CLM,
          lois[i],
          "loiclm " + lois[i].numCondLim));
    }
    clm_.nbCondLim= nouv.length;
    clm_.condLimites= nouv;
    prop_.firePropertyChange("loiclm", old, nouv);
  }
  public void changeTypeLoi(final SParametresCondLimBlocCLM loi, final int typLoi) {
    loi.typLoi= typLoi;
    fireParamStructModified(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        loi,
        "loiclm " + loi.numCondLim));
    ph_.APPORT().changeTypeLoi(loi);
    ph_.LIMITE().changeTypeLoi(loi);
  }
  public void nouveauPoint(final SParametresCondLimBlocCLM cl, int pos) {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.apport == null)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return;
    }
    final SParametresCondLimPointLigneCLM[] points= cl.point;
    if ((pos > points.length) || (pos < 0)) {
      pos= points.length;
    }
    cl.point= new SParametresCondLimPointLigneCLM[points.length + 1];
    for (int i= 0; i < pos; i++) {
      cl.point[i]= points[i];
    }
    cl.point[pos]= new SParametresCondLimPointLigneCLM();
    for (int i= pos; i < points.length; i++) {
      cl.point[i + 1]= points[i];
    }
    cl.nbPoints++;
    if (loiPrec != cl) {
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.CLM,
          cl,
          "loiclm " + cl.numCondLim));
    }
    loiPrec= cl;
  }
  public void supprimePoints(final SParametresCondLimBlocCLM cl, final int[] indices) {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.apport == null)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return;
    }
    final SParametresCondLimPointLigneCLM[] points= cl.point;
    final Vector newPoints= new Vector(points.length);
    boolean trouve= false;
    for (int i= 0; i < points.length; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (i == indices[j]) {
          trouve= true;
          break;
        } 
          //trouve= false;
      }
      if (!trouve) {
        newPoints.add(points[i]);
      }
    }
    cl.point= new SParametresCondLimPointLigneCLM[newPoints.size()];
    for (int i= 0; i < newPoints.size(); i++) {
      cl.point[i]= (SParametresCondLimPointLigneCLM)newPoints.get(i);
    }
    cl.nbPoints= cl.point.length;
    if (loiPrec != cl) {
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.CLM,
          cl,
          "loiclm " + cl.numCondLim));
    }
    loiPrec= cl;
  }
  public boolean verifiePermanent(final SParametresCondLimBlocCLM loi) {
    if ((loi == null) || (loi.point == null) || (loi.point.length == 0)) {
      return true;
    }
    boolean res= true;
    switch (loi.typLoi) {
      case TARAGE :
        {
          res= false;
          break;
        }
      case HYDRO :
        {
          final double q0= loi.point[0].qLim;
          for (int i= 1; i < loi.point.length; i++) {
            if (loi.point[i].qLim != q0) {
              res= false;
              break;
            }
          }
          break;
        }
      case LIMNI :
        {
          final double z0= loi.point[0].zLim;
          for (int i= 1; i < loi.point.length; i++) {
            if (loi.point[i].zLim != z0) {
              res= false;
              break;
            }
          }
          break;
        }
    }
    return res;
  }
  public double getTempsInitial() {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.nbCondLim == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return 1.;
    }
    double res= Double.POSITIVE_INFINITY;
    boolean found= false;
    //Ancienne version
    /* for(int i=0; i<clm_.nbCondLim; i++) {
      if( (clm_.condLimites[i].typLoi==LIMNI)||
          (clm_.condLimites[i].typLoi==MAREE)||
          (clm_.condLimites[i].typLoi==HYDRO) ) {
        for(int p=0; p<clm_.condLimites[i].nbPoints; p++) {
          if( clm_.condLimites[i].point[p].tLim<res ) {
            res=clm_.condLimites[i].point[p].tLim;
            found=true;
          }
        }
      }
    } */
    //Fin Ancienne version debut nouvelle version
    final SParametresCondLimBlocCLM[] loisUtilisees= ph_.LIMITE().getLoisLimites();
    if ((loisUtilisees == null) || (loisUtilisees.length == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: aucune loi utilisee");
      return 1.;
    }
    System.out.println(
      "loi utilisees= " + loisUtilisees.length + "/" + clm_.nbCondLim);
    for (int i= loisUtilisees.length - 1; i >= 0; i--) {
      if ((loisUtilisees[i].typLoi == LIMNI)
        || (loisUtilisees[i].typLoi == MAREE)
        || (loisUtilisees[i].typLoi == HYDRO)) {
        for (int p= loisUtilisees[i].nbPoints - 1; p >= 0; p--) {
          if (loisUtilisees[i].point[p].tLim < res) {
            res= loisUtilisees[i].point[p].tLim;
            found= true;
          }
        }
      }
    }
    //fin nouvelle version
    if (!found) {
      res= 1.;
    }
    return res;
  }
  public double getTempsFinal() {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.nbCondLim == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return -1.;
    }
    double res= Double.NEGATIVE_INFINITY;
    boolean found= false;
    //Ancienne version
    /*
    for(int i=0; i<clm_.nbCondLim; i++) {
      if( (clm_.condLimites[i].typLoi==LIMNI)||
          (clm_.condLimites[i].typLoi==MAREE)||
          (clm_.condLimites[i].typLoi==HYDRO) ) {
        for(int p=0; p<clm_.condLimites[i].nbPoints; p++) {
          if( clm_.condLimites[i].point[p].tLim>res ) {
            res=clm_.condLimites[i].point[p].tLim;
            found=true;
          }
        }
      }
    }
    */
    //Fin Ancienne version debut nouvelle version
    final SParametresCondLimBlocCLM[] loisUtilisees= ph_.LIMITE().getLoisLimites();
    if ((loisUtilisees == null) || (loisUtilisees.length == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: aucune loi utilisee");
      return 1.;
    }
    for (int i= loisUtilisees.length - 1; i >= 0; i--) {
      if ((loisUtilisees[i].typLoi == LIMNI)
        || (loisUtilisees[i].typLoi == MAREE)
        || (loisUtilisees[i].typLoi == HYDRO)) {
        for (int p= loisUtilisees[i].nbPoints - 1; p >= 0; p--) {
          if (loisUtilisees[i].point[p].tLim > res) {
            res= loisUtilisees[i].point[p].tLim;
            found= true;
          }
        }
      }
    }
    //fin nouvelle version
    if (!found) {
      res= -1.;
    }
    return res;
  }
  public double getMaxZLimni() {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.nbCondLim == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return 0.;
    }
    double res= Double.NEGATIVE_INFINITY;
    boolean found= false;
    final int nbCl= clm_.nbCondLim;
    for (int i= 0; i < nbCl; i++) {
      if ((clm_.condLimites[i].typLoi == LIMNI)
        || (clm_.condLimites[i].typLoi == MAREE)) {
        final int nbPoints= clm_.condLimites[i].nbPoints;
        for (int p= 0; p < nbPoints; p++) {
          final SParametresCondLimPointLigneCLM pt= clm_.condLimites[i].point[p];
          if (pt == null) {
            System.err.println("!!!! Point ligne Condition Limites null");
          } else {
            if (pt.zLim > res) {
              res= clm_.condLimites[i].point[p].zLim;
              found= true;
            }
          }
        }
      }
    }
    if (!found) {
      res= 0.;
    }
    return res;
  }
  public int getNbrCondByType(final int type) {
    if ((clm_ == null)
      || (clm_.condLimites == null)
      || (clm_.nbCondLim == 0)) {
      System.err.println("LidoPH_Loiclm: Warning: lims null");
      return 0;
    }
    int res= 0;
    for (int i= 0; i < clm_.nbCondLim; i++) {
      if ((clm_.condLimites[i].typLoi == type)) {
        res++;
      }
    }
    return res;
  }
  public boolean verifieCote(final SParametresCondLimBlocCLM loiclm, final double cote) {
    boolean res= true;
    switch (loiclm.typLoi) {
      case LidoResource.LIMITE.HYDRO :
        break;
      case LidoResource.LIMITE.LIMNI :
      case LidoResource.LIMITE.MAREE :
      case LidoResource.LIMITE.TARAGE :
        {
          for (int i= 0; i < loiclm.point.length; i++) {
            if (loiclm.point[i].zLim <= cote) {
              res= false;
            }
          }
          break;
        }
    }
    return res;
  }
  public boolean verifieTempsNP(final SParametresCondLimBlocCLM loiclm) {
    boolean res= true;
    switch (loiclm.typLoi) {
      case LidoResource.LIMITE.TARAGE :
        break;
      case LidoResource.LIMITE.HYDRO :
      case LidoResource.LIMITE.MAREE :
      case LidoResource.LIMITE.LIMNI :
        {
          for (int i= 1; i < loiclm.point.length; i++) {
            if (loiclm.point[i].tLim <= loiclm.point[i - 1].tLim) {
              res= false;
            }
          }
          break;
        }
    }
    return res;
  }
  /*  
    public int getTypeLoi(SParametresCondLimBlocCLM loi)
    {
      if( (clm_==null)||(clm_.condLimites==null)||(clm_.apport==null) ) {
        System.err.println("LidoPH_Limite: Warning: lims null");
        return 0;
      }
      boolean hasT=false;
      boolean hasZ=false;
      boolean hasQ=false;
      for(int i=0; i<loi.point.length; i++) {
        if( loi.point[i].tLim!=0. ) hasT=true;
        if( loi.point[i].zLim!=0. ) hasZ=true;
        if( loi.point[i].qLim!=0. ) hasQ=true;
      }
      if( (!hasT)&&(!hasZ)&&(!hasQ) ) return 0;
           if( !hasT ) return TARAGE;
      else if( !hasZ ) return HYDRO;
      else if( !hasQ ) return LIMNI;
      else return 0;
    }
    */
  public SParametresCondLimBlocCLM creeLoiCLM(
    final double[][] valeurs,
    final int type,
    final String titre) {
    final SParametresCondLimBlocCLM loi= new SParametresCondLimBlocCLM();
    if (type == LIMNI) {
      loi.typLoi= LIMNI;
    } else if (type == HYDRO) {
      loi.typLoi= HYDRO;
    } else if (type == TARAGE) {
      loi.typLoi= TARAGE;
    } else {
      return loi;
    }
    loi.description= titre;
    loi.nbPoints= valeurs.length;
    loi.point= new SParametresCondLimPointLigneCLM[loi.nbPoints];
    for (int i= 0; i < loi.point.length; i++) {
      loi.point[i]= new SParametresCondLimPointLigneCLM();
      if (type == LIMNI) {
        loi.point[i].tLim= valeurs[i][0];
        loi.point[i].zLim= valeurs[i][1];
      } else if (type == HYDRO) {
        loi.point[i].tLim= valeurs[i][0];
        loi.point[i].qLim= valeurs[i][1];
      } else if (type == TARAGE) {
        loi.point[i].zLim= valeurs[i][0];
        loi.point[i].qLim= valeurs[i][1];
      }
    }
    return loi;
  }
  private int getNumeroLoiclmLibre() {
    int maxNum= 0;
    for (int i= 0; i < clm_.nbCondLim; i++) {
      if (clm_.condLimites[i].numCondLim > maxNum) {
        maxNum= clm_.condLimites[i].numCondLim;
      }
    }
    return maxNum + 1;
  }
}
