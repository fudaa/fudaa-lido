/*
 * @file         LidoPH_Loisng.java
 * @creation     1999-10-13
 * @modification $Date: 2006-09-19 15:05:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import org.fudaa.dodico.corba.lido.SParametresSNG;
import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Loisng                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:02 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPH_Loisng extends LidoPH_Base {
  private final static int TARAGE_AMONT= LidoResource.SINGULARITE.TARAGE_AMONT;
  private final static int TARAGE_AVAL= LidoResource.SINGULARITE.TARAGE_AVAL;
  private final static int LIMNI_AMONT= LidoResource.SINGULARITE.LIMNI_AMONT;
  private final static int SEUIL_GEOM= LidoResource.SINGULARITE.SEUIL_GEOM;
  private final static int SEUIL_NOYE= LidoResource.SINGULARITE.SEUIL_NOYE;
  private final static int SEUIL_DENOYE= LidoResource.SINGULARITE.SEUIL_DENOYE;
  private static final int ITYPE= LidoResource.SINGULARITE.ITYPE;
  private static final int INBPOINT= LidoResource.SINGULARITE.ICOURBE.INBPOINT;
  private static final int IHT= LidoResource.SINGULARITE.ICOURBE.IHT;
  // TARAGE: cotes
  private static final int IHL= LidoResource.SINGULARITE.ICOURBE.IHL; // LIMNI: cotes
  private static final int IQT= LidoResource.SINGULARITE.ICOURBE.IQT;
  // TARAGE: debits
  private static final int ITL= LidoResource.SINGULARITE.ICOURBE.ITL; // LIMNI: temps
  private static final int IYS= LidoResource.SINGULARITE.ICOURBE.IYS;
  // SEUIL: abs crete
  private static final int IZS= LidoResource.SINGULARITE.ICOURBE.IZS;
  // SEUIL: cotes crete
  private static final int DEBIT= LidoResource.SINGULARITE.DEBIT;
  private static final int COTEAVAL= LidoResource.SINGULARITE.COTEAVAL;
  private SParametresSNG sng_;
  private SParametresSingBlocSNG loiPrec;
  LidoPH_Loisng(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    loiPrec= null;
    sng_= (SParametresSNG)p.getParam(LidoResource.SNG);
    if (sng_ == null) {
      System.err.println(
        "LidoPH_Loisng: Warning: passing null SNG to constructor");
    }
  }
  public SParametresSingBlocSNG nouveauLoisng(final int pos) {
    if ((sng_ == null) || (sng_.singularites == null)) {
      System.err.println("LidoPH_Loisng: Warning: singularites null");
      return null;
    }
    final SParametresSingBlocSNG[] lims= sng_.singularites;
    final SParametresSingBlocSNG nouv= new SParametresSingBlocSNG();
    // A FAIRE: gerer intelligemment numExtBief
    nouv.numSing= getNumeroLoisngLibre();
    nouv.titre= "SINGULARITE " + nouv.numSing;
    // gerer ns3 et compagnie...
    nouv.tabParamEntier= new int[sng_.nbMaxParam];
    nouv.tabParamReel= new double[sng_.nbMaxParam];
    nouv.ns3= 2;
    nouv.tabDonLois= new double[nouv.ns3][sng_.nbValTD];
    final SParametresSingBlocSNG[] nouvLim=
      new SParametresSingBlocSNG[lims.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvLim[i]= lims[i];
    }
    nouvLim[pos]= nouv;
    for (int i= pos; i < lims.length; i++) {
      nouvLim[i + 1]= lims[i];
    }
    sng_.singularites= nouvLim;
    sng_.nbSing++;
    prop_.firePropertyChange("loisng", lims, nouvLim);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.SNG,
        nouv,
        "loisng " + nouv.numSing));
    return nouv;
  }
  public void supprimeLoisng(final SParametresSingBlocSNG loi) {
    if ((sng_ == null) || (sng_.singularites == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return;
    }
    final SParametresSingBlocSNG[] lois= sng_.singularites;
    sng_.singularites= new SParametresSingBlocSNG[lois.length - 1];
    int n= 0;
    for (int i= 0; i < lois.length; i++) {
      if (lois[i] != loi) {
        sng_.singularites[n++]= lois[i];
      }
    }
    sng_.nbSing= sng_.singularites.length;
    // remise a 0 des numLoi des Sings du RZO qui avaient celle-la...
    ph_.SINGULARITE().loisngSupprimee(loi);
    prop_.firePropertyChange("loisng", lois, sng_.singularites);
    fireParamStructDeleted(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.SNG,
        loi,
        "loisng " + loi.numSing));
  }
  public void changeTypeLoi(final SParametresSingBlocSNG loi, final int typLoi) {}
  public void nouveauPoint(final SParametresSingBlocSNG cl, final int varACreer, final int pos) {
    if ((sng_ == null) || (sng_.singularites == null) || (cl == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return;
    }
    switch (cl.tabParamEntier[ITYPE]) {
      case TARAGE_AMONT :
        nouveauPointCourbe(cl, IQT, IHT, INBPOINT, pos);
        break;
      case TARAGE_AVAL :
        nouveauPointCourbe(cl, IQT, IHT, INBPOINT, pos);
        break;
      case LIMNI_AMONT :
        nouveauPointCourbe(cl, ITL, IHL, INBPOINT, pos);
        break;
      case SEUIL_GEOM :
        nouveauPointCourbe(cl, IYS, IZS, INBPOINT, pos);
        break;
      case SEUIL_DENOYE :
        nouveauPointCourbe(
          cl,
          LidoResource.SINGULARITE.ISEUIL_DENOYE.IDEBIT,
          LidoResource.SINGULARITE.ISEUIL_DENOYE.ICOTAMONT,
          LidoResource.SINGULARITE.ISEUIL_DENOYE.INBDEBIT,
          pos);
        break;
      case SEUIL_NOYE :
        {
          // seuil noye
          if ((varACreer & DEBIT) == DEBIT) {
            nouveauPointSeuilnoye(
              cl,
              LidoResource.SINGULARITE.ISEUIL_NOYE.IDEBIT,
              LidoResource.SINGULARITE.ISEUIL_NOYE.INBDEBIT);
          }
          if ((varACreer & COTEAVAL) == COTEAVAL) {
            nouveauPointSeuilnoye(
              cl,
              LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAVAL,
              LidoResource.SINGULARITE.ISEUIL_NOYE.INBCOTAVAL);
          }
          break;
        }
      default :
        return;
    }
    if (loiPrec != cl) {
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.SNG,
          cl,
          "loisng " + cl.numSing));
    }
    loiPrec= cl;
  }
  private void nouveauPointCourbe(
    final SParametresSingBlocSNG cl,
    final int IABS,
    final int IORD,
    final int _INBPOINT,
    int pos) {
    if (cl.tabParamEntier[_INBPOINT] >= sng_.nbValTD) {
      System.err.println(
        "taille maximale du tableau des lois atteinte (" + sng_.nbValTD + ")");
      return;
    }
    if ((pos < 0) || (pos > cl.tabParamEntier[_INBPOINT])) {
      pos= cl.tabParamEntier[_INBPOINT];
    }
    /*double tabDonLoisIABSTmp[]=cl.tabDonLois[IABS];
    double tabDonLoisIORDTmp[]=cl.tabDonLois[IORD];
    cl.tabDonLois[IABS]=new double[tabDonLoisIABSTmp.length+1];
    cl.tabDonLois[IORD]=new double[tabDonLoisIORDTmp.length+1];
    for(int i=0; i<tabDonLoisIABSTmp.length; i++) {
      cl.tabDonLois[IABS][i]=tabDonLoisIABSTmp[i];
      cl.tabDonLois[IORD][i]=tabDonLoisIORDTmp[i];
    }*/
    for (int i= cl.tabParamEntier[_INBPOINT]; i > pos; i--) {
      cl.tabDonLois[IABS][i]= cl.tabDonLois[IABS][i - 1];
      cl.tabDonLois[IORD][i]= cl.tabDonLois[IORD][i - 1];
    }
    cl.tabDonLois[IABS][pos]= 0.;
    cl.tabDonLois[IORD][pos]= 0.;
    cl.tabParamEntier[_INBPOINT]++;
    return;
  }
  private void nouveauPointSeuilnoye(
    final SParametresSingBlocSNG cl,
    final int IABS,
    final int _INBPOINT) {
    if (cl.tabParamEntier[_INBPOINT] >= sng_.nbValTD) {
      System.err.println(
        "taille maximale du tableau des lois atteinte (" + sng_.nbValTD + ")");
      return;
    }
    // COTE AVAL et DEBIT
    /*double coteAvalTmp[]=cl.tabDonLois[IABS];
    cl.tabDonLois[IABS]=new double[coteAvalTmp.length+1];
    for(int i=0; i<coteAvalTmp.length; i++) {
      cl.tabDonLois[IABS][i]=coteAvalTmp[i];
    }*/
    cl.tabDonLois[IABS][cl.tabParamEntier[_INBPOINT]]= 0.;
    // COTE AMONT
    if (IABS == LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAVAL) {
      for (int i= LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET;
        i < cl.tabDonLois.length;
        i++) {
        /*double coteAmontTmp[]=cl.tabDonLois[i];
        cl.tabDonLois[i]=new double[coteAmontTmp.length+1];
        for(int j=0; j<coteAmontTmp.length; j++) {
          cl.tabDonLois[i][j]=coteAmontTmp[j];
        }
        cl.tabDonLois[i][coteAmontTmp.length]=0.;*/
        cl.tabDonLois[i][cl.tabParamEntier[_INBPOINT]]= 0.;
      }
    } else if (IABS == LidoResource.SINGULARITE.ISEUIL_NOYE.IDEBIT) {
      final double tabDonLoisTmp[][]= cl.tabDonLois;
      cl.tabDonLois= new double[tabDonLoisTmp.length + 1][];
      for (int i= 0; i < tabDonLoisTmp.length; i++) {
        cl.tabDonLois[i]= tabDonLoisTmp[i];
      }
      cl.tabDonLois[tabDonLoisTmp.length]= new double[sng_.nbValTD];
      cl.ns3++;
    }
    cl.tabParamEntier[_INBPOINT]++;
    return;
  }
  public void supprimePoints(
    final SParametresSingBlocSNG cl,
    final int[] indices,
    final int varASupprimer) {
    if ((sng_ == null) || (sng_.singularites == null) || (cl == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return;
    }
    switch (cl.tabParamEntier[ITYPE]) {
      case TARAGE_AMONT :
        supprimePointCourbe(cl, indices, IQT, IHT, INBPOINT);
        break;
      case TARAGE_AVAL :
        supprimePointCourbe(cl, indices, IQT, IHT, INBPOINT);
        break;
      case LIMNI_AMONT :
        supprimePointCourbe(cl, indices, ITL, IHL, INBPOINT);
        break;
      case SEUIL_GEOM :
        supprimePointCourbe(cl, indices, IYS, IZS, INBPOINT);
        break;
      case SEUIL_DENOYE :
        supprimePointCourbe(
          cl,
          indices,
          LidoResource.SINGULARITE.ISEUIL_DENOYE.IDEBIT,
          LidoResource.SINGULARITE.ISEUIL_DENOYE.ICOTAMONT,
          LidoResource.SINGULARITE.ISEUIL_DENOYE.INBDEBIT);
        break;
      case SEUIL_NOYE :
        {
          // seuil noye
          if ((varASupprimer & DEBIT) == DEBIT) {
            supprimePointSeuilnoye(
              cl,
              indices,
              LidoResource.SINGULARITE.ISEUIL_NOYE.IDEBIT,
              LidoResource.SINGULARITE.ISEUIL_NOYE.INBDEBIT);
          }
          if ((varASupprimer & COTEAVAL) == COTEAVAL) {
            supprimePointSeuilnoye(
              cl,
              indices,
              LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAVAL,
              LidoResource.SINGULARITE.ISEUIL_NOYE.INBCOTAVAL);
          }
          break;
        }
      default :
        return;
    }
    if (loiPrec != cl) {
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.SNG,
          cl,
          "loisng " + cl.numSing));
    }
    loiPrec= cl;
  }
  private void supprimePointCourbe(
    final SParametresSingBlocSNG cl,
    final int[] indices,
    final int IABS,
    final int IORD,
    final int _INBPOINT) {
    final double tabDonLoisIABSTmp[]= cl.tabDonLois[IABS];
    final double tabDonLoisIORDTmp[]= cl.tabDonLois[IORD];
    cl.tabDonLois[IABS]= new double[tabDonLoisIABSTmp.length];
    cl.tabDonLois[IORD]= new double[tabDonLoisIABSTmp.length];
    int n= 0;
    boolean trouve= false;
    for (int i= 0; i < cl.tabParamEntier[_INBPOINT]; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (i == indices[j]) {
          trouve= true;
          break;
        } //else
          trouve= false;
      }
      if (!trouve) {
        cl.tabDonLois[IABS][n]= tabDonLoisIABSTmp[i];
        cl.tabDonLois[IORD][n++]= tabDonLoisIORDTmp[i];
      }
    }
    cl.tabParamEntier[_INBPOINT]= n;
  }
  private void supprimePointSeuilnoye(
    final SParametresSingBlocSNG cl,
    final int[] indices,
    final int IABS,
    final int _INBPOINT) {
    /*      for(int i=0; i<cl.tabDonLois.length; i++) {
            for(int j=0; j<cl.tabParamEntier[INBPOINT]; j++)
              System.err.print(cl.tabDonLois[i][j]+" ");
            System.err.println("");
          }*/
    int nouvTaille= 0;
    // COTE AVAL et DEBIT
    final double tabDonLoisIABSTmp[]= cl.tabDonLois[IABS];
    cl.tabDonLois[IABS]= new double[tabDonLoisIABSTmp.length];
    int n= 0;
    boolean trouve= false;
    for (int i= 0; i < cl.tabParamEntier[_INBPOINT]; i++) {
      for (int j= 0; j < indices.length; j++) {
        if (i == indices[j]) {
          trouve= true;
          break;
        } //else
          trouve= false;
      }
      if (!trouve) {
        cl.tabDonLois[IABS][n++]= tabDonLoisIABSTmp[i];
      }
    }
    nouvTaille= n;
    // COTE AMONT
    if (IABS == LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAVAL) {
      for (int k= LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET;
        k < cl.tabDonLois.length;
        k++) {
        final double tabDonLoisTmp[]= cl.tabDonLois[k];
        cl.tabDonLois[k]= new double[tabDonLoisTmp.length];
        trouve= false;
        n= 0;
        for (int i= 0; i < cl.tabParamEntier[_INBPOINT]; i++) {
          for (int j= 0; j < indices.length; j++) {
            if (i == indices[j]) {
              trouve= true;
              break;
            } //else
              trouve= false;
          }
          if (!trouve) {
            cl.tabDonLois[k][n++]= tabDonLoisTmp[i];
          }
        }
      }
    } else if (IABS == LidoResource.SINGULARITE.ISEUIL_NOYE.IDEBIT) {
      final double tabDonLoisTmp[][]= cl.tabDonLois;
      cl.tabDonLois=
        new double[n + LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET][];
      for (int i= 0;
        i < LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET;
        i++) {
        cl.tabDonLois[i]= tabDonLoisTmp[i];
      }
      n= LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET;
      trouve= false;
      for (int i= 0; i < cl.tabParamEntier[_INBPOINT]; i++) {
        for (int j= 0; j < indices.length; j++) {
          if (i == indices[j]) {
            trouve= true;
            break;
          } //else
            trouve= false;
        }
        if (!trouve) {
          cl.tabDonLois[n++]=
            tabDonLoisTmp[i
              + LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET];
        }
      }
      cl.ns3= n + LidoResource.SINGULARITE.ISEUIL_NOYE.ICOTAMONTOFFSET;
      /*      System.err.println("");
            for(int i=0; i<cl.tabDonLois.length; i++) {
              for(int j=0; j<cl.tabParamEntier[INBPOINT]; j++)
                System.err.print(cl.tabDonLois[i][j]+" ");
              System.err.println("");
            }*/
    }
    cl.tabParamEntier[_INBPOINT]= nouvTaille;
    return;
  }
  public double getTempsInitial() {
    if ((sng_ == null) || (sng_.singularites == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return 1.;
    }
    double res= Double.POSITIVE_INFINITY;
    boolean found= false;
    for (int i= 0; i < sng_.nbSing; i++) {
      if (sng_.singularites[i].tabParamEntier[ITYPE] == LIMNI_AMONT) {
        for (int j= 0;
          j < sng_.singularites[i].tabParamEntier[INBPOINT];
          j++) {
          if (sng_.singularites[i].tabDonLois[ITL][j] < res) {
            res= sng_.singularites[i].tabDonLois[ITL][j];
            found= true;
          }
        }
      }
    }
    if (!found) {
      res= 1.;
    }
    return res;
  }
  public double getTempsFinal() {
    if ((sng_ == null) || (sng_.singularites == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return -1.;
    }
    double res= Double.NEGATIVE_INFINITY;
    boolean found= false;
    for (int i= 0; i < sng_.nbSing; i++) {
      if (sng_.singularites[i].tabParamEntier[ITYPE] == LIMNI_AMONT) {
        for (int j= 0;
          j < sng_.singularites[i].tabParamEntier[INBPOINT];
          j++) {
          if (sng_.singularites[i].tabDonLois[ITL][j] > res) {
            res= sng_.singularites[i].tabDonLois[ITL][j];
            found= true;
          }
        }
      }
    }
    if (!found) {
      res= -1.;
    }
    return res;
  }
  private int getNumeroLoisngLibre() {
    int maxNum= 0;
    for (int i= 0; i < sng_.nbSing; i++) {
      if (sng_.singularites[i].numSing > maxNum) {
        maxNum= sng_.singularites[i].numSing;
      }
    }
    return maxNum + 1;
  }
}
