/*
 * @file         LidoPH_Noeud.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresBiefNoeudLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Noeud                                  */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPH_Noeud extends LidoPH_Base {
  private SParametresRZO rzo_;
  LidoPH_Noeud(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoPH_Noeud: Warning: passing null RZO to constructor");
    }
  }
  public SParametresBiefNoeudLigneRZO nouveauNoeud(final int pos) {
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return null;
    }
    // A FAIRE: gerer les incoherences!!
    final SParametresBiefNoeudLigneRZO[] noeuds= rzo_.blocNoeuds.ligneNoeud;
    final SParametresBiefNoeudLigneRZO nouv= new SParametresBiefNoeudLigneRZO();
    nouv.noeud= new int[5];
    final SParametresBiefNoeudLigneRZO[] nouvNoeud=
      new SParametresBiefNoeudLigneRZO[noeuds.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvNoeud[i]= noeuds[i];
    }
    nouvNoeud[pos]= nouv;
    for (int i= pos; i < noeuds.length; i++) {
      nouvNoeud[i + 1]= noeuds[i];
    }
    rzo_.blocNoeuds.ligneNoeud= nouvNoeud;
    rzo_.nbNoeud++;
    prop_.firePropertyChange("noeuds", noeuds, nouvNoeud);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        nouv,
        "noeud ["
          + nouv.noeud[0]
          + CtuluLibString.VIR
          + nouv.noeud[1]
          + CtuluLibString.VIR
          + nouv.noeud[2]
          + CtuluLibString.VIR
          + nouv.noeud[3]
          + CtuluLibString.VIR
          + nouv.noeud[4]
          + "]"));
    return nouv;
  }
  public SParametresBiefNoeudLigneRZO nouveauNoeud(final int[] extr) {
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return null;
    }
    final SParametresBiefNoeudLigneRZO nouv=
      nouveauNoeud(rzo_.blocNoeuds.ligneNoeud.length);
    for (int i= 0; i < Math.min(extr.length, 5); i++) {
      nouv.noeud[i]= extr[i];
    }
    prop_.firePropertyChange(
      "noeuds",
      rzo_.blocNoeuds.ligneNoeud,
      rzo_.blocNoeuds.ligneNoeud);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        nouv,
        "noeud ["
          + nouv.noeud[0]
          + CtuluLibString.VIR
          + nouv.noeud[1]
          + CtuluLibString.VIR
          + nouv.noeud[2]
          + CtuluLibString.VIR
          + nouv.noeud[3]
          + CtuluLibString.VIR
          + nouv.noeud[4]
          + "]"));
    return nouv;
  }
  /*private SParametresBiefNoeudLigneRZO supprimeNoeud(SParametresBiefNoeudLigneRZO p) {
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return null;
    }
    if (p == null)
      return null;
    SParametresBiefNoeudLigneRZO[] noeuds= rzo_.blocNoeuds.ligneNoeud;
    SParametresBiefNoeudLigneRZO[] nouvNoeuds=
      new SParametresBiefNoeudLigneRZO[noeuds.length - 1];
    int i= 0;
    for (i= 0; i < noeuds.length; i++) {
      if (p == noeuds[i])
        break;
    }
    // pas trouve
    if (i >= noeuds.length)
      return null;
    int n= 0;
    for (i= 0; i < noeuds.length; i++) {
      if (p != noeuds[i])
        nouvNoeuds[n++]= noeuds[i];
    }
    rzo_.blocNoeuds.ligneNoeud= nouvNoeuds;
    rzo_.nbNoeud= nouvNoeuds.length;
    prop_.firePropertyChange("noeuds", noeuds, nouvNoeuds);
    fireParamStructDeleted(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        p,
        "noeud ["
          + p.noeud[0]
          + ","
          + p.noeud[1]
          + ","
          + p.noeud[2]
          + ","
          + p.noeud[3]
          + ","
          + p.noeud[4]
          + "]"));
    return p;
  }*/
  /*private SParametresBiefNoeudLigneRZO[] new_supprimeSelection(SParametresBiefNoeudLigneRZO[] sel) {
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return null;
    }
    if (sel == null)
      return null;
    Vector v= new Vector();
    for (int i= 0; i < sel.length; i++)
      v.add(supprimeNoeud(sel[i]));
    SParametresBiefNoeudLigneRZO[] res=
      new SParametresBiefNoeudLigneRZO[v.size()];
    for (int i= 0; i < v.size(); i++)
      res[i]= (SParametresBiefNoeudLigneRZO)v.get(i);
    return res;
  }*/
  public SParametresBiefNoeudLigneRZO[] supprimeSelection(final SParametresBiefNoeudLigneRZO[] sel) {
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresBiefNoeudLigneRZO[] noeuds= rzo_.blocNoeuds.ligneNoeud;
    final SParametresBiefNoeudLigneRZO[] nouvNoeuds=
      new SParametresBiefNoeudLigneRZO[noeuds.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < noeuds.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (noeuds[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvNoeuds[n++]= noeuds[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            noeuds[i],
            "noeud ["
              + noeuds[i].noeud[0]
              + CtuluLibString.VIR
              + noeuds[i].noeud[1]
              + CtuluLibString.VIR
              + noeuds[i].noeud[2]
              + CtuluLibString.VIR
              + noeuds[i].noeud[3]
              + CtuluLibString.VIR
              + noeuds[i].noeud[4]
              + "]"));
      }
    }
    rzo_.blocNoeuds.ligneNoeud= nouvNoeuds;
    rzo_.nbNoeud= nouvNoeuds.length;
    prop_.firePropertyChange("noeuds", noeuds, nouvNoeuds);
    return sel;
  }
  boolean majBiefSupprime(final SParametresBiefSituLigneRZO b) {
    boolean maj= false;
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return maj;
    }
    if (b == null) {
      return maj;
    }
    for (int n= 0; n < rzo_.nbNoeud; n++) {
      int nbN= 0;
      for (int m= 0; m < rzo_.blocNoeuds.ligneNoeud[n].noeud.length; m++) {
        if ((rzo_.blocNoeuds.ligneNoeud[n].noeud[m] == b.branch1)
          || (rzo_.blocNoeuds.ligneNoeud[n].noeud[m] == b.branch2)) {
          rzo_.blocNoeuds.ligneNoeud[n].noeud[m]= 0;
          System.err.println("Maj noeud " + n + " (extr " + m + ")");
          fireParamStructModified(
            new FudaaParamEvent(
              this,
              0,
              LidoResource.RZO,
              rzo_.blocNoeuds.ligneNoeud[n],
              "noeud ["
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[0]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[1]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[2]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[3]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[4]
                + "]"));
          maj= true;
        }
        if (rzo_.blocNoeuds.ligneNoeud[n].noeud[m] != 0) {
          nbN++;
        }
      }
      // suppression du noeud
      if (nbN < 2) {
        supprimeSelection(
          new SParametresBiefNoeudLigneRZO[] { rzo_.blocNoeuds.ligneNoeud[n] });
        System.err.println("Suppression noeud " + n);
        maj= true;
      } else {
        // reorganisation du noeud
        int r= 0;
        final int[] nouvNoeud= new int[rzo_.blocNoeuds.ligneNoeud[n].noeud.length];
        for (int m= 0; m < rzo_.blocNoeuds.ligneNoeud[n].noeud.length; m++) {
          if (rzo_.blocNoeuds.ligneNoeud[n].noeud[m] != 0) {
            nouvNoeud[r++]= rzo_.blocNoeuds.ligneNoeud[n].noeud[m];
          }
        }
        rzo_.blocNoeuds.ligneNoeud[n].noeud= nouvNoeud;
      }
    }
    if (maj) {
      prop_.firePropertyChange("noeuds", null, rzo_.blocNoeuds.ligneNoeud);
    }
    return maj;
  }
  boolean majExtrRenommee(final int oldExtr, final int newExtr) {
    boolean maj= false;
    if ((rzo_ == null)
      || (rzo_.blocNoeuds == null)
      || (rzo_.blocNoeuds.ligneNoeud == null)) {
      System.err.println("LidoPH_Noeud: Warning: noeuds null");
      return maj;
    }
    for (int n= 0; n < rzo_.nbNoeud; n++) {
      for (int m= 0; m < rzo_.blocNoeuds.ligneNoeud[n].noeud.length; m++) {
        if (rzo_.blocNoeuds.ligneNoeud[n].noeud[m] == oldExtr) {
          rzo_.blocNoeuds.ligneNoeud[n].noeud[m]= newExtr;
          fireParamStructModified(
            new FudaaParamEvent(
              this,
              0,
              LidoResource.RZO,
              rzo_.blocNoeuds.ligneNoeud[n],
              "noeud ["
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[0]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[1]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[2]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[3]
                + CtuluLibString.VIR
                + rzo_.blocNoeuds.ligneNoeud[n].noeud[4]
                + "]"));
          maj= true;
        }
      }
    }
    return maj;
  }
}
