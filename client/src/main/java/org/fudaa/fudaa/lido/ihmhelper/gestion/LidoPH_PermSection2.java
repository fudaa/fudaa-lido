/*
 * @file         LidoPH_PermSection2.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SParametresSerieBlocCAL;
import org.fudaa.dodico.corba.lido.SParametresSerieLigneCAL;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_PermSection2                          */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPH_PermSection2 extends LidoPH_Base {
  private SParametresCAL cal_;
  private SParametresPRO pro_;
  LidoPH_PermSection2(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoPH_PermSection2: Warning: passing null CAL to constructor");
    }
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoPH_PermSection2: Warning: passing null PRO to constructor");
    }
  }
  public SParametresSerieLigneCAL nouveauPermSection2(final int pos) {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.series == null)
      || (cal_.sections.series.ligne == null)) {
      System.err.println("LidoPH_PermSection2: Warning: sections null");
      return null;
    }
    final SParametresSerieLigneCAL[] pers= cal_.sections.series.ligne;
    final SParametresSerieLigneCAL nouv= new SParametresSerieLigneCAL();
    // A FAIRE : gerer intelligemment les valeurs par defaut
    nouv.absDebBief= 0.;
    nouv.absFinBief= 0.;
    final SParametresSerieLigneCAL[] nouvPer=
      new SParametresSerieLigneCAL[pers.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvPer[i]= pers[i];
    }
    nouvPer[pos]= nouv;
    for (int i= pos; i < pers.length; i++) {
      nouvPer[i + 1]= pers[i];
    }
    cal_.sections.series.ligne= nouvPer;
    cal_.sections.series.nbSeries++;
    prop_.firePropertyChange("permSection2s", pers, nouvPer);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CAL,
        nouv,
        "section " + nouv.absDebBief + CtuluLibString.VIR + nouv.absFinBief + "]"));
    return nouv;
  }
  /*private SParametresSerieLigneCAL supprimeSection(SParametresSerieLigneCAL p) {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.series == null)
      || (cal_.sections.series.ligne == null)
      || (cal_.sections.series.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection2: Warning: sections null");
      return null;
    }
    if (p == null)
      return null;
    SParametresSerieLigneCAL[] pers= cal_.sections.series.ligne;
    SParametresSerieLigneCAL[] nouvPers=
      new SParametresSerieLigneCAL[pers.length - 1];
    int i= 0;
    for (i= 0; i < pers.length; i++) {
      if (p == pers[i])
        break;
    }
    // pas trouve
    if (i >= pers.length)
      return null;
    int n= 0;
    for (i= 0; i < pers.length; i++) {
      if (p != pers[i])
        nouvPers[n++]= pers[i];
    }
    cal_.sections.series.ligne= nouvPers;
    cal_.sections.series.nbSeries= nouvPers.length;
    prop_.firePropertyChange("permSection2s", pers, nouvPers);
    fireParamStructDeleted(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CAL,
        p,
        "section [" + p.absDebBief + "," + p.absFinBief + "]"));
    return p;
  }*/
  /*private SParametresSerieLigneCAL[] new_supprimeSelection(SParametresSerieLigneCAL[] sel) {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.series == null)
      || (cal_.sections.series.ligne == null)
      || (cal_.sections.series.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection2: Warning: sections null");
      return null;
    }
    if (sel == null)
      return null;
    Vector v= new Vector();
    for (int i= 0; i < sel.length; i++)
      v.add(supprimeSection(sel[i]));
    SParametresSerieLigneCAL[] res= new SParametresSerieLigneCAL[v.size()];
    for (int i= 0; i < v.size(); i++)
      res[i]= (SParametresSerieLigneCAL)v.get(i);
    return res;
  }*/
  public SParametresSerieLigneCAL[] supprimeSelection(final SParametresSerieLigneCAL[] sel) {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.series == null)
      || (cal_.sections.series.ligne == null)
      || (cal_.sections.series.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection2: Warning: sections null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresSerieLigneCAL[] pers= cal_.sections.series.ligne;
    final SParametresSerieLigneCAL[] nouvPers=
      new SParametresSerieLigneCAL[pers.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < pers.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (pers[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvPers[n++]= pers[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CAL,
            pers[i],
            "section " + pers[i].absDebBief + CtuluLibString.VIR + pers[i].absFinBief + "]"));
      }
    }
    cal_.sections.series.ligne= nouvPers;
    cal_.sections.series.nbSeries= nouvPers.length;
    prop_.firePropertyChange("permSection2s", pers, nouvPers);
    return sel;
  }
  public void remplitAvecProfils(final SParametresSerieBlocCAL series) {
    if ((series == null) || (series.ligne == null)) {
      return;
    }
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_PermSection2: Warning: profils null");
      return;
    }
    final SParametresSerieLigneCAL[] oldPers= series.ligne;
    final Vector lignes= new Vector();
    for (int i= 0; i < pro_.nbProfils - 1; i++) {
      if (ph_.BIEF().getBiefContenantAbscisse(pro_.profilsBief[i].abscisse)
        == ph_.BIEF().getBiefContenantAbscisse(
          pro_.profilsBief[i + 1].abscisse)) {
        final SParametresSerieLigneCAL ligne= new SParametresSerieLigneCAL();
        ligne.absDebBief= pro_.profilsBief[i].abscisse;
        ligne.absFinBief= pro_.profilsBief[i + 1].abscisse;
        lignes.add(ligne);
      }
    }
    series.ligne= new SParametresSerieLigneCAL[lignes.size()];
    series.nbSeries= series.ligne.length;
    for (int i= 0; i < series.nbSeries; i++) {
      series.ligne[i]= (SParametresSerieLigneCAL)lignes.get(i);
      fireParamStructCreated(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.CAL,
          series.ligne[i],
          "section "
            + series.ligne[i].absDebBief
            + CtuluLibString.VIR
            + series.ligne[i].absFinBief
            + "]"));
    }
    prop_.firePropertyChange("permSection2s", oldPers, series.ligne);
  }
  public SParametresSerieLigneCAL[] verifieContraintesProfil(final SParametresSerieLigneCAL[] lignes) {
    SParametresSerieLigneCAL[] errs= new SParametresSerieLigneCAL[0];
    if ((lignes == null)) {
      return errs;
    }
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_PermSection2: Warning: profils null");
      return errs;
    }
    final Vector erreurs= new Vector();
    for (int i= 0; i < lignes.length; i++) {
      final int b1= ph_.PROFIL().getJByAbscisse(lignes[i].absDebBief);
      final int b2= ph_.PROFIL().getJByAbscisse(lignes[i].absFinBief);
      if (b1 == b2) {
        erreurs.add(lignes[i]);
      }
    }
    errs= new SParametresSerieLigneCAL[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++) {
      errs[i]= (SParametresSerieLigneCAL)erreurs.get(i);
    }
    return errs;
  }
  public SParametresSerieLigneCAL[] verifieContraintesChevauchement(final SParametresSerieLigneCAL[] lignes) {
    SParametresSerieLigneCAL[] errs= new SParametresSerieLigneCAL[0];
    if ((lignes == null)) {
      return errs;
    }
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_PermSection2: Warning: profils null");
      return errs;
    }
    final Vector erreurs= new Vector();
    int bmax= 0;
    for (int i= 0; i < lignes.length; i++) {
      final int b1= ph_.PROFIL().getJByAbscisse(lignes[i].absDebBief);
      final int b2= ph_.PROFIL().getJByAbscisse(lignes[i].absFinBief);
      if (b1 < bmax) {
        erreurs.add(lignes[i - 1]);
        erreurs.add(lignes[i]);
      }
      bmax= b2;
    }
    errs= new SParametresSerieLigneCAL[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++) {
      errs[i]= (SParametresSerieLigneCAL)erreurs.get(i);
    }
    return errs;
  }
  public SParametresSerieLigneCAL[] verifieContraintesBief(final SParametresSerieLigneCAL[] lignes) {
    SParametresSerieLigneCAL[] errs= new SParametresSerieLigneCAL[0];
    if ((lignes == null)) {
      return errs;
    }
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_PermSection2: Warning: profils null");
      return errs;
    }
    final Vector erreurs= new Vector();
    for (int i= 0; i < lignes.length; i++) {
      final int b1= ph_.BIEF().getBiefContenantAbscisse(lignes[i].absDebBief);
      final int b2= ph_.BIEF().getBiefContenantAbscisse(lignes[i].absFinBief);
      if (b1 != b2) {
        erreurs.add(lignes[i]);
      }
    }
    errs= new SParametresSerieLigneCAL[erreurs.size()];
    for (int i= 0; i < erreurs.size(); i++) {
      errs[i]= (SParametresSerieLigneCAL)erreurs.get(i);
    }
    return errs;
  }
  public SParametresSerieLigneCAL[] getSectionsHorsBief() {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.series == null)
      || (cal_.sections.series.ligne == null)
      || (cal_.sections.series.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection2: Warning: sections null");
      return null;
    }
    final SParametresSerieLigneCAL[] pers= cal_.sections.series.ligne;
    final Vector resV= new Vector();
    for (int i= 0; i < pers.length; i++) {
      final int b1= ph_.BIEF().getBiefContenantAbscisse(pers[i].absDebBief);
      final int b2= ph_.BIEF().getBiefContenantAbscisse(pers[i].absFinBief);
      if ((b1 == -1) || (b2 == -1)) {
        System.err.println(
          "Section ["
            + pers[i].absDebBief
            + CtuluLibString.VIR
            + pers[i].absFinBief
            + "] hors-bief");
        resV.add(pers[i]);
      }
    }
    final SParametresSerieLigneCAL[] res= new SParametresSerieLigneCAL[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresSerieLigneCAL)resV.get(i);
    }
    return res;
  }
  boolean majProfilSupprime(final SParametresBiefBlocPRO p) {
    boolean maj= false;
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.series == null)
      || (cal_.sections.series.ligne == null)
      || (cal_.sections.series.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection2: Warning: sections null");
      return maj;
    }
    if (p == null) {
      return maj;
    }
    for (int i= 0; i < cal_.sections.series.nbSeries; i++) {
      if (Math
        .round(
          LidoResource.PRECISION * cal_.sections.series.ligne[i].absDebBief)
        == Math.round(LidoResource.PRECISION * p.abscisse)) {
        if (p.indice < (pro_.nbProfils - 1)) {
          System.err.println(
            "Maj section "
              + i
              + ": deb="
              + cal_.sections.series.ligne[i].absDebBief);
          cal_.sections.series.ligne[i].absDebBief=
            pro_.profilsBief[p.indice + 1].abscisse;
          fireParamStructModified(
            new FudaaParamEvent(
              this,
              0,
              LidoResource.CAL,
              cal_.sections.series.ligne[i],
              "section "
                + cal_.sections.series.ligne[i].absDebBief
                + CtuluLibString.VIR
                + cal_.sections.series.ligne[i].absFinBief
                + "]"));
        } else {
          // suppression de la section
          System.err.println("Suppression section " + i);
          supprimeSelection(
            new SParametresSerieLigneCAL[] { cal_.sections.series.ligne[i] });
          i--;
        }
        maj= true;
      }
      if (Math
        .round(
          LidoResource.PRECISION * cal_.sections.series.ligne[i].absFinBief)
        == Math.round(LidoResource.PRECISION * p.abscisse)) {
        if (p.indice > 0) {
          System.err.println(
            "Maj section "
              + i
              + ": fin="
              + cal_.sections.series.ligne[i].absFinBief);
          cal_.sections.series.ligne[i].absFinBief=
            pro_.profilsBief[p.indice - 1].abscisse;
          fireParamStructModified(
            new FudaaParamEvent(
              this,
              0,
              LidoResource.CAL,
              cal_.sections.series.ligne[i],
              "section "
                + cal_.sections.series.ligne[i].absDebBief
                + CtuluLibString.VIR
                + cal_.sections.series.ligne[i].absFinBief
                + "]"));
        } else {
          // suppression de la section
          System.err.println("Suppression section " + i);
          supprimeSelection(
            new SParametresSerieLigneCAL[] { cal_.sections.series.ligne[i] });
          i--;
        }
        maj= true;
      }
      if (Math
        .round(
          LidoResource.PRECISION * cal_.sections.series.ligne[i].absDebBief)
        == Math.round(
          LidoResource.PRECISION * cal_.sections.series.ligne[i].absFinBief)) {
        // suppression de la section
        System.err.println("Suppression section " + i);
        supprimeSelection(
          new SParametresSerieLigneCAL[] { cal_.sections.series.ligne[i] });
        i--;
        maj= true;
      }
    }
    if (maj) {
      prop_.firePropertyChange(
        "permSection2s",
        null,
        cal_.sections.series.ligne);
    }
    return maj;
  }
}
