/*
 * @file         LidoPH_PermSection3.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresSectionLigneCAL;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_PermSection3                          */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoPH_PermSection3 extends LidoPH_Base {
  private SParametresCAL cal_;
  LidoPH_PermSection3(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    cal_= (SParametresCAL)p.getParam(LidoResource.CAL);
    if (cal_ == null) {
      System.err.println(
        "LidoPH_PermSection2: Warning: passing null CAL to constructor");
    }
  }
  public SParametresSectionLigneCAL nouveauPermSection3(final int pos) {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.sections == null)
      || (cal_.sections.sections.ligne == null)) {
      System.err.println("LidoPH_PermSection3: Warning: sections null");
      return null;
    }
    final SParametresSectionLigneCAL[] pers= cal_.sections.sections.ligne;
    final SParametresSectionLigneCAL nouv= new SParametresSectionLigneCAL();
    // A FAIRE : gerer intelligemment les valeurs par defaut
    nouv.absSect= 0.;
    final SParametresSectionLigneCAL[] nouvPer=
      new SParametresSectionLigneCAL[pers.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvPer[i]= pers[i];
    }
    nouvPer[pos]= nouv;
    for (int i= pos; i < pers.length; i++) {
      nouvPer[i + 1]= pers[i];
    }
    cal_.sections.sections.ligne= nouvPer;
    cal_.sections.sections.nbSect++;
    prop_.firePropertyChange("permSection3s", pers, nouvPer);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CAL,
        nouv,
        "section " + nouv.absSect));
    return nouv;
  }
  public SParametresSectionLigneCAL[] supprimeSelection(final SParametresSectionLigneCAL[] sel) {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.sections == null)
      || (cal_.sections.sections.ligne == null)
      || (cal_.sections.sections.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection3: Warning: sections null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresSectionLigneCAL[] pers= cal_.sections.sections.ligne;
    final SParametresSectionLigneCAL[] nouvPers=
      new SParametresSectionLigneCAL[pers.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < pers.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (pers[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvPers[n++]= pers[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CAL,
            pers[i],
            "section " + pers[i].absSect));
      }
    }
    cal_.sections.sections.ligne= nouvPers;
    cal_.sections.sections.nbSect= nouvPers.length;
    prop_.firePropertyChange("permSection3s", pers, nouvPers);
    return sel;
  }
  public SParametresSectionLigneCAL[] getSectionsHorsBief() {
    if ((cal_ == null)
      || (cal_.sections == null)
      || (cal_.sections.sections == null)
      || (cal_.sections.sections.ligne == null)
      || (cal_.sections.sections.ligne.length == 0)) {
      System.err.println("LidoPH_PermSection3: Warning: sections null");
      return null;
    }
    final SParametresSectionLigneCAL[] pers= cal_.sections.sections.ligne;
    final Vector resV= new Vector();
    for (int i= 0; i < pers.length; i++) {
      final int b= ph_.BIEF().getBiefContenantAbscisse(pers[i].absSect);
      if ((b == -1)) {
        System.err.println("Section " + pers[i].absSect + " hors-bief");
        resV.add(pers[i]);
      }
    }
    final SParametresSectionLigneCAL[] res=
      new SParametresSectionLigneCAL[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresSectionLigneCAL)resV.get(i);
    }
    return res;
  }
}
