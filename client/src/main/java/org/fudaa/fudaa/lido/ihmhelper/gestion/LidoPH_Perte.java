/*
 * @file         LidoPH_Perte.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresPerteLigneCLM;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Perte                                  */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoPH_Perte extends LidoPH_Base {
  private SParametresCLM clm_;
  LidoPH_Perte(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    clm_= (SParametresCLM)p.getParam(LidoResource.CLM);
    if (clm_ == null) {
      System.err.println(
        "LidoPH_Perte: Warning: passing null CLM to constructor");
    }
  }
  public SParametresPerteLigneCLM nouveauPerte(final int pos) {
    if ((clm_ == null) || (clm_.perte == null)) {
      System.err.println("LidoPH_Perte: Warning: pertes null");
      return null;
    }
    final SParametresPerteLigneCLM[] pers= clm_.perte;
    final SParametresPerteLigneCLM nouv= new SParametresPerteLigneCLM();
    // A FAIRE: gerer intelligemment numExtBief
    nouv.xPerte= 0.;
    nouv.coefPerte= 1.;
    final SParametresPerteLigneCLM[] nouvPer=
      new SParametresPerteLigneCLM[pers.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvPer[i]= pers[i];
    }
    nouvPer[pos]= nouv;
    for (int i= pos; i < pers.length; i++) {
      nouvPer[i + 1]= pers[i];
    }
    clm_.perte= nouvPer;
    clm_.nbPerte++;
    prop_.firePropertyChange("pertes", pers, nouvPer);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.CLM,
        nouv,
        "perte " + nouv.xPerte));
    return nouv;
  }
  public SParametresPerteLigneCLM[] supprimeSelection(final SParametresPerteLigneCLM[] sel) {
    if ((clm_ == null) || (clm_.perte == null) || (clm_.perte.length == 0)) {
      System.err.println("LidoPH_Pertes: Warning: pertes null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresPerteLigneCLM[] pers= clm_.perte;
    final SParametresPerteLigneCLM[] nouvPers=
      new SParametresPerteLigneCLM[pers.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < pers.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (pers[i] == sel[j]) {
          break;
        }
      }
      if (j == sel.length) {
        nouvPers[n++]= pers[i]; // pas trouve
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            pers[i],
            "perte " + pers[i].xPerte));
      }
    }
    clm_.perte= nouvPers;
    clm_.nbPerte= nouvPers.length;
    // reclassement des indices ?
    prop_.firePropertyChange("pertes", pers, nouvPers);
    return sel;
  }
  public SParametresPerteLigneCLM[] getPertesHorsBief() {
    if ((clm_ == null) || (clm_.perte == null) || (clm_.perte.length == 0)) {
      System.err.println("LidoPH_Pertes: Warning: pertes null");
      return null;
    }
    final SParametresPerteLigneCLM[] pers= clm_.perte;
    final Vector resV= new Vector();
    for (int i= 0; i < pers.length; i++) {
      final int b= ph_.BIEF().getBiefContenantAbscisse(pers[i].xPerte);
      if (b == -1) {
        System.err.println("Perte " + pers[i].xPerte + " hors-bief");
        resV.add(pers[i]);
      }
    }
    final SParametresPerteLigneCLM[] res= new SParametresPerteLigneCLM[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresPerteLigneCLM)resV.get(i);
    }
    return res;
  }
}
