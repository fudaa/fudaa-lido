/*
 * @file         LidoPH_Profil.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SParametresRZO;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Profil                                 */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:01 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoPH_Profil extends LidoPH_Base {
  private SParametresPRO pro_;
  private SParametresRZO rzo_;
  private SResultatsRSN rsn_;
  LidoPH_Profil(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    pro_= (SParametresPRO)p.getParam(LidoResource.PRO);
    if (pro_ == null) {
      System.err.println(
        "LidoPH_Profil: Warning: passing null PRO to constructor");
    }
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoPH_Profil: Warning: passing null RZO to constructor");
    }
    rsn_= (SResultatsRSN)p.getResult(LidoResource.RSN);
  }
  public SParametresBiefBlocPRO nouveauProfil(final int pos) {
    return nouveauProfil(pos, false);
  }
  public SParametresBiefBlocPRO nouveauProfil(final int pos, boolean quiet) {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    final SParametresBiefBlocPRO[] profils= pro_.profilsBief;
    final SParametresBiefBlocPRO nouv= new SParametresBiefBlocPRO();
    nouv.indice= pos;
    nouv.numProfil= "Nouveau";
    nouv.abscisse= 0.;
    nouv.absMajMin= new double[2];
    nouv.absMajSto= new double[2];
    nouv.altMinMaj= new double[2];
    nouv.altMajSto= new double[2];
    nouv.coefStrickMajMin= 0.;
    nouv.coefStrickMajSto= 10.;
    nouv.coteRivDr= 0.;
    nouv.coteRivGa= 0.;
    nouv.nbPoints= 0;
    nouv.abs= new double[0];
    nouv.cotes= new double[0];
    final SParametresBiefBlocPRO[] nouvPros=
      new SParametresBiefBlocPRO[profils.length + 1];
    for (int i= 0; i < pos; i++) {
      nouvPros[i]= profils[i];
    }
    nouvPros[pos]= nouv;
    for (int i= pos; i < profils.length; i++) {
      nouvPros[i + 1]= profils[i];
    }
    // mises a jour
    if (!quiet) {
      ph_.CALCUL().majPlaniProfilAjoute(nouv);
    }
    pro_.profilsBief= nouvPros;
    pro_.nbProfils++;
    // reclassement des indices
    for (int i= 0; i < pro_.profilsBief.length; i++) {
      pro_.profilsBief[i].indice= i;
    }
    // IL RESTE A METTRE A JOUR LES "profilsBief" du PRO (et le RZO?)
    if (!quiet) {
      prop_.firePropertyChange("profils", profils, nouvPros);
      fireParamStructCreated(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.PRO,
          nouv,
          "profil " + nouv.numProfil));
    }
    return nouv;
  }
  public SParametresBiefBlocPRO supprimeProfil(final SParametresBiefBlocPRO p) {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    if (p == null) {
      return null;
    }
    final SParametresBiefBlocPRO[] profils= pro_.profilsBief;
    final SParametresBiefBlocPRO[] nouvPros=
      new SParametresBiefBlocPRO[profils.length - 1];
    int i= 0;
    for (i= 0; i < profils.length; i++) {
      if (p == profils[i]) {
        break;
      }
    }
    // pas trouve
    if (i >= profils.length) {
      return null;
    }
    int n= 0;
    for (i= 0; i < profils.length; i++) {
      if (p != profils[i]) {
        nouvPros[n++]= profils[i];
      }
    }
    // Mises a jour avant reclassement
    ph_.BIEF().majProfilSupprime(p);
    ph_.PERMSECTION2().majProfilSupprime(p);
    ph_.CALCUL().majPlaniProfilSupprime(p);
    pro_.profilsBief= nouvPros;
    pro_.nbProfils= pro_.profilsBief.length;
    // reclassement des indices
    for (i= 0; i < pro_.profilsBief.length; i++) {
      pro_.profilsBief[i].indice= i;
    }
    // Mises a jour apres reclassement
    ph_.CALCUL().majGencalProfilSupprime(p);
    prop_.firePropertyChange("profils", profils, nouvPros);
    fireParamStructDeleted(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.PRO,
        p,
        "profil " + p.numProfil));
    return p;
  }
  public SParametresBiefBlocPRO[] supprimeSelection(final SParametresBiefBlocPRO[] sel) {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final Vector v= new Vector();
    for (int i= 0; i < sel.length; i++) {
      v.add(supprimeProfil(sel[i]));
    }
    final SParametresBiefBlocPRO[] res= new SParametresBiefBlocPRO[v.size()];
    for (int i= 0; i < v.size(); i++) {
      res[i]= (SParametresBiefBlocPRO)v.get(i);
    }
    return res;
  }
  /*private SParametresBiefBlocPRO[] old_supprimeSelection(SParametresBiefBlocPRO[] sel) {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    if (sel == null)
      return null;
    SParametresBiefBlocPRO[] profils= pro_.profilsBief;
    SParametresBiefBlocPRO[] nouvPros=
      new SParametresBiefBlocPRO[profils.length - sel.length];
    int n= 0;
    int j= 0;
    for (int i= 0; i < profils.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (profils[i] == sel[j])
          break;
      }
      if (j == sel.length)
        nouvPros[n++]= profils[i]; // pas trouve
      else { // un profil supprime: tester si c'est une extremite de bief et
        // mettre a jour le bief et cal.biefXFin/Origi si oui
        ph_.BIEF().majProfilSupprime(profils[i]);
        // MAJ sections de calcul
        ph_.PERMSECTION2().majProfilSupprime(profils[i]);
        ph_.CALCUL().majPlaniProfilSupprime(profils[i]);
        fireParamStructCreated(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.PRO,
            profils[i],
            "profil " + profils[i].numProfil));
      }
    }
    pro_.profilsBief= nouvPros;
    pro_.nbProfils= pro_.profilsBief.length;
    // reclassement des indices
    for (int i= 0; i < pro_.profilsBief.length; i++)
      pro_.profilsBief[i].indice= i;
    ph_.CALCUL().majPlaniProfilSupprime(null);
    prop_.firePropertyChange("profils", profils, nouvPros);
    return sel;
  }*/
  public SParametresBiefBlocPRO dupliqueProfil(
    final SParametresBiefBlocPRO profil,
    final int pos) {
    if (profil == null) {
      return null;
    }
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    final SParametresBiefBlocPRO[] profils= pro_.profilsBief;
    final SParametresBiefBlocPRO nouv= nouveauProfil(pos, true);
    nouv.abscisse= profil.abscisse;
    nouv.absMajMin[0]= profil.absMajMin[0];
    nouv.absMajMin[1]= profil.absMajMin[1];
    nouv.absMajSto[0]= profil.absMajSto[0];
    nouv.absMajSto[1]= profil.absMajSto[1];
    if (profil.altMinMaj.length > 0) {
      nouv.altMinMaj[0]= profil.altMinMaj[0];
      nouv.altMinMaj[1]= profil.altMinMaj[1];
    }
    if (profil.altMinMaj.length > 0) {
      nouv.altMajSto[0]= profil.altMajSto[0];
      nouv.altMajSto[1]= profil.altMajSto[1];
    }
    nouv.coefStrickMajMin= profil.coefStrickMajMin;
    nouv.coefStrickMajSto= profil.coefStrickMajSto;
    nouv.coteRivDr= profil.coteRivDr;
    nouv.coteRivGa= profil.coteRivGa;
    nouv.nbPoints= profil.nbPoints;
    nouv.abs= new double[profil.abs.length];
    for (int i= 0; i < profil.abs.length; i++) {
      nouv.abs[i]= profil.abs[i];
    }
    nouv.cotes= new double[profil.cotes.length];
    for (int i= 0; i < profil.cotes.length; i++) {
      nouv.cotes[i]= profil.cotes[i];
    }
    // mises a jour
    ph_.CALCUL().majPlaniProfilAjoute(nouv);
    prop_.firePropertyChange("profils", profils, pro_.profilsBief);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.PRO,
        nouv,
        "profil " + nouv.numProfil));
    return nouv;
  }
  public SParametresBiefBlocPRO[] importProfils(final SParametresBiefBlocPRO[] imports) {
    if (imports == null) {
      return null;
    }
    final SParametresBiefBlocPRO[] tmp= pro_.profilsBief;
    pro_.profilsBief= new SParametresBiefBlocPRO[tmp.length + imports.length];
    for (int i= 0; i < tmp.length; i++) {
      pro_.profilsBief[i]= tmp[i];
    }
    for (int i= 0; i < imports.length; i++) {
      pro_.profilsBief[i + tmp.length]= imports[i];
    }
    pro_.nbProfils= pro_.profilsBief.length;
    // reaffectation des indices
    for (int i= 0; i < pro_.profilsBief.length; i++) {
      pro_.profilsBief[i].indice= i;
    }
    for (int i= 0; i < imports.length; i++) {
      fireParamStructCreated(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.PRO,
          imports[i],
          "profil " + imports[i].numProfil));
    }
    return imports;
  }
  public SParametresBiefBlocPRO profondeurMax(int pDebut, int pFin) {
    if (pDebut > 0) {
      pDebut--; // les indices dans les profils sont les indices Java
    }
    if (pFin > 0) {
      pFin--; // (-> de 0 a nbProfils-1)
    }
    int minLit= pDebut; // indice du profil de lit min
    double minCote= Double.MAX_VALUE; // cote de lit mini trouvee
    for (int i= 0; i < pro_.nbProfils; i++) {
      if ((pro_.profilsBief[i].indice >= pDebut)
        && (pro_.profilsBief[i].indice <= pFin)) {
        // on cherche le fond du lit
        double profLitI= 0.;
        if (pro_.profilsBief[i].nbPoints > 0) {
          profLitI= pro_.profilsBief[i].cotes[0];
        }
        for (int z= 0; z < pro_.profilsBief[i].nbPoints; z++) {
          if (pro_.profilsBief[i].cotes[z] < profLitI) {
            profLitI= pro_.profilsBief[i].cotes[z];
          }
        }
        if (profLitI < minCote) {
          minCote= profLitI;
          minLit= i;
        }
      }
    }
    return pro_.profilsBief[minLit];
  }
  public double getFond(final SParametresBiefBlocPRO p) {
    double profLitI= Double.MAX_VALUE;
    for (int z= 0; z < p.nbPoints; z++) {
      if (p.cotes[z] < profLitI) {
        profLitI= p.cotes[z];
      }
    }
    return profLitI;
  }
  public double interpoleFond(final double abs) {
    if ((pro_ == null) || (pro_.nbProfils == 0)) {
      return Double.POSITIVE_INFINITY;
    }
    double res= Double.POSITIVE_INFINITY;
    SParametresBiefBlocPRO p1= null;
    SParametresBiefBlocPRO p2= null;
    for (int i= 0; i < pro_.nbProfils; i++) {
      if (pro_.profilsBief[i].abscisse >= abs) {
        if (i > 0) {
          p1= pro_.profilsBief[i - 1];
        }
        p2= pro_.profilsBief[i];
        break;
      }
    }
    if ((p1 != null) && (p2 != null)) {
      final double n1= getFond(p1);
      final double n2= getFond(p2);
      res= n2 + (n1 - n2) * (p2.abscisse - abs) / (p2.abscisse - p1.abscisse);
    }
    return res;
  }
  public SParametresBiefBlocPRO profondeurMin(int pDebut, int pFin) {
    if (pDebut > 0) {
      pDebut--; // les indices dans les profils sont les indices Java
    }
    if (pFin > 0) {
      pFin--; // (-> de 0 a nbProfils-1)
    }
    // verif coherence
    if ((pDebut < 0)
      || (pDebut >= pro_.nbProfils)
      || (pFin < 0)
      || (pFin >= pro_.nbProfils)) {
      return null;
    }
    int maxLit= pDebut; // indice du profil de lit max
    double maxCote= -Double.MAX_VALUE; // cote de lit maxi trouvee
    for (int i= 0; i < pro_.nbProfils; i++) {
      if ((pro_.profilsBief[i].indice >= pDebut)
        && (pro_.profilsBief[i].indice <= pFin)) {
        // on cherche le fond du lit
        double profLitI= 0.;
        if (pro_.profilsBief[i].nbPoints > 0) {
          profLitI= pro_.profilsBief[i].cotes[0];
        }
        for (int z= 0; z < pro_.profilsBief[i].nbPoints; z++) {
          if (pro_.profilsBief[i].cotes[z] < profLitI) {
            profLitI= pro_.profilsBief[i].cotes[z];
          }
        }
        if (profLitI > maxCote) {
          maxCote= profLitI;
          maxLit= i;
        }
      }
    }
    return pro_.profilsBief[maxLit];
  }
  public SParametresBiefBlocPRO getProfil(final int ind) {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    if ((ind < 0) || (ind >= pro_.nbProfils)) {
      return null;
    }
    return pro_.profilsBief[ind];
  }
  public void miseAZero(final SParametresBiefBlocPRO[] pros) {
    if (pros == null) {
      return;
    }
    for (int i= 0; i < pros.length; i++) {
      pros[i].abscisse= 0.;
    }
  }
  public void decalage(final SParametresBiefBlocPRO[] pros, final double offset) {
    if (pros == null) {
      return;
    }
    for (int i= 0; i < pros.length; i++) {
      pros[i].abscisse += offset;
    }
  }
  public double getAbscisseByJ(final int j) {
    if ((pro_ == null) || (pro_.nbProfils == 0)) {
      return 0.;
    }
    if ((j < 0) || (j > pro_.nbProfils)) {
      return 0.;
    }
    for (int i= 0; i < pro_.nbProfils; i++) {
      if (pro_.profilsBief[i].indice == j) {
        return pro_.profilsBief[i].abscisse;
      }
    }
    return 0.;
  }
  public int getJByAbscisse(final double abs) {
    if ((pro_ == null) || (pro_.nbProfils == 0)) {
      return 0;
    }
    for (int i= 0; i < pro_.nbProfils; i++) {
      if (Math.round(LidoResource.PRECISION * pro_.profilsBief[i].abscisse)
        == Math.round(LidoResource.PRECISION * abs)) {
        return pro_.profilsBief[i].indice;
      }
    }
    return 0;
  }
  public SParametresBiefBlocPRO getProfilByAbscisse(final double abs) {
    if ((pro_ == null) || (pro_.nbProfils == 0)) {
      return null;
    }
    for (int i= 0; i < pro_.nbProfils; i++) {
      if (Math.round(LidoResource.PRECISION * pro_.profilsBief[i].abscisse)
        == Math.round(LidoResource.PRECISION * abs)) {
        return pro_.profilsBief[i];
      }
    }
    return null;
  }
  public void reindiceProfils() {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return;
    }
    boolean maj= false;
    for (int i= 0; i < pro_.profilsBief.length; i++) {
      if (pro_.profilsBief[i].indice != i) {
        pro_.profilsBief[i].indice= i;
        maj= true;
      }
    }
    if (maj) {
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.PRO,
          pro_.profilsBief,
          "profils reordonnes"));
      prop_.firePropertyChange("profils", null, pro_.profilsBief);
    }
  }
  public double[][] getCourbeResultat(final SParametresBiefBlocPRO p, final int var) {
    if ((rsn_ == null)
      || (rsn_.pasTemps == null)
      || (rsn_.pasTemps.length == 0)) {
      System.err.println("LidoPH_Profils: Warning: resultats null");
      return null;
    }
    final int bief= ph_.BIEF().getBiefContenantAbscisse(p.abscisse);
    if (bief == -1) {
      return null;
    }
    final double[][] limni= new double[rsn_.pasTemps.length][2];
    for (int t= 0; t < rsn_.pasTemps.length; t++) {
      limni[t][0]= rsn_.pasTemps[t].t; //temps
      for (int i= 0; i < rsn_.pasTemps[t].ligBief[bief].ligne.length; i++) {
        if (Math
          .round(
            LidoResource.PRECISION * rsn_.pasTemps[t].ligBief[bief].ligne[i].x)
          >= Math.round(LidoResource.PRECISION * p.abscisse)) {
          double niveau;
          double debit;
          //boolean isInterpole= false;
          if (Math
            .round(
              LidoResource.PRECISION
                * rsn_.pasTemps[t].ligBief[bief].ligne[i].x)
            == Math.round(LidoResource.PRECISION * p.abscisse)) {
            niveau= rsn_.pasTemps[t].ligBief[bief].ligne[i].z;
            debit= rsn_.pasTemps[t].ligBief[bief].ligne[i].q;
          } else {
            if (i == 0) {
              break;
            }
            final double x1= rsn_.pasTemps[t].ligBief[bief].ligne[i - 1].x;
            final double x2= rsn_.pasTemps[t].ligBief[bief].ligne[i].x;
            if (x1 == x2) {
              niveau= rsn_.pasTemps[t].ligBief[bief].ligne[i].z;
              debit= rsn_.pasTemps[t].ligBief[bief].ligne[i].q;
            } else {
              final double n1= rsn_.pasTemps[t].ligBief[bief].ligne[i - 1].z;
              final double n2= rsn_.pasTemps[t].ligBief[bief].ligne[i].z;
              final double q1= rsn_.pasTemps[t].ligBief[bief].ligne[i - 1].q;
              final double q2= rsn_.pasTemps[t].ligBief[bief].ligne[i].q;
              niveau= n2 + (n1 - n2) * (x2 - p.abscisse) / (x2 - x1);
              debit= q2 + (q1 - q2) * (x2 - p.abscisse) / (x2 - x1);
              //isInterpole= true;
            }
          }
          if (var == LidoResource.LIMITE.LIMNI) {
            limni[t][1]= niveau; //niveau
          } else if (var == LidoResource.LIMITE.HYDRO) {
            limni[t][1]= debit;
          } else if (var == LidoResource.LIMITE.TARAGE) {
            limni[t][0]= niveau;
            limni[t][1]= debit;
          }
          break;
        }
      }
    }
    return limni;
  }
  public Double getNiveauEauResultat(final SParametresBiefBlocPRO p, final int pasTemps) {
    return getNiveauEauResultat(p.abscisse, pasTemps);
  }
  public Double getCoteEauResultat(final SParametresBiefBlocPRO p, final int pasTemps) {
    return getCoteEauResultat(p.abscisse, pasTemps);
  }
  /*  public Double getNiveauEauResultat(SParametresBiefBlocPRO p, int pasTemps)
    {
      if( (rsn_==null)||(rsn_.pasTemps==null)||(rsn_.pasTemps.length==0) ) {
        System.err.println("LidoPH_Profils: Warning: resultats null");
        return null;
      }
      Double res=null;
      int bief=ph_.BIEF().getBiefContenantAbscisse(p.abscisse);
      if( bief==-1 ) return null;
      for(int i=0; i<rsn_.pasTemps[pasTemps].ligBief[bief].ligne.length; i++) {
        if( Math.round(LidoResource.PRECISION*rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].x)>=
            Math.round(LidoResource.PRECISION*p.abscisse) ) {
          double niveau;
          boolean isInterpole=false;
          if( Math.round(LidoResource.PRECISION*rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].x)==
              Math.round(LidoResource.PRECISION*p.abscisse) ) {
            niveau=rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].y;
          } else {
            if( i==0 ) break;
            double x1=rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i-1].x;
            double x2=rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].x;
            if( x1==x2 ) niveau=x2;
            else {
              double n1=rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i-1].y;
              double n2=rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].y;
              niveau=n2+(n1-n2)*(x2-p.abscisse)/(x2-x1);
              isInterpole=true;
            }
          }
          res=new Double(niveau);
          break;
        }
      }
      return res;
    }*/
  public Double getCoteEauResultat(final double abscisse, final int pasTemps) {
    return getNiveauEauResultat(abscisse, pasTemps, true);
  }
  public Double getNiveauEauResultat(final double abscisse, final int pasTemps) {
    return getNiveauEauResultat(abscisse, pasTemps, false);
  }
  public Double getNiveauEauResultat(
    final double abscisse,
    final int pasTemps,
    final boolean cote) {
    if ((rsn_ == null)
      || (rsn_.pasTemps == null)
      || (rsn_.pasTemps.length == 0)) {
      System.err.println("LidoPH_Profils: Warning: resultats null");
      return null;
    }
    Double res= null;
    final int bief= ph_.BIEF().getBiefContenantAbscisse(abscisse);
    if (bief == -1) {
      return null;
    }
    for (int i= 0;
      i < rsn_.pasTemps[pasTemps].ligBief[bief].ligne.length;
      i++) {
      if (Math
        .round(
          LidoResource.PRECISION
            * rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].x)
        >= Math.round(LidoResource.PRECISION * abscisse)) {
        double niveau;
        if (i == 0) {
          if (cote) {
            niveau= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].z;
          } else {
            niveau= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].y;
          }
          res= new Double(niveau);
          break;
        }
        final double x1= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i - 1].x;
        final double x2= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].x;
        if (x1 == x2) {
          niveau= x2;
        } else {
          double n1;
          double n2;
          if (cote) {
            n1= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i - 1].z;
            n2= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].z;
          } else {
            n1= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i - 1].y;
            n2= rsn_.pasTemps[pasTemps].ligBief[bief].ligne[i].y;
          }
          niveau= n2 + (n1 - n2) * (x2 - abscisse) / (x2 - x1);
        }
        res= new Double(niveau);
        break;
      }
    }
    return res;
  }
  public Double getMaxCoteEauResultat(final double abscisse) {
    if ((rsn_ == null)
      || (rsn_.pasTemps == null)
      || (rsn_.pasTemps.length == 0)) {
      System.err.println("LidoPH_Profils: Warning: resultats null");
      return null;
    }
    double res= Double.NEGATIVE_INFINITY;
    for (int t= 0; t < rsn_.pasTemps.length; t++) {
      final Double tres= getCoteEauResultat(abscisse, t);
      if (tres != null) {
        if (tres.doubleValue() > res) {
          res= tres.doubleValue();
        }
      }
    }
    return new Double(res);
  }
  public SParametresBiefBlocPRO[] getProfilsHorsBief() {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    final SParametresBiefBlocPRO[] profs= pro_.profilsBief;
    final Vector resV= new Vector();
    for (int i= 0; i < profs.length; i++) {
      final int b= ph_.BIEF().getBiefContenantAbscisse(profs[i].abscisse);
      if ((b == -1)) {
        System.err.println("Profil " + profs[i].abscisse + " hors-bief");
        resV.add(profs[i]);
      }
    }
    final SParametresBiefBlocPRO[] res= new SParametresBiefBlocPRO[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresBiefBlocPRO)resV.get(i);
    }
    return res;
  }
  public SParametresBiefBlocPRO profilSuivant(final SParametresBiefBlocPRO profil) {
    if ((pro_ == null) || (pro_.profilsBief == null)) {
      System.err.println("LidoPH_Profils: Warning: profils null");
      return null;
    }
    SParametresBiefBlocPRO suivant= null;
    int i;
    for (i= 0; i < pro_.profilsBief.length; i++) {
      if (pro_.profilsBief[i].abscisse > profil.abscisse) {
        suivant= pro_.profilsBief[i];
        break;
      }
    }
    if (suivant == null) {
      return null;
    }
    for (int j= i; j < pro_.profilsBief.length; j++) {
      if ((pro_.profilsBief[j].abscisse > profil.abscisse)
        && (pro_.profilsBief[j].abscisse < suivant.abscisse)) {
        suivant= pro_.profilsBief[j];
      }
    }
    return suivant;
  }
  public int getIndiceProfilAmont(final double abs) {
    int kam= 0;
    if ((pro_ == null) || (pro_.nbProfils == 0)) {
      return -1;
    }
    System.out.println("abs    " + abs);
    //System.out.println("pro_.nbProfils"   +pro_.nbProfils);
    for (int i= 0; i < pro_.nbProfils; i++) {
      //System.out.println("  i"   + i +".abscisse"+pro_.profilsBief[i].abscisse);
      if (pro_.profilsBief[i].abscisse < abs) {
        kam= i;
        //System.out.println("*  kam      "   + kam);
        //System.out.println("*  kam bief "   + pro_.profilsBief[kam].indice);
      }
    }
    System.out.println(" ==>  kam " + pro_.profilsBief[kam].indice);
    return pro_.profilsBief[kam].indice;
  }
  public int getIndiceProfilAval(final double abs) {
    int kav= -1;
    if ((pro_ == null) || (pro_.nbProfils == 0)) {
      return -1;
    }
    for (int i= 0; i < pro_.nbProfils; i++) {
      //System.out.println("  i"   + i +".abscisse"+pro_.profilsBief[i].abscisse);
      if (pro_.profilsBief[i].abscisse > abs) {
        if (kav == -1) {
          kav= i;
          //System.out.println("*  kav     "   + kav);
          //System.out.println("*  kav bief"   + pro_.profilsBief[kav].indice);
        }
      }
    }
    System.out.println("==>  kav " + pro_.profilsBief[kav].indice);
    return pro_.profilsBief[kav].indice;
  }
}
