/*
 * @file         LidoPH_Singularite.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Hashtable;
import java.util.Vector;

import org.fudaa.dodico.corba.lido.SParametresBiefSingLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresRZO;
import org.fudaa.dodico.corba.lido.SParametresSNG;
import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoResource;
/******************************************************************************/
/*                                                                            */
/*                              LidoPH_Singularite                            */
/*                                                                            */
/******************************************************************************/
/**
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoPH_Singularite extends LidoPH_Base {
  private SParametresRZO rzo_;
  private SParametresSNG sng_;
  private Hashtable singBiefCorres_;
  LidoPH_Singularite(final FudaaProjet p, final LidoParamsHelper ph) {
    super(p, ph);
  }
  void setProjet(final FudaaProjet p) {
    rzo_= (SParametresRZO)p.getParam(LidoResource.RZO);
    if (rzo_ == null) {
      System.err.println(
        "LidoPH_Singularite: Warning: passing null RZO to constructor");
    }
    if (rzo_ == null) {
      return;
    }
    sng_= (SParametresSNG)p.getParam(LidoResource.SNG);
    if (sng_ == null) {
      System.err.println(
        "LidoPH_Singularite: Warning: passing null SNG to constructor");
    }
    singBiefCorres_= new Hashtable();
    if ((rzo_.blocSitus == null)
      || (rzo_.blocSitus.ligneSitu == null)
      || (rzo_.blocSings == null)
      || (rzo_.blocSings.ligneSing == null)) {
      return;
    }
    final SParametresBiefSituLigneRZO[] biefs= rzo_.blocSitus.ligneSitu;
    final SParametresBiefSingLigneRZO[] sings= rzo_.blocSings.ligneSing;
    for (int i= 0; i < rzo_.nbBief; i++) {
      for (int j= 0; j < sings.length; j++) {
        if (biefs[i].numBief == sings[j].numBief) {
          singBiefCorres_.put(sings[j], biefs[i]);
          break;
        }
      }
    }
    if (singBiefCorres_.size() != rzo_.nbBief) {
      System.err.println(
        "LidoPH_Singularite: Warning: incohérence biefs/sings");
    }
  }
  // methode interdite au public (on ne cree pas de singularite:
  // on cree un bief et on edite la singularite correspondante)
  SParametresBiefSingLigneRZO nouveauSingularite(final SParametresBiefSituLigneRZO bief) {
    if ((rzo_ == null)
      || (rzo_.blocSings == null)
      || (rzo_.blocSings.ligneSing == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return null;
    }
    if (bief == null) {
      System.err.println("LidoPH_Singularite: Warning: bief null");
      return null;
    }
    final SParametresBiefSingLigneRZO[] sings= rzo_.blocSings.ligneSing;
    final SParametresBiefSingLigneRZO nouv= new SParametresBiefSingLigneRZO();
    final int pos= bief.numBief;
    nouv.numBief= pos;
    nouv.xSing= 0.;
    nouv.nSing= 0;
    if (singBiefCorres_.put(nouv, bief) != null) {
      System.err.println(
        "LidoPH_Singularite: Warning: singularite already bound");
    }
    final SParametresBiefSingLigneRZO[] nouvSings=
      new SParametresBiefSingLigneRZO[sings.length + 1];
    for (int i= 0; i < sings.length; i++) {
      sings[i].numBief=
        ((SParametresBiefSituLigneRZO)singBiefCorres_.get(sings[i])).numBief;
      nouvSings[i]= sings[i];
    }
    nouvSings[sings.length]= nouv;
    rzo_.blocSings.ligneSing= nouvSings;
    // rzo_.nbBief a deja ete incremente auparavant par nouveauBief()
    // reclassement
    java.util.Arrays.sort(
      rzo_.blocSings.ligneSing,
      new SingulariteComparator());
    prop_.firePropertyChange("singularites", sings, nouvSings);
    fireParamStructCreated(
      new FudaaParamEvent(
        this,
        0,
        LidoResource.RZO,
        nouv,
        "singularite " + nouv.numBief));
    return nouv;
  }
  SParametresBiefSingLigneRZO[] supprimeSelectionBiefs(final SParametresBiefSituLigneRZO[] sel) {
    if ((rzo_ == null)
      || (rzo_.blocSings == null)
      || (rzo_.blocSings.ligneSing == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresBiefSingLigneRZO[] sings= rzo_.blocSings.ligneSing;
    final SParametresBiefSingLigneRZO[] tmpSing=
      new SParametresBiefSingLigneRZO[rzo_.blocSings.ligneSing.length
        - sel.length];
    final SParametresBiefSingLigneRZO[] suppr=
      new SParametresBiefSingLigneRZO[sel.length];
    int n= 0;
    int s= 0;
    int j= 0;
    int i= 0;
    for (j= 0; j < sings.length; j++) {
      for (i= 0; i < sel.length; i++) {
        if (sel[i].numBief == j) {
          suppr[s++]= sings[j];
          singBiefCorres_.remove(sings[j]);
          break;
        }
      }
      if (i == sel.length) { // pas trouve
        tmpSing[n]= sings[j];
        tmpSing[n].numBief= n++;
      } else {
        fireParamStructDeleted(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            sings[j],
            "singularite " + sings[j].numBief));
      }
    }
    rzo_.blocSings.ligneSing= tmpSing;
    prop_.firePropertyChange("singularites", sings, tmpSing);
    return suppr;
  }
  public SParametresBiefSingLigneRZO[] supprimeSelection(final SParametresBiefSingLigneRZO[] sel) {
    if ((rzo_ == null)
      || (rzo_.blocSings == null)
      || (rzo_.blocSings.ligneSing == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    final SParametresBiefSingLigneRZO[] sings= rzo_.blocSings.ligneSing;
    int j= 0;
    for (int i= 0; i < sings.length; i++) {
      for (j= 0; j < sel.length; j++) {
        if (sings[i] == sel[j]) {
          sings[i].xSing= 0.;
          sings[i].nSing= 0;
        } else {
          fireParamStructDeleted(
            new FudaaParamEvent(
              this,
              0,
              LidoResource.RZO,
              sings[i],
              "singularite " + sings[i].numBief));
        }
      }
    }
    rzo_.blocSings.ligneSing= sings;
    // A FAIRE: supprimer aussi dans le SNG
    prop_.firePropertyChange("singularites", sings, sings);
    return sel;
  }
  /*public SParametresSingBlocSNG initialiseSingularite(SParametresBiefSingLigneRZO s)
  {
    if( (rzo_==null)||(rzo_.blocSings==null)||(rzo_.blocSings.ligneSing==null) ) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return null;
    }
    if( s==null ) return null;
    //SParametresSingBlocSNG nouv=getSingularite(s);
    
    SParametresSingBlocSNG nouv=phLoisng_.nouveauLoisng(sng_.nbSing);
    
    // lien avec le bief
    s.nSing=nouv.numSing;
    return nouv;
  }*/
  public SParametresSingBlocSNG getSingularite(final SParametresBiefSingLigneRZO sel) {
    if ((sng_ == null)
      || (sng_.singularites == null)
      || (sng_.singularites.length == 0)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return null;
    }
    if (sel == null) {
      return null;
    }
    for (int i= 0; i < sng_.singularites.length; i++) {
      if (sng_.singularites[i].numSing == sel.nSing) {
        return sng_.singularites[i];
      }
    }
    return null;
  }
  public void reindiceSingularites() {
    if ((rzo_ == null)
      || (rzo_.blocSings == null)
      || (rzo_.blocSings.ligneSing == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return;
    }
    boolean maj= false;
    final SParametresBiefSingLigneRZO[] sings= rzo_.blocSings.ligneSing;
    final SParametresBiefSingLigneRZO[] nouvSings=
      new SParametresBiefSingLigneRZO[rzo_.nbBief];
    final SParametresBiefSituLigneRZO[] biefs= rzo_.blocSitus.ligneSitu;
    for (int i= 0; i < biefs.length; i++) {
      nouvSings[biefs[i].numBief]= sings[i];
      if (sings[i].numBief != biefs[i].numBief) {
        nouvSings[biefs[i].numBief].numBief= biefs[i].numBief;
        maj= true;
      }
    }
    if (maj) {
      rzo_.blocSings.ligneSing= nouvSings;
      fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.RZO,
          rzo_.blocSings.ligneSing,
          "singularites reordonnees"));
      prop_.firePropertyChange("singularites", null, rzo_.blocSings.ligneSing);
    }
  }
  void loisngSupprimee(final SParametresSingBlocSNG loi) {
    if ((sng_ == null) || (sng_.singularites == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return;
    }
    for (int i= 0; i < rzo_.blocSings.ligneSing.length; i++) {
      if (rzo_.blocSings.ligneSing[i].nSing == loi.numSing) {
        rzo_.blocSings.ligneSing[i].nSing= 0;
        rzo_.blocSings.ligneSing[i].xSing= 0;
        fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            rzo_.blocSings.ligneSing[i],
            "singularite " + rzo_.blocSings.ligneSing[i].numBief));
      }
    }
    prop_.firePropertyChange("singularites", null, rzo_.blocSings.ligneSing);
  }
  public SParametresBiefSingLigneRZO[] getSingularitesHorsBief() {
    if ((rzo_ == null)
      || (rzo_.blocSings == null)
      || (rzo_.blocSings.ligneSing == null)) {
      System.err.println("LidoPH_Singularite: Warning: singularites null");
      return null;
    }
    final SParametresBiefSingLigneRZO[] sings= rzo_.blocSings.ligneSing;
    final Vector resV= new Vector();
    for (int i= 0; i < sings.length; i++) {
      final int b= ph_.BIEF().getBiefContenantAbscisse(sings[i].xSing);
      if ((sings[i].nSing > 0) && (b == -1)) {
        System.err.println("Singularité " + sings[i].xSing + " hors-bief");
        resV.add(sings[i]);
      }
    }
    final SParametresBiefSingLigneRZO[] res=
      new SParametresBiefSingLigneRZO[resV.size()];
    for (int i= 0; i < res.length; i++) {
      res[i]= (SParametresBiefSingLigneRZO)resV.get(i);
    }
    return res;
  }
}
