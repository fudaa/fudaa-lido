/*
 * @file         LidoParamsHelper.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTableCellRenderer;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.lido.*;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Id: LidoParamsHelper.java,v 1.10 2006-09-19 15:05:02 deniger Exp $
 * @author       Axel von Arnim
 */
public class LidoParamsHelper implements FudaaProjetListener {
  FudaaProjet projet_;
  LidoPH_Profil phProfil_;
  LidoPH_Noeud phNoeud_;
  LidoPH_Bief phBief_;
  LidoPH_Singularite phSingularite_;
  LidoPH_Loisng phLoisng_;
  LidoPH_Limite phLimite_;
  LidoPH_Loiclm phLoiclm_;
  LidoPH_Apport phApport_;
  LidoPH_Perte phPerte_;
  LidoPH_Laisse phLaisse_;
  LidoPH_PermSection2 phPermSection2_;
  LidoPH_PermSection3 phPermSection3_;
  LidoPH_Calcul phCalcul_;
  public static NumberFormat NUMBER_FORMAT;
  public static NumberFormat NUMBER_FORMAT_DOUBLE;
  static {
    NUMBER_FORMAT= NumberFormat.getInstance(Locale.ENGLISH);
    if (NUMBER_FORMAT == null) {
      NUMBER_FORMAT= NumberFormat.getInstance();
    }
    NUMBER_FORMAT.setMaximumFractionDigits(LidoResource.FRACTIONDIGITS);
    //    NUMBER_FORMAT.setMinimumFractionDigits(1);
    NUMBER_FORMAT.setGroupingUsed(false);
    NUMBER_FORMAT_DOUBLE= NumberFormat.getInstance(Locale.ENGLISH);
    if (NUMBER_FORMAT_DOUBLE == null) {
      NUMBER_FORMAT_DOUBLE= NumberFormat.getInstance();
    }
    NUMBER_FORMAT_DOUBLE.setMaximumFractionDigits(LidoResource.FRACTIONDIGITS);
    NUMBER_FORMAT_DOUBLE.setMinimumFractionDigits(1);
    NUMBER_FORMAT_DOUBLE.setGroupingUsed(false);
  }
  public static TableCellEditor createDoubleCellEditor() {
    final BuTextField txt= BuTextField.createDoubleField();
    txt.setHorizontalAlignment(SwingConstants.RIGHT);
    return new BuTableCellEditor(txt);
  }
  public static TableCellEditor createIntegerCellEditor() {
    final BuTextField txt= BuTextField.createIntegerField();
    txt.setHorizontalAlignment(SwingConstants.RIGHT);
    return new BuTableCellEditor(txt);
  }
  public static TableCellEditor createStringCellEditor() {
    final BuTextField txt= new BuTextField();
    txt.setHorizontalAlignment(SwingConstants.RIGHT);
    return new BuTableCellEditor(txt);
  }
  public static TableCellRenderer createDoubleCellRenderer() {
    final BuTableCellRenderer r= new BuTableCellRenderer();
    r.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    return r;
  }
  //  public final static ProfilComparator PROFIL_COMPARATOR=new ProfilComparator();
  //  public final static NoeudComparator NOEUD_COMPARATOR=new NoeudComparator();
  //  public final static BiefComparator BIEF_COMPARATOR=new BiefComparator();
  //  public final static SingulariteComparator SINGULARITE_COMPARATOR=new SingulariteComparator();
  //  public final static LimiteComparator LIMITE_COMPARATOR=new LimiteComparator();
  //  public final static LoiclmComparator LOICLM_COMPARATOR=new LoiclmComparator();
  //  public final static LoiclmPointComparator LOICLMPOINT_TARAGE_COMPARATOR=new LoiclmPointComparator(LidoResource.LIMITE.TARAGE);
  //  public final static LoiclmPointComparator LOICLMPOINT_HYDRO_COMPARATOR=new LoiclmPointComparator(LidoResource.LIMITE.HYDRO);
  //  public final static LoiclmPointComparator LOICLMPOINT_LIMNI_COMPARATOR=new LoiclmPointComparator(LidoResource.LIMITE.LIMNI);
  //  public final static ApportComparator APPORT_COMPARATOR=new ApportComparator();
  //  public final static PerteComparator PERTE_COMPARATOR=new PerteComparator();
  //  public final static LaisseComparator LAISSE_COMPARATOR=new LaisseComparator();
  //  public final static PermSection2Comparator PERMSECTION2_COMPARATOR=new PermSection2Comparator();
  //  public final static PermSection3Comparator PERMSECTION3_COMPARATOR=new PermSection3Comparator();
  private static ProfilComparator PROFIL_COMPARATOR;
  public final static ProfilComparator PROFIL_COMPARATOR() {
    if (PROFIL_COMPARATOR == null) {
      PROFIL_COMPARATOR= new ProfilComparator();
    }
    return PROFIL_COMPARATOR;
  }
  private static NoeudComparator NOEUD_COMPARATOR;
  public final static NoeudComparator NOEUD_COMPARATOR() {
    if (NOEUD_COMPARATOR == null) {
      NOEUD_COMPARATOR= new NoeudComparator();
    }
    return NOEUD_COMPARATOR;
  }
  private static BiefComparator BIEF_COMPARATOR;
  public final static BiefComparator BIEF_COMPARATOR() {
    if (BIEF_COMPARATOR == null) {
      BIEF_COMPARATOR= new BiefComparator();
    }
    return BIEF_COMPARATOR;
  }
  private static SingulariteComparator SINGULARITE_COMPARATOR;
  public final static SingulariteComparator SINGULARITE_COMPARATOR() {
    if (SINGULARITE_COMPARATOR == null) {
      SINGULARITE_COMPARATOR= new SingulariteComparator();
    }
    return SINGULARITE_COMPARATOR;
  }
  private static LimiteComparator LIMITE_COMPARATOR;
  public final static LimiteComparator LIMITE_COMPARATOR() {
    if (LIMITE_COMPARATOR == null) {
      LIMITE_COMPARATOR= new LimiteComparator();
    }
    return LIMITE_COMPARATOR;
  }
  private static LoiclmComparator LOICLM_COMPARATOR;
  public final static LoiclmComparator LOICLM_COMPARATOR() {
    if (LOICLM_COMPARATOR == null) {
      LOICLM_COMPARATOR= new LoiclmComparator();
    }
    return LOICLM_COMPARATOR;
  }
  private static LoiclmPointComparator LOICLMPOINT_TARAGE_COMPARATOR;
  public final static LoiclmPointComparator LOICLMPOINT_TARAGE_COMPARATOR() {
    if (LOICLMPOINT_TARAGE_COMPARATOR == null) {
      LOICLMPOINT_TARAGE_COMPARATOR=
        new LoiclmPointComparator(LidoResource.LIMITE.TARAGE);
    }
    return LOICLMPOINT_TARAGE_COMPARATOR;
  }
  private static LoiclmPointComparator LOICLMPOINT_HYDRO_COMPARATOR;
  public final static LoiclmPointComparator LOICLMPOINT_HYDRO_COMPARATOR() {
    if (LOICLMPOINT_HYDRO_COMPARATOR == null) {
      LOICLMPOINT_HYDRO_COMPARATOR=
        new LoiclmPointComparator(LidoResource.LIMITE.HYDRO);
    }
    return LOICLMPOINT_HYDRO_COMPARATOR;
  }
  private static LoiclmPointComparator LOICLMPOINT_LIMNI_COMPARATOR;
  public final static LoiclmPointComparator LOICLMPOINT_LIMNI_COMPARATOR() {
    if (LOICLMPOINT_LIMNI_COMPARATOR == null) {
      LOICLMPOINT_LIMNI_COMPARATOR=
        new LoiclmPointComparator(LidoResource.LIMITE.LIMNI);
    }
    return LOICLMPOINT_LIMNI_COMPARATOR;
  }
  private static ApportComparator APPORT_COMPARATOR;
  public final static ApportComparator APPORT_COMPARATOR() {
    if (APPORT_COMPARATOR == null) {
      APPORT_COMPARATOR= new ApportComparator();
    }
    return APPORT_COMPARATOR;
  }
  private static PerteComparator PERTE_COMPARATOR;
  public final static PerteComparator PERTE_COMPARATOR() {
    if (PERTE_COMPARATOR == null) {
      PERTE_COMPARATOR= new PerteComparator();
    }
    return PERTE_COMPARATOR;
  }
  private static LaisseComparator LAISSE_COMPARATOR;
  public final static LaisseComparator LAISSE_COMPARATOR() {
    if (LAISSE_COMPARATOR == null) {
      LAISSE_COMPARATOR= new LaisseComparator();
    }
    return LAISSE_COMPARATOR;
  }
  private static PermSection2Comparator PERMSECTION2_COMPARATOR;
  public final static PermSection2Comparator PERMSECTION2_COMPARATOR() {
    if (PERMSECTION2_COMPARATOR == null) {
      PERMSECTION2_COMPARATOR= new PermSection2Comparator();
    }
    return PERMSECTION2_COMPARATOR;
  }
  private static PermSection3Comparator PERMSECTION3_COMPARATOR;
  public final static PermSection3Comparator PERMSECTION3_COMPARATOR() {
    if (PERMSECTION3_COMPARATOR == null) {
      PERMSECTION3_COMPARATOR= new PermSection3Comparator();
    }
    return PERMSECTION3_COMPARATOR;
  }
  public LidoParamsHelper() {
    projet_= null;
    phProfil_= null;
    phNoeud_= null;
    phBief_= null;
    phSingularite_= null;
    phLoisng_= null;
    phLimite_= null;
    phLoiclm_= null;
    phApport_= null;
    phPerte_= null;
    phLaisse_= null;
    phPermSection2_= null;
    phPermSection3_= null;
    phCalcul_= null;
  }
  public void setProjet(final FudaaProjet p) {
    if (p == projet_) {
      return;
    }
    if (projet_ != null) {
      projet_.removeFudaaProjetListener(this);
    }
    projet_= p;
    projet_.addFudaaProjetListener(this);
  }
  public BuInformationsDocument getInformationsDocument() {
    if (projet_ != null) {
      return projet_.getInformationsDocument();
    }
      return new BuInformationsDocument();
  }
  public void dataChanged(final FudaaProjetEvent e) {
    final FudaaProjet p= (FudaaProjet)e.getSource();
    if (phProfil_ != null) {
      phProfil_.setProjet(p);
    }
    if (phNoeud_ != null) {
      phNoeud_.setProjet(p);
    }
    if (phBief_ != null) {
      phBief_.setProjet(p);
    }
    if (phSingularite_ != null) {
      phSingularite_.setProjet(p);
    }
    if (phLoisng_ != null) {
      phLoisng_.setProjet(p);
    }
    if (phLimite_ != null) {
      phLimite_.setProjet(p);
    }
    if (phLoiclm_ != null) {
      phLoiclm_.setProjet(p);
    }
    if (phApport_ != null) {
      phApport_.setProjet(p);
    }
    if (phPerte_ != null) {
      phPerte_.setProjet(p);
    }
    if (phLaisse_ != null) {
      phLaisse_.setProjet(p);
    }
    if (phPermSection2_ != null) {
      phPermSection2_.setProjet(p);
    }
    if (phPermSection3_ != null) {
      phPermSection3_.setProjet(p);
    }
    if (phCalcul_ != null) {
      phCalcul_.setProjet(p);
    }
  }
  public void statusChanged(final FudaaProjetEvent e) {
    if ((e.getID() == FudaaProjetEvent.PROJECT_OPENED)
      || (e.getID() == FudaaProjetEvent.PROJECT_CLOSED)) {
      dataChanged(e);
    }
  }
  public void initialiseParametres() {
    projet_.addParam(LidoResource.PRO, creeParametresPRO());
    projet_.addParam(LidoResource.RZO, creeParametresRZO());
    projet_.addParam(LidoResource.CAL, creeParametresCAL());
    projet_.addParam(LidoResource.CLM, creeParametresCLM());
    projet_.addParam(LidoResource.SNG, creeParametresSNG());
    projet_.addParam(LidoResource.EXT, creeParametresEXT());
  }
  public LoiclmPointComparator LOICLMPOINT_COMPARATOR(final int type) {
    if (type == LidoResource.LIMITE.TARAGE) {
      return LOICLMPOINT_TARAGE_COMPARATOR();
    } else if (type == LidoResource.LIMITE.HYDRO) {
      return LOICLMPOINT_HYDRO_COMPARATOR();
    } else if (type == LidoResource.LIMITE.LIMNI) {
      return LOICLMPOINT_LIMNI_COMPARATOR();
    } else if (type == LidoResource.LIMITE.MAREE) {
      return LOICLMPOINT_LIMNI_COMPARATOR();
    }
    return null;
  }
  public LidoPH_Profil PROFIL() {
    if (phProfil_ == null) {
      phProfil_= new LidoPH_Profil(projet_, this);
    }
    return phProfil_;
  }
  public LidoPH_Noeud NOEUD() {
    if (phNoeud_ == null) {
      phNoeud_= new LidoPH_Noeud(projet_, this);
    }
    return phNoeud_;
  }
  public LidoPH_Bief BIEF() {
    if (phBief_ == null) {
      phBief_= new LidoPH_Bief(projet_, this);
    }
    return phBief_;
  }
  public LidoPH_Singularite SINGULARITE() {
    if (phSingularite_ == null) {
      phSingularite_= new LidoPH_Singularite(projet_, this);
    }
    return phSingularite_;
  }
  public LidoPH_Loisng LOISNG() {
    if (phLoisng_ == null) {
      phLoisng_= new LidoPH_Loisng(projet_, this);
    }
    return phLoisng_;
  }
  public LidoPH_Limite LIMITE() {
    if (phLimite_ == null) {
      phLimite_= new LidoPH_Limite(projet_, this);
    }
    return phLimite_;
  }
  public LidoPH_Loiclm LOICLM() {
    if (phLoiclm_ == null) {
      phLoiclm_= new LidoPH_Loiclm(projet_, this);
    }
    return phLoiclm_;
  }
  public LidoPH_Apport APPORT() {
    if (phApport_ == null) {
      phApport_= new LidoPH_Apport(projet_, this);
    }
    return phApport_;
  }
  public LidoPH_Perte PERTE() {
    if (phPerte_ == null) {
      phPerte_= new LidoPH_Perte(projet_, this);
    }
    return phPerte_;
  }
  public LidoPH_Laisse LAISSE() {
    if (phLaisse_ == null) {
      phLaisse_= new LidoPH_Laisse(projet_, this);
    }
    System.out.println("laisse");
    return phLaisse_;
  }
  public LidoPH_PermSection2 PERMSECTION2() {
    if (phPermSection2_ == null) {
      phPermSection2_= new LidoPH_PermSection2(projet_, this);
    }
    return phPermSection2_;
  }
  public LidoPH_PermSection3 PERMSECTION3() {
    if (phPermSection3_ == null) {
      phPermSection3_= new LidoPH_PermSection3(projet_, this);
    }
    return phPermSection3_;
  }
  public LidoPH_Calcul CALCUL() {
    if (phCalcul_ == null) {
      phCalcul_= new LidoPH_Calcul(projet_, this);
    }
    return phCalcul_;
  }
  // creeParametres
  public static SParametresPRO creeParametresPRO() {
    System.err.println("LidoParamsHelper: creating new PRO struct");
    final SParametresPRO pro= new SParametresPRO();
    pro.status= true;
    pro.titrePRO=
      new String[] {
        "ETUDE LIDO 2.0 : DEFINITION DES PROFILS",
        "",
        "",
        "",
        "",
        "" };
    pro.entreeProfils= 1; // par points
    pro.zoneStock= 0; // non
    pro.nbProfils= 0;
    pro.profilsBief= new SParametresBiefBlocPRO[0];
    pro.donneesBief= new SParametresDonBiefBlocPRO[0];
    return pro;
  }
  public static SParametresRZO creeParametresRZO() {
    System.err.println("LidoParamsHelper: creating new RZO struct");
    final SParametresRZO rzo= new SParametresRZO();
    rzo.status= true;
    rzo.titreRZO= new String[] { "ETUDE LIDO 2.0 : DEFINITION DU RESEAU", "" };
    rzo.nbBief= 0;
    rzo.nbNoeud= 0;
    rzo.nbLimi= 0;
    rzo.blocSitus= new SParametresBiefSituBlocRZO();
    rzo.blocSitus.ligneSitu= new SParametresBiefSituLigneRZO[0];
    rzo.blocNoeuds= new SParametresBiefNoeudBlocRZO();
    rzo.blocNoeuds.ligneNoeud= new SParametresBiefNoeudLigneRZO[0];
    rzo.blocLims= new SParametresBiefLimBlocRZO();
    rzo.blocLims.ligneLim= new SParametresBiefLimLigneRZO[0];
    rzo.blocSings= new SParametresBiefSingBlocRZO();
    rzo.blocSings.ligneSing= new SParametresBiefSingLigneRZO[0];
    return rzo;
  }
  public static SParametresCAL creeParametresCAL() {
    System.err.println("LidoParamsHelper: creating new CAL struct");
    final SParametresCAL cal= new SParametresCAL();
    cal.status= true;
    cal.titreCAL=
      new String[] { "ETUDE LIDO 2.0 : DONNEES GENERALES DU CALCUL", "" };
    cal.genCal= new SParametresGenCAL();
    cal.genCal.code= "REZO";
    cal.genCal.regime= "P";
    cal.genCal.seuil= "NON";
    cal.genCal.compLits= "MINEUR/MAJEUR";
    cal.genCal.frotPV= "NON";
    cal.genCal.impGeo= "NON";
    cal.genCal.impPlan= "NON";
    cal.genCal.impRez= "NON";
    cal.genCal.impHyd= "NON";
    cal.genCal.biefXOrigi= 0.;
    cal.genCal.biefXFin= 0.;
    cal.fic= new SParametresFicCAL();
    cal.fic.nFGeo= 20;
    cal.fic.nFSing= 21;
    cal.fic.nFRez= 26;
    cal.fic.nFLign= 22;
    cal.fic.nFCLim= 23;
    cal.fic.nFSLec= 24;
    cal.fic.nFSEcr= 25;
    cal.planimetrage= new SParametresPlaniCAL();
    cal.planimetrage.varPlanNbVal= LidoResource.PLANIMETRAGE.NBVALEURS_PLANI;
    cal.planimetrage.varPlanNbPas= 0;
    cal.planimetrage.varsPlanimetrage= new SParametresPasLigneCAL[0];
    cal.temporel= new SParametresTempCAL();
    cal.temporel.tInit= 1.;
    cal.temporel.tMax= 1.;
    cal.temporel.pas2T= 1.;
    cal.temporel.numDerPaStoc= 0;
    cal.temporel.pas2TImp= 1.;
    cal.temporel.pas2TStoc= 1.;
    cal.sections= new SParametresSectionsCAL();
    cal.sections.nChoix= 1;
    cal.sections.series= new SParametresSerieBlocCAL();
    cal.sections.series.nbSeries= 0;
    cal.sections.series.ligne= new SParametresSerieLigneCAL[0];
    cal.sections.sections= new SParametresSectionBlocCAL();
    cal.sections.sections.nbSect= 0;
    cal.sections.sections.ligne= new SParametresSectionLigneCAL[0];
    return cal;
  }
  public static SParametresCLM creeParametresCLM() {
    System.err.println("LidoParamsHelper: creating new CLM struct");
    final SParametresCLM clm= new SParametresCLM();
    clm.status= true;
    clm.titreCLM= "ETUDE LIDO 2.0 : CONDITIONS LIMITES";
    clm.nbCondLim= 0;
    clm.condLimites= new SParametresCondLimBlocCLM[0];
    clm.sousTitrePerte= "DEFINITION DES PERTES SINGULIERES";
    clm.nbPerte= 0;
    clm.perte= new SParametresPerteLigneCLM[0];
    clm.sousTitreApport= "DEFINITION DES APPORTS ET SOUTIRAGES";
    clm.nbApport= 0;
    clm.apport= new SParametresApportLigneCLM[0];
    return clm;
  }
  public static SParametresSNG creeParametresSNG() {
    System.err.println("LidoParamsHelper: creating new SNG struct");
    final SParametresSNG sng= new SParametresSNG();
    sng.status= true;
    sng.titre= "ETUDE LIDO 2.0 : SINGULARITES";
    sng.nbSing= 0;
    sng.nbMaxParam= 5; // toujours 5
    sng.nbValTD= 20; // 5, 10, 15, ou 20 (1, 2, 3, ou 4 lignes de nombres)
    sng.singularites= new SParametresSingBlocSNG[0];
    return sng;
  }
  public static SParametresEXT creeParametresEXT() {
    System.err.println("LidoParamsHelper: creating new EXT struct");
    final SParametresEXT ext= new SParametresEXT();
    ext.laisses= new SParametresLaisseLigneEXT[0];
    return ext;
  }
  public void hackPermanentMaille(final String cas) {
    final SParametresCAL cal= (SParametresCAL)projet_.getParam(LidoResource.CAL);
    final SParametresCLM clm= (SParametresCLM)projet_.getParam(LidoResource.CLM);
    if ("PRE-TRAITEMENT".equals(cas)) {
      System.err.println(
        "LidoParamsHelper: passage en mode sp�cial MAILLE PERM");
      // generer le faux LIG
      final SParametresLIG lig= new SParametresLIG();
      lig.status= true;
      lig.titreLIG=
        new String[] {
          "FAUX LIG POUR RESEAU MAILLE PERMANENT",
          "GENERE PAR FUDAA-LIDO",
          "*************************************" };
      lig.nbSections= 2;
      lig.nbBief= 1;
      lig.delimitBief= new SParametresLimBiefLIG[1];
      lig.delimitBief[0]= new SParametresLimBiefLIG();
      lig.delimitBief[0].sectionDebut= 1;
      lig.delimitBief[0].sectionFin= 2;
      lig.x= new double[2];
      lig.x[0]= cal.genCal.biefXOrigi;
      lig.x[1]= cal.genCal.biefXFin;
      lig.z= new double[2];
      lig.z[0]= LOICLM().getMaxZLimni();
      lig.z[1]= lig.z[0];
      lig.q= new double[2];
      lig.q[0]= 0.;
      lig.q[1]= 0.;
      lig.zref=
        lig.vmin=
          lig.vmaj=
            lig.rgc=
              lig.rdc=
                lig.st1=
                  lig.st2=
                    lig.rmin= lig.rmaj= lig.vol= lig.vols= lig.frou= null;
      projet_.addParam(LidoResource.LIG, lig);
      // modif du CAL
      cal.genCal.regime= "NP";
      cal.temporel.tInit= 0.;
      cal.temporel.tMax= 345600.;
      cal.temporel.pas2T= 7200.;
      cal.temporel.pas2TImp= 345600.;
      cal.temporel.pas2TStoc= 345600.;
      // modif des CLM
      final int nbLimni= LOICLM().getNbrCondByType(LidoResource.LIMITE.LIMNI);
      for (int i= 0; i < clm.nbCondLim; i++) {
        // limnis
        if ((clm.condLimites[i].typLoi == LidoResource.LIMITE.LIMNI)) {
          SParametresCondLimPointLigneCLM[] old= clm.condLimites[i].point;
          clm.condLimites[i].point= new SParametresCondLimPointLigneCLM[3];
          clm.condLimites[i].point[0]= new SParametresCondLimPointLigneCLM();
          clm.condLimites[i].point[0].tLim= 0.;
          if (nbLimni > 1) {
            clm.condLimites[i].point[0].zLim= LOICLM().getMaxZLimni();
          } else {
            clm.condLimites[i].point[0].zLim= old[0].zLim;
          }
          clm.condLimites[i].point[1]= new SParametresCondLimPointLigneCLM();
          clm.condLimites[i].point[1].tLim= 86400.;
          clm.condLimites[i].point[1].zLim= old[0].zLim;
          clm.condLimites[i].point[2]= new SParametresCondLimPointLigneCLM();
          clm.condLimites[i].point[2].tLim= 345600.;
          clm.condLimites[i].point[2].zLim= old[0].zLim;
          clm.condLimites[i].nbPoints= 3;
          old= null;
        } else          // hydros
          if ((clm.condLimites[i].typLoi == LidoResource.LIMITE.HYDRO)) {
            SParametresCondLimPointLigneCLM[] old= clm.condLimites[i].point;
            clm.condLimites[i].point= new SParametresCondLimPointLigneCLM[3];
            clm.condLimites[i].point[0]= new SParametresCondLimPointLigneCLM();
            clm.condLimites[i].point[0].tLim= 0.;
            clm.condLimites[i].point[0].qLim= 0.;
            clm.condLimites[i].point[1]= new SParametresCondLimPointLigneCLM();
            clm.condLimites[i].point[1].tLim= 86400.;
            clm.condLimites[i].point[1].qLim= old[0].qLim;
            clm.condLimites[i].point[2]= new SParametresCondLimPointLigneCLM();
            clm.condLimites[i].point[2].tLim= 345600.;
            clm.condLimites[i].point[2].qLim= old[0].qLim;
            clm.condLimites[i].nbPoints= 3;
            old= null;
          }
      }
    } else if ("POST-TRAITEMENT".equals(cas)) {
      System.err.println("LidoParamsHelper: restauration du mode normal");
      // restauration du CAL
      cal.genCal.regime= "P";
      cal.temporel.tInit= 1.;
      cal.temporel.tMax= 1.;
      cal.temporel.pas2T= 1.;
      cal.temporel.pas2TImp= 1.;
      cal.temporel.pas2TStoc= 1.;
      // restauration des CLM
      for (int i= 0; i < clm.nbCondLim; i++) {
        // limnis
        if ((clm.condLimites[i].typLoi == LidoResource.LIMITE.LIMNI)) {
          SParametresCondLimPointLigneCLM[] old= clm.condLimites[i].point;
          clm.condLimites[i].point= new SParametresCondLimPointLigneCLM[2];
          clm.condLimites[i].point[0]= new SParametresCondLimPointLigneCLM();
          clm.condLimites[i].point[0].tLim= 1.;
          clm.condLimites[i].point[0].zLim= old[1].zLim;
          clm.condLimites[i].point[1]= new SParametresCondLimPointLigneCLM();
          clm.condLimites[i].point[1].tLim= 2.;
          clm.condLimites[i].point[1].zLim= old[1].zLim;
          clm.condLimites[i].nbPoints= 2;
          old= null;
        } else          // hydros
          if ((clm.condLimites[i].typLoi == LidoResource.LIMITE.HYDRO)) {
            SParametresCondLimPointLigneCLM[] old= clm.condLimites[i].point;
            clm.condLimites[i].point= new SParametresCondLimPointLigneCLM[2];
            clm.condLimites[i].point[0]= new SParametresCondLimPointLigneCLM();
            clm.condLimites[i].point[0].tLim= 1.;
            clm.condLimites[i].point[0].qLim= old[1].qLim;
            clm.condLimites[i].point[1]= new SParametresCondLimPointLigneCLM();
            clm.condLimites[i].point[1].tLim= 2.;
            clm.condLimites[i].point[1].qLim= old[1].qLim;
            clm.condLimites[i].nbPoints= 2;
            old= null;
          }
      }
    }
  }
}
