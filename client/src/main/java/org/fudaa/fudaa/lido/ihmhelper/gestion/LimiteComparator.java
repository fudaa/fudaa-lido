/*
 * @file         LimiteComparator.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Comparator;

import org.fudaa.dodico.corba.lido.SParametresBiefLimLigneRZO;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:03 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LimiteComparator implements Comparator {
  LimiteComparator() {}
  public int compare(final Object o1, final Object o2) {
    return ((SParametresBiefLimLigneRZO)o1).numLoi
      < ((SParametresBiefLimLigneRZO)o2).numLoi
      ? -1
      : ((SParametresBiefLimLigneRZO)o1).numLoi
        == ((SParametresBiefLimLigneRZO)o2).numLoi
      ? 0
      : 1;
  }
  public boolean equals(final Object obj) {
    return false;
  }
}
