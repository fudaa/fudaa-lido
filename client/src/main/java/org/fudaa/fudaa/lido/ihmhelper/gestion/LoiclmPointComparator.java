/*
 * @file         LoiclmPointComparator.java
 * @creation     2000-04-04
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Comparator;

import org.fudaa.dodico.corba.lido.SParametresCondLimPointLigneCLM;

import org.fudaa.fudaa.lido.LidoResource;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LoiclmPointComparator implements Comparator {
  private int type_;
  LoiclmPointComparator(final int type) {
    type_= type;
  }
  public int compare(final Object o1, final Object o2) {
    int res= 0;
    if (type_ == LidoResource.LIMITE.TARAGE) {
      res=
        ((SParametresCondLimPointLigneCLM)o1).qLim
          < ((SParametresCondLimPointLigneCLM)o2).qLim
          ? -1
          : ((SParametresCondLimPointLigneCLM)o1).qLim
            == ((SParametresCondLimPointLigneCLM)o2).qLim
          ? 0
          : 1;
    } else {
      res=
        ((SParametresCondLimPointLigneCLM)o1).tLim
          < ((SParametresCondLimPointLigneCLM)o2).tLim
          ? -1
          : ((SParametresCondLimPointLigneCLM)o1).tLim
            == ((SParametresCondLimPointLigneCLM)o2).tLim
          ? 0
          : 1;
    }
    return res;
  }
  public boolean equals(final Object obj) {
    return false;
  }
}
