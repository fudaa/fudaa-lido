/*
 * @file         ProfilComparator.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.ihmhelper.gestion;
import java.util.Comparator;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:02 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class ProfilComparator implements Comparator {
  ProfilComparator() {}
  public int compare(final Object o1, final Object o2) {
    return ((SParametresBiefBlocPRO)o1).abscisse
      < ((SParametresBiefBlocPRO)o2).abscisse
      ? -1
      : ((SParametresBiefBlocPRO)o1).abscisse
        == ((SParametresBiefBlocPRO)o2).abscisse
      ? 0
      : 1;
  }
  public boolean equals(final Object obj) {
    return false;
  }
}
