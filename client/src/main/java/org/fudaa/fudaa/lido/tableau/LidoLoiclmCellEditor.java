/*
 * @file         LidoLoiclmCellEditor.java
 * @creation     1999-10-10
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JToolTip;
import javax.swing.JWindow;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;

import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoLoiclmCellEditor
  extends JComboBox
  implements TableCellEditor {
  private SParametresCondLimBlocCLM[] lois_;
  JWindow popup_;
  public LidoLoiclmCellEditor() {
    super();
    lois_= null;
    final MyLoiclmRenderer r= new MyLoiclmRenderer();
    final JToolTip t= new JToolTip();
    popup_= new JWindow();
    //popup_.setTitle(null);
    popup_.getContentPane().setLayout(new BorderLayout());
    //popup_.setBorder(new EmptyBorder(new Insets(0,0,0,0)));
    popup_.getContentPane().add(BorderLayout.CENTER, t);
    r.addPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange(final PropertyChangeEvent e) {
        if ("selected".equals(e.getPropertyName())) {
          if (e.getNewValue() instanceof SParametresCondLimBlocCLM) {
            final SParametresCondLimBlocCLM item=
              (SParametresCondLimBlocCLM)e.getNewValue();
            t.setTipText(item.description);
          } else {
            t.setTipText("Cr�er une nouvelle loi");
          }
          popup_.invalidate();
          popup_.doLayout();
          popup_.setSize(t.getPreferredSize());
          final Point p= getLocationOnScreen();
          p.y += (1 + ((Integer)e.getOldValue()).intValue())
            * (getHeight() + 1);
          p.x += getWidth();
          popup_.setLocation(p);
          if (popup_.isShowing()) {
            popup_.repaint();
          } else {
            popup_.pack();
            popup_.show();
            popup_.toFront();
          }
          popup_.setLocation(p);
        }
      }
    });
    this.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        if (popup_.isShowing()) {
          popup_.hide();
        }
      }
    });
    setRenderer(r);
    addItem("Nouveau");
  }
  public void hidePopup() {
    System.err.println("hidePopup");
    if (popup_.isShowing()) {
      popup_.hide();
    }
    super.hidePopup();
  }
  public void setLoisclm(final SParametresCondLimBlocCLM[] o) {
    if ((o == lois_) || (o == null)) {
      return;
    }
    final SParametresCondLimBlocCLM[] vp= lois_;
    lois_= o;
    removeAllItems();
    for (int i= 0; i < lois_.length; i++) {
      addItem(lois_[i]);
    }
    addItem("Nouveau");
    firePropertyChange("loiclm", vp, lois_);
  }
  // cellEditor
  public Component getTableCellEditorComponent(
    final JTable table,
    final Object value,
    final boolean isSelected,
    final int row,
    final int column) {
    if (column == 1) {
      if (lois_ != null) {
        setSelectedItem("Nouveau");
        final int num= ((Integer)value).intValue();
        for (int i= 0; i < lois_.length; i++) {
          if (lois_[i].numCondLim == num) {
            setSelectedIndex(i);
          }
        }
      }
      return this;
    }
    return new JLabel("???");
  }
  private final Vector list_= new Vector();
  public void addCellEditorListener(final CellEditorListener l) {
    if (!list_.contains(l)) {
      list_.add(l);
    }
  }
  public void removeCellEditorListener(final CellEditorListener l) {
    if (list_.contains(l)) {
      list_.remove(l);
    }
  }
  public void cancelCellEditing() {
    for (int i= 0; i < list_.size(); i++) {
      ((CellEditorListener)list_.get(i)).editingCanceled(new ChangeEvent(this));
    }
  }
  public Object getCellEditorValue() {
    return getSelectedItem();
  }
  public boolean isCellEditable(final EventObject e) {
    return true;
  }
  public boolean shouldSelectCell(final EventObject e) {
    return true;
  }
  public boolean stopCellEditing() {
    for (int i= 0; i < list_.size(); i++) {
      ((CellEditorListener)list_.get(i)).editingStopped(new ChangeEvent(this));
    }
    return true;
  }
}
class MyLoiclmRenderer extends JLabel implements ListCellRenderer {
  public MyLoiclmRenderer() {
    setOpaque(true);
  }
  public Component getListCellRendererComponent(
    final JList list,
    final Object value,
    final int index,
    final boolean isSelected,
    final boolean cellHasFocus) {
    if (value instanceof String) {
      setText((String)value);
    } else {
      final SParametresCondLimBlocCLM item= (SParametresCondLimBlocCLM)value;
      setText("" + item.numCondLim);
    }
    setBackground(
      isSelected
        ? UIManager.getColor("List.selectionBackground")
        : UIManager.getColor("List.background"));
    setForeground(
      isSelected
        ? UIManager.getColor("List.selectionForeground")
        : UIManager.getColor("List.foreground"));
    if (isSelected) {
      firePropertyChange("selected", new Integer(index), value);
    }
    return this;
  }
}
