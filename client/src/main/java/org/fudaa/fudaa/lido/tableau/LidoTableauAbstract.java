/*
 *  @file         LidoTableauAbstract.java
 *  @creation     20 juin 2003
 *  @modification $Date: 2006-09-19 15:05:00 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @author deniger
 * @version $Id: LidoTableauAbstract.java,v 1.5 2006-09-19 15:05:00 deniger Exp $
 */
public abstract class LidoTableauAbstract extends JTable {
  TableCellEditor doubleEditor_;
  TableCellEditor intEditor_;
  TableCellEditor stringEditor_;
  TableCellRenderer doubleRenderer_;
  TableCellRenderer defaultRenderer_;
  public TableCellEditor getDefaultEditor(final Class columnClass) {
    if (columnClass == Double.class) {
      if (doubleEditor_ == null) {
        doubleEditor_= LidoParamsHelper.createDoubleCellEditor();
      }
      return doubleEditor_;
    } else if (columnClass == Integer.class) {
      if (intEditor_ == null) {
        intEditor_= LidoParamsHelper.createIntegerCellEditor();
      }
      return intEditor_;
    } else if (columnClass == String.class) {
      if (stringEditor_ == null) {
        stringEditor_= LidoParamsHelper.createStringCellEditor();
      }
      return stringEditor_;
    } else {
      return super.getDefaultEditor(columnClass);
    }
  }
  /**
   *
   */
  public TableCellRenderer getDefaultRenderer(final Class columnClass) {
    if (columnClass == Double.class) {
      if (doubleRenderer_ == null) {
        doubleRenderer_= LidoParamsHelper.createDoubleCellRenderer();
      }
      return doubleRenderer_;
    }
    if (defaultRenderer_ == null) {
      defaultRenderer_= new BuTableCellRenderer();
    }
    return defaultRenderer_;
  }
}
