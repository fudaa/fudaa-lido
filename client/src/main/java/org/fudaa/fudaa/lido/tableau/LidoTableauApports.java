/*
 * @file         LidoTableauApports.java
 * @creation     1999-09-09
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import org.fudaa.dodico.corba.lido.SParametresApportLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauApports extends LidoTableauBase {
  private LidoLoiclmCellEditor loiclmEditor_;
  public LidoTableauApports(final LidoLoiclmCellEditor loiclmEditor) {
    super();
    loiclmEditor_= loiclmEditor;
    init();
  }
  private void init() {
    setModel(new LidoTableauApportsModel(new SParametresApportLigneCLM[0]));
    ((LidoTableauApportsModel)getModel()).setTableau(this);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    int n = getModel().getColumnCount();
    //    TableColumnModel model = getColumnModel();
    //    for (int i = 0; i < n; i++)
    //    {
    //      if(( i!=3) && (i!=0))
    //      model.getColumn(i).setCellRenderer(tcr);
    //    }
    //     
    //colonne 1
    getColumnModel().getColumn(1).setCellEditor(loiclmEditor_);
    //colonne 0 et 3
    //    TableCellRenderer tcrDouble = LidoParamsHelper.createDoubleCellRenderer();
    //    TableCellEditor tceDouble =LidoParamsHelper.createDoubleCellEditor();
    //    model.getColumn(0).setCellEditor(tceDouble);
    //    model.getColumn(0).setCellRenderer(tcrDouble);
    //    model.getColumn(3).setCellEditor(tceDouble);
    //    model.getColumn(3).setCellRenderer(tcrDouble);
  }
  public void reinitialise() {
    final SParametresApportLigneCLM[] apps=
      (SParametresApportLigneCLM[])getObjects(false);
    if (apps == null) {
      return;
    }
    //    setModel(new LidoTableauApportsModel(apps));
    //    ((LidoTableauApportsModel) getModel()).setTableau(this);
    //    //((LidoTableauApportsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    //BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    //BuTableCellEditor tceInteger=new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble =
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    int n = getModel().getColumnCount();
    //    TableColumnModel model = getColumnModel();
    //    for (int i = 0; i < n; i++)
    //      model.getColumn(i).setCellRenderer(tcr);
    //      
    //    model.getColumn(0).setCellEditor(tceDouble);
    //    model.getColumn(1).setCellEditor(loiclmEditor_);
    //    model.getColumn(3).setCellEditor(tceDouble);
     ((LidoTableauApportsModel)getModel()).setObjects(apps);
    tableChanged(new TableModelEvent(getModel()));
  }
  protected String getPropertyName() {
    return "apports";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "xApport";
        break;
      case 1 :
        r= "numLoi";
        break;
      case 2 :
        r= "typLoi";
        break;
      case 3 :
        r= "coefApport";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.APPORT_COMPARATOR();
  }
  void numLoiChanged(final int row) {
    firePropertyChange("numloichanged", null, getObjects(false)[row]);
  }
}
class LidoTableauApportsModel extends LidoTableauBaseModel {
  LidoTableauApports table_;
  public LidoTableauApportsModel(final SParametresApportLigneCLM[] _apps) {
    super(_apps);
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresApportLigneCLM[taille];
  }
  public void setTableau(final LidoTableauApports table) {
    table_= table;
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Double.class; // xApport
      case 1 :
        return Integer.class; // no de loi
      case 2 :
        return String.class; // type de loi
      case 3 :
        return Double.class; // coef app
      case 4 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 5;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "abscisse";
        break;
      case 1 :
        r= "n� loi";
        break;
      case 2 :
        r= "type loi";
        break;
      case 3 :
        r= "coefficient";
        break;
      case 4 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresApportLigneCLM[] apps= (SParametresApportLigneCLM[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Double(apps[low_ + row].xApport);
          break;
        case 1 :
          r= new Integer(apps[low_ + row].numLoi);
          break;
        case 2 :
          {
            switch (apps[low_ + row].typLoi) {
              case LidoResource.LIMITE.TARAGE :
                r= "Tarage";
                break;
              case LidoResource.LIMITE.LIMNI :
                r= "Limni";
                break;
              case LidoResource.LIMITE.MAREE :
                r= "Mar�e";
                break;
              case LidoResource.LIMITE.HYDRO :
                r= "Hydro";
                break;
              default :
                r= "?";
                break;
            }
            break;
          }
        case 3 :
          r= new Double(apps[low_ + row].coefApport);
          break;
        case 4 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column != 4) && (column != 2));
  }
  public void setValueAt(Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresApportLigneCLM[] apps=
        (SParametresApportLigneCLM[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 0 :
          apps[low_ + row].xApport= ((Double)value).doubleValue();
          break;
        case 1 :
          {
            if (value instanceof String) {
              apps[low_ + row].numLoi= 0;
            } else {
              apps[low_ + row].numLoi=
                ((SParametresCondLimBlocCLM)value).numCondLim;
            }
            value= new Integer(apps[low_ + row].numLoi);
            table_.numLoiChanged(low_ + row);
            break;
          }
          //case 2: apps_[low_+row].typLoi=((Integer)value).intValue(); break;
        case 3 :
          apps[low_ + row].coefApport= ((Double)value).doubleValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            apps[low_ + row],
            "apport " + (apps[low_ + row].xApport)));
      }
    }
  }
}
