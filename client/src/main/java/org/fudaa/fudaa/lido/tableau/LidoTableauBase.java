/*
 * @file         LidoTableauBase.java
 * @creation     1999-11-26
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Comparator;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;

import org.fudaa.fudaa.lido.dnd.LidoDnDTransfer;
import org.fudaa.fudaa.lido.dnd.LidoDnDTransferParam;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public abstract class LidoTableauBase
  extends LidoTableauAbstract
  implements
    PropertyChangeListener,
    DropTargetListener,
    DragSourceListener,
    DragGestureListener {
  //DND
  //private DropTarget dndDropTarget_;
  private boolean dndAcceptFlavor_;
  protected DragSource dndDragSrc_;
  private DataFlavor dndFlavor_;
  private int dndDropCol_;
  private int dndDropRow_;
  private LidoDnDTransferParam dndDropParam_;
  public LidoTableauBase() {
    super();
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //setSelectionModel(new LidoTableauBaseSelectionModel());
  }
  public void editeCellule(final int lig, final int col) {
    final int[] restriction= ((LidoTableauBaseModel)getModel()).getRestriction();
    ((LidoTableauBaseModel)getModel()).setDirty(lig, true);
    editCellAt(lig, col);
    final JComponent parent= (JComponent)getParent();
    if (parent instanceof JViewport) {
      if (lig <= restriction[0]) {
        System.err.println("LidoTableauBase: scrolling up viewport");
        ((JScrollPane)parent.getParent()).getVerticalScrollBar().setValue(
          Integer.MIN_VALUE);
      } else if (lig >= restriction[1] - 1) {
        System.err.println("LidoTableauBase: scrolling down viewport");
        ((JScrollPane)parent.getParent()).getVerticalScrollBar().setValue(
          Integer.MAX_VALUE);
      }
    }
    repaint();
  }
  public int getPositionNouveau() {
    return ((LidoTableauBaseModel)getModel()).getRestriction()[0];
  }
  public int getSelectedIndex() {
    final int row= getSelectedRow();
    if (row < 0) {
      return -1;
    }
    return ((LidoTableauBaseModel)getModel()).getRestriction()[0] + row;
  }
  public Object getSelectedObject() {
    final int row= getSelectedRow();
    if (row < 0) {
      return null;
    }
    return ((LidoTableauBaseModel)getModel()).getObject(row);
  }
  public Object[] getSelectedObjects() {
    return ((LidoTableauBaseModel)getModel()).getObjects(getSelectedRows());
  }
  public abstract void reinitialise();
  public void restreintA(final int low, final int high) {
    //((LidoTableauBaseSelectionModel)getSelectionModel()).setOffset(low);
     ((LidoTableauBaseModel)getModel()).restreintA(low, high);
    repaint();
  }
  public boolean verifieContraintes() {
    return true;
  }
  // evenements
  public void propertyChange(final PropertyChangeEvent e) {
    if (getPropertyName().equals(e.getPropertyName())) {
      System.err.println("property " + getPropertyName());
      final Object[] nouvs= (Object[])e.getNewValue();
      final boolean prevsort= isAutosort();
      setAutosort(false);
      setObjects(nouvs);
      setAutosort(prevsort);
      repaint();
    }
  }
  protected abstract String getPropertyName();
  protected String getObjectFieldNameByColumnLink(final int col) {
    return "";
  }
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  private Object[] objs_;
  public Object[] getObjects() {
    if (autosort_) {
      java.util.Arrays.sort(objs_, getComparator());
    }
    return objs_;
  }
  public Object[] getObjects(final boolean autosort) {
    if (autosort) {
      java.util.Arrays.sort(objs_, getComparator());
    }
    return objs_;
  }
  protected abstract Comparator getComparator();
  public void setObjects(final Object[] _objs) {
    if (objs_ == _objs) {
      return;
    }
    final Object[] vp= objs_;
    objs_= _objs;
    if (autosort_) {
      trie();
    }
    reinitialise();
    repaint();
    firePropertyChange(getPropertyName(), vp, objs_);
  }
  public void trie() {
    if (getComparator() == null) {
      return;
    }
    java.util.Arrays.sort(objs_, getComparator());
    if (getModel() instanceof LidoTableauBaseModel) {
      final LidoTableauBaseModel model= (LidoTableauBaseModel)getModel();
      final int rows= model.getRowCount();
      for (int r= 0; r < rows; r++) {
        model.setDirty(r, false);
      }
    }
  }
  private boolean autosort_= true;
  public boolean isAutosort() {
    return autosort_;
  }
  public void setAutosort(final boolean _autosort) {
    if (autosort_ == _autosort) {
      return;
    }
    final boolean vp= autosort_;
    autosort_= _autosort;
    firePropertyChange("autosort", vp, autosort_);
  }
  // DND Source
  protected void dndInitDragSource() {
    dndDragSrc_= DragSource.getDefaultDragSource();
    dndDragSrc_.createDefaultDragGestureRecognizer(
      this,
      DnDConstants.ACTION_COPY_OR_MOVE,
      this);
  }
  protected boolean dndIsColumnSourceAccepted(final int col) {
    return false;
  }
  public void dragDropEnd(final DragSourceDropEvent dsde) {}
  public void dragEnter(final DragSourceDragEvent dsde) {}
  public void dragExit(final DragSourceEvent dse) {}
  public void dragOver(final DragSourceDragEvent dsde) {
    // HACK immonde pour windows (le cursor ne veut pas se mettre bien!)
    //dsde.getDragSourceContext().setCursor(DragSource.DefaultCopyDrop);
  }
  public void dropActionChanged(final DragSourceDragEvent dsde) {}
  public void dragGestureRecognized(final DragGestureEvent dge) {
    final Object[] selected= getSelectedObjects();
    final int col= columnAtPoint(dge.getDragOrigin());
    if ((selected.length != 1) || !dndIsColumnSourceAccepted(col)) {
      System.err.println("DND: selection invalide");
      return;
    }
    dndDragSrc_.startDrag(
      dge,
      DragSource.DefaultMoveNoDrop,
      new LidoDnDTransfer(selected[0], getObjectFieldNameByColumnLink(col)),
      this);
  }
  // DND Target
  protected void dndInitDropTarget() {
    /*dndDropTarget_=*/
      new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, this);
    dndAcceptFlavor_= false;
    dndFlavor_=
      new DataFlavor(
        dndGetSourceParamClass(),
        DataFlavor.javaJVMLocalObjectMimeType);
    dndDropParam_= null;
    dndDropCol_= -1;
    dndDropRow_= -1;
  }
  protected Class dndGetSourceParamClass() {
    return null;
  }
  protected boolean dndIsColumnTargetAccepted(final int col) {
    return false;
  }
  protected void dndDropSucceeded() {}
  public void dragEnter(final DropTargetDragEvent dtde) {
    dndAcceptFlavor_= dtde.isDataFlavorSupported(dndFlavor_);
    /*    System.err.println("entered");
        Container parent=getParent();
        while( (parent!=null)&&!(parent instanceof JInternalFrame) ) {
          parent=parent.getParent();
        }
        if( parent!=null ) {
          System.err.println("move to front");
          ((JInternalFrame)parent).moveToFront();
        }*/
    dtde.rejectDrag();
  }
  public void dragExit(final DropTargetEvent dte) {
    dndAcceptFlavor_= false;
  }
  public void dragOver(final DropTargetDragEvent dtde) {
    final int col= columnAtPoint(dtde.getLocation());
    if (dndAcceptFlavor_ && dndIsColumnTargetAccepted(col)) {
      dtde.acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE);
    } else {
      dtde.rejectDrag();
    }
  }
  protected int getDropColumn() {
    return dndDropCol_;
  }
  protected int getDropRow() {
    return dndDropRow_;
  }
  protected LidoDnDTransferParam getDropObject() {
    return dndDropParam_;
  }
  public void drop(final DropTargetDropEvent dtde) {
    dndDropCol_= columnAtPoint(dtde.getLocation());
    dndDropRow_= rowAtPoint(dtde.getLocation());
    final Transferable t= dtde.getTransferable();
    if ((!t.isDataFlavorSupported(dndFlavor_))
      || (dndDropRow_ == -1)
      || !dndIsColumnTargetAccepted(dndDropCol_)) {
      System.err.println("DND: drop rejected");
      dtde.rejectDrop();
    } else {
      try {
        System.err.println("DND: drop accepted");
        dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
        dndDropParam_= (LidoDnDTransferParam)t.getTransferData(dndFlavor_);
        dndDropSucceeded();
        dtde.getDropTargetContext().dropComplete(true);
      } catch (final java.io.IOException ioe) {
        System.err.println(ioe);
        dtde.rejectDrop();
      } catch (final UnsupportedFlavorException ufe) {
        System.err.println(ufe);
        dtde.rejectDrop();
      }
    }
  }
  public void dropActionChanged(final DropTargetDragEvent dtde) {}
  /**
   *
   */
}
class LidoTableauBaseSelectionModel extends DefaultListSelectionModel {
  public LidoTableauBaseSelectionModel() {
    super();
    offset= 0;
  }
  private int offset;
  public void setOffset(final int _offset) {
    if (_offset == offset) {
      return;
    }
    offset= _offset;
  }
  public int getOffset() {
    return offset;
  }
}
