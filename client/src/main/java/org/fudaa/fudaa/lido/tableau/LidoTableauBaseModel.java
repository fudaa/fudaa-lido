/*
 * @file         LidoTableauBaseModel.java
 * @creation     1999-11-26
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Vector;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public abstract class LidoTableauBaseModel implements TableModel {
  Vector listeners_;
  Object[] pers_;
  int low_, high_;
  boolean[] dirty_;
  public LidoTableauBaseModel(final Object[] _pers) {
    listeners_= new Vector();
    setObjects(_pers);
  }
  public final void setObjects(final Object[] _pers) {
    pers_= _pers;
    low_= 0;
    if ((pers_ == null) || (pers_.length == 0)) {
      high_= 0;
      dirty_= new boolean[0];
    } else {
      high_= pers_.length;
      dirty_= new boolean[pers_.length];
    }
  }
  public Object getObject(final int row) {
    return pers_[low_ + row];
  }
  protected abstract Object[] getTableauType(int taille);
  public Object[] getObjects(final int[] rows) {
    final Object[] res= getTableauType(rows.length);
    for (int i= 0; i < rows.length; i++) {
      res[i]= pers_[low_ + rows[i]];
    }
    return res;
  }
  public Object[] getObjects() {
    return pers_;
  }
  public void restreintA(final int low, final int high) {
    low_= low;
    high_= high;
  }
  public boolean isRestreint() {
    return ((low_ > 0) || (high_ < pers_.length));
  }
  public int[] getRestriction() {
    return new int[] { low_, high_ };
  }
  public boolean getDirty(final int row) {
    return dirty_[low_ + row];
  }
  public void setDirty(final int row, final boolean dirty) {
    dirty_[low_ + row]= dirty;
  }
  public abstract Class getColumnClass(int column);
  public abstract int getColumnCount();
  public abstract String getColumnName(int column);
  public int getRowCount() {
    return high_ - low_;
  }
  public abstract Object getValueAt(int row, int column);
  public abstract boolean isCellEditable(int row, int column);
  public abstract void setValueAt(Object value, int row, int column);
  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }
  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }
}
