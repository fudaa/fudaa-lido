/*
 * @file         LidoTableauBiefs.java
 * @creation     1999-08-12
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauBiefs extends LidoTableauBase {
  LidoParamsHelper ph_;
  public LidoTableauBiefs(final LidoParamsHelper ph) {
    super();
    ph_= ph;
    dndInitDragSource();
    dndInitDropTarget();
    init();
  }
  private void init() {
    setModel(new LidoTableauBiefsModel(new SParametresBiefSituLigneRZO[0]));
    //    BuTableCellRenderer tcr=new BuTableCellRenderer();
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    BuTableCellEditor tceInteger=new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    
    //    TableColumnModel colModel=getColumnModel();
    //    int n=colModel.getColumnCount();
    //    for(int i=0;i<n;i++)
    //    {
    //      if( (i!=0) && (i!=2))
    //      {
    //        colModel.getColumn(i).setCellRenderer(tcr);
    //      }
    //      
    //    }
    //    BuTableCellRenderer tcrDouble=new BuTableCellRenderer();
    //    tcrDouble.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    //    colModel.getColumn(0).setCellEditor(tceDouble);
    //    colModel.getColumn(0).setCellRenderer(tcrDouble);
    //    colModel.getColumn(2).setCellEditor(tceDouble);
    //    colModel.getColumn(2).setCellRenderer(tcrDouble);
    //    colModel.getColumn(3).setCellEditor(tceInteger);
    //    colModel.getColumn(4).setCellEditor(tceInteger);
  }
  public void reinitialise() {
    final SParametresBiefSituLigneRZO[] biefs=
      (SParametresBiefSituLigneRZO[])getObjects(false);
    if (biefs == null) {
      return;
    }
    ((LidoTableauBiefsModel)getModel()).setObjects(biefs);
    tableChanged(new TableModelEvent(getModel()));
    //    
    //    setModel(new LidoTableauBiefsModel(biefs));
    //    //((LidoTableauBiefsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr=new BuTableCellRenderer();
    //    //BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger=new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    for(int i=0;i<getModel().getColumnCount();i++)
    //      getColumn(getColumnName(i)).setCellRenderer(tcr);
    //    //getColumn(getColumnName(0)).setWidth(30);
    //    //getColumn(getColumnName(1)).setWidth(60);
    //    getColumn(getColumnName(1)).setCellEditor(tceDouble);
    //    //getColumn(getColumnName(2)).setWidth(60);
    //    getColumn(getColumnName(2)).setCellEditor(tceDouble);
    //    //getColumn(getColumnName(3)).setWidth(60);
    //    getColumn(getColumnName(3)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(4)).setWidth(60);
    //    getColumn(getColumnName(4)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(5)).setWidth(60);
  }
  public int getPositionNouveau() {
    return ((LidoTableauBaseModel)getModel()).getRestriction()[1];
  }
  protected String getPropertyName() {
    return "biefs";
  }
  protected String getObjectFieldNameByColumnLink(final int col) {
    return getObjectFieldNameByColumn(col);
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "numBief";
        break;
      case 1 :
        r= "x1";
        break;
      case 2 :
        r= "x2";
        break;
      case 3 :
        r= "branch1";
        break;
      case 4 :
        r= "branch2";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.BIEF_COMPARATOR();
  }
  // surcharge de trie() pour recalculer les J
  public void trie() {
    super.trie();
    ph_.BIEF().reindiceBiefs();
  }
  // DND Target
  protected Class dndGetSourceParamClass() {
    return SParametresBiefBlocPRO.class;
  }
  protected boolean dndIsColumnTargetAccepted(final int col) {
    return (col == 1) || (col == 2);
  }
  public void dndDropSucceeded() {
    final int col= getDropColumn();
    final int row= getDropRow();
    final SParametresBiefBlocPRO profil=
      (SParametresBiefBlocPRO)getDropObject().getParam();
    if (profil != null) {
      final SParametresBiefSituLigneRZO bief=
        (SParametresBiefSituLigneRZO) (((LidoTableauBiefsModel)getModel())
          .getObject(row));
      if (col == 1) {
        bief.x1= profil.abscisse;
      } else if (col == 2) {
        bief.x2= profil.abscisse;
      }
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.RZO,
          bief,
          "bief " + (bief.numBief + 1)));
      ((LidoTableauBiefsModel)getModel()).setDirty(row, true);
      repaint();
    }
  }
  // DND Source
  protected boolean dndIsColumnSourceAccepted(final int col) {
    return (col == 3) || (col == 4);
  }
}
class LidoTableauBiefsModel extends LidoTableauBaseModel {
  public LidoTableauBiefsModel(final SParametresBiefSituLigneRZO[] _biefs) {
    super(_biefs);
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresBiefSituLigneRZO[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Integer.class; // indice
      case 1 :
        return Double.class; // x1
      case 2 :
        return Double.class; // x2
      case 3 :
        return Integer.class; // branch1
      case 4 :
        return Integer.class; // branch2
      case 5 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 6;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "n� bief";
        break;
      case 1 :
        r= "pk d�but";
        break;
      case 2 :
        r= "pk fin";
        break;
      case 3 :
        r= "extr amont";
        break;
      case 4 :
        r= "extr aval";
        break;
      case 5 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresBiefSituLigneRZO[] biefs=
      (SParametresBiefSituLigneRZO[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Integer(biefs[low_ + row].numBief + 1);
          break;
        case 1 :
          r= new Double(biefs[low_ + row].x1);
          break;
        case 2 :
          r= new Double(biefs[low_ + row].x2);
          break;
        case 3 :
          r= new Integer(biefs[low_ + row].branch1);
          break;
        case 4 :
          r= new Integer(biefs[low_ + row].branch2);
          break;
        case 5 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column == 1) || (column == 2));
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if (value == null) {
      return;
    }
    if ((low_ + row) < high_) {
      final SParametresBiefSituLigneRZO[] biefs=
        (SParametresBiefSituLigneRZO[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 1 :
          biefs[low_ + row].x1= ((Double)value).doubleValue();
          break;
        case 2 :
          biefs[low_ + row].x2= ((Double)value).doubleValue();
          break;
        case 3 :
          biefs[low_ + row].branch1= ((Integer)value).intValue();
          break;
        case 4 :
          biefs[low_ + row].branch2= ((Integer)value).intValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            biefs[low_ + row],
            "bief " + (biefs[low_ + row].numBief + 1)));
      }
    }
  }
}
