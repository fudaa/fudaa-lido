/*
 * @file         LidoTableauLaisses.java
 * @creation     1999-12-24
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.table.TableColumnModel;

import org.fudaa.dodico.corba.lido.SParametresLaisseLigneEXT;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauLaisses extends LidoTableauBase {
  private boolean modeResultat_;
  private LidoParamsHelper ph_;
  private SResultatsRSN rsn_;
  public LidoTableauLaisses() {
    super();
    modeResultat_= false;
    ph_= null;
    rsn_= null;
  }
  public void setModeResultat(
    boolean b,
    final LidoParamsHelper ph,
    final SResultatsRSN rsn) {
    modeResultat_= b;
    ph_= ph;
    rsn_= rsn;
    if ((getModel() != null)
      && (getModel() instanceof LidoTableauLaissesModel)) {
      ((LidoTableauLaissesModel)getModel()).setEditable(!b);
      ((LidoTableauLaissesModel)getModel()).setPH(ph);
      ((LidoTableauLaissesModel)getModel()).setRSN(rsn);
    }
  }
  public void reinitialise() {
    final SParametresLaisseLigneEXT[] lais=
      (SParametresLaisseLigneEXT[])getObjects(false);
    if (lais == null) {
      return;
    }
    final LidoTableauLaissesModel model= new LidoTableauLaissesModel(lais);
    setModel(model);
    if (modeResultat_) {
      model.setEditable(!modeResultat_);
      model.setPH(ph_);
      model.setRSN(rsn_);
    }
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    BuTableCellEditor tceString = new BuTableCellEditor(new BuTextField());
    //    //BuTableCellEditor tceInteger=new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble =
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 2; i < n; i++)
    //    {
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //    }
    //    BuTableCellRenderer tcrDouble = new BuTableCellRenderer();
    //    tcrDouble.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    //    colModel.getColumn(0).setCellEditor(tceDouble);
    //    colModel.getColumn(0).setCellRenderer(tcrDouble);
    //    colModel.getColumn(1).setCellEditor(tceDouble);
    //    colModel.getColumn(1).setCellRenderer(tcrDouble);
    //    colModel.getColumn(2).setCellEditor(tceString);
    if (!modeResultat_) {
      final TableColumnModel colModel= getColumnModel();
      System.err.println("removing cols");
      removeColumn(colModel.getColumn(4));
      removeColumn(colModel.getColumn(3));
    }
  }
  protected String getPropertyName() {
    return "laisses";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "abscisse";
        break;
      case 1 :
        r= "cote";
        break;
      case 2 :
        r= "titre";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.LAISSE_COMPARATOR();
  }
}
class LidoTableauLaissesModel extends LidoTableauBaseModel {
  SResultatsRSN rsn_;
  LidoParamsHelper ph_;
  boolean editable_;
  public LidoTableauLaissesModel(final SParametresLaisseLigneEXT[] _lais) {
    super(_lais);
    rsn_= null;
    editable_= true;
  }
  public LidoTableauLaissesModel(
    final SParametresLaisseLigneEXT[] _lais,
    final SResultatsRSN rsn) {
    super(_lais);
    rsn_= rsn;
    editable_= true;
  }
  public void setPH(final LidoParamsHelper ph) {
    ph_= ph;
  }
  public void setRSN(final SResultatsRSN rsn) {
    rsn_= rsn;
  }
  public void setEditable(final boolean b) {
    editable_= b;
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresLaisseLigneEXT[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Double.class; // abscisse
      case 1 :
        return Double.class; // cote
      case 2 :
        return String.class; // titre
      case 3 :
        return Double.class; // ligne d'eau resultat
      case 4 :
        return Double.class; // ecart cote/ligne d'eau
      case 5 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 6;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "abscisse";
        break;
      case 1 :
        r= "cote";
        break;
      case 2 :
        r= "titre";
        break;
      case 3 :
        r= "max cote eau";
        break;
      case 4 :
        r= "ecart";
        break;
      case 5 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresLaisseLigneEXT[] lais= (SParametresLaisseLigneEXT[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Double(lais[low_ + row].abscisse);
          break;
        case 1 :
          r= new Double(lais[low_ + row].cote);
          break;
        case 2 :
          r= lais[low_ + row].titre;
          break;
        case 3 :
          {
            r= ph_.PROFIL().getMaxCoteEauResultat(lais[low_ + row].abscisse);
            if (r == null) {
              r= new Double(0.);
            }
            break;
          }
        case 4 :
          {
            r= ph_.PROFIL().getMaxCoteEauResultat(lais[low_ + row].abscisse);
            if (r == null) {
              r= new Double(0.);
            } else {
              r= new Double(((Double)r).doubleValue() - lais[low_ + row].cote);
            }
            break;
          }
        case 5 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    if (!editable_) {
      return false;
    }
    return ((column != 3) && (column != 4) && (column != 5));
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresLaisseLigneEXT[] lais=
        (SParametresLaisseLigneEXT[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 0 :
          lais[low_ + row].abscisse= ((Double)value).doubleValue();
          break;
        case 1 :
          lais[low_ + row].cote= ((Double)value).doubleValue();
          break;
        case 2 :
          lais[low_ + row].titre= (String)value;
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.EXT,
            lais[low_ + row],
            "laisse " + (lais[low_ + row].titre)));
      }
    }
  }
}
