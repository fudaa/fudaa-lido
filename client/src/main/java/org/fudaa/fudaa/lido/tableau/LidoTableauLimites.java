/*
 * @file         LidoTableauLimites.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.corba.lido.SParametresBiefLimLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauLimites extends LidoTableauBase {
  private LidoLoiclmCellEditor loiclmEditor_;
  private TableCellEditor extrLibreEditor_;
  public LidoTableauLimites(
    final LidoLoiclmCellEditor loiclmEditor,
    final TableCellEditor extrLibreEditor) {
    super();
    loiclmEditor_= loiclmEditor;
    extrLibreEditor_= extrLibreEditor;
    init();
  }
  private void init() {
    if (CtuluLibMessage.DEBUG) {
      System.out.println("LidoTableauLimites");
    }
    setModel(new LidoTableauLimitesModel(new SParametresBiefLimLigneRZO[0]));
    ((LidoTableauLimitesModel)getModel()).setTableau(this);
    //        BuTableCellRenderer tcr = new BuTableCellRenderer();
    //        BuTableCellEditor tceInteger =
    //          new BuTableCellEditor(BuTextField.createIntegerField());
    //        tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    final TableColumnModel colModel= getColumnModel();
    //        int n = colModel.getColumnCount();
    //        for (int i = 0; i < n; i++)
    //          colModel.getColumn(i).setCellRenderer(tcr);
    colModel.getColumn(0).setCellEditor(extrLibreEditor_);
    colModel.getColumn(1).setCellEditor(loiclmEditor_);
    //        colModel.getColumn(2).setCellEditor(tceInteger);
  }
  public void reinitialise() {
    final SParametresBiefLimLigneRZO[] lims=
      (SParametresBiefLimLigneRZO[])getObjects(false);
    if (lims == null) {
      return;
    }
    //    setModel(new LidoTableauLimitesModel(lims));
    //    //((LidoTableauLimitesSelectionModel)getSelectionModel()).setOffset(0);
    //     ((LidoTableauLimitesModel) getModel()).setTableau(this);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    //    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger =
    //      new BuTableCellEditor(BuTextField.createIntegerField());
    //    //  BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //    //getColumn(getColumnName(0)).setWidth(50);
    //    colModel.getColumn(0).setCellEditor(extrLibreEditor_);
    //    //getColumn(getColumnName(1)).setWidth(50);
    //    colModel.getColumn(1).setCellEditor(loiclmEditor_);
    //    //getColumn(getColumnName(2)).setWidth(50);
    //    colModel.getColumn(2).setCellEditor(tceInteger);
     ((LidoTableauLimitesModel)getModel()).setObjects(lims);
    tableChanged(new TableModelEvent(getModel()));
  }
  void numLoiChanged(final int row) {
    firePropertyChange("numloichanged", null, getObjects(false)[row]);
  }
  protected String getPropertyName() {
    return "limites";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "numExtBief";
        break;
      case 1 :
        r= "numLoi";
        break;
      case 2 :
        r= "typLoi";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.LIMITE_COMPARATOR();
  }
}
class LidoTableauLimitesModel extends LidoTableauBaseModel {
  LidoTableauLimites table_;
  public LidoTableauLimitesModel(final SParametresBiefLimLigneRZO[] _lims) {
    super(_lims);
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresBiefLimLigneRZO[taille];
  }
  public void setTableau(final LidoTableauLimites table) {
    table_= table;
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Integer.class; // indice
      case 1 :
        return Integer.class; // no de loi
      case 2 :
        return String.class; // type de loi
      case 3 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 4;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "extr libre";
        break;
      case 1 :
        r= "n� loi";
        break;
      case 2 :
        r= "type loi";
        break;
      case 3 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresBiefLimLigneRZO[] lims=
      (SParametresBiefLimLigneRZO[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Integer(lims[low_ + row].numExtBief);
          break;
        case 1 :
          r= new Integer(lims[low_ + row].numLoi);
          break;
        case 2 :
          {
            switch (lims[low_ + row].typLoi) {
              case LidoResource.LIMITE.TARAGE :
                r= "Tarage";
                break;
              case LidoResource.LIMITE.LIMNI :
                r= "Limni";
                break;
              case LidoResource.LIMITE.MAREE :
                r= "Mar�e";
                break;
              case LidoResource.LIMITE.HYDRO :
                r= "Hydro";
                break;
              default :
                r= "?";
                break;
            }
            break;
          }
        case 3 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column != 3) && (column != 2));
  }
  public void setValueAt(Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresBiefLimLigneRZO[] lims=
        (SParametresBiefLimLigneRZO[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 0 :
          lims[low_ + row].numExtBief= ((Integer)value).intValue();
          break;
        case 1 :
          {
            if (value instanceof String) {
              lims[low_ + row].numLoi= 0;
            } else {
              lims[low_ + row].numLoi=
                ((SParametresCondLimBlocCLM)value).numCondLim;
            }
            value= new Integer(lims[low_ + row].numLoi);
            table_.numLoiChanged(low_ + row);
            break;
          }
          //case 2: lims[low_+row].typLoi=((Integer)value).intValue(); break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            lims[low_ + row],
            "limite " + (lims[low_ + row].numLoi)));
      }
    }
  }
}
