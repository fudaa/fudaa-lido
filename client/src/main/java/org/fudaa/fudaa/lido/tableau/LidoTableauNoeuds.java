/*
 * @file         LidoTableauNoeuds.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresBiefNoeudLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauNoeuds extends LidoTableauBase {
  public LidoTableauNoeuds() {
    super();
    dndInitDropTarget();
    init();
  }
  private void init() {
    setModel(new LidoTableauNoeudsModel(new SParametresBiefNoeudLigneRZO[0]));
    //((LidoTableauNoeudsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr=new BuTableCellRenderer();
    ////    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger=new BuTableCellEditor(BuTextField.createIntegerField());
    //  //  BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //
    //    //getColumn(getColumnName(0)).setWidth(30);
    //    //getColumn(getColumnName(1)).setWidth(30);
    //    colModel.getColumn(1).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(2)).setWidth(30);
    //    colModel.getColumn(2).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(3)).setWidth(30);
    //    colModel.getColumn(3).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(4)).setWidth(30);
    //    colModel.getColumn(4).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(5)).setWidth(30);
    //    colModel.getColumn(5).setCellEditor(tceInteger);
    //getColumn(getColumnName(6)).setWidth(30);
  }
  public void reinitialise() {
    final SParametresBiefNoeudLigneRZO[] noeuds=
      (SParametresBiefNoeudLigneRZO[])getObjects(false);
    if (noeuds == null) {
      return;
    }
    //    setModel(new LidoTableauNoeudsModel(noeuds));
    //    //((LidoTableauNoeudsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr=new BuTableCellRenderer();
    ////    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger=new BuTableCellEditor(BuTextField.createIntegerField());
    //  //  BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    for(int i=0;i<getModel().getColumnCount();i++)
    //      getColumn(getColumnName(i)).setCellRenderer(tcr);
    //    //getColumn(getColumnName(0)).setWidth(30);
    //    //getColumn(getColumnName(1)).setWidth(30);
    //    getColumn(getColumnName(1)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(2)).setWidth(30);
    //    getColumn(getColumnName(2)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(3)).setWidth(30);
    //    getColumn(getColumnName(3)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(4)).setWidth(30);
    //    getColumn(getColumnName(4)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(5)).setWidth(30);
    //    getColumn(getColumnName(5)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(6)).setWidth(30);
     ((LidoTableauNoeudsModel)getModel()).setObjects(noeuds);
    tableChanged(new TableModelEvent(getModel()));
  }
  public int getPositionNouveau() {
    return ((LidoTableauBaseModel)getModel()).getRestriction()[1];
  }
  protected String getPropertyName() {
    return "noeuds";
  }
  protected String getObjectFieldNameByColumnLink(final int col) {
    return getObjectFieldNameByColumn(col);
  }
  public static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "numNoeud";
        break;
      case 1 :
        r= "extr1";
        break;
      case 2 :
        r= "extr1";
        break;
      case 3 :
        r= "extr1";
        break;
      case 4 :
        r= "extr1";
        break;
      case 5 :
        r= "extr1";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.NOEUD_COMPARATOR();
  }
  // DND Target
  protected Class dndGetSourceParamClass() {
    return SParametresBiefSituLigneRZO.class;
  }
  protected boolean dndIsColumnTargetAccepted(final int col) {
    return (col != 0) && (col != 6);
  }
  protected void dndDropSucceeded() {
    final int col= getDropColumn();
    final int row= getDropRow();
    final SParametresBiefSituLigneRZO bief=
      (SParametresBiefSituLigneRZO)getDropObject().getParam();
    if (bief != null) {
      final SParametresBiefNoeudLigneRZO noeud=
        (SParametresBiefNoeudLigneRZO) (((LidoTableauNoeudsModel)getModel())
          .getObject(row));
      System.err.println(getDropObject().getField());
      final int extr=
        "branch2".equals(getDropObject().getField())
          ? bief.branch2
          : bief.branch1;
      if (noeud.noeud[col - 1] != 0) {
        noeud.noeud[col - 1]= extr;
      } else {
        int i= 0;
        while ((i < noeud.noeud.length) && (noeud.noeud[i] != 0)) {
          i++;
        }
        noeud.noeud[i]= extr;
      }
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          LidoResource.RZO,
          noeud,
          "noeud ["
            + noeud.noeud[0]
            + CtuluLibString.VIR
            + noeud.noeud[1]
            + CtuluLibString.VIR
            + noeud.noeud[2]
            + CtuluLibString.VIR
            + noeud.noeud[3]
            + CtuluLibString.VIR
            + noeud.noeud[4]
            + "]"));
      ((LidoTableauNoeudsModel)getModel()).setDirty(row, true);
      repaint();
    }
  }
}
class LidoTableauNoeudsModel extends LidoTableauBaseModel {
  public LidoTableauNoeudsModel(final SParametresBiefNoeudLigneRZO[] _noeuds) {
    super(_noeuds);
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresBiefNoeudLigneRZO[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Integer.class; // indice
      case 1 :
        return Integer.class; // 1
      case 2 :
        return Integer.class; // 2
      case 3 :
        return Integer.class; // 3
      case 4 :
        return Integer.class; // 4
      case 5 :
        return Integer.class; // 5
      case 6 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 7;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "n� noeud";
        break;
      case 1 :
        r= "extr�mit� 1";
        break;
      case 2 :
        r= "extr�mit� 2";
        break;
      case 3 :
        r= "extr�mit� 3";
        break;
      case 4 :
        r= "extr�mit� 4";
        break;
      case 5 :
        r= "extr�mit� 5";
        break;
      case 6 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresBiefNoeudLigneRZO[] noeuds=
      (SParametresBiefNoeudLigneRZO[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Integer(low_ + row + 1);
          break;
        case 1 :
          r= new Integer(noeuds[low_ + row].noeud[0]);
          break;
        case 2 :
          r= new Integer(noeuds[low_ + row].noeud[1]);
          break;
        case 3 :
          r= new Integer(noeuds[low_ + row].noeud[2]);
          break;
        case 4 :
          r= new Integer(noeuds[low_ + row].noeud[3]);
          break;
        case 5 :
          r= new Integer(noeuds[low_ + row].noeud[4]);
          break;
        case 6 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column != 0) && (column != 6));
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresBiefNoeudLigneRZO[] noeuds=
        (SParametresBiefNoeudLigneRZO[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 1 :
          noeuds[low_ + row].noeud[0]= ((Integer)value).intValue();
          break;
        case 2 :
          noeuds[low_ + row].noeud[1]= ((Integer)value).intValue();
          break;
        case 3 :
          noeuds[low_ + row].noeud[2]= ((Integer)value).intValue();
          break;
        case 4 :
          noeuds[low_ + row].noeud[3]= ((Integer)value).intValue();
          break;
        case 5 :
          noeuds[low_ + row].noeud[4]= ((Integer)value).intValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            noeuds[low_ + row],
            "noeud ["
              + noeuds[low_
              + row].noeud[0]
              + CtuluLibString.VIR
              + noeuds[low_
              + row].noeud[1]
              + CtuluLibString.VIR
              + noeuds[low_
              + row].noeud[2]
              + CtuluLibString.VIR
              + noeuds[low_
              + row].noeud[3]
              + CtuluLibString.VIR
              + noeuds[low_
              + row].noeud[4]
              + "]"));
      }
    }
  }
}
