/*
 * @file         LidoTableauPermSection2s.java
 * @creation     1999-09-14
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import com.memoire.bu.BuCommonInterface;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.lido.SParametresSerieLigneCAL;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoDialogContraintes;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauPermSection2s extends LidoTableauBase {
  private LidoParamsHelper ph_;
  public LidoTableauPermSection2s(final LidoParamsHelper ph) {
    super();
    ph_= ph;
    init();
  }
  private void init() {
    setModel(
      new LidoTableauPermSection2sModel(new SParametresSerieLigneCAL[0], ph_));
    //    //((LidoTableauPermSection2sSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    //    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger =
    //      new BuTableCellEditor(BuTextField.createIntegerField());
    //    //    BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //
    //    colModel.getColumn(0).setCellEditor(tceInteger);
    //    colModel.getColumn(1).setCellEditor(tceInteger);
    //    colModel.getColumn(2).setCellEditor(tceInteger);
  }
  public void reinitialise() {
    final SParametresSerieLigneCAL[] pers=
      (SParametresSerieLigneCAL[])getObjects(false);
    if (pers == null) {
      return;
    }
    //    setModel(new LidoTableauPermSection2sModel(pers, ph_));
    //    //((LidoTableauPermSection2sSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    //    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger =
    //      new BuTableCellEditor(BuTextField.createIntegerField());
    //    //    BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //
    //    colModel.getColumn(0).setCellEditor(tceInteger);
    //    colModel.getColumn(1).setCellEditor(tceInteger);
    //    colModel.getColumn(2).setCellEditor(tceInteger);
     ((LidoTableauPermSection2sModel)getModel()).setObjects(pers);
    tableChanged(new TableModelEvent(getModel()));
  }
  public boolean verifieContraintes() {
    final SParametresSerieLigneCAL[] pers= (SParametresSerieLigneCAL[])getObjects();
    SParametresSerieLigneCAL[] erreurs=
      ph_.PERMSECTION2().verifieContraintesBief(pers);
    boolean verif= true;
    if (erreurs.length > 0) {
      String lst= "";
      for (int i= 0; i < erreurs.length; i++) {
        lst += "["
          + (ph_.PROFIL().getJByAbscisse(erreurs[i].absDebBief) + 1)
          + CtuluLibString.VIR
          + (ph_.PROFIL().getJByAbscisse(erreurs[i].absFinBief) + 1)
          + "]\n";
      }
      final int resp=
        new LidoDialogContraintes(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME)
            .getImplementation()
            .getInformationsSoftware(),
          new String(
            "Attention: les sections suivantes sont � cheval\n"
              + "sur deux biefs\n\n"
              + lst),
          true)
          .activate();
      if (resp == LidoDialogContraintes.IGNORER) {
        verif= true;
      } else {
        verif= false;
      }
    }
    erreurs= ph_.PERMSECTION2().verifieContraintesProfil(pers);
    if (erreurs.length > 0) {
      String lst= "";
      for (int i= 0; i < erreurs.length; i++) {
        lst += "["
          + (ph_.PROFIL().getJByAbscisse(erreurs[i].absDebBief) + 1)
          + CtuluLibString.VIR
          + (ph_.PROFIL().getJByAbscisse(erreurs[i].absFinBief) + 1)
          + "]\n";
      }
      final int resp=
        new LidoDialogContraintes(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME)
            .getImplementation()
            .getInformationsSoftware(),
          new String(
            "Attention: les sections suivantes ne contiennent\n"
              + "qu'un profil\n\n"
              + lst),
          true)
          .activate();
      if (resp == LidoDialogContraintes.IGNORER) {
        verif= true;
      } else {
        verif= false;
      }
    }
    erreurs= ph_.PERMSECTION2().verifieContraintesChevauchement(pers);
    if (erreurs.length > 0) {
      String lst= "";
      for (int i= 0; i < erreurs.length; i += 2) {
        lst += "["
          + (ph_.PROFIL().getJByAbscisse(erreurs[i].absDebBief) + 1)
          + CtuluLibString.VIR
          + (ph_.PROFIL().getJByAbscisse(erreurs[i].absFinBief) + 1)
          + "] "
          + "["
          + (ph_.PROFIL().getJByAbscisse(erreurs[i + 1].absDebBief) + 1)
          + CtuluLibString.VIR
          + (ph_.PROFIL().getJByAbscisse(erreurs[i + 1].absFinBief) + 1)
          + "]\n";
      }
      final int resp=
        new LidoDialogContraintes(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME)
            .getImplementation()
            .getInformationsSoftware(),
          new String(
            "Attention: les sections suivantes se chevauchent\n\n" + lst),
          true)
          .activate();
      if (resp == LidoDialogContraintes.IGNORER) {
        verif= true;
      } else {
        verif= false;
      }
    }
    return verif;
  }
  protected String getPropertyName() {
    return "permSection2s";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "JDebBief";
        break;
      case 1 :
        r= "JFinBief";
        break;
      case 2 :
        r= "nbSectCalc";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.PERMSECTION2_COMPARATOR();
  }
}
class LidoTableauPermSection2sModel extends LidoTableauBaseModel {
  LidoParamsHelper ph_;
  public LidoTableauPermSection2sModel(
    final SParametresSerieLigneCAL[] _pers,
    final LidoParamsHelper ph) {
    super(_pers);
    ph_= ph;
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresSerieLigneCAL[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Integer.class; // J DebBief
      case 1 :
        return Integer.class; // J FinBief
      case 2 :
        return Integer.class; // nbSectCal
      case 3 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 4;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "J amont";
        break;
      case 1 :
        r= "J aval";
        break;
      case 2 :
        r= "nb sections";
        break;
      case 3 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresSerieLigneCAL[] pers= (SParametresSerieLigneCAL[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          {
            r=
              new Integer(
                ph_.PROFIL().getJByAbscisse(pers[low_ + row].absDebBief) + 1);
            break;
          }
        case 1 :
          {
            r=
              new Integer(
                ph_.PROFIL().getJByAbscisse(pers[low_ + row].absFinBief) + 1);
            break;
          }
        case 2 :
          r= new Integer(pers[low_ + row].nbSectCalc);
          break;
        case 3 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column != 3));
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresSerieLigneCAL[] pers= (SParametresSerieLigneCAL[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 0 :
          {
            pers[low_ + row].absDebBief=
              ph_.PROFIL().getAbscisseByJ(((Integer)value).intValue() - 1);
            break;
          }
        case 1 :
          {
            pers[low_ + row].absFinBief=
              ph_.PROFIL().getAbscisseByJ(((Integer)value).intValue() - 1);
            break;
          }
        case 2 :
          pers[low_ + row].nbSectCalc= ((Integer)value).intValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CAL,
            pers[low_ + row],
            "section "
              + pers[low_
              + row].absDebBief
              + CtuluLibString.VIR
              + pers[low_
              + row].absFinBief
              + "]"));
      }
    }
  }
}
