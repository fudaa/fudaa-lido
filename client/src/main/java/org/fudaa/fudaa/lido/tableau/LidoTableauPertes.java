/*
 * @file         LidoTableauPertes.java
 * @creation     1999-09-09
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import org.fudaa.dodico.corba.lido.SParametresPerteLigneCLM;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauPertes extends LidoTableauBase {
  public LidoTableauPertes() {
    super();
    init();
  }
  private void init() {
    setModel(new LidoTableauPertesModel(new SParametresPerteLigneCLM[0]));
    //((LidoTableauPertesSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    //    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceDouble =
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 2; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //
    //    BuTableCellRenderer tcrDouble = new BuTableCellRenderer();
    //    tcrDouble.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    //    colModel.getColumn(0).setCellEditor(tceDouble);
    //    colModel.getColumn(0).setCellRenderer(tcrDouble);
    //    colModel.getColumn(1).setCellRenderer(tcrDouble);
    //    colModel.getColumn(1).setCellEditor(tceDouble);
  }
  public void reinitialise() {
    final SParametresPerteLigneCLM[] pers=
      (SParametresPerteLigneCLM[])getObjects(false);
    if (pers == null) {
      return;
    }
    //    setModel(new LidoTableauPertesModel(pers));
    //    //((LidoTableauPertesSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr=new BuTableCellRenderer();
    ////    BuTableCellEditor tceString=new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceDouble=new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //
    //    colModel.getColumn(0).setCellEditor(tceDouble);
    //    colModel.getColumn(1).setCellEditor(tceDouble);
     ((LidoTableauPertesModel)getModel()).setObjects(pers);
    tableChanged(new TableModelEvent(getModel()));
  }
  protected String getPropertyName() {
    return "pertes";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "xPerte";
        break;
      case 1 :
        r= "coefPerte";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.PERTE_COMPARATOR();
  }
}
class LidoTableauPertesModel extends LidoTableauBaseModel {
  public LidoTableauPertesModel(final SParametresPerteLigneCLM[] _pers) {
    super(_pers);
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresPerteLigneCLM[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Double.class; // xPerte
      case 1 :
        return Double.class; // coef per
      case 2 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 3;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "abscisse";
        break;
      case 1 :
        r= "coefficient";
        break;
      case 2 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresPerteLigneCLM[] pers= (SParametresPerteLigneCLM[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Double(pers[low_ + row].xPerte);
          break;
        case 1 :
          r= new Double(pers[low_ + row].coefPerte);
          break;
        case 2 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column != 2));
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresPerteLigneCLM[] pers= (SParametresPerteLigneCLM[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 0 :
          pers[low_ + row].xPerte= ((Double)value).doubleValue();
          break;
        case 1 :
          pers[low_ + row].coefPerte= ((Double)value).doubleValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.CLM,
            pers[low_ + row],
            "perte " + (pers[low_ + row].xPerte)));
      }
    }
  }
}
