/*
 * @file         LidoTableauProfils.java
 * @creation     1999-04-27
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragSource;
import java.util.Comparator;

import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.memoire.bu.BuCommonInterface;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;

import org.fudaa.fudaa.commun.dodico.Profil1D2D;
import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoDialogContraintes;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauProfils extends LidoTableauBase {
  public final static int PROFILS= LidoResource.PROFIL.PROFILS;
  public final static int CALAGE= LidoResource.PROFIL.CALAGE;
  private int mode_;
  private TableColumn strick1_, strick2_, stock_;
  private LidoParamsHelper ph_;
  private boolean modeResultat_;
  public LidoTableauProfils() {
    super();
    mode_= PROFILS;
    dndInitDragSource();
    ph_= null;
    modeResultat_= false;
  }
  public LidoTableauProfils(final LidoParamsHelper ph) {
    this();
    ph_= ph;
  }
  public void setMode(final int m) {
    if (m == mode_) {
      return;
    }
    mode_= m;
    if (!(getModel() instanceof LidoTableauProfilsModel)) {
      return;
    }
    ((LidoTableauProfilsModel)getModel()).setMode(m);
    switch (mode_) {
      case PROFILS :
        {
          if (strick1_ != null) {
            removeColumn(strick1_);
          }
          if (strick2_ != null) {
            removeColumn(strick2_);
          }
          if (stock_ != null) {
            removeColumn(stock_);
          }
          break;
        }
      case CALAGE :
        {
          if (strick1_ != null) {
            addColumn(strick1_);
          }
          if (strick2_ != null) {
            addColumn(strick2_);
          }
          if (stock_ != null) {
            addColumn(stock_);
          }
          moveColumn(3, 6); // on remet la colonne "modifie" a la fin
        }
    }
  }
  public void setModeResultat(boolean edit) {
    modeResultat_= edit;
    if ((getModel() != null)
      && (getModel() instanceof LidoTableauProfilsModel)) {
      ((LidoTableauProfilsModel)getModel()).setEditable(!edit);
    }
  }
  public void reinitialise() {
    final SParametresBiefBlocPRO[] prof= (SParametresBiefBlocPRO[])getObjects(false);
    if (prof == null) {
      return;
    }
    final LidoTableauProfilsModel model= new LidoTableauProfilsModel(prof);
    model.setMode(mode_);
    setModel(model);
    //((LidoTableauProfilsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    BuTableCellEditor tceString = new BuTableCellEditor(new BuTextField());
    //    BuTableCellEditor tceInteger =
    //      new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble =
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //    {
    //      if(i!=2)
    //      colModel.getColumn(i).setCellRenderer(tcr);
    //    }
    //    
    //    colModel.getColumn(0).setCellEditor(tceString);
    //    colModel.getColumn(1).setCellEditor(tceInteger);
    //    BuTableCellRenderer tcrDouble = new BuTableCellRenderer();
    //    tcrDouble.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    //    colModel.getColumn(2).setCellRenderer(tcrDouble);
    //    colModel.getColumn(2).setCellEditor(tceDouble);
    final TableColumnModel colModel= getColumnModel();
    strick1_= colModel.getColumn(3);
    //strick1_.setWidth(100);
    strick2_= colModel.getColumn(4);
    //strick2_.setWidth(100);
    stock_= colModel.getColumn(5);
    //stock_.setWidth(60);
    //getColumn(model.getColumnName(6)).setWidth(60);
    if (mode_ == PROFILS) {
      removeColumn(strick1_);
      removeColumn(strick2_);
      removeColumn(stock_);
    }
  }
  public boolean verifieContraintes() {
    boolean verif= true;
    final Object[] profils= getObjects();
    String errstr= "";
    SParametresBiefBlocPRO p1= null;
    if (profils.length > 0) {
      p1= (SParametresBiefBlocPRO)profils[0];
    }
    for (int i= 1; i < profils.length; i++) {
      final SParametresBiefBlocPRO p2= (SParametresBiefBlocPRO)profils[i];
      if (Math.round(LidoResource.PRECISION * p2.abscisse)
        == Math.round(LidoResource.PRECISION * p1.abscisse)) {
        errstr= errstr + (p1.indice + 1) + "-" + (p2.indice + 1) + "\n";
      }
      p1= p2;
    }
    if (!"".equals(errstr)) {
      final int resp=
        new LidoDialogContraintes(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME)
            .getImplementation()
            .getInformationsSoftware(),
          new String(
            "Attention: les profils suivant ont la m�me abscisse:\n\n"
              + errstr),
          true)
          .activate();
      if (resp == LidoDialogContraintes.IGNORER) {
        verif= true;
      } else {
        verif= false;
      }
    }
    return verif;
  }
  protected String getPropertyName() {
    return "profils";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "numProfil";
        break;
      case 1 :
        r= "indice";
        break;
      case 2 :
        r= "abscisse";
        break;
      case 3 :
        r= "coefStrickMajMin";
        break;
      case 4 :
        r= "coefStrickMajSto";
        break;
      case 5 :
        r= "stockage";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.PROFIL_COMPARATOR();
  }
  // surcharge de trie() pour recalculer les J
  public void trie() {
    super.trie();
    if (ph_ != null) {
      ph_.PROFIL().reindiceProfils();
    }
  }
  // DND Source
  protected boolean dndIsColumnSourceAccepted(final int col) {
    boolean res= false;
    if (mode_ == PROFILS) {
      res= (col == 3);
    } else if (mode_ == CALAGE) {
      res= (col == 6);
    }
    return res;
  }
  // HACK pour le DnD Lido-Reflux (il faut envoyer un objet special (Profil1D2D)
  public void dragGestureRecognized(final DragGestureEvent dge) {
    if (!modeResultat_) {
      super.dragGestureRecognized(dge);
      return;
    }
    final Object[] selected= getSelectedObjects();
    final int col= columnAtPoint(dge.getDragOrigin());
    if ((selected.length != 1) || !dndIsColumnSourceAccepted(col)) {
      System.err.println("DND: selection invalide");
      return;
    }
    final SParametresBiefBlocPRO profil= (SParametresBiefBlocPRO)selected[0];
    final Profil1D2D profil1d2d=
      new Profil1D2D(profil.numProfil, ph_.CALCUL().isRegimePermanent());
    if (ph_ != null) {
      final double[][] limni=
        ph_.PROFIL().getCourbeResultat(profil, LidoResource.LIMITE.LIMNI);
      if (limni != null) {
        profil1d2d.setLimnigramme(limni);
      }
    }
    String debugmsg= "\n";
    debugmsg += "Profil1D2D: \n"
      + "  name="
      + profil1d2d.getName()
      + "\n"
      + "  RP="
      + profil1d2d.isRegimePermanent()
      + "\n"
      + "  resultat dispo="
      + profil1d2d.isResultatDisponible()
      + "\n";
    if (profil1d2d.isResultatDisponible()) {
      debugmsg += "  limnigramme=\n";
      final double[][] l= profil1d2d.getLimnigramme();
      if (l == null) {
        debugmsg += "    null\n";
      } else {
        for (int i= 0; i < l.length; i++) {
          debugmsg += "    [" + l[i][0] + ", " + l[i][1] + "]\n";
        }
      }
    }
    System.err.println(debugmsg);
    dndDragSrc_.startDrag(dge, DragSource.DefaultCopyNoDrop, profil1d2d, this);
  }
}
class LidoTableauProfilsModel extends LidoTableauBaseModel {
  private final static int PRECISION= LidoResource.PRECISION;
  boolean editable_;
  int mode_;
  public LidoTableauProfilsModel(final SParametresBiefBlocPRO[] _pros) {
    super(_pros);
    editable_= true;
    mode_= LidoResource.PROFIL.PROFILS;
  }
  public void setMode(final int m) {
    mode_= m;
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresBiefBlocPRO[taille];
  }
  public void setEditable(final boolean b) {
    editable_= b;
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return String.class; // TITRE
      case 1 :
        return Integer.class; // J
      case 2 :
        return Double.class; // abscisse
      case 3 :
        return Integer.class; // Strickler mineur
      case 4 :
        return Integer.class; // Strickler majeur
      case 5 :
        return String.class; // zone de stockage
      case 6 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 7;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "intitul�";
        break;
      case 1 :
        r= "J";
        break;
      case 2 :
        r= "abscisse";
        break;
      case 3 :
        r= "strickler mineur";
        break;
      case 4 :
        r= "strickler majeur";
        break;
      case 5 :
        r= "stockage";
        break;
      case 6 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresBiefBlocPRO[] pros= (SParametresBiefBlocPRO[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new String(pros[low_ + row].numProfil);
          break;
        case 1 :
          r= new Integer(pros[low_ + row].indice + 1);
          break;
        case 2 :
          r= new Double(pros[low_ + row].abscisse);
          break;
        case 3 :
          r= new Integer((int)pros[low_ + row].coefStrickMajMin);
          break;
        case 4 :
          r= new Integer((int)pros[low_ + row].coefStrickMajSto);
          break;
        case 5 :
          r=
            ((pros[low_ + row].nbPoints > 0)
              && ((Math.round(PRECISION * pros[low_ + row].absMajSto[0])
                > Math.round(PRECISION * pros[low_ + row].abs[0]))
                || (Math.round(PRECISION * pros[low_ + row].absMajSto[1])
                  < Math.round(
                    PRECISION * pros[low_
                      + row].abs[pros[low_
                      + row].abs.length
                      - 1]))))
              ? new String("S")
              : new String("");
          break;
        case 6 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    if (!editable_) {
      return false;
    }
    if (mode_ == LidoResource.PROFIL.PROFILS) {
      return ((column != 1) && (column != 5) && (column != 6));
    }
    if (mode_ == LidoResource.PROFIL.CALAGE) {
      return ((column == 3) || (column == 4));
    }
    return false;
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresBiefBlocPRO[] pros= (SParametresBiefBlocPRO[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 0 :
          pros[low_ + row].numProfil= (String)value;
          break;
        case 2 :
          pros[low_ + row].abscisse= ((Double)value).doubleValue();
          break;
        case 3 :
          pros[low_ + row].coefStrickMajMin= ((Integer)value).doubleValue();
          break;
        case 4 :
          pros[low_ + row].coefStrickMajSto= ((Integer)value).doubleValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.PRO,
            pros[low_ + row],
            "profil " + (pros[low_ + row].numProfil),
            LidoTableauProfils.getObjectFieldNameByColumn(column)));
      }
    }
  }
}
