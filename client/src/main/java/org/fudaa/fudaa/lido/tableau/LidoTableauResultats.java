/*
 * @file         LidoTableauResultats.java
 * @creation     1999-10-05
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauResultats extends LidoTableauBase {
  public LidoTableauResultats() {
    super();
    init();
  }
  public void editeCellule(final int lig, final int col) {}
  private void init() {
    setModel(new LidoTableauResultatsModel(new SResultatsBiefRSN[0]));
    //((LidoTableauBiefsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //      colModel.getColumn(i).setCellRenderer(tcr);
  }
  public void reinitialise() {
    final SResultatsBiefRSN[] biefs= (SResultatsBiefRSN[])getObjects(false);
    if (biefs == null) {
      return;
    }
    //    setModel(new LidoTableauResultatsModel(biefs));
    //    //((LidoTableauBiefsSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    NumberFormat nf = NumberFormat.getInstance();
    //    nf.setMaximumFractionDigits(LidoResource.FRACTIONDIGITS);
    //    tcr.setNumberFormat(nf);
    //    for (int i = 0; i < getModel().getColumnCount(); i++)
    //      getColumn(getColumnName(i)).setCellRenderer(tcr);
     ((LidoTableauResultatsModel)getModel()).setObjects(biefs);
    tableChanged(new TableModelEvent(getModel()));
  }
  protected String getPropertyName() {
    return "biefsResultats";
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "numero";
        break;
      case 1 :
        r= "volum";
        break;
      case 2 :
        r= "volums";
        break;
      case 3 :
        r= "vbief";
        break;
      case 4 :
        r= "vappo";
        break;
      case 5 :
        r= "vperdu";
        break;
    }
    return r;
  }
  protected Comparator getComparator() {
    return null;
  }
}
class LidoTableauResultatsModel extends LidoTableauBaseModel {
  public LidoTableauResultatsModel(final SResultatsBiefRSN[] _biefs) {
    super(_biefs);
  }
  protected Object[] getTableauType(final int taille) {
    return new SResultatsBiefRSN[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Integer.class; // numero
      case 1 :
        return Double.class; // volum
      case 2 :
        return Double.class; // volums
      case 3 :
        return Double.class; // vbief
      case 4 :
        return Double.class; // vappo
      case 5 :
        return Double.class; // vperdu
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 6;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "num";
        break;
      case 1 :
        r= "volum";
        break;
      case 2 :
        r= "volums";
        break;
      case 3 :
        r= "vbief";
        break;
      case 4 :
        r= "vappo";
        break;
      case 5 :
        r= "vperdu";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SResultatsBiefRSN[] biefs= (SResultatsBiefRSN[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Integer(biefs[low_ + row].numero + 1);
          break;
        case 1 :
          r= new Double(biefs[low_ + row].volum);
          break;
        case 2 :
          r= new Double(biefs[low_ + row].volums);
          break;
        case 3 :
          r= new Double(biefs[low_ + row].vbief);
          break;
        case 4 :
          r= new Double(biefs[low_ + row].vappo);
          break;
        case 5 :
          r= new Double(biefs[low_ + row].vperdu);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return false;
  }
  public void setValueAt(final Object value, final int row, final int column) {}
}
