/*
 * @file         LidoTableauSingularites.java
 * @creation     1999-08-13
 * @modification $Date: 2006-09-19 15:05:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.tableau;
import java.util.Comparator;

import javax.swing.event.TableModelEvent;

import org.fudaa.dodico.corba.lido.SParametresBiefSingLigneRZO;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:00 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class LidoTableauSingularites extends LidoTableauBase {
  public LidoTableauSingularites() {
    super();
    init();
  }
  private void init() {
    setModel(
      new LidoTableauSingularitesModel(new SParametresBiefSingLigneRZO[0]));
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    BuTableCellEditor tceInteger =
    //      new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble =
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    tcr.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT);
    //    TableColumnModel colModel = getColumnModel();
    //    int n = colModel.getColumnCount();
    //    for (int i = 0; i < n; i++)
    //    {
    //      if (i != 1)
    //        colModel.getColumn(i).setCellRenderer(tcr);
    //    }
    //    BuTableCellRenderer tcrDouble = new BuTableCellRenderer();
    //    tcrDouble.setNumberFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    //    colModel.getColumn(1).setCellEditor(tceDouble);
    //    colModel.getColumn(1).setCellRenderer(tcrDouble);
    //    colModel.getColumn(2).setCellEditor(tceInteger);
  }
  public void reinitialise() {
    final SParametresBiefSingLigneRZO[] sings=
      (SParametresBiefSingLigneRZO[])getObjects(false);
    if (sings == null) {
      return;
    }
    //    setModel(new LidoTableauSingularitesModel(sings));
    //    //((LidoTableauSingularitesSelectionModel)getSelectionModel()).setOffset(0);
    //    BuTableCellRenderer tcr = new BuTableCellRenderer();
    //    BuTableCellEditor tceInteger =
    //      new BuTableCellEditor(BuTextField.createIntegerField());
    //    BuTableCellEditor tceDouble =
    //      new BuTableCellEditor(BuTextField.createDoubleField());
    //    NumberFormat nf = NumberFormat.getInstance();
    //    nf.setMaximumFractionDigits(LidoResource.FRACTIONDIGITS);
    //    tcr.setNumberFormat(nf);
    //    for (int i = 0; i < getModel().getColumnCount(); i++)
    //      getColumn(getColumnName(i)).setCellRenderer(tcr);
    //    //getColumn(getColumnName(0)).setWidth(30);
    //    //getColumn(getColumnName(1)).setWidth(30);
    //    getColumn(getColumnName(1)).setCellEditor(tceDouble);
    //    //getColumn(getColumnName(2)).setWidth(30);
    //    getColumn(getColumnName(2)).setCellEditor(tceInteger);
    //    //getColumn(getColumnName(3)).setWidth(30);
     ((LidoTableauSingularitesModel)getModel()).setObjects(sings);
    tableChanged(new TableModelEvent(getModel()));
  }
  protected String getPropertyName() {
    return "singularites";
  }
  protected Comparator getComparator() {
    return LidoParamsHelper.SINGULARITE_COMPARATOR();
  }
  protected static String getObjectFieldNameByColumn(final int col) {
    String r= "";
    switch (col) {
      case 0 :
        r= "numBief";
        break;
      case 1 :
        r= "xSing";
        break;
      case 2 :
        r= "nSing";
        break;
    }
    return r;
  }
}
class LidoTableauSingularitesModel extends LidoTableauBaseModel {
  public LidoTableauSingularitesModel(final SParametresBiefSingLigneRZO[] _sings) {
    super(_sings);
  }
  protected Object[] getTableauType(final int taille) {
    return new SParametresBiefSingLigneRZO[taille];
  }
  public Class getColumnClass(final int column) {
    switch (column) {
      case 0 :
        return Integer.class; // numBief
      case 1 :
        return Double.class; // abscisse seuil
      case 2 :
        return Integer.class; // no de loi de seuil
      case 3 :
        return Boolean.class; // dirty
      default :
        return null;
    }
  }
  public int getColumnCount() {
    return 4;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "n� bief";
        break;
      case 1 :
        r= "abscisse de seuil";
        break;
      case 2 :
        r= "n�loi de seuil";
        break;
      case 3 :
        r= "s�lection";
        break;
    }
    return r;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    final SParametresBiefSingLigneRZO[] sings=
      (SParametresBiefSingLigneRZO[])getObjects();
    if ((low_ + row) < high_) {
      switch (column) {
        case 0 :
          r= new Integer(sings[low_ + row].numBief + 1);
          break;
        case 1 :
          r= new Double(sings[low_ + row].xSing);
          break;
        case 2 :
          r= new Integer(sings[low_ + row].nSing);
          break;
        case 3 :
          r= new Boolean(dirty_[low_ + row]);
          break;
      }
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return ((column != 0) && (column != 3));
  }
  public void setValueAt(final Object value, final int row, final int column) {
    if ((low_ + row) < high_) {
      final SParametresBiefSingLigneRZO[] sings=
        (SParametresBiefSingLigneRZO[])getObjects();
      final Object old= getValueAt(row, column);
      switch (column) {
        case 1 :
          sings[low_ + row].xSing= ((Double)value).doubleValue();
          break;
        case 2 :
          sings[low_ + row].nSing= ((Integer)value).intValue();
          break;
      }
      if (!old.equals(value)) {
        dirty_[low_ + row]= true;
        FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
          new FudaaParamEvent(
            this,
            0,
            LidoResource.RZO,
            sings[low_ + row],
            "singularite " + (sings[low_ + row].numBief)));
      }
    }
  }
}
