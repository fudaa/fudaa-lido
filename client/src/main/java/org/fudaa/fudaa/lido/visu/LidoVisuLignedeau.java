/*
 * @file         LidoVisuLignedeau.java
 * @creation     1999-10-05
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.visu;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluLibMessage;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresBiefSituLigneRZO;
import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SParametresLIG;
import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.dodico.lido.DParametresLido;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BDialogContentImprimable;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoImport;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheLignedeau;
/**
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoVisuLignedeau
  extends BDialogContentImprimable
  implements ActionListener, KeyListener, PropertyChangeListener {
  private static final int GAUCHE= LidoResource.VISU_BIEF.GAUCHE;
  private static final int DROITE= LidoResource.VISU_BIEF.DROITE;
  BuPanel pnCtrl_;
  JCheckBox cbFond_,
    cbRivDr_,
    cbRivGa_,
    cbLidoI_,
    cbLidoR_,
    cbLidoImport_,
    cbLidoEnv_,
    cbRapid_;
  BuButton btAvancer_, btReculer_, btAvancerVite_, btReculerVite_;
  BuLabel lbTemps_, lbPasTemps_;
  LidoGrapheLignedeau graphe_;
  BGrapheEditeurAxes edAxes_;
  Vector biefs_;
  int temps_;
  SResultatsRSN rsn_;
  boolean isModeResultat_;
  FudaaProjet p_;
  public LidoVisuLignedeau(final boolean res, final FudaaProjet _p) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      "Visualisation de ligne d'eau",
      null);
    p_= _p;
    isModeResultat_= res;
    //    System.out.println("ligne d'eau");
    init();
  }
  public LidoVisuLignedeau(
    final BDialogContent _parent,
    final boolean res,
    final FudaaProjet _p) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      _parent,
      "Visualisation de ligne d'eau",
      null);
    p_= _p;
    isModeResultat_= res;
    init();
  }
  public void setTitle(String t) {
    if (t == null) {
      return;
    }
    super.setTitle(t);
    if (t.startsWith("Visualisation de ")) {
      t= t.substring(17);
      final String tt= "" + Character.toUpperCase(t.charAt(0));
      t= tt + t.substring(1);
    }
    if (graphe_ != null) {
      graphe_.getGraphe().titre_= t;
    }
  }
  private void init() {
    graphe_= new LidoGrapheLignedeau(isModeResultat_);
    graphe_.setInteractif(true);
    biefs_= new Vector();
    temps_= 0;
    rsn_= null;
    int m= 0;
    pnCtrl_= new BuPanel();
    pnCtrl_.setLayout(new BuHorizontalLayout(5, true, true));
    int n= 0;
    final BuPanel pnCbs= new BuPanel();
    pnCbs.setLayout(new BuVerticalLayout(5, false, false));
    pnCbs.setBorder(new EtchedBorder());
    cbFond_= new JCheckBox("Fond");
    cbFond_.addActionListener(this);
    pnCbs.add(cbFond_, n++);
    cbRivGa_= new JCheckBox("Rive Gauche");
    cbRivGa_.addActionListener(this);
    pnCbs.add(cbRivGa_, n++);
    cbRivDr_= new JCheckBox("Rive Droite");
    cbRivDr_.addActionListener(this);
    pnCbs.add(cbRivDr_, n++);
    pnCtrl_.add(pnCbs, m++);
    n= 0;
    final BuPanel pnLido= new BuPanel();
    pnLido.setBorder(new EtchedBorder());
    pnLido.setLayout(new BuVerticalLayout(5, false, false));
    cbLidoI_= new JCheckBox("Ligne d'eau initiale");
    cbLidoI_.addActionListener(this);
    cbLidoI_.setEnabled(false);
    pnLido.add(cbLidoI_, n++);
    if (isModeResultat_) {
      cbLidoR_= new JCheckBox("Ligne d'eau r�sultat");
      cbLidoR_.addActionListener(this);
      pnLido.add(cbLidoR_, n++);
      cbLidoEnv_= new JCheckBox("Enveloppe");
      cbLidoEnv_.addActionListener(this);
      pnLido.add(cbLidoEnv_, n++);
      final BuPanel pnLidoImport= new BuPanel();
      pnLidoImport.setLayout(new BuHorizontalLayout(1, true, true));
      cbLidoImport_= new JCheckBox();
      cbLidoImport_.addActionListener(this);
      pnLidoImport.add(cbLidoImport_, 0);
      final BuButton btImport= new BuButton("Importer");
      btImport.setActionCommand("IMPORTER");
      btImport.addActionListener(this);
      pnLidoImport.add(btImport, 1);
      pnLido.add(pnLidoImport, n++);
    }
    pnCtrl_.add(pnLido, m++);
    n= 0;
    final BuPanel pnAxes= new BuPanel();
    pnAxes.setBorder(new EtchedBorder());
    pnAxes.setLayout(new BuVerticalLayout(5, false, false));
    pnAxes.add(new BuLabel("Bornes des axes"), n++);
    edAxes_= new BGrapheEditeurAxes(graphe_);
    edAxes_.addActionListener(this);
    pnAxes.add(edAxes_, n++);
    pnCtrl_.add(pnAxes, m++);
    if (isModeResultat_) {
      n= 0;
      final BuPanel pnTempsExt= new BuPanel();
      pnTempsExt.setBorder(new EtchedBorder());
      pnTempsExt.setLayout(new BuVerticalLayout(5, false, false));
      lbPasTemps_= new BuLabel("Pas de temps: " + (temps_ + 1));
      pnTempsExt.add(lbPasTemps_, n++);
      int n2= 0;
      final BuPanel pnTemps= new BuPanel();
      pnTemps.setLayout(new BuGridLayout(2, 0, 0));
      btReculer_= new BuButton(BuResource.BU.getIcon("reculer"), "<");
      btReculer_.setActionCommand("RECULER");
      btReculer_.addActionListener(this);
      pnTemps.add(btReculer_, n2++);
      btAvancer_= new BuButton(BuResource.BU.getIcon("avancer"), ">");
      btAvancer_.setActionCommand("AVANCER");
      btAvancer_.addActionListener(this);
      pnTemps.add(btAvancer_, n2++);
      btReculerVite_= new BuButton(BuResource.BU.getIcon("reculervite"), "<<");
      btReculerVite_.setActionCommand("RECULERVITE");
      btReculerVite_.addActionListener(this);
      pnTemps.add(btReculerVite_, n2++);
      btAvancerVite_= new BuButton(BuResource.BU.getIcon("avancervite"), ">>");
      btAvancerVite_.setActionCommand("AVANCERVITE");
      btAvancerVite_.addActionListener(this);
      pnTemps.add(btAvancerVite_, n2++);
      pnTempsExt.add(pnTemps, n++);
      lbTemps_= new BuLabel("Temps: 0");
      pnTempsExt.add(lbTemps_, n++);
      pnCtrl_.add(pnTempsExt, m++);
    }
    n= 0;
    final BuPanel pnBtGr= new BuPanel();
    pnBtGr.setBorder(new EtchedBorder());
    pnBtGr.setLayout(new BuVerticalLayout(5, false, false));
    final BuButton btGraphe= new BuButton("Graphe");
    btGraphe.setActionCommand("GRAPHESETUP");
    btGraphe.addActionListener(this);
    pnBtGr.add(btGraphe, n++);
    cbRapid_= new JCheckBox("Affichage rapide");
    cbRapid_.addActionListener(this);
    cbRapid_.setSelected(graphe_.isRapide());
    pnBtGr.add(cbRapid_, n++);
    pnCtrl_.add(pnBtGr, m++);
    final Container cp= getContentPane();
    cp.setLayout(new BorderLayout());
    cp.add(BorderLayout.CENTER, graphe_);
    cp.add(BorderLayout.SOUTH, pnCtrl_);
    //pack();
  }
  public void setPermanent(final boolean p) {
    if (p) {
      cbLidoEnv_.setSelected(false);
      cbLidoEnv_.setEnabled(false);
      graphe_.setLigneDeauEnvVisible(false);
    }
  }
  // en mode visu bief
  public void addBief(
    final SParametresBiefSituLigneRZO b,
    final SParametresBiefBlocPRO[] p) {
    if (isModeResultat_) {
      throw new IllegalArgumentException("en mode resultat, il faut appeler: addBief(int, SParametresBiefBlocPRO[])");
    }
    if ((b == null) || (p == null)) {
      return;
    }
    graphe_.addBief(b, p);
    biefs_.add(b);
  }
  // en mode resultat
  public void addBief(final int b, final SParametresBiefBlocPRO[] p) {
    if (!isModeResultat_) {
      throw new IllegalArgumentException("en mode bief, il faut appeler: addBief(SParametresBiefSituLigneRZO, SParametresBiefBlocPRO[])");
    }
    graphe_.addBief(b, p);
    biefs_.add(new Integer(b));
  }
  public void setLigneDeauInitialeEnabled(final boolean e) {
    cbLidoI_.setEnabled(e);
  }
  public void commitData() {
    graphe_.commitData();
  }
  public void show() {
    graphe_.commitData();
    updateVisu();
    edAxes_.setAxes(graphe_.getAxes());
    super.show();
  }
  public void setResultats(final SResultatsRSN rsn) {
    if (rsn == rsn_) {
      return;
    }
    rsn_= rsn;
    graphe_.setResultats(rsn);
    edAxes_.setAxes(graphe_.getAxes());
    if (temps_ >= (rsn_.pasTemps.length - 1)) {
      temps_= rsn_.pasTemps.length - 1;
      btAvancer_.setEnabled(false);
      btAvancerVite_.setEnabled(false);
    }
    if (temps_ <= 0) {
      temps_= 0;
      btReculer_.setEnabled(false);
      btReculerVite_.setEnabled(false);
    }
    lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
    lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
  }
  public void setLaisses(final SParametresEXT ext) {
    graphe_.setLaisses(ext);
  }
  public void setLigneDeau(final SParametresLIG lig, final String name, final boolean initiale) {
    if (lig == null) {
      if (initiale) {
        cbLidoI_.setEnabled(false);
        graphe_.setLigneDeauInitiale(null, null);
      }
      return;
    }
    if (rsn_.pasTemps.length == 0) {
      return;
    }
    double[] lidox= null;
    double[] lidoz= null;
    int cmt= 0;
    for (int b= 0; b < biefs_.size(); b++) {
      final SResultatsBiefRSN bief=
        rsn_.pasTemps[0].ligBief[((Integer)biefs_.get(b)).intValue()];
      if (bief.ligne.length == 0) {
        continue;
      }
      for (int i= 0; i < lig.x.length; i++) {
        if ((lig.x[i] >= bief.ligne[0].x)
          && (lig.x[i] <= bief.ligne[bief.ligne.length - 1].x)) {
          cmt++;
        }
      }
    }
    lidox= new double[cmt];
    lidoz= new double[cmt];
    cmt= 0;
    for (int b= 0; b < biefs_.size(); b++) {
      final SResultatsBiefRSN bief=
        rsn_.pasTemps[0].ligBief[((Integer)biefs_.get(b)).intValue()];
      for (int i= 0; i < lig.x.length; i++) {
        if (bief.ligne.length == 0) {
          continue;
        }
        if ((lig.x[i] >= bief.ligne[0].x)
          && (lig.x[i] <= bief.ligne[bief.ligne.length - 1].x)) {
          lidox[cmt]= lig.x[i];
          lidoz[cmt]= lig.z[i];
          cmt++;
        }
      }
    }
    if (initiale) {
      cbLidoI_.setEnabled(true);
      graphe_.setLigneDeauInitiale(lidox, lidoz);
    } else {
      graphe_.addLigneDeauImport(lidox, lidoz, name);
      graphe_.setLigneDeauImportVisible(true);
      cbLidoImport_.setSelected(graphe_.isLigneDeauImportVisible());
    }
    edAxes_.setAxes(graphe_.getAxes());
  }
  public void propertyChange(final PropertyChangeEvent p) {
    graphe_.fullRepaint();
    updateVisu();
  }
  public void actionPerformed(final ActionEvent e) {
    String cmd= e.getActionCommand();
    if (cmd == null) {
      cmd= "";
    }
    final Object src= e.getSource();
    if ((src == cbRivGa_) || (src == cbRivDr_)) {
      final int rive=
        (cbRivGa_.isSelected() ? GAUCHE : 0)
          | (cbRivDr_.isSelected() ? DROITE : 0);
      graphe_.setRive(rive);
    } else if (src == cbFond_) {
      graphe_.setFondVisible(cbFond_.isSelected());
    } else if (src == cbLidoI_) {
      graphe_.setLigneDeauIVisible(cbLidoI_.isSelected());
    } else if (src == cbLidoR_) {
      graphe_.setLigneDeauRVisible(cbLidoR_.isSelected());
    } else if (src == cbLidoEnv_) {
      graphe_.setLigneDeauEnvVisible(cbLidoEnv_.isSelected());
    } else if (src == cbLidoImport_) {
      graphe_.setLigneDeauImportVisible(cbLidoImport_.isSelected());
    } else if (src == cbRapid_) {
      graphe_.setRapide(cbRapid_.isSelected());
    } else if (cmd.equals("GRAPHESETUP")) {
      final BDialogContent dl=
        new BDialogContent(
          (BuCommonInterface)LidoApplication.FRAME,
          this,
          "Graphe setup");
      final BGraphePersonnaliseur gp= new BGraphePersonnaliseur(graphe_.getGraphe());
      gp.addPropertyChangeListener(this);
      dl.getContentPane().add(gp);
      dl.show();
    } else if (cmd.startsWith("AVANCER")) {
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("temps.rapide", 5);
        temps_= Math.min(rsn_.pasTemps.length - 1, temps_ + avt);
      } else {
        temps_++;
      }
      graphe_.setTemps(temps_);
      edAxes_.setAxes(graphe_.getAxes());
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
      if (temps_ >= (rsn_.pasTemps.length - 1)) {
        btAvancer_.setEnabled(false);
        btAvancerVite_.setEnabled(false);
      }
      if (!btReculer_.isEnabled()) {
        btReculer_.setEnabled(true);
      }
      if (!btReculerVite_.isEnabled()) {
        btReculerVite_.setEnabled(true);
      }
    } else if (cmd.startsWith("RECULER")) {
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("temps.rapide", 5);
        temps_= Math.max(0, temps_ - avt);
      } else {
        temps_--;
      }
      graphe_.setTemps(temps_);
      edAxes_.setAxes(graphe_.getAxes());
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
      if (temps_ <= 0) {
        btReculer_.setEnabled(false);
        btReculerVite_.setEnabled(false);
      }
      if (!btAvancer_.isEnabled()) {
        btAvancer_.setEnabled(true);
      }
      if (!btAvancerVite_.isEnabled()) {
        btAvancerVite_.setEnabled(true);
      }
    } else if (src == edAxes_) {
      final Double[][] axes= edAxes_.getAxes();
      graphe_.setAxes(axes[0], axes[1]);
      edAxes_.setAxes(graphe_.getAxes());
    } else if ("IMPORTER".equals(cmd)) {
      final File file= LidoImport.chooseFile("lig");
      if (file == null) {
        return;
      }
      try {
        final SParametresLIG lig= DParametresLido.litParametresLIG(file);
        String name= file.getName();
        name= name.substring(0, name.lastIndexOf('.'));
        setLigneDeau(lig, name, false);
      } catch (final java.io.IOException ioe) {
        new BuDialogError(
          (BuCommonInterface)LidoApplication.FRAME,
          ((BuCommonInterface)LidoApplication.FRAME).getInformationsSoftware(),
          ioe.getMessage())
          .activate();
        return;
      }
    }
  }
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    if (p_ != null) {
      return p_.getInformationsDocument();
    }
    return new BuInformationsDocument();
  }
  private void updateVisu() {
    if (isBuildMode()) {
      if (CtuluLibMessage.DEBUG) {
        System.out.println("mode construction");
      }
      return;
    }
    cbFond_.setSelected(graphe_.isFondVisible());
    cbRivGa_.setSelected((graphe_.getRive() & GAUCHE) == GAUCHE);
    cbRivDr_.setSelected((graphe_.getRive() & DROITE) == DROITE);
    cbLidoI_.setSelected(graphe_.isLigneDeauIVisible());
    if (isModeResultat_) {
      cbLidoR_.setSelected(graphe_.isLigneDeauRVisible());
      cbLidoImport_.setSelected(graphe_.isLigneDeauImportVisible());
      cbLidoEnv_.setSelected(graphe_.isLigneDeauEnvVisible());
    }
  }
}
