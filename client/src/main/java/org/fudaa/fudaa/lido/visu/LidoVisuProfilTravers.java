/*
 * @file         LidoVisuProfilTravers.java
 * @creation     1999-10-19
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.visu;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BDialogContentImprimable;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;

import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoExport;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheCourbe;
import org.fudaa.fudaa.lido.graphe.LidoGrapheProfil;
import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoVisuProfilTravers
  extends BDialogContentImprimable
  implements ActionListener, KeyListener {
  BuPanel pnCtrl_;
  BuButton btAvancer_, btReculer_, btAvancerVite_, btReculerVite_;
  JCheckBox cbZoom_, cbRives_, cbAdjustZ_, cbFixAxes_, cbLidoEnv_;
  BuButton btProfAvancer_,
    btProfReculer_,
    btProfAvancerVite_,
    btProfReculerVite_;
  BuButton btExportProfil_;
  BuLabel lbTemps_, lbPasTemps_, lbNoProfil_;
  JTabbedPane tab_;
  LidoGrapheProfil graphe_;
  LidoGrapheCourbe grLimni_, grHydro_, grTarage_;
  BGrapheEditeurAxes edAxes_;
  int temps_;
  SResultatsRSN rsn_;
  SParametresBiefBlocPRO prof_;
  LidoParamsHelper ph_;
  public LidoVisuProfilTravers(final LidoParamsHelper ph) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      "Visualisation du profil",
      ph.getInformationsDocument());
    ph_= ph;
    init();
  }
  public LidoVisuProfilTravers(final BDialogContent _parent, final LidoParamsHelper ph) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      _parent,
      "Visualisation du profil",
      ph.getInformationsDocument());
    ph_= ph;
    init();
  }
  private void init() {
    graphe_= new LidoGrapheProfil();
    graphe_.setInteractif(true);
    temps_= 0;
    rsn_= null;
    prof_= null;
    int m= 0;
    pnCtrl_= new BuPanel();
    pnCtrl_.setLayout(new BuHorizontalLayout(5, true, true));
    int n= 0;
    final BuPanel pnProfExt= new BuPanel();
    pnProfExt.setBorder(new EtchedBorder());
    pnProfExt.setLayout(new BuVerticalLayout(5, false, false));
    final BuPanel pnNoProfil= new BuPanel();
    pnNoProfil.add(new BuLabel("Profil"));
    lbNoProfil_= new BuLabel("1000");
    pnNoProfil.add(lbNoProfil_);
    pnProfExt.add(pnNoProfil, n++);
    int n2= 0;
    final BuPanel pnProf= new BuPanel();
    pnProf.setLayout(new BuGridLayout(2, 0, 0));
    btProfReculer_= new BuButton(BuResource.BU.getIcon("reculer"), "<");
    btProfReculer_.setActionCommand("PROFRECULER");
    pnProf.add(btProfReculer_, n2++);
    btProfAvancer_= new BuButton(BuResource.BU.getIcon("avancer"), ">");
    btProfAvancer_.setActionCommand("PROFAVANCER");
    pnProf.add(btProfAvancer_, n2++);
    btProfReculerVite_=
      new BuButton(BuResource.BU.getIcon("reculervite"), "<<");
    btProfReculerVite_.setActionCommand("PROFRECULERVITE");
    pnProf.add(btProfReculerVite_, n2++);
    btProfAvancerVite_=
      new BuButton(BuResource.BU.getIcon("avancervite"), ">>");
    btProfAvancerVite_.setActionCommand("PROFAVANCERVITE");
    pnProf.add(btProfAvancerVite_, n2++);
    pnProfExt.add(pnProf, n++);
    btExportProfil_= new BuButton("Exporter");
    btExportProfil_.addActionListener(this);
    pnProfExt.add(btExportProfil_, n++);
    pnCtrl_.add(pnProfExt, m++);
    n= 0;
    final BuPanel pnCbs= new BuPanel();
    pnCbs.setLayout(new BuVerticalLayout(5, false, false));
    pnCbs.setBorder(new EtchedBorder());
    cbZoom_= new JCheckBox("Zoom sur lit mineur");
    cbZoom_.addActionListener(this);
    cbZoom_.setSelected(graphe_.isZoomLitMineur());
    pnCbs.add(cbZoom_, n++);
    cbRives_= new JCheckBox("Affichage des rives");
    cbRives_.addActionListener(this);
    cbRives_.setSelected(graphe_.isRivesVisible());
    pnCbs.add(cbRives_, n++);
    cbAdjustZ_= new JCheckBox("Ajustement de la cote");
    cbAdjustZ_.addActionListener(this);
    cbAdjustZ_.setSelected(graphe_.isYAjuste());
    pnCbs.add(cbAdjustZ_, n++);
    cbFixAxes_= new JCheckBox("Fixer l'�chelle");
    cbFixAxes_.addActionListener(this);
    cbFixAxes_.setSelected(graphe_.isFixAxes());
    pnCbs.add(cbFixAxes_, n++);
    cbLidoEnv_= new JCheckBox("Enveloppe");
    cbLidoEnv_.addActionListener(this);
    cbLidoEnv_.setEnabled(false);
    pnCbs.add(cbLidoEnv_, n++);
    pnCtrl_.add(pnCbs, m++);
    n= 0;
    final BuPanel pnAxes= new BuPanel();
    pnAxes.setBorder(new EtchedBorder());
    pnAxes.setLayout(new BuVerticalLayout(5, false, false));
    pnAxes.add(new BuLabel("Bornes des axes"), n++);
    edAxes_= new BGrapheEditeurAxes(graphe_);
    edAxes_.addActionListener(this);
    pnAxes.add(edAxes_, n++);
    pnCtrl_.add(pnAxes, m++);
    n= 0;
    final BuPanel pnTempsExt= new BuPanel();
    pnTempsExt.setBorder(new EtchedBorder());
    pnTempsExt.setLayout(new BuVerticalLayout(5, false, false));
    lbPasTemps_= new BuLabel("Pas de temps: " + (temps_ + 1));
    pnTempsExt.add(lbPasTemps_, n++);
    n2= 0;
    final BuPanel pnTemps= new BuPanel();
    pnTemps.setLayout(new BuGridLayout(2, 0, 0));
    btReculer_= new BuButton(BuResource.BU.getIcon("reculer"), "<");
    btReculer_.setActionCommand("RECULER");
    btReculer_.addActionListener(this);
    pnTemps.add(btReculer_, n2++);
    btAvancer_= new BuButton(BuResource.BU.getIcon("avancer"), ">");
    btAvancer_.setActionCommand("AVANCER");
    btAvancer_.addActionListener(this);
    pnTemps.add(btAvancer_, n2++);
    btReculerVite_= new BuButton(BuResource.BU.getIcon("reculervite"), "<<");
    btReculerVite_.setActionCommand("RECULERVITE");
    btReculerVite_.addActionListener(this);
    pnTemps.add(btReculerVite_, n2++);
    btAvancerVite_= new BuButton(BuResource.BU.getIcon("avancervite"), ">>");
    btAvancerVite_.setActionCommand("AVANCERVITE");
    btAvancerVite_.addActionListener(this);
    pnTemps.add(btAvancerVite_, n2++);
    pnTempsExt.add(pnTemps, n++);
    lbTemps_= new BuLabel("Temps: 0");
    pnTempsExt.add(lbTemps_, n++);
    pnCtrl_.add(pnTempsExt, m++);
    final BuPanel pnBtGr= new BuPanel();
    final BuVerticalLayout loBtGr= new BuVerticalLayout(5, false, false);
    pnBtGr.setLayout(loBtGr);
    final BuButton btGraphe= new BuButton("Graphe");
    btGraphe.setActionCommand("GRAPHESETUP");
    btGraphe.addActionListener(this);
    pnBtGr.add(btGraphe, 0);
    pnCtrl_.add(pnBtGr, m++);
    final BuPanel pnGraphe= new BuPanel();
    pnGraphe.setLayout(new BorderLayout());
    pnGraphe.add(BorderLayout.CENTER, graphe_);
    pnGraphe.add(BorderLayout.SOUTH, pnCtrl_);
    final BuPanel pnGrLimni= new BuPanel();
    pnGrLimni.setLayout(new BorderLayout());
    grLimni_= new LidoGrapheCourbe("Limnigramme", "T", "H", "s", "m");
    grLimni_.setInteractif(true);
    m= 0;
    final BuPanel pnCtrlLimni= new BuPanel();
    pnCtrlLimni.setLayout(new BuHorizontalLayout(5, true, true));
    n= 0;
    final BuPanel pnExportLimni= new BuPanel();
    pnExportLimni.setLayout(new BuVerticalLayout(5, false, false));
    pnExportLimni.setBorder(new EtchedBorder());
    pnExportLimni.add(new BuLabel("Exporter"), n++);
    final BuButton btExportLimni= new BuButton("Format CLM");
    btExportLimni.setActionCommand("EXPORTLIMNICLM");
    btExportLimni.addActionListener(this);
    pnExportLimni.add(btExportLimni, n++);
    pnCtrlLimni.add(pnExportLimni, m++);
    pnGrLimni.add(BorderLayout.CENTER, grLimni_);
    pnGrLimni.add(BorderLayout.SOUTH, pnCtrlLimni);
    final BuPanel pnGrHydro= new BuPanel();
    pnGrHydro.setLayout(new BorderLayout());
    grHydro_= new LidoGrapheCourbe("Hydrogramme", "T", "Q", "s", "m3/s");
    grHydro_.setInteractif(true);
    m= 0;
    final BuPanel pnCtrlHydro= new BuPanel();
    pnCtrlHydro.setLayout(new BuHorizontalLayout(5, true, true));
    n= 0;
    final BuPanel pnExportHydro= new BuPanel();
    pnExportHydro.setLayout(new BuVerticalLayout(5, false, false));
    pnExportHydro.setBorder(new EtchedBorder());
    pnExportHydro.add(new BuLabel("Exporter"), n++);
    final BuButton btExportHydro= new BuButton("Format CLM");
    btExportHydro.setActionCommand("EXPORTHYDROCLM");
    btExportHydro.addActionListener(this);
    pnExportHydro.add(btExportHydro, n++);
    pnCtrlHydro.add(pnExportHydro, m++);
    pnGrHydro.add(BorderLayout.CENTER, grHydro_);
    pnGrHydro.add(BorderLayout.SOUTH, pnCtrlHydro);
    final BuPanel pnGrTarage= new BuPanel();
    pnGrTarage.setLayout(new BorderLayout());
    grTarage_= new LidoGrapheCourbe("Tarage", "H", "Q", "m", "m3/s");
    grTarage_.setInteractif(true);
    m= 0;
    final BuPanel pnCtrlTarage= new BuPanel();
    pnCtrlTarage.setLayout(new BuHorizontalLayout(5, true, true));
    n= 0;
    final BuPanel pnExportTarage= new BuPanel();
    pnExportTarage.setLayout(new BuVerticalLayout(5, false, false));
    pnExportTarage.setBorder(new EtchedBorder());
    pnExportTarage.add(new BuLabel("Exporter"), n++);
    final BuButton btExportTarage= new BuButton("Format CLM");
    btExportTarage.setActionCommand("EXPORTTARAGECLM");
    btExportTarage.addActionListener(this);
    pnExportTarage.add(btExportTarage, n++);
    pnCtrlTarage.add(pnExportTarage, m++);
    pnGrTarage.add(BorderLayout.CENTER, grTarage_);
    pnGrTarage.add(BorderLayout.SOUTH, pnCtrlTarage);
    tab_= new JTabbedPane();
    tab_.addTab("Profil", pnGraphe);
    tab_.addTab("Limni", pnGrLimni);
    tab_.addTab("Hydro", pnGrHydro);
    tab_.addTab("Tarage", pnGrTarage);
    tab_.setEnabledAt(1, false);
    tab_.setEnabledAt(2, false);
    tab_.setEnabledAt(3, false);
    getContentPane().add(BorderLayout.CENTER, tab_);
    //pack();
  }
  public void setPermanent(final boolean p) {
    if (p) {
      tab_.setEnabledAt(1, false);
      tab_.setEnabledAt(2, false);
      tab_.setEnabledAt(3, false);
      cbLidoEnv_.setEnabled(false);
    } else {
      tab_.setEnabledAt(1, true);
      tab_.setEnabledAt(2, true);
      tab_.setEnabledAt(3, true);
      cbLidoEnv_.setEnabled(false);
    }
  }
  public void setProfil(final SParametresBiefBlocPRO prof) {
    prof_= prof;
    graphe_.setProfil(prof_);
    lbNoProfil_.setText("" + (prof_.indice + 1));
    grLimni_.setValeurs(
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.LIMNI));
    grHydro_.setValeurs(
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.HYDRO));
    grTarage_.setValeurs(
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.TARAGE));
    calculeNiveauEau();
    calculeEnveloppeEau();
    edAxes_.setAxes(graphe_.getAxes());
  }
  public void setResultats(final SResultatsRSN rsn) {
    if (rsn == rsn_) {
      return;
    }
    rsn_= rsn;
    grLimni_.setValeurs(
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.LIMNI));
    grHydro_.setValeurs(
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.HYDRO));
    grTarage_.setValeurs(
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.TARAGE));
    calculeEnveloppeEau();
    calculeNiveauEau();
  }
  public void addActionListener(final ActionListener l) {
    btProfReculer_.addActionListener(l);
    btProfAvancer_.addActionListener(l);
    btProfReculerVite_.addActionListener(l);
    btProfAvancerVite_.addActionListener(l);
  }
  private void calculeEnveloppeEau() {
    if (rsn_ == null) {
      return;
    }
    if (prof_ == null) {
      return;
    }
    final double[][] v=
      ph_.PROFIL().getCourbeResultat(prof_, LidoResource.LIMITE.LIMNI);
    if (v == null) {
      return;
    }
    double min= v[0][1], max= v[0][1];
    for (int i= 0; i < v.length; i++) {
      if (v[i][1] < min) {
        min= v[i][1];
      }
      if (v[i][1] > max) {
        max= v[i][1];
      }
    }
    graphe_.setNiveauEauEnv(min, max);
  }
  private void calculeNiveauEau() {
    if (rsn_ == null) {
      return;
    }
    if (prof_ == null) {
      return;
    }
    graphe_.setEauVisible(false);
    final Double niveau= ph_.PROFIL().getCoteEauResultat(prof_, temps_);
    if (niveau != null) {
      graphe_.setEauVisible(true);
      graphe_.setNiveauEau(niveau.doubleValue());
      if (temps_ >= (rsn_.pasTemps.length - 1)) {
        temps_= rsn_.pasTemps.length - 1;
        btAvancer_.setEnabled(false);
        btAvancerVite_.setEnabled(false);
      }
      if (temps_ <= 0) {
        temps_= 0;
        btReculer_.setEnabled(false);
        btReculerVite_.setEnabled(false);
      }
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
    }
    if (temps_ >= (rsn_.pasTemps.length - 1)) {
      btAvancer_.setEnabled(false);
      btAvancerVite_.setEnabled(false);
    }
    if (temps_ <= 0) {
      btReculer_.setEnabled(false);
      btReculerVite_.setEnabled(false);
    }
  }
  public void propertyChange(final PropertyChangeEvent p) {
    if (isBuildMode()) {
      return;
    }
    graphe_.fullRepaint();
  }
  public void actionPerformed(final ActionEvent e) {
    String cmd= e.getActionCommand();
    if (cmd == null) {
      cmd= "";
    }
    final Object src= e.getSource();
    if (cmd.equals("GRAPHESETUP")) {
      final BDialogContent dl=
        new BDialogContent(
          (BuCommonInterface)LidoApplication.FRAME,
          this,
          "Graphe setup");
      final BGraphePersonnaliseur gp= new BGraphePersonnaliseur(graphe_.getGraphe());
      gp.addPropertyChangeListener(this);
      dl.getContentPane().add(gp);
      dl.show();
    } else if (cmd.startsWith("AVANCER")) {
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("temps.rapide", 5);
        temps_= Math.min(rsn_.pasTemps.length - 1, temps_ + avt);
      } else {
        temps_++;
      }
      calculeNiveauEau();
      edAxes_.setAxes(graphe_.getAxes());
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
      if (temps_ >= (rsn_.pasTemps.length - 1)) {
        btAvancer_.setEnabled(false);
        btAvancerVite_.setEnabled(false);
      }
      if (!btReculer_.isEnabled()) {
        btReculer_.setEnabled(true);
      }
      if (!btReculerVite_.isEnabled()) {
        btReculerVite_.setEnabled(true);
      }
    } else if (cmd.startsWith("RECULER")) {
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("temps.rapide", 5);
        temps_= Math.max(0, temps_ - avt);
      } else {
        temps_--;
      }
      calculeNiveauEau();
      edAxes_.setAxes(graphe_.getAxes());
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
      if (temps_ <= 0) {
        btReculer_.setEnabled(false);
        btReculerVite_.setEnabled(false);
      }
      if (!btAvancer_.isEnabled()) {
        btAvancer_.setEnabled(true);
      }
      if (!btAvancerVite_.isEnabled()) {
        btAvancerVite_.setEnabled(true);
      }
    } else if (src == btExportProfil_) {
      exportProfil();
    } else if (src == edAxes_) {
      final Double[][] axes= edAxes_.getAxes();
      graphe_.setAxes(axes[0], axes[1]);
      edAxes_.setAxes(graphe_.getAxes());
    } else if (src == cbZoom_) {
      graphe_.setZoomLitMineur(cbZoom_.isSelected());
    } else if (src == cbRives_) {
      graphe_.setRivesVisible(cbRives_.isSelected());
    } else if (src == cbAdjustZ_) {
      graphe_.setYAjuste(cbAdjustZ_.isSelected());
    } else if (src == cbFixAxes_) {
      final boolean selected= cbFixAxes_.isSelected();
      if (selected) {
        cbZoom_.setSelected(false);
        graphe_.setZoomLitMineur(false);
        cbAdjustZ_.setSelected(false);
        graphe_.setYAjuste(false);
      }
      graphe_.setFixAxes(selected);
    } else if (src == cbLidoEnv_) {
      graphe_.setEauEnvVisible(cbLidoEnv_.isSelected());
    } else if ("EXPORTLIMNICLM".equals(cmd)) {
      exportClm(LidoResource.LIMITE.LIMNI);
    } else if ("EXPORTHYDROCLM".equals(cmd)) {
      exportClm(LidoResource.LIMITE.HYDRO);
    } else if ("EXPORTTARAGECLM".equals(cmd)) {
      exportClm(LidoResource.LIMITE.TARAGE);
    }
  }
  private void exportClm(final int type) {
    String tit= "";
    if (type == LidoResource.LIMITE.LIMNI) {
      tit= "LIMNIGRAMME";
    } else if (type == LidoResource.LIMITE.HYDRO) {
      tit= "HYDROGRAMME";
    } else if (type == LidoResource.LIMITE.TARAGE) {
      tit= "TARAGE";
    }
    final BuDialogInput dl=
      new BuDialogInput(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME)
          .getImplementation()
          .getInformationsSoftware(),
        "Exportation CLM",
        "Titre",
        tit);
    final int res= dl.activate();
    if (res != JOptionPane.OK_OPTION) {
      return;
    }
    final SParametresCondLimBlocCLM loi=
      ph_.LOICLM().creeLoiCLM(
        ph_.PROFIL().getCourbeResultat(prof_, type),
        type,
        dl.getValue());
    final File file= LidoExport.chooseFile("clm");
    if (file == null) {
      return;
    }
    try {
      LidoExport.exportCLM(new SParametresCondLimBlocCLM[] { loi }, file);
    } catch (final Throwable t) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME)
          .getImplementation()
          .getInformationsSoftware(),
        t.getMessage())
        .activate();
    }
  }
  private void exportProfil() {
    final File file= LidoExport.chooseFile(null);
    if (file == null) {
      return;
    }
    try {
      LidoExport.exportResultatsProfil(prof_, rsn_, file, ph_);
    } catch (final Throwable t) {
      new BuDialogError(
        (BuCommonInterface)LidoApplication.FRAME,
        ((BuCommonInterface)LidoApplication.FRAME)
          .getImplementation()
          .getInformationsSoftware(),
        t.getMessage())
        .activate();
    }
  }
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  public BuInformationsDocument getInformationsDocument() {
    if (ph_ != null) {
      return ph_.getInformationsDocument();
    }
    return new BuInformationsDocument();
  }
}
