/*
 * @file         LidoVisuResultatEnLong.java
 * @creation     1999-10-06
 * @modification $Date: 2006-09-19 15:05:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido.visu;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.beans.PropertyChangeEvent;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.lido.SResultatsRSN;

import org.fudaa.ebli.dialog.BDialogContent;
import org.fudaa.ebli.dialog.BDialogContentImprimable;
import org.fudaa.ebli.graphe.BGrapheEditeurAxes;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;

import org.fudaa.fudaa.lido.LidoApplication;
import org.fudaa.fudaa.lido.LidoPreferences;
import org.fudaa.fudaa.lido.LidoResource;
import org.fudaa.fudaa.lido.graphe.LidoGrapheResultatEnLong;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:05:06 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class LidoVisuResultatEnLong
  extends BDialogContentImprimable
  implements ActionListener, KeyListener {
  public final static String HAUTEUREAU= LidoResource.RESULTAT.HAUTEUREAU;
  public final static String SECTIONMOUILLEE=
    LidoResource.RESULTAT.SECTIONMOUILLEE;
  public final static String RAYON= LidoResource.RESULTAT.RAYON;
  public final static String B1= LidoResource.RESULTAT.B1;
  public final static String VITESSE= LidoResource.RESULTAT.VITESSE;
  public final static String DEBITTOTAL= LidoResource.RESULTAT.DEBITTOTAL;
  public final static String FROUDE= LidoResource.RESULTAT.FROUDE;
  public final static String DEBIT= LidoResource.RESULTAT.DEBIT;
  public final static String CHARGE= LidoResource.RESULTAT.CHARGE;
  public final static String REGIMECRIT= LidoResource.RESULTAT.REGIMECRIT;
  public final static String REGIMEUNIF= LidoResource.RESULTAT.REGIMEUNIF;
  public final static String FORCETRAC= LidoResource.RESULTAT.FORCETRAC;
  String type_;
  String strMin_, strMax_;
  BuPanel pnCtrl_;
  JCheckBox cbMin_, cbMax_, cbRapid_;
  BuButton btAvancer_, btReculer_, btAvancerVite_, btReculerVite_;
  BuLabel lbTemps_, lbPasTemps_;
  LidoGrapheResultatEnLong graphe_;
  BGrapheEditeurAxes edAxes_;
  int temps_;
  SResultatsRSN rsn_;
  public LidoVisuResultatEnLong(final String type, final BuInformationsDocument _id) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      HAUTEUREAU.equals(type)
        ? "Visualisation de la hauteur d'eau"
        : SECTIONMOUILLEE.equals(type)
        ? "Visualisation des sections mouill�es"
        : RAYON.equals(type)
        ? "Visualisation des rayons hydrauliques"
        : B1.equals(type)
        ? "Visualisation des largeurs au miroir"
        : VITESSE.equals(type)
        ? "Visualisation des vitesses"
        : DEBITTOTAL.equals(type)
        ? "Visualisation du d�bit total"
        : FROUDE.equals(type)
        ? "Visualisation des Froude"
        : DEBIT.equals(type)
        ? "Visualisation du d�bit"
        : CHARGE.equals(type)
        ? "Visualisation de la charge"
        : REGIMECRIT.equals(type)
        ? "Visualisation du r�gime critique"
        : REGIMEUNIF.equals(type)
        ? "Visualisation du r�gime uniforme"
        : FORCETRAC.equals(type)
        ? "Visualisation de la force tractrice"
        : "",
      _id);
    type_= type;
    init();
  }
  public LidoVisuResultatEnLong(
    final BDialogContent _parent,
    final String type,
    final BuInformationsDocument _id) {
    super(
      (BuCommonInterface)LidoApplication.FRAME,
      _parent,
      HAUTEUREAU.equals(type)
        ? "Visualisation de la hauteur d'eau"
        : SECTIONMOUILLEE.equals(type)
        ? "Visualisation des sections mouill�es"
        : RAYON.equals(type)
        ? "Visualisation des rayons hydrauliques"
        : B1.equals(type)
        ? "Visualisation des largeurs au miroir"
        : VITESSE.equals(type)
        ? "Visualisation des vitesses"
        : DEBITTOTAL.equals(type)
        ? "Visualisation du d�bit total"
        : FROUDE.equals(type)
        ? "Visualisation des Froude"
        : DEBIT.equals(type)
        ? "Visualisation du d�bit"
        : CHARGE.equals(type)
        ? "Visualisation de la charge"
        : REGIMECRIT.equals(type)
        ? "Visualisation du r�gime critique"
        : REGIMEUNIF.equals(type)
        ? "Visualisation du r�gime uniforme"
        : FORCETRAC.equals(type)
        ? "Visualisation de la force tractrice"
        : "",
      _id);
    type_= type;
    init();
  }
  private void init() {
    graphe_= new LidoGrapheResultatEnLong(type_);
    graphe_.setInteractif(true);
    temps_= 0;
    rsn_= null;
    if (HAUTEUREAU.equals(type_)) {
      strMin_= "H";
      strMax_= null;
    } else if (SECTIONMOUILLEE.equals(type_)) {
      strMin_= "S1";
      strMax_= "S2";
    } else if (RAYON.equals(type_)) {
      strMin_= "R1";
      strMax_= "R2";
    } else if (B1.equals(type_)) {
      strMin_= "B1";
      strMax_= null;
    } else if (VITESSE.equals(type_)) {
      strMin_= "Vmin";
      strMax_= "Vmaj";
    } else if (DEBITTOTAL.equals(type_)) {
      strMin_= "Q";
      strMax_= null;
    } else if (FROUDE.equals(type_)) {
      strMin_= "Frou";
      strMax_= null;
    } else if (DEBIT.equals(type_)) {
      strMin_= "Q1";
      strMax_= "Q2";
    } else if (CHARGE.equals(type_)) {
      strMin_= "Charge";
      strMax_= null;
    } else if (REGIMECRIT.equals(type_)) {
      strMin_= "Hcmin";
      strMax_= null;
    } else if (REGIMEUNIF.equals(type_)) {
      strMin_= "Hnmin";
      strMax_= null;
    } else if (FORCETRAC.equals(type_)) {
      strMin_= "Ft";
      strMax_= null;
    } else {
      strMin_= null;
      strMax_= null;
    }
    int m= 0;
    pnCtrl_= new BuPanel();
    pnCtrl_.setLayout(new BuHorizontalLayout(5, true, true));
    int n= 0;
    final BuPanel pnCbs= new BuPanel();
    pnCbs.setLayout(new BuVerticalLayout(5, false, false));
    pnCbs.setBorder(new EtchedBorder());
    if (strMin_ != null) {
      cbMin_= new JCheckBox(strMin_ + (strMax_ != null ? ": lit mineur" : ""));
      cbMin_.addActionListener(this);
      cbMin_.setSelected(graphe_.isMinEnabled());
      pnCbs.add(cbMin_, n++);
    }
    if (strMax_ != null) {
      cbMax_= new JCheckBox(strMax_ + ": lit majeur");
      cbMax_.addActionListener(this);
      cbMax_.setSelected(graphe_.isMaxEnabled());
      pnCbs.add(cbMax_, n++);
    }
    pnCtrl_.add(pnCbs, m++);
    n= 0;
    final BuPanel pnAxes= new BuPanel();
    pnAxes.setBorder(new EtchedBorder());
    pnAxes.setLayout(new BuVerticalLayout(5, false, false));
    pnAxes.add(new BuLabel("Bornes des axes"), n++);
    edAxes_= new BGrapheEditeurAxes(graphe_);
    edAxes_.addActionListener(this);
    pnAxes.add(edAxes_, n++);
    pnCtrl_.add(pnAxes, m++);
    n= 0;
    final ContraintesPanel pnContraintes= new ContraintesPanel();
    pnContraintes.setBorder(new EtchedBorder());
    pnCtrl_.add(pnContraintes, m++);
    n= 0;
    final BuPanel pnTempsExt= new BuPanel();
    pnTempsExt.setBorder(new EtchedBorder());
    pnTempsExt.setLayout(new BuVerticalLayout(5, false, false));
    lbPasTemps_= new BuLabel("Pas de temps: " + (temps_ + 1));
    pnTempsExt.add(lbPasTemps_, n++);
    int n2= 0;
    final BuPanel pnTemps= new BuPanel();
    pnTemps.setLayout(new BuGridLayout(2, 0, 0));
    btReculer_= new BuButton(BuResource.BU.getIcon("reculer"), "<");
    btReculer_.setActionCommand("RECULER");
    btReculer_.addActionListener(this);
    pnTemps.add(btReculer_, n2++);
    btAvancer_= new BuButton(BuResource.BU.getIcon("avancer"), ">");
    btAvancer_.setActionCommand("AVANCER");
    btAvancer_.addActionListener(this);
    pnTemps.add(btAvancer_, n2++);
    btReculerVite_= new BuButton(BuResource.BU.getIcon("reculervite"), "<<");
    btReculerVite_.setActionCommand("RECULERVITE");
    btReculerVite_.addActionListener(this);
    pnTemps.add(btReculerVite_, n2++);
    btAvancerVite_= new BuButton(BuResource.BU.getIcon("avancervite"), ">>");
    btAvancerVite_.setActionCommand("AVANCERVITE");
    btAvancerVite_.addActionListener(this);
    pnTemps.add(btAvancerVite_, n2++);
    pnTempsExt.add(pnTemps, n++);
    lbTemps_= new BuLabel("Temps: 0");
    pnTempsExt.add(lbTemps_, n++);
    pnCtrl_.add(pnTempsExt, m++);
    final BuPanel pnBtGr= new BuPanel();
    final BuVerticalLayout loBtGr= new BuVerticalLayout(5, false, false);
    pnBtGr.setLayout(loBtGr);
    final BuButton btGraphe= new BuButton("Graphe");
    btGraphe.setActionCommand("GRAPHESETUP");
    btGraphe.addActionListener(this);
    int n3= 0;
    pnBtGr.add(btGraphe, n3++);
    cbRapid_= new JCheckBox("Affichage rapide");
    cbRapid_.addActionListener(this);
    cbRapid_.setSelected(graphe_.isRapide());
    pnBtGr.add(cbRapid_, n3++);
    pnCtrl_.add(pnBtGr, m++);
    final Container cp= getContentPane();
    cp.setLayout(new BorderLayout());
    cp.add(BorderLayout.CENTER, graphe_);
    cp.add(BorderLayout.SOUTH, pnCtrl_);
    //pack();
  }
  public void addBief(final int b) {
    graphe_.addBief(b);
    edAxes_.setAxes(graphe_.getAxes());
  }
  public void show() {
    System.err.println("lidoVisuLignedeau.show()");
    graphe_.commitData();
    updateVisu();
    edAxes_.setAxes(graphe_.getAxes());
    super.show();
  }
  public void setResultats(final SResultatsRSN rsn) {
    if (rsn == rsn_) {
      return;
    }
    rsn_= rsn;
    graphe_.setResultats(rsn);
    edAxes_.setAxes(graphe_.getAxes());
    if (temps_ >= (rsn_.pasTemps.length - 1)) {
      temps_= rsn_.pasTemps.length - 1;
      btAvancer_.setEnabled(false);
      btAvancerVite_.setEnabled(false);
    }
    if (temps_ <= 0) {
      temps_= 0;
      btReculer_.setEnabled(false);
      btReculerVite_.setEnabled(false);
    }
    lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
    lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
  }
  public void propertyChange(final PropertyChangeEvent p) {
    if (isBuildMode()) {
      return;
    }
    graphe_.fullRepaint();
    updateVisu();
  }
  public void actionPerformed(final ActionEvent e) {
    final Object src= e.getSource();
    String cmd= e.getActionCommand();
    if (cmd == null) {
      cmd= "";
    }
    if (src == cbMin_) {
      graphe_.setMinEnabled(cbMin_.isSelected());
    } else if (src == cbMax_) {
      graphe_.setMaxEnabled(cbMax_.isSelected());
    } else if (src == cbRapid_) {
      graphe_.setRapide(cbRapid_.isSelected());
    } else if (cmd.equals("GRAPHESETUP")) {
      final BDialogContent dl=
        new BDialogContent(
          (BuCommonInterface)LidoApplication.FRAME,
          this,
          "Graphe setup");
      final BGraphePersonnaliseur gp= new BGraphePersonnaliseur(graphe_.getGraphe());
      gp.addPropertyChangeListener(this);
      dl.getContentPane().add(gp);
      dl.show();
    } else if (cmd.startsWith("AVANCER")) {
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("temps.rapide", 5);
        temps_= Math.min(rsn_.pasTemps.length - 1, temps_ + avt);
      } else {
        temps_++;
      }
      graphe_.setTemps(temps_);
      edAxes_.setAxes(graphe_.getAxes());
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
      if (temps_ >= (rsn_.pasTemps.length - 1)) {
        btAvancer_.setEnabled(false);
        btAvancerVite_.setEnabled(false);
      }
      if (!btReculer_.isEnabled()) {
        btReculer_.setEnabled(true);
      }
      if (!btReculerVite_.isEnabled()) {
        btReculerVite_.setEnabled(true);
      }
    } else if (cmd.startsWith("RECULER")) {
      if (cmd.endsWith("VITE")) {
        final int avt= LidoPreferences.LIDO.getIntegerProperty("temps.rapide", 5);
        temps_= Math.max(0, temps_ - avt);
      } else {
        temps_--;
      }
      graphe_.setTemps(temps_);
      edAxes_.setAxes(graphe_.getAxes());
      lbPasTemps_.setText("Pas de temps:" + (temps_ + 1));
      lbTemps_.setText("Temps: " + rsn_.pasTemps[temps_].t);
      if (temps_ <= 0) {
        btReculer_.setEnabled(false);
        btReculerVite_.setEnabled(false);
      }
      if (!btAvancer_.isEnabled()) {
        btAvancer_.setEnabled(true);
      }
      if (!btAvancerVite_.isEnabled()) {
        btAvancerVite_.setEnabled(true);
      }
    } else if (src == edAxes_) {
      final Double[][] axes= edAxes_.getAxes();
      graphe_.setAxes(axes[0], axes[1]);
      edAxes_.setAxes(graphe_.getAxes());
    }
  }
  public int getNumberOfPages() {
    if (graphe_ == null) {
      return 0;
    }
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _page) {
    if (graphe_ == null) {
      return Printable.NO_SUCH_PAGE;
    }
    graphe_.setName(getTitle());
    return graphe_.print(_g, _format, _page);
  }
  private void updateVisu() {
    if (cbMin_ != null) {
      cbMin_.setSelected(graphe_.isMinEnabled());
    }
    if (cbMax_ != null) {
      cbMax_.setSelected(graphe_.isMaxEnabled());
    }
  }
  class ContraintesPanel extends BuPanel implements ActionListener {
    //    private Object item_;
    private JComboBox cmContr;
    private BuTextField tfContr;
    private BuButton btSuppr, btAjou;
    public ContraintesPanel() {
      cmContr= new JComboBox();
      tfContr= BuTextField.createDoubleField();
      tfContr.setColumns(8);
      cmContr.setEditable(false);
      final double[] c= graphe_.getContraintes();
      for (int i= 0; i < c.length; i++) {
        cmContr.addItem(new Double(c[i]));
      }
      //cmContr.addItemListener(this);
      //tfContr.addActionListener(this);
      final BuPanel pnBt= new BuPanel();
      pnBt.setLayout(new BuHorizontalLayout(0, true, false));
      btAjou= new BuButton("Cr�e");
      btAjou.setIcon(BuResource.BU.getIcon("creer"));
      btAjou.addActionListener(this);
      pnBt.add(btAjou, 0);
      btSuppr= new BuButton("Suppr");
      btSuppr.setIcon(BuResource.BU.getIcon("detruire"));
      btSuppr.addActionListener(this);
      pnBt.add(btSuppr, 1);
      this.setLayout(new BuVerticalLayout(5, true, false));
      this.add(new BuLabel("Contraintes"), 0);
      this.add(tfContr, 1);
      this.add(cmContr, 2);
      this.add(pnBt, 3);
    }
    public void actionPerformed(final ActionEvent e) {
      final Object src= e.getSource();
      if (src == btAjou) {
        final Double val= (Double)tfContr.getValue();
        if (val == null) {
          return;
        }
        cmContr.addItem(val);
        //        item_=val;
      }
      //      if( val==null ) cmContr.removeItem(item_);
      else if (src == btSuppr) {
        final int ind= cmContr.getSelectedIndex();
        if (ind >= 0) {
          cmContr.removeItemAt(ind);
        }
        tfContr.setValue(null);
      }
      final double[] co= new double[cmContr.getItemCount()];
      int ic= 0;
      for (ic= 0; ic < co.length; ic++) {
        co[ic]= ((Double)cmContr.getItemAt(ic)).doubleValue();
      }
      graphe_.setContraintes(co);
    }
    /*
        public void itemStateChanged(ItemEvent e)
        {
          item_=e.getItem();
          tfContr.setValue(item_);
        }
    */
  }
}
