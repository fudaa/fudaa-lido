/*
 * @file         Seuil.java
 * @creation     2000-10-23
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * L'application cliente Seuil.
 *
 * @version      $Revision: 1.9 $ $Date: 2007-01-19 13:14:36 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class Seuil {
  /**
   * @param args les arguments
   */
  public static void main(final String[] args) {
    final Fudaa f=new Fudaa();
    f.launch(args,SeuilImplementation.informationsSoftware(),false);
    f.startApp(new SeuilImplementation());  }
}
