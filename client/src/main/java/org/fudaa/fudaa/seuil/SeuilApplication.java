/*
 * @file         SeuilApplication.java
 * @creation     1998-10-02
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
//import org.fudaa.ebli.bu.BuApplication;
import com.memoire.bu.BuApplication;
/**
 * L'application cliente Seuil.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class SeuilApplication extends BuApplication {
  public static java.awt.Frame FRAME= null;
  public SeuilApplication() {
    super();
    FRAME= this;
    final SeuilImplementation app= new SeuilImplementation();
    setImplementation(app);
  }
}
