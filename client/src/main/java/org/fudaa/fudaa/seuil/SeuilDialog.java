/*
 * @file         SeuilDialog.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuVerticalLayout;
/**
 * Copie modifiee de BuDialog
 * avec suppression de la notion de message,  du panneau
 * et du cadre dans lequel on ecrivait le message
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:10:26 $ by $Author: deniger $
 * @author       Riou Jean-Yves
 */
public abstract class SeuilDialog extends JDialog implements ActionListener {
  protected JComponent content_;
  protected BuBorderLayout layout_;
  protected int reponse_;
  protected Object message_;
  public SeuilDialog(
    final BuCommonInterface _parent,
    final BuInformationsSoftware _isoft,
    final String _titre) {
    super(_parent == null ? null : _parent.getFrame());
    reponse_= JOptionPane.CANCEL_OPTION;
    final BuBorderLayout mlo= new BuBorderLayout();
    final JComponent mct= (JComponent)getContentPane();
    mct.setLayout(mlo);
    if (_isoft != null) {
      final JPanel mpi= new JPanel();
      mpi.setLayout(new BuVerticalLayout());
      mpi.setBorder(new EmptyBorder(10, 10, 10, 10));
      mpi.setBackground(mpi.getBackground().darker());
      mpi.setForeground(Color.white);
      BuPicture ml_logo= null;
      if (_isoft.logo != null) {
        ml_logo= new BuPicture(_isoft.logo);
        // setIconImage(BuResource.BU.getImage("dialog"));
      }
      final BuLabelMultiLine ml_isoft=
        new BuLabelMultiLine(
          _isoft.name
            + "\nversion: "
            + _isoft.version
            + "\ndate: "
            + _isoft.date);
      ml_isoft.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
      ml_isoft.setForeground(Color.white);
      if (ml_logo != null) {
        mpi.add(ml_logo);
      }
      mpi.add(ml_isoft);
      mct.add(mpi, BuBorderLayout.WEST);
    }
    layout_= new BuBorderLayout();
    layout_.setHgap(5);
    layout_.setVgap(5);
    content_= new JPanel();
    content_.setBorder(new EmptyBorder(10, 10, 10, 10));
    content_.setLayout(layout_);
    mct.add(content_, BuBorderLayout.CENTER);
    // if(lbMessage.isFocusTraversable())
    //   lbMessage.requestFocus();
    // else
    transferFocus();
    setResizable(false);
    setTitle(_titre);
    setModal(true);
  }
  public JComponent getComponent() {
    return null;
  }
  public abstract void actionPerformed(ActionEvent _evt);
  public final int activate() {
    final Dimension d= getPreferredSize();
    final Dimension ecran= Toolkit.getDefaultToolkit().getScreenSize();
    d.width += 30;
    d.height += 30;
    if (!(message_ instanceof JComponent)) {
      if (d.width < 450) {
        d.width= 450;
      }
      if (d.width > 550) {
        d.width= 550;
      }
      if (d.height < 150) {
        d.height= 150;
      }
      if (d.height > 250) {
        d.height= 250;
      }
    }
    if (d.width > ecran.width * 3 / 4) {
      d.width= ecran.width * 3 / 4;
    }
    if (d.height > ecran.height * 3 / 4) {
      d.height= ecran.height * 3 / 4;
    }
    final boolean resizable= isResizable();
    setResizable(true);
    setSize(d.width, d.height);
    setLocation((ecran.width - d.width) / 2, (ecran.height - d.height) / 2);
    setResizable(resizable);
    // SWING 1.03: LightweightDispatcher.retargetMouseEvent
    // try { setVisible(true); }
    // catch(NullPointerException ex) { }
    show();
    return reponse_;
  }
  public void show() {
    BuLib.computeMnemonics(getRootPane(), this);
    super.show();
  }
}
