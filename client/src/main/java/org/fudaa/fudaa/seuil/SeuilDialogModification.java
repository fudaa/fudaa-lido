/*
 * @file         SeuilDialogModification.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
/**
 * Distributed under license GPL2
 * (c)1999-2000 Guillaume Desnoix
 * A standard confirm dialog (valider/modifier).
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:10:26 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class SeuilDialogModification// extends BuDialog
extends SeuilDialog implements ActionListener {
  protected BuButton btOui_;
  protected BuButton btNon_;
  BuCommonInterface parent_;
  public SeuilDialogModification(
    final BuCommonInterface _parent,
    final BuInformationsSoftware _isoft,
    final String _message1,
    final String _message2,
    final String _message3,
    final String _message4) {
    super(_parent, _isoft, BuResource.BU.getString("Validation/Modification"));
    parent_= _parent;
    final String message1_= _message1;
    final String message2_= _message2;
    final String message3_= _message3;
    final String message4_= _message4;
    final BuPanel pnb= new BuPanel();
    pnb.setLayout(new BuBorderLayout());
    final JLabel messag1_= new JLabel(message1_, SwingConstants.CENTER);
    final JLabel messag2_= new JLabel(message2_, SwingConstants.CENTER);
    final JLabel messag3_= new JLabel(message3_, SwingConstants.CENTER);
    final JLabel messag4_= new JLabel(message4_, SwingConstants.CENTER);
    btOui_= new BuButton("Valider");
    btOui_.addActionListener(this);
    final JPanel pnBt1_= new JPanel();
    pnBt1_.setLayout(new FlowLayout());
    pnBt1_.add("Center", btOui_);
    final JPanel pnMes1_= new JPanel();
    pnMes1_.setLayout(new BorderLayout());
    pnMes1_.add("North", messag1_);
    pnMes1_.add("Center", messag2_);
    pnMes1_.add("South", pnBt1_);
    btNon_= new BuButton("Modifier");
    btNon_.addActionListener(this);
    final JPanel pnBt2_= new JPanel();
    pnBt2_.setLayout(new FlowLayout());
    pnBt2_.add("Center", btNon_);
    final JPanel pnMes2_= new JPanel();
    pnMes2_.setLayout(new BorderLayout());
    pnMes2_.add("North", messag3_);
    pnMes2_.add("Center", messag4_);
    pnMes2_.add("South", pnBt2_);
    //getRootPane().setDefaultButton(btNon_);
    pnb.add("North", pnMes1_);
    pnb.add("South", pnMes2_);
    content_.add(pnb, BuBorderLayout.SOUTH);
  }
  public void actionPerformed(final ActionEvent _evt) {
    final JComponent source= (JComponent)_evt.getSource();
    // System.err.println("BuDialog : "+source);
    if (source == btOui_) {
      reponse_= JOptionPane.YES_OPTION;
      setVisible(false);
    }
    if (source == btNon_) {
      reponse_= JOptionPane.NO_OPTION;
      setVisible(false);
    }
  }
}
