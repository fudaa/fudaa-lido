/*
 * @file         SeuilExport.java
 * @creation     2000-04-18
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

import com.memoire.bu.BuInformationsSoftware;

import org.fudaa.dodico.corba.seuil.IResultatsSeuil;
/**
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class SeuilExport {
  private static NumberFormat nf;
  static {
    nf= NumberFormat.getInstance();
    nf.setGroupingUsed(false);
    nf.setMaximumFractionDigits(2);
    nf.setMinimumIntegerDigits(2);
  }
  private static NumberFormat nfd;
  static {
    nfd= NumberFormat.getInstance(Locale.US);
    nfd.setGroupingUsed(false);
    nfd.setMinimumIntegerDigits(2);
  }
  public static void exportResultats(
    final IResultatsSeuil _result,
    final BuInformationsSoftware isSeuil_,
    final String filename)
    throws IOException {
    String dateCreation;
    Calendar cal;
    IResultatsSeuil res_;
    res_= _result;
    cal= Calendar.getInstance();
    dateCreation=
      nfd.format(cal.get(Calendar.DAY_OF_MONTH))
        + "/"
        + nfd.format(cal.get(Calendar.MONTH) + 1)
        + "/"
        + nfd.format(cal.get(Calendar.YEAR))
        + ", "
        + nfd.format(cal.get(Calendar.HOUR_OF_DAY))
        + ":"
        + nfd.format(cal.get(Calendar.MINUTE));
    System.out.println("date " + dateCreation);
    System.err.println("export   dans " + filename);
    final PrintWriter fic= new PrintWriter(new FileOutputStream(filename));
    fic.println(
      isSeuil_.name + isSeuil_.version + "    " + isSeuil_.rights + "  \n");
    fic.println(
      "========================================================================"
        + "  \n");
    fic.println(
      "  R�sultats du calcul de seuil pour l'�tude : "
        + res_.resultatsSEU().titreEtude
        + "  \n");
    fic.println("  Le  : " + dateCreation + "  \n");
    fic.println(
      "========================================================================"
        + "  \n");
    fic.println(
      "           Seuil � crete        :  "
        + res_.resultatsSEU().typeCretCal
        + "\n");
    fic.println(
      "           Type d'�coulement    :  "
        + res_.resultatsSEU().regime
        + "\n");
    fic.println(
      "           Hauteur critique     :  "
        + nf.format(res_.resultatsSEU().hc)
        + "   (m)"
        + "\n");
    fic.println(
      "           D�bit			     :  "
        + nf.format(res_.resultatsSEU().debitCal)
        + "   (m3/s)"
        + "\n");
    fic.println(
      "           Charge  amont        :  "
        + nf.format(res_.resultatsSEU().chargAmont)
        + "   (m)"
        + "\n");
    fic.println(
      "           Hauteur amont        :  "
        + nf.format(res_.resultatsSEU().hautAmont)
        + "   (m)"
        + "\n");
    fic.println(
      "           Cote    amont        :  "
        + nf.format(res_.resultatsSEU().cotAmont)
        + "   (m)"
        + "\n");
    fic.println(
      "           Charge  aval         :  "
        + nf.format(res_.resultatsSEU().chargAval)
        + "   (m)"
        + "\n");
    fic.println(
      "           Hauteur aval         :  "
        + nf.format(res_.resultatsSEU().hautAval)
        + "   (m)"
        + "\n");
    fic.println(
      "           Cote    aval         :  "
        + nf.format(res_.resultatsSEU().cotAval)
        + "   (m)"
        + "\n");
    fic.println(
      "           Diff�rence de charge :  "
        + nf.format(res_.resultatsSEU().deltaCharge)
        + "   (m)"
        + "\n");
    fic.println(
      "           Diff�rence de cotes  :  "
        + nf.format(res_.resultatsSEU().deltaCote)
        + "   (m)"
        + "\n");
    fic.println(
      "           Coeficient de d�bit  :  "
        + nf.format(res_.resultatsSEU().coefSeuil)
        + "\n");
    fic.close();
  }
}
