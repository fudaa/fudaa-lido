/*
 * @file         SeuilFilleParametres.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.seuil.SParametresSEU;

import org.fudaa.ebli.graphe.BGraphe;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
import org.fudaa.fudaa.lido.editor.LidoProfilEditor;
/**
 * Une fenetre fille pour entrer les parametres.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:10:26 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class SeuilFilleParametres
  extends BuInternalFrame
  implements
    FudaaProjetListener,
    FudaaParamListener,
    ActionListener,
    ChangeListener,
    PropertyChangeListener,
    BuPrintable,
    FocusListener {
  //===================================================
  //    Variables
  //===================================================
  // pour editeur de profil
  //SParametresPRO pro_;
  //SParametresBiefBlocPRO profil_;
  //=========================================
  int VALIDER= 0; // pour lancement calcul
  int MODIFIER= 1;
  int VUEPARTRAV= 1; // pour le dessin
  int nptmax= 80; // nbre max points d'un profil
  int mode;
  int mode1;
  int mode2;
  public static final int EDITER= 0x01;
  public static final int VOIR= 0x02;
  public final static int PROFILS= 1;
  public final static int CALAGE= 2;
  public final static int AMONT= 1;
  public final static int AVAL= 2;
  //  general
  FudaaProjet project_;
  BuCommonInterface appli_;
  SeuilImplementation imp_;
  //   panneau principal
  JComponent content_;
  // boite a onglets
  JTabbedPane tpMain;
  int indexPanVu; // index onglet vu (0/1/2)
  int indexPanAnc; // index " ancien
  BuTextField tf_nom_etude; //prevu
  // dessin du profil en travers
  BGraphe pngraphe;
  //panneaux 1 et 2 de la boite a onglets
  JPanel pnGeoGraph= new JPanel();
  JPanel pnHydGraph= new JPanel();
  //profil de l' editeur de profil
  SParametresBiefBlocPRO profilsBief_;
  int editProf_;
  //hauteur critique
  double hautCrit= 0.;
  JLabel tf_HautCrit; // pour un panneau
  JLabel tf_HautCrit1; // pour l'autre
  String chain= " "; // pour affichage hauteur critique
  // profils
  BuTextField tf_nomProfAm;
  BuTextField tf_nbPointsAm;
  BuTextField tf_nomProfAv;
  BuTextField tf_nbPointsAv;
  BuTextField tf_profAmontX[]= new BuTextField[nptmax];
  BuTextField tf_profAmontY[]= new BuTextField[nptmax];
  BuTextField tf_profAvalX[]= new BuTextField[nptmax];
  BuTextField tf_profAvalY[]= new BuTextField[nptmax];
  // parametres geometriques
  BuTextField tf_pelle;
  BuTextField tf_epaisCret;
  BuTextField tf_inclinaison;
  BuTextField tf_oblicite;
  BuTextField tf_largEcoul;
  BuTextField tf_rayonCourb;
  // caracteristiques hydrauliques
  BuTextField tf_debit;
  BuTextField tf_coteAmont;
  BuTextField tf_coteAval;
  // boutons
  JButton btFermer_, btCalculer_; //btAnnuler_;
  JButton btEffacerAm_, btEditerAm_;
  JButton btEffacerAv_, btEditerAv_;
  //  caracteristique du seuil
  String[] sLibTypeSeuil= { "mince", "epais", "inconnu" };
  String[] sLibTypeIncli= { "Droit", "Incline", "inconnu" };
  String[] sLibTypeObli= { "Droit", "Oblique", "inconnu" };
  String[] sLibTypeNoy= { "Noye", "Denoye", "inconnu" };
  //
  String sTypeSeuil= "";
  String sTypeIncli= "";
  String sTypeObli= "";
  String sTypeNoy= "";
  //
  String messagew1= " en crete mince";
  String messagew2= " en crete �paisse";
  String messagew3= " pour passer en crete �paisse, changez : ";
  String messagew4= " pour passer en crete mince , changez : ";
  String mesenvoie1= " ";
  String mesenvoie2= " ";
  //==========================================================
  //   Constructeur
  //==========================================================
  public SeuilFilleParametres(
    final BuCommonInterface _appli,
    final FudaaProjet projet,
    final SeuilImplementation _imp) {
    super("", false, true, false, true);
    appli_= _appli;
    project_= projet;
    imp_= _imp;
    System.out.println("_imp =  " + _imp);
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    project_.addFudaaProjetListener(this);
    int n;
    for (int i= 0; i < nptmax; i++) {
      tf_profAmontX[i]= new BuTextField();
      tf_profAmontY[i]= new BuTextField();
      tf_profAvalX[i]= new BuTextField();
      tf_profAvalY[i]= new BuTextField();
      tf_profAmontX[i].setText("");
      tf_profAmontY[i].setText("");
      tf_profAvalX[i].setText("");
      tf_profAvalY[i].setText("");
    }
    tf_HautCrit= new JLabel("      ", SwingConstants.CENTER);
    final JPanel pnIntermed= new JPanel();
    final BuGridLayout loCali= new BuGridLayout();
    loCali.setColumns(5);
    loCali.setHgap(20); // entre champs
    loCali.setVgap(5);
    loCali.setHfilled(true);
    loCali.setCfilled(true);
    pnIntermed.setLayout(loCali);
    n= 0;
    pnIntermed.add(new JLabel("       ", SwingConstants.LEFT), n++);
    pnIntermed.add(new JLabel("             ", SwingConstants.LEFT), n++);
    pnIntermed.add(new JLabel("La hauteur critique est : ", SwingConstants.RIGHT), n++);
    pnIntermed.add(tf_HautCrit, n++);
    pnIntermed.add(new JLabel("(m) ", SwingConstants.LEFT), n++);
    final JPanel pnHautCrit= new JPanel();
    pnHautCrit.setLayout(new BorderLayout());
    pnHautCrit.setPreferredSize(new Dimension(200, 40));
    pnHautCrit.setSize(new Dimension(200, 40));
    pnHautCrit.add("South", pnIntermed);
    //===========================================================
    //     Panneau La hauteur critique est ...
    //===========================================================
    tf_HautCrit1= new JLabel("      ", SwingConstants.CENTER);
    final JPanel pnIntermed1= new JPanel();
    final BuGridLayout loCali1= new BuGridLayout();
    loCali1.setColumns(5);
    loCali1.setHgap(20); // entre champs
    loCali1.setVgap(5);
    loCali1.setHfilled(true);
    loCali1.setCfilled(true);
    pnIntermed1.setLayout(loCali1);
    n= 0;
    pnIntermed1.add(new JLabel("       ", SwingConstants.LEFT), n++);
    pnIntermed1.add(new JLabel("             ", SwingConstants.LEFT), n++);
    pnIntermed1.add(
      new JLabel("La hauteur critique est : ", SwingConstants.RIGHT),
      n++);
    pnIntermed1.add(tf_HautCrit1, n++);
    pnIntermed1.add(new JLabel("(m) ", SwingConstants.LEFT), n++);
    final JPanel pnHautCrit1= new JPanel();
    pnHautCrit1.setLayout(new BorderLayout());
    pnHautCrit1.setPreferredSize(new Dimension(200, 40));
    pnHautCrit1.setSize(new Dimension(200, 40));
    pnHautCrit1.add("South", pnIntermed1);
    //=========================================================
    //  Profils amont/aval
    //==========================================================
    // amont
    tf_nomProfAm= new BuTextField();
    tf_nbPointsAm= BuTextField.createIntegerField();
    tf_nbPointsAm.addFocusListener(this);
    //tf_nbPointsAm   = BuTextField.createTextField();
    //tf_nbPointsAm   = BuTextField;
    final JPanel pnNomAm= new JPanel();
    pnNomAm.setBorder(new TitledBorder("Profil amont"));
    final BuGridLayout loCal3= new BuGridLayout();
    loCal3.setColumns(2);
    loCal3.setHgap(5);
    loCal3.setVgap(10);
    loCal3.setHfilled(true);
    loCal3.setCfilled(true);
    pnNomAm.setLayout(loCal3);
    pnNomAm.setBorder(new EmptyBorder(50, 5, 5, 5)); //top,left,bottom,right
    n= 0;
    tf_nomProfAm.setColumns(6);
    pnNomAm.add(new JLabel("             nom ", SwingConstants.RIGHT), n++);
    //n = n+1;  ne marche pas
    pnNomAm.add(tf_nomProfAm, n++);
    pnNomAm.add(new JLabel("nombre de points ", SwingConstants.RIGHT), n++);
    //tf_nbPointsAm.setColumns(7);
    pnNomAm.add(tf_nbPointsAm, n++);
    final JPanel pnEditAm= new JPanel();
    btEffacerAm_= new BuButton("Effacer");
    btEffacerAm_.addActionListener(this);
    pnEditAm.add(btEffacerAm_);
    btEditerAm_= new BuButton("Editer");
    btEditerAm_.addActionListener(this);
    pnEditAm.add(btEditerAm_);
    final JPanel pnProfAm= new JPanel();
    pnProfAm.setBorder(new TitledBorder("Profil amont"));
    pnProfAm.setLayout(new BorderLayout());
    pnProfAm.setPreferredSize(new Dimension(320, 200));
    pnProfAm.add("North", pnNomAm);
    pnProfAm.add("South", pnEditAm);
    // aval
    tf_nomProfAv= new BuTextField();
    tf_nbPointsAv= BuTextField.createIntegerField();
    tf_nbPointsAv.addFocusListener(this);
    final JPanel pnNomAv= new JPanel();
    pnNomAv.setBorder(new TitledBorder("Profil amont"));
    final BuGridLayout loCal4= new BuGridLayout();
    loCal4.setColumns(2);
    loCal4.setHgap(5);
    loCal4.setVgap(10);
    loCal4.setHfilled(true);
    loCal4.setCfilled(true);
    pnNomAv.setLayout(loCal4);
    pnNomAv.setBorder(new EmptyBorder(50, 5, 5, 5)); //top,left,bottom,right
    n= 0;
    tf_nomProfAv.setColumns(6);
    pnNomAv.add(new JLabel("            nom ", SwingConstants.RIGHT), n++);
    pnNomAv.add(tf_nomProfAv, n++);
    pnNomAv.add(new JLabel("nombre de points ", SwingConstants.RIGHT), n++);
    pnNomAv.add(tf_nbPointsAv, n++);
    final JPanel pnEditAv= new JPanel();
    btEffacerAv_= new BuButton("Effacer");
    btEffacerAv_.addActionListener(this);
    pnEditAv.add(btEffacerAv_);
    btEditerAv_= new BuButton("Editer");
    btEditerAv_.addActionListener(this);
    pnEditAv.add(btEditerAv_);
    final JPanel pnProfAv= new JPanel();
    pnProfAv.setBorder(new TitledBorder("Profil aval"));
    pnProfAv.setLayout(new BorderLayout());
    pnProfAv.setPreferredSize(new Dimension(320, 200));
    pnProfAv.add("North", pnNomAv);
    pnProfAv.add("South", pnEditAv);
    final JPanel pnProfils= new JPanel();
    pnProfils.setLayout(new BorderLayout());
    pnProfils.add("West", pnProfAm);
    pnProfils.add("East", pnProfAv);
    //===============================================================
    // Parametres Geometriques
    //=================================================================
    tf_pelle= BuTextField.createDoubleField();
    tf_epaisCret= BuTextField.createDoubleField();
    tf_inclinaison= BuTextField.createDoubleField();
    tf_oblicite= BuTextField.createDoubleField();
    tf_largEcoul= BuTextField.createDoubleField();
    tf_rayonCourb= BuTextField.createDoubleField();
    tf_pelle.addFocusListener(this);
    tf_epaisCret.addFocusListener(this);
    tf_inclinaison.addFocusListener(this);
    tf_rayonCourb.addFocusListener(this);
    tf_largEcoul.addFocusListener(this);
    tf_oblicite.addFocusListener(this);
    //=====================================
    final JPanel pnGeoParm= new JPanel();
    pnGeoParm.setBorder(new TitledBorder("Param�tres Geometriques"));
    final BuGridLayout loCal1= new BuGridLayout();
    loCal1.setColumns(3);
    loCal1.setHgap(5);
    loCal1.setVgap(10);
    loCal1.setHfilled(true);
    loCal1.setCfilled(true);
    pnGeoParm.setLayout(loCal1);
    pnGeoParm.setBorder(new EmptyBorder(50, 5, 5, 5)); //top,left,bottom,right
    n= 0;
    tf_pelle.setColumns(6);
    pnGeoParm.add(new JLabel("Pelle amont ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_pelle, n++);
    pnGeoParm.add(new JLabel("(m)     ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Epaisseur de la crete ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_epaisCret, n++);
    pnGeoParm.add(new JLabel("(m)     ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Inclinaison du seuil ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_inclinaison, n++);
    pnGeoParm.add(new JLabel("(degr�s)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Oblicit� du seuil ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_oblicite, n++);
    pnGeoParm.add(new JLabel("(degr�s)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Largeur d'�coulement ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_largEcoul, n++);
    pnGeoParm.add(new JLabel("(m)     ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Courbure du rebord ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_rayonCourb, n++);
    pnGeoParm.add(new JLabel("(m)      ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("(0 si angle droit) ", SwingConstants.RIGHT), n++);
    //================================
    pnGeoGraph.setBorder(new TitledBorder("Graphique"));
    pnGeoGraph.setLayout(new BorderLayout());
    pnGeoGraph.setPreferredSize(new Dimension(300, 300));
    //================================
    final JPanel pnGeoSeuil= new JPanel();
    pnGeoSeuil.setBorder(new TitledBorder(""));
    pnGeoSeuil.setLayout(new BorderLayout());
    pnGeoSeuil.add("West", pnGeoParm);
    pnGeoSeuil.add("East", pnGeoGraph);
    pnGeoSeuil.add("South", pnHautCrit1);
    //================================================================
    // Caracteristiques hydrauliques du seuil
    //=================================================================
    tf_debit= BuTextField.createDoubleField();
    tf_coteAmont= BuTextField.createDoubleField();
    tf_coteAval= BuTextField.createDoubleField();
    tf_debit.addFocusListener(this);
    tf_debit.setActionCommand("TATA"); // ne marche pas
    tf_debit.addActionListener(this);
    tf_coteAmont.addFocusListener(this);
    tf_coteAval.addFocusListener(this);
    //===============================================
    final JPanel pnHydParm= new JPanel();
    pnHydParm.setBorder(new TitledBorder("Param�tres Hydrauliques"));
    final BuGridLayout loCal2= new BuGridLayout();
    loCal2.setColumns(3);
    loCal2.setHgap(5);
    loCal2.setVgap(10);
    loCal2.setHfilled(true);
    loCal2.setCfilled(true);
    pnHydParm.setLayout(loCal2);
    pnHydParm.setBorder(new EmptyBorder(80, 5, 5, 5)); //top,left,bottom,right
    n= 0;
    tf_debit.setColumns(7);
    pnHydParm.add(new JLabel("D�bit estim� ", SwingConstants.RIGHT), n++);
    pnHydParm.add(tf_debit, n++);
    pnHydParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnHydParm.add(new JLabel("Cote amont estim�e ", SwingConstants.RIGHT), n++);
    pnHydParm.add(tf_coteAmont, n++);
    pnHydParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnHydParm.add(new JLabel("Cote aval ", SwingConstants.RIGHT), n++);
    pnHydParm.add(tf_coteAval, n++);
    pnHydParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    //================================================
    pnHydGraph.setBorder(new TitledBorder("Graphique"));
    pnHydGraph.setLayout(new BorderLayout());
    pnHydGraph.setPreferredSize(new Dimension(300, 300));
    //==================================================
    final JPanel pnHydrau= new JPanel();
    pnHydrau.setBorder(new TitledBorder(""));
    pnHydrau.setLayout(new BorderLayout());
    pnHydrau.add("West", pnHydParm);
    pnHydrau.add("East", pnHydGraph);
    pnHydrau.add("South", pnHautCrit);
    //==================================================================
    //   Panneau Fermer/Calculer
    //==================================================================
    final JPanel pnValider= new JPanel();
    btFermer_= new BuButton("Fermer");
    btFermer_.addActionListener(this);
    pnValider.add(btFermer_);
    btCalculer_= new BuButton("Calculer");
    btCalculer_.addActionListener(this);
    pnValider.add(btCalculer_);
    //btAnnuler_=new BuButton("Annuler");
    //btAnnuler_.addActionListener(this);
    //pnValider.add(btAnnuler_);
    //===================================================================
    //   Boite � onglets
    //===================================================================
    //System.err.println("****" +g);
    //JTabbedPane tpMain=new JTabbedPane();
    tpMain= new JTabbedPane();
    tpMain.addFocusListener(this);
    // tpMain.setPreferredSize(new Dimension(650,400));
    final JPanel cd01= new JPanel();
    tpMain.addChangeListener(this);
    cd01.setLayout(new BorderLayout());
    cd01.add("North", pnProfils);
    //cd01.add("West", pnProfAm);
    //cd01.add("East", pnProfAv);
    tpMain.addTab(" Profils amont/aval    ", null, cd01, "Profils amont/aval");
    final JPanel cd02= new JPanel();
    cd02.setLayout(new BorderLayout());
    cd02.add("North", pnHydrau);
    tpMain.addTab(
      "Param�tres Hydrauliques ",
      null,
      cd02,
      "Param�tres hydrauliques");
    final JPanel cd03= new JPanel();
    cd03.setLayout(new BorderLayout());
    cd03.add("North", pnGeoSeuil);
    tpMain.addTab(
      " G�om�trie du seuil     ",
      null,
      cd03,
      "Caract�ristiques g�om�triques du seuil");
    cd01.addFocusListener(this);
    cd02.addFocusListener(this);
    cd03.addFocusListener(this);
    content_= (JComponent)getContentPane();
    content_.setLayout(new BorderLayout());
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add(BorderLayout.CENTER, tpMain);
    content_.add(BorderLayout.SOUTH, pnValider);
    setTitle("Param�tres de calcul");
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setLocation(80, 60);
    indexPanVu= 0;
    indexPanAnc= 0;
    tpMain.setEnabledAt(0, true);
    //inidoncalc ();     // pour tests
    setValeurs();
    updatePanels();
    pack();
  } // fin constructeur
  //==============================================================
  // Methodes publiques
  //==============================================================
  public void setProjet(final FudaaProjet project) {
    System.out.println("action : setProjet   ");
    project_= project;
    updatePanels();
  }
  public void print(final PrintJob _job, final Graphics _g) {
    BuPrinter.INFO_DOC= new BuInformationsDocument();
    BuPrinter.INFO_DOC.name= getTitle();
    BuPrinter.INFO_DOC.logo= null; //BuResource.BU.getIcon("tableau",24);
    BuPrinter.printComponent(_job, _g, content_);
  }
  //=======================================
  //pour detecter changement d'onglet
  //=======================================
  public void stateChanged(final ChangeEvent e) {
    final Object src= e.getSource();
    if (src instanceof JTabbedPane) {
      indexPanVu= tpMain.getSelectedIndex();
      //System.out.println(" updatePanels indice tpMain = "+indexPanVu);
      if (indexPanVu != indexPanAnc) {
        updatePanels();
      }
    }
  }
  //=================================================
  // pour detecter valider de l'editeur de profil
  //=================================================
  public void propertyChange(final PropertyChangeEvent evt) {
    System.out.println("filleparam  PropertyChange  = ");
    // Object src=evt.getSource();
    // String chainw = evt.getPropertyName();
    // System.out.println("getPropertyName  = "+chainw);
    if (evt.getPropertyName().equals("fermer")) {
      recupProfil();
    }
  }
  public void focusGained(final FocusEvent e) {
    //  ne sert plus a rien pour tpMain
    //  et au retour editer le champ ne prend pas le focus
    //  Object src=e.getSource();
    //   System.out.println("focusGained  " );
    //  if( src instanceof  JTabbedPane )
    //  if( src instanceof  JButton )
    //    {
    // System.out.println("focusGained  " );
    //        //  System.out.println("focusGained source " + src);
    //		   updatePanels();
    //    }
  }
  //===================================
  // pour sortie des textField
  //===================================
  public void focusLost(final FocusEvent e) {
    final Object src= e.getSource();
    System.out.println("filleparam focusLost ");
    if (src instanceof JTextField) {
      //System.out.println("focusLost source " + src);
      updatePanels();
    }
  }
  public void actionListener(final ActionEvent e) {
    final Object src= e.getSource();
    System.out.println("filleparam src actionListener = " + src);
    if (e.getActionCommand().equals("TATA")) {}
  }
  public void actionPerformed(final ActionEvent e) {
    final Object src= e.getSource();
    // System.out.println("src actionPerformed = " + src);
    if (src == btFermer_) {
      fermer();
    } else if (src == btCalculer_) {
      calculer();
    } else if (src == btEditerAm_) {
      editerAm();
    } else if (src == btEditerAv_) {
      editerAv();
    } else if (src == btEffacerAm_) {
      effacerAm();
    } else if (src == btEffacerAv_) {
      effacerAv();
    } else {
      try {
        setClosed(true);
        setSelected(false);
      } catch (final PropertyVetoException ex) {}
    }
  }
  public void valider() // appelee par SeuilImplementation
  { // mais la validation est fait sur
    // bouton calculer devient visible
    System.out.println("filleparam valider() ");
    try {} catch (final IllegalArgumentException e) {
      return;
    }
    try {
      setClosed(true);
      setSelected(false);
    } catch (final PropertyVetoException e) {}
  }
  public void fermer() {
    System.out.println("filleparam fermer() ");
    try {
      getValeurs();
    } catch (final IllegalArgumentException e) {
      return;
    }
    try {
      setClosed(true);
      setSelected(false);
    } catch (final PropertyVetoException e) {}
  }
  public void calculer() {
    System.out.println("filleparam calculer() ");
    determineCas();
    if (sTypeSeuil.equals(sLibTypeSeuil[0])) //mince
      {
      mesenvoie1= messagew1;
      mesenvoie2= messagew3;
    }
    if (sTypeSeuil.equals(sLibTypeSeuil[1])) //epais
      {
      mesenvoie1= messagew2;
      mesenvoie2= messagew4;
    }
    final int res=
      new SeuilDialogModification(
        imp_.getApp(),
        SeuilImplementation.isSeuil_,
        "Le calcul du coefficient de d�bit va s'effectuer",
        mesenvoie1,
        mesenvoie2,
        "le d�bit,la largeur d'�coulement ou l'�paisseur du seuil")
        .activate();
    //  System.out.println("res = " + res);
    if (res == 0) {
      getValeurs();
      // inidoncalc ();     // pour test
      imp_.calculer(); // => SeuilImplementation
    }
  }
  //======================================================
  public void delete() // a conserver sinon on ne peut sortir de seuil
  {
    System.out.println("filleparam delete ");
    FudaaParamEventProxy.FUDAA_PARAM.removeFudaaParamListener(this);
    if (project_ != null) {
      project_.removeFudaaProjetListener(this);
    }
    project_= null;
  }
  //
  // LidoParamListener
  //
  public void paramStructCreated(final FudaaParamEvent e) {
    System.out.println("filleparam paramsStrucCreated ");
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    System.out.println("filleparam paramsStrucDeleted ");
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  public void paramStructModified(final FudaaParamEvent e) {
    //	Object src=e.getSource();
    //   System.out.println("paramsStrucModified "+ src);
    //   System.out.println("paramsStrucModified "+ e);
    // les updatesb sont geres a la sortie des champs
    // par focus lost
    // on supprime ligne suivante sinon 2 update
    //  if( e.getSource()!=this ) updatePanels();            // ici
    // si on supprime le update ca ne marche plus
  }
  //
  // fudaaprojet listener
  //
  public void dataChanged(final FudaaProjetEvent e) {
    System.out.println("filleparam dataChanged ");
    //   System.out.println("dataChanged "+ e.getSource());
    switch (e.getID()) {
      case FudaaProjetEvent.PARAM_ADDED :
      case FudaaProjetEvent.PARAM_IMPORTED :
        {
          if (e.getSource() != this) {
            updatePanels();
          }
          break;
        }
    }
  }
  public void statusChanged(final FudaaProjetEvent e) {
    System.out.println("filleparam statusChanged");
    switch (e.getID()) {
      case FudaaProjetEvent.PROJECT_OPENED :
      case FudaaProjetEvent.PROJECT_CLOSED :
        {
          if (e.getSource() != this) {
            updatePanels();
          }
          break;
        }
    }
  }
  //=====================================================
  //   Methodes privees
  //=====================================================
  private void determineCas() // calcul hauteur critique
  {
    final SParametresSEU parm_=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    double inclinaison= parm_.inclinaison;
    //      double  oblicite       = parm_.oblicite;
    double raycourb= parm_.rayonCourb;
    //     String  typeCretCal    = parm_.typeCret;
    double pelle= parm_.pelle;
    double epais= parm_.epaisCret;
    double larg= parm_.largEcoul;
    double cotAmont= parm_.coteAmont;
    double cotAval= parm_.coteAval;
    double debit= parm_.debit;
    double refam; // zref amont <= profil
    double refav; //   "  aval   "   "
    //
    double hautLim= 0.;
    double cotePelle= 0.;
    double hcomp= 0.;
    double varw= 0.;
    double w= 0.;
    //
    // determination des zref
    //
    refam= parm_.profAmont.xy[0].y;
    for (int i= 0; i < parm_.profAmont.nbPoints; i++) {
      if (parm_.profAmont.xy[i].y < refam) {
        refam= parm_.profAmont.xy[i].y;
      }
    }
    refav= parm_.profAval.xy[0].y;
    for (int i= 0; i < parm_.profAval.nbPoints; i++) {
      if (parm_.profAval.xy[i].y < refav) {
        refav= parm_.profAval.xy[i].y;
      }
    }
    // System.out.println(" refam  = " +  refam);
    // System.out.println(" refav  = " +  refav);
    // System.out.println(" larg   = " +  larg);
    // System.out.println(" debit  = " +  debit);
    //=========================================================
    //    attribution de valeurs quand inexistance
    //========================================================
    if (debit == -999.) {
      debit= 1.;
    }
    if (inclinaison == -999.) {
      inclinaison= 0.;
    }
    if (pelle == -999.) {
      pelle= 0.8;
    }
    if (epais == -999.) {
      epais= 0.2;
    }
    if (cotAmont == -999.) {
      cotAmont= refam + 2. * pelle;
    }
    if (cotAval == -999.) {
      cotAval= refav + 0.5 * pelle;
    }
    if (raycourb == -999.) {
      raycourb= 0.;
    }
    if (larg == -999.) {
      larg= 10.;
    }
    //
    // calcul hauteur critique
    //
    varw= 9.81 * (Math.pow(larg, 2.));
    w= Math.pow(debit, 2.) / varw;
    hautCrit= Math.pow(w, (1. / 3.));
    //
    //discussion mince/epais
    //
    cotePelle= pelle + refam;
    hcomp= cotAmont - cotePelle;
    hautLim= 2. * epais;
    if ((hcomp <= hautLim) && (1.3 * hautCrit <= hautLim)) {
      sTypeSeuil= sLibTypeSeuil[1];
      parm_.typeCret= sLibTypeSeuil[1];
    } else {
      sTypeSeuil= sLibTypeSeuil[0];
      parm_.typeCret= sLibTypeSeuil[0];
    }
  }
  private void affichHautCrit() {
    determineCas(); // calcul hauteur critique
    final String tmp= String.valueOf(hautCrit).substring(0, 5);
    tf_HautCrit.setText(tmp);
    tf_HautCrit1.setText(tmp);
  }
  private void dessinValeurs() {
    if (indexPanVu == 2) {
      pnGeoGraph.removeAll(); //pour effacer precedent
      pngraphe= new SeuilGraphe(project_, null, VUEPARTRAV, hautCrit);
      pnGeoGraph.add("Center", pngraphe);
    }
    if (indexPanVu == 1) {
      pnHydGraph.removeAll();
      pngraphe= new SeuilGraphe(project_, null, VUEPARTRAV, hautCrit);
      pnHydGraph.add("Center", pngraphe);
    }
    show();
  }
  private void updatePanels() {
    getValeurs();
    //System.err.println("SeuilFilleParametres: update");
    final SParametresSEU parm_=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    //===========================================================
    // verification des donnees pour activation bouton calcul
    // affichage hautcrit et dessin eventuel
    //===========================================================
    int contcalc= 0; // pour bouton calcul
    int conthcrit= 0; // pour hauteur critique
    int contdes= 0; // pour dessin
    if (parm_.inclinaison == -999.) {
      contcalc++;
    }
    if (parm_.oblicite == -999.) {
      contcalc++;
    }
    if (parm_.rayonCourb == -999.) {
      contcalc++;
    }
    if (parm_.pelle == -999) {
      contcalc++;
    }
    if (parm_.epaisCret == -999.) {
      contcalc++;
    }
    if (parm_.largEcoul == -999.) {
      contcalc++;
      conthcrit++;
    }
    if (parm_.coteAmont == -999.) {
      contcalc++;
      contdes++;
    }
    if (parm_.coteAval == -999.) {
      contcalc++;
      contdes++;
    }
    if (parm_.debit == -999.) {
      contcalc++;
      conthcrit++;
    }
    if (parm_.profAmont.nbPoints < 4) {
      contcalc++;
      contdes++;
    }
    if (parm_.profAval.nbPoints < 4) {
      contcalc++;
      contdes++;
    }
    //System.out.println("parm_.profAmont.nbPoints = "+parm_.profAmont.nbPoints);
    //System.out.println("parm_.profAval.nbPoints = " +parm_.profAval.nbPoints);
    //System.out.println("       contcalc  = " + contcalc);
    //System.out.println("       conthcrit = " + conthcrit);
    //System.out.println("       contdes   = " + contdes);
    //=======================
    //  bouton calculer actif
    //=======================
    if (contcalc > 0) {
      btCalculer_.setEnabled(false);
    } else {
      btCalculer_.setEnabled(true);
      btCalculer_.revalidate();
    }
    //========================================
    //   determination des onglets affichables
    //========================================
    tpMain.setEnabledAt(2, false);
    if (parm_.profAmont.nbPoints > 3 && parm_.profAval.nbPoints > 3) {
      tpMain.setEnabledAt(1, true);
      if (parm_.coteAmont != -999.
        && parm_.coteAval != -999.
        && parm_.debit != -999.) {
        tpMain.setEnabledAt(2, true);
      } else {
        tpMain.setEnabledAt(2, false);
      }
    } else {
      tpMain.setEnabledAt(1, false);
      tpMain.setEnabledAt(2, false);
    }
    //=============================
    //   affichage hauteur critique
    //=============================
    if (conthcrit == 0) {
      affichHautCrit();
    }
    //============================
    //    dessin
    //===========================
    if (contdes == 0) {
      dessinValeurs();
    }
    indexPanAnc= indexPanVu;
  }
  private void setValeurs() {
    // System.out.println("action = setValeurs");
    final SParametresSEU _params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    //System.out.println("action setValeurs params = " + _params);
    //    String blanc="";
    if (_params != null) {
      // profils
      tf_nomProfAm.setValue(_params.profAmont.nomProfil);
      tf_nomProfAv.setValue(_params.profAval.nomProfil);
      //tf_nbPointsAm .setValue(new Integer(_params.profAmont.nbPoints));
      // System.out.println(" profAmont.nbPoints = " + _params.profAmont.nbPoints);
      if (_params.profAmont.nbPoints > 0) {
        tf_nbPointsAm.setValue(new Integer(_params.profAmont.nbPoints));
      } else {
        tf_nbPointsAm.setValue("");
      }
      // System.out.println(" tf_nbPointsAm = " + tf_nbPointsAm );
      //
      //tf_nbPointsAv  			 .setValue(new Integer(_params.profAval.nbPoints));
      if (_params.profAval.nbPoints > 0) {
        tf_nbPointsAv.setValue(new Integer(_params.profAval.nbPoints));
      } else {
        tf_nbPointsAv.setValue("");
      }
      // coordonnees des profils (
      //for(int i=0; i<_params.profAmont.nbPoints; i++)
      for (int i= 0; i < nptmax; i++) {
        //tf_profAmontX[i]          .setValue(new Double(_params.profAmont.xy[i].x));
        //tf_profAmontY[i]          .setValue(new Double(_params.profAmont.xy[i].x));
        //System.out.println("setValeurs tf_profAmontX[i] = "+i+" " + tf_profAmontX[i]);
        if (_params.profAmont.xy[i].x >= 0) {
          tf_profAmontX[i].setValue(new Double(_params.profAmont.xy[i].x));
        } else {
          tf_profAmontX[i].setValue("");
        }
        // System.out.println("setValeurs tf_profAmontX[i] = "+i+" " + tf_profAmontX[i].getText());
        if (_params.profAmont.xy[i].y >= 0) {
          tf_profAmontY[i].setValue(new Double(_params.profAmont.xy[i].y));
        } else {
          tf_profAmontY[i].setValue("");
        }
      }
      //for(int i=0; i<_params.profAval.nbPoints; i++)
      for (int i= 0; i < nptmax; i++) {
        //tf_profAvalX[i]          .setValue(new Double(_params.profAval.xy[i].x));
        //tf_profAvalY[i]          .setValue(new Double(_params.profAval.xy[i].x));
        if (_params.profAval.xy[i].x >= 0) {
          tf_profAvalX[i].setValue(new Double(_params.profAval.xy[i].x));
        } else {
          tf_profAvalX[i].setValue("");
        }
        if (_params.profAval.xy[i].y >= 0) {
          tf_profAvalY[i].setValue(new Double(_params.profAval.xy[i].y));
        } else {
          tf_profAvalY[i].setValue("");
        }
      }
      // parametres hydrauliques
      //tf_pelle          		 .setValue(new Double(_params.pelle));
      if (_params.pelle >= 0) {
        tf_pelle.setValue(new Double(_params.pelle));
      } else {
        tf_pelle.setValue("");
      }
      //tf_epaisCret              .setValue(new Double(_params.epaisCret));
      if (_params.epaisCret >= 0) {
        tf_epaisCret.setValue(new Double(_params.epaisCret));
      } else {
        tf_epaisCret.setValue("");
      }
      //tf_inclinaison            .setValue(new Double(_params.inclinaison));
      if (_params.inclinaison >= 0) {
        tf_inclinaison.setValue(new Double(_params.inclinaison));
      } else {
        tf_inclinaison.setValue("");
      }
      //tf_oblicite               .setValue(new Double(_params.oblicite));
      if (_params.oblicite >= 0) {
        tf_oblicite.setValue(new Double(_params.oblicite));
      } else {
        tf_oblicite.setValue("");
      }
      //tf_largEcoul              .setValue(new Double(_params.largEcoul));
      if (_params.largEcoul >= 0) {
        tf_largEcoul.setValue(new Double(_params.largEcoul));
      } else {
        tf_largEcoul.setValue("");
      }
      //tf_rayonCourb             .setValue(new Double(_params.rayonCourb ));
      if (_params.rayonCourb >= 0) {
        tf_rayonCourb.setValue(new Double(_params.rayonCourb));
      } else {
        tf_rayonCourb.setValue("");
      }
      // caracteristiques du seuil
      // tf_debit                   .setValue(new Double(_params.debit));
      if (_params.debit >= 0) {
        tf_debit.setValue(new Double(_params.debit));
      } else {
        tf_debit.setValue("");
      }
      // tf_coteAmont               .setValue(new Double(_params.coteAmont));
      if (_params.coteAmont >= 0) {
        tf_coteAmont.setValue(new Double(_params.coteAmont));
      } else {
        tf_coteAmont.setValue("");
      }
      // tf_coteAval                .setValue(new Double(_params.coteAval));
      if (_params.coteAval >= 0) {
        tf_coteAval.setValue(new Double(_params.coteAval));
      } else {
        tf_coteAval.setValue("");
      }
      // tf_nom_etude .setValue(_params.titreEtude==project_.getTitre()?"":_params.titreEtude);
    }
  }
  private void getValeurs() throws IllegalArgumentException {
    SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    //System.out.println("action getValeurs params = " + params);
    String changed= "";
    double tmpD= 0.;
    int tmpI= 0;
    String tmpS= "";
    final String blanc= "";
    int n= 0;
    try {
      params.profAmont.nomProfil= tf_nomProfAm.getText();
      params.profAval.nomProfil= tf_nomProfAv.getText();
      //System.out.println("getValeurs profAmont.nbPoints 1)= " + params.profAmont.nbPoints);
      tmpI= params.profAmont.nbPoints;
      tmpS= tf_nbPointsAm.getText();
      //System.out.println("tmpS= " + tmpS);
      //System.out.println("blanc= " + blanc);
      if (tmpS.equals(blanc)) {
        params.profAmont.nbPoints= 0;
      } else {
        params.profAmont.nbPoints= Integer.parseInt(tmpS);
      }
      //System.out.println("getValeurs profAmont.nbPoints 2)= " + params.profAmont.nbPoints);
      if (params.profAmont.nbPoints != tmpI) {
        changed += "nbPointsAm|";
      }
      //System.out.println(" profAmont.nbPoints = " + params.profAmont.nbPoints);
      //params.profAval.nbPoints =((Integer)tf_nbPointsAv.getValue()).intValue();
      tmpI= params.profAval.nbPoints;
      tmpS= tf_nbPointsAv.getText();
      if (tmpS.equals(blanc)) {
        params.profAval.nbPoints= 0;
      } else {
        params.profAval.nbPoints= Integer.parseInt(tmpS);
      }
      //params.profAval.nbPoints =((Integer)tf_nbPointsAv.getValue()).intValue();
      if (params.profAval.nbPoints != tmpI) {
        changed += "nbPointsAv|";
      }
      //n = ((Integer)tf_nbPointsAm.getValue()) .intValue();
      n= params.profAmont.nbPoints;
      for (int i= 0; i < n; i++) {
        //System.out.println("getValeurs profAmont.xy[i].x 3)= " + params.profAmont.xy[i].x);
        tmpD= params.profAmont.xy[i].x;
        //System.out.println("getValeurs profAmont.nbPoints 4)= " + tf_profAmontX[i].getText());
        tmpS= tf_profAmontX[i].getText();
        if (tmpS.equals(blanc)) {
          params.profAmont.xy[i].x= -999.;
        } else {
          params.profAmont.xy[i].x= Double.parseDouble(tmpS);
        }
        //params.profAmont.xy[i].x=((Double)tf_profAmontX[i].getValue()).doubleValue();
        if (params.profAmont.xy[i].x != tmpD) {
          changed += "profAmontX[i]|";
        }
        //System.out.println("getValeurs profAmont.xy[i].x 3)= " + params.profAmont.xy[i].x);
        tmpD= params.profAmont.xy[i].y;
        tmpS= tf_profAmontY[i].getText();
        if (tmpS.equals(blanc)) {
          params.profAmont.xy[i].y= -999.;
        } else {
          params.profAmont.xy[i].y= Double.parseDouble(tmpS);
        }
        //params.profAmont.xy[i].y=((Double)tf_profAmontY[i].getValue()).doubleValue();
        if (params.profAmont.xy[i].y != tmpD) {
          changed += "profAmontY[i]|";
        }
      }
      //n = ((Integer)tf_nbPointsAv.getValue()) .intValue();
      n= params.profAval.nbPoints;
      for (int i= 0; i < n; i++) {
        //tmpD=params.profAval.xy[i].x;
        //params.profAval.xy[i].x=((Double)tf_profAvalX[i].getValue()).doubleValue();
        //if( params.profAval.xy[i].x!=tmpD ) changed+="profAvalX[i]|";
        //tmpD=params.profAval.xy[i].y;
        //params.profAval.xy[i].y=((Double)tf_profAvalY[i].getValue()).doubleValue();
        //if( params.profAval.xy[i].y!=tmpD ) changed+="profAvalY[i]|";
        tmpD= params.profAval.xy[i].x;
        tmpS= tf_profAvalX[i].getText();
        if (tmpS.equals(blanc)) {
          params.profAval.xy[i].x= -999.;
        } else {
          params.profAval.xy[i].x= Double.parseDouble(tmpS);
        }
        if (params.profAval.xy[i].x != tmpD) {
          changed += "profAvalX[i]|";
        }
        tmpD= params.profAval.xy[i].y;
        tmpS= tf_profAvalY[i].getText();
        if (tmpS.equals(blanc)) {
          params.profAval.xy[i].y= -999.;
        } else {
          params.profAval.xy[i].y= Double.parseDouble(tmpS);
        }
        if (params.profAval.xy[i].y != tmpD) {
          changed += "profAvalY[i]|";
        }
      }
      //
      tmpD= params.pelle;
      //    params.pelle=((Double)tf_pelle.getValue()).doubleValue();
      tmpS= tf_pelle.getText();
      if (tmpS.equals(blanc)) {
        params.pelle= -999.;
      } else {
        params.pelle= Double.parseDouble(tmpS);
      }
      if (params.pelle != tmpD) {
        changed += "pelle|";
      }
      tmpD= params.epaisCret;
      //     params.epaisCret=((Double)tf_epaisCret.getValue()).doubleValue();
      tmpS= tf_epaisCret.getText();
      if (tmpS.equals(blanc)) {
        params.epaisCret= -999.;
      } else {
        params.epaisCret= Double.parseDouble(tmpS);
      }
      if (params.epaisCret != tmpD) {
        changed += "epaisCret|";
      }
      tmpD= params.inclinaison;
      //    params.inclinaison=((Double)tf_inclinaison.getValue()).doubleValue();
      tmpS= tf_inclinaison.getText();
      if (tmpS.equals(blanc)) {
        params.inclinaison= -999.;
      } else {
        params.inclinaison= Double.parseDouble(tmpS);
      }
      if (params.inclinaison != tmpD) {
        changed += "inclinaison|";
      }
      tmpD= params.oblicite;
      //    params.oblicite =((Double)tf_oblicite .getValue()).doubleValue();
      tmpS= tf_oblicite.getText();
      if (tmpS.equals(blanc)) {
        params.oblicite= -999.;
      } else {
        params.oblicite= Double.parseDouble(tmpS);
      }
      if (params.oblicite != tmpD) {
        changed += "oblicite |";
      }
      tmpD= params.largEcoul;
      //   params.largEcoul=((Double)tf_largEcoul.getValue()).doubleValue();
      tmpS= tf_largEcoul.getText();
      if (tmpS.equals(blanc)) {
        params.largEcoul= -999.;
      } else {
        params.largEcoul= Double.parseDouble(tmpS);
      }
      if (params.largEcoul != tmpD) {
        changed += "largEcoul|";
      }
      tmpD= params.rayonCourb;
      //    params.rayonCourb=((Double)tf_rayonCourb.getValue()).doubleValue();
      tmpS= tf_rayonCourb.getText();
      if (tmpS.equals(blanc)) {
        params.rayonCourb= -999.;
      } else {
        params.rayonCourb= Double.parseDouble(tmpS);
      }
      if (params.rayonCourb != tmpD) {
        changed += "rayonCourb|";
      }
      //
      tmpD= params.debit;
      //  params.debit=((Double)tf_debit.getValue()).doubleValue();
      tmpS= tf_debit.getText();
      if (tmpS.equals(blanc)) {
        params.debit= -999.;
      } else {
        params.debit= Double.parseDouble(tmpS);
      }
      if (params.debit != tmpD) {
        changed += "debit|";
      }
      tmpD= params.coteAmont;
      //  params.coteAmont=((Double)tf_coteAmont.getValue()).doubleValue();
      tmpS= tf_coteAmont.getText();
      if (tmpS.equals(blanc)) {
        params.coteAmont= -999.;
      } else {
        params.coteAmont= Double.parseDouble(tmpS);
      }
      if (params.coteAmont != tmpD) {
        changed += "coteAmont|";
      }
      tmpD= params.coteAval;
      //  params.coteAval=((Double)tf_coteAval.getValue()).doubleValue();
      tmpS= tf_coteAval.getText();
      if (tmpS.equals(blanc)) {
        params.coteAval= -999.;
      } else {
        params.coteAval= Double.parseDouble(tmpS);
      }
      if (params.coteAval != tmpD) {
        changed += "coteAval|";
      }
    } catch (final NumberFormatException e) {
      params= null;
      new BuDialogError(
        appli_,
        appli_.getInformationsSoftware(),
        "parametres 01 => \nun champ est vide ou de format incorrect ! ")
        .activate();
      System.err.println("Parametres01 : NumberFormatException");
      throw new IllegalArgumentException("parametres 01 incorrects");
    }
    final StringTokenizer tok= new StringTokenizer(changed, "|");
    while (tok.hasMoreTokens()) {
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          SeuilResource.SEUIL01,
          params,
          SeuilResource.SEUIL01 + ":" + tok.nextToken()));
    }
  }
  private void effacerAm() {
    getValeurs();
    final SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    params.profAmont.nbPoints= 0;
    for (int i= 0; i < nptmax; i++) {
      params.profAmont.xy[i].x= -999.;
      params.profAmont.xy[i].y= -999.;
    }
    setValeurs();
    updatePanels();
    // envoi evenement pour ombrer l'item menu resultats
    final FudaaParamEvent e=
      new FudaaParamEvent(this, FudaaParamEvent.STRUCT_DELETED, profilsBief_);
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructDeleted(e);
  }
  private void effacerAv() {
    getValeurs();
    final SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    params.profAval.nbPoints= 0;
    for (int i= 0; i < nptmax; i++) {
      params.profAval.xy[i].x= -999.;
      params.profAval.xy[i].y= -999.;
    }
    setValeurs();
    updatePanels();
    // envoi evenement pour ombrer l'item menu resultats
    final FudaaParamEvent e=
      new FudaaParamEvent(this, FudaaParamEvent.STRUCT_DELETED, profilsBief_);
    FudaaParamEventProxy.FUDAA_PARAM.fireParamStructDeleted(e);
  }
  private void editerAm() {
    editProf_= AMONT;
    editerProfil();
  }
  private void editerAv() {
    editProf_= AVAL;
    editerProfil();
  }
  private void editerProfil() {
    imp_.assistant_.changeAttitude(
      BuAssistant.ATTENTE,
      "Nombre de points \nminimum  4\nmaximum 70");
    getValeurs();
    final SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    final int indice= 0;
    String numProfil= "";
    final double abscisse= 0D;
    final double absMajMin[]= new double[2];
    final double absMajSto[]= new double[2];
    final double altMinMaj[]= new double[2];
    final double altMajSto[]= new double[2];
    final double coefStrickMajMin= 0D;
    final double coefStrickMajSto= 0D;
    final double coteRivDr= 0D;
    final double coteRivGa= 0D;
    int nbPoints= 0;
    final double abs[]= new double[nptmax];
    final double cotes[]= new double[nptmax];
    if (editProf_ == AMONT) {
      numProfil= params.profAmont.nomProfil;
      if (params.profAmont.nbPoints > 0) {
        nbPoints= params.profAmont.nbPoints;
        for (int i= 0; i < nbPoints; i++) {
          abs[i]= params.profAmont.xy[i].x;
          cotes[i]= params.profAmont.xy[i].y;
        }
      }
    } else if (editProf_ == AVAL) {
      numProfil= params.profAval.nomProfil;
      if (params.profAval.nbPoints > 0) {
        nbPoints= params.profAval.nbPoints;
        for (int i= 0; i < nbPoints; i++) {
          abs[i]= params.profAval.xy[i].x;
          cotes[i]= params.profAval.xy[i].y;
        }
      }
    }
    profilsBief_=
      new SParametresBiefBlocPRO(
        indice,
        numProfil,
        abscisse,
        absMajMin,
        absMajSto,
        altMinMaj,
        altMajSto,
        coefStrickMajMin,
        coefStrickMajSto,
        coteRivDr,
        coteRivGa,
        nbPoints,
        abs,
        cotes);
    mode= EDITER;
    mode1= PROFILS;
    mode2= LidoProfilEditor.LEGER;
    final LidoProfilEditor edit_=
      new LidoProfilEditor(appli_, null, mode, mode1, mode2, project_);
    edit_.setObject(profilsBief_);
    edit_.addPropertyChangeListener(this);
    edit_.show();
  }
  private void recupProfil() {
    getValeurs();
    final SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    if (editProf_ == AMONT) {
      params.profAmont.nbPoints= profilsBief_.nbPoints;
      for (int i= 0; i < profilsBief_.nbPoints; i++) {
        params.profAmont.xy[i].x= profilsBief_.abs[i];
        params.profAmont.xy[i].y= profilsBief_.cotes[i];
      }
    } else if (editProf_ == AVAL) {
      params.profAval.nbPoints= profilsBief_.nbPoints;
      for (int i= 0; i < profilsBief_.nbPoints; i++) {
        params.profAval.xy[i].x= profilsBief_.abs[i];
        params.profAval.xy[i].y= profilsBief_.cotes[i];
      }
    }
    // liste();
    System.out.println(" recupProfil");
    setValeurs();
    updatePanels();
    getValeurs();
    imp_.assistant_.changeAttitude(BuAssistant.ATTENTE, " \n\n");
    // a la sortie du bouton valider de l'editeur de profil
    // l'item imprimer du menu principal devient ombr�
    // on le remt actif
    imp_.setEnabledForAction("IMPRIMER", true);
  }
  //
  //======================================================================
  //
  // pour les test
  //
  /*private void inidoncalc() {
    SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    params.sortieEcran= "imprim ";
    params.titreEtude= "titre del'etude ";
    params.comentEtude= "comentaire de l'etude ";
    params.profAmont.nomProfil= "titreprofamont";
    params.profAval.nomProfil= "titreprofaval";
    params.profAmont.nbPoints= 8;
    params.profAval.nbPoints= 4;
    params.profAmont.xy[0].x= 0.;
    params.profAmont.xy[0].y= 603.;
    params.profAmont.xy[1].x= 0.;
    params.profAmont.xy[1].y= 602.5;
    params.profAmont.xy[2].x= 2.;
    params.profAmont.xy[2].y= 602.5;
    params.profAmont.xy[3].x= 2.;
    params.profAmont.xy[3].y= 599.7;
    params.profAmont.xy[4].x= 61.;
    params.profAmont.xy[4].y= 599.7;
    params.profAmont.xy[5].x= 61.;
    params.profAmont.xy[5].y= 602.5;
    params.profAmont.xy[6].x= 63.;
    params.profAmont.xy[6].y= 602.5;
    params.profAmont.xy[7].x= 63;
    params.profAmont.xy[7].y= 603.;
    params.profAval.xy[0].x= 0.;
    params.profAval.xy[0].y= 603.;
    params.profAval.xy[1].x= 0.;
    params.profAval.xy[1].y= 599.7;
    params.profAval.xy[2].x= 63.;
    params.profAval.xy[2].y= 599.7;
    params.profAval.xy[3].x= 63.;
    params.profAval.xy[3].y= 603.;
    params.typeCret= "epais";
    //params.typeCret =  "mince";
    params.pelle= 0.9;
    params.epaisCret= 0.8;
    params.inclinaison= 0.;
    params.oblicite= 0.;
    params.largEcoul= 59.;
    params.rayonCourb= 0.;
    params.debit= 100.;
    params.coteAmont= 601.5;
    params.coteAval= 600.;
  }*/
  /*private void inidonprof() {
    SParametresSEU params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    params.profAmont.nbPoints= 8;
    params.profAval.nbPoints= 4;
    params.profAmont.xy[0].x= 0.;
    params.profAmont.xy[0].y= 103.;
    params.profAmont.xy[1].x= 0.;
    params.profAmont.xy[1].y= 102.5;
    params.profAmont.xy[2].x= 2.;
    params.profAmont.xy[2].y= 102.5;
    params.profAmont.xy[3].x= 2.;
    params.profAmont.xy[3].y= 99.7;
    params.profAmont.xy[4].x= 61.;
    params.profAmont.xy[4].y= 99.7;
    params.profAmont.xy[5].x= 61.;
    params.profAmont.xy[5].y= 102.5;
    params.profAmont.xy[6].x= 63.;
    params.profAmont.xy[6].y= 102.5;
    params.profAmont.xy[7].x= 63;
    params.profAmont.xy[7].y= 103.;
    params.profAval.xy[0].x= 0.;
    params.profAval.xy[0].y= 103.;
    params.profAval.xy[1].x= 0.;
    params.profAval.xy[1].y= 99.2;
    params.profAval.xy[2].x= 63.;
    params.profAval.xy[2].y= 99.2;
    params.profAval.xy[3].x= 63.;
    params.profAval.xy[3].y= 103.;
    System.out.println("inidonprof ");
  }*/
  /*private void liste() //pour les tests
  {
    SParametresSEU parm_=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    System.out.println("  incli " + parm_.titreEtude);
    System.out.println("  incli " + parm_.comentEtude);
    System.out.println("prof amont " + parm_.profAmont.nomProfil);
    System.out.println("prof aval  " + parm_.profAval.nomProfil);
    System.out.println("  incli " + parm_.inclinaison);
    System.out.println("  obli " + parm_.oblicite);
    System.out.println("  raycourb " + parm_.rayonCourb);
    System.out.println("  typrcret " + parm_.typeCret);
    System.out.println("  pelle " + parm_.pelle);
    System.out.println("  epais " + parm_.epaisCret);
    System.out.println("  largecoul " + parm_.largEcoul);
    System.out.println("  cotamont " + parm_.coteAmont);
    System.out.println("  cotaval " + parm_.coteAval);
    System.out.println("  debit " + parm_.debit);
    System.out.println("np amont " + parm_.profAmont.nbPoints);
    for (int i= 0; i < parm_.profAmont.nbPoints; i++) {
      System.out.println(
        " i x y "
          + i
          + " "
          + parm_.profAmont.xy[i].x
          + " "
          + parm_.profAmont.xy[i].y);
    }
    System.out.println("np aval " + parm_.profAval.nbPoints);
    for (int i= 0; i < parm_.profAval.nbPoints; i++) {
      System.out.println(
        " i x y "
          + i
          + " "
          + parm_.profAval.xy[i].x
          + " "
          + parm_.profAval.xy[i].y);
    }
  }*/
} // fin de classe
