/*
 * @file         SeuilFilleResultats.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.seuil.IResultatsSeuil;
import org.fudaa.dodico.corba.seuil.SParametresSEU;

import org.fudaa.ebli.graphe.BGraphe;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
/**
 * Une fenetre fille pour editer les resultats
 *
 * @version      $Id: SeuilFilleResultats.java,v 1.8 2006-09-19 15:10:27 deniger Exp $
 * @author       Jean-Yves Riou
 */
public class SeuilFilleResultats
  extends BuInternalFrame
  implements
    FudaaProjetListener,
    FudaaParamListener,
    ActionListener,
    BuPrintable,
    FocusListener {
  //===================================================
  //    Variables
  //===================================================
  //  general
  SeuilImplementation imp_; // implementation
  FudaaProjet project_; // projet
  BuCommonInterface appli_; // application
  IResultatsSeuil res_; // resultats
  // pour le dessin
  BGraphe pnGraphe;
  BGraphe pnGraphe1;
  JPanel pnGeoGraph= new JPanel();
  int VUERESPLAN= 2;
  int VUERESTRAV= 3;
  JComponent content_; // panneau general
  BuTextField tf_nom_etude;
  BuTextField tf_debit;
  BuTextField tf_chargeAmont;
  BuTextField tf_hautAmont;
  BuTextField tf_coteAmont;
  BuTextField tf_chargeAval;
  BuTextField tf_hautAval;
  BuTextField tf_coteAval;
  BuTextField tf_diffCharge;
  BuTextField tf_diffCote;
  BuTextField tf_coefDebit;
  BuTextField tf_HautCrit;
  BuTextField tf_TypeCrete;
  BuTextField tf_TypeEcoul;
  // boutons
  JButton btFermer_, btExporter_, btAnnuler_;
  JButton btVueTrav_, btVuePlan_;
  //  caracteristique du seuil  (prevu pour le dessin)
  // pour le moment ne sert pas
  String[] sLibTypeSeuil= { "Mince", "Epais", "inconnu" };
  String[] sLibTypeIncli= { "Droit", "Incline", "inconnu" };
  String[] sLibTypeObli= { "Droit", "Oblique", "inconnu" };
  String[] sLibTypeNoy= { "Noye", "Denoye", "inconnu" };
  String sTypeSeuil= "";
  String sTypeIncli= "";
  String sTypeObli= "";
  String sTypeNoy= "";
  String messagew1= " ";
  String messagew2= " ";
  //==========================================================
  //   Constructeur
  //==========================================================
  public SeuilFilleResultats(
    final BuCommonInterface _appli,
    final FudaaProjet projet,
    final SeuilImplementation _imp,
    final IResultatsSeuil _seuilResults) {
    super("", false, true, false, true);
    appli_= _appli;
    project_= projet;
    imp_= _imp;
    res_= _seuilResults;
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    project_.addFudaaProjetListener(this);
    System.out.println(
      "*FilleRes* coef seuil calcule = "
        + _seuilResults.resultatsSEU().coefSeuil);
    System.out.println("*FilleRes*  res_ = " + res_.resultatsSEU().coefSeuil);
    int n;
    //============================================================
    //     Panneau titre.
    //===========================================================
    tf_nom_etude= new BuTextField(" ");
    final JPanel pnTit= new JPanel();
    pnTit.setLayout(new BorderLayout());
    pnTit.add(
      "West",
      new JLabel("R�sultats du calcul au droit du seuil intitul�   "));
    tf_nom_etude.setColumns(20);
    pnTit.add("East", tf_nom_etude);
    //============================================================
    //     Panneau La hauteur critique est ...
    //===========================================================
    tf_HautCrit= BuTextField.createDoubleField();
    tf_TypeCrete= new BuTextField(" ");
    tf_TypeEcoul= new BuTextField(" ");
    final JPanel pnHautCrit= new JPanel();
    final BuGridLayout loCal0= new BuGridLayout();
    loCal0.setColumns(7);
    loCal0.setHgap(5);
    loCal0.setVgap(10);
    loCal0.setHfilled(true);
    loCal0.setCfilled(true);
    pnHautCrit.setLayout(loCal0);
    n= 0;
    tf_TypeEcoul.setColumns(6);
    tf_TypeCrete.setColumns(8);
    tf_HautCrit.setColumns(8);
    pnHautCrit.add(new JLabel("Seuil � Crete ", SwingConstants.RIGHT), n++);
    pnHautCrit.add(tf_TypeCrete, n++);
    pnHautCrit.add(new JLabel("Type d'�coulement ", SwingConstants.RIGHT), n++);
    pnHautCrit.add(tf_TypeEcoul, n++);
    pnHautCrit.add(new JLabel("Hauteur critique ", SwingConstants.RIGHT), n++);
    pnHautCrit.add(tf_HautCrit, n++);
    pnHautCrit.add(new JLabel("(m) ", SwingConstants.LEFT), n++);
    //====================================================
    //  Panneau du haut
    //====================================================
    final JPanel pnHaut= new JPanel();
    pnHaut.setBorder(new TitledBorder(""));
    pnHaut.setLayout(new BorderLayout());
    pnHaut.add("North", pnTit);
    pnHaut.add("South", pnHautCrit);
    //===============================================================
    // Parametres hydrauliques
    //=================================================================
    tf_debit= BuTextField.createDoubleField();
    tf_chargeAmont= BuTextField.createDoubleField();
    tf_hautAmont= BuTextField.createDoubleField();
    tf_coteAmont= BuTextField.createDoubleField();
    tf_chargeAval= BuTextField.createDoubleField();
    tf_hautAval= BuTextField.createDoubleField();
    tf_coteAval= BuTextField.createDoubleField();
    tf_diffCharge= BuTextField.createDoubleField();
    tf_diffCote= BuTextField.createDoubleField();
    tf_coefDebit= BuTextField.createDoubleField();
    //=====================================
    final JPanel pnGeoParm= new JPanel();
    pnGeoParm.setBorder(new TitledBorder("Param�tres Georauliques"));
    final BuGridLayout loCal1= new BuGridLayout();
    loCal1.setColumns(3);
    loCal1.setHgap(5);
    loCal1.setVgap(10);
    loCal1.setHfilled(true);
    loCal1.setCfilled(true);
    pnGeoParm.setLayout(loCal1);
    pnGeoParm.setBorder(new EmptyBorder(5, 5, 5, 5)); //top,left,bottom,right
    n= 0;
    tf_debit.setColumns(6);
    pnGeoParm.add(new JLabel("D�bit ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_debit, n++);
    pnGeoParm.add(new JLabel("(m3/s)     ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Charge amont ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_chargeAmont, n++);
    pnGeoParm.add(new JLabel("(m)     ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Hauteur amont ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_hautAmont, n++);
    pnGeoParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Cote amont ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_coteAmont, n++);
    pnGeoParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Charge aval ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_chargeAval, n++);
    pnGeoParm.add(new JLabel("(m)     ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Hauteur aval", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_hautAval, n++);
    pnGeoParm.add(new JLabel("(m)      ", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Cote aval", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_coteAval, n++);
    pnGeoParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Diff�rence de charges ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_diffCharge, n++);
    pnGeoParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Diff�rence de cotes ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_diffCote, n++);
    pnGeoParm.add(new JLabel("(m)", SwingConstants.LEFT), n++);
    pnGeoParm.add(new JLabel("Coefficient de d�bit ", SwingConstants.RIGHT), n++);
    pnGeoParm.add(tf_coefDebit, n++);
    pnGeoParm.add(new JLabel(" ", SwingConstants.LEFT), n++);
    //===============================
    // Panneau graphique droite
    //================================
    pnGeoGraph.setBorder(new TitledBorder("Graphique"));
    pnGeoGraph.setLayout(new BorderLayout());
    pnGeoGraph.setPreferredSize(new Dimension(300, 300));
    //==================================================================
    //   Panneau vue en plan /Profil en travers
    //==================================================================
    final JPanel pnTypeDessin= new JPanel();
    pnTypeDessin.setBorder(new LineBorder(Color.black, 2));
    btVuePlan_= new BuButton("Vue en Plan");
    btVueTrav_= new BuButton("Profil en travers");
    //btVuePlan_.setBackground(Color.blue);
    //btVueTrav_.setBackground(Color.white);
    pnTypeDessin.add(btVuePlan_);
    pnTypeDessin.add(btVueTrav_);
    btVuePlan_.addActionListener(this);
    btVueTrav_.addActionListener(this);
    //==================================
    //   Panneau Exporter /Fermer
    //=================================
    final JPanel pnValider= new JPanel();
    btExporter_= new BuButton("Exporter");
    btFermer_= new BuButton("Fermer");
    pnValider.add(btExporter_);
    pnValider.add(btFermer_);
    btExporter_.addActionListener(this);
    btFermer_.addActionListener(this);
    //====================================================
    //  Panneau Gauche
    //====================================================
    final JPanel pnGauche= new JPanel();
    pnGauche.setLayout(new BuVerticalLayout());
    pnGauche.add(pnGeoParm);
    pnGauche.add(pnTypeDessin);
    pnGauche.add(pnValider);
    //====================================================
    //  Panneau Bas
    //====================================================
    final JPanel pnBas= new JPanel();
    pnBas.setBorder(new TitledBorder(""));
    pnBas.setLayout(new BorderLayout());
    pnBas.add("West", pnGauche);
    pnBas.add("Center", pnGeoGraph);
    //================================
    content_= (JComponent)getContentPane();
    content_.setLayout(new BorderLayout());
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add(BorderLayout.NORTH, pnHaut);
    content_.add(BorderLayout.SOUTH, pnBas);
    updatePanels();
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setLocation(0, 0);
    vueTrav();
    pack();
  } // fin constructeur
  //==============================================================
  // Methodes publiques
  //==============================================================
  public void setProjet(final FudaaProjet project) {
    project_= project;
    updatePanels();
  }
  public void print(final PrintJob _job, final Graphics _g) {
    BuPrinter.INFO_DOC= new BuInformationsDocument();
    BuPrinter.INFO_DOC.name= getTitle();
    BuPrinter.INFO_DOC.logo= null; //BuResource.BU.getIcon("tableau",24);
    BuPrinter.printComponent(_job, _g, content_);
  }
  public void focusGained(final FocusEvent e) {
    //Object src=e.getSource();
    System.out.println("**** SeuilFilleResultats  focusGained");
  }
  public void focusLost(final FocusEvent e) {
    //Object src=e.getSource();
    //System.out.println("source " + src);
  }
  public void actionListener(final ActionEvent e) {
    //Object src=e.getSource();
  }
  public void actionPerformed(final ActionEvent e) {
    final Object src= e.getSource();
    if (src == btFermer_) {
      fermer();
    } else if (src == btVuePlan_) {
      vuePlan();
    } else if (src == btVueTrav_) {
      vueTrav();
    } else if (src == btExporter_) {
      exporter();
    } else {
      try {
        setClosed(true);
        setSelected(false);
      } catch (final PropertyVetoException ex) {}
    }
  }
  public void valider() // appelee par SeuilImplementation a voir
  {
    try {
      // System.out.println("fille parametres  valider()");
      //getValeurs ();
    } catch (final IllegalArgumentException e) {
      return;
    }
    try {
      setClosed(true);
      setSelected(false);
    } catch (final PropertyVetoException e) {}
  }
  public void fermer() {
    //    System.out.println("fille Resultats fermer()");
    //    System.out.println(" LidoMode "+imp_.LidoMode);
    //
    int res= 0;
    if (imp_.LidoMode == 1) {
      res=
        new SeuilDialogLido(
          imp_.getApp(),
          SeuilImplementation.isSeuil_,
          "  ",
          messagew1,
          messagew2,
          "  ")
          .activate();
    }
    //
    try {} catch (final IllegalArgumentException e) {
      return;
    }
    try {
      setClosed(true);
      setSelected(false);
      if (imp_.LidoMode == 1) {
        if (res == 0) {
          SeuilLido.t.getSeuilResult(imp_);
          //imp_.setValeurLido();
          imp_.exit();
        } // quitter Seuil
        else {
          imp_.parametre(); //ouverture  SeuilFilleParametres
        }
      }
    } catch (final PropertyVetoException e) {}
  }
  public void exporter() // on aurait pu deporter le source de imp_.exportResultats()
  { // mais entraine trop de modif
    try {
      imp_.exportResultats();
    } catch (final IllegalArgumentException e) {
      return;
    }
    // try {
    //   setClosed(true);
    //   setSelected(false);
    // } catch( PropertyVetoException e ) {}
  }
  public void delete() // a conserver sinon on ne peut sortir de seuil
  {
    FudaaParamEventProxy.FUDAA_PARAM.removeFudaaParamListener(this);
    if (project_ != null) {
      project_.removeFudaaProjetListener(this);
    }
    project_= null;
  }
  //
  // LidoParamListener
  //
  public void paramStructCreated(final FudaaParamEvent e) {
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  public void paramStructModified(final FudaaParamEvent e) {
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  //
  // fudaaprojet listener
  //
  public void dataChanged(final FudaaProjetEvent e) {
    switch (e.getID()) {
      case FudaaProjetEvent.PARAM_ADDED :
      case FudaaProjetEvent.PARAM_IMPORTED :
        {
          if (e.getSource() != this) {
            updatePanels();
          }
          break;
        }
    }
  }
  public void statusChanged(final FudaaProjetEvent e) {
    switch (e.getID()) {
      case FudaaProjetEvent.PROJECT_OPENED :
      case FudaaProjetEvent.PROJECT_CLOSED :
        {
          if (e.getSource() != this) {
            updatePanels();
          }
          break;
        }
    }
  }
  //=====================================================
  //   Methodes privees
  //=====================================================
  private void updatePanels() {
    // System.out.println("SeuilFilleParametres: update");
    setValeurs();
  }
  //private void setValeurs()
  public void setValeurs() {
    //System.out.println("action = setValeurs");
    final SParametresSEU _params=
      (SParametresSEU)project_.getParam(SeuilResource.SEUIL01);
    //System.out.println("action getValeurs params = " + _params);
    //    String blanc="";
    if (_params != null) {
      tf_nom_etude.setValue(res_.resultatsSEU().titreEtude);
      tf_TypeCrete.setValue(res_.resultatsSEU().typeCretCal);
      tf_TypeEcoul.setValue(res_.resultatsSEU().regime);
      tf_HautCrit.setValue(new Double(res_.resultatsSEU().hc));
      tf_debit.setValue(new Double(res_.resultatsSEU().debitCal));
      tf_chargeAmont.setValue(new Double(res_.resultatsSEU().chargAmont));
      tf_hautAmont.setValue(new Double(res_.resultatsSEU().hautAmont));
      tf_coteAmont.setValue(new Double(res_.resultatsSEU().cotAmont));
      tf_chargeAval.setValue(new Double(res_.resultatsSEU().chargAval));
      tf_hautAval.setValue(new Double(res_.resultatsSEU().hautAval));
      tf_coteAval.setValue(new Double(res_.resultatsSEU().cotAval));
      tf_diffCharge.setValue(new Double(res_.resultatsSEU().deltaCharge));
      tf_diffCote.setValue(new Double(res_.resultatsSEU().deltaCote));
      tf_coefDebit.setValue(new Double(res_.resultatsSEU().coefSeuil));
    }
  }
  public void vueTrav() {
    // System.out.println("vueTrav() je dessinne travers"+ pnGraphe1);
    if (pnGraphe != null) {
      pnGraphe.setVisible(false);
    }
    // btVuePlan_.setBackground(Color.white);
    //btVueTrav_.setBackground(Color.blue);
    btVuePlan_.setVisible(true);
    btVueTrav_.setVisible(true);
    btVuePlan_.repaint();
    btVueTrav_.repaint();
    // repaint();// ne fait rien
    // show();  // ne fait rien
    // pack();  // idem
    creeVueTrav();
    pnGraphe1.setVisible(true);
    pnGraphe1.repaint();
    repaint();
  }
  //public void propertyChange (PropertyChangeEvent _evt)
  //{
  //repaint();          ne fait rien pour les boutons
  //}
  private void creeVuePlan() {
    final double bidon= 0.;
    pnGeoGraph.removeAll(); //pour effacer precedent
    pnGraphe= new SeuilGraphe(project_, res_, VUERESPLAN, bidon);
    pnGeoGraph.add("Center", pnGraphe);
    show();
  }
  private void creeVueTrav() {
    final double bidon= 0.;
    pnGeoGraph.removeAll(); //pour effacer precedent
    pnGraphe1= new SeuilGraphe(project_, res_, VUERESTRAV, bidon);
    pnGeoGraph.add("Center", pnGraphe1);
    show();
  }
  private void vuePlan() {
    //System.out.println("je dessinne plan"+ pnGraphe);
    if (pnGraphe1 != null) {
      pnGraphe1.setVisible(false);
    }
    //btVuePlan_.setBackground(Color.blue);
    //btVueTrav_.setBackground(Color.white);
    btVuePlan_.repaint();
    btVueTrav_.repaint();
    repaint();
    creeVuePlan();
    pnGraphe.setVisible(true);
    pnGraphe.repaint();
    repaint();
  }
} // fin de classe
