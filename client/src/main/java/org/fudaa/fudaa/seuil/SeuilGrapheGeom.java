/*
 * @file         SeuilGrapheGeom.java
 * @creation     20-10-04
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;
/**
 * put your module comment here
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
class SeuilGrapheGeom extends BGraphe implements PropertyChangeListener {
  private double[][] valeurs_;
  private double[] temps_;
  private String nomaxeX_;
  //private String[] nomcol_;
  private Graphe graphe_;
  private Axe axeX;
  private CourbeDefault courbe_;
  private final Color[] col=
    { Color.black, Color.blue, Color.green, Color.orange, Color.pink };
  public SeuilGrapheGeom(
    final double[][] _valeurs,
    final double[] _temps,
    final String _nomaxeX,
    final String[] _nomcol) {
    graphe_= null;
    axeX= null;
    valeurs_= _valeurs;
    temps_= _temps;
    nomaxeX_= _nomaxeX;
//    nomcol_= _nomcol;
    initGraphe();
  }
  //Cette fonction renvoie un axe bien dimensionn�
  private Axe createAxe(final double[] _tab, final String _titre) {
    final Axe axe= new Axe();
    axe.titre_= _titre;
    axe.vertical_= true;
    axe.graduations_= true;
    axe.grille_= false;
    //Calcul du min et du max
    double min= _tab[0];
    double max= _tab[0];
    for (int i= 0; i < _tab.length; i++) {
      min= (_tab[i] < min) ? _tab[i] : min;
      max= (_tab[i] > max) ? _tab[i] : max;
    }
    // ajout riou
    min= -300.;
    max= 300.;
    //Calcul du pas � l'aide de min/max
    axe.pas_= (max - min) / 10;
    axe.minimum_= min;
    axe.maximum_= max;
    axe.visible_= true;
    return axe;
  }
  /**
   * put your documentation comment here
   */
  public void initGraphe() {
    final Marges marges= new Marges();
    graphe_= new Graphe();
    graphe_.animation_= false;
    graphe_.legende_= false;
    graphe_.marges_= marges;
    //Determination de l'intervalle de temps(Abscisse)
    //Calcul du pas pour le temps
    //Axe des x
    axeX= new Axe();
    axeX.titre_= nomaxeX_;
    axeX.unite_= "";
    axeX.vertical_= false;
    axeX.graduations_= true;
    axeX.grille_= false;
    //axeX.minimum = temps_[0];
    //axeX.maximum = temps_[temps_.length - 1];
    // riou
    axeX.minimum_= -300.;
    axeX.maximum_= 300.;
    axeX.pas_= Math.floor((axeX.maximum_ - axeX.maximum_) / temps_.length);
    axeX.visible_= true;
    graphe_.ajoute(axeX);
    int nbcourbes= valeurs_[0].length;
    System.out.println("ncourbes   = " + nbcourbes);
    System.out.println("col.length   = " + col.length);
    //Au plus trois courbes
    // if (valeurs_[0].length > col.length)
    //{
    //  new BuDialogError(null, null, "Trop de courbes � afficher seules les 3 premi�re"
    //      + "seront affich�es.").activate();
    //nbcourbes = 3;
    // }
    nbcourbes= 5;
    //Definition de lamarge en fonction du nombre de courbes
    //marges.gauche = 60*nbcourbes;
    //marges.droite = 150;
    //marges.haut = 45;
    //marges.bas = 30;
    marges.gauche_= 45;
    marges.droite_= 150;
    marges.haut_= 10;
    marges.bas_= 25;
    for (int u= 0; u < nbcourbes; u++) {
      System.out.println("ucourbes   = " + u);
      System.out.println("valeurs_.length   = " + valeurs_.length);
      final double[] vals= new double[valeurs_.length];
      Axe axe= new Axe();
      courbe_= new CourbeDefault();
      /*
      if(u < 3)
      {
       courbe_.titre = nomcol_[u];
      courbe_.trace = "lineaire";
      courbe_.marqueurs = false;
      //Creation d'un aspect
      //On affecte une couleur prise dans
      //le tableau membre de la classe
      Aspect asp = new Aspect();
      //  courbe_.aspect = asp;
      //  Vector valcourbe = new Vector();
        asp.contour = col[u];
        asp.surface = col[u];




        courbe_.aspect = asp;
        Vector valcourbe = new Vector();

      	for (i = 0; i < valeurs_.length; i++)
      	{
      	 	Valeur v = new Valeur();
         	vals[i] = v.v =valeurs_[i][u];
        	v.s = temps_[i];
        	valcourbe.add(v);
        }

      	 axe = createAxe(vals, nomcol_[u]);
      	 axe.aspect = asp;
      	courbe_.valeurs = valcourbe;
      	courbe_.axe = u;
      }
      */
      courbe_.titre_= "zigouigoui";
      courbe_.trace_= "lineaire";
      courbe_.marqueurs_= false;
      //Creation d'un aspect
      //On affecte une couleur prise dans
      //le tableau membre de la classe
      final Aspect asp= new Aspect();
      asp.contour_= Color.green;
      asp.surface_= Color.green;
      courbe_.aspect_= asp;
      final Vector valcourbe= new Vector();
      if (u == 1) {
        asp.contour_= Color.green;
        asp.surface_= Color.green;
        final Valeur v= new Valeur();
        vals[0]= v.v_= 3.;
        v.s_= 5.;
        valcourbe.add(v);
        final Valeur v1= new Valeur();
        vals[1]= v1.v_= 150.;
        v1.s_= 150.;
        valcourbe.add(v1);
      } else {
        asp.contour_= Color.pink;
        asp.surface_= Color.pink;
        final Valeur v= new Valeur();
        vals[0]= v.v_= 150.;
        v.s_= 5.;
        valcourbe.add(v);
        final Valeur v1= new Valeur();
        vals[1]= v1.v_= 50.;
        v1.s_= 130.;
        valcourbe.add(v1);
        v1.titre_= "jfjfjf";
      }
      axe= createAxe(vals, "tututata");
      axe.aspect_= asp;
      courbe_.valeurs_= valcourbe;
      courbe_.axe_= u;
      //================================================
      //=============================================
      courbe_.visible_= true;
      graphe_.legende_= true;
      graphe_.ajoute(axe);
      graphe_.ajoute(courbe_);
    }
    setInteractif(true);
    setGraphe(graphe_);
    fullRepaint();
  }
  public void propertyChange(final PropertyChangeEvent _evt) {
    fullRepaint();
  }
}
