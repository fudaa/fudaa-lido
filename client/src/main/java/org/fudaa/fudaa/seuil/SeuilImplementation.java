/**
 * @file SeuilImplementation.java
 * @creation 2000-10-23
 * @modification $Date: 2008-02-01 11:02:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.seuil;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.seuil.ICalculSeuil;
import org.fudaa.dodico.corba.seuil.ICalculSeuilHelper;
import org.fudaa.dodico.corba.seuil.IParametresSeuil;
import org.fudaa.dodico.corba.seuil.IParametresSeuilHelper;
import org.fudaa.dodico.corba.seuil.IResultatsSeuil;
import org.fudaa.dodico.corba.seuil.SParametresSEU;
import org.fudaa.dodico.corba.seuil.SPoint01;
import org.fudaa.dodico.corba.seuil.SProfelem01;
import org.fudaa.dodico.corba.seuil.SResultatsSEU;
import org.fudaa.dodico.seuil.DCalculSeuil;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaParamChangeLog;
import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamEventView;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetInformationsFrame;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;

import com.memoire.bu.BuApplication;
import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuBrowserControl;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuHelpFrame;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLookPreferencesPanel;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTaskOperation;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuToolBar;
import com.memoire.bu.BuUserPreferencesPanel;
import com.memoire.fu.FuLib;
/**
 * L'implementation du client Seuil.
 *
 * @version $Revision: 1.19 $ $Date: 2008-02-01 11:02:55 $ by $Author: clavreul $
 * @author Jean-Yves Riou
 */
public class SeuilImplementation extends FudaaImplementation implements FudaaParamListener,
    FudaaProjetListener {

  //public final static String              LOCAL_UPDATE    = ".";
  public final static boolean             IS_APPLICATION  = true;
  public static ICalculSeuil              SERVEUR_SEUIL   ;
  public static IConnexion                CONNEXION_SEUIL ;
  public static IPersonne                 PERSONNE        ;
  // VOIR SI ON PEUT VIRER CA: (AXA)
  //private IParametresSeuil              seuilParams;
  //protected IResultatsSeuil seuilResults;
  public IResultatsSeuil                  seuilResults;                                           // public
                                                                                                  // car
                                                                                                  // appele
                                                                                                  // par
                                                                                                  // Lido
  protected static BuInformationsSoftware isSeuil_        = new BuInformationsSoftware();
  protected static BuInformationsDocument idSeuil_        = new BuInformationsDocument();
  protected SeuilFilleParametres          fp_;
  protected SeuilFilleResultats           fr_;
  //protected FudaaProjet projet_; // public car appele par Lido
  public FudaaProjet                      projet_;
  protected JFrame                        ts_;
  protected BuAssistant                   assistant_;
  protected BuHelpFrame                   aide_;
  protected BuTaskView                    taches_;
  protected FudaaParamEventView           msgView_;
  protected FudaaProjetInformationsFrame  fProprietes_;
  // protected LidoSingulariteCourbeEditor impLido_;
  public int                              LidoMode        ;                                    //   Seuil
                                                                                                  // "Autonome"
                                                                                                  // LidoMode
                                                                                                  // = 0
  //   Seuil appele par Lido LidoMode = 1
  static {
    isSeuil_.name = "Seuil";
    isSeuil_.version = "0.12";
    isSeuil_.date = "19-fev-2001";
    isSeuil_.rights = "Tous droits r�serv�s. CETMEF (c)1999,2000";
    isSeuil_.contact = "isabelle.descatoire@equipement.gouv.fr";
    isSeuil_.license = "GPL2";
    isSeuil_.languages = "fr,en";
    isSeuil_.logo = SeuilResource.SEUIL.getIcon("seuil_logo");
    isSeuil_.banner = SeuilResource.SEUIL.getIcon("seuil_banner");
    isSeuil_.http = "http://www.utc.fr/fudaa/seuil/";
    isSeuil_.update = "http://www.utc.fr/fudaa/distrib/deltas/seuil/";
    //  isSeuil_.man ="http://marina.cetmef.equipement.gouv.fr/fudaa/manuels/seuil/";
    //  isSeuil_.man =REMOTE_MAN; non ne marche pas
    isSeuil_.authors = new String[] { "Jean-Yves Riou"};
    isSeuil_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa", "Axel von Arnim"};
    isSeuil_.documentors = new String[] { "     "};
    isSeuil_.testers = new String[] { "     "};
    idSeuil_.name = "Etude";
    idSeuil_.version = "0.0";
    idSeuil_.organization = "CETMEF";
    idSeuil_.author = System.getProperty("user.name");
    idSeuil_.contact = idSeuil_.author + "@cete13.equipement.gouv.fr";
    idSeuil_.date = FuLib.date();
    // idSeuil_.logo =EbliResource.EBLI.getIcon("minlogo.gif");
    BuPrinter.INFO_LOG = isSeuil_;
    BuPrinter.INFO_DOC = idSeuil_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isSeuil_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isSeuil_;
  }

  public BuInformationsDocument getInformationsDocument() {
    return idSeuil_;
  }

  public void init() {
    super.init();
    ts_ = null;
    fp_ = null;
    projet_ = null;
    aide_ = null;
    try {
      setTitle(isSeuil_.name + " " + isSeuil_.version);
      final BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
      setMainMenuBar(mb);
      mb.addActionListener(this);
      mb.addMenu(buildCalculMenu(IS_APPLICATION));
      final BuToolBar tb = BuToolBar.buildBasicToolBar();
      setMainToolBar(tb);
      tb.addActionListener(this);
      tb.removeAll();
      tb.addToolButton("Creer", "CREER", BuResource.BU.getIcon("CREER"), true);
      tb.addToolButton("Ouvrir", "OUVRIR", BuResource.BU.getIcon("OUVRIR"), true);
      tb.addSeparator();
      tb.addToolButton("Enregitrer", "ENREGISTRER", BuResource.BU.getIcon("ENREGISTRER"), true);
      tb.addToolButton("EnregisterSous", "ENREGISTRERSOUS", BuResource.BU
          .getIcon("ENREGISTRERSOUS"), true);
      tb.addSeparator();
      //tb.addToolButton("Fermer","FERMER",BuResource.BU.getIcon("FERMER"),true);
      tb.addToolButton("Imprimer", "IMPRIMER", BuResource.BU.getIcon("IMPRIMER"), true);
      //tb.addToolButton("Quitter","QUITTER",BuResource.BU.getIcon("QUITTER"),true);
      tb.addSeparator();
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      //tb.addToolButton("Calculer","CALCULER",false);
      tb.addToolButton("Parametres", "PARAMETRE", BuResource.BU.getIcon("PARAMETRE"), true);
      tb.addToolButton("Resultats", "RESULTAT", BuResource.BU.getIcon("RESULTAT"), true);
      //((BuMenu)mb.getMenu("MENU_EDITION")).addMenuItem("Console","CONSOLE",false);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", true);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("RESULTAT", false);
      removeAction("REOUVRIR");
      removeAction("PREVISUALISER");
      //=== essai suppression impoter exporter
      // removeAction("EXPORTER");
      // removeAction("IMPORTER");
      // ((JMenu)mb.getMenu("Fichier")).remove("IMPORTER");
      //((JMenu)mb.getMenu("Fichier")).removeAction("IMPORTER");
      // JMenu m=(JMenu)mb.getMenu("FICHIER");
      // System.out.println("m = "+m);
      //((JMenu)m.remove("IMPORTER");
      //========================================
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", true);
      //
      // remarque removeMenuItem(DString -cmd) n'existe plus
      //
      final BuMainPanel mp = getApp().getMainPanel();
      mp.setPreferredSize(new Dimension(1000, 600));
      final BuColumn rc = mp.getRightColumn();
      assistant_ = new SeuilAssistant();
      taches_ = new BuTaskView();
      final BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      //new BuTaskOperation(this, "SpyTask", "oprServeurCopie" ).run();
      rc.addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp, this);
      mp.setLogo(isSeuil_.logo);
      mp.setTaskView(taches_);
      msgView_ = new FudaaParamEventView();
      final BuScrollPane sp2 = new BuScrollPane(msgView_);
      sp2.setPreferredSize(new Dimension(150, 80));
      // lc.addToggledComponent("Messages", "MESSAGE", sp2, this); jyr
      rc.addToggledComponent("Messages", "MESSAGE", sp2, this);
      setParamEventView(msgView_);
      //==========================================
      // peut etre a remettre
      FudaaParamChangeLog.CHANGE_LOG.setApplication((BuApplication) SeuilApplication.FRAME,
          msgView_);
      //===========================================
    }
    catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }

  public void start() {
    super.start();
    // about();
    //(new VagConnexion(this)).start();
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + isSeuil_.name + " "
        + isSeuil_.version);
    final BuMainPanel mp = getMainPanel();
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("seuil"));
    projet_.addFudaaProjetListener(this);
    System.out.println("Seuilimplementation start projet_" + projet_);
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE,
        "Vous pouvez cr�er un\nnouveau projet Seuil\nou en ouvrir un");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    // Preferences Seuil
    SeuilPreferences.SEUIL.applyOn(this);
    fProprietes_ = new FudaaProjetInformationsFrame(this);
    fProprietes_.setVisible(false);
    fProprietes_.setProjet(projet_);
    addInternalFrame(fProprietes_);
    fProprietes_.setVisible(false);
    if (fProprietes_.isVisible()) {
      try {
        fProprietes_.setClosed(true);
      }
      catch (final Exception ex) {}
    }
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    //fullscreen();
  }


  public void setParamEventView(final FudaaParamEventView v) {
    msgView_ = v;
  }

  public FudaaParamEventView getParamEventView() {
    return msgView_;
  }

  // Accesseurs
  // Menu Calcul
  protected BuMenu buildCalculMenu(final boolean _app) {
    final BuMenu r = new BuMenu("Seuil", "CALCUL");
    r.addMenuItem("Param�tres", "PARAMETRE", false);
    r.addMenuItem("R�sutats", "RESULTAT", false);
    r.addSeparator();
    return r;
  }

  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    String arg = null;
    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    final int i = action.indexOf('(');
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir();
    } else if (action.equals("REOUVRIR")) {
      ouvrir(arg);
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("FERMER")) {
      fermer();
    } else //if( action.endsWith("IN")&&
    //   (action.startsWith("ASC")||
    //    action.startsWith("XML")) ) importerProjet();
    //else
    if (action.equals("PREFERENCE")) {
      preferences();
    } else if (action.equals("PROPRIETE")) {
      fProprietes_.setVisible(true);
      if (fProprietes_.isClosed()) {
        try {
          fProprietes_.setClosed(false);
        }
        catch (final Exception ex) {}
      }
      activateInternalFrame(fProprietes_);
    }
    else if (action.equals("PARAMETRE")) {
      parametre();
    } else //if( action.equals("RAPPORT") ) rapport();
    //else
    if (action.equals("RESULTAT")) {
      resultats();
    } else if (action.equals("ASSISTANT") || action.equals("CALQUE")) {
      final BuColumn rc = getMainPanel().getRightColumn();
      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
    }
    else if (action.equals("MESSAGE")) {
      final BuColumn lc = getMainPanel().getLeftColumn();
      lc.toggleComponent(action);
      setCheckedForAction(action, lc.isToggleComponentVisible(action));
    } else {
      super.actionPerformed(_evt);
    }
  }

  // LidoParamListener
  public void paramStructCreated(final FudaaParamEvent e) {
    System.out.println("Implementation  paramStructCreated  " + e);
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  public void paramStructDeleted(final FudaaParamEvent e) {
    System.out.println("Implementation  paramStructDeleted   " + e);
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  public void paramStructModified(final FudaaParamEvent e) {
    System.out.println("Implementation  paramStructModified   " + e);
    System.out.println("Implementation projet_.containsResults  : " + projet_.containsResults());
    if (projet_.containsResults()) {
      new BuDialogMessage(getApp(), getInformationsSoftware(), "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n" + "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }

  // fudaaprojet listener
  public void dataChanged(final FudaaProjetEvent e) {
    System.out.println("Implementation  dataChanged" + e);
    switch (e.getID()) {
    case FudaaProjetEvent.RESULT_ADDED:
    case FudaaProjetEvent.RESULTS_CLEARED:
        FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
        setEnabledForAction("ENREGISTRER", true);
        break;
    }
  }

  public void statusChanged(final FudaaProjetEvent e) {
    System.out.println("Implementation  statusChanged" + e);
    if (e.getID() == FudaaProjetEvent.HEADER_CHANGED) {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
    }
  }

  public void displayURL(String _url) {
    final String netHelp = System.getProperty("net.access.man");
    if (!"remote".equals(netHelp)) {
      _url = "file:" + System.getProperty("user.dir") + "/manuels/seuil/utilisation.html";
    }
    if (BuPreferences.BU.getStringProperty("browser.internal").equals("true")) {
      if (aide_ == null) {
        aide_ = new BuHelpFrame();
        aide_.addInternalFrameListener(this);
      }
      addInternalFrame(aide_);
      aide_.setDocumentUrl(_url);
    }
    else {
      if (_url == null) {
        final BuInformationsSoftware il = getInformationsSoftware();
        _url = il.http;
      }
      BuBrowserControl.displayURL(_url);
    }
  }

  public void oprServeurCopie() {
    System.err.println("Lancement du serveur de copie d'ecran");
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }

  public void oprCalculer() {
    System.out.println("Implementation oprCalculer()");
    if (!isConnected()) {
      new BuDialogError(getApp(), isSeuil_, "vous n'etes pas connect� � un serveur SEUIL ! ")
          .activate();
      return;
    }
    setEnabledForAction("CALCULER", false);
    final BuMainPanel mp = getMainPanel();
    System.err.println("Transmission des parametres...");
    mp.setMessage("Transmission des parametres...");
    mp.setProgression(0);
    //=================================================================================
    
    IParametresSeuil seuilParams = IParametresSeuilHelper.narrow(SERVEUR_SEUIL.parametres(CONNEXION_SEUIL));
    seuilParams.parametresSEU((SParametresSEU) projet_.getParam(SeuilResource.SEUIL01));
    //public SParametresSEU p1= new SParametresSEU();
    //projet_.addParam(SeuilResource.SEUIL01, p1);
    
    // � voir !!!
    //=================================================================================
    System.err.println("Execution du calcul...");
    mp.setMessage("Execution du calcul...");
    mp.setProgression(20);
    try {
      //==================================================
      SERVEUR_SEUIL.calcul(CONNEXION_SEUIL); // lancement du calcul
      receptionResultats(true);
      //==================================================
    }
    catch (final Throwable u) {
      new BuDialogError(getApp(), isSeuil_, u.toString()).activate();
      u.printStackTrace();
      //   SResultatsOUT out=seuilResults.resultatsOUT();
      //  if( out!=null ) afficheResultatOUT(out);
      return;
    }
  }

  public void oprReceptionResultats() {
    System.out.println("Implementation oprRecptionResultats()");
    try {
      receptionResultats(false);
    }
    catch (final Exception e) {
      new BuDialogError(getApp(), isSeuil_, e.toString()).activate();
      e.printStackTrace();
      //  SResultatsOUT out=seuilResults.resultatsOUT();
      //  if( out!=null ) afficheResultatOUT(out);
    }
  }

  // Methodes privees
  protected void creer() {
    //System.out.println("ouvrir projetconfigure "+projet_.estConfigure());
    //System.out.println("ouvrir projetestvierge "+projet_.estVierge());
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si creation de projet annulee
    } else { // nouveau projet cree
      if (fp_ != null) {
        closeFilleParams();
      }
      creeParametres();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("REOUVRIR", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      //================================================
      // rajout� ici temporairement par RIOU pour tests
      //================================================
      //    setEnabledForAction("RESULTAT" , true);
      //================================================
      setTitle(projet_.getFichier());
    }
  }

  protected void ouvrir() {
    System.out.println("ouvrir  projetconfigure " + projet_.estConfigure());
    System.out.println("ouvrir  projetestvierge " + projet_.estVierge());
    if (!projet_.estVierge()) {
      fermer();
    }
    // ===========on force =====================
    projet_.setEnrResultats(true);
    //==========================================
    projet_.ouvrir();
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("INFOCALCUL", false);
      setEnabledForAction("REOUVRIR", false);
    }
    else {
      if (fp_ != null) {
        closeFilleParams();
      }
      new BuDialogMessage(getApp(), isSeuil_, "Param�tres charg�s").activate();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("REOUVRIR", true);
      //================= avril 2001======================
      System.out.println("mode                **** " + projet_.getMode());
      System.out.println("isEnrResultats      **** " + projet_.containsResults());
      if (projet_.containsResults()) {
        System.out.println("containsResults   **** " + projet_.containsResults());
        setEnabledForAction("RESULTAT", true);
        //==== marche ========================================================================
        final SResultatsSEU wseuilResults = (SResultatsSEU) projet_.getResult(SeuilResource.SEUIL01);
        System.out.println("wseuilResults coef seuil calcule = " + wseuilResults.coefSeuil);
        System.out.println(" xxx = " + seuilResults);
        seuilResults.resultatsSEU(wseuilResults);
        System.out.println("seuilResults           = " + seuilResults);
        System.out.println("seuilResults.debitCal  = " + seuilResults.resultatsSEU().debitCal);
        System.out.println("seuilResults.coefSeuil = " + seuilResults.resultatsSEU().coefSeuil);
      }
      //==================================================
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
    }
  }

  protected void ouvrir(final String arg) {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.ouvrir(arg);
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("MAILLEUR", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("REOUVRIR", false);
    }
    else {
      if (fp_ != null) {
        closeFilleParams();
      }
      // HACK pour recuperer les projets ancien modele
      if (!projet_.containsParam(SeuilResource.SEUIL01)) {
        projet_.addParam(SeuilResource.SEUIL01, new SParametresSEU());
      }
      new BuDialogMessage(getApp(), isSeuil_, "Param�tres charg�s").activate();
      setEnabledForAction("PARAMETRE", true);
      // setEnabledForAction("EXPORTER" , true); avirl 2001
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
      if (arg == null) {
        final String r = projet_.getFichier();
        getMainMenuBar().addRecentFile(r, "seuil");
        SeuilPreferences.SEUIL.writeIniFile();
      }
    }
  }

  protected void enregistrer() {
    if (fp_ != null) {
      if (!fp_.isClosed()) {
        fp_.valider();
      }
    }
    System.out.println("Enregistre containsResults  " + projet_.containsResults());
    //============= riou avril 2001 =====
    if (projet_.containsResults()) {
      projet_.setEnrResultats(true);
    }
    //===================================
    projet_.enregistre();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
  }

  protected void enregistrerSous() {
    if (fp_ != null) {
      if (!fp_.isClosed()) {
        fp_.valider();
      }
    }
    projet_.enregistreSous();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
    setTitle(projet_.getFichier());
  }

  protected void fermer() {
    //System.out.println("**** fermer");
    //if( projet_.estConfigure() ) System.out.println(" projetconfigure "+projet_.estConfigure());
    //if( FudaaParamChangeLog.CHANGE_LOG.isDirty() ) System.out.println(" changelog
    // "+FudaaParamChangeLog.CHANGE_LOG.isDirty() );
    if (projet_.estConfigure() && FudaaParamChangeLog.CHANGE_LOG.isDirty()) {
      final int res = new BuDialogConfirmation(getApp(), isSeuil_, "Votre projet va etre ferm�.\n"
          + "Voulez-vous l'enregistrer avant?\n").activate();
      if (res == JOptionPane.YES_OPTION) {
        enregistrer();
      }
    }
    if (fProprietes_.isVisible()) {
      fProprietes_.setVisible(false);
    }
    final BuDesktop dk = getMainPanel().getDesktop();
    if (fp_ != null) {
      closeFilleParams();
      dk.removeInternalFrame(fp_);
      fp_.removeInternalFrameListener(this);
      fp_.delete();
      fp_ = null;
    }
    // ajoute par jyr
    if (projet_.containsResults()) {
      // new BuDialogMessage(getApp(), getInformationsSoftware(),
      //   "Vous avez modifi� un param�tre.\n"+
      //   "Les r�sultats ne sont plus valides:\n"+
      //   "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    projet_.fermer();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    dk.repaint();
    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("REOUVRIR", false);
    setEnabledForAction("INFOCALCUL", false);
    setTitle("Seuil");
  }

  protected void closeFilleParams() {
    try {
      fp_.setClosed(true);
      fp_.setSelected(false);
    }
    catch (final PropertyVetoException e) {}
  }

  protected void buildPreferences(final List _prefs){
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
  }

  protected void parametre() {
    if (fp_ == null) {
      fp_ = new SeuilFilleParametres(getApp(), projet_, this);
      fp_.addInternalFrameListener(this);
      addInternalFrame(fp_);
      assistant_.addEmitters(fp_);
    }
    else {
      if (fp_.isClosed()) {
        addInternalFrame(fp_);
      } else {
        activateInternalFrame(fp_);
      }
    }
  }

  protected void resultats() {
    //System.out.println("======avant ==== SeuilFilleResultats() fr_"+fr_);
    //System.out.println("======avant ==== SeuilFilleResultats()");
    //System.out.println(" seuilResults"+seuilResults);
    //System.out.println("seuilResults.debitCal = "+ seuilResults.resultatsSEU().debitCal );
    //System.out.println("seuilResults.coefSeuil = "+ seuilResults.resultatsSEU().coefSeuil );
    //System.out.println(" epais " + seuilResults.resultatsSEU().epaisCret) ;
    //System.out.println(" largecoul "+seuilResults.resultatsSEU().largEcoul ) ;
    if (fr_ == null) {
      fr_ = new SeuilFilleResultats(getApp(), projet_, this, seuilResults);
      fr_.addInternalFrameListener(this);
      addInternalFrame(fr_);
      assistant_.addEmitters(fr_);
    }
    else {
      if (fr_.isClosed()) {
        addInternalFrame(fr_);
        if (projet_.containsResults()) {
          fr_.setValeurs(); // chargement des champs de FilleResultats
          fr_.vueTrav(); // dessin vue profil de FilleResultats
        }
      } else {
        activateInternalFrame(fr_);
      }
    }
  }

  // appelee dans SeuilFilleParametres
  //protected void calculer()
  public void calculer() {
    if (!isConnected()) {
      new BuDialogError(getApp(), isSeuil_, "vous n'etes pas connect� � un serveur!").activate();
      return;
    }
    System.out.println("Implementation calculer()");
    if (fp_ != null) {
      if (!fp_.isClosed()) {
        fp_.valider();
      }
    }
    if (valideContraintesCalcul()) {
      new BuTaskOperation(this, "Calcul", "oprCalculer").start();
    }
  }

  //protected void exportResultats(String key)
  public void exportResultats() {
    System.out.println("Implementation exportResultats()");
    final JFileChooser chooser = new JFileChooser(); //
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        return true;
      }

      public String getDescription() {
        return "*.*";
      }
    });
    chooser.setCurrentDirectory(new java.io.File(System.getProperty("user.dir")));
    final int returnVal = chooser.showSaveDialog(SeuilApplication.FRAME);
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
      if (filename == null) {
        return;
      }
      if (new File(filename).exists()) {
        if (new BuDialogConfirmation(getApp(), isSeuil_, "Le fichier :\n" + filename
            + "\nexiste d�j�. Voulez-vous l'�craser?\n").activate() == JOptionPane.NO_OPTION) {
          return;
        }
      }
      try {
        if (projet_.containsResults()) {
          SeuilExport.exportResultats(seuilResults, isSeuil_, filename);
        } else {
          new BuDialogError(getApp(), getInformationsSoftware(),
              "Il n'y a pas de r�sultats :\nvous n'avez pas lance de calcul").activate();
        }
      }
      catch (final java.io.IOException ex) {
        new BuDialogError(getApp(), getInformationsSoftware(), ex.toString()).activate();
      }
    }
  }

  protected void receptionResultats(final boolean calcul) {
    System.out.println("Implementation reception Resultats()");
    final BuMainPanel mp = getMainPanel();
    System.err.println("Reception des resultats...");
    mp.setMessage("Reception des resultats...");
    mp.setProgression(90);
    // important permet d'inialiser l'existence des resultats
    // pour utiliser if( projet_.containsResults() )
    projet_.addResult(SeuilResource.SEUIL01, seuilResults.resultatsSEU());
    //======================================================================================
    //  les resultats sont dans seuilResults.resultatsSEU()
    //=======================================================================================
    System.out.println("===============Apr�s calcul()");
    System.out.println(" seuilResults" + seuilResults);
    System.out.println("seuilResults.debitCal  = " + seuilResults.resultatsSEU().debitCal);
    System.out.println("seuilResults.coefSeuil = " + seuilResults.resultatsSEU().coefSeuil);
    System.err.println("Operation terminee.");
    mp.setMessage("Operation terminee.");
    //==============================================
    setEnabledForAction("RESULTAT", true);
    //setEnabledForAction("EXPORTER" , true); avril 2001
    //==============================================
    mp.setProgression(100);
    if (LidoMode == 0) {
      if (calcul) {
        new BuDialogMessage(getApp(), isSeuil_,
            "Calcul termin�\nCliquez sur \"Seuil..R�sultats\" pour voir les r�sultats.").activate();
      } else {
        new BuDialogMessage(getApp(), isSeuil_, "R�sultats charg�s").activate();
      }
      mp.setMessage("Affichage...");
      mp.setProgression(10);
    }
    if (LidoMode == 1) {
      resultats();
    }
  }

  public void fullscreen() {
    //if(getMainPanel().getCurrentInternalFrame()==fVue3D_)
    //	fVue3D_.fullscreen();
    //else
    super.fullscreen();
  }

  private void creeParametres() {
    System.out.println("Implementation creeParametres()");
    final int nptmax = 80;
    final SParametresSEU p01 = new SParametresSEU();
    p01.titreEtude = "aaaaaaaaaa";
    p01.comentEtude = "bbbbbbbbbb";
    if (LidoMode == 0) {
      p01.titreEtude = projet_.getInformations().titre;
      p01.comentEtude = projet_.getInformations().commentaire;
    }
    p01.profAmont = new SProfelem01();
    p01.profAmont.xy = new SPoint01[nptmax];
    p01.profAval = new SProfelem01();
    p01.profAval.xy = new SPoint01[nptmax];
    p01.profAmont.nomProfil = " ";
    p01.profAval.nomProfil = " ";
    p01.profAmont.nbPoints = 0;
    p01.profAval.nbPoints = 0;
    for (int i = 0; i < nptmax; i++) {
      p01.profAmont.xy[i] = new SPoint01();
      p01.profAval.xy[i] = new SPoint01();
    }
    for (int i = 0; i < nptmax; i++) {
      p01.profAmont.xy[i].x = -999.;
      p01.profAmont.xy[i].y = -999.;
      p01.profAval.xy[i].x = -999.;
      p01.profAval.xy[i].y = -999.;
    }
    p01.pelle = -999.;
    p01.epaisCret = -999.;
    p01.inclinaison = -999.;
    p01.oblicite = -999.;
    p01.largEcoul = -999.;
    p01.rayonCourb = -999.;
    p01.debit = -999.;
    p01.coteAmont = -999.;
    p01.coteAval = -999.;
    //===============================================
    projet_.addParam(SeuilResource.SEUIL01, p01);
    //===============================================
  }

  private boolean valideContraintesCalcul() {
    //System.out.println("Implementation valideContraintesCalcul()");
    // la verif se fait dans SeuilFilleParametres
    //SParametresSEU p01=(SParametresSEU)projet_.getParam(SeuilResource.SEUIL01); // a voir
    return true;
  }


  public void exit() {
    fermer();
    super.exit();
  }

  public boolean confirmExit() {
    return true;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  public SeuilImplementation getSeuilImplementation() {
    return (this);
  }

  public void lidocreer(final SParametresBiefBlocPRO proAm, final SParametresBiefBlocPRO proAv) //			 	 LidoSingulariteCourbeEditor
                                                                                    // _impLido)
  {
    System.out.println("*** lidocreer ***");
    //impLido_ = _impLido;
    System.out.println("abscisse  =  " + proAm.abscisse);
    System.out.println("nbPoints  =  " + proAm.nbPoints);
    System.out.println("numProfil =  " + proAm.numProfil);
    /*
     * for(int i=0; i <proAm.nbPoints;i++) { System.out.println("i x y = "+i+" "+proAm.abs[i]+" "+
     * proAm.cotes[i]); }
     */
    System.out.println("abscisse  =  " + proAv.abscisse);
    System.out.println("nbPoints  =  " + proAv.nbPoints);
    System.out.println("numProfil =  " + proAv.numProfil);
    /*
     * for(int i=0; i <proAv.nbPoints;i++) { System.out.println("i x y = "+i+" "+proAv.abs[i]+" "+
     * proAv.cotes[i]); }
     */
    //System.out.println("ouvrir projetconfigure "+projet_.estConfigure());
    //System.out.println("ouvrir projetestvierge "+projet_.estVierge());
    //projet_.creer();
    //projet_=new FudaaProjet(getApp(), new FudaaFiltreFichier("seuil"));
    //projet_.addFudaaProjetListener(this);
    if (fp_ != null) {
      closeFilleParams();
    }
    //System.out.println("projet_"+projet_);
    projet_.setFichier("Lido");
    //projet_.infos_.titre = "aaa"; info_ innaccessible, private
    //projet_.infos_.commentaire = "bbb";
    LidoMode = 1;
    creeParametres(); // creation structure
    //initialisation avec valeurs <= Lido
    final SParametresSEU params = (SParametresSEU) projet_.getParam(SeuilResource.SEUIL01);
    params.profAmont.nomProfil = proAm.numProfil;
    params.profAval.nomProfil = proAv.numProfil;
    params.profAmont.nbPoints = proAm.nbPoints;
    for (int i = 0; i < proAm.nbPoints; i++) {
      params.profAmont.xy[i].x = proAm.abs[i];
      params.profAmont.xy[i].y = proAm.cotes[i];
    }
    params.profAval.nbPoints = proAv.nbPoints;
    for (int i = 0; i < proAv.nbPoints; i++) {
      params.profAval.xy[i].x = proAv.abs[i];
      params.profAval.xy[i].y = proAv.cotes[i];
    }
    setEnabledForAction("PARAMETRE", true);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("REOUVRIR", false);
    setEnabledForAction("EXPORTER", false);
    setEnabledForAction("INFOCALCUL", false);
    //================================================
    // rajout� ici temporairement par RIOU pour tests
    //================================================
    setEnabledForAction("RESULTAT", true);
    //================================================
    setTitle(projet_.getFichier());
    parametre(); //ouverture SeuilFilleParametres
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_SEUIL = null;
    SERVEUR_SEUIL = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_SEUIL, CONNEXION_SEUIL);
    return new FudaaDodicoTacheConnexion[] { c};
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculSeuil.class};
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculSeuil.class);
    CONNEXION_SEUIL = c.getConnexion();
    SERVEUR_SEUIL = ICalculSeuilHelper.narrow(c.getTache());
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return SeuilPreferences.SEUIL;
  }
} // fin classe
