/*
 * @file         SeuilLido.java
 * @creation     2000-04-18
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.fudaa.dodico.corba.seuil.SParametresSEU;
/**
 * @version      $Id: SeuilLido.java,v 1.7 2006-09-19 15:10:27 deniger Exp $
 * @author       Jean-Yves Riou
 */
public class SeuilLido //implements  ChangeListener
{
  public static final SeuilLido t= new SeuilLido();
  public double coefdebit;
  public double x1, y1, x2, y2, x3, y3, x4, y4;
  private double refam;
  private double largw;
  private double tm;
  private ChangeListener l;
  private ChangeEvent e;
  //public   SeuilLido ()
  //{
  //}
  public void addChangeListener(final ChangeListener _l) {
    l= _l;
  }
  public void getSeuilResult(final SeuilImplementation _imp) {
    final SeuilImplementation imp_= _imp;
    System.out.println(
      "seuilResults.coefSeuil = " + imp_.seuilResults.resultatsSEU().coefSeuil);
    final SParametresSEU parm_=
      (SParametresSEU)imp_.projet_.getParam(SeuilResource.SEUIL01);
    System.out.println("pelle    = " + parm_.pelle);
    System.out.println("largeur  = " + parm_.largEcoul);
    System.out.println("oblicite = " + parm_.oblicite);
    refam= parm_.profAmont.xy[0].y;
    System.out.println("refam = " + refam);
    for (int i= 0; i < parm_.profAmont.nbPoints; i++) {
      if (parm_.profAmont.xy[i].y < refam) {
        refam= parm_.profAmont.xy[i].y;
      }
    }
    largw= parm_.largEcoul * Math.cos(parm_.oblicite);
    tm=
      (parm_.profAmont.xy[0].x
        + parm_.profAmont.xy[parm_.profAmont.nbPoints
        - 1].x)
        / 2.;
    x1= tm - largw / 2.;
    x4= tm + largw / 2.;
    x2= x1;
    x3= x4;
    y1= refam;
    y4= refam;
    y2= refam + parm_.pelle;
    y3= y2;
    coefdebit= imp_.seuilResults.resultatsSEU().coefSeuil;
    e= new ChangeEvent(this);
    l.stateChanged(e);
  }
}
