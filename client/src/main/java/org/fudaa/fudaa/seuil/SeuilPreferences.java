/*
 * @file         SeuilPreferences.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
//import org.fudaa.ebli.bu.*;
import com.memoire.bu.BuPreferences;
/**
 * Preferences pour Seuil.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:10:26 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class SeuilPreferences extends BuPreferences {
  public final static SeuilPreferences SEUIL= new SeuilPreferences();
  public void applyOn(final Object _o) {
    if (!(_o instanceof SeuilImplementation)) {
      throw new RuntimeException("" + _o + " is not a SeuilImplementation.");
    //    SeuilImplementation _appli=(SeuilImplementation)_o;
    }
  }
}
