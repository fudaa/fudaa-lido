/*
 * @file         SeuilPreferencesPanel.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
//import org.fudaa.ebli.bu.*;
import java.util.Hashtable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.AbstractBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;
/**
 * Panneau de preferences pour Seuil.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:10:26 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class SeuilPreferencesPanel extends BuAbstractPreferencesPanel {
  Hashtable optionsStr_;
  BuGridLayout loSeuil_;
  JPanel pnSeuilCalq_;
  AbstractBorder boSeuilCalq_;
  BuGridLayout loSeuilCalq_;
  JLabel lbSeuilCalqInter_;
  JComboBox liSeuilCalqInter_;
  SeuilPreferences options_;
  SeuilImplementation seuil_;
  public String getTitle() {
    return "Seuil";
  }
  // Constructeur
  public SeuilPreferencesPanel(final SeuilImplementation _seuil) {
    super();
    options_= SeuilPreferences.SEUIL;
    seuil_= _seuil;
    optionsStr_= new Hashtable();
    // Panneau Seuil
    loSeuil_= new BuGridLayout();
    loSeuil_.setColumns(1);
    this.setLayout(loSeuil_);
    updateComponents();
  }
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return true;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(seuil_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {}
  private void updateComponents() {}
}
