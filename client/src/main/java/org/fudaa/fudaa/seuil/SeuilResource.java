/*
 * @file         SeuilResource.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:10:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.seuil;
//import org.fudaa.fudaa.commun.*;
import com.memoire.bu.BuResource;
//import org.fudaa.ebli.bu.*;
/**
 * Le gestionnaire de ressources de Seuil.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:10:26 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class SeuilResource extends BuResource {
  public final static String SEUIL01= "01";
  public final static String SEUILOUT= "OUT";
  public final static SeuilResource SEUIL= new SeuilResource();
}
