# fichier

CREER_ACTION
Cliquer sur "Parcourir"
pour choisir le fichier
� cr�er

OUVRIR_ACTION
Cherche une �tude d�j�
cr��e sous le nom
***.lid

IMPORTLIDO_ACTION
Importe les donn�es
***.PRO, ***.CAL,
***.CLM, ***.RZO,
***.SNG par le biais du
fichier ***.fic
Sp�cifique aux
connaisseurs

EXPORTLIDO_ACTION
Exporte l'�tude au
format ***.lid sous la
forme de fichiers
***.PRO, ***.CAL,
***.CLM, ***.RZO,
***.SNG

# LIDO****************

# param�tres du r�seau

REZO:PROFIL_ACTION
Permet de cr�er,
modifier ou visualiser
les profils

TABLEAUPROFILS:CREER_ACTION
Entrez l'abscisse
curviligne et ses
coefficients de rugosit�
puis l'�diter pour le
constituer

TABLEAUPROFILS:EDITER_ACTION
Visualisation du profil
avec possibilit� de le
modifier, ou de le
cr�er. D�finition des
limites de berges et de
zones de stockage

REZO:BIEF_ACTION
Permet de d�finir des
biefs avec la
possibilit� de
visualiser le profil en
long des biefs
s�lectionn�s

TABLEAUBIEFS:CREER_ACTION
Entrez les abscisses
curvilignes de d�but et
de fin du bief.
Attention: entre deux
biefs cons�cutifs,
l'�cart doit �tre faible
(1m maximum)

LIDO_PARAMETRES DU
RESEAU_BIEFS_SCINDER
Permet de "couper" le
bief s�lectionn� en deux
biefs

TABLEAUBIEFS:VOIR_ACTION
Permet de visualiser le
profil en long des biefs
s�lectionn�s avec les
rives gauche ou droite
et la ligne d'eau
initiale pour un calcul
pr�alable en permanent

REZO:NOEUD_ACTION
Permet de cr�er,
modifier ou supprimer
les relations entre les
biefs

REZO:LIMITE_ACTION
Permet de d�finir les
conditions hydrauliques
aux extr�mit�s libres
des biefs. Rappel des
types de lois:
1=HYDROGRAMME
2=LIMNIGRAMME
3=COURBE DE TARAGE

TABLEAULIMITES:CREER_ACTION
Entrez l'extr�mit� du
bief relative � cette
condition limite ainsi
que son type:
1=HYDROGRAMME
2=LIMNIGRAMME
3=COURBE DE TARAGE

TABLEAULIMITES:EDITER_ACTION
D�finir la condition
limite point par point
dans le tableau. -
Repr�sentation graphique
simultan�e -

REZO:APPORT_ACTION
Permet de d�finir les
lois d'apports et de
soutirages sur le r�seau
�tudi�

TABLEAUAPPORTS:CREER_ACTION
Donnez l'abscisse
curviligne de
l'apport/soutirage ainsi
que son type de loi :
1=HYDROGRAMME
2=LIMNIGRAMME
3=COURBE DE TARAGE

TABLEAUAPPORTS:EDITER_ACTION
D�finir la courbe de la
loi point par point dans
le tableau et donner son
coefficient
multiplicateur sachant
que (+ : apport) et (- :
soutirage)

REZO:PERTE_ACTION
Permet de d�finir les
pertes de charge
singuli�res d�es � des
singularit�s sp�cifiques
(tel qu'un seuil)

TABLEAUPERTES:CREER_ACTION
Donner l'abscisse
curviligne de la perte
de charge ainsi que son
coefficient de perte de
charge

REZO:SINGULARITE_ACTION
Permet de d�finir
plusieurs types de
singularit�s applicables
aux biefs

TABLEAUSINGULARITES:CREER_ACTION
-------------------
A FINIR
-------------------

REZO:VISUALISER_ACTION
Permet la visualisation
� l'�cran du r�seau
complet

# param�tres g�n�raux

GEN:PARAMCALCUL_ACTION
Choisir le type de
rivi�re correspondant �
l'�tude, le r�gime
d'�coulement et la
composition du lit.
Entrer aussi les
abscisses curvilignes
extr�mes

GEN:SECTIONCALCUL_ACTION
Permet d'affiner le
calcul de la ligne d'eau
en rajoutant des
sections sur lesquels se
base le code. Choisir
entre les 4 modes
sachant que le 2�me est
pr�conis�.

LIDO_PARAMETRES
GENERAUX_SECTIONS DE CALCUL_EDITER_CHOIX=2
Entrer l'abscisse
curviligne du profil
amont et aval ainsi que
le nombre de sections de
calcul � mettre entre.

GEN:PLANIMETRAGE_ACTION
Permet de d�finir 5 pas
de hauteur d'eau
diff�rents. Entrer entre
quels profils on a un
pas sensiblement
constant et cliquer sur
"Voir"

LIDO_PARAMETRES
GENERAUX_PLANIMETRAGE_VOIR
Permet de voir la
hauteur d'eau minimale
et maximale entre les
profils d�finis Entrer
une hauteur d'eau
maximale suppos�e entre
les profils d�finis
pr�c�dement et valider
si cela convient.

GEN:VARTEMP_ACTION
Permet de d�finir le
temps de d�but et de fin
du r�gime transitoire.
Le pas de temps permet
d'obtenir un r�sultat
tous les pas de temps en
partant du temps
initial.

# resultats
