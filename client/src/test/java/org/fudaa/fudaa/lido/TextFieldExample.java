/*
 *  @file         TestTextField.java
 *  @creation     19 juin 2003
 *  @modification $Date: 2006-10-19 14:15:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.lido;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.fudaa.lido.ihmhelper.gestion.LidoParamsHelper;
/**
 * @author deniger
 * @version $Id: TestTextField.java,v 1.4 2006-10-19 14:15:26 deniger Exp $
 */
public final class TextFieldExample {
  private TextFieldExample() {
    super();
  }
  public static void main(final String[] _args) {
    final JFrame frame= new JFrame();
    frame.addWindowListener(new WindowAdapter() {
      public void windowClosing(final WindowEvent _e) {
        System.exit(0);
      }
    });
    final JPanel p= new JPanel(new BuGridLayout(2, 5, 5, false, false, false, false));
    p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    final BuTextField butxt= BuTextField.createDoubleField();
    butxt.setText("123,456");
    butxt.setColumns(8);
    butxt.setDisplayFormat(LidoParamsHelper.NUMBER_FORMAT_DOUBLE);
    p.add(new JLabel("bu"));
    p.add(butxt);
    final JTextField jtxt= new JTextField() {
      protected void processFocusEvent(FocusEvent _evt) {
        System.out.println("");
        super.processFocusEvent(_evt);
      }
    };
    jtxt.setText("123,456");
    jtxt.setColumns(8);
    p.add(new JLabel("jt"));
    p.add(jtxt);
    frame.setContentPane(p);
    frame.pack();
    frame.setSize(800, 800);
    frame.setVisible(true);
  }
}
