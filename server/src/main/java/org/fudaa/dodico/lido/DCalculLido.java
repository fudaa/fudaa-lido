/**
 * @file         DCalculLido.java
 * @creation     1998-11-19
 * @modification $Date: 2006-09-19 14:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido;

import java.io.File;

import com.memoire.fu.FuLib;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.ICalculLidoOperations;
import org.fudaa.dodico.corba.lido.IParametresLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.IResultatsLido;
import org.fudaa.dodico.corba.lido.IResultatsLidoHelper;
import org.fudaa.dodico.corba.lido.SResultatsERN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;

/**
 * Classe Calcul de Lido.
 * 
 * @version $Revision: 1.13 $ $Date: 2006-09-19 14:43:26 $ by $Author: deniger $
 * @author Mickael Rubens , Axel von Arnim
 */
public class DCalculLido extends DCalcul implements ICalculLido, ICalculLidoOperations {
  private SProgression strOperation_;

  public DCalculLido() {
    super();
    strOperation_ = new SProgression("", 0);
    setFichiersExtensions(new String[] { ".60", ".cal", ".lig", ".clm", ".pro", ".rzo", ".sng", ".ern", ".rsn" });
  }

  public String description() {
    return "Lido, serveur de calcul de ligne d'eau: " + super.description();
  }

  public SProgression progression() {
    return strOperation_;
  }

  private void setProgression(String _op, int _pc) {
    strOperation_.operation = _op;
    if (strOperation_.operation == null) strOperation_.operation = "";
    strOperation_.pourcentage = _pc;
  }

  public void calcul(IConnexion _c) {
    if (!verifieConnexion(_c)) return;
    IParametresLido params = IParametresLidoHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    IResultatsLido results = IResultatsLidoHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    log(_c, "lancement du calcul");
    int noEtude = _c.numero();
    SResultatsRSN rsn = null;
    String path = cheminServeur();
    String nomEtude = "lido" + noEtude;
    params.parametresFIC(DParametresLido.creeParametresFIC(noEtude));
    setProgression("Ecriture des fichiers param�tres", 0);
    System.out.println("Ecriture des fichiers parametres");
    // File fic60=getFichier(c, ".60");
    File ficCAL = getFichier(_c, ".cal");
    File ficLIG = getFichier(_c, ".lig");
    File ficCLM = getFichier(_c, ".clm");
    File ficPRO = getFichier(_c, ".pro");
    File ficRZO = getFichier(_c, ".rzo");
    File ficSNG = getFichier(_c, ".sng");
    File ficERN = getFichier(_c, ".ern");
    File ficRSN = getFichier(_c, ".rsn");
    try {
      setProgression("Ecriture des fichiers param�tres CAL", 2);
      DParametresLido.ecritParametresCAL(ficCAL, params.parametresCAL());
      setProgression("Ecriture des fichiers param�tres LIG", 5);
      DParametresLido.ecritParametresLIG(ficLIG, params.parametresLIG());
      setProgression("Ecriture des fichiers param�tres CLM", 8);
      DParametresLido.ecritParametresCLM(ficCLM, params.parametresCLM());
      setProgression("Ecriture des fichiers param�tres PRO", 10);
      DParametresLido.ecritParametresPRO(ficPRO, params.parametresPRO(), params.parametresRZO());
      setProgression("Ecriture des fichiers param�tres RZO", 15);
      DParametresLido.ecritParametresRZO(ficRZO, params.parametresRZO());
      setProgression("Ecriture des fichiers param�tres SNG", 17);
      DParametresLido.ecritParametresSNG(ficSNG, params.parametresSNG());
      setProgression("Calcul", 20);
      System.out.println("Appel de l'executable lido");
      String[] cmd;
      String string = CtuluLibString.ESPACE;
      if (FuLib.isWindows()) {
        cmd = new String[4];
        cmd[0] = path + "lido-win.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1] = path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2] = path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[1] = "fake_cmd";
          cmd[2] = path;
        }
        cmd[3] = nomEtude;
        System.out.println(cmd[0] + string + cmd[1] + string + cmd[2] + string + cmd[3]);
      } else {
        cmd = new String[3];
        cmd[0] = path + "lido.sh";
        cmd[1] = path;
        cmd[2] = nomEtude;
        System.out.println(cmd[0] + string + cmd[1] + string + cmd[2]);
      }
      CExec ex = new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      setProgression("Calcul", 25);
      ex.exec();
      System.out.println("Fin du calcul");
      System.out.println("Les resultats sont dans " + nomEtude + ".rsn");
      setProgression("Lecture des fichiers resultats ERN", 90);
      System.out.println("Lecture des resultats");
      if (ficERN.length() > 524288) {
        results.resultatsERN(new SResultatsERN("Les informations ERN sont trop volumineuses\n" + "( > 512 Ko).\n"
            + "Ouvrez directement le fichier :\n" + ficERN.getPath() + "\n" + "pour les voir.\n"));
      } else {
        try {
          results.resultatsERN(DResultatsLido.litResultatsERN(ficERN));
        } catch (Exception e) {
          System.err.println(e);
        }
      }
      setProgression("Lecture des fichiers resultats RSN", 100);
      // results_.resultatsLIG(CResultatsLido.litResultatsLIG(ficLIG));
      // strOperation_="Lecture des fichiers resultats:90";
      rsn = new SResultatsRSN();
      DResultatsLido.litResultatsRSN(ficRSN, rsn, params.parametresRZO().nbBief,
          params.parametresCAL().temporel.pas2TImp, params.parametresCAL().titreCAL[0], progression());
      results.resultatsRSN(rsn);
      setProgression("Lecture des fichiers resultats LIG", 95);
      if ("P".equalsIgnoreCase(params.parametresCAL().genCal.regime.trim())) results.resultatsLIG(DResultatsLido
          .litResultatsLIG(ficLIG));
      System.out.println("Lecture des resultats terminee.");
      setProgression(null, 0);
      log(_c, "calcul termin�");
    } catch (Exception ex) {
      results.resultatsRSN(rsn);
      setProgression(null, 0);
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
