/**
 * @file         DResultatsLido.java
 * @creation     1998-10-21
 * @modification $Date: 2006-09-19 14:43:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido;

import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Calendar;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranLib;

import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.lido.*;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Les resultats Lido.
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 14:43:26 $ by $Author: deniger $
 * @author Mickael Rubens , Axel von Arnim
 */
public class DResultatsLido extends DResultats implements IResultatsLido, IResultatsLidoOperations {
  private static boolean DEBUG = "yes".equals(System.getProperty("fudaa.debug"));
  private SResultatsRSN resultsRSN_;
  private SParametresLIG resultsLIG_;
  private SResultatsERN resultsERN_;

  public DResultatsLido() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DResultatsLido();
  }

  public SResultatsRSN resultatsRSN() {
    return resultsRSN_;
  }

  public void resultatsRSN(SResultatsRSN _p) {
    resultsRSN_ = _p;
  }

  public SParametresLIG resultatsLIG() {
    return resultsLIG_;
  }

  public void resultatsLIG(SParametresLIG _p) {
    resultsLIG_ = _p;
  }

  public SResultatsERN resultatsERN() {
    return resultsERN_;
  }

  public void resultatsERN(SResultatsERN _p) {
    resultsERN_ = _p;
  }

  private static void setProgression(SProgression _progres, int _pc) {
    _progres.pourcentage = _pc;
  }

  // ******************* .LIG ***********************
  // ATTENTION : on identifie les resultats et les parametres LIG
  // (c'est la meme structure)
  public static SParametresLIG litResultatsLIG(File _fichier) throws IOException {
    return DParametresLido.litParametresLIG(_fichier);
  }

  /*
   * public static SResultatsLIG litResultatsLIG(File fichier) { SResultatsLIG resultl = new SResultatsLIG(); try{ //
   * initialisation par defaut pour permettre aux structures, et champs d'etres serialisees
   * System.out.println("Recuperation des donnees du fichier STO, utiles pour la construction du fichier LIG");
   * resultl.delimitBief = new SResultatsLimBiefLIG[0]; resultl.x = new double[0]; resultl.z = new double[0]; resultl.q =
   * new double[0]; resultl.stricklerMineur = new double[0]; resultl.stricklerMajeur = new double[0]; //Nous lisons le
   * fichier STO qui contient les donnees pour le fichier LIG FortranReader fsto = new FortranReader(new
   * FileReader(fichier)); if (fsto.ready()) { int fmt1 [] = new int [] {72}; int fmt2 [] = new int [] {8,5,8,5}; int
   * fmt3 []; int fmt4 [] = new int [] {5}; int fmt5 [] = new int [] {10,10,10,10,10}; resultl.titreLIG = new String[3];
   * System.out.println("TITRE"); for(int i=0; i<3; i++) { resultl.titreLIG[i] = new String(); fsto.readFields(fmt1);
   * resultl.titreLIG[i] = fsto.stringField(0); } fsto.readFields(fmt2); resultl.nbSections = fsto.intField(1);
   * resultl.nbBief = fsto.intField(3); // Lecture du debut et de la fin de section de chaque biefs int cmpt = 11;
   * resultl.delimitBief = new SResultatsLimBiefLIG[resultl.nbBief]; for(int j=0; j<resultl.nbBief; j++) { if(cmpt >
   * 10) { if(resultl.nbBief-j < 5) { fmt3 = new int[2*(resultl.nbBief-j)+1]; fmt3[0] = 8; for(int k=1; k<=2*(resultl.nbBief-j);
   * k++) { fmt3 [k] = 5; } } else { fmt3 = new int [] {8,5,5,5,5,5,5,5,5,5,5}; } fsto.readFields(fmt3); cmpt = 1; }
   * resultl.delimitBief[j] = new SResultatsLimBiefLIG(); resultl.delimitBief[j].sectionDebut = fsto.intField(cmpt++);
   * resultl.delimitBief[j].sectionFin = fsto.intField(cmpt++); } fsto.readFields(); cmpt = 5; resultl.x = new
   * double[resultl.nbSections]; for(int j=0; j<resultl.nbSections; j++) { if( cmpt>4 ) { fsto.readFields(); cmpt = 0; }
   * resultl.x[j] =fsto.doubleField(cmpt++); } fsto.readFields(); cmpt = 5; for(int j=0; j<resultl.nbSections; j++) {
   * if(cmpt > 4) { fsto.readFields(); cmpt = 0; } cmpt++; } fsto.readFields(); cmpt = 5; resultl.z = new
   * double[resultl.nbSections]; for(int j=0; j<resultl.nbSections; j++) { if(cmpt > 4) { fsto.readFields(); cmpt = 0; }
   * resultl.z[j] = fsto.doubleField(cmpt++); } fsto.readFields(); cmpt = 5; resultl.q = new double[resultl.nbSections];
   * for(int j=0; j<resultl.nbSections; j++) { if(cmpt > 4) { fsto.readFields(); cmpt = 0; } resultl.q[j] =
   * fsto.doubleField(cmpt++); } fsto.readFields(); cmpt = 5; for(int j=0; j<resultl.nbSections; j++) { if(cmpt > 4) {
   * fsto.readFields(); cmpt = 0; } cmpt++; } fsto.readFields(); cmpt = 5; for(int j=0; j<resultl.nbSections; j++) {
   * if(cmpt > 4) { fsto.readFields(); cmpt = 0; } cmpt++; } fsto.readFields(); cmpt = 5; for(int j=0; j<resultl.nbSections;
   * j++) { if(cmpt > 4) { fsto.readFields(); cmpt = 0; } cmpt++; } fsto.readFields(); cmpt = 5; for(int j=0; j<resultl.nbSections;
   * j++) { if(cmpt > 4) { fsto.readFields(); cmpt = 0; } cmpt++; } fsto.readFields(); cmpt = 5; resultl.stricklerMineur =
   * new double[resultl.nbSections]; for(int j=0; j<resultl.nbSections; j++) { if( cmpt>4 ) { fsto.readFields(); cmpt =
   * 0; } resultl.stricklerMineur[j] = fsto.doubleField(cmpt++); } fsto.readFields(); cmpt = 5; resultl.stricklerMajeur =
   * new double[resultl.nbSections]; for(int j=0; j<resultl.nbSections; j++) { if( cmpt>4 ) { fsto.readFields(); cmpt =
   * 0; } resultl.stricklerMajeur[j] = fsto.doubleField(cmpt++); } } else { System.out.println("Fichier
   * "+fichier.getName()+" vide ou inexistant!"); } } catch( Exception ex ) { ex.printStackTrace();
   * System.out.println("IT: "+ ex); }; return resultl; } public static void ecritResultatsLIG(File fichier,
   * SResultatsLIG results, int nChoix) { try { FortranWriter flig = new FortranWriter(new FileWriter(fichier));
   * System.out.println("Creation du fichier "+fichier.getName()); int fmt1 [] = new int [] {72}; int fmt2 [] = new int []
   * {8,5,8,5}; int fmt3 []; int fmt4 [] = new int [] {5}; int fmt5 []; for(int i=0; i<3; i++) {
   * flig.stringField(0,results.titreLIG[i]); flig.writeFields(fmt1); } flig.stringField(0," IMAX =");
   * flig.intField(1,results.nbSections); flig.stringField(2," NBBIEF="); flig.intField(3,results.nbBief);
   * flig.writeFields(fmt2); // ecriture du debut et de la fin de section de chaque biefs int cmpt = 1; for(int i=0; i<results.nbBief;
   * i++) { flig.intField(cmpt++, results.delimitBief[i].sectionDebut); flig.intField(cmpt++,
   * results.delimitBief[i].sectionFin); if(cmpt > 10) { fmt3 = new int [] {8,5,5,5,5,5,5,5,5,5,5};
   * flig.writeFields(fmt3); cmpt = 1; } else { if(results.nbBief == i+1) { fmt3 = new int[cmpt]; fmt3[0] = 8; for(int
   * j=1; j<cmpt; j++) { fmt3 [j] = 5; } flig.writeFields(fmt3); } } flig.stringField(0,"I1,I2 = "); }
   * flig.stringField(0," X "); flig.writeFields(fmt4); //int cmpt = 0; for(int i=0; i<results.nbSections; i++) {
   * flig.doubleField(cmpt++, results.x[i]); if(cmpt > 4) { fmt5 = new int [] {10,10,10,10,10}; flig.writeFields(fmt5);
   * cmpt = 0; } else { if(results.nbSections == i+1) { fmt5 = new int[cmpt]; for(int j=0; j<cmpt; j++) { fmt5 [j] =
   * 10; } flig.writeFields(fmt5); } } } flig.stringField(0," Z "); flig.writeFields(fmt4); cmpt = 0; for(int i=0; i<results.nbSections;
   * i++) { flig.doubleField(cmpt++, results.z[i]); if(cmpt > 4) { fmt5 = new int [] {10,10,10,10,10};
   * flig.writeFields(fmt5); cmpt = 0; } else { if(results.nbSections == i+1) { fmt5 = new int[cmpt]; for(int j=0; j<cmpt;
   * j++) { fmt5 [j] = 10; } flig.writeFields(fmt5); } } } flig.stringField(0," Q "); flig.writeFields(fmt4); cmpt = 0;
   * for(int i=0; i<results.nbSections; i++) { flig.doubleField(cmpt++, results.q[i]); if( cmpt>4 ) { fmt5 = new int []
   * {10,10,10,10,10}; flig.writeFields(fmt5); cmpt = 0; } else { if(results.nbSections == i+1) { fmt5 = new int[cmpt];
   * for(int j=0; j<cmpt; j++) { fmt5 [j] = 10; } flig.writeFields(fmt5); } } } // if(nChoix == 4) {
   * flig.stringField(0," stricklerMineur "); flig.writeFields(fmt4); cmpt = 0; for(int i=0; i<results.nbSections; i++) {
   * flig.doubleField(cmpt++, results.stricklerMineur[i]); if(cmpt > 4) { fmt5 = new int [] {10,10,10,10,10};
   * flig.writeFields(fmt5); cmpt = 0; } else { if(results.nbSections == i+1) { fmt5 = new int[cmpt]; for(int j=0; j<cmpt;
   * j++) { fmt5 [j] = 10; } flig.writeFields(fmt5); } } } flig.stringField(0," stricklerMajeur ");
   * flig.writeFields(fmt4); cmpt = 0; for(int i=0; i<results.nbSections; i++) { flig.doubleField(cmpt++,
   * results.stricklerMajeur[i]); if(cmpt > 4) { fmt5 = new int [] {10,10,10,10,10}; flig.writeFields(fmt5); cmpt = 0; }
   * else { if(results.nbSections == i+1) { fmt5 = new int[cmpt]; for(int j=0; j<cmpt; j++) { fmt5 [j] = 10; }
   * flig.writeFields(fmt5); } } } // } flig.stringField(0," FIN "); flig.writeFields(fmt4); flig.close(); }
   * catch(Exception ex) { ex.printStackTrace(); System.err.println("IT: "+ ex); } }
   */
  // ************************ STA ***********************************
  // Methode appelee lors d'un calcul en mode non permanent!
  public static SResultatsSTA litResultatsSTA(File _fichier, int _nbSections, int _nbBiefs, int _impMax, double _tMax,
      SParametresLimBiefLIG[] _biefLim) {
    SResultatsSTA results = new SResultatsSTA();
    System.out.println("Lecture de " + _fichier.getName());
    try {
      // initialisation par defaut pour permettre aux structures, et champs d'etres serialisees
      results.tempBief = new SResultatsDonBiefTempsSTA[0];
      FortranReader fsta = new FortranReader(new FileReader(_fichier));
      int t = -1; // indice indiquant le numero d'impression
      if (fsta.ready()) {
        int[] fmt = new int[] { 36, 5, 5, 10 };
        results.tempBief = new SResultatsDonBiefTempsSTA[_impMax];
        do {
          t++;
          results.tempBief[t] = new SResultatsDonBiefTempsSTA();
          fsta.readFields();
          fsta.readFields(fmt);
          results.tempBief[t].tempImp = fsta.doubleField(3);
          int cmpt;
          fsta.readFields();
          fsta.readFields();
          cmpt = 0;
          while (cmpt < _nbSections) {
            results.tempBief[t].blocBief = new SResultatsDonBiefBlocSTA[_nbBiefs];
            for (int i = 0; i < _nbBiefs; i++) {
              results.tempBief[t].blocBief[i] = new SResultatsDonBiefBlocSTA();
              fsta.readFields();
              fsta.readFields();
              results.tempBief[t].blocBief[i].ligneBief = new SResultatsDonBiefLigneSTA[_biefLim[i].sectionFin
                  - _biefLim[i].sectionDebut + 1];
              // .cmpt];
              for (int j = 0; j < (_biefLim[i].sectionFin - _biefLim[i].sectionDebut + 1); j++) {
                // cmpt); j++) {
                results.tempBief[t].blocBief[i].ligneBief[j] = new SResultatsDonBiefLigneSTA();
                fsta.readFields();
                results.tempBief[t].blocBief[i].ligneBief[j].numSect = fsta.intField(0);
                results.tempBief[t].blocBief[i].ligneBief[j].abs = fsta.doubleField(1);
                results.tempBief[t].blocBief[i].ligneBief[j].lignePiezFonds = fsta.doubleField(2);
                results.tempBief[t].blocBief[i].ligneBief[j].coteLig = fsta.doubleField(3);
                results.tempBief[t].blocBief[i].ligneBief[j].debitTotal = fsta.doubleField(4);
                results.tempBief[t].blocBief[i].ligneBief[j].debitLitMin = fsta.doubleField(5);
                results.tempBief[t].blocBief[i].ligneBief[j].debitLitMaj = fsta.doubleField(6);
                results.tempBief[t].blocBief[i].ligneBief[j].vitMoyLitMin = fsta.doubleField(7);
                results.tempBief[t].blocBief[i].ligneBief[j].vitMoyLitMaj = fsta.doubleField(8);
                results.tempBief[t].blocBief[i].ligneBief[j].coefRugLitMin = fsta.doubleField(9);
                results.tempBief[t].blocBief[i].ligneBief[j].coefRugLitMaj = fsta.doubleField(10);
                results.tempBief[t].blocBief[i].ligneBief[j].largMirLitMin = fsta.doubleField(11);
                results.tempBief[t].blocBief[i].ligneBief[j].largMirLitMaj = fsta.doubleField(12);
                results.tempBief[t].blocBief[i].ligneBief[j].periMouilLitMin = fsta.doubleField(13);
                results.tempBief[t].blocBief[i].ligneBief[j].periMouilLitMaj = fsta.doubleField(14);
                results.tempBief[t].blocBief[i].ligneBief[j].surfMLitMin = fsta.doubleField(15);
                results.tempBief[t].blocBief[i].ligneBief[j].surfMLitMaj = fsta.doubleField(16);
                results.tempBief[t].blocBief[i].ligneBief[j].rayHydLitMin = fsta.doubleField(17);
                results.tempBief[t].blocBief[i].ligneBief[j].rayHydLitMaj = fsta.doubleField(18);
                results.tempBief[t].blocBief[i].ligneBief[j].valFroud = fsta.doubleField(19);
                cmpt++;
              }
            }
          }
        } while ((t < results.tempBief.length - 1) && (results.tempBief[t].tempImp < _tMax));
      }
      fsta.close();
    } catch (Exception ex) {
      ex.printStackTrace();
      System.out.println("IT: " + ex);
    }
    return results;
  }

  // ************************ RSN ***********************************
  public static SResultatsRSN litResultatsRSN(File _fichier, SResultatsRSN _results, int _nbBiefs, double _pasImpression,
      String _titreCAL, SProgression _progres) throws IOException {
    SResultatsRSN results=_results;
    if (results == null) results = new SResultatsRSN();
    System.out.println("Lecture du fichier " + _fichier.getName());
    // initialisation par defaut pour permettre aux structures, et champs d'etres serialisees
    results.pasTemps = new SResultatsTempRSN[0];
    FortranReader f = null;
    boolean progression = (_progres != null);
    if (progression) setProgression(_progres, 0);
    int maxPas = 0;
    try {
      f = new FortranReader(new FileReader(_fichier));
      if (f.ready()) {
        int[] fmtCal = new int[] { 33, 39 };
        int[] fmtPas = new int[] { 36, 5, 5, 9 };
        int[] fmtDt = new int[] { 6, 6, 10, 6 };
        int[] fmtJour = new int[] { 8, 4, 9, 3, 10, 3, 11, 3 };
        int[] fmtBief = new int[] { 13, 2, 10, 5, 5, 5 };
        int[] fmtBiefPied = new int[] { 39, 12 };
        // lecture des donnees CAL
        RSNJumpToCalSection(f, _titreCAL);
        // saute a la section des donnees generales
        // "mange" le titre aussi
        f.readLine(); // ligne de pointilles
        f.readLine(); // ligne vide
        f.readFields(fmtCal); // code
        String code = f.stringField(1).trim().toUpperCase();
        f.readFields(fmtCal); // regime
        boolean permanent = !f.stringField(1).trim().equalsIgnoreCase("NP");
        if (DEBUG) System.err.println(permanent ? "regime permanent" : "regime non permanent");
        RSNJumpToTempSection(f); // on saute a la section des params temporels
        f.readFields(fmtCal); // temps
        double t = f.doubleField(1);
        if (DEBUG) System.err.println("t=" + t);
        f.readFields(fmtCal);
        double tMax = f.doubleField(1);
        if (DEBUG) System.err.println("tMax=" + tMax);
        f.readFields(fmtCal);
        double dt = f.doubleField(1);
        if (DEBUG) System.err.println("dt=" + dt);
        f.readFields(fmtCal);
        int np = f.intField(1);
        if (DEBUG) System.err.println("np=" + np);
        int nbPasT = (int) ((tMax - t) / _pasImpression);
        // HACK : en Permanent, le nb de pas a l'air d'etre incremente de 1!?
        if (permanent) nbPasT++;
        if (DEBUG) System.err.println("nbPas=" + nbPasT);
        results.pasTemps = new SResultatsTempRSN[nbPasT];
        for (int pas = 0; pas < nbPasT; pas++) {
          if (progression) setProgression(_progres, (int) (100 * ((double) pas) / nbPasT));
          if (DEBUG) System.err.println("pas " + pas);
          // saute a la prochaine section de resultats en recuperant le texte lu
          String info = RSNJumpToResSection(f);
          results.pasTemps[pas] = new SResultatsTempRSN();
          results.pasTemps[pas].info = info;
          f.readFields(fmtPas); // ligne de np et t
          results.pasTemps[pas].np = f.intField(1);
          results.pasTemps[pas].t = f.intField(3);
          f.readLine(); // ligne de pointilles
          f.readLine(); // ligne vide
          if (permanent) {
            results.pasTemps[pas].dt = 0.;
            results.pasTemps[pas].dtlevy = 0.;
            results.pasTemps[pas].jour = 0;
            results.pasTemps[pas].heure = 0;
            results.pasTemps[pas].minute = 0;
            results.pasTemps[pas].seconde = 0;
          } else {
            f.readFields(fmtDt); // ligne de dt et dtlevy
            results.pasTemps[pas].dt = f.doubleField(1);
            results.pasTemps[pas].dtlevy = f.doubleField(3);
            f.readFields(fmtJour); // ligne de jour, heure, minute, seconde
            results.pasTemps[pas].jour = f.intField(1);
            results.pasTemps[pas].heure = f.intField(3);
            results.pasTemps[pas].minute = f.intField(5);
            results.pasTemps[pas].seconde = f.intField(7);
            f.readLine(); // ligne vide
          }
          results.pasTemps[pas].ligBief = new SResultatsBiefRSN[_nbBiefs];
          for (int b = 0; b < _nbBiefs; b++) {
            results.pasTemps[pas].ligBief[b] = new SResultatsBiefRSN();
            if (!"LIDO".equals(code)) {
              // f.readLine(); // ligne vide
              String info2 = RSNJumpToBiefSection(f);
              if (info2 != null) {
                String string = CtuluLibString.LINE_SEP_SIMPLE;
                results.pasTemps[pas].info += string + info2;
              }
              f.readFields(fmtBief); // ligne de numero, i1, i2
              results.pasTemps[pas].ligBief[b].numero = f.intField(1) - 1;
              results.pasTemps[pas].ligBief[b].i1 = f.intField(3);
              results.pasTemps[pas].ligBief[b].i2 = f.intField(5);
              f.readLine(); // ligne de pointilles
              f.readLine(); // ligne vide
            }
            f.readLine(); // en_tete du tableau de resultats
            f.readLine(); // ligne vide
            int nbLignes = results.pasTemps[pas].ligBief[b].i2 - results.pasTemps[pas].ligBief[b].i1 + 1;
            if (DEBUG) System.err.println("nbLignes=" + nbLignes + " position " + f.getLineNumber());
            results.pasTemps[pas].ligBief[b].ligne = new SResultatsBiefLigneRSN[nbLignes];
            for (int l = 0; l < nbLignes; l++) {
              results.pasTemps[pas].ligBief[b].ligne[l] = new SResultatsBiefLigneRSN();
              f.readFields(); // une ligne
              // HACK temporaire pour CODE1D.EXE qui met des *** a partir de
              // I=999
              if (f.stringField(0).indexOf('*') > -1) {
                results.pasTemps[pas].ligBief[b].ligne[l].i = l > 0 ? (results.pasTemps[pas].ligBief[b].ligne[l - 1].i + 1)
                    : b > 0 ? (results.pasTemps[pas].ligBief[b - 1].ligne[results.pasTemps[pas].ligBief[b - 1].ligne.length - 1].i + 1)
                        : 0;
              } else
                results.pasTemps[pas].ligBief[b].ligne[l].i = f.intField(0);
              results.pasTemps[pas].ligBief[b].ligne[l].x = f.doubleField(1);
              results.pasTemps[pas].ligBief[b].ligne[l].z = f.doubleField(2);
              results.pasTemps[pas].ligBief[b].ligne[l].y = f.doubleField(3);
              results.pasTemps[pas].ligBief[b].ligne[l].s1 = f.doubleField(4);
              results.pasTemps[pas].ligBief[b].ligne[l].s2 = f.doubleField(5);
              results.pasTemps[pas].ligBief[b].ligne[l].r1 = f.doubleField(6);
              results.pasTemps[pas].ligBief[b].ligne[l].r2 = f.doubleField(7);
              results.pasTemps[pas].ligBief[b].ligne[l].b1 = f.doubleField(8);
              results.pasTemps[pas].ligBief[b].ligne[l].vmin = f.doubleField(9);
              results.pasTemps[pas].ligBief[b].ligne[l].vmaj = f.doubleField(10);
              results.pasTemps[pas].ligBief[b].ligne[l].q = f.doubleField(11);
              results.pasTemps[pas].ligBief[b].ligne[l].froude = f.doubleField(12);
              results.pasTemps[pas].ligBief[b].ligne[l].qmin = f.doubleField(13);
              results.pasTemps[pas].ligBief[b].ligne[l].qmaj = f.doubleField(14);
              results.pasTemps[pas].ligBief[b].ligne[l].charge = f.doubleField(15);
            }
            f.readLine(); // ligne vide
            f.readFields(fmtBiefPied); // ligne de volume lit actif
            results.pasTemps[pas].ligBief[b].volum = f.doubleField(1);
            f.readFields(fmtBiefPied); // ligne de volume stocke
            results.pasTemps[pas].ligBief[b].volums = f.doubleField(1);
            f.readFields(fmtBiefPied); // ligne de volume total
            results.pasTemps[pas].ligBief[b].vbief = f.doubleField(1);
            if (permanent) {
              results.pasTemps[pas].ligBief[b].vappo = 0.;
              results.pasTemps[pas].ligBief[b].vperdu = 0.;
            } else {
              f.readFields(); // ligne de titre
              f.readFields(fmtBiefPied); // ligne de volume apport
              results.pasTemps[pas].ligBief[b].vappo = f.doubleField(1);
              f.readFields(); // ligne de titre
              f.readFields(fmtBiefPied); // ligne de volume perdu
              results.pasTemps[pas].ligBief[b].vperdu = f.doubleField(1);
            }
            f.readLine(); // ligne vide
          }
          maxPas++;
        }
      }
      f.close();
    } catch (Exception e) {
      SResultatsTempRSN[] tmp = results.pasTemps;
      results.pasTemps = new SResultatsTempRSN[Math.min(tmp.length, maxPas + 1)];
      for (int i = 0; i < results.pasTemps.length; i++)
        results.pasTemps[i] = tmp[i];
      e.printStackTrace();
      if (f == null) throw new IOException(e.getMessage());
      throw new IOException(e.getMessage() + ": ligne " + f.getLineNumber());
    }
    return results;
  }

  private static void RSNJumpToCalSection(FortranReader f, String title) throws IOException {
    String line = "";
    try {
      while (!line.startsWith(title)) {
        line = f.readLine();
        // if( line==null ) throw new EOFException("parametres generaux introuvables");
      }
    } catch (EOFException e) {
      throw new EOFException("parametres generaux introuvables");
    }
    if (DEBUG) System.err.println("parametres cal trouves en ligne " + f.getLineNumber());
  }

  private static void RSNJumpToTempSection(FortranReader f) throws IOException {
    String line = "";
    try {
      while (!line.startsWith(" PARAMETRES EN TEMPS")) {
        f.mark(128);
        line = f.readLine();
        // if( line==null ) throw new EOFException("parametres temporels introuvables");
      }
    } catch (EOFException e) {
      throw new EOFException("parametres temporels introuvables");
    }
    if (DEBUG) System.err.println("parametres temporels trouves en ligne " + f.getLineNumber());
    f.reset();
  }

  private static String RSNJumpToResSection(FortranReader f) throws IOException {
    // String info="";
    StringBuffer info = new StringBuffer("");
    String line = "";
    try {
      while (!line.startsWith(" RESULTATS DU MODELE")) {
        while (!"1".equals(line.trim())) {
          info.append(line);
          info.append(CtuluLibString.LINE_SEP_SIMPLE);
          line = f.readLine();
          // if( line==null ) throw new EOFException("resultats introuvables");
        }
        if (DEBUG) System.err.println("1 trouve en ligne " + f.getLineNumber());
        f.mark(128);
        // on marque pour un depassement d'une ligne max (72 chars)
        line = f.readLine();
      }
    } catch (EOFException e) {
      throw new EOFException("resultats introuvables");
    }
    if (DEBUG) System.err.println("section trouvee en ligne " + f.getLineNumber());
    f.reset();
    if (DEBUG) System.err.println("retour ligne " + f.getLineNumber());
    // return info;
    return info.toString();
  }

  private static String RSNJumpToBiefSection(FortranReader f) throws IOException {
    // String info="";
    StringBuffer info = new StringBuffer("");
    String line = "";
    try {
      while (!line.trim().startsWith("BIEF NUMERO")) {
        f.mark(128);
        // info+=line+CtuluLibString.LINE_SEP_SIMPLE;
        info.append(line);
        info.append(CtuluLibString.LINE_SEP_SIMPLE);
        line = f.readLine();
        // if( line==null ) throw new EOFException("bief introuvable");
      }
    } catch (EOFException e) {
      throw new EOFException("bief introuvable");
    }
    if (DEBUG) System.err.println("bief trouve en ligne " + f.getLineNumber());
    f.reset();
    if (DEBUG) System.err.println("retour ligne " + f.getLineNumber());
    if (info.length() == 0) return null;
    String r = info.toString();
    if (r.trim().equals("")) r = null;
    return r;
  }

  public static void ecritResultatsRSN(File fichier, SResultatsRSN resultsRSN, SParametresCAL paramsCAL,
      SProgression progres) throws IOException {
    ecritResultatsRSN(fichier, resultsRSN, paramsCAL, true, progres);
  }

  public static void ecritResultatsRSN(File fichier, SResultatsRSN resultsRSN, SParametresCAL paramsCAL,
      boolean enTete, SProgression progres) throws IOException {
    FortranWriter frsn = new FortranWriter(new FileWriter(fichier));
    System.out.println("Creation du fichier " + fichier.getName());
    boolean progression = (progres != null);
    if (progression) setProgression(progres, 0);
    try {
      int[] fmtCal = new int[] { 33, 8 };
      int[] fmtCalAbs = new int[] { 33, 10, 8, 8, 8 };
      int[] fmtPlani = new int[] { 9, 9, 11, 5, 11, 5 };
      int[] fmtPas = new int[] { 21, 6, 9, 5, 5, 10 };
      int[] fmtDt = new int[] { 6, 6, 10, 6 };
      int[] fmtJour = new int[] { 8, 4, 9, 3, 10, 3, 11, 3 };
      int[] fmtBiefLigne = new int[] { 1, 4, 10, 9, 9, 9, 9, 9, 9, 9, 8, 8, 9, 6, 8, 8, 8 };
      int[] fmtBiefPied = new int[] { 30, 9, 11, 3 };
      frsn.setStringQuoted(false);
      if (enTete) {
        frsn.writeFields(); // on saute 3 lignes et on ecrit le titre
        frsn.writeFields();
        frsn.writeFields();
        frsn.stringField(0, "                     ******************************");
        frsn.writeFields();
        frsn.stringField(0, "                     *    PROGRAMME GENERAL 1D    *");
        frsn.writeFields();
        frsn.stringField(0, "                     *        VERSION 2.0         *");
        frsn.writeFields();
        Calendar cal = Calendar.getInstance();
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumIntegerDigits(2);
        frsn.stringField(0, "                     *    CALCUL DU " + nf.format(cal.get(Calendar.DAY_OF_MONTH)) + "-"
            + nf.format(cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR) + "    *");
        frsn.writeFields();
        frsn.stringField(0, "                     ******************************");
        frsn.writeFields();
        frsn.writeFields();
        frsn.writeFields();
        // Parametres generaux
        frsn.stringField(0, " LECTURE DU FICHIER PARAMETRE");
        frsn.writeFields();
        frsn.stringField(0, " ----------------------------");
        frsn.writeFields();
        frsn.writeFields();
        frsn.stringField(0, paramsCAL.titreCAL[0]);
        frsn.writeFields();
        frsn.stringField(0, "-------------------------------------");
        frsn.writeFields();
        frsn.writeFields();
        frsn.stringField(0, " MODELE MATHEMATIQUE           : ");
        frsn.stringField(1, paramsCAL.genCal.code);
        frsn.writeFields();
        frsn.stringField(0, " REGIME                        : ");
        frsn.stringField(1, paramsCAL.genCal.regime);
        frsn.writeFields();
        frsn.stringField(0, " SINGULARITES                  : ");
        frsn.stringField(1, paramsCAL.genCal.seuil);
        frsn.writeFields();
        frsn.stringField(0, " COMPOSITION DES RUGOSITES     : ");
        frsn.stringField(1, paramsCAL.genCal.compLits);
        frsn.writeFields();
        frsn.stringField(0, " FROTTE. SUR PAROIS VERTICALES : ");
        frsn.stringField(1, paramsCAL.genCal.frotPV);
        frsn.writeFields();
        frsn.stringField(0, " IMPRESSION DE LA GEOMETRIE    : ");
        frsn.stringField(1, paramsCAL.genCal.impGeo);
        frsn.writeFields();
        frsn.stringField(0, " IMPRESSION DU PLANIMETRAGE    : ");
        frsn.stringField(1, paramsCAL.genCal.impPlan);
        frsn.writeFields();
        frsn.stringField(0, " IMPRESSION DU RESEAU          : ");
        frsn.stringField(1, paramsCAL.genCal.impRez);
        frsn.writeFields();
        frsn.stringField(0, " IMPRESSION DES COND. LIMITES  : ");
        frsn.stringField(1, paramsCAL.genCal.impHyd);
        frsn.writeFields();
        frsn.stringField(0, " NUMERO DU FICHIER GEOMETRIE   : ");
        frsn.intField(1, paramsCAL.fic.nFGeo);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NUMERO DU FICHIER SINGULARIT. : ");
        frsn.intField(1, paramsCAL.fic.nFSing);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NUMERO DU FICHIER RESEAU      : ");
        frsn.intField(1, paramsCAL.fic.nFRez);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NUMERO DU FICHIER LIGNE D EAU : ");
        frsn.intField(1, paramsCAL.fic.nFLign);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NUMERO DU FICHIER C. LIMITES  : ");
        frsn.intField(1, paramsCAL.fic.nFCLim);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NUMERO DU FICHIER STO./LECT.  : ");
        frsn.intField(1, paramsCAL.fic.nFSLec);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NUMERO DU FICHIER STO./ECRI.  : ");
        frsn.intField(1, paramsCAL.fic.nFSEcr);
        frsn.writeFields(fmtCal);
        frsn.writeFields();
        frsn.stringField(0, " ABSCISSES  LIMITES DU CALCUL  : ");
        frsn.stringField(1, " XORIGI = ");
        frsn.doubleField(2, paramsCAL.genCal.biefXOrigi);
        frsn.stringField(3, " XFIN = ");
        frsn.doubleField(4, paramsCAL.genCal.biefXFin);
        frsn.writeFields(fmtCalAbs);
        frsn.writeFields();
        // planimetrage
        frsn.stringField(0, " NB DE VALEURS DE PLANIMETRAGE : ");
        frsn.intField(1, paramsCAL.planimetrage.varPlanNbVal);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, " NB DE PAS DE PLANIMETRAGE     : ");
        frsn.intField(1, paramsCAL.planimetrage.varPlanNbPas);
        frsn.writeFields(fmtCal);
        frsn.writeFields();
        for (int i = 0; i < paramsCAL.planimetrage.varPlanNbPas; i++) {
          frsn.stringField(0, " PAS " + (i + 1) + " : ");
          frsn.doubleField(1, paramsCAL.planimetrage.varsPlanimetrage[i].taillePas);
          frsn.stringField(2, " DU PROFIL ");
          frsn.intField(3, paramsCAL.planimetrage.varsPlanimetrage[i].profilDebut);
          frsn.stringField(4, " AU PROFIL ");
          frsn.intField(5, paramsCAL.planimetrage.varsPlanimetrage[i].profilFin);
          frsn.writeFields(fmtPlani);
        }
        frsn.writeFields();
        // parametres temporels
        frsn.stringField(0, " PARAMETRES EN TEMPS      T    : ");
        frsn.doubleField(1, paramsCAL.temporel.tInit);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, "                          TMAX : ");
        frsn.doubleField(1, paramsCAL.temporel.tMax);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, "                          DT   : ");
        frsn.doubleField(1, paramsCAL.temporel.pas2T);
        frsn.writeFields(fmtCal);
        frsn.stringField(0, "                          NP   : ");
        frsn.intField(1, paramsCAL.temporel.numDerPaStoc);
        frsn.writeFields(fmtCal);
        frsn.writeFields();
        frsn.writeFields();
      }
      // Resultats RSN
      frsn.stringField(0, paramsCAL.genCal.code);
      frsn.writeFields();
      frsn.stringField(0, "1");
      frsn.writeFields();
      for (int i = 0; i < resultsRSN.pasTemps.length; i++) {
        if (progression) setProgression(progres, (int) (100 * ((double) i) / resultsRSN.pasTemps.length));
        frsn.stringField(0, " RESULTATS DU MODELE ");
        frsn.stringField(1, paramsCAL.genCal.code);
        frsn.stringField(2, " EN NP = ");
        frsn.intField(3, resultsRSN.pasTemps[i].np);
        frsn.stringField(4, " T = ");
        frsn.intField(5, resultsRSN.pasTemps[i].t);
        frsn.writeFields(fmtPas);
        frsn.stringField(0, " -------------------------------------------------------");
        frsn.writeFields();
        frsn.writeFields();
        if (paramsCAL.genCal.regime.trim().equalsIgnoreCase("NP")) {
          frsn.stringField(0, " DT = ");
          frsn.doubleField(1, resultsRSN.pasTemps[i].dt);
          frsn.stringField(2, " DTLEVY = ");
          frsn.doubleField(3, resultsRSN.pasTemps[i].dtlevy);
          frsn.writeFields(fmtDt);
          frsn.stringField(0, " JOUR = ");
          frsn.intField(1, resultsRSN.pasTemps[i].jour);
          frsn.stringField(2, " HEURE = ");
          frsn.intField(3, resultsRSN.pasTemps[i].heure);
          frsn.stringField(4, " MINUTE = ");
          frsn.intField(5, resultsRSN.pasTemps[i].minute);
          frsn.stringField(6, " SECONDE = ");
          frsn.intField(7, resultsRSN.pasTemps[i].seconde);
          frsn.writeFields(fmtJour);
          frsn.writeFields();
        }
        frsn.writeFields();
        // biefs
        for (int b = 0; b < resultsRSN.pasTemps[i].ligBief.length; b++) {
          frsn.stringField(0, " BIEF NUMERO ");
          frsn.intField(1, resultsRSN.pasTemps[i].ligBief[b].numero + 1);
          frsn.stringField(2, "      I1 = ");
          frsn.intField(3, resultsRSN.pasTemps[i].ligBief[b].i1);
          frsn.stringField(4, " I2 = ");
          frsn.intField(5, resultsRSN.pasTemps[i].ligBief[b].i2);
          frsn.writeFields();
          frsn.stringField(0, " =======================================");
          frsn.writeFields();
          frsn.writeFields();
          frsn.stringField(0, " ");
          frsn.stringField(1, "  I");
          frsn.stringField(2, "  X");
          frsn.stringField(3, "  Z");
          frsn.stringField(4, "  Y");
          frsn.stringField(5, "  S1");
          frsn.stringField(6, "  S2");
          frsn.stringField(7, "  R1");
          frsn.stringField(8, "  R2");
          frsn.stringField(9, "  B1");
          frsn.stringField(10, "  VMIN");
          frsn.stringField(11, "  VMAJ");
          frsn.stringField(12, "  Q");
          frsn.stringField(13, "FROUDE");
          frsn.stringField(14, "  QMIN");
          frsn.stringField(15, "  QMAJ");
          frsn.stringField(16, " CHARGE");
          frsn.writeFields(fmtBiefLigne);
          frsn.writeFields();
          for (int l = 0; l < resultsRSN.pasTemps[i].ligBief[b].ligne.length; l++) {
            frsn.stringField(0, " ");
            frsn.intField(1, resultsRSN.pasTemps[i].ligBief[b].ligne[l].i);
            frsn.doubleField(2, resultsRSN.pasTemps[i].ligBief[b].ligne[l].x);
            frsn.doubleField(3, resultsRSN.pasTemps[i].ligBief[b].ligne[l].z);
            frsn.doubleField(4, resultsRSN.pasTemps[i].ligBief[b].ligne[l].y);
            frsn.doubleField(5, resultsRSN.pasTemps[i].ligBief[b].ligne[l].s1);
            frsn.doubleField(6, resultsRSN.pasTemps[i].ligBief[b].ligne[l].s2);
            frsn.doubleField(7, resultsRSN.pasTemps[i].ligBief[b].ligne[l].r1);
            frsn.doubleField(8, resultsRSN.pasTemps[i].ligBief[b].ligne[l].r2);
            frsn.doubleField(9, resultsRSN.pasTemps[i].ligBief[b].ligne[l].b1);
            frsn.doubleField(10, resultsRSN.pasTemps[i].ligBief[b].ligne[l].vmin);
            frsn.doubleField(11, resultsRSN.pasTemps[i].ligBief[b].ligne[l].vmaj);
            frsn.doubleField(12, resultsRSN.pasTemps[i].ligBief[b].ligne[l].q);
            frsn.doubleField(13, resultsRSN.pasTemps[i].ligBief[b].ligne[l].froude);
            frsn.doubleField(14, resultsRSN.pasTemps[i].ligBief[b].ligne[l].qmin);
            frsn.doubleField(15, resultsRSN.pasTemps[i].ligBief[b].ligne[l].qmaj);
            frsn.doubleField(16, resultsRSN.pasTemps[i].ligBief[b].ligne[l].charge);
            frsn.writeFields(fmtBiefLigne);
          }
          frsn.writeFields();
          frsn.stringField(0, " VOLUME DANS LE LIT ACTIF   : ");
          frsn.stringField(1, "VOLUM  = ");
          frsn.doubleField(2, resultsRSN.pasTemps[i].ligBief[b].volum);
          frsn.stringField(3, " M3");
          frsn.writeFields(fmtBiefPied);
          frsn.stringField(0, " VOLUME STOCKE DANS LE BIEF : ");
          frsn.stringField(1, "VOLUMS = ");
          frsn.doubleField(2, resultsRSN.pasTemps[i].ligBief[b].volums);
          frsn.stringField(3, " M3");
          frsn.writeFields(fmtBiefPied);
          frsn.stringField(0, " VOLUME TOTAL DANS LE BIEF  : ");
          frsn.stringField(1, "VBIEF  = ");
          frsn.doubleField(2, resultsRSN.pasTemps[i].ligBief[b].vbief);
          frsn.stringField(3, " M3");
          frsn.writeFields(fmtBiefPied);
          if (paramsCAL.genCal.regime.trim().equalsIgnoreCase("NP")) {
            frsn.stringField(0, " VOLUME ENTRE DANS LE BIEF");
            frsn.writeFields();
            frsn.stringField(0, " ENTRE T-DT ET T            : ");
            frsn.stringField(1, "VAPPO  = ");
            frsn.doubleField(2, resultsRSN.pasTemps[i].ligBief[b].vappo);
            frsn.stringField(3, " M3");
            frsn.writeFields(fmtBiefPied);
            frsn.stringField(0, " BILAN VOLUMIQUE");
            frsn.writeFields();
            frsn.stringField(0, " ENTRE T-DT ET T            : ");
            frsn.stringField(1, "VPERDU = ");
            frsn.doubleField(2, resultsRSN.pasTemps[i].ligBief[b].vperdu);
            frsn.stringField(3, " M3");
            frsn.writeFields(fmtBiefPied);
          }
          frsn.writeFields();
          frsn.writeFields();
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
     // if (frsn == null) throw new IOException(ex.getMessage());
      throw new IOException(ex.getMessage() + ": ligne " + frsn.getLineNumber());
    }
    finally{
      frsn.close();
    }
  }

  // ************************ ERN ***********************************
  public static SResultatsERN litResultatsERN(File _fichier) throws IOException {
    SResultatsERN results = new SResultatsERN();
    System.out.println("Lecture de " + _fichier.getName());
    // initialisation par defaut pour permettre aux structures, et champs d'etres serialisees
    results.contenu = "";
    FortranReader f = null;
    try {
      f = new FortranReader(new FileReader(_fichier));
      if (f.ready()) {
        String line = f.readLine();
        while (line != null) {
          results.contenu += line + CtuluLibString.LINE_SEP_SIMPLE;
          line = f.readLine();
        }
      }
    } catch (EOFException e) {
    } catch (Exception e) {
      if (f == null) throw new IOException(e.getMessage());
      throw new IOException(e.getMessage() + ": ligne " + f.getLineNumber());
    }
    FortranLib.close(f);
    return results;
  }

  public static void ecritResultatsERN(File _fichier, SResultatsERN _resultsERN) throws IOException {
    // FortranWriter frsn = new FortranWriter(new FileWriter(fichier));
    FileWriter frsn = new FileWriter(_fichier);
    System.out.println("Creation du fichier " + _fichier.getName());
    frsn.write(_resultsERN.contenu, 0, _resultsERN.contenu.length());
    frsn.close();
  }
}
