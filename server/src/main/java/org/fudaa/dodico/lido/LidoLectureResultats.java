package org.fudaa.dodico.lido;
import java.io.File;

import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SResultatsRSN;
/**
 * @author Fred Deniger
 * @version $Id: LidoLectureResultats.java,v 1.5 2004-08-02 08:43:20 deniger Exp $
 */
public final class LidoLectureResultats {
  private LidoLectureResultats(){
    
  }
  /**
   * @param args
   */
  public static void main(String[] args) {
    boolean longFic= false;
    if (args.length >= 0) {
      for (int i= args.length - 1; i >= 0; i--) {
        System.out.println("argument " + i + " = " + args[i]);
        if ("L".equals(args[i])) {
          longFic= true;
          break;
        }
      }
    }
    long t1= System.currentTimeMillis();
    try {
      SResultatsRSN results= new SResultatsRSN();
      SProgression progress= new SProgression();
      String titre= "ETUDE LIDO 2.0 : DONNEES GENERALES DU CALCUL";
      String dir= "/home/echanges/deniger/testLido/";
      if (longFic) {
        File fichier= new File(dir, "long/lido.rsn");
        DResultatsLido.litResultatsRSN(
          fichier,
          results,
          1,
          3600,
          titre,
          progress);
      } else {
        File fichier= new File(dir, "court/lido.rsn");
        File fichierCal= new File(dir, "court/lido.cal");
        File fichierCopie= new File(fichier.getAbsolutePath() + ".cpy");
        System.out.println("lecture fichier cal");
        SParametresCAL paramsCal= DParametresLido.litParametresCAL(fichierCal);
        System.out.println("lecture fichier cal Fin");
        System.out.println("lecture fichier rsn");
        DResultatsLido.litResultatsRSN(
          fichier,
          results,
          5,
          3600,
          titre,
          progress);
        System.out.println("lecture fichier rsn Fin");
        System.out.println("ecriture fichier rsn");
        DResultatsLido.ecritResultatsRSN(
          fichierCopie,
          results,
          paramsCal,
          progress);
        System.out.println("ecriture fichier rsn Fin");
      }
    } catch (Exception e) {
      System.out.println(e);
    }
    System.out.println(
      "Temp pris " + ((System.currentTimeMillis() - t1) / 1000) + " sec");
  }
}
