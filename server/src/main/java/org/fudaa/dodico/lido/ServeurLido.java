/**
 * @creation     2000-02-16
 * @modification $Date: 2006-04-14 14:51:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.lido;
import java.util.Date;

import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.objet.CDodico;
/**
 * Une classe serveur pour Lido
 *
 * @version      $Revision: 1.9 $ $Date: 2006-04-14 14:51:47 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public final class ServeurLido {
  
  private ServeurLido(){
    
  }
  /**
   * @param args
   */
  public static void main(String[] args) {
    String nom=
      (args.length > 0 ? args[0] : CDodico.generateName("::lido::ICalculLido"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculLido.class));
    System.out.println("Lido server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
