/*
 * @file         CConversionHydraulique1d.java
 * @creation     2000-08-11
 * @modification $Date: 2005-08-16 13:04:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion;
import org.fudaa.dodico.corba.objet.*;
import org.fudaa.dodico.corba.hydraulique1d.*;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.lido.conversion.lido.*;
import org.fudaa.dodico.lido.conversion.hydro1d.*;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.6 $ $Date: 2005-08-16 13:04:44 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1d {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  //private IConnexion connexion_;
  // conversion hydraulique1d -> Lido
  private CConversionHydraulique1dCAL CCAL;
  private CConversionHydraulique1dCLM CCLM;
  private CConversionHydraulique1dLIG CLIG;
  private CConversionHydraulique1dRZO CRZO;
  private CConversionHydraulique1dPRO CPRO;
  private CConversionHydraulique1dSNG CSNG;
  private CConversionHydraulique1dEXT CEXT;
  private CConversionHydraulique1dRSN CRSN;
  private CConversionHydraulique1dERN CERN;
  // conversion lido -> hydraulique1d
  private CConversionHydraulique1dGeneral CGeneral;
  private CConversionHydraulique1dHydro CHydro;
  private CConversionHydraulique1dReseau CReseau;
  private CConversionHydraulique1dTemps CTemps;
  private CConversionHydraulique1dResultats CResultats;
  private CConversionHydraulique1dResultatsReseau CResultatsReseau;
  private CConversionHydraulique1dResultatsGeneraux CResultatsGeneraux;
  public CConversionHydraulique1d(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    //connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
    CCAL= new CConversionHydraulique1dCAL(calculHydraulique1d, c);
    CCLM= new CConversionHydraulique1dCLM(calculHydraulique1d, c);
    CLIG= new CConversionHydraulique1dLIG(calculHydraulique1d, c);
    CRZO= new CConversionHydraulique1dRZO(calculHydraulique1d, c);
    CPRO= new CConversionHydraulique1dPRO(calculHydraulique1d, c);
    CSNG= new CConversionHydraulique1dSNG(calculHydraulique1d, c);
    CEXT= new CConversionHydraulique1dEXT(calculHydraulique1d, c);
    CRSN= new CConversionHydraulique1dRSN(calculHydraulique1d, c);
    CERN= new CConversionHydraulique1dERN(calculHydraulique1d, c);
    CGeneral= new CConversionHydraulique1dGeneral(calculHydraulique1d, c);
    CReseau= new CConversionHydraulique1dReseau(calculHydraulique1d, c);
    CHydro= new CConversionHydraulique1dHydro(calculHydraulique1d, c);
    CTemps= new CConversionHydraulique1dTemps(calculHydraulique1d, c);
    CResultats= new CConversionHydraulique1dResultats(calculHydraulique1d, c);
    CResultatsReseau=
      new CConversionHydraulique1dResultatsReseau(calculHydraulique1d, c);
    CResultatsGeneraux=
      new CConversionHydraulique1dResultatsGeneraux(calculHydraulique1d, c);
  }
  private boolean testCalculs() {
    boolean res= true;
    if (calculHydraulique1d_ == null) {
      System.err.println("calculHydraulique is null");
      res= false;
    }
    if (calculLido_ == null) {
      System.err.println("calculLido is null");
      res= false;
    }
    if (!(calculLido_ instanceof ICalculLido)) {
      System.err.println("calculLido is not a lido calcul");
      res= false;
    }
    return res;
  }
  public void parametresHydraulique1dToLido() {
    if (!testCalculs())
      return;
    System.err.println("parametresHydraulique1dToLido");
    CCAL.ecritCAL();
    CCLM.ecritCLM();
    CLIG.ecritLIG();
    CRZO.ecritRZO();
    CPRO.ecritPRO();
    CSNG.ecritSNG();
    CEXT.ecritEXT();
  }
  public void resultatsHydraulique1dToLido() {
    if (!testCalculs())
      return;
    CRSN.ecritRSN();
    CERN.ecritERN();
  }
  public void parametresLidoToHydraulique1d() {
    if (!testCalculs())
      return;
    CReseau.ecritReseau();
    CHydro.ecritHydro();
    CGeneral.ecritGeneral();
    CTemps.ecritTemps();
    CResultats.ecritResultats();
  }
  public void resultatsLidoToHydraulique1d() {
    if (!testCalculs())
      return;
    CResultatsGeneraux.ecritResultats();
    CResultatsReseau.ecritResultats();
  }
}
