/*
 * @file         CConversionHydraulique1dGeneral.java
 * @creation     2000-08-29
 * @modification $Date: 2005-08-16 13:04:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;

import org.fudaa.dodico.corba.objet.*;
import org.fudaa.dodico.corba.hydraulique1d.*;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.objet.*;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.11 $ $Date: 2005-08-16 13:04:38 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dGeneral {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dGeneral(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritGeneral() {
    SParametresCAL cal=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCAL();
    if (cal == null)
      return;
    IEtude1d etude= calculHydraulique1d_.etude();
    etude.paramGeneraux().regime(
      "P".equals(cal.genCal.regime)
        ? LRegime.FLUVIAL_PERMANENT
        : LRegime.FLUVIAL_NON_PERMANENT);
    etude.paramGeneraux().compositionLits(
      "MINEUR/MAJEUR".equals(cal.genCal.compLits)
        ? LTypeCompositionLits.MINEUR_MAJEUR
        : LTypeCompositionLits.FOND_BERGE);
    etude.paramGeneraux().frottementsParois("OUI".equals(cal.genCal.frotPV));
    etude.paramResultats().optionsListing().geometrie(
      "OUI".equals(cal.genCal.impGeo));
    etude.paramResultats().optionsListing().planimetrage(
      "OUI".equals(cal.genCal.impPlan));
    etude.paramResultats().optionsListing().reseau(
      "OUI".equals(cal.genCal.impRez));
    etude.paramResultats().optionsListing().loisHydrauliques(
      "OUI".equals(cal.genCal.impHyd));
    etude.paramGeneraux().zoneEtude().abscisseDebut(cal.genCal.biefXOrigi);
    etude.paramGeneraux().zoneEtude().abscisseFin(cal.genCal.biefXFin);
    litZonesPlanimetrage(etude.reseau(), cal.planimetrage);
    etude.paramGeneraux().maillage().methode(
      cal.sections.nChoix == 1
        ? LMethodeMaillage.SECTIONS_SUR_PROFILS
        : cal.sections.nChoix == 2
        ? LMethodeMaillage.SECTIONS_PAR_SERIES
        : cal.sections.nChoix == 3
        ? LMethodeMaillage.SECTIONS_UTILISATEUR
        : cal.sections.nChoix == 4
        ? LMethodeMaillage.SECTIONS_LIGNE_EAU_INITIALE
        : LMethodeMaillage.SECTIONS_SUR_PROFILS);
    if (cal.sections.nChoix == 2) {
      litSectionsSeries(
        etude.reseau(),
        etude.paramGeneraux().maillage(),
        cal.sections.series);
    } else if (cal.sections.nChoix == 3) {
      litSectionsSections(
        etude.reseau(),
        etude.paramGeneraux().maillage(),
        cal.sections.sections);
    } else {
      etude.paramGeneraux().maillage().sections(null);
    }
  }
  private void litZonesPlanimetrage(
    IReseau reseau,
    SParametresPlaniCAL plani) {
    IZonePlanimetrage[] zones=
      new IZonePlanimetrage[plani.varsPlanimetrage.length];
    for (int i= 0; i < plani.varsPlanimetrage.length; i++) {
      int pDeb=
        reseau.getIndiceProfilNumero(plani.varsPlanimetrage[i].profilDebut - 1);
      if (pDeb < 0) {
        CDodico.exceptionAxel(
          this,
          new Exception(
            "profil "
              + (plani.varsPlanimetrage[i].profilDebut - 1)
              + ": introuvable"));
      }
      int pFin=
        reseau.getIndiceProfilNumero(plani.varsPlanimetrage[i].profilFin - 1);
      if (pFin < 0) {
        CDodico.exceptionAxel(
          this,
          new Exception(
            "profil "
              + (plani.varsPlanimetrage[i].profilFin - 1)
              + ": introuvable"));
      }
      zones[i]=
        reseau.creeZonePlanimetrage(
          pDeb,
          pFin,
          plani.varsPlanimetrage[i].taillePas);
    }
  }
  private void litSectionsSeries(
    IReseau reseau,
    IMaillage maillage,
    SParametresSerieBlocCAL series) {
    IDefinitionSectionsParSeries sections=
      UsineLib.findUsine().creeHydraulique1dDefinitionSectionsParSeries();
    sections.surProfils(true);
    IDefinitionSectionsParSeriesUnitaire[] unitaires=
      new IDefinitionSectionsParSeriesUnitaire[series.nbSeries];
    for (int i= 0; i < series.nbSeries; i++) {
      IBief bief=
        reseau.getBiefContenantProfilAbscisse(series.ligne[i].absDebBief);
      if (bief == null) {
        CDodico.exceptionAxel(
          this,
          new Exception(
            "profil " + series.ligne[i].absDebBief + ": aucun bief associe"));
      }
      unitaires[i]=
        UsineLib
          .findUsine()
          .creeHydraulique1dDefinitionSectionsParSeriesUnitaire();
      IZone zone= UsineLib.findUsine().creeHydraulique1dZone();
      zone.abscisseDebut(series.ligne[i].absDebBief);
      zone.abscisseFin(series.ligne[i].absFinBief);
      zone.biefRattache(bief);
      unitaires[i].zone(zone);
      unitaires[i].pas(series.ligne[i].nbSectCalc);
    }
    sections.unitaires(unitaires);
    maillage.sections(sections);
  }
  private void litSectionsSections(
    IReseau reseau,
    IMaillage maillage,
    SParametresSectionBlocCAL _sections) {
    IDefinitionSectionsParSections sections=
      UsineLib.findUsine().creeHydraulique1dDefinitionSectionsParSections();
    ISite[] unitaires= new ISite[_sections.nbSect];
    for (int i= 0; i < _sections.nbSect; i++) {
      IBief bief=
        reseau.getBiefContenantProfilAbscisse(_sections.ligne[i].absSect);
      if (bief == null) {
        CDodico.exceptionAxel(
          this,
          new Exception(
            "profil " + _sections.ligne[i].absSect + ": aucun bief associe"));
      }
      unitaires[i]= UsineLib.findUsine().creeHydraulique1dSite();
      unitaires[i].abscisse(_sections.ligne[i].absSect);
      unitaires[i].biefRattache(bief);
    }
    sections.unitaires(unitaires);
    maillage.sections(sections);
  }
}
