/*
 * @file         CConversionHydraulique1dHydro.java
 * @creation     2000-09-05
 * @modification $Date: 2005-08-16 13:04:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;
import java.util.*;

import org.fudaa.dodico.corba.objet.*;
import org.fudaa.dodico.corba.hydraulique1d.*;
import org.fudaa.dodico.corba.hydraulique1d.loi.*;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.objet.*;
import org.fudaa.dodico.lido.conversion.LidoResource;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.10 $ $Date: 2005-08-16 13:04:38 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dHydro {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dHydro(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
  }
  public void ecritHydro() {
    SParametresLIG lig=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresLIG();
    SParametresRZO rzo=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresRZO();
    if ((lig == null) || (rzo == null))
      return;
    IEtude1d etude= calculHydraulique1d_.etude();
    IDonneesHydrauliques paramHydro=
      calculHydraulique1d_.etude().donneesHydro();
    paramHydro.lois(ecritLois());
    paramHydro.conditionsInitiales().ligneEauInitiale().nom(lig.titreLIG[1]);
    ILigneEauPoint[] points= new ILigneEauPoint[lig.nbSections];
    IReseau reseau= calculHydraulique1d_.etude().reseau();
    for (int i= 0; i < points.length; i++) {
      IBief bief= reseau.getBiefContenantAbscisse(lig.x[i]);
      points[i]= UsineLib.findUsine().creeHydraulique1dLigneEauPoint();
      points[i].numeroBief(bief == null ? -1 : bief.numero());
      points[i].abscisse(lig.x[i]);
      points[i].cote(lig.z[i]);
      points[i].debit(lig.q[i]);
    }
    paramHydro.conditionsInitiales().ligneEauInitiale().points(points);
    // LIMITES
    for (int i= 0; i < rzo.nbLimi; i++) {
      ILimite lim= UsineLib.findUsine().creeHydraulique1dLimite();
      lim.nom(null);
      lim.typeLimiteCalcule(LLimiteCalcule.EVACUATION_LIBRE);
      ILoiHydraulique loi= paramHydro.getLoi(rzo.blocLims.ligneLim[i].numLoi);
      lim.loi(loi);
      IExtremite extr=
        etude.reseau().getExtremiteNumero(rzo.blocLims.ligneLim[i].numExtBief);
      extr.conditionLimite(lim);
    }
  }
  private ILoiHydraulique[] ecritLois() {
    SParametresCLM clm=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCLM();
    SParametresSNG sng=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresSNG();
    if ((clm == null) || (sng == null))
      return null;
    ILoiHydraulique[] lois= null;
    Vector vlois= new Vector();
    for (int i= 0; i < clm.condLimites.length; i++) {
      ILoiHydraulique loi= creeLoi(clm.condLimites[i]);
      if (loi != null) {
        loi.numero(clm.condLimites[i].numCondLim);
        loi.nom(clm.condLimites[i].description);
        vlois.add(loi);
      }
    }
    for (int i= 0; i < sng.nbSing; i++) {
      ILoiHydraulique loi= creeLoi(sng.singularites[i]);
      if (loi != null) {
        loi.numero(sng.singularites[i].numSing);
        loi.nom(sng.singularites[i].titre);
        vlois.add(loi);
      }
    }
    lois= new ILoiHydraulique[vlois.size()];
    for (int i= 0; i < lois.length; i++)
      lois[i]= (ILoiHydraulique)vlois.get(i);
    return lois;
  }
  private ILoiHydraulique creeLoi(SParametresCondLimBlocCLM loiclm) {
    ILoiHydraulique loi= null;
    switch (loiclm.typLoi) {
      case LidoResource.LIMITE.HYDRO :
        {
          ILoiHydrogramme cloi=
            UsineLib.findUsine().creeHydraulique1dLoiHydrogramme();
          double[] t= new double[loiclm.nbPoints];
          double[] q= new double[loiclm.nbPoints];
          for (int i= 0; i < loiclm.nbPoints; i++) {
            t[i]= loiclm.point[i].tLim;
            q[i]= loiclm.point[i].qLim;
          }
          cloi.t(t);
          cloi.q(q);
          loi= cloi;
          break;
        }
      case LidoResource.LIMITE.LIMNI :
        {
          ILoiLimnigramme cloi=
            UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
          double[] t= new double[loiclm.nbPoints];
          double[] z= new double[loiclm.nbPoints];
          for (int i= 0; i < loiclm.nbPoints; i++) {
            t[i]= loiclm.point[i].tLim;
            z[i]= loiclm.point[i].zLim;
          }
          cloi.t(t);
          cloi.z(z);
          loi= cloi;
          break;
        }
      case LidoResource.LIMITE.TARAGE :
        {
          ILoiTarage cloi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
          double[] z= new double[loiclm.nbPoints];
          double[] q= new double[loiclm.nbPoints];
          for (int i= 0; i < loiclm.nbPoints; i++) {
            z[i]= loiclm.point[i].zLim;
            q[i]= loiclm.point[i].qLim;
          }
          cloi.z(z);
          cloi.q(q);
          cloi.amont(true);
          loi= cloi;
          break;
        }
    }
    return loi;
  }
  private ILoiHydraulique creeLoi(SParametresSingBlocSNG loisng) {
    ILoiHydraulique loi= null;
    switch (loisng.tabParamEntier[LidoResource.SINGULARITE.ITYPE]) {
      case LidoResource.SINGULARITE.SEUIL_NOYE :
        {
          ILoiSeuil cloi= UsineLib.findUsine().creeHydraulique1dLoiSeuil();
          loi= cloi;
          break;
        }
      case LidoResource.SINGULARITE.SEUIL_DENOYE :
        {
          ILoiSeuil cloi= UsineLib.findUsine().creeHydraulique1dLoiSeuil();
          loi= cloi;
          break;
        }
      case LidoResource.SINGULARITE.SEUIL_GEOM :
        {
          ILoiGeometrique cloi=
            UsineLib.findUsine().creeHydraulique1dLoiGeometrique();
          loi= cloi;
          break;
        }
      case LidoResource.SINGULARITE.LIMNI_AMONT :
        {
          ILoiLimnigramme cloi=
            UsineLib.findUsine().creeHydraulique1dLoiLimnigramme();
          loi= cloi;
          break;
        }
      case LidoResource.SINGULARITE.TARAGE_AMONT :
        {
          ILoiTarage cloi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
          cloi.amont(true);
          loi= cloi;
          break;
        }
      case LidoResource.SINGULARITE.TARAGE_AVAL :
        {
          ILoiTarage cloi= UsineLib.findUsine().creeHydraulique1dLoiTarage();
          cloi.amont(false);
          loi= cloi;
          break;
        }
    }
    return loi;
  }
}
