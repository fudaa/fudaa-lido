/*
 * @file         CConversionHydraulique1dReseau.java
 * @creation     2000-08-29
 * @modification $Date: 2005-08-16 13:04:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;
import java.util.Vector;

import org.fudaa.dodico.corba.geometrie.SPoint2D;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.IExtremite;
import org.fudaa.dodico.corba.hydraulique1d.INoeud;
import org.fudaa.dodico.corba.hydraulique1d.IProfil;
import org.fudaa.dodico.corba.hydraulique1d.IReseau;
import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottement;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.lido.SParametresRZO;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.hydraulique1d.CGlobal;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.10 $ $Date: 2005-08-16 13:04:38 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dReseau {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dReseau(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    if (calculHydraulique1d_ != null)
      calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
  }
  public void ecritReseau() {
    SParametresPRO pro=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresPRO();
    SParametresRZO rzo=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresRZO();
    if ((pro == null) || (rzo == null))
      return;
    IEtude1d etude= calculHydraulique1d_.etude();
    if (etude == null) {
      etude= UsineLib.findUsine().creeHydraulique1dEtude1d();
      calculHydraulique1d_.etude(etude);
    }
    IBief[] biefs= new IBief[rzo.nbBief];
    for (int i= 0; i < rzo.blocSitus.ligneSitu.length; i++) {
      Vector vprofils= new Vector();
      Vector vstrickMin= new Vector();
      Vector vstrickMaj= new Vector();
      biefs[i]= UsineLib.findUsine().creeHydraulique1dBief();
      biefs[i].numero(rzo.blocSitus.ligneSitu[i].numBief);
      for (int j= 0; j < pro.nbProfils; j++) {
        if (CGlobal
          .appartient(
            pro.profilsBief[j].abscisse,
            rzo.blocSitus.ligneSitu[i].x1,
            rzo.blocSitus.ligneSitu[i].x2)) {
          IProfil p= UsineLib.findUsine().creeHydraulique1dProfil();
          remplitProfil(p, pro.profilsBief[j]);
          vstrickMin.add(new Double(pro.profilsBief[j].coefStrickMajMin));
          vstrickMaj.add(new Double(pro.profilsBief[j].coefStrickMajSto));
          vprofils.add(p);
        }
      }
      IProfil[] profils= new IProfil[vprofils.size()];
      for (int j= 0; j < vprofils.size(); j++)
        profils[j]= (IProfil)vprofils.get(j);
      biefs[i].profils(profils);
      if (profils.length == 0)
        continue;
      IExtremite extremite= biefs[i].extrAmont();
      extremite.numero(rzo.blocSitus.ligneSitu[i].branch1);
      extremite.profilRattache(profils[0]);
      // noeud a affecter par la suite, dans le parsage des noeuds
      // limite a affecter par la suite, dans le parsage des limites
      extremite= biefs[i].extrAval();
      extremite.numero(rzo.blocSitus.ligneSitu[i].branch2);
      extremite.profilRattache(profils[profils.length - 1]);
      // noeud a affecter par la suite, dans le parsage des noeuds
      // limite a affecter par la suite, dans le parsage des limites
      Vector vzonesF= new Vector();
      double strickMinPrec, strickMajPrec, strickMinCour, strickMajCour;
      int jprec= 0;
      strickMinPrec= ((Double)vstrickMin.get(0)).doubleValue();
      strickMajPrec= ((Double)vstrickMaj.get(0)).doubleValue();
      for (int j= 1; j < biefs[i].profils().length; j++) {
        strickMinCour= ((Double)vstrickMin.get(j)).doubleValue();
        strickMajCour= ((Double)vstrickMaj.get(j)).doubleValue();
        if ((j == biefs[i].profils().length - 1)
          || (!CGlobal.egale(strickMinCour, strickMinPrec))
          || (!CGlobal.egale(strickMajCour, strickMajPrec))) {
          IZoneFrottement zf=
            UsineLib.findUsine().creeHydraulique1dZoneFrottement();
          zf.abscisseDebut(biefs[i].profils()[jprec].abscisse());
          zf.abscisseFin(biefs[i].profils()[j].abscisse());
          zf.biefRattache(biefs[i]);
          zf.coefMajeur(strickMajPrec);
          zf.coefMineur(strickMinPrec);
          vzonesF.add(zf);
          strickMinPrec= strickMinCour;
          strickMajPrec= strickMajCour;
          jprec= j;
        }
      }
      IZoneFrottement[] zonesF= new IZoneFrottement[vzonesF.size()];
      for (int j= 0; j < zonesF.length; j++)
        zonesF[j]= (IZoneFrottement)vzonesF.get(j);
      biefs[i].zonesFrottement(zonesF);
      // singularite a affecter par la suite dans le parsage du SNG
      // resultats a affecter
    }
    IReseau reseau= etude.reseau();
    if (reseau == null) {
      reseau= UsineLib.findUsine().creeHydraulique1dReseau();
      etude.reseau(reseau);
    }
    reseau.biefs(biefs);
    // NOEUDS
    for (int i= 0; i < rzo.nbNoeud; i++) {
      INoeud noeud= UsineLib.findUsine().creeHydraulique1dNoeud();
      noeud.numero(i);
      int nb= 0;
      for (int j= 0; j < rzo.blocNoeuds.ligneNoeud[i].noeud.length; j++)
        if (rzo.blocNoeuds.ligneNoeud[i].noeud[j] > 0)
          nb++;
      IExtremite[] extr= new IExtremite[nb];
      nb= 0;
      for (int j= 0; j < rzo.blocNoeuds.ligneNoeud[i].noeud.length; j++) {
        if (rzo.blocNoeuds.ligneNoeud[i].noeud[j] <= 0)
          continue;
        extr[nb]=
          etude.reseau().getExtremiteNumero(
            rzo.blocNoeuds.ligneNoeud[i].noeud[j]);
        if (extr[nb] != null)
          extr[nb].noeudRattache(noeud);
        nb++;
      }
      noeud.extremites(extr);
    }
  }
  private void remplitProfil(IProfil p, SParametresBiefBlocPRO pPRO) {
    p.nom(pPRO.numProfil);
    p.abscisse(pPRO.abscisse);
    SPoint2D[] points= new SPoint2D[pPRO.nbPoints];
    boolean stockG= false;
    int indiceLitMinGa= 0,
      indiceLitMinDr= 0,
      indiceLitMajGa= 0,
      indiceLitMajDr= 0;
    for (int i= 0; i < pPRO.nbPoints; i++) {
      points[i]= new SPoint2D();
      points[i].x= pPRO.abs[i];
      points[i].y= pPRO.cotes[i];
      if (CGlobal.egale(pPRO.absMajMin[0], pPRO.abs[i]))
        indiceLitMinGa= i;
      if (CGlobal.egale(pPRO.absMajMin[1], pPRO.abs[i]))
        indiceLitMinDr= i;
      if (CGlobal.egale(pPRO.absMajSto[0], pPRO.abs[i]) && (!stockG)) {
        stockG= true;
        indiceLitMajGa= i;
      }
      if (CGlobal.egale(pPRO.absMajSto[1], pPRO.abs[i]))
        indiceLitMajDr= i;
    }
    p.points(points);
    p.indiceLitMajDr(indiceLitMajDr);
    p.indiceLitMinDr(indiceLitMinDr);
    p.indiceLitMinGa(indiceLitMinGa);
    p.indiceLitMajGa(indiceLitMajGa);
    // le planimetrage est fait dans le parsage du CAL
  }
  // pour l'importation
  public void convertitProfils(SParametresPRO pro, IBief bief) {
    IProfil[] profils= new IProfil[pro.nbProfils];
    IZoneFrottement zoneF= null;
    for (int j= 0; j < pro.nbProfils; j++) {
      IProfil p= bief.creeProfil();
      remplitProfil(p, pro.profilsBief[j]);
      double strickMin= pro.profilsBief[j].coefStrickMajMin;
      double strickMaj= pro.profilsBief[j].coefStrickMajSto;
      if ((zoneF == null)
        || (strickMaj != zoneF.coefMajeur())
        || (strickMin != zoneF.coefMineur())) {
        zoneF= bief.creeZoneFrottement();
        zoneF.abscisseDebut(p.abscisse());
        zoneF.abscisseFin(p.abscisse());
        zoneF.biefRattache(bief);
        zoneF.coefMajeur(strickMaj);
        zoneF.coefMineur(strickMin);
      } else {
        zoneF.abscisseFin(p.abscisse());
      }
      profils[j]= p;
    }
  }
}
