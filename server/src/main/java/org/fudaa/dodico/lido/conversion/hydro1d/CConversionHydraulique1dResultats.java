/*
 * @file         CConversionHydraulique1dResultats.java
 * @creation     2000-09-05
 * @modification $Date: 2005-08-16 13:04:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariable;
import org.fudaa.dodico.corba.hydraulique1d.IParametresResultats;
import org.fudaa.dodico.corba.hydraulique1d.ISite;
import org.fudaa.dodico.corba.hydraulique1d.LOptionStockage;
import org.fudaa.dodico.corba.hydraulique1d.LTypeNombre;
import org.fudaa.dodico.corba.hydraulique1d.LUnite;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.11 $ $Date: 2005-08-16 13:04:38 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dResultats {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dResultats(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
  }
  public void ecritResultats() {
    SParametresCAL cal=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCAL();
    if (cal == null)
      return;
    IParametresResultats paramResultats=
      calculHydraulique1d_.etude().paramResultats();
    paramResultats.optionsListing().geometrie("OUI".equals(cal.genCal.impGeo));
    paramResultats.optionsListing().planimetrage(
      "OUI".equals(cal.genCal.impPlan));
    paramResultats.optionsListing().reseau("OUI".equals(cal.genCal.impRez));
    paramResultats.optionsListing().loisHydrauliques(
      "OUI".equals(cal.genCal.impHyd));
    paramResultats.optionsListing().ligneEauInitiale(false);
    paramResultats.optionsListing().calcul(true);
    paramResultats.paramStockage().option(LOptionStockage.TOUTES_SECTIONS);
    paramResultats.paramStockage().premierPasTempsStocke(0);
    paramResultats.paramStockage().periodeListing(1);
    paramResultats.paramStockage().periodeResultat(1);
    paramResultats.paramStockage().pasTempsImpression(cal.temporel.pas2TImp);
    paramResultats.paramStockage().sites(new ISite[0]);
    paramResultats.postRubens(false);
    paramResultats.postOpthyca(false);
    paramResultats.decalage(0.);
    paramResultats.variables(ecritVariables());
  }
  private IDescriptionVariable[] ecritVariables() {
    IDescriptionVariable[] desc= new IDescriptionVariable[14];
    desc[0]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[0].nom("Z");
    desc[0].description("Ligne d'eau");
    desc[0].unite(LUnite.M);
    desc[0].type(LTypeNombre.REEL);
    desc[0].nbDecimales(3);
    desc[1]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[1].nom("Y");
    desc[1].description("Hauteur d'eau");
    desc[1].unite(LUnite.M);
    desc[1].type(LTypeNombre.REEL);
    desc[1].nbDecimales(3);
    desc[2]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[2].nom("S1");
    desc[2].description("Section mouill�e (lit mineur)");
    desc[2].unite(LUnite.RIEN);
    desc[2].type(LTypeNombre.REEL);
    desc[2].nbDecimales(3);
    desc[3]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[3].nom("S2");
    desc[3].description("Section mouill�e (lit majeur)");
    desc[3].unite(LUnite.RIEN);
    desc[3].type(LTypeNombre.REEL);
    desc[3].nbDecimales(3);
    desc[4]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[4].nom("R1");
    desc[4].description("Rayon hydraulique (lit mineur)");
    desc[4].unite(LUnite.M);
    desc[4].type(LTypeNombre.REEL);
    desc[4].nbDecimales(3);
    desc[5]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[5].nom("R2");
    desc[5].description("Rayon hydraulique (lit majeur)");
    desc[5].unite(LUnite.RIEN);
    desc[5].type(LTypeNombre.REEL);
    desc[5].nbDecimales(3);
    desc[6]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[6].nom("B1");
    desc[6].description("Largeur au miroir");
    desc[6].unite(LUnite.M);
    desc[6].type(LTypeNombre.REEL);
    desc[6].nbDecimales(3);
    desc[7]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[7].nom("VMIN");
    desc[7].description("Vitesse (lit mineur)");
    desc[7].unite(LUnite.M_PAR_S);
    desc[7].type(LTypeNombre.REEL);
    desc[7].nbDecimales(3);
    desc[8]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[8].nom("VMAJ");
    desc[8].description("Vitesse (lit majeur)");
    desc[8].unite(LUnite.M_PAR_S);
    desc[8].type(LTypeNombre.REEL);
    desc[8].nbDecimales(3);
    desc[9]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[9].nom("Q");
    desc[9].description("D�bit total");
    desc[9].unite(LUnite.M3_PAR_S);
    desc[9].type(LTypeNombre.REEL);
    desc[9].nbDecimales(3);
    desc[10]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[10].nom("FROUDE");
    desc[10].description("Nombre de Froude");
    desc[10].unite(LUnite.RIEN);
    desc[10].type(LTypeNombre.REEL);
    desc[10].nbDecimales(3);
    desc[11]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[11].nom("QMIN");
    desc[11].description("D�bit (lit mineur)");
    desc[11].unite(LUnite.M3_PAR_S);
    desc[11].type(LTypeNombre.REEL);
    desc[11].nbDecimales(3);
    desc[12]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[12].nom("QMAJ");
    desc[12].description("D�bit (lit majeur)");
    desc[12].unite(LUnite.M3_PAR_S);
    desc[12].type(LTypeNombre.REEL);
    desc[12].nbDecimales(3);
    desc[13]= UsineLib.findUsine().creeHydraulique1dDescriptionVariable();
    desc[13].nom("CHARGE");
    desc[13].description("Charge");
    desc[13].unite(LUnite.RIEN);
    desc[13].type(LTypeNombre.REEL);
    desc[13].nbDecimales(3);
    return desc;
  }
}
