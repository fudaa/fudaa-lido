/*
 * @file         CConversionHydraulique1dResultatsGeneraux.java
 * @creation     2000-09-06
 * @modification $Date: 2005-05-20 21:50:53 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGeneraux;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IResultatsLidoHelper;
import org.fudaa.dodico.corba.lido.SResultatsERN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.8 $ $Date: 2005-05-20 21:50:53 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dResultatsGeneraux {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dResultatsGeneraux(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
  }
  public void ecritResultats() {
    SResultatsERN ern=
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsERN();
    SResultatsRSN rsn=
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsRSN();
    if ((ern == null) || (rsn == null))
      return;
    IResultatsGeneraux resultats=
      calculHydraulique1d_.etude().resultatsGeneraux();
    resultats.listing(null);
    resultats.resultatReprise().fichier(null);
    resultats.resultatReprise().contenu(null);
    resultats.resultatReprise().tFinal(0.);
    resultats.messagesEcran(ern.contenu.getBytes());
    resultats.resultatsTemporelSpatial().descriptionVariables(
      calculHydraulique1d_.etude().paramResultats().variables());
  }
}
