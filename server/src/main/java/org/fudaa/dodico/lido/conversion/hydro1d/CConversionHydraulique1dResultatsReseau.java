/*
 * @file         CConversionHydraulique1dResultatsReseau.java
 * @creation     2000-09-06
 * @modification $Date: 2005-08-16 13:04:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariable;
import org.fudaa.dodico.corba.hydraulique1d.IInformationTemps;
import org.fudaa.dodico.corba.hydraulique1d.IReseau;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsBiefPasTemps;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGeneraux;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculee;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculeePasTemps;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IResultatsLidoHelper;
import org.fudaa.dodico.corba.lido.SResultatsBiefLigneRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.10 $ $Date: 2005-08-16 13:04:38 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dResultatsReseau {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dResultatsReseau(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
  }
  public void ecritResultats() {
    SResultatsRSN rsn=
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsRSN();
    if (rsn == null)
      return;
    IReseau reseau= calculHydraulique1d_.etude().reseau();
    IBief[] biefs= reseau.biefs();
    for (int i= 0; i < biefs.length; i++) {
      IResultatsBiefPasTemps[] pas=
        new IResultatsBiefPasTemps[rsn.pasTemps.length];
      for (int t= 0; t < pas.length; t++) {
        pas[t]= UsineLib.findUsine().creeHydraulique1dResultatsBiefPasTemps();
        IInformationTemps it=
          UsineLib.findUsine().creeHydraulique1dInformationTemps();
        it.info(rsn.pasTemps[t].info);
        it.numeroPas(rsn.pasTemps[t].np);
        it.t(rsn.pasTemps[t].t);
        it.dt(rsn.pasTemps[t].dt);
        it.dtlevy(rsn.pasTemps[t].dtlevy);
        it.jour(rsn.pasTemps[t].jour);
        it.heure(rsn.pasTemps[t].heure);
        it.minute(rsn.pasTemps[t].minute);
        it.seconde(rsn.pasTemps[t].seconde);
        pas[t].infoTemps(it);
        pas[t].volum(rsn.pasTemps[t].ligBief[i].volum);
        pas[t].volums(rsn.pasTemps[t].ligBief[i].volums);
        pas[t].vbief(rsn.pasTemps[t].ligBief[i].vbief);
        pas[t].vappo(rsn.pasTemps[t].ligBief[i].vappo);
        pas[t].vperdu(rsn.pasTemps[t].ligBief[i].vperdu);
      }
      biefs[i].resultatsBief().pasTemps(pas);
      // sections
      ISectionCalculee[] sections= new ISectionCalculee[0];
      if (rsn.pasTemps.length > 0) {
        sections= new ISectionCalculee[rsn.pasTemps[0].ligBief[i].ligne.length];
        for (int j= 0; j < sections.length; j++) {
          sections[j]= UsineLib.findUsine().creeHydraulique1dSectionCalculee();
          sections[j].abscisse(rsn.pasTemps[0].ligBief[i].ligne[j].x);
          ISectionCalculeePasTemps[] pasSection=
            new ISectionCalculeePasTemps[rsn.pasTemps.length];
          for (int t= 0; t < pasSection.length; t++) {
            pasSection[t]=
              UsineLib.findUsine().creeHydraulique1dSectionCalculeePasTemps();
            pasSection[t].infoTemps(
              biefs[i].resultatsBief().pasTemps()[t].infoTemps());
            double[] valeurs=
              remplitValeurs(rsn.pasTemps[t].ligBief[i].ligne[j]);
            pasSection[t].valeurs(valeurs);
          }
          sections[j].pasTemps(pasSection);
        }
      }
      biefs[i].sectionsCalculees(sections);
    }
  }
  private double[] remplitValeurs(SResultatsBiefLigneRSN ssection) {
    IResultatsGeneraux res= calculHydraulique1d_.etude().resultatsGeneraux();
    IDescriptionVariable[] vars=
      res.resultatsTemporelSpatial().descriptionVariables();
    double[] vals= new double[vars.length];
    for (int v= 0; v < vars.length; v++) {
      if ("Z".equals(vars[v].nom()))
        vals[v]= ssection.z;
      else if ("Y".equals(vars[v].nom()))
        vals[v]= ssection.y;
      else if ("S1".equals(vars[v].nom()))
        vals[v]= ssection.s1;
      else if ("S2".equals(vars[v].nom()))
        vals[v]= ssection.s2;
      else if ("R1".equals(vars[v].nom()))
        vals[v]= ssection.r1;
      else if ("R2".equals(vars[v].nom()))
        vals[v]= ssection.r2;
      else if ("B1".equals(vars[v].nom()))
        vals[v]= ssection.b1;
      else if ("VMIN".equals(vars[v].nom()))
        vals[v]= ssection.vmin;
      else if ("VMAJ".equals(vars[v].nom()))
        vals[v]= ssection.vmaj;
      else if ("Q".equals(vars[v].nom()))
        vals[v]= ssection.q;
      else if ("FROUDE".equals(vars[v].nom()))
        vals[v]= ssection.froude;
      else if ("QMIN".equals(vars[v].nom()))
        vals[v]= ssection.qmin;
      else if ("QMAJ".equals(vars[v].nom()))
        vals[v]= ssection.qmaj;
      else if ("CHARGE".equals(vars[v].nom()))
        vals[v]= ssection.charge;
    }
    return vals;
  }
}
