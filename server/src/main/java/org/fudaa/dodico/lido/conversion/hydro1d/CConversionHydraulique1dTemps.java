/*
 * @file         CConversionHydraulique1dTemps.java
 * @creation     2000-09-01
 * @modification $Date: 2005-08-16 13:04:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.hydro1d;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IParametresTemporels;
import org.fudaa.dodico.corba.hydraulique1d.LCritereArret;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculLido en calculhydraulique1d
 *
 * @version      $Revision: 1.9 $ $Date: 2005-08-16 13:04:38 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dTemps {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dTemps(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d_.calculCode();
  }
  public void ecritTemps() {
    SParametresCAL cal=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCAL();
    if (cal == null)
      return;
    IParametresTemporels paramTemps= calculHydraulique1d_.etude().paramTemps();
    paramTemps.tempsInitial(cal.temporel.tInit);
    paramTemps.tempsFinal(cal.temporel.tMax);
    paramTemps.pasTemps(cal.temporel.pas2T);
    paramTemps.critereArret(LCritereArret.TEMPS_MAX);
    paramTemps.nbPasTemps(
      (int) ((cal.temporel.tMax - cal.temporel.tInit) / cal.temporel.pas2T));
    paramTemps.pasTempsVariable(false);
  }
}
