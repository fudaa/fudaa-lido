/*
 * @file         CConversionHydraulique1dCAL.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.*;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.7 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dCAL {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dCAL(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritCAL() {
    SParametresCAL cal=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCAL();
    if (cal == null) {
      cal= new SParametresCAL();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCAL(cal);
    }
    System.err.println("ecritCAL");
    IEtude1d etude= calculHydraulique1d_.etude();
    cal.status= true;
    cal.titreCAL=
      new String[] { "ETUDE LIDO 2.0 : DONNEES GENERALES DU CALCUL", "" };
    cal.genCal= new SParametresGenCAL();
    cal.genCal.code= "REZO";
    cal.genCal.regime=
      etude.paramGeneraux().regime().value() == LRegime._FLUVIAL_PERMANENT
        ? "P"
        : "NP";
    cal.genCal.seuil= etude.reseau().hasSingularites() ? "OUI" : "NON";
    cal.genCal.compLits=
      etude.paramGeneraux().compositionLits().value()
        == LTypeCompositionLits._MINEUR_MAJEUR
        ? "MINEUR/MAJEUR"
        : "FOND/BERGE";
    cal.genCal.frotPV=
      etude.paramGeneraux().frottementsParois() ? "OUI" : "NON";
    cal.genCal.impGeo=
      etude.paramResultats().optionsListing().geometrie() ? "OUI" : "NON";
    cal.genCal.impPlan=
      etude.paramResultats().optionsListing().planimetrage() ? "OUI" : "NON";
    cal.genCal.impRez=
      etude.paramResultats().optionsListing().reseau() ? "OUI" : "NON";
    cal.genCal.impHyd=
      etude.paramResultats().optionsListing().loisHydrauliques()
        ? "OUI"
        : "NON";
    cal.genCal.biefXOrigi= etude.paramGeneraux().zoneEtude().abscisseDebut();
    cal.genCal.biefXFin= etude.paramGeneraux().zoneEtude().abscisseFin();
    cal.fic= new SParametresFicCAL();
    cal.fic.nFGeo= 20;
    cal.fic.nFSing= 21;
    cal.fic.nFRez= 26;
    cal.fic.nFLign= 22;
    cal.fic.nFCLim= 23;
    cal.fic.nFSLec= 24;
    cal.fic.nFSEcr= 25;
    cal.planimetrage= new SParametresPlaniCAL();
    cal.planimetrage.varPlanNbVal= 50;
    ecritZonesPlanimetrage(etude.reseau(), cal.planimetrage);
    cal.temporel= new SParametresTempCAL();
    cal.temporel.tInit= etude.paramTemps().tempsInitial();
    cal.temporel.tMax= etude.paramTemps().tempsFinal();
    cal.temporel.pas2T= etude.paramTemps().pasTemps();
    cal.temporel.numDerPaStoc= 0;
    cal.temporel.pas2TImp=
      etude.paramResultats().paramStockage().pasTempsImpression();
    cal.temporel.pas2TStoc= 1.;
    cal.sections= new SParametresSectionsCAL();
    cal.sections.nChoix=
      etude.paramGeneraux().maillage().methode().value()
        == LMethodeMaillage._SECTIONS_SUR_PROFILS
        ? 1
        : etude.paramGeneraux().maillage().methode().value()
          == LMethodeMaillage._SECTIONS_PAR_SERIES
        ? 2
        : etude.paramGeneraux().maillage().methode().value()
          == LMethodeMaillage._SECTIONS_UTILISATEUR
        ? 3
        : etude.paramGeneraux().maillage().methode().value()
          == LMethodeMaillage._SECTIONS_LIGNE_EAU_INITIALE
        ? 4
        : 0;
    cal.sections.series= new SParametresSerieBlocCAL();
    cal.sections.sections= new SParametresSectionBlocCAL();
    cal.sections.series.nbSeries= 0;
    cal.sections.series.ligne= new SParametresSerieLigneCAL[0];
    cal.sections.sections.nbSect= 0;
    cal.sections.sections.ligne= new SParametresSectionLigneCAL[0];
    if (etude.paramGeneraux().maillage().methode().value()
      == LMethodeMaillage._SECTIONS_PAR_SERIES) {
      ecritSectionsSeries(
        ((IDefinitionSectionsParSeries)etude
          .paramGeneraux()
          .maillage()
          .sections())
          .unitaires(),
        cal.sections.series);
    } else if (
      etude.paramGeneraux().maillage().methode().value()
        == LMethodeMaillage._SECTIONS_UTILISATEUR) {
      ecritSectionsSections(
        ((IDefinitionSectionsParSections)etude
          .paramGeneraux()
          .maillage()
          .sections())
          .unitaires(),
        cal.sections.sections);
    }
  }
  private void ecritZonesPlanimetrage(
    IReseau reseau,
    SParametresPlaniCAL plani) {
    IZonePlanimetrage[] planis= reseau.zonesPlanimetrage();
    SParametresPasLigneCAL[] splanis= new SParametresPasLigneCAL[planis.length];
    for (int i= 0; i < planis.length; i++) {
      SParametresPasLigneCAL s= new SParametresPasLigneCAL();
      s.taillePas= planis[i].taillePas();
      s.profilDebut=
        reseau.getIndiceProfilAbscisse(planis[i].abscisseDebut()) + 1;
      s.profilFin= reseau.getIndiceProfilAbscisse(planis[i].abscisseFin()) + 1;
      splanis[i]= s;
    }
    plani.varPlanNbPas= splanis.length;
    plani.varsPlanimetrage= splanis;
  }
  private void ecritSectionsSeries(
    IDefinitionSectionsParSeriesUnitaire[] unitaires,
    SParametresSerieBlocCAL series) {
    series.nbSeries= unitaires.length;
    series.ligne= new SParametresSerieLigneCAL[series.nbSeries];
    for (int i= 0; i < series.nbSeries; i++) {
      series.ligne[i]= new SParametresSerieLigneCAL();
      series.ligne[i].absDebBief= unitaires[i].zone().abscisseDebut();
      series.ligne[i].absFinBief= unitaires[i].zone().abscisseFin();
      series.ligne[i].nbSectCalc= (int)unitaires[i].pas();
    }
  }
  private void ecritSectionsSections(
    ISite[] unitaires,
    SParametresSectionBlocCAL sections) {
    sections.nbSect= unitaires.length;
    sections.ligne= new SParametresSectionLigneCAL[sections.nbSect];
    for (int i= 0; i < sections.nbSect; i++) {
      sections.ligne[i]= new SParametresSectionLigneCAL();
      sections.ligne[i].absSect= unitaires[i].abscisse();
    }
  }
}
