/*
 * @file         CConversionHydraulique1dCLM.java
 * @creation     2000-08-17
 * @modification $Date: 2004-06-30 15:18:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import java.util.Vector;

import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IApport;
import org.fudaa.dodico.corba.hydraulique1d.singularite.IPerteCharge;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresApportLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimBlocCLM;
import org.fudaa.dodico.corba.lido.SParametresCondLimPointLigneCLM;
import org.fudaa.dodico.corba.lido.SParametresPerteLigneCLM;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.lido.conversion.LidoResource;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.7 $ $Date: 2004-06-30 15:18:02 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dCLM {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dCLM(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritCLM() {
    SParametresCLM clm=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCLM();
    if (clm == null) {
      clm= new SParametresCLM();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresCLM(clm);
    }
    System.err.println("ecritCLM");
    IEtude1d etude= calculHydraulique1d_.etude();
    clm.status= true;
    clm.titreCLM= "ETUDE LIDO 2.0 : CONDITIONS LIMITES";
    ILoiHydraulique[] lois= etude.donneesHydro().lois();
    Vector vlois= new Vector();
    for (int i= 0; i < lois.length; i++) {
      int t= typeLoiLimite(lois[i]);
      if (t > 0) {
        SParametresCondLimBlocCLM l= new SParametresCondLimBlocCLM();
        l.typLoi= t;
        l.numCondLim= lois[i].numero();
        l.description= lois[i].nom();
        l.nbPoints= lois[i].nbPoints();
        l.point= new SParametresCondLimPointLigneCLM[l.nbPoints];
        remplitLoiPoints(lois[i], l.point);
        vlois.add(l);
      }
    }
    clm.condLimites= new SParametresCondLimBlocCLM[vlois.size()];
    clm.nbCondLim= clm.condLimites.length;
    for (int i= 0; i < vlois.size(); i++)
      clm.condLimites[i]= (SParametresCondLimBlocCLM)vlois.get(i);
    clm.sousTitrePerte= "DEFINITION DES PERTES SINGULIERES";
    IPerteCharge[] pertes= etude.reseau().pertesCharges();
    clm.nbPerte= pertes.length;
    clm.perte= new SParametresPerteLigneCLM[pertes.length];
    for (int i= 0; i < pertes.length; i++) {
      clm.perte[i]= new SParametresPerteLigneCLM();
      clm.perte[i].xPerte= pertes[i].abscisse();
      clm.perte[i].coefPerte= pertes[i].coefficient();
    }
    clm.sousTitreApport= "DEFINITION DES APPORTS ET SOUTIRAGES";
    IApport[] apports= etude.reseau().apports();
    clm.nbApport= apports.length;
    clm.apport= new SParametresApportLigneCLM[apports.length];
    for (int i= 0; i < apports.length; i++) {
      clm.apport[i]= new SParametresApportLigneCLM();
      clm.apport[i].xApport= apports[i].abscisse();
      clm.apport[i].numLoi= apports[i].loi().numero();
      clm.apport[i].typLoi= typeLoiLimite(apports[i].loi());
      clm.apport[i].coefApport= apports[i].coefficient();
    }
  }
  private int typeLoiLimite(ILoiHydraulique loi) {
    if (loi instanceof ILoiHydrogramme)
      return LidoResource.LIMITE.HYDRO;
    if (loi instanceof ILoiLimnigramme)
      return LidoResource.LIMITE.LIMNI;
    if (loi instanceof ILoiTarage)
      return LidoResource.LIMITE.TARAGE;
    return 0;
  }
  private void remplitLoiPoints(
    ILoiHydraulique loi,
    SParametresCondLimPointLigneCLM[] points) {
    for (int j= 0; j < points.length; j++) {
      points[j]= new SParametresCondLimPointLigneCLM();
      if (loi instanceof ILoiHydrogramme) {
        points[j].tLim= ((ILoiHydrogramme)loi).t()[j];
        points[j].qLim= ((ILoiHydrogramme)loi).q()[j];
      } else if (loi instanceof ILoiLimnigramme) {
        points[j].tLim= ((ILoiLimnigramme)loi).t()[j];
        points[j].zLim= ((ILoiLimnigramme)loi).z()[j];
      } else if (loi instanceof ILoiTarage) {
        points[j].zLim= ((ILoiTarage)loi).z()[j];
        points[j].qLim= ((ILoiTarage)loi).q()[j];
      }
    }
  }
}
