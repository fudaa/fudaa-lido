/*
 * @file         CConversionHydraulique1dERN.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGeneraux;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IResultatsLidoHelper;
import org.fudaa.dodico.corba.lido.SResultatsERN;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.7 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dERN {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dERN(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritERN() {
    SResultatsERN ern=
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsERN();
    if (ern == null) {
      ern= new SResultatsERN();
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsERN(ern);
    }
    System.err.println("ecritERN");
    IResultatsGeneraux resultats=
      calculHydraulique1d_.etude().resultatsGeneraux();
    ern.contenu= new String(resultats.messagesEcran());
  }
}
