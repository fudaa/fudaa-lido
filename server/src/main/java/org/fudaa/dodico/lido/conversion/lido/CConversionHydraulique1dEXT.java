/*
 * @file         CConversionHydraulique1dEXT.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresEXT;
import org.fudaa.dodico.corba.lido.SParametresLaisseLigneEXT;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.8 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dEXT {
  private ICalculLido calculLido_;
 // private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dEXT(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    //calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritEXT() {
    SParametresEXT ext=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresEXT();
    if (ext == null) {
      ext= new SParametresEXT();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresEXT(ext);
    }
    System.err.println("ecritEXT");
    ext.laisses= new SParametresLaisseLigneEXT[0];
  }
}
