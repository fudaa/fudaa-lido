/*
 * @file         CConversionHydraulique1dLIG.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauInitiale;
import org.fudaa.dodico.corba.hydraulique1d.ILigneEauPoint;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresLIG;
import org.fudaa.dodico.corba.lido.SParametresLimBiefLIG;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.7 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dLIG {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dLIG(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritLIG() {
    SParametresLIG lig=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresLIG();
    if (lig == null) {
      lig= new SParametresLIG();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresLIG(lig);
    }
    System.err.println("ecritLIG");
    IEtude1d etude= calculHydraulique1d_.etude();
    ILigneEauInitiale liginit=
      etude.donneesHydro().conditionsInitiales().ligneEauInitiale();
    ILigneEauPoint[] points= liginit.points();
    lig.status= true;
    lig.titreLIG=
      new String[] { "FICHIER DE LIGNE D'EAU INITIALE", liginit.nom(), "" };
    lig.nbSections= points.length;
    int[][] delim= liginit.delimitationsSectionsBiefs();
    lig.nbBief= delim.length;
    lig.delimitBief= new SParametresLimBiefLIG[delim.length];
    for (int i= 0; i < delim.length; i++) {
      lig.delimitBief[i]= new SParametresLimBiefLIG();
      lig.delimitBief[i].sectionDebut= delim[i][0];
      lig.delimitBief[i].sectionFin= delim[i][1];
    }
    lig.x= new double[points.length];
    lig.z= new double[points.length];
    lig.q= new double[points.length];
    for (int i= 0; i < points.length; i++) {
      lig.x[i]= points[i].abscisse();
      lig.z[i]= points[i].cote();
      lig.q[i]= points[i].debit();
    }
  }
}
