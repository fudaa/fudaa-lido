/*
 * @file         CConversionHydraulique1dPRO.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.IProfil;
import org.fudaa.dodico.corba.hydraulique1d.IZoneFrottement;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresBiefBlocPRO;
import org.fudaa.dodico.corba.lido.SParametresPRO;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.hydraulique1d.CGlobal;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.7 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dPRO {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dPRO(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritPRO() {
    SParametresPRO pro=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresPRO();
    if (pro == null) {
      pro= new SParametresPRO();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresPRO(pro);
    }
    System.err.println("ecritPRO");
    IEtude1d etude= calculHydraulique1d_.etude();
    pro.status= true;
    pro.titrePRO=
      new String[] {
        "ETUDE LIDO 2.0 : DEFINITION DES PROFILS",
        "",
        "",
        "",
        "",
        "" };
    pro.entreeProfils= 1;
    pro.zoneStock= etude.reseau().hasZonesStockage() ? 1 : 0;
    pro.nbProfils= etude.reseau().nbProfils();
    ecritProfilsBief();
  }
  private void ecritProfilsBief() {
    SParametresPRO pro=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresPRO();
    IEtude1d etude= calculHydraulique1d_.etude();
    int nbBiefs= etude.reseau().biefs().length;
    pro.profilsBief= new SParametresBiefBlocPRO[etude.reseau().nbProfils()];
    int i= 0;
    for (int b= 0; b < nbBiefs; b++) {
      IProfil[] profils= etude.reseau().biefs()[b].profils();
      IZoneFrottement[] zones= etude.reseau().biefs()[b].zonesFrottement();
      for (int p= 0; p < profils.length; p++) {
        pro.profilsBief[i]= new SParametresBiefBlocPRO();
        pro.profilsBief[i].indice= profils[p].numero();
        pro.profilsBief[i].numProfil= profils[p].nom();
        pro.profilsBief[i].abscisse= profils[p].abscisse();
        pro.profilsBief[i].absMajMin= new double[2];
        pro.profilsBief[i].absMajSto= new double[2];
        for (int z= 0; z < zones.length; z++) {
          if (CGlobal.appartient(profils[p].abscisse(), zones[z])) {
            pro.profilsBief[i].coefStrickMajMin= zones[z].coefMineur();
            pro.profilsBief[i].coefStrickMajSto= zones[z].coefMajeur();
          }
        }
        pro.profilsBief[i].nbPoints= profils[p].points().length;
        pro.profilsBief[i].abs= new double[pro.profilsBief[i].nbPoints];
        pro.profilsBief[i].cotes= new double[pro.profilsBief[i].nbPoints];
        for (int j= 0; j < pro.profilsBief[i].nbPoints; j++) {
          pro.profilsBief[i].abs[j]= profils[p].points()[j].x;
          pro.profilsBief[i].cotes[j]= profils[p].points()[j].y;
          if (j == profils[p].indiceLitMinGa()) {
            pro.profilsBief[i].absMajMin[0]= pro.profilsBief[i].abs[j];
            pro.profilsBief[i].coteRivGa= pro.profilsBief[i].cotes[j];
          }
          if (j == profils[p].indiceLitMinDr()) {
            pro.profilsBief[i].absMajMin[1]= pro.profilsBief[i].abs[j];
            pro.profilsBief[i].coteRivDr= pro.profilsBief[i].cotes[j];
          }
          if (j == profils[p].indiceLitMajGa())
            pro.profilsBief[i].absMajSto[0]= pro.profilsBief[i].abs[j];
          if (j == profils[p].indiceLitMajDr())
            pro.profilsBief[i].absMajSto[1]= pro.profilsBief[i].abs[j];
        }
        i++;
      }
    }
  }
}
