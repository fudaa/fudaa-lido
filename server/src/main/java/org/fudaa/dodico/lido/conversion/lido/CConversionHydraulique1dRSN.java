/*
 * @file         CConversionHydraulique1dRSN.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IDescriptionVariable;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.IResultatsGeneraux;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculee;
import org.fudaa.dodico.corba.hydraulique1d.ISectionCalculeePasTemps;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IResultatsLidoHelper;
import org.fudaa.dodico.corba.lido.SResultatsBiefLigneRSN;
import org.fudaa.dodico.corba.lido.SResultatsBiefRSN;
import org.fudaa.dodico.corba.lido.SResultatsRSN;
import org.fudaa.dodico.corba.lido.SResultatsTempRSN;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.7 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dRSN {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dRSN(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritRSN() {
    SResultatsRSN rsn=
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsRSN();
    if (rsn == null) {
      rsn= new SResultatsRSN();
      IResultatsLidoHelper.narrow(calculLido_.resultats(connexion_)).resultatsRSN(rsn);
    }
    System.err.println("ecritRSN");
    IEtude1d etude= calculHydraulique1d_.etude();
    int numSections= 1;
    int nbPasTemps= etude.reseau().nbPasTempsResultats();
    rsn.pasTemps= new SResultatsTempRSN[nbPasTemps];
    IBief[] biefs= etude.reseau().biefs();
    for (int t= 0; t < nbPasTemps; t++) {
      rsn.pasTemps[t]= new SResultatsTempRSN();
      if (biefs.length > 0) {
        rsn.pasTemps[t].info=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().info();
        rsn.pasTemps[t].np=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().numeroPas();
        rsn.pasTemps[t].t=
          (int)biefs[0].resultatsBief().pasTemps()[t].infoTemps().t();
        rsn.pasTemps[t].dt=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().dt();
        rsn.pasTemps[t].dtlevy=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().dtlevy();
        rsn.pasTemps[t].jour=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().jour();
        rsn.pasTemps[t].heure=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().heure();
        rsn.pasTemps[t].minute=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().minute();
        rsn.pasTemps[t].seconde=
          biefs[0].resultatsBief().pasTemps()[t].infoTemps().seconde();
      }
      SResultatsBiefRSN[] sbiefs= new SResultatsBiefRSN[biefs.length];
      for (int b= 0; b < biefs.length; b++) {
        sbiefs[b]= new SResultatsBiefRSN();
        sbiefs[b].numero= biefs[b].numero();
        sbiefs[b].i1= numSections;
        ISectionCalculee[] sections= biefs[b].sectionsCalculees();
        if (sections == null)
          continue;
        SResultatsBiefLigneRSN[] ssections=
          new SResultatsBiefLigneRSN[sections.length];
        for (int s= 0; s < sections.length; s++) {
          ssections[s]= new SResultatsBiefLigneRSN();
          ssections[s].i= numSections++;
          ssections[s].x= sections[s].abscisse();
          remplitResultatsSection(ssections[s], sections[s].pasTemps()[t]);
        }
        sbiefs[b].i2= numSections - 1;
        sbiefs[b].ligne= ssections;
        sbiefs[b].volum= biefs[b].resultatsBief().pasTemps()[t].volum();
        sbiefs[b].volums= biefs[b].resultatsBief().pasTemps()[t].volums();
        sbiefs[b].vbief= biefs[b].resultatsBief().pasTemps()[t].vbief();
        sbiefs[b].vappo= biefs[b].resultatsBief().pasTemps()[t].vappo();
        sbiefs[b].vperdu= biefs[b].resultatsBief().pasTemps()[t].vperdu();
      }
      rsn.pasTemps[t].ligBief= sbiefs;
    }
  }
  private void remplitResultatsSection(
    SResultatsBiefLigneRSN ssection,
    ISectionCalculeePasTemps section) {
    IResultatsGeneraux res= calculHydraulique1d_.etude().resultatsGeneraux();
    IDescriptionVariable[] vars=
      res.resultatsTemporelSpatial().descriptionVariables();
    for (int v= 0; v < vars.length; v++) {
      if ("Z".equals(vars[v].nom()))
        ssection.z= section.valeurs()[v];
      else if ("Y".equals(vars[v].nom()))
        ssection.y= section.valeurs()[v];
      else if ("S1".equals(vars[v].nom()))
        ssection.s1= section.valeurs()[v];
      else if ("S2".equals(vars[v].nom()))
        ssection.s2= section.valeurs()[v];
      else if ("R1".equals(vars[v].nom()))
        ssection.r1= section.valeurs()[v];
      else if ("R2".equals(vars[v].nom()))
        ssection.r2= section.valeurs()[v];
      else if ("B1".equals(vars[v].nom()))
        ssection.b1= section.valeurs()[v];
      else if ("VMIN".equals(vars[v].nom()))
        ssection.vmin= section.valeurs()[v];
      else if ("VMAJ".equals(vars[v].nom()))
        ssection.vmaj= section.valeurs()[v];
      else if ("Q".equals(vars[v].nom()))
        ssection.q= section.valeurs()[v];
      else if ("FROUDE".equals(vars[v].nom()))
        ssection.froude= section.valeurs()[v];
      else if ("QMIN".equals(vars[v].nom()))
        ssection.qmin= section.valeurs()[v];
      else if ("QMAJ".equals(vars[v].nom()))
        ssection.qmaj= section.valeurs()[v];
      else if ("CHARGE".equals(vars[v].nom()))
        ssection.charge= section.valeurs()[v];
    }
  }
}
