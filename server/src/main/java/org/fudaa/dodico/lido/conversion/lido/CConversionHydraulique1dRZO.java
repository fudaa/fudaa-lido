/*
 * @file         CConversionHydraulique1dRZO.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.IBief;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.IExtremite;
import org.fudaa.dodico.corba.hydraulique1d.ILimite;
import org.fudaa.dodico.corba.hydraulique1d.ILoiHydraulique;
import org.fudaa.dodico.corba.hydraulique1d.INoeud;
import org.fudaa.dodico.corba.hydraulique1d.ISingularite;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiHydrogramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiLimnigramme;
import org.fudaa.dodico.corba.hydraulique1d.loi.ILoiTarage;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.corba.objet.IConnexion;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.8 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class CConversionHydraulique1dRZO {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dRZO(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritRZO() {
    SParametresRZO rzo=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresRZO();
    if (rzo == null) {
      rzo= new SParametresRZO();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresRZO(rzo);
    }
    System.err.println("ecritRZO");
    IEtude1d etude= calculHydraulique1d_.etude();
    rzo.status= true;
    rzo.titreRZO= new String[] { "ETUDE LIDO 2.0 : DEFINITION DU RESEAU", "" };
    IBief[] biefs= etude.reseau().biefs();
    INoeud[] noeuds= etude.reseau().noeudsConnectesBiefs();
    IExtremite[] limites= etude.reseau().extremitesLibres();
    rzo.nbBief= biefs.length;
    rzo.blocSitus= new SParametresBiefSituBlocRZO();
    rzo.blocSitus.ligneSitu= new SParametresBiefSituLigneRZO[biefs.length];
    for (int i= 0; i < biefs.length; i++) {
      rzo.blocSitus.ligneSitu[i]= new SParametresBiefSituLigneRZO();
      rzo.blocSitus.ligneSitu[i].x1=
        biefs[i].extrAmont().profilRattache().abscisse();
      rzo.blocSitus.ligneSitu[i].x2=
        biefs[i].extrAval().profilRattache().abscisse();
      rzo.blocSitus.ligneSitu[i].numBief= i;
      rzo.blocSitus.ligneSitu[i].branch1= biefs[i].extrAmont().numero();
      rzo.blocSitus.ligneSitu[i].branch2= biefs[i].extrAval().numero();
    }
    rzo.nbNoeud= noeuds.length;
    rzo.blocNoeuds= new SParametresBiefNoeudBlocRZO();
    rzo.blocNoeuds.ligneNoeud= new SParametresBiefNoeudLigneRZO[noeuds.length];
    if (rzo.nbNoeud > 0) {
      for (int i= 0; i < noeuds.length; i++) {
        IExtremite[] extr= noeuds[i].extremites();
        rzo.blocNoeuds.ligneNoeud[i]= new SParametresBiefNoeudLigneRZO();
        rzo.blocNoeuds.ligneNoeud[i].noeud= new int[5];
        for (int e= 0; e < extr.length; e++) {
          rzo.blocNoeuds.ligneNoeud[i].noeud[e]= extr[e].numero();
        }
      }
    }
    rzo.nbLimi= limites.length;
    rzo.blocLims= new SParametresBiefLimBlocRZO();
    rzo.blocLims.ligneLim= new SParametresBiefLimLigneRZO[limites.length];
    if (rzo.nbLimi > 0) {
      for (int i= 0; i < limites.length; i++) {
        rzo.blocLims.ligneLim[i]= new SParametresBiefLimLigneRZO();
        rzo.blocLims.ligneLim[i].numExtBief= limites[i].numero();
        ILimite lim= limites[i].conditionLimite();
        if (lim != null) {
          ILoiHydraulique loi= limites[i].conditionLimite().loi();
          rzo.blocLims.ligneLim[i].numLoi= loi.numero();
          rzo.blocLims.ligneLim[i].typLoi=
            (loi instanceof ILoiHydrogramme)
              ? 1
              : (loi instanceof ILoiLimnigramme)
              ? 2
              : (loi instanceof ILoiTarage)
              ? 3
              : 0;
        }
      }
    }
    rzo.blocSings= new SParametresBiefSingBlocRZO();
    rzo.blocSings.ligneSing= new SParametresBiefSingLigneRZO[biefs.length];
    for (int i= 0; i < biefs.length; i++) {
      rzo.blocSings.ligneSing[i]= new SParametresBiefSingLigneRZO();
      ISingularite[] s= biefs[i].singularites();
      if ((s != null) && (s.length > 0)) {
        rzo.blocSings.ligneSing[i].numBief= biefs[i].numero();
        rzo.blocSings.ligneSing[i].xSing= s[0].abscisse();
        rzo.blocSings.ligneSing[i].nSing= s[0].getLoi().numero();
      }
    }
  }
}
