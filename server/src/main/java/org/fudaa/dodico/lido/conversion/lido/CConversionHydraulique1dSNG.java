/*
 * @file         CConversionHydraulique1dSNG.java
 * @creation     2000-08-17
 * @modification $Date: 2005-08-16 13:04:41 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.lido.conversion.lido;
import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.hydraulique1d.IEtude1d;
import org.fudaa.dodico.corba.hydraulique1d.ISingularite;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilDenoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilGeometrique;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilLimniAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilNoye;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAmont;
import org.fudaa.dodico.corba.hydraulique1d.singularite.ISeuilTarageAval;
import org.fudaa.dodico.corba.lido.ICalculLido;
import org.fudaa.dodico.corba.lido.IParametresLidoHelper;
import org.fudaa.dodico.corba.lido.SParametresSNG;
import org.fudaa.dodico.corba.lido.SParametresSingBlocSNG;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.lido.conversion.LidoResource;
/**
 * Classe Conversion de calculhydraulique1d en calculLido
 *
 * @version      $Revision: 1.8 $ $Date: 2005-08-16 13:04:41 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class CConversionHydraulique1dSNG {
  private ICalculLido calculLido_;
  private ICalculHydraulique1d calculHydraulique1d_;
  private IConnexion connexion_;
  public CConversionHydraulique1dSNG(
    ICalculHydraulique1d calculHydraulique1d,
    IConnexion c) {
    calculHydraulique1d_= calculHydraulique1d;
    connexion_= c;
    calculLido_= (ICalculLido)calculHydraulique1d.calculCode();
  }
  public void ecritSNG() {
    SParametresSNG sng=
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresSNG();
    if (sng == null) {
      sng= new SParametresSNG();
      (IParametresLidoHelper.narrow(calculLido_.parametres(connexion_))).parametresSNG(sng);
    }
    System.err.println("ecritSNG");
    IEtude1d etude= calculHydraulique1d_.etude();
    sng.status= true;
    sng.titre= "ETUDE LIDO 2.0 : SINGULARITES";
    ISingularite sings[]= etude.reseau().seuils();
    sng.nbMaxParam= 5;
    sng.nbValTD= 20;
    for (int i= 0; i < sings.length; i++) {
      int type= typeLoiSNG(sings[i]);
      if (type < 0)
        continue;
      SParametresSingBlocSNG ss= new SParametresSingBlocSNG();
      ss.titre= sings[i].nom();
      ss.numSing= sings[i].numero();
      ss.tabParamEntier= new int[sng.nbMaxParam];
      ss.tabParamReel= new double[sng.nbMaxParam];
      ss.tabParamEntier[LidoResource.SINGULARITE.ITYPE]= type;
    }
  }
  private int typeLoiSNG(ISingularite sing) {
    int res= -1;
    if (sing instanceof ISeuilNoye)
      res= LidoResource.SINGULARITE.SEUIL_NOYE;
    else if (sing instanceof ISeuilDenoye)
      res= LidoResource.SINGULARITE.SEUIL_DENOYE;
    else if (sing instanceof ISeuilGeometrique)
      res= LidoResource.SINGULARITE.SEUIL_GEOM;
    else if (sing instanceof ISeuilLimniAmont)
      res= LidoResource.SINGULARITE.LIMNI_AMONT;
    else if (sing instanceof ISeuilTarageAmont)
      res= LidoResource.SINGULARITE.TARAGE_AMONT;
    else if (sing instanceof ISeuilTarageAval)
      res= LidoResource.SINGULARITE.TARAGE_AVAL;
    return res;
  }
}
