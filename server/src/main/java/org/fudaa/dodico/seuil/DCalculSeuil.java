/**
 * @file         DCalculSeuil.java
 * @creation     2000-07-20
 * @modification $Date: 2006-09-19 14:45:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.seuil;

import java.io.File;

import com.memoire.fu.FuLib;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.seuil.ICalculSeuil;
import org.fudaa.dodico.corba.seuil.ICalculSeuilOperations;
import org.fudaa.dodico.corba.seuil.IParametresSeuil;
import org.fudaa.dodico.corba.seuil.IParametresSeuilHelper;
import org.fudaa.dodico.corba.seuil.IResultatsSeuil;
import org.fudaa.dodico.corba.seuil.IResultatsSeuilHelper;
import org.fudaa.dodico.corba.seuil.SResultatsSEU;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;

/**
 * Classe Calcul de Seuil.
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 14:45:57 $ by $Author: deniger $
 * @author Jean-Yves Riou
 */
public class DCalculSeuil extends DCalcul implements ICalculSeuil, ICalculSeuilOperations {
  public DCalculSeuil() {
    super();
    setFichiersExtensions(new String[] { ".seu", "" });
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DCalculSeuil();
  }

  public String toString() {
    return "DCalculSeuil()";
  }

  public String description() {
    return "Seuil, serveur de calcul: " + super.description();
  }

  public void calcul(final IConnexion _c) {
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresSeuil params = IParametresSeuilHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsSeuil results = IResultatsSeuilHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    log(_c, "lancement du calcul");
    final int noEtude = _c.numero();
    final String path = cheminServeur();
    final File fic01 = getFichier(_c, ".seu");
    final File ficRes = getFichier(_c, "");
    try {
      DParametresSeuil.ecritParametresSEU(fic01, params.parametresSEU());
      System.out.println("Appel de l'executable seuil pour l'etude " + noEtude);
      String[] cmd;
      final String espace = CtuluLibString.ESPACE;
      if (FuLib.isWindows()) {
        cmd = new String[4];
        cmd[0] = path + "seuil-win.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1] = path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2] = path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[1] = "fake_cmd";
          cmd[2] = path;
        }
        cmd[3] = "" + noEtude;
        System.out.println(cmd[0] + espace + cmd[1] + espace + cmd[2] + espace + cmd[3]);
      } else {
        cmd = new String[3];
        cmd[0] = path + "seuil.sh";
        cmd[1] = path;
        cmd[2] = "" + noEtude;
        System.out.println(cmd[0] + espace + cmd[1] + espace + cmd[2]);
      }
      final CExec ex = new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      ex.exec();
      System.out.println("Fin du calcul");
      System.out.println("Les resultats sont dans seuil" + noEtude);
      System.out.println("Lecture des resultats");
      final SResultatsSEU sres = DResultatsSeuil.litResultatsSEU(ficRes);
      results.resultatsSEU(sres);
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
