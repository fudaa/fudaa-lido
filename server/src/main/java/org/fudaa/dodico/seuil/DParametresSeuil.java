/**
 * @file         DParametresSeuil.java
 * @creation     2000-05-26
 * @modification $Date: 2007-02-26 16:01:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.seuil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.seuil.IParametresSeuil;
import org.fudaa.dodico.corba.seuil.IParametresSeuilOperations;
import org.fudaa.dodico.corba.seuil.SParametresSEU;
import org.fudaa.dodico.corba.seuil.SPoint01;
import org.fudaa.dodico.corba.seuil.SProfelem01;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;

/**
 * Les parametres Seuil.
 * 
 * @version $Revision: 1.10 $ $Date: 2007-02-26 16:01:45 $ by $Author: clavreul $
 * @author Jean-Yves Riou
 */
public class DParametresSeuil extends DParametres implements IParametresSeuil, IParametresSeuilOperations {
  static final int NPTMAX = 80; // nbre pts max par profil (dans le fortran = 75 )
  //static String CHAINW;
  private SParametresSEU paramsSEU_ ;

  public DParametresSeuil() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DParametresSeuil();
  }

  public SParametresSEU parametresSEU() {
    return paramsSEU_;
  }

  public void parametresSEU(final SParametresSEU _p) {
    paramsSEU_ = _p;
  }

  public static void ecritParametresSEU(final OutputStream _f01, final SParametresSEU _params) throws IOException {
    System.out.println("Ecriture des parametres de Seuil");
    final FortranWriter f01 = new FortranWriter(new OutputStreamWriter(_f01));
    if (System.getProperty("os.name").startsWith("Windows")) {
      f01.setLineSeparator("\r\n"); // fichiers DOS (\r)
    } else {
      f01.setLineSeparator("\n");
    }
    // String chainw = new String[];
    int[] fmtD=new int[] { 40, 40 };
    int[] fmtP= new int[] { 12, 28, 20, 20 };
    _params.sortieEcran = "N";
    f01.stringField(0, _params.sortieEcran);
    f01.stringField(1, "Titre du passageType de sortie");
    f01.writeFields(fmtD);
    f01.stringField(0, _params.titreEtude);
    f01.stringField(1, "Titre du passage");
    f01.writeFields(fmtD);
    String espaces = "  ";
    // ==============
    // profil amont
    // =============
    f01.stringField(1, espaces);
    // on suppose que la verification sur nb pts 4< nbpt <= 75
    // est deja faite
    f01.stringField(1, espaces);
    String chainw;
    writeProfilAmont(_params, f01, fmtP);
    // ligne blanche pour separer les profils
    f01.stringField(0, "");
    f01.stringField(1, "FIN PROFIL AMONT");
    f01.writeFields(fmtD);
    // ==============
    // profil aval
    // =============
    f01.stringField(1, espaces);
    // on suppose que la verification sur nb pts 4< nbpt <= 75
    // est deja faite
    writeProfAval(_params, f01, fmtP);
    // ligne blanche pour separer les profils
    f01.stringField(0, "");
    f01.stringField(1, "FIN PROFIL AVAL");
    f01.writeFields(fmtD);
    // =====================
    // Description du seuil
    // =====================
    f01.stringField(1, espaces);
    f01.stringField(3, espaces);
    // f01.doubleField(0, _params.pelle);
    chainw = Double.toString(_params.pelle);
    f01.stringField(0, chainw);
    f01.stringField(2, "PELLE AMONT");
    f01.writeFields(fmtP);
    // f01.doubleField(0, _params.epaisCret);
    chainw = Double.toString(_params.epaisCret);
    f01.stringField(0, chainw);
    f01.stringField(2, "EPAISSEUR DE LA CRETE");
    f01.writeFields(fmtP);
    // f01.doubleField(0, _params.largEcoul);
    chainw = Double.toString(_params.largEcoul);
    f01.stringField(0, chainw);
    f01.stringField(2, "Largeur D ECOULEMENT");
    f01.writeFields(fmtP);
    // =====================
    // Donnees hydrauliques
    // =====================
    f01.stringField(1, espaces);
    f01.stringField(3, espaces);
    // on suppose que la verification de la coherence entre les cotes
    // a deja ete faite
    // f01.doubleField(0, _params.debit);
    chainw = Double.toString(_params.debit);
    f01.stringField(0, chainw);
    f01.stringField(2, "DEBIT ESTIME");
    f01.writeFields(fmtP);
    // f01.doubleField(0, _params.coteAmont);
    chainw = Double.toString(_params.coteAmont);
    f01.stringField(0, chainw);
    f01.stringField(2, "COTE AMONT ESTIMEE");
    f01.writeFields(fmtP);
    // f01.doubleField(0, _params.coteAval);
    chainw = Double.toString(_params.coteAval);
    f01.stringField(0, chainw);
    f01.stringField(2, "COTE AVAL");
    f01.writeFields(fmtP);
    // =========================================
    // le programme repond etes -vous d'accord
    // et on repond o(ui)
    // ========================================
    f01.stringField(0, "o");
    f01.stringField(1, "Etes-vous d accord ?");
    f01.writeFields(fmtD);
    // ======================================
    // donnees complementaires pour le seuil
    // ======================================
    // on suppose que la discussion a deja ete faite et
    // que l'on peut se servir de typCret
    System.out.println(" _params.typeCret  = " + _params.typeCret);
    if (_params.typeCret.equals("epais")) {
      writeEpais(_params, f01, fmtD, fmtP, espaces);
    } else if (_params.typeCret.equals("mince")) {
      writeMince(_params, f01, fmtD, fmtP, espaces);
    }
    // ==================================
    // on termine par une ligne blanche
    // pour repondre a Debit ? pour le fortran
    // ===================================
    f01.stringField(0, " ");
    f01.stringField(1, "Debit ?");
    f01.writeFields(fmtD);
    // ==================================
    // on ecrit commentaire
    // et noms de profils
    // ===================================
    if (_params.comentEtude.length() == 0) {
      _params.comentEtude = "   ";
    }
    if (_params.profAmont.nomProfil.length() == 0) {
      _params.profAmont.nomProfil = "   ";
    }
    if (_params.profAval.nomProfil.length() == 0) {
      _params.profAval.nomProfil = "   ";
    }
    f01.stringField(0, _params.comentEtude);
    f01.stringField(1, "Commentaire ");
    f01.writeFields(fmtD);
    f01.stringField(0, _params.profAmont.nomProfil);
    f01.stringField(1, "Profil amont");
    f01.writeFields(fmtD);
    f01.stringField(0, _params.profAval.nomProfil);
    f01.stringField(1, "Profil aval");
    f01.writeFields(fmtD);
    f01.flush();
  }

  private static void writeMince(final SParametresSEU _params, final FortranWriter _f01, int[] _fmtD, int[] _fmtP, String _espaces) throws IOException {
    String chainw;
    // cas seuil mince
    // =======================
    System.out.println(" passe par mince ");
    if (_params.oblicite == 0.) {
      _f01.stringField(0, " ");
      _f01.stringField(1, "OBLICITE");
      _f01.writeFields(_fmtD);
    } else {
      _f01.stringField(1, _espaces);
      _f01.stringField(3, _espaces);
      // f01.doubleField(0, _params.oblicite);
      chainw = Double.toString(_params.oblicite);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "OBLICITE");
      _f01.writeFields(_fmtP);
    }
    if (_params.oblicite == 0.) {
      _f01.stringField(0, " ");
      _f01.stringField(1, "INCLINAISON");
      _f01.writeFields(_fmtD);
    } else {
      _f01.stringField(1, _espaces);
      _f01.stringField(3, _espaces);
      // f01.doubleField(0, _params.inclinaison);
      chainw = Double.toString(_params.inclinaison);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "INCLINAISON");
      _f01.writeFields(_fmtP);
    }
  }

  private static void writeProfAval(final SParametresSEU _params, final FortranWriter _f01, int[] _fmtP) throws IOException {
    String chainw;
    for (int i = 0; i < _params.profAval.nbPoints; i++) {
      // f01.doubleField(0, _params.profAval.xy[i].x);
      chainw = Double.toString(_params.profAval.xy[i].x);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "Abscisse du point");
      _f01.intField(3, i + 1);
      _f01.writeFields(_fmtP);
      // f01.doubleField(0, _params.profAval.xy[i].y);
      chainw = Double.toString(_params.profAval.xy[i].y);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "Ordonnee du point");
      _f01.intField(3, i + 1);
      _f01.writeFields(_fmtP);
    }
  }

  private static void writeProfilAmont(final SParametresSEU _params, final FortranWriter _f01, int[] _fmtP) throws IOException {
    String chainw;
    for (int i = 0; i < _params.profAmont.nbPoints; i++) {
      // f01.doubleField(0, _params.profAmont.xy[i].x);
      chainw = Double.toString(_params.profAmont.xy[i].x);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "Abcisse du point");
      _f01.intField(3, i + 1);
      _f01.writeFields(_fmtP);
      // f01.doubleField(0, _params.profAmont.xy[i].y);
      chainw = Double.toString(_params.profAmont.xy[i].y);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "Ordonnee du point");
      _f01.intField(3, i + 1);
      _f01.writeFields(_fmtP);
    }
  }

  private static void writeEpais(final SParametresSEU _params, final FortranWriter _f01, int[] _fmtD, int[] _fmtP, String _string) throws IOException {
    String chainw;
    // ======================
    // cas seuil epais
    // =======================
    System.out.println(" passe par epais ");
    if (_params.rayonCourb == 0.) {
      _f01.stringField(0, "d");
      _f01.stringField(1, "REBORD DROIT");
      _f01.writeFields(_fmtD);
    } else {
      _f01.stringField(0, "a");
      _f01.stringField(1, "REBORD ARRONDI");
      _f01.writeFields(_fmtD);
      _f01.stringField(1, _string);
      _f01.stringField(3, _string);
      // f01.doubleField(0, _params.rayonCourb);
      chainw = Double.toString(_params.rayonCourb);
      _f01.stringField(0, chainw);
      _f01.stringField(2, "RAYON DE COURBURE");
      _f01.writeFields(_fmtP);
    }
  }

  public static void ecritParametresSEU(final File _f01, final SParametresSEU _params) throws IOException {
    final FileOutputStream fichier = new FileOutputStream(_f01);
    ecritParametresSEU(fichier, _params);
    fichier.close();
  }

  public static void ecritParametresSEU(final SParametresSEU _params) throws IOException {
    ecritParametresSEU(new File("seuil" + ".01"), _params);
  }

  public static SParametresSEU litParametresSEU(final InputStream _f01) throws IOException {
    System.out.println("Lecture des parametres 01");
    final SParametresSEU params = new SParametresSEU();
    final FortranReader f01 = new FortranReader(new InputStreamReader(_f01));
    int i;
    int[] fmtD= new int[] { 40, 40 };
    //
    // il faut tout allouer avant de rentrer
    // dans les boucles while d'affectation
    //
    params.profAmont = new SProfelem01();
    params.profAmont.xy = new SPoint01[NPTMAX];
    params.profAval = new SProfelem01();
    params.profAval.xy = new SPoint01[NPTMAX];
    // il faut affecter les variablles qui ne transitent
    // pas par le fichier
    // params.profAmont/Aval.nbPoints defini plus bas
    params.profAmont.nomProfil = "";
    params.profAval.nomProfil = "";
    for (i = 0; i < NPTMAX; i++) {
      params.profAmont.xy[i] = new SPoint01();
      params.profAval.xy[i] = new SPoint01();
    }
    f01.readFields(fmtD);
    params.sortieEcran = f01.stringField(0);
    f01.readFields(fmtD);
    params.titreEtude = f01.stringField(0);
    // System.out.println(f01.stringField(0));
    System.out.println(params.titreEtude);
    // ==============
    // profil amont
    // =============
    i = 0;
    f01.readFields(fmtD);
    while (f01.stringField(0).trim().length() > 0) {
      // System.out.println(f01.stringField(0));
      params.profAmont.xy[i].x = Double.parseDouble(f01.stringField(0));
      System.out.println(params.profAmont.xy[i].x);
      f01.readFields(fmtD); // si ligne blanche on est cuit!
      if (f01.stringField(0).trim().length() > 0) {
        params.profAmont.xy[i].y = Double.parseDouble(f01.stringField(0));
        System.out.println(params.profAmont.xy[i].y);
        f01.readFields(fmtD);
        i = i + 1;
      }
    }
    // on vient de lire une ligne blanche
    params.profAmont.nbPoints = i;
    System.out.println(params.profAmont.nbPoints);
    // ==============
    // profil aval
    // =============
    i = 0;
    f01.readFields(fmtD);
    while (f01.stringField(0).trim().length() > 0) {
      params.profAval.xy[i].x = Double.parseDouble(f01.stringField(0));
      System.out.println(params.profAval.xy[i].x);
      f01.readFields(fmtD); // si ligne blanche on est cuit!
      if (f01.stringField(0).trim().length() > 0) {
        params.profAval.xy[i].y = Double.parseDouble(f01.stringField(0));
        System.out.println(params.profAval.xy[i].y);
        f01.readFields(fmtD);
        i = i + 1;
      }
    }
    // on vient de lire une ligne blanche
    params.profAval.nbPoints = i;
    System.out.println(params.profAval.nbPoints);
    // =====================
    // Description du seuil
    // =====================
    f01.readFields(fmtD);
    params.pelle = f01.doubleField(0);
    f01.readFields(fmtD);
    params.epaisCret = f01.doubleField(0);
    f01.readFields(fmtD);
    params.largEcoul = f01.doubleField(0);
    // =====================
    // Donnees hydrauliques
    // =====================
    f01.readFields(fmtD);
    params.debit = f01.doubleField(0);
    f01.readFields(fmtD);
    params.coteAmont = f01.doubleField(0);
    f01.readFields(fmtD);
    params.coteAval = f01.doubleField(0);
    // =============================
    // lecture du "o" de la reponse
    // =============================
    f01.readFields(fmtD);
    // ======================================
    // donnees complementaires pour le seuil
    // ======================================
    f01.readFields(fmtD);
    // si "d" ou "a" on est en seuil epais
    // sinon on est en seuil mince
    if (f01.stringField(0).equals("a") || f01.stringField(0).equals("d")) {
      // ========================
      // cas seuil epais
      // =======================
      params.typeCret = "epais";
      if (f01.stringField(0).equals("a")) {
        f01.readFields(fmtD);
        params.rayonCourb = f01.doubleField(0);
      } else {
        params.rayonCourb = 0.;
      }
      System.out.println("seuil epais R = " + params.rayonCourb);
    } else {
      // =======================
      // cas seuil mince
      // =======================
      params.typeCret = "mince";
      // on vient de lire l'oblicite " " ou nombre
      if (f01.stringField(0).trim().length() == 0) {
        params.rayonCourb = 0.;
      } else {
        params.rayonCourb = Double.parseDouble(f01.stringField(0));
      }
      f01.readFields(fmtD);
      // on lit l inclinaison ou " "
      if (f01.stringField(0).trim().length() == 0) {
        params.oblicite = 0.;
      } else {
        params.oblicite = Double.parseDouble(f01.stringField(0));
      }
    }
    // on ne lit pas la derniere ligne blanche, ne sert � rien
    // si on la lit car apres il y commentaire et noms des profils
    f01.readFields(fmtD);
    // ==================================
    // on lit commentaire
    // et noms de profils
    // ===================================
    f01.readFields(fmtD);
    params.comentEtude = f01.stringField(0);
    f01.readFields(fmtD);
    params.profAmont.nomProfil = f01.stringField(0);
    f01.readFields(fmtD);
    params.profAmont.nomProfil = f01.stringField(0);
    System.out.println("*** fin lecture parametres  ***");
    return params;
  }

  public static SParametresSEU litParametresSEU(final File _f01) throws IOException {
    SParametresSEU res = null;
    final FileInputStream fichier = new FileInputStream(_f01);
    res = litParametresSEU(fichier);
    fichier.close();
    return res;
  }

  public static SParametresSEU litParametresSEU() throws IOException {
    return litParametresSEU(new File("seuil01" + ".seu"));
  }
}
