/**
 * @file         DResultatsSeuil.java
 * @creation     2000-07-19
 * @modification $Date: 2006-09-19 14:45:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.seuil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.seuil.IResultatsSeuil;
import org.fudaa.dodico.corba.seuil.IResultatsSeuilOperations;
import org.fudaa.dodico.corba.seuil.SResultatsSEU;
import org.fudaa.dodico.fortran.FortranReader;
/**
 * Les resultats Seuil.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:45:57 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class DResultatsSeuil
  extends DResultats
  implements IResultatsSeuil,IResultatsSeuilOperations {
  private SResultatsSEU resultsSEU_;
  public DResultatsSeuil() {
    super();
  }
  public final  Object clone() throws CloneNotSupportedException {
    return new DResultatsSeuil();
  }
  public String toString() {
    return "DResultatsSeuil()";
  }
  public SResultatsSEU resultatsSEU() {
    //System.out.println("SResultatsSEU path"   +path);
    //System.out.println("SResultatsSEU noEtude"+noEtude);
    //System.out.println("SResultatsSEU resultsSEU = "+resultsSEU);
    return resultsSEU_;
  }
  public void resultatsSEU(final SResultatsSEU _p) {
    resultsSEU_= _p;
  }
  //=============================================================
  public static SResultatsSEU litResultatsSEU(final InputStream _f02)
    throws IOException {
    System.out.println("Lecture des Resultats litResultatsSEU ");
    final SResultatsSEU results= new SResultatsSEU();
    final FortranReader f02= new FortranReader(new InputStreamReader(_f02));
    int[] fmtT= new int[] { 20, 55 }; //titre
    int[]  fmtD1= new int[] { 47, 9 }; //epaisseur..hc
    int[]  fmtS1= new int[] { 31, 16 }; //type crete et rebord
    int[] fmtS2= new int[] { 44, 13 }; //methode ... regime + attention nb iter (int)
    int[] fmtD2= new int[] { 9, 13, 1, 13, 1, 13, 1, 13 }; //R/h ....L/B
    int[] fmtD3= new int[] { 44, 11 }; //Q .....coef
    //====================
    // Rappel de donnees
    //====================
    // on saute 13 lignes
    for (int i= 0; i < 9; i++) {
      f02.readFields(fmtT);
    }
    // titre du passage
    f02.readFields(fmtT);
    results.titreEtude= f02.stringField(1);
    //System.out.println(" litResultatsSEU() titreEtude ="+results.titreEtude);
    // on saute une ligne
    f02.readFields(fmtT);
    // pelle .... hc
    f02.readFields(fmtD1);
    results.pelle= f02.doubleField(1);
    // System.out.println(" litResultatsSEU() pelle = "+ results.pelle);
    f02.readFields(fmtD1);
    results.epaisCret= f02.doubleField(1);
    // System.out.println(" litResultatsSEU() epaisCret = "+ results.epaisCret);
    f02.readFields(fmtD1);
    results.largEcoul= f02.doubleField(1);
    // System.out.println(" litResultatsSEU() largEcoul = "+ results.largEcoul);
    f02.readFields(fmtD1);
    results.debit= f02.doubleField(1);
    f02.readFields(fmtD1);
    results.coteAmont= f02.doubleField(1);
    f02.readFields(fmtD1);
    results.coteAval= f02.doubleField(1);
    f02.readFields(fmtD1);
    results.hc= f02.doubleField(1);
    // System.out.println(" litResultatsSEU() hc= "+ results.hc);
    // on saute une ligne
    f02.readFields(fmtT);
    // type de crete et rebord
    f02.readFields(fmtS1);
    results.typeCret= f02.stringField(1);
    System.out.println(" litResultatsSEU() typeCret= " + results.typeCret);
    f02.readFields(fmtS1);
    if (results.typeCret.equals("epaisse")) {
      results.rebord= f02.stringField(1);
      System.out.println(" litResultatsSEU() rebord= " + results.rebord);
      if (results.rebord.equals("arron")) {
        //====================
        // Resultats
        //====================
        // on saute 14 lignes
        for (int i= 0; i < 14; i++) {
          f02.readFields(fmtT);
        }
      } else // rebord droit
        {
        //====================
        // Resultats
        //====================
        // on saute 13 lignes
        for (int i= 0; i < 13; i++) {
          f02.readFields(fmtT);
        }
      }
    } else // type mince
      {
      results.rebord= "droit"; // pour dessin des resultats dans graphe.java
      System.out.println(" mince reconnu ");
      //====================
      // Resultats
      //====================
      // on saute 14 lignes
      for (int i= 0; i < 14; i++) {
        f02.readFields(fmtT);
      }
    }
    // methode .... nb iter
    f02.readFields(fmtS2);
    results.methode= f02.stringField(1);
    System.out.println(" litResultatsSEU() methode= " + results.methode);
    f02.readFields(fmtS2);
    results.typeCretCal= f02.stringField(1);
    System.out.println(
      " litResultatsSEU() typeCretCal= " + results.typeCretCal);
    f02.readFields(fmtS2);
    results.regime= f02.stringField(1);
    System.out.println(" litResultatsSEU() regime= " + results.regime);
    f02.readFields(fmtS2);
    results.nbIter= Integer.parseInt(f02.stringField(1));
    System.out.println(" litResultatsSEU() nbIter= " + results.nbIter);
    // on saute 8 lignes
    for (int i= 0; i < 8; i++) {
      f02.readFields(fmtT);
    }
    // R.H .... L/B
    f02.readFields(fmtD2);
    results.rsurh= f02.doubleField(1);
    //  System.out.println(" litResultatsSEU() rsurh= "+ results.rsurh);
    results.psurh= f02.doubleField(3);
    results.csurh= f02.doubleField(5);
    results.lsurb= f02.doubleField(7);
    //   System.out.println(" litResultatsSEU() lsurb= "+ results.lsurb);
    // on saute 7 lignes
    for (int i= 0; i < 7; i++) {
      f02.readFields(fmtT);
    }
    // Debit .... coef
    f02.readFields(fmtD3);
    results.debitCal= f02.doubleField(1);
    //  System.out.println(" litResultatsSEU() debitCal= "+ results.debitCal);
    f02.readFields(fmtD3);
    results.cotAval= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.hautAval= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.chargAval= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.cotAmont= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.hautAmont= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.chargAmont= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.deltaCharge= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.deltaCote= f02.doubleField(1);
    f02.readFields(fmtD3);
    results.coefSeuil= f02.doubleField(1);
    //  System.out.println(" litResultatsSEU() coefSeuil= "+ results.coefSeuil);
    // on ne lit pas les dernieres lignes blanches, ne sert � rien
    return results;
  }
  public static SResultatsSEU litResultatsSEU(final File _f02) throws IOException {
    System.out.println(" litResultatsSEU()=> _f02" + _f02);
    SResultatsSEU res= null;
    final FileInputStream fichier= new FileInputStream(_f02);
    res= litResultatsSEU(fichier);
    fichier.close();
    return res;
  }
  public static SResultatsSEU litResultatsSEU() throws IOException {
    System.out.println(" litResultatsSEU()=> Resseuil01");
    return litResultatsSEU(new File("Resseuil01"));
  }
}
