/**
 * @creation     1998-11-19
 * @modification $Date: 2006-09-19 14:45:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.lido;

import org.fudaa.dodico.corba.lido.SParametresCAL;
import org.fudaa.dodico.corba.lido.SParametresLIG;
import org.fudaa.dodico.corba.lido.SParametresRZO;

import java.io.File;
/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:46 $ by $Author: deniger $
 * @author       Mickael Rubens
 */
public final class LidoClient {

  private LidoClient(){

  }
  /**
   * @param _args
   */
  public static void main(String[] _args) {
    SParametresLIG lig;
    /*SParametresPRO pro;
    SResultatsSTA sta;*/
    SParametresCAL cal= new SParametresCAL();
    //SParametresFIC fic= new SParametresFIC();
    SParametresRZO rzo= new SParametresRZO();
    //SParametresSNG sng= new SParametresSNG();
    try {
      rzo=
        DParametresLido.litParametresRZO(
          new File("/home/users/rubens/lido_fic/therain99.rzo"));
      /*pro=*/
      DParametresLido.litParametresPRO(
          new File("/home/users/rubens/lido_fic/therain99.pro"),
          rzo.nbBief);
      cal=
        DParametresLido.litParametresCAL(
          new File("/home/users/rubens/lido_fic/therain99.cal"));
      lig=
        DParametresLido.litParametresLIG(
          new File("/home/users/rubens/lido_fic/therain99.lig"));
      /*sng=*/
        DParametresLido.litParametresSNG(
          new File("/home/users/rubens/lido_fic/therain99.sng"));
      /*fic=*/
        DParametresLido.litParametresFIC(
          new File("/home/users/rubens/lido_fic/lidonp.fic"));
      if (_args[1].equals("CALCUL")) {
        int tMax= new Double(cal.temporel.tMax).intValue();
        int pas2TImp= new Double(cal.temporel.pas2TImp).intValue();
        /*sta=*/
          DResultatsLido.litResultatsSTA(
            new File("/home3/users/rubens/therain99.sta"),
            lig.nbSections,
            lig.nbBief,
            (tMax / pas2TImp),
            cal.temporel.tMax - (cal.temporel.tMax % cal.temporel.pas2TImp),
            lig.delimitBief);
      }
      /*        lido.lireFichier(true);
              lido.parametres(pFic);

              lido.calcule();
              System.out.println("recuperation des resultats dans le client") ;
              SResultatsSTA sta = new SResultatsSTA() ;
              sta = IResultatsLidoHelper.narrow(lido.resultats()).resultatsSTA() ;
        */
      System.out.println("Endofclient........");
    } catch (Exception ex) {
      System.out.println(ex);
    }
  }
  /*public static SParametresLIG getLig() {
    return lig;
  }
  public static SParametresPRO getPro() {
    return pro;
  }
  public static SResultatsSTA getSta() {
    return sta;
  }*/
}
