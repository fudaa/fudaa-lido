/**
 * @creation     1998-11-19
 * @modification $Date: 2006-09-19 14:45:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.lido;

import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
// ATTENTION : NE PAS OUBLIER DE SPECIFIER LE CHEMIN OU SE TROUVE LE SERVEUR
//             (VAR "FUDAA_SERVEUR") SI CE N'EST PAS : "serveurs/lido"
/**
 * @version      $Revision: 1.1 $ $Date: 2006-09-19 14:45:47 $ by $Author: deniger $
 * @author       Mickael Rubens , Axel von Arnim
 */
public final class LidoClientTexte {
  
  private LidoClientTexte(){
    
  }
  /**
   * @param _args chemin
   */
  public static void main(String[] _args) {
    if (_args.length < 1) {
      System.out.println("Usage: LidoClientTexte <chemin>");
      System.out.println(
        "       chemin=ou se trouvent les fichiers de parametres");
      System.exit(1);
    }
    String path= _args[0];
    if (!path.endsWith(File.separator))
      path += File.separator;
    DCalculLido lido;
    System.out.println("LidoClient...");
    //Recherche du serveur Lido
    //    do {
    //      try { 
    //        lido=(ICalculLido)CDodico.findServer("::lido::ICalculLido"); 
    //      } catch(Exception ex) {
    //          lido=null; 
    //      }
    //    } while(lido==null);
    lido= new DCalculLido();
    CDodico.rebind("un-serveur-lido", UsineLib.buildStubFromDObject(lido));
    //    CDodico.getBOA().obj_is_ready(lido);
    SParametresCAL cal= new SParametresCAL();
    SParametresCLM clm= new SParametresCLM();
    //    SParametresFIC fic = new SParametresFIC();    
    SParametresLIG lig= new SParametresLIG();
    SParametresPRO pro= new SParametresPRO();
    SParametresRZO rzo= new SParametresRZO();
    SParametresSNG sng= new SParametresSNG();
    try {
      DParametresLido pFic= new DParametresLido();
      //      DResultatsLido rFic = null;
      System.out.println("Donnez le nom du nouveau fichier d'etude: ");
      BufferedReader bEtu= new BufferedReader(new InputStreamReader(System.in));
      String nomEtu= bEtu.readLine();
      System.out.println("Donnez le nom du nouveau fichier profil: ");
      BufferedReader bPro= new BufferedReader(new InputStreamReader(System.in));
      String nomPro= bPro.readLine();
      System.out.println(
        "Donnez le nom du nouveau fichier archive(permet de reprendre le calcul ulterieurement): ");
      BufferedReader bSto= new BufferedReader(new InputStreamReader(System.in));
      String nomSto= bSto.readLine();
      pFic.parametresFIC(new SParametresFIC());
      pFic.parametresFIC().racineFicPro= nomPro;
      pFic.parametresFIC().racineFicEtu= nomEtu;
      pFic.parametresFIC().ficEcriArchi= nomSto;
      pFic.parametresFIC().status= true;
      cal= DParametresLido.litParametresCAL(new File(path + nomEtu + ".cal"));
      pFic.parametresCAL(cal);
      System.out.println("Voici les donnees temporelles de l'etude " + nomEtu);
      SParametresCAL pCal= null;
      pCal= pFic.parametresCAL();
      System.out.println(
        "T INITIAL                     =" + pCal.temporel.tInit);
      System.out.println(
        "T MAXIMAL                     =" + pCal.temporel.tMax);
      System.out.println(
        "PAS DE TEMPS                  =" + pCal.temporel.pas2T);
      System.out.println(
        "NUMERO DU DERNIER PAS STOCKE  =" + pCal.temporel.numDerPaStoc);
      System.out.println(
        "PAS DE TEMPS D IMPRESSION     =" + pCal.temporel.pas2TImp);
      System.out.println(
        "PAS DE TEMPS DE STOCKAGE      =" + pCal.temporel.pas2TStoc);
      System.out.println("Donnez le nouveau PAS DE TEMPS: ");
      BufferedReader d= new BufferedReader(new InputStreamReader(System.in));
      pCal.temporel.pas2T= Double.valueOf(d.readLine()).intValue();
      System.out.println("Nouveau PAS DE TEMPS     =" + pCal.temporel.pas2T);
      pFic.parametresCAL(pCal);
      clm= DParametresLido.litParametresCLM(new File(path + nomEtu + ".clm"));
      pFic.parametresCLM(clm);
      lig= DParametresLido.litParametresLIG(new File(path + nomEtu + ".lig"));
      pFic.parametresLIG(lig);
      rzo= DParametresLido.litParametresRZO(new File(path + nomEtu + ".rzo"));
      pFic.parametresRZO(rzo);
      pro=
          DParametresLido.litParametresPRO(
          new File(path + nomPro + ".pro"),
          pFic.parametresRZO().nbBief);
      pFic.parametresPRO(pro);
      sng= DParametresLido.litParametresSNG(new File(path + nomEtu + ".sng"));
      pFic.parametresSNG(sng);
      lido.calcul(null);
      //System.out.println("Recuperation des resultats dans le client") ;
      //SResultatsSTA sta = new SResultatsSTA() ;
      //sta = IResultatsLidoHelper.narrow(lido.resultats()).resultatsSTA() ;
      System.out.println("Endofclient........");
    } catch (Exception ex) {
      System.err.println(ex);
    }
  }
}
