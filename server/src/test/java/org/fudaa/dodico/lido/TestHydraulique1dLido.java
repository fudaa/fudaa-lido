/**
 * @creation 2000-09-05
 * @modification $Date: 2006-09-19 14:45:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.lido;

import org.fudaa.dodico.corba.hydraulique1d.ICalculHydraulique1d;
import org.fudaa.dodico.corba.lido.*;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.usine.IUsine;
import org.fudaa.dodico.lido.conversion.CConversionHydraulique1d;
import org.fudaa.dodico.objet.ServeurPersonne;
import org.fudaa.dodico.objet.UsineLib;

import java.io.File;
import java.io.IOException;

/**
 * Une classe de test de conversion hydraulique1d <->lido.
 * 
 * @version $Revision: 1.4 $ $Date: 2006-09-19 14:45:46 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public final class TestHydraulique1dLido {

  private TestHydraulique1dLido() {

  }

  /**
   * @param _args non utilise
   */
  public static void main(String[] _args) {
    UsineLib.setAllLocal(true);
    IUsine usine = UsineLib.findUsine();
    ICalculHydraulique1d chydro = usine.creeHydraulique1dCalculHydraulique1d();
    ICalculLido clido = usine.creeLidoCalculLido();
    System.out.println("Creation de la connexion");

    final IPersonne sp = ServeurPersonne.createPersonne("test-personne-lido", "test-organisme-lido");
    System.out.println("Connexion au serveur Lido : " + clido);
    IConnexion c = clido.connexion(sp);
    chydro.etude(usine.creeHydraulique1dEtude1d());
    IParametresLido plido = IParametresLidoHelper.narrow(clido.parametres(c));
    IResultatsLido rlido = IResultatsLidoHelper.narrow(clido.resultats(c));
    chydro.calculCode(clido);
    try {
      plido.parametresCAL(DParametresLido.litParametresCAL(new File("p.cal")));
      plido.parametresCLM(DParametresLido.litParametresCLM(new File("p.clm")));
      plido.parametresLIG(DParametresLido.litParametresLIG(new File("p.lig")));
      plido.parametresRZO(DParametresLido.litParametresRZO(new File("p.rzo")));
      plido.parametresPRO(DParametresLido.litParametresPRO(new File("p.pro"), plido.parametresRZO().nbBief));
      plido.parametresSNG(DParametresLido.litParametresSNG(new File("p.sng")));
      plido.parametresEXT(new SParametresEXT());
      rlido.resultatsRSN(DResultatsLido.litResultatsRSN(new File("p.rsn"), null, plido.parametresRZO().nbBief, plido
          .parametresCAL().temporel.pas2TImp, plido.parametresCAL().titreCAL[0], null));
      rlido.resultatsERN(DResultatsLido.litResultatsERN(new File("p.ern")));
      CConversionHydraulique1d conv = new CConversionHydraulique1d(chydro, c);
      conv.parametresLidoToHydraulique1d();
      conv.parametresHydraulique1dToLido();
      chydro.etude().resultatsGeneraux(usine.creeHydraulique1dResultatsGeneraux());
      chydro.etude().reseau().creeResultatsReseau();
      conv.resultatsLidoToHydraulique1d();
      conv.resultatsHydraulique1dToLido();
      DParametresLido.ecritParametresCAL(new File("test.cal"), plido.parametresCAL());
      DParametresLido.ecritParametresCLM(new File("test.clm"), plido.parametresCLM());
      DParametresLido.ecritParametresLIG(new File("test.lig"), plido.parametresLIG());
      DParametresLido.ecritParametresRZO(new File("test.rzo"), plido.parametresRZO());
      DParametresLido.ecritParametresPRO(new File("test.pro"), plido.parametresPRO(), plido.parametresRZO());
      DParametresLido.ecritParametresSNG(new File("test.sng"), plido.parametresSNG());
      DResultatsLido.ecritResultatsRSN(new File("test.rsn"), rlido.resultatsRSN(), plido.parametresCAL(), true, null);
      DResultatsLido.ecritResultatsERN(new File("test.ern"), rlido.resultatsERN());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
