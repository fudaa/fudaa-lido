/*
 * @file         SeuilClient.java
 * @creation     2000-07-21
 * @modification $Date: 2006-10-19 14:12:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.seuil;

import java.io.File;
import java.io.IOException;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.seuil.ICalculSeuil;
import org.fudaa.dodico.corba.seuil.ICalculSeuilHelper;
import org.fudaa.dodico.corba.seuil.IParametresSeuil;
import org.fudaa.dodico.corba.seuil.IParametresSeuilHelper;
import org.fudaa.dodico.corba.seuil.IResultatsSeuil;
import org.fudaa.dodico.corba.seuil.IResultatsSeuilHelper;
import org.fudaa.dodico.corba.seuil.SResultatsSEU;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.ServeurPersonne;
import org.fudaa.dodico.seuil.DParametresSeuil;

/**
 * Une classe client de SeuilServeur.
 * 
 * @version $Revision: 1.2 $ $Date: 2006-10-19 14:12:28 $ by $Author: deniger $
 * @author Jean-Yves Riou
 */
public final class SeuilClient {

  private SeuilClient() {}

  public static void main(final String[] _args) {
    ICalculSeuil seuil = null;
    System.out.println("SeuilClient");
    // int essai=0;
    do {
      seuil = ICalculSeuilHelper.narrow(CDodico.findServerByInterface("::seuil::ICalculSeuil"));
    } while (seuil == null);
    System.out.println("Creation de la connexion");
    // IUsine usine=CDodico.creeUsineLocale();
    final IPersonne sp = ServeurPersonne.createPersonne("test-personne-seuil", "test-organisme-seuil");
    System.out.println("Connexion au serveur Seuil : " + seuil);
    final IConnexion c = seuil.connexion(sp);
    final IParametresSeuil params = IParametresSeuilHelper.narrow(seuil.parametres(c));
    try {
      params.parametresSEU(DParametresSeuil.litParametresSEU(new File("seuil01" + ".seu")));
    } catch (final IOException e) {
      System.err.println(e);
      System.exit(1);
    }
    seuil.calcul(c);
    // System.out.println("apres calcul seuil.resultats() = " +seuil. resultats());
    final IResultatsSeuil results = IResultatsSeuilHelper.narrow(seuil.resultats(c));
    // System.out.println("apres calcul results = " + results);
    // System.out.println("apres calcul results.resultatsSEU() = " + results.resultatsSEU());
    final SResultatsSEU resultats = results.resultatsSEU();
    System.out.println("apres calcul  resultats = " + resultats);
  }
}
